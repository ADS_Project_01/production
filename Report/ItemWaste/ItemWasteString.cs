﻿// Type: BCE.AutoCount.Manufacturing.Production.ProductionString
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Localization;

namespace Production.Report.ItemWaste
{
  [LocalizableString]
  public enum ItemWasteString
    {
    [DefaultString("Batch Print ItemWaste / Print ItemWaste Listing")] BatchPrintItemWaste_PrintItemWasteListing,
    [DefaultString("Print ItemWaste Detail Listing")] PrintItemWasteDetailListing,
    [DefaultString("Print Outstanding ItemWaste Listing")] PrintOutstandingItemWasteListing,
    [DefaultString("{0} opened ItemWaste List window.")] OpenItemWaste,
    [DefaultString("ItemWaste {0}")] ItemWaste,
    [DefaultString("{0} viewed ItemWaste {1} in edit mode.")] ViewItemWasteInEditMode,
    [DefaultString("{0} viewed ItemWaste {1}.")] ViewItemWaste,
    [DefaultString("{0} edited ItemWaste {1}.")] EditedItemWaste,
    [DefaultString("{0} cancels ItemWaste {1}.")] CancelItemWaste,
    [DefaultString("{0} un-cancels ItemWaste {1}.")] UncancelItemWaste,
    [DefaultString("{0} created new ItemWaste {1}.")] CreatedNewItemWaste,
    [DefaultString("{0} updated ItemWaste {1}.")] UpdatedItemWaste,
    [DefaultString("{0} deleted ItemWaste {1}.")] DeletedItemWaste,
    [DefaultString("{0} opened Print ItemWaste Detail Listing window.")] OpenedPrintItemWasteDetailListing,
    [DefaultString("{0} inquired on Print ItemWaste Detail Listing.")] InquiredPrintItemWasteDetailListing,
    [DefaultString("ItemWaste Detail Listing")] ItemWasteDetailListing,
    [DefaultString("{0} opened Print ItemWaste Listing window.")] OpenedPrintItemWasteListing,
    [DefaultString("{0} inquired on Print ItemWaste Listing.")] InquiredPrintItemWasteListing,
    [DefaultString("{0} inquired on Batch Print ItemWaste.")] InquiredBatchPrintItemWaste,
    [DefaultString("Batch ItemWaste")] BatchItemWaste,
    [DefaultString("ItemWaste Listing")] ItemWasteListing,
    [DefaultString("{0} opened Print Outstanding ItemWaste Listing window.")] OpenedPrintOutstandingItemWasteListing,
    [DefaultString("{0} inquired on Print Outstanding ItemWaste Listing.")] InquiredPrintOutstandingItemWasteListing,
    [DefaultString("Outstanding ItemWaste Listing.")] OutstandingItemWasteListing,
  }
}
