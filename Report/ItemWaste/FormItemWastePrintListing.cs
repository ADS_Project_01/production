﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormStockAssemblyOrderPrintDetailListing
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Settings;
using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Production.Report.ItemWaste
{
    [BCE.AutoCount.PlugIn.MenuItem("Item Waste Print Listing", BeginNewGroup = false, MenuOrder = 3, OpenAccessRight = "RPA_ITEMWASTE_LISTING_OPEN", ParentBeginNewGroup = false, ParentMenuCaption = "Report", ParentMenuOrder = 7, ShowAsDialog = false, VisibleAccessRight = "RPA_ITEMWASTE_LISTING_SHOW")]

    [SingleInstanceThreadForm]

    public class FormItemWastePrintListing : XtraForm
  {
    public static string myColumnName;
    private DBSetting myDBSetting;
    private DataTable myDataTable;
    private DataTable myDetailDataTable;
    private DataSet myDataSet;
    private ItemWasteReportCommand myCommand;
    private bool myInSearch;
    private ItemWasteReportingCriteria myReportingCriteria;
    private const string LISTING = "Print ItemWaste Listing";
    private bool myLoadAllColumns;
    private bool myFilterByLocation;
   // private ScriptObject myScriptObject;
    private UserAuthentication myUserAuthentication;
    private MouseDownHelper myMouseDownHelper;
    private IContainer components;
    private PanelHeader panelHeader1;
    private PreviewButton previewButton1;
    private PrintButton printButton1;
    private UCDateSelector ucDateSelector2;
    private UCItemSelector ucItemSelector2;
    private UCSearchResult ucSearchResult1;
   // private UCWOSelector ucStockAssemblyOrderSelector1;
  //  private UCWOSelector ucStockAssemblyOrderSelector2;
    private Bar bar1;
    private BarButtonItem barbtnAdvancedFilter;
    private BarButtonItem barBtnDesignDetailListingReport;
    private BarDockControl barDockControlBottom;
    private BarDockControl barDockControlLeft;
    private BarDockControl barDockControlRight;
    private BarDockControl barDockControlTop;
    private BarManager barManager1;
    private BarSubItem barSubItem1;
    private CheckEdit chkEditShowCriteria2;
    private ComboBoxEdit cbEditGroupBy2;
    private ComboBoxEdit cbEditSortBy2;
    private GroupControl gbBasic;
    private GroupControl groupControl1;
    private MemoEdit memoEdit_Criteria;
    private PanelControl panelCenter;
    private PanelControl panelControl2;
    private PanelControl pnCriteriaBasic;
    private RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private RepositoryItemTextEdit repositoryItemTextEdit_Cancelled;
    private SimpleButton sbtnAdvOptions;
    private SimpleButton sbtnClose;
    private SimpleButton sbtnInquiry;
    private SimpleButton sbtnToggleOptions;
    private SimpleButton simpleButton1;
    private GridColumn colCheck;
    private GridColumn colDeptNo;
    private GridColumn colDocDate;
    private GridColumn colDocNo;
    private GridColumn colDtlDescription;
    private GridColumn colFurtherDescription;
    private GridColumn colItemCode;
    private GridColumn colLocation;
    private GridColumn colProjNo;
    private GridColumn colProdQty;
    private GridColumn colRate;
    private GridColumn colProdCost;
    private GridControl gridControl1;
    private GridView gvMaster;
    private XtraTabControl xtraTabControl2;
    private XtraTabPage xtraTabPage3;
    private XtraTabPage xtraTabPage4;
    private Label label12;
    private Label label13;
    private Label label14;
    private Label label15;
    private Label label16;
    private GridColumn colExpCompletedDate;
    private GridColumn colProjDesc;
    private GridColumn colDeptDesc;
        private XtraTabPage xtraTabPage5;
        private XtraTabPage xtraTabPage6;
        private GridColumn gridColumn1;
        private GridColumn colItemWasteEndDate;
        private GridColumn colBOMCode;
        private GridColumn colUOM;
        private Bar bar2;
        private BarSubItem barSubItem2;
        private BarButtonItem barButtonItem1;
        private BarButtonItem barButtonItem2;
        private GridColumn colWONo;
        private GridColumn colWODate;
        private GridColumn colItemWasteStartDate;
        private GridColumn colDocType;
        private WorkOrder.UCWOSelector ucwoSelector1;
        private GridColumn colWOQty;
        private GridColumn colWOCost;
        private GridColumn DtlKey;
        private BarButtonItem barBtnConvertToPlainText;

    public DataTable MasterDataTable
    {
      get
      {
        return this.myDataTable;
      }
    }

    public DataTable DetailDataTable
    {
      get
      {
        return this.myDetailDataTable;
      }
    }

    public string SelectedDocNosInString
    {
      get
      {
        return this.GenerateDocNosToString();
      }
    }

    public string SelectedDocKeysInString
    {
      get
      {
        return StringHelper.ArrayListToCommaString(new ArrayList((ICollection) CommonFunction.GetDocKeyList("ToBeUpdate", this.myDataTable)));
      }
    }

    public string SelectedDtlKeysInString
    {
      get
      {
        return StringHelper.ArrayListToCommaString(new ArrayList((ICollection) CommonFunction.GetDtlKeyList("ToBeUpdate", this.myDataTable)));
      }
    }

    public string ColumnName
    {
      get
      {
        return FormItemWastePrintListing.myColumnName;
      }
    }

    public ItemWasteReportingCriteria StockAssemblyReportingCriteria
    {
      get
      {
        return this.myReportingCriteria;
      }
    }

    public FormItemWastePrintListing(DBSetting dbSetting)
    {
      this.InitializeComponent();
      //this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockAssemblyOrderDetailListing");
      this.myCommand = ItemWasteReportCommand.Create(dbSetting, new BasicReportOption());
      this.myDBSetting = this.myCommand.DBSetting;
      this.myUserAuthentication = UserAuthentication.GetOrCreate(this.myDBSetting);
      this.previewButton1.ReportType = "ItemWaste Listing";
      this.previewButton1.SetDBSetting(this.myDBSetting);
      this.printButton1.ReportType = "ItemWaste Listing";
      this.printButton1.SetDBSetting(this.myDBSetting);
      this.myDataTable = new DataTable("Master");
      this.myDetailDataTable = new DataTable("Detail");
      this.myDataSet = new DataSet();
      this.myDataSet.Tables.Add(this.myDataTable);
      this.myDataSet.Tables.Add(this.myDetailDataTable);
      this.gridControl1.DataSource = (object) this.myDataTable;
      this.LoadCriteria();
      this.ucSearchResult1.Initialize(this.gvMaster, "ToBeUpdate");
      CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name+"PRD", this.gvMaster, new EventHandler(this.ReloadAllColumns));
      this.Tag = (object) EnterKeyMessageFilter.NoFilter;
      this.InitUserControls();
      this.InitFormControls();
      this.InitializeSettings();
    //  new UDFUtil(this.myDBSetting).SetupDetailListingReportGrid(this.gvMaster, "RPA_ItemWasteReport_v", "RPA_WODTL");
      this.RefreshDesignReport();
      this.myMouseDownHelper = new MouseDownHelper();
      this.myMouseDownHelper.Init(this.gvMaster);
      this.myUserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.RefreshDesignReport), (Component) this);
      BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "ItemWasteReport.htm");
      DBSetting dbSetting1 = dbSetting;
      string docType = "";
      long docKey = 0L;
      long eventKey = 0L;
      // ISSUE: variable of a boxed type
      ItemWasteString local =  ItemWasteString.OpenedPrintItemWasteListing;
      object[] objArray = new object[1];
      int index = 0;
      string loginUserId = this.myUserAuthentication.LoginUserID;
      objArray[index] = (object) loginUserId;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      string detail = "";
      Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
      string fieldname1 = "WOQty";
      string fieldtype1 = "Quantity";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "Rate";
      string fieldtype2 = "Currency";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "WOCost";
      string fieldtype3 = "Currency";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "ProdQty";
      string fieldtype4 = "Quantity";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "Rate";
      string fieldtype5 = "Quantity";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "ProdCost";
      string fieldtype6 = "Currency";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "ItemWasteEndDate";
      string fieldtype7 = "Date";
      formControlUtil.AddField(fieldname7, fieldtype7);
      string fieldname8 = "ItemWasteStartDate";
      string fieldtype8 = "Date";
      formControlUtil.AddField(fieldname8, fieldtype8);
      string fieldname9 = "DocDate";
      string fieldtype9 = "Date";
      formControlUtil.AddField(fieldname9, fieldtype9);
      string fieldname10 = "WODate";
      string fieldtype10 = "Date";
      formControlUtil.AddField(fieldname10, fieldtype10);
      string fieldname11 = "CreatedTimeStamp";
      string fieldtype11 = "DateTime";
      formControlUtil.AddField(fieldname11, fieldtype11);
      FormItemWastePrintListing printDetailListing = this;
      formControlUtil.InitControls((Control) printDetailListing);
      DecimalSetting orCreate = DecimalSetting.GetOrCreate(this.myDBSetting);
      GridGroupSummaryItemCollection groupSummary1 = this.gvMaster.GroupSummary;
      int num1 = 3;
      string fieldName1 = "ItemCode";
      // ISSUE: variable of the null type
      GridColumn local1 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local2 = GridGroupSummaryItemStringId.ItemCodeCount;
      object[] objArray1 = new object[1];
      int index1 = 0;
      string str = "{0}";
      objArray1[index1] = (object) str;
      string string1 = BCE.Localization.Localizer.GetString((Enum) local2, objArray1);
      groupSummary1.Add((SummaryItemType) num1, fieldName1, (GridColumn) local1, string1);
      GridGroupSummaryItemCollection groupSummary2 = this.gvMaster.GroupSummary;
      int num2 = 0;
      string fieldName2 = "Total";
      // ISSUE: variable of the null type
      GridColumn local3 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.Total;
      object[] objArray2 = new object[1];
      int index2 = 0;
      string currencyFormatString1 = orCreate.GetCurrencyFormatString(0);
      objArray2[index2] = (object) currencyFormatString1;
      string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
      groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      GridGroupSummaryItemCollection groupSummary3 = this.gvMaster.GroupSummary;
      int num3 = 0;
      string fieldName3 = "NetTotal";
      // ISSUE: variable of the null type
      GridColumn local5 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local6 =  GridGroupSummaryItemStringId.NetTotal;
      object[] objArray3 = new object[1];
      int index3 = 0;
      string currencyFormatString2 = orCreate.GetCurrencyFormatString(0);
      objArray3[index3] = (object) currencyFormatString2;
      string string3 = BCE.Localization.Localizer.GetString((Enum) local6, objArray3);
      groupSummary3.Add((SummaryItemType) num3, fieldName3, (GridColumn) local5, string3);
      GridGroupSummaryItemCollection groupSummary4 = this.gvMaster.GroupSummary;
      int num4 = 0;
      string fieldName4 = "AssemblyCost";
      // ISSUE: variable of the null type
      GridColumn local7 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local8 =  GridGroupSummaryItemStringId.AssemblyCost;
      object[] objArray4 = new object[1];
      int index4 = 0;
      string costFormatString1 = orCreate.GetCostFormatString(0);
      objArray4[index4] = (object) costFormatString1;
      string string4 = BCE.Localization.Localizer.GetString((Enum) local8, objArray4);
      groupSummary4.Add((SummaryItemType) num4, fieldName4, (GridColumn) local7, string4);
      GridGroupSummaryItemCollection groupSummary5 = this.gvMaster.GroupSummary;
      int num5 = 0;
      string fieldName5 = "Qty";
      // ISSUE: variable of the null type
      GridColumn local9 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local10 =GridGroupSummaryItemStringId.Quantity;
      object[] objArray5 = new object[1];
      int index5 = 0;
      string quantityFormatString = orCreate.GetQuantityFormatString(0);
      objArray5[index5] = (object) quantityFormatString;
      string string5 = BCE.Localization.Localizer.GetString((Enum) local10, objArray5);
      groupSummary5.Add((SummaryItemType) num5, fieldName5, (GridColumn) local9, string5);
      GridGroupSummaryItemCollection groupSummary6 = this.gvMaster.GroupSummary;
      int num6 = 0;
      string fieldName6 = "ItemCost";
      // ISSUE: variable of the null type
      GridColumn local11 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local12 =  GridGroupSummaryItemStringId.ItemCost;
      object[] objArray6 = new object[1];
      int index6 = 0;
      string costFormatString2 = orCreate.GetCostFormatString(0);
      objArray6[index6] = (object) costFormatString2;
      string string6 = BCE.Localization.Localizer.GetString((Enum) local12, objArray6);
      groupSummary6.Add((SummaryItemType) num6, fieldName6, (GridColumn) local11, string6);
      GridGroupSummaryItemCollection groupSummary7 = this.gvMaster.GroupSummary;
      int num7 = 0;
      string fieldName7 = "OverHeadCost";
      // ISSUE: variable of the null type
      GridColumn local13 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local14 = GridGroupSummaryItemStringId.OverHeadCost;
      object[] objArray7 = new object[1];
      int index7 = 0;
      string costFormatString3 = orCreate.GetCostFormatString(0);
      objArray7[index7] = (object) costFormatString3;
      string string7 = BCE.Localization.Localizer.GetString((Enum) local14, objArray7);
      groupSummary7.Add((SummaryItemType) num7, fieldName7, (GridColumn) local13, string7);
      GridGroupSummaryItemCollection groupSummary8 = this.gvMaster.GroupSummary;
      int num8 = 0;
      string fieldName8 = "SubTotalCost";
      // ISSUE: variable of the null type
      GridColumn local15 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local16 =  GridGroupSummaryItemStringId.SubTotalCost;
      object[] objArray8 = new object[1];
      int index8 = 0;
      string costFormatString4 = orCreate.GetCostFormatString(0);
      objArray8[index8] = (object) costFormatString4;
      string string8 = BCE.Localization.Localizer.GetString((Enum) local16, objArray8);
      groupSummary8.Add((SummaryItemType) num8, fieldName8, (GridColumn) local15, string8);
    }

    private void SaveCriteria()
    {
      PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting) this.myReportingCriteria, "ItemWasteReport2.setting");
    }

    private void LoadCriteria()
    {
      try
      {
        this.myReportingCriteria = (ItemWasteReportingCriteria) PersistenceUtil.LoadCriteriaData(this.myDBSetting, "ItemWasteReport2.setting");
      }
      catch
      {
      }
      if (this.myReportingCriteria == null)
        this.myReportingCriteria = new ItemWasteReportingCriteria();
      this.cbEditGroupBy2.EditValue = (object) this.myReportingCriteria.GroupBy;
      this.cbEditSortBy2.EditValue = (object) this.myReportingCriteria.SortBy;
      this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
      //this.chkEditShowCriteria.Checked = this.myReportingCriteria.IsShowCriteria;
      //if (this.myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.All)
        //this.cbCancelledOption.SelectedIndex = 0;
      //else if (this.myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
      //  this.cbCancelledOption.SelectedIndex = 1;
      //else
        //this.cbCancelledOption.SelectedIndex = 2;
    }

    private void InitUserControls()
    {
     
      {
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToAdvancedOptions, new object[0]);
       // this.panelCriteria.Visible = false;
        this.pnCriteriaBasic.Visible = true;
        this.cbEditGroupBy2.Text = this.myReportingCriteria.GroupBy;
        this.cbEditSortBy2.Text = this.myReportingCriteria.SortBy;
      }
     // this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
    //  this.ucStockAssemblyOrderSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter,false);
     // this.ucItemSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
     // this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupFilter);
     // this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeFilter);
     // this.ucLocationSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.LocationFilter);
    //  this.ucProjectSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ProjecNoFilter);
     // this.ucDepartmentSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DeptNoFilter);
      this.ucDateSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
      this.ucwoSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter,false);
      this.ucItemSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
    }

    private void InitializeSettings()
    {
      this.GetGroupSortCriteria();
    }

    private void GetGroupSortCriteria()
    {
     
      {
        this.myReportingCriteria.GroupBy = this.cbEditGroupBy2.Text;
        this.myReportingCriteria.SortBy = this.cbEditSortBy2.Text;
        this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria2.Checked;
      }
    }

    private void AddNewColumn()
    {
      this.myDataTable.Columns.Add(new DataColumn()
      {
        DataType = typeof (bool),
        AllowDBNull = true,
        Caption = "Check",
        ColumnName = "ToBeUpdate",
        DefaultValue = (object) false
      });
    }

    private void BasicSearch(bool isSearchAll)
    {
      if (!this.myInSearch)
      {
        this.myInSearch = true;
        BCE.XtraUtils.GridViewUtils.UpdateData(this.gvMaster);
        this.gridControl1.DataSource = (object) null;
        try
        {
          this.gridControl1.MainView.UpdateCurrentRow();
          this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
          string columnName = CommonFunction.BuildSQLColumns(isSearchAll, this.gvMaster.Columns.View);
          if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
            this.AddNewColumn();
          this.myCommand.BasicSearch(this.myReportingCriteria, columnName, this.myDataSet, "ToBeUpdate");
          this.gridControl1.DataSource = (object) this.myDataTable;
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage(ex.Message);
        }
        finally
        {
          this.myInSearch = false;
        }
        //FormItemWastePrintDetailListing.FormInquiryEventArgs inquiryEventArgs1 = new FormItemWastePrintDetailListing.FormInquiryEventArgs(this, this.myDataTable);
        //ScriptObject scriptObject = this.myScriptObject;
        //string name = "OnFormInquiry";
        //System.Type[] types = new System.Type[1];
        //int index1 = 0;
        //System.Type type = inquiryEventArgs1.GetType();
        //types[index1] = type;
        //object[] objArray = new object[1];
        //int index2 = 0;
        //FormItemWastePrintDetailListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
        //objArray[index2] = (object) inquiryEventArgs2;
        //scriptObject.RunMethod(name, types, objArray);
      }
    }

   

    private string GenerateDocNosToString()
    {
      BCE.XtraUtils.GridViewUtils.UpdateData(this.gvMaster);
      DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = True");
      if (dataRowArray.Length == 0)
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderDetailSelected, new object[0]));
        this.DialogResult = DialogResult.None;
        return (string) null;
      }
      else
      {
        string str = "";
        foreach (DataRow dataRow in dataRowArray)
        {
          if (str.Length != 0)
            str = str + ", ";
          str = str + "'" + dataRow["WONo"].ToString() + "'";
        }
        return str;
      }
    }

    private void sbtnInquiry_Click(object sender, EventArgs e)
    {
      this.sbtnToggleOptions.Enabled = true;
      this.BasicSearch(this.myLoadAllColumns);
     
      if (this.cbEditSortBy2.SelectedIndex == 0)
      {
        this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.Ascending;
        this.gvMaster.Columns["WODate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 1)
      {
        this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["WODate"].SortOrder = ColumnSortOrder.Ascending;
        //this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 2)
      {
        this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["WODate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.Ascending;
        //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 3)
      {
        this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["WODate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.Ascending;
        //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      //else if (this.cbEditSortBy2.SelectedIndex == 4)
      //{
      //  this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.Ascending;
      //  this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //}
      //else if (this.cbEditSortBy2.SelectedIndex == 5)
      //{
      //  this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.Ascending;
      //  this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //}
      //else if (this.cbEditSortBy2.SelectedIndex == 6)
      //{
      //  this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.Ascending;
      //  this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //}
      //else if (this.cbEditSortBy2.SelectedIndex == 7)
      //{
      //  this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //  this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.Ascending;
      //}
      this.ucSearchResult1.CheckAll();
      this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
      DBSetting dbSetting = this.myDBSetting;
      string docType = "";
      long docKey = 0L;
      long eventKey = 0L;
      // ISSUE: variable of a boxed type
      ItemWasteString local =  ItemWasteString.InquiredPrintItemWasteListing;
      object[] objArray = new object[1];
      int index = 0;
      string loginUserId = this.myUserAuthentication.LoginUserID;
      objArray[index] = (object) loginUserId;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      string text = this.memoEdit_Criteria.Text;
      Activity.Log(dbSetting, docType, docKey, eventKey, @string, text);
    }

    private void ReloadAllColumns(object sender, EventArgs e)
    {
      this.myLoadAllColumns = true;
      this.BasicSearch(true);
    }

    private void barbtnAdvancedFilter_ItemClick(object sender, ItemClickEventArgs e)
    {
      
      {
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToBasicOptions, new object[0]);
        this.pnCriteriaBasic.Visible = false;
       // this.panelCriteria.Visible = true;
        //this.myReportingCriteria.AdvancedOptions = true;
        //this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
        //this.ucStockAssemblyOrderSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter,false);
        //this.ucItemSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
        //this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupFilter);
        //this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeFilter);
        //this.ucLocationSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.LocationFilter);
       // this.ucProjectSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ProjecNoFilter);
      //  this.ucDepartmentSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DeptNoFilter);
      //  this.cbEditGroupBy.SelectedIndex = this.cbEditGroupBy2.SelectedIndex;
       // this.cbEditSortBy.SelectedIndex = this.cbEditSortBy2.SelectedIndex;
       // this.chkEditShowCriteria.Checked = this.chkEditShowCriteria2.Checked;
      }
    }

    private void sbtnToggleOptions_Click(object sender, EventArgs e)
    {
      
      {
        this.pnCriteriaBasic.Visible = !this.pnCriteriaBasic.Visible;
        if (this.pnCriteriaBasic.Visible)
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_HideOptions, new object[0]);
        else
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowOptions, new object[0]);
      }
    }

    private void sbtnClose_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void cbCancelledOption_SelectedIndexChanged(object sender, EventArgs e)
    {
      //if (this.myReportingCriteria != null)
      //{
      //  if (this.cbCancelledOption.SelectedIndex == 0)
      //    this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.All;
      //  else if (this.cbCancelledOption.SelectedIndex == 1)
      //    this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.Cancelled;
      //  else
      //    this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.UnCancelled;
      //}
    }

    private void FormStockAssemblyPrintDetailListing_Closing(object sender, CancelEventArgs e)
    {
      this.SaveCriteria();
    }

    private void FormStockAssemblyPrintDetailListing_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == (Keys) 120)
      {
        if (this.sbtnInquiry.Enabled)
          this.sbtnInquiry.PerformClick();
      }
      else if (e.KeyCode == (Keys) 119)
      {
        if (this.previewButton1.Enabled)
          this.previewButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 118)
      {
        if (this.printButton1.Enabled)
          this.printButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 117 && this.sbtnToggleOptions.Enabled)
        this.sbtnToggleOptions.PerformClick();
    }

    private void cbEditSortBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      //if (this.myReportingCriteria != null)
      //{
      //  if (this.myReportingCriteria.SortBy != this.cbEditSortBy2.Text)
      //    this.myReportingCriteria.SortBy = this.cbEditSortBy2.Text;
      //  if (this.cbEditSortBy2.SelectedIndex == 0)
      //  {
      //    this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
      //    this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //  }
      //  else if (this.cbEditSortBy2.SelectedIndex == 1)
      //  {
      //    this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
      //    this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //  }
      //  else if (this.cbEditSortBy2.SelectedIndex == 2)
      //  {
      //    this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.Ascending;
      //    this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //  }
      //  else if (this.cbEditSortBy2.SelectedIndex == 3)
      //  {
      //    this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.Ascending;
      //    this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //  }
      //  else if (this.cbEditSortBy2.SelectedIndex == 4)
      //  {
      //    this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.Ascending;
      //    this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //  }
      //  else if (this.cbEditSortBy.SelectedIndex == 5)
      //  {
      //    this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.Ascending;
      //    this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //  }
      //  else if (this.cbEditSortBy.SelectedIndex == 6)
      //  {
      //    this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.Ascending;
      //    this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      //  }
      //  else if (this.cbEditSortBy.SelectedIndex == 7)
      //  {
      //    this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
      //    this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.Ascending;
      //  }
      //}
    }

    private void cbEditSortBy2_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
      {
        if (this.myReportingCriteria.SortBy != this.cbEditSortBy2.Text)
          this.myReportingCriteria.SortBy = this.cbEditSortBy2.Text;
        if (this.cbEditSortBy2.SelectedIndex == 0)
        {
          this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 1)
        {
          this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 2)
        {
          this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.Ascending;
          //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 3)
        {
          this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.Ascending;
          //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 4)
        {
          this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.Ascending;
          //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 5)
        {
          this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.Ascending;
          //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 6)
        {
          this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.Ascending;
          //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 7)
        {
          this.gvMaster.Columns["WONo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          //this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.Ascending;
        }
      }
    }

    private void cbEditGroupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      //if (this.myReportingCriteria != null)
      //  this.myReportingCriteria.GroupBy = this.cbEditGroupBy.Text;
    }

    private void cbEditGroupBy2_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.GroupBy = this.cbEditGroupBy2.Text;
    }

    private void sbtnAdvOptions_Click(object sender, EventArgs e)
    {
     
    }

    private void sbtnAdvanceSearch_Click(object sender, EventArgs e)
    {
      
    }

    private void gvMaster_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      GridView gridView = (GridView) sender;
      int[] selectedRows = gridView.GetSelectedRows();
      if (selectedRows != null && selectedRows.Length != 1)
      {
        for (int index = 0; index < selectedRows.Length; ++index)
          gridView.GetDataRow(selectedRows[index])["ToBeUpdate"] = (object) true;
      }
    }

    private void chkEditShowCriteria_CheckedChanged(object sender, EventArgs e)
    {
      //if (this.myReportingCriteria != null)
      //  this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
    }

    private void repositoryItemTextEdit_Cancelled_FormatEditValue(object sender, ConvertEditValueEventArgs e)
    {
      if (e.Value != null)
      {
        if (e.Value.ToString() == "T")
          e.Value = (object) BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Cancelled, new object[0]);
        else
          e.Value = (object) "";
      }
    }

    private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      string selectedDtlKeysInString = this.SelectedDtlKeysInString;
      if (selectedDtlKeysInString == "")
      {
        AppMessage.ShowMessage((IWin32Window) this, "No Item Waste/ NG Listing record is Selected");
        this.DialogResult = DialogResult.None;
      }
      else if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_ITEMWASTE_LISTING_PREVIEW"))
      {
        Cursor current = Cursor.Current;
        Cursor.Current = Cursors.WaitCursor;
        this.GetGroupSortCriteria();
        this.myCommand.PreviewListingReport(selectedDtlKeysInString, this.myReportingCriteria, e.DefaultReport);
        Cursor.Current = current;
      }
    }

    private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      string selectedDtlKeysInString = this.SelectedDtlKeysInString;
      if (selectedDtlKeysInString == "")
      {
        AppMessage.ShowMessage((IWin32Window) this, "No Item Waste/ NG Listing record is Selected");
        this.DialogResult = DialogResult.None;
      }
      else if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_ITEMWASTE_LISTING_PRINT"))
      {
        Cursor current = Cursor.Current;
        Cursor.Current = Cursors.WaitCursor;
        this.GetGroupSortCriteria();
        this.myCommand.PrintListingReport(selectedDtlKeysInString, this.myReportingCriteria, e.DefaultReport);
        Cursor.Current = current;
      }
    }

    private void barBtnDesignDetailListingReport_ItemClick(object sender, ItemClickEventArgs e)
    {
     // ReportTool.DesignReport("ItemWaste Listing", this.myDBSetting);
            string selectedDtlKeysInString = this.SelectedDtlKeysInString;
            ReportTool.DesignReport("ItemWaste Listing", this.myCommand.GetListingReportDataSource(selectedDtlKeysInString, this.myReportingCriteria), this.myDBSetting);
        }

    public virtual void RefreshDesignReport()
    {
      this.barBtnDesignDetailListingReport.Visibility = XtraBarsUtils.ToBarItemVisibility(this.myUserAuthentication.AccessRight.IsAccessible("TOOLS_RPT_SHOW"));
      //if (this.ucLocationSelector1 != null)
      //{
      //  this.myFilterByLocation = SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnableFilterByCurrentUserLocationAccessRight;
      //  if (this.myFilterByLocation)
      //  {
      //    this.ucLocationSelector1.Filter.Type = FilterType.ByRange;
      //    this.ucLocationSelector1.Filter.From = (object) this.myUserAuthentication.MainLocation;
      //    this.ucLocationSelector1.Filter.To = (object) this.myUserAuthentication.MainLocation;
      //    this.ucLocationSelector1.ApplyFilter();
      //    this.ucLocationSelector1.Enabled = false;
      //  }
      //  else
      //    this.ucLocationSelector1.Enabled = true;
      //}
    }

    private void FormSAOPrintDetailListing_Load(object sender, EventArgs e)
    {
      this.FormInitialize();
    }

    private void FormInitialize()
    {
      //FormItemWastePrintDetailListing.FormInitializeEventArgs initializeEventArgs1 = new FormItemWastePrintDetailListing.FormInitializeEventArgs(this);
      //ScriptObject scriptObject = this.myScriptObject;
      //string name = "OnFormInitialize";
      //System.Type[] types = new System.Type[1];
      //int index1 = 0;
      //System.Type type = initializeEventArgs1.GetType();
      //types[index1] = type;
      //object[] objArray = new object[1];
      //int index2 = 0;
      //FormItemWastePrintDetailListing.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
      //objArray[index2] = (object) initializeEventArgs2;
      //scriptObject.RunMethod(name, types, objArray);
    }

    private void barBtnConvertToPlainText_ItemClick(object sender, ItemClickEventArgs e)
    {
      RichTextHelper.ConvertToPlainText(this.gvMaster);
    }

    private void gvMaster_DoubleClick(object sender, EventArgs e)
    {
      if (this.myMouseDownHelper.IsLeftMouseDown && BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView) sender).InRow && UserAuthentication.GetOrCreate(this.myDBSetting).AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
        this.GoToDocument();
    }

    private void GoToDocument()
    {
      ColumnView columnView = (ColumnView) this.gridControl1.FocusedView;
      int focusedRowHandle = columnView.FocusedRowHandle;
      DataRow dataRow = columnView.GetDataRow(focusedRowHandle);
      if (dataRow != null)
        DocumentDispatcher.Open(this.myDBSetting, "WO", BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.pnCriteriaBasic = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAdvOptions = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.chkEditShowCriteria2 = new DevExpress.XtraEditors.CheckEdit();
            this.cbEditSortBy2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbEditGroupBy2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.gbBasic = new DevExpress.XtraEditors.GroupControl();
            this.ucwoSelector1 = new WorkOrder.UCWOSelector();
            this.ucItemSelector2 = new BCE.AutoCount.FilterUI.UCItemSelector();
            this.ucDateSelector2 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panelCenter = new DevExpress.XtraEditors.PanelControl();
            this.printButton1 = new BCE.AutoCount.Controls.PrintButton();
            this.previewButton1 = new BCE.AutoCount.Controls.PreviewButton();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnToggleOptions = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gvMaster = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.DtlKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWONo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWODate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemWasteStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemWasteEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDtlDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOMCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProdQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProdCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWOQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWOCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ucSearchResult1 = new BCE.AutoCount.FilterUI.UCSearchResult();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit_Criteria = new DevExpress.XtraEditors.MemoEdit();
            this.repositoryItemTextEdit_Cancelled = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFurtherDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeptNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeptDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpCompletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnDesignDetailListingReport = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnAdvancedFilter = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnConvertToPlainText = new DevExpress.XtraBars.BarButtonItem();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaBasic)).BeginInit();
            this.pnCriteriaBasic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbBasic)).BeginInit();
            this.gbBasic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelCenter)).BeginInit();
            this.panelCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit_Cancelled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(401, 75);
            this.xtraTabPage6.Text = "xtraTabPage6";
            // 
            // pnCriteriaBasic
            // 
            this.pnCriteriaBasic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnCriteriaBasic.Controls.Add(this.simpleButton1);
            this.pnCriteriaBasic.Controls.Add(this.sbtnAdvOptions);
            this.pnCriteriaBasic.Controls.Add(this.groupControl1);
            this.pnCriteriaBasic.Controls.Add(this.gbBasic);
            this.pnCriteriaBasic.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnCriteriaBasic.Location = new System.Drawing.Point(0, 66);
            this.pnCriteriaBasic.Name = "pnCriteriaBasic";
            this.pnCriteriaBasic.Size = new System.Drawing.Size(877, 121);
            this.pnCriteriaBasic.TabIndex = 2;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(591, 119);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "More Options";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.sbtnAdvanceSearch_Click);
            // 
            // sbtnAdvOptions
            // 
            this.sbtnAdvOptions.Location = new System.Drawing.Point(683, 119);
            this.sbtnAdvOptions.Name = "sbtnAdvOptions";
            this.sbtnAdvOptions.Size = new System.Drawing.Size(102, 23);
            this.sbtnAdvOptions.TabIndex = 1;
            this.sbtnAdvOptions.Text = "Advanced Filter...";
            this.sbtnAdvOptions.Visible = false;
            this.sbtnAdvOptions.Click += new System.EventHandler(this.sbtnAdvOptions_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.chkEditShowCriteria2);
            this.groupControl1.Controls.Add(this.cbEditSortBy2);
            this.groupControl1.Controls.Add(this.cbEditGroupBy2);
            this.groupControl1.Controls.Add(this.label14);
            this.groupControl1.Controls.Add(this.label16);
            this.groupControl1.Location = new System.Drawing.Point(591, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(194, 107);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Report Options";
            // 
            // chkEditShowCriteria2
            // 
            this.chkEditShowCriteria2.Location = new System.Drawing.Point(43, 73);
            this.chkEditShowCriteria2.Name = "chkEditShowCriteria2";
            this.chkEditShowCriteria2.Properties.Caption = "Show Criteria in Report";
            this.chkEditShowCriteria2.Size = new System.Drawing.Size(140, 19);
            this.chkEditShowCriteria2.TabIndex = 0;
            // 
            // cbEditSortBy2
            // 
            this.cbEditSortBy2.Location = new System.Drawing.Point(71, 47);
            this.cbEditSortBy2.Name = "cbEditSortBy2";
            this.cbEditSortBy2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditSortBy2.Properties.Items.AddRange(new object[] {
            "None",
            "WONo",
            "ItemCode",
            "Date"});
            this.cbEditSortBy2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditSortBy2.Size = new System.Drawing.Size(118, 20);
            this.cbEditSortBy2.TabIndex = 1;
            this.cbEditSortBy2.SelectedIndexChanged += new System.EventHandler(this.cbEditSortBy2_SelectedIndexChanged);
            // 
            // cbEditGroupBy2
            // 
            this.cbEditGroupBy2.Location = new System.Drawing.Point(71, 25);
            this.cbEditGroupBy2.Name = "cbEditGroupBy2";
            this.cbEditGroupBy2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditGroupBy2.Properties.DropDownRows = 10;
            this.cbEditGroupBy2.Properties.Items.AddRange(new object[] {
            "None",
            "WONo",
            "ItemCode",
            "Date"});
            this.cbEditGroupBy2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditGroupBy2.Size = new System.Drawing.Size(118, 20);
            this.cbEditGroupBy2.TabIndex = 2;
            this.cbEditGroupBy2.SelectedIndexChanged += new System.EventHandler(this.cbEditGroupBy2_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(5, 50);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 17);
            this.label14.TabIndex = 3;
            this.label14.Text = "Sort By";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(5, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 17);
            this.label16.TabIndex = 4;
            this.label16.Text = "Group By";
            // 
            // gbBasic
            // 
            this.gbBasic.Controls.Add(this.ucwoSelector1);
            this.gbBasic.Controls.Add(this.ucItemSelector2);
            this.gbBasic.Controls.Add(this.ucDateSelector2);
            this.gbBasic.Controls.Add(this.label15);
            this.gbBasic.Controls.Add(this.label12);
            this.gbBasic.Controls.Add(this.label13);
            this.gbBasic.Location = new System.Drawing.Point(12, 6);
            this.gbBasic.Name = "gbBasic";
            this.gbBasic.Size = new System.Drawing.Size(573, 107);
            this.gbBasic.TabIndex = 3;
            this.gbBasic.Text = "Basic Filter";
            // 
            // ucwoSelector1
            // 
            this.ucwoSelector1.Location = new System.Drawing.Point(106, 48);
            this.ucwoSelector1.Name = "ucwoSelector1";
            this.ucwoSelector1.Size = new System.Drawing.Size(449, 23);
            this.ucwoSelector1.TabIndex = 6;
            // 
            // ucItemSelector2
            // 
            this.ucItemSelector2.Appearance.Options.UseBackColor = true;
            this.ucItemSelector2.Location = new System.Drawing.Point(109, 75);
            this.ucItemSelector2.Name = "ucItemSelector2";
            this.ucItemSelector2.Size = new System.Drawing.Size(362, 20);
            this.ucItemSelector2.TabIndex = 1;
            // 
            // ucDateSelector2
            // 
            this.ucDateSelector2.Appearance.Options.UseBackColor = true;
            this.ucDateSelector2.Location = new System.Drawing.Point(109, 28);
            this.ucDateSelector2.Name = "ucDateSelector2";
            this.ucDateSelector2.Size = new System.Drawing.Size(362, 21);
            this.ucDateSelector2.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(12, 53);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(93, 18);
            this.label15.TabIndex = 3;
            this.label15.Text = "WO No.";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(12, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 16);
            this.label12.TabIndex = 4;
            this.label12.Text = "Item Code";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(12, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 16);
            this.label13.TabIndex = 5;
            this.label13.Text = "WO Date";
            // 
            // panelCenter
            // 
            this.panelCenter.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCenter.Controls.Add(this.printButton1);
            this.panelCenter.Controls.Add(this.previewButton1);
            this.panelCenter.Controls.Add(this.sbtnClose);
            this.panelCenter.Controls.Add(this.sbtnToggleOptions);
            this.panelCenter.Controls.Add(this.sbtnInquiry);
            this.panelCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCenter.Location = new System.Drawing.Point(0, 187);
            this.panelCenter.Name = "panelCenter";
            this.panelCenter.Size = new System.Drawing.Size(877, 33);
            this.panelCenter.TabIndex = 1;
            // 
            // printButton1
            // 
            this.printButton1.Location = new System.Drawing.Point(172, 5);
            this.printButton1.Name = "printButton1";
            this.printButton1.ReportType = "";
            this.printButton1.Size = new System.Drawing.Size(72, 23);
            this.printButton1.TabIndex = 0;
            this.printButton1.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // previewButton1
            // 
            this.previewButton1.Location = new System.Drawing.Point(94, 5);
            this.previewButton1.Name = "previewButton1";
            this.previewButton1.ReportType = "";
            this.previewButton1.Size = new System.Drawing.Size(72, 23);
            this.previewButton1.TabIndex = 1;
            this.previewButton1.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // sbtnClose
            // 
            this.sbtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnClose.Location = new System.Drawing.Point(328, 5);
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Size = new System.Drawing.Size(72, 23);
            this.sbtnClose.TabIndex = 2;
            this.sbtnClose.Text = "Close";
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // sbtnToggleOptions
            // 
            this.sbtnToggleOptions.Location = new System.Drawing.Point(250, 5);
            this.sbtnToggleOptions.Name = "sbtnToggleOptions";
            this.sbtnToggleOptions.Size = new System.Drawing.Size(72, 23);
            this.sbtnToggleOptions.TabIndex = 3;
            this.sbtnToggleOptions.Text = "Options";
            this.sbtnToggleOptions.Click += new System.EventHandler(this.sbtnToggleOptions_Click);
            // 
            // sbtnInquiry
            // 
            this.sbtnInquiry.Location = new System.Drawing.Point(16, 5);
            this.sbtnInquiry.Name = "sbtnInquiry";
            this.sbtnInquiry.Size = new System.Drawing.Size(72, 23);
            this.sbtnInquiry.TabIndex = 4;
            this.sbtnInquiry.Text = "Inquiry";
            this.sbtnInquiry.Click += new System.EventHandler(this.sbtnInquiry_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.xtraTabControl2);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 220);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(877, 404);
            this.panelControl2.TabIndex = 0;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
            this.xtraTabControl2.Size = new System.Drawing.Size(877, 404);
            this.xtraTabControl2.TabIndex = 0;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl1);
            this.xtraTabPage3.Controls.Add(this.ucSearchResult1);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(871, 376);
            this.xtraTabPage3.Text = "Result";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 46);
            this.gridControl1.MainView = this.gvMaster;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(871, 330);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMaster});
            // 
            // gvMaster
            // 
            this.gvMaster.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCheck,
            this.DtlKey,
            this.colWONo,
            this.colWODate,
            this.colItemWasteStartDate,
            this.colItemWasteEndDate,
            this.colDocNo,
            this.colDocDate,
            this.colDocType,
            this.colUOM,
            this.colItemCode,
            this.colDtlDescription,
            this.colBOMCode,
            this.colRate,
            this.colProdQty,
            this.colProdCost,
            this.colWOQty,
            this.colWOCost});
            this.gvMaster.GridControl = this.gridControl1;
            this.gvMaster.Name = "gvMaster";
            this.gvMaster.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gvMaster_SelectionChanged);
            this.gvMaster.DoubleClick += new System.EventHandler(this.gvMaster_DoubleClick);
            // 
            // colCheck
            // 
            this.colCheck.Caption = "Check";
            this.colCheck.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCheck.FieldName = "ToBeUpdate";
            this.colCheck.Name = "colCheck";
            this.colCheck.Visible = true;
            this.colCheck.VisibleIndex = 0;
            this.colCheck.Width = 74;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // DtlKey
            // 
            this.DtlKey.Caption = "DtlKey";
            this.DtlKey.FieldName = "DtlKey";
            this.DtlKey.Name = "DtlKey";
            this.DtlKey.OptionsColumn.AllowEdit = false;
            // 
            // colWONo
            // 
            this.colWONo.Caption = "WO No.";
            this.colWONo.FieldName = "WONo";
            this.colWONo.Name = "colWONo";
            this.colWONo.OptionsColumn.AllowEdit = false;
            this.colWONo.Visible = true;
            this.colWONo.VisibleIndex = 3;
            this.colWONo.Width = 63;
            // 
            // colWODate
            // 
            this.colWODate.Caption = "WO Date";
            this.colWODate.FieldName = "WODate";
            this.colWODate.Name = "colWODate";
            this.colWODate.OptionsColumn.AllowEdit = false;
            this.colWODate.Visible = true;
            this.colWODate.VisibleIndex = 4;
            this.colWODate.Width = 63;
            // 
            // colItemWasteStartDate
            // 
            this.colItemWasteStartDate.Caption = "Start Plan Date";
            this.colItemWasteStartDate.FieldName = "ProductionStartDate";
            this.colItemWasteStartDate.Name = "colItemWasteStartDate";
            this.colItemWasteStartDate.OptionsColumn.AllowEdit = false;
            this.colItemWasteStartDate.Width = 89;
            // 
            // colItemWasteEndDate
            // 
            this.colItemWasteEndDate.Caption = "End Plan Date";
            this.colItemWasteEndDate.FieldName = "ProductionEndDate";
            this.colItemWasteEndDate.Name = "colItemWasteEndDate";
            this.colItemWasteEndDate.OptionsColumn.AllowEdit = false;
            this.colItemWasteEndDate.Width = 78;
            // 
            // colDocNo
            // 
            this.colDocNo.Caption = "DocNo";
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.OptionsColumn.AllowEdit = false;
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 5;
            this.colDocNo.Width = 62;
            // 
            // colDocDate
            // 
            this.colDocDate.Caption = "DocDate";
            this.colDocDate.FieldName = "DocDate";
            this.colDocDate.Name = "colDocDate";
            this.colDocDate.OptionsColumn.AllowEdit = false;
            this.colDocDate.Visible = true;
            this.colDocDate.VisibleIndex = 6;
            this.colDocDate.Width = 87;
            // 
            // colDocType
            // 
            this.colDocType.Caption = "DocType";
            this.colDocType.FieldName = "DocType";
            this.colDocType.Name = "colDocType";
            this.colDocType.OptionsColumn.AllowEdit = false;
            this.colDocType.Width = 49;
            // 
            // colUOM
            // 
            this.colUOM.Caption = "UOM";
            this.colUOM.FieldName = "UOM";
            this.colUOM.Name = "colUOM";
            this.colUOM.OptionsColumn.AllowEdit = false;
            this.colUOM.Visible = true;
            this.colUOM.VisibleIndex = 8;
            this.colUOM.Width = 35;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item Code Waste/NG";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 1;
            this.colItemCode.Width = 133;
            // 
            // colDtlDescription
            // 
            this.colDtlDescription.Caption = "Item Description";
            this.colDtlDescription.FieldName = "Description";
            this.colDtlDescription.Name = "colDtlDescription";
            this.colDtlDescription.OptionsColumn.AllowEdit = false;
            this.colDtlDescription.Visible = true;
            this.colDtlDescription.VisibleIndex = 2;
            this.colDtlDescription.Width = 200;
            // 
            // colBOMCode
            // 
            this.colBOMCode.Caption = "BOM Code";
            this.colBOMCode.FieldName = "BOMCode";
            this.colBOMCode.Name = "colBOMCode";
            this.colBOMCode.OptionsColumn.AllowEdit = false;
            this.colBOMCode.Visible = true;
            this.colBOMCode.VisibleIndex = 7;
            this.colBOMCode.Width = 73;
            // 
            // colRate
            // 
            this.colRate.FieldName = "Rate";
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowEdit = false;
            this.colRate.Width = 29;
            // 
            // colProdQty
            // 
            this.colProdQty.Caption = "Prod. Qty";
            this.colProdQty.FieldName = "ProdQty";
            this.colProdQty.Name = "colProdQty";
            this.colProdQty.OptionsColumn.AllowEdit = false;
            this.colProdQty.Visible = true;
            this.colProdQty.VisibleIndex = 9;
            this.colProdQty.Width = 60;
            // 
            // colProdCost
            // 
            this.colProdCost.Caption = "Prod. Cost";
            this.colProdCost.FieldName = "ProdCost";
            this.colProdCost.Name = "colProdCost";
            this.colProdCost.OptionsColumn.AllowEdit = false;
            this.colProdCost.Width = 49;
            // 
            // colWOQty
            // 
            this.colWOQty.Caption = "WO Qty";
            this.colWOQty.FieldName = "WOQty";
            this.colWOQty.Name = "colWOQty";
            this.colWOQty.OptionsColumn.AllowEdit = false;
            this.colWOQty.Visible = true;
            this.colWOQty.VisibleIndex = 10;
            this.colWOQty.Width = 61;
            // 
            // colWOCost
            // 
            this.colWOCost.Caption = "WO Cost";
            this.colWOCost.FieldName = "WOCost";
            this.colWOCost.Name = "colWOCost";
            this.colWOCost.OptionsColumn.AllowEdit = false;
            this.colWOCost.Width = 70;
            // 
            // ucSearchResult1
            // 
            this.ucSearchResult1.Appearance.Options.UseBackColor = true;
            this.ucSearchResult1.ClearAllUncheckRecords = false;
            this.ucSearchResult1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucSearchResult1.KeepSearchResultVisible = false;
            this.ucSearchResult1.Location = new System.Drawing.Point(0, 0);
            this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ucSearchResult1.Name = "ucSearchResult1";
            this.ucSearchResult1.Size = new System.Drawing.Size(871, 46);
            this.ucSearchResult1.TabIndex = 1;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.memoEdit_Criteria);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(871, 376);
            this.xtraTabPage4.Text = "Criteria";
            // 
            // memoEdit_Criteria
            // 
            this.memoEdit_Criteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit_Criteria.Location = new System.Drawing.Point(0, 0);
            this.memoEdit_Criteria.Name = "memoEdit_Criteria";
            this.memoEdit_Criteria.Properties.Appearance.Options.UseFont = true;
            this.memoEdit_Criteria.Properties.ReadOnly = true;
            this.memoEdit_Criteria.Size = new System.Drawing.Size(871, 376);
            this.memoEdit_Criteria.TabIndex = 0;
            this.memoEdit_Criteria.UseOptimizedRendering = true;
            // 
            // repositoryItemTextEdit_Cancelled
            // 
            this.repositoryItemTextEdit_Cancelled.Name = "repositoryItemTextEdit_Cancelled";
            this.repositoryItemTextEdit_Cancelled.FormatEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repositoryItemTextEdit_Cancelled_FormatEditValue);
            // 
            // colLocation
            // 
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            // 
            // colFurtherDescription
            // 
            this.colFurtherDescription.FieldName = "FurtherDescription";
            this.colFurtherDescription.Name = "colFurtherDescription";
            // 
            // colProjNo
            // 
            this.colProjNo.FieldName = "ProjNo";
            this.colProjNo.Name = "colProjNo";
            // 
            // colProjDesc
            // 
            this.colProjDesc.FieldName = "ProjDesc";
            this.colProjDesc.Name = "colProjDesc";
            // 
            // colDeptNo
            // 
            this.colDeptNo.FieldName = "DeptNo";
            this.colDeptNo.Name = "colDeptNo";
            // 
            // colDeptDesc
            // 
            this.colDeptDesc.FieldName = "DeptDesc";
            this.colDeptDesc.Name = "colDeptDesc";
            // 
            // colExpCompletedDate
            // 
            this.colExpCompletedDate.FieldName = "ExpectedCompletedDate";
            this.colExpCompletedDate.Name = "colExpCompletedDate";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem2,
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 4;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 2";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Custom 2";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Report";
            this.barSubItem2.Id = 4;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Design Listing Report";
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDetailListingReport_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Switch to Advanced";
            this.barButtonItem2.Id = 6;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnAdvancedFilter_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(877, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 624);
            this.barDockControlBottom.Size = new System.Drawing.Size(877, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 602);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(877, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 602);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnDesignDetailListingReport
            // 
            this.barBtnDesignDetailListingReport.Id = 2;
            this.barBtnDesignDetailListingReport.Name = "barBtnDesignDetailListingReport";
            this.barBtnDesignDetailListingReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDetailListingReport_ItemClick);
            // 
            // barbtnAdvancedFilter
            // 
            this.barbtnAdvancedFilter.Id = 1;
            this.barbtnAdvancedFilter.Name = "barbtnAdvancedFilter";
            this.barbtnAdvancedFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnAdvancedFilter_ItemClick);
            // 
            // barBtnConvertToPlainText
            // 
            this.barBtnConvertToPlainText.Id = 3;
            this.barBtnConvertToPlainText.Name = "barBtnConvertToPlainText";
            this.barBtnConvertToPlainText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnConvertToPlainText_ItemClick);
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Item Waste / NG Listing";
            this.panelHeader1.HelpTopicId = "Stock_Work_Order.htm";
            this.panelHeader1.Location = new System.Drawing.Point(0, 22);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(877, 44);
            this.panelHeader1.TabIndex = 4;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(871, 200);
            this.xtraTabPage5.Text = "xtraTabPage5";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // FormItemWastePrintListing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnClose;
            this.ClientSize = new System.Drawing.Size(877, 624);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelCenter);
            this.Controls.Add(this.pnCriteriaBasic);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormItemWastePrintListing";
            this.Text = "Item Waste / NG Listing";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormStockAssemblyPrintDetailListing_Closing);
            this.Load += new System.EventHandler(this.FormSAOPrintDetailListing_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormStockAssemblyPrintDetailListing_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaBasic)).EndInit();
            this.pnCriteriaBasic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbBasic)).EndInit();
            this.gbBasic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelCenter)).EndInit();
            this.panelCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit_Cancelled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

    }

    public class FormEventArgs
    {
      private FormItemWastePrintListing myForm;

      public ItemWasteReportCommand Command
      {
        get
        {
          return this.myForm.myCommand;
        }
      }

      //public PanelControl PanelCriteria
      //{
      //  //get
      //  //{
      //  //  return this.myForm.panelCriteria;
      //  //}
      //}

      public PanelControl PanelButtons
      {
        get
        {
          return this.myForm.panelCenter;
        }
      }

      public XtraTabControl TabControl
      {
        get
        {
          return this.myForm.xtraTabControl2;
        }
      }

      public GridControl GridControl
      {
        get
        {
          return this.myForm.gridControl1;
        }
      }

      public FormItemWastePrintListing Form
      {
        get
        {
          return this.myForm;
        }
      }

      public DBSetting DBSetting
      {
        get
        {
          return this.myForm.myDBSetting;
        }
      }

      public FormEventArgs(FormItemWastePrintListing form)
      {
        this.myForm = form;
      }
    }

    public class FormInitializeEventArgs : FormItemWastePrintListing.FormEventArgs
    {
      public FormInitializeEventArgs(FormItemWastePrintListing form)
        : base(form)
      {
      }
    }

    public class FormInquiryEventArgs : FormItemWastePrintListing.FormEventArgs
    {
      private DataTable myResultTable;

      public DataTable ResultTable
      {
        get
        {
          return this.myResultTable;
        }
      }

      public FormInquiryEventArgs(FormItemWastePrintListing form, DataTable resultTable)
        : base(form)
      {
        this.myResultTable = resultTable;
      }
    }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }
    }
}
