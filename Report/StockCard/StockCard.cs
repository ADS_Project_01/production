﻿using BCE.AutoCount;
using BCE.AutoCount.Document;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Misc;
using DevExpress.XtraEditors;
using System;
using System.Data;
using BCE.AutoCount.Settings;
namespace Production.StockCard
{
    public class StockCard
    {
        protected DBSetting myDBSetting;
        protected DataSet myResultDataSet;
        protected DateEdit myFromDate;
        protected DateEdit myToDate;
        protected BCE.AutoCount.SearchFilter.Filter myLocationFilter;
        protected BCE.AutoCount.SearchFilter.Filter myItemFilter;
        protected BCE.AutoCount.SearchFilter.Filter myWONoFilter;
        protected BCE.AutoCount.SearchFilter.Filter myGroupFilter;
        protected BCE.AutoCount.SearchFilter.Filter myItemTypeFilter;
        protected bool IsMergeSameCost;
        protected bool IsIncludeZeroBalance;
        protected bool IsIncludeNonTransactionItem;
        protected bool IsPrintActive;
        protected bool IsPrintInactive;
        protected bool IsConsignmentEnable;
        protected ShowUOMOption myUOMOption;
        protected PrintBatchOption myBatchOption;
        protected int myCommandTimeOut;
        protected GeneralSetting myGeneralSetting;
        protected DecimalSetting myDecimalSetting;
        public const string MASTERTABLE = "Master";
        public const string DETAILTABLE = "Detail";
        public const string ITEMSNTABLE = "ItemSerialNo";
        public const string SUBDETAILTABLE = "SubDetail";
        protected const string MASTERMISSINGTABLE = "MasterMissing";
        protected const string UOMRATETABLE = "UOMRate";
        protected const string MISSINGCSGNTABLE = "MissingCSGN";
        private const string MASTERCOPYTABLE = "MasterCopy";

        public DataSet ResultDataSet
        {
            get
            {
                return this.myResultDataSet;
            }
        }

        protected StockCard()
        {
        }

        public static StockCard Create(DBSetting dbSetting)
        {
            StockCard stockCard = (StockCard)null;
            if (dbSetting.ServerType == DBServerType.SQL2000)
                stockCard = (StockCard)new StockCardSql();
            else
                dbSetting.ThrowServerTypeNotSupportedException();
            stockCard.myDBSetting = dbSetting;
            stockCard.myGeneralSetting = GeneralSetting.GetOrCreate(dbSetting);
            stockCard.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
            return stockCard;
        }

        public void Inquire(DateEdit fromDate, DateEdit toDate, BCE.AutoCount.SearchFilter.Filter locationFilter, BCE.AutoCount.SearchFilter.Filter itemFilter, BCE.AutoCount.SearchFilter.Filter wonoFilter, BCE.AutoCount.SearchFilter.Filter groupFilter, BCE.AutoCount.SearchFilter.Filter itemTypeFilter, bool isMergeSameCost, bool isPrintActive, bool isPrintInactive, ShowUOMOption uomOption, PrintBatchOption batchOption)
        {
            this.Inquire(fromDate, toDate, locationFilter, itemFilter,wonoFilter, groupFilter, itemTypeFilter, isMergeSameCost, isPrintActive, isPrintInactive, uomOption, batchOption, true, false);
        }

        public void Inquire(DateEdit fromDate, DateEdit toDate, BCE.AutoCount.SearchFilter.Filter locationFilter, BCE.AutoCount.SearchFilter.Filter itemFilter, BCE.AutoCount.SearchFilter.Filter wonoFilter, BCE.AutoCount.SearchFilter.Filter groupFilter, BCE.AutoCount.SearchFilter.Filter itemTypeFilter, bool isMergeSameCost, bool isPrintActive, bool isPrintInactive, ShowUOMOption uomOption, PrintBatchOption batchOption, bool isIncludeZeroBalance, bool isIncludeNonTransactionItem)
        {
            this.myFromDate = fromDate;
            this.myToDate = toDate;
            this.myLocationFilter = locationFilter;
            this.myItemFilter = itemFilter;
            this.myWONoFilter = wonoFilter;
            this.myGroupFilter = groupFilter;
            this.myItemTypeFilter = itemTypeFilter;
            this.IsMergeSameCost = isMergeSameCost;
            this.IsIncludeZeroBalance = isIncludeZeroBalance;
            this.IsIncludeNonTransactionItem = isIncludeNonTransactionItem;
            this.IsPrintActive = isPrintActive;
            this.IsPrintInactive = isPrintInactive;
            this.IsConsignmentEnable = false;// ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.Consignment.Enable;
            this.myUOMOption = uomOption;
            this.myBatchOption = batchOption;
            this.myCommandTimeOut = this.myGeneralSetting.SearchCommandTimeout;
            this.myResultDataSet = new DataSet();
            this.GetStockCards();
            this.CreateRelationship();
            this.FixDifferenceCase();
            if (!this.IsIncludeZeroBalance)
            {
                foreach (DataRow r in this.myResultDataSet.Tables["Master"].Select("BalanceQty=0"))
                {
                    this.DeleteChildTable(r);
                    r.Delete();
                }
                this.myResultDataSet.Tables["Master"].AcceptChanges();
            }
        }

        private void DeleteChildTable(DataRow r)
        {
            foreach (DataRelation dataRelation in (InternalDataCollectionBase)this.myResultDataSet.Tables[r.Table.TableName].ChildRelations)
            {
                foreach (DataRow r1 in r.GetChildRows(dataRelation.RelationName))
                {
                    if (this.myResultDataSet.Tables[dataRelation.ChildTable.TableName].ChildRelations.Count > 0)
                        this.DeleteChildTable(r1);
                    r1.Delete();
                }
                this.myResultDataSet.Tables[dataRelation.ChildTable.TableName].AcceptChanges();
            }
        }

        private void GetStockCards()
        {
            this.ProcessMaster();
            this.ProcessDetailAndSubdetail();
            this.CalculateMasterAverageCost();
            this.QueryItemSNTable();
        }

        private void AddAverageCost()
        {
            this.myResultDataSet.Tables["Master"].Columns.Add(new DataColumn()
            {
                DataType = typeof(Decimal),
                ColumnName = "AverageBFCost",
                Expression = "IIF([Balance]<>0, TotalCost/(IIF([Balance]<>0,Balance,1)),0)"
            });
            this.myResultDataSet.Tables["Master"].Columns.Add(new DataColumn()
            {
                DataType = typeof(Decimal),
                ColumnName = "AverageBalanceCost",
                Expression = "IIF([BalanceQty]<>0, BalanceCost/(IIF([BalanceQty]<>0,BalanceQty,1)),0)"
            });
            this.myResultDataSet.Tables["Detail"].Columns.Add(new DataColumn()
            {
                DataType = typeof(Decimal),
                ColumnName = "AverageBalanceCost",
                Expression = "IIF([Balance]<>0, BalanceCost/(IIF([Balance]<>0,Balance,1)),0)"
            });
        }

        private void CreateRelationship()
        {
            DataColumn[] dataColumnArray1 = new DataColumn[4];
            int index1 = 0;
            DataColumn dataColumn1 = this.myResultDataSet.Tables["Master"].Columns["ItemCode"];
            dataColumnArray1[index1] = dataColumn1;
            int index2 = 1;
            DataColumn dataColumn2 = this.myResultDataSet.Tables["Master"].Columns["Location"];
            dataColumnArray1[index2] = dataColumn2;
            //int index3 = 2;
            //DataColumn dataColumn3 = this.myResultDataSet.Tables["Master"].Columns["BatchNo"];
            //dataColumnArray1[index3] = dataColumn3;
            int index4 = 2;
            DataColumn dataColumn4 = this.myResultDataSet.Tables["Master"].Columns["UOM"];
            dataColumnArray1[index4] = dataColumn4;
            int index44 = 3;
            DataColumn dataColumn44 = this.myResultDataSet.Tables["Master"].Columns["WONo"];
            dataColumnArray1[index44] = dataColumn44;
            //int index55 = 5;
            //DataColumn dataColumn55 = this.myResultDataSet.Tables["Master"].Columns["ProductCode"];
            //dataColumnArray1[index55] = dataColumn55;
            DataColumn[] parentColumns1 = dataColumnArray1;
            DataColumn[] dataColumnArray2 = new DataColumn[4];
            int index5 = 0;
            DataColumn dataColumn5 = this.myResultDataSet.Tables["Detail"].Columns["ItemCode"];
            dataColumnArray2[index5] = dataColumn5;
            int index6 = 1;
            DataColumn dataColumn6 = this.myResultDataSet.Tables["Detail"].Columns["Location"];
            dataColumnArray2[index6] = dataColumn6;
            //int index7 = 2;
            //DataColumn dataColumn7 = this.myResultDataSet.Tables["Detail"].Columns["BatchNo"];
            //dataColumnArray2[index7] = dataColumn7;
            int index8 = 2;
            DataColumn dataColumn8 = this.myResultDataSet.Tables["Detail"].Columns["UOM"];
            dataColumnArray2[index8] = dataColumn8;
            int index88 = 3;
            DataColumn dataColumn88 = this.myResultDataSet.Tables["Detail"].Columns["WONo"];
            dataColumnArray2[index88] = dataColumn88;
            //int index99 = 5;
            //DataColumn dataColumn99 = this.myResultDataSet.Tables["Detail"].Columns["ProductCode"];
            //dataColumnArray2[index99] = dataColumn99;
            DataColumn[] childColumns1 = dataColumnArray2;
            this.myResultDataSet.Relations.Add("MasterDetailRelation", parentColumns1, childColumns1);
            DataColumn[] dataColumnArray3 = new DataColumn[4];
            int index9 = 0;
            DataColumn dataColumn9 = this.myResultDataSet.Tables["Master"].Columns["ItemCode"];
            dataColumnArray3[index9] = dataColumn9;
            int index10 = 1;
            DataColumn dataColumn10 = this.myResultDataSet.Tables["Master"].Columns["Location"];
            dataColumnArray3[index10] = dataColumn10;
            int index11 = 2;
            DataColumn dataColumn11 = this.myResultDataSet.Tables["Master"].Columns["UOM"];
            dataColumnArray3[index11] = dataColumn11;
            int index12 = 3;
            DataColumn dataColumn122 = this.myResultDataSet.Tables["Master"].Columns["WONo"];
            dataColumnArray3[index12] = dataColumn122;
            DataColumn[] parentColumns2 = dataColumnArray3;
            DataColumn[] dataColumnArray4 = new DataColumn[3];
            int index122 = 0;
            DataColumn dataColumn12 = this.myResultDataSet.Tables["ItemSerialNo"].Columns["ItemCode"];
            dataColumnArray4[index122] = dataColumn12;
            int index13 = 1;
            DataColumn dataColumn13 = this.myResultDataSet.Tables["ItemSerialNo"].Columns["BatchNo"];
            dataColumnArray4[index13] = dataColumn13;
            int index14 = 2;
            DataColumn dataColumn14 = this.myResultDataSet.Tables["ItemSerialNo"].Columns["Location"];
            dataColumnArray4[index14] = dataColumn14;
            DataColumn[] childColumns2 = dataColumnArray4;
          //  this.myResultDataSet.Relations.Add("MasterItemSNRelation", parentColumns2, childColumns2, false);
          //  this.myResultDataSet.Relations.Add("DetailSubdetailRelation", this.myResultDataSet.Tables["Detail"].Columns["StockDTLKey"], this.myResultDataSet.Tables["SubDetail"].Columns["StockDTLKey"]);
        }

        private void FixDifferenceCase()
        {
            for (int index1 = 0; index1 < this.myResultDataSet.Tables["Master"].Rows.Count; ++index1)
            {
                DataRow dataRow = this.myResultDataSet.Tables["Master"].Rows[index1];
                string strB1 = dataRow["ItemCode"].ToString();
                string strB2 = dataRow["Location"].ToString();
                string strB3 = dataRow["UOM"].ToString();
                string strB4 = dataRow["WONo"].ToString();
                DataRow[] childRows1 = dataRow.GetChildRows("MasterDetailRelation");
                for (int index2 = 0; index2 < childRows1.Length; ++index2)
                {
                    childRows1[index2].BeginEdit();
                    if (string.Compare(childRows1[index2]["ItemCode"].ToString(), strB1) != 0)
                        childRows1[index2]["ItemCode"] = (object)strB1;
                    if (string.Compare(childRows1[index2]["Location"].ToString(), strB2) != 0)
                        childRows1[index2]["Location"] = (object)strB2;
                    if (string.Compare(childRows1[index2]["UOM"].ToString(), strB3) != 0)
                        childRows1[index2]["UOM"] = (object)strB3;
                    if (string.Compare(childRows1[index2]["WONo"].ToString(), strB4) != 0)
                        childRows1[index2]["WONo"] = (object)strB4;
                    childRows1[index2].EndEdit();
                }
                DataRow[] childRows2 = dataRow.GetChildRows("MasterItemSNRelation");
                for (int index2 = 0; index2 < childRows2.Length; ++index2)
                {
                    childRows2[index2].BeginEdit();
                    if (string.Compare(childRows2[index2]["ItemCode"].ToString(), strB1) != 0)
                        childRows2[index2]["ItemCode"] = (object)strB1;
                    //if (string.Compare(childRows2[index2]["BatchNo"].ToString(), strB4) != 0)
                    //    childRows2[index2]["BatchNo"] = (object)strB4;
                    if (string.Compare(childRows2[index2]["Location"].ToString(), strB3) != 0)
                        childRows2[index2]["Location"] = (object)strB3;
                    childRows2[index2].EndEdit();
                }
            }
        }

        private DataColumn AddColumn(string columnName, string columnType, bool allowDBNull, string tableName)
        {
            return this.AddColumn(columnName, columnType, allowDBNull, (object)null, tableName);
        }

        private DataColumn AddColumn(string columnName, string columnType, bool allowDBNull, object defaultValue, string tableName)
        {
            DataColumn dataColumn = new DataColumn();
            dataColumn.DataType = Type.GetType(columnType);
            dataColumn.AllowDBNull = allowDBNull;
            if (defaultValue != null)
                dataColumn.DefaultValue = defaultValue;
            dataColumn.Caption = columnName;
            dataColumn.ColumnName = columnName;
            return dataColumn;
        }

        private void ProcessMaster()
        {
            this.QueryMasterTable();
            this.QueryMissingMasterTable();
            //this.QueryMissingCSGN();
            this.AddMasterColumn();
        }

        private void PostMasterProcess()
        {
            this.MasterTableIntegrity();
            if (this.myUOMOption != ShowUOMOption.ShowMultiUOM)
                this.NotMultiUOMMasterTableIntegrity();
            if (this.IsConsignmentEnable)
                this.MasterTableCSGNIntegrity();
            DataTable dataTable1 = this.myResultDataSet.Tables["MasterMissing"];
            this.myResultDataSet.Tables.Remove("MasterMissing");
            if (this.IsConsignmentEnable)
            {
                DataTable dataTable2 = this.myResultDataSet.Tables["MissingCSGN"];
                this.myResultDataSet.Tables.Remove("MissingCSGN");
            }
        }

        private void AddMasterColumn()
        {
            DataColumn[] columns = new DataColumn[3];
            int num1 = 0;
            DataColumn[] dataColumnArray1 = columns;
            int index1 = num1;
            int num2 = 1;
            int num3 = index1 + num2;
            DataColumn dataColumn1 = this.AddColumn("AverageBFCost", "System.Decimal", true, (object)new Decimal(0, 0, 0, false, (byte)1), "Master");
            dataColumnArray1[index1] = dataColumn1;
            DataColumn[] dataColumnArray2 = columns;
            int index2 = num3;
            int num4 = 1;
            int num5 = index2 + num4;
            DataColumn dataColumn2 = this.AddColumn("AverageBalanceCost", "System.Decimal", true, (object)new Decimal(0, 0, 0, false, (byte)1), "Master");
            dataColumnArray2[index2] = dataColumn2;
            this.myResultDataSet.Tables["Master"].Columns.AddRange(columns);
        }

        private void CalculateMasterAverageCost()
        {
            for (int index = 0; index < this.myResultDataSet.Tables["Master"].Rows.Count; ++index)
            {
                Decimal num1 = Decimal.Round(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["Balance"]), 15);
                Decimal num2 = Decimal.Round(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["BalanceQty"]), 15);
                if (num1 != Decimal.Zero)
                    this.myResultDataSet.Tables["Master"].Rows[index]["AverageBFCost"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["TotalCost"]) / num1);
                if (num2 != Decimal.Zero)
                    this.myResultDataSet.Tables["Master"].Rows[index]["AverageBalanceCost"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["BalanceCost"]) / num2);
                if (this.IsConsignmentEnable)
                {
                    this.myResultDataSet.Tables["Master"].Rows[index]["BalQtyAfterCSGN"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["BalanceQty"]) - BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["CSGNBalQty"]));
                    this.myResultDataSet.Tables["Master"].Rows[index]["CSGNCost"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["AverageBalanceCost"]) * BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["CSGNBalQty"]));
                    this.myResultDataSet.Tables["Master"].Rows[index]["BalCostAfterCSGN"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["BalanceCost"]) - BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index]["CSGNCost"]));
                }
            }
        }

        private void MasterTableIntegrity()
        {
            for (int index1 = 0; index1 < this.myResultDataSet.Tables["MasterMissing"].Rows.Count; ++index1)
            {
                object obj1 = this.myResultDataSet.Tables["MasterMissing"].Rows[index1]["ItemCode"];
                object obj2 = this.myResultDataSet.Tables["MasterMissing"].Rows[index1]["Location"];
                //object obj3 = this.myResultDataSet.Tables["MasterMissing"].Rows[index1]["BatchNo"];
                object obj4 = this.myResultDataSet.Tables["MasterMissing"].Rows[index1]["UOM"];
                object obj5 = this.myResultDataSet.Tables["MasterMissing"].Rows[index1]["WONo"];
               // object obj6 = this.myResultDataSet.Tables["MasterMissing"].Rows[index1]["ProductCode"];
                if (!this.myResultDataSet.Tables["Master"].Rows.Contains(new object[4]
                {
          obj1,
          obj2,
        //  obj3,
          obj4,
          obj5
        //  obj6
                }))
                {
                    DataTable dataTable = this.myResultDataSet.Tables["Detail"];
                    string[] strArray = new string[8];
                    int index2 = 0;
                    string str1 = "ItemCode = '";
                    strArray[index2] = str1;
                    int index3 = 1;
                    string str2 = StringHelper.ToSingleQuoteString(obj1.ToString());
                    strArray[index3] = str2;
                    int index4 = 2;
                    string str3 = "' AND Location = '";
                    strArray[index4] = str3;
                    int index5 = 3;
                    string str4 = StringHelper.ToSingleQuoteString(obj2.ToString());
                    strArray[index5] = str4;
                    //int index6 = 4;
                    //string str5 = "' AND BatchNo = '";
                    //strArray[index6] = str5;
                    //int index7 = 5;
                    //string str6 = StringHelper.ToSingleQuoteString(obj3.ToString());
                    //strArray[index7] = str6;
                    int index8 = 4;
                    string str7 = "' AND UOM = '";
                    strArray[index8] = str7;
                    int index9 = 5;
                    string str8 = StringHelper.ToSingleQuoteString(obj4.ToString());
                    strArray[index9] = str8;
                    int index10 = 6;
                    string str9 = "'  AND WONo ='";
                    strArray[index10] = str9;
                    int index11 = 7;
                    string str11 = StringHelper.ToSingleQuoteString(obj5.ToString());
                    strArray[index11] = str11+"'";
                    string filterExpression = string.Concat(strArray);
                    if (dataTable.Select(filterExpression).Length != 0)
                        this.myResultDataSet.Tables["Master"].ImportRow(this.myResultDataSet.Tables["MasterMissing"].Rows[index1]);
                }
            }
        }

        private void MasterTableCSGNIntegrity()
        {
            for (int index1 = 0; index1 < this.myResultDataSet.Tables["MissingCSGN"].Rows.Count; ++index1)
            {
                object obj1 = this.myResultDataSet.Tables["MissingCSGN"].Rows[index1]["ItemCode"];
                object obj2 = this.myResultDataSet.Tables["MissingCSGN"].Rows[index1]["Location"];
                object obj3 = this.myResultDataSet.Tables["MissingCSGN"].Rows[index1]["UOM"];
                object obj4 = this.myResultDataSet.Tables["MissingCSGN"].Rows[index1]["WONo"];
                DataRowCollection rows = this.myResultDataSet.Tables["Master"].Rows;
                object[] keys = new object[4];
                int index2 = 0;
                object obj5 = obj1;
                keys[index2] = obj5;
                int index3 = 1;
                object obj6 = obj2;
                keys[index3] = obj6;
                int index4 = 2;
                object obj7 = obj3;
                keys[index4] = obj7;
                int index5 = 3;
                object obj8 = obj4;
                keys[index5] = obj8;
                if (rows.Find(keys) == null)
                    this.myResultDataSet.Tables["Master"].ImportRow(this.myResultDataSet.Tables["MissingCSGN"].Rows[index1]);
            }
        }

        private void NotMultiUOMMasterTableIntegrity()
        {
            DataColumn[] dataColumnArray1 = new DataColumn[4];
            int index1 = 0;
            DataColumn dataColumn1 = this.myResultDataSet.Tables["Master"].Columns["ItemCode"];
            dataColumnArray1[index1] = dataColumn1;
            int index2 = 1;
            DataColumn dataColumn2 = this.myResultDataSet.Tables["Master"].Columns["Location"];
            dataColumnArray1[index2] = dataColumn2;
            int index3 = 2;
            DataColumn dataColumn3 = this.myResultDataSet.Tables["Master"].Columns["UOM"];
            dataColumnArray1[index3] = dataColumn3;
          //  this.myResultDataSet.Tables["Master"].PrimaryKey = dataColumnArray1;
            int index4a = 3;
            DataColumn dataColumn4 = this.myResultDataSet.Tables["Master"].Columns["WONo"];
            dataColumnArray1[index4a] = dataColumn4;
            this.myResultDataSet.Tables["Master"].PrimaryKey = dataColumnArray1;
            string aStr1 = string.Empty;
            string aStr2 = string.Empty;
            string aStr3 = string.Empty;
            string aStr4 = string.Empty;
            for (int index4 = 0; index4 < this.myResultDataSet.Tables["Detail"].Rows.Count; ++index4)
            {
                if (string.Compare(aStr1.TrimEnd(new char[0]), this.myResultDataSet.Tables["Detail"].Rows[index4]["ItemCode"].ToString().TrimEnd(new char[0]), true) != 0 || string.Compare(aStr2.TrimEnd(new char[0]), this.myResultDataSet.Tables["Detail"].Rows[index4]["Location"].ToString().TrimEnd(new char[0]), true) != 0 || string.Compare(aStr3.TrimEnd(new char[0]), this.myResultDataSet.Tables["Detail"].Rows[index4]["UOM"].ToString().TrimEnd(new char[0]), true) != 0 || string.Compare(aStr4.TrimEnd(new char[0]), this.myResultDataSet.Tables["Detail"].Rows[index4]["WONo"].ToString().TrimEnd(new char[0]), true) != 0)
                {
                    aStr1 = this.myResultDataSet.Tables["Detail"].Rows[index4]["ItemCode"].ToString();
                    aStr2 = this.myResultDataSet.Tables["Detail"].Rows[index4]["Location"].ToString();
                    aStr3 = this.myResultDataSet.Tables["Detail"].Rows[index4]["UOM"].ToString();
                    aStr4 = this.myResultDataSet.Tables["Detail"].Rows[index4]["WONo"].ToString();
                    if (!this.myResultDataSet.Tables["Master"].Rows.Contains(new object[4]
                    {
            (object) aStr2,
            (object) aStr1,
            (object) aStr3,
              (object) aStr4
                    }))
                    {
                        DataTable dataTable = this.myResultDataSet.Tables["MasterMissing"];
                        string[] strArray = new string[8];
                        int index5 = 0;
                        string str1 = "ItemCode = '";
                        strArray[index5] = str1;
                        int index6 = 1;
                        string str2 = StringHelper.ToSingleQuoteString(aStr2);
                        strArray[index6] = str2;
                        int index7 = 2;
                        string str3 = "' AND Location = '";
                        strArray[index7] = str3;
                        int index8 = 3;
                        string str4 = StringHelper.ToSingleQuoteString(aStr1);
                        strArray[index8] = str4;
                        int index9 = 4;
                        string str5 = "' AND UOM = '";
                        strArray[index9] = str5;
                        int index10 = 5;
                        string str6 = StringHelper.ToSingleQuoteString(aStr3);
                        strArray[index10] = str6;

                        int index11 = 6;
                        string str7 = "' AND WONo= '";
                        strArray[index11] = str7;
                        int index133= 7;
                        string str8 = StringHelper.ToSingleQuoteString(aStr4);
                        strArray[index133] = str8 + "'";
                        string filterExpression = string.Concat(strArray);
                        DataRow[] dataRowArray = dataTable.Select(filterExpression);
                        DataRow row = this.myResultDataSet.Tables["Master"].NewRow();
                        row.BeginEdit();
                        row["ItemCode"] = (object)aStr1;
                        row["Location"] = (object)aStr2;
                        row["BatchNo"] = (object)aStr3;
                        row["ExpiryDate"] = dataRowArray[0]["ExpiryDate"];
                        row["ItemBatchDescription"] = dataRowArray[0]["ItemBatchDescription"];
                        row["UOM"] = (object)this.myResultDataSet.Tables["Detail"].Rows[index4]["UOM"].ToString();
                        row["Balance"] = (object)0.0;
                        row["TotalCost"] = (object)0.0;
                        row["ItemGroup"] = dataRowArray[0]["ItemGroup"];
                        row["ItemType"] = dataRowArray[0]["ItemType"];
                        row["Description"] = dataRowArray[0]["Description"];
                        row["Desc2"] = dataRowArray[0]["Desc2"];
                        row["CostingMethod"] = dataRowArray[0]["CostingMethod"];
                        row["CostingMethodString"] = dataRowArray[0]["CostingMethodString"];
                        if (this.IsConsignmentEnable)
                        {
                            row["CSGNBalQty"] = dataRowArray[0]["CSGNBalQty"];
                            row["BalQtyAfterCSGN"] = dataRowArray[0]["BalQtyAfterCSGN"];
                            row["CSGNCost"] = dataRowArray[0]["CSGNCost"];
                            row["BalCostAfterCSGN"] = dataRowArray[0]["BalCostAfterCSGN"];
                        }
                        row.EndEdit();
                        this.myResultDataSet.Tables["Master"].Rows.Add(row);
                    }
                }
            }
            DataColumn[] dataColumnArray2 = new DataColumn[4];
            int index12 = 0;
            DataColumn dataColumn44 = this.myResultDataSet.Tables["Master"].Columns["ItemCode"];
            dataColumnArray2[index12] = dataColumn44;
            int index13 = 1;
            DataColumn dataColumn5 = this.myResultDataSet.Tables["Master"].Columns["Location"];
            dataColumnArray2[index13] = dataColumn5;
            int index14 = 2;
            DataColumn dataColumn6 = this.myResultDataSet.Tables["Master"].Columns["UOM"];
            dataColumnArray2[index14] = dataColumn6;
            int index15 = 3;
            DataColumn dataColumn7 = this.myResultDataSet.Tables["Master"].Columns["WONo"];
            dataColumnArray2[index15] = dataColumn7;
            this.myResultDataSet.Tables["Master"].PrimaryKey = dataColumnArray2;
        }

        private void ProcessDetailAndSubdetail()
        {
            this.QueryDetailTable();
            this.AddDetailColumns();
            this.CreateSubdetailTable();
            if (this.myUOMOption != ShowUOMOption.ShowMultiUOM)
            {
                if (this.IsIncludeNonTransactionItem)
                    this.QueryUOMNonTransactionOptions();
                else
                    this.QueryUOMOptions();
                this.CalculateMasterUOMOption();
                this.CalculateUOMOptions();
            }
            this.CalculateStockCards();
            this.CreateDetailTablePrimaryKey();
            this.FillDocumentInfo();
        }

        private void AddDetailColumns()
        {
            DataColumn[] columns = new DataColumn[10];
            int num1 = 0;
            DataColumn[] dataColumnArray1 = columns;
            int index1 = num1;
            int num2 = 1;
            int num3 = index1 + num2;
            DataColumn dataColumn1 = this.AddColumn("DocNo", "System.String", true, "Detail");
            dataColumnArray1[index1] = dataColumn1;
            DataColumn[] dataColumnArray2 = columns;
            int index2 = num3;
            int num4 = 1;
            int num5 = index2 + num4;
            DataColumn dataColumn2 = this.AddColumn("InQty", "System.Decimal", true, "Detail");
            dataColumnArray2[index2] = dataColumn2;
            DataColumn[] dataColumnArray3 = columns;
            int index3 = num5;
            int num6 = 1;
            int num7 = index3 + num6;
            DataColumn dataColumn3 = this.AddColumn("OutQty", "System.Decimal", true, "Detail");
            dataColumnArray3[index3] = dataColumn3;
            DataColumn[] dataColumnArray4 = columns;
            int index4 = num7;
            int num8 = 1;
            int num9 = index4 + num8;
            DataColumn dataColumn4 = this.AddColumn("Balance", "System.Decimal", true, "Detail");
            dataColumnArray4[index4] = dataColumn4;
            DataColumn[] dataColumnArray5 = columns;
            int index5 = num9;
            int num10 = 1;
            int num11 = index5 + num10;
            DataColumn dataColumn5 = this.AddColumn("BalanceString", "System.String", true, "Detail");
            dataColumnArray5[index5] = dataColumn5;
            DataColumn[] dataColumnArray6 = columns;
            int index6 = num11;
            int num12 = 1;
            int num13 = index6 + num12;
            DataColumn dataColumn6 = this.AddColumn("BalanceCost", "System.Decimal", true, "Detail");
            dataColumnArray6[index6] = dataColumn6;
            DataColumn[] dataColumnArray7 = columns;
            int index7 = num13;
            int num14 = 1;
            int num15 = index7 + num14;
            DataColumn dataColumn7 = this.AddColumn("AverageBalanceCost", "System.Decimal", true, (object)new Decimal(0, 0, 0, false, (byte)1), "Detail");
            dataColumnArray7[index7] = dataColumn7;
            DataColumn[] dataColumnArray8 = columns;
            int index8 = num15;
            int num16 = 1;
            int num17 = index8 + num16;
            DataColumn dataColumn8 = this.AddColumn("Description", "System.String", true, "Detail");
            dataColumnArray8[index8] = dataColumn8;
            DataColumn[] dataColumnArray9 = columns;
            int index9 = num17;
            int num18 = 1;
            int num19 = index9 + num18;
            DataColumn dataColumn9 = this.AddColumn("RefDocNo", "System.String", true, "Detail");
            dataColumnArray9[index9] = dataColumn9;
            DataColumn[] dataColumnArray10 = columns;
            int index10 = num19;
            int num20 = 1;
            int num21 = index10 + num20;
            DataColumn dataColumn10 = this.AddColumn("SerialNoList", "System.String", true, "Detail");
            dataColumnArray10[index10] = dataColumn10;
            this.myResultDataSet.Tables["Detail"].Columns.AddRange(columns);
        }

        private void CreateSubdetailTable()
        {
            this.myResultDataSet.Tables["Detail"].DefaultView.Sort = "DocKey";
            DataTable table = this.myResultDataSet.Tables["Detail"].Clone();
            table.TableName = "SubDetail";
            this.myResultDataSet.Tables.Add(table);
        }

        private void CalculateStockCards()
        {
            this.PostMasterProcess();
            string str1 = string.Empty;
            string str2 = string.Empty;
            string str3 = string.Empty;
            string str4 = string.Empty;
            string str5 = string.Empty;
            string str6 = string.Empty;
            Decimal num1 = new Decimal();
            Decimal num2 = new Decimal();
            Decimal num10 = new Decimal();
            num10 = 0;
            DataRow dataRow = (DataRow)null;
            DataRow[] drDetail2 = this.myResultDataSet.Tables["Detail"].Select("", "WONo,ItemCode,DocDate Asc", DataViewRowState.CurrentRows);
            for (int index = 0; index < drDetail2.Length; ++index)
            {
                DataRow drDetail = drDetail2[index];
                if (index > 0)
                {
                    if ((drDetail["WONo"].ToString() != drDetail2[index - 1]["WONo"].ToString()) || (drDetail["ItemCode"].ToString() != drDetail2[index - 1]["ItemCode"].ToString()))
                    {
                        num1 = 0;
                        num2 = 0;
                        num10 = 0;
                    }
                }
                if (string.Compare(str1.TrimEnd(new char[0]), drDetail["ItemCode"].ToString().TrimEnd(new char[0]), true) != 0 || string.Compare(str2.TrimEnd(new char[0]), drDetail["Location"].ToString().TrimEnd(new char[0]), true) != 0 || (string.Compare(str3.TrimEnd(new char[0]), drDetail["UOM"].ToString().TrimEnd(new char[0]), true) != 0 || string.Compare(str5.TrimEnd(new char[0]), drDetail["WONo"].ToString().TrimEnd(new char[0]), true) != 0))
                {
                    if (index != 0)
                    {
                        if (dataRow != null)
                        {
                            if (dataRow["WONo"].ToString() == drDetail["WONo"].ToString() && dataRow["ItemCode"].ToString() == drDetail["ItemCode"].ToString())
                            {
                                dataRow.BeginEdit();
                                dataRow["BalanceQty"] = (object)num1;
                                dataRow["BalanceCost"] = (object)num2;
                                dataRow.EndEdit();
                            }
                        }
                    }
                    str1 = drDetail["ItemCode"].ToString();
                    str2 = drDetail["Location"].ToString();
                    str3 = drDetail["UOM"].ToString();
                  //  str4 = drDetail["BatchNo"].ToString();
                    str5 = drDetail["WONo"].ToString();
                   // str6 = drDetail["ProductCode"].ToString();
                    dataRow = this.myResultDataSet.Tables["Master"].Rows.Find(new object[4]
                    {
            (object) str1,
            (object) str2,
            (object) str3,
            (object) str5
                    });
                    if (dataRow != null)
                    {
                        if (dataRow["WONo"].ToString() == drDetail["WONo"].ToString() && dataRow["ItemCode"].ToString() == drDetail["ItemCode"].ToString())
                        {
                            num1 = BCE.Data.Convert.ToDecimal(dataRow["Balance"]);
                            num2 = BCE.Data.Convert.ToDecimal(dataRow["TotalCost"]);
                        }
                    }
                    else
                    {
                        num1 = new Decimal();
                        num2 = new Decimal();
                    }
                }
                if (drDetail["FIFOSeq"] == DBNull.Value || BCE.Data.Convert.ToInt32(drDetail["FIFOSeq"]) == 1)
                {
                    Decimal num3 = BCE.Data.Convert.ToDecimal(drDetail["Qty"]);
                    Decimal num4 = BCE.Data.Convert.ToDecimal(drDetail["TotalCost"]);
                    Decimal num5 = BCE.Data.Convert.ToDecimal(drDetail["AdjustedCost"]);
                    num1 += num3;
                    num2 += num4 - num5;
                    num10 +=num3;
                    drDetail.BeginEdit();
                    drDetail["Balance"] = (object)num1;
                    drDetail["BalanceCost"] = (object)num2;
                    if (num1 != Decimal.Zero)
                        drDetail["AverageBalanceCost"] = (object)(num2 / num1);
                    if (drDetail["FIFOSeq"] != DBNull.Value)
                    {
                        drDetail["Qty"] = drDetail["FIFOQty"];
                        drDetail["Cost"] = drDetail["FIFOCost"];
                    }
                    drDetail["OutQty"] = (num3 >= Decimal.Zero) ? (object)0 : (object)num3;
                    drDetail["InQty"] = (num3 >= Decimal.Zero) ? (object)num3 : (object)0;

                    drDetail.EndEdit();
                }
                else
                {
                    this.myResultDataSet.Tables["SubDetail"].ImportRow(drDetail);
                    drDetail.Delete();
                }
                //if (index == this.myResultDataSet.Tables["Detail"].Rows.Count - 1)
                {
                    if (dataRow != null)
                    {
                        if (dataRow["WONo"].ToString() == drDetail["WONo"].ToString() && dataRow["ItemCode"].ToString() == drDetail["ItemCode"].ToString())
                        {
                            dataRow.BeginEdit();
                            dataRow["BalanceQty"] = (object)num1;
                            dataRow["BalanceCost"] = (object)num2;
                            dataRow.EndEdit();
                        }
                    }
                }
            }
            this.myResultDataSet.Tables["Detail"].AcceptChanges();
        }

        protected void CreateDetailTablePrimaryKey()
        {
            DataColumn[] dataColumnArray = new DataColumn[1];
            int index = 0;
            DataColumn dataColumn = this.myResultDataSet.Tables["Detail"].Columns["StockDTLKey"];
            dataColumnArray[index] = dataColumn;
            this.myResultDataSet.Tables["Detail"].PrimaryKey = dataColumnArray;
        }

        private void FillDocumentInfo()
        {
            Document.CreateDocument(this.myDBSetting).FillStockCardDocumentInfo(this.myResultDataSet.Tables["Detail"]);
        }

        private string GetBalanceString(DataTable myTable)
        {
            string str1 = string.Empty;
            for (int index1 = 0; index1 < myTable.DefaultView.Count; ++index1)
            {
                if (index1 == 0)
                {
                    str1 = this.myDecimalSetting.RoundQuantity(myTable.DefaultView[index1]["Qty"]).ToString() + myTable.DefaultView[index1]["UOM"];
                }
                else
                {
                    object[] objArray = new object[4];
                    int index2 = 0;
                    string str2 = str1;
                    objArray[index2] = (object)str2;
                    int index3 = 1;
                    string str3 = ", ";
                    objArray[index3] = (object)str3;
                    int index4 = 2;
                    // ISSUE: variable of a boxed type
                    Decimal local = this.myDecimalSetting.RoundQuantity(myTable.DefaultView[index1]["Qty"]);
                    objArray[index4] = (object)local;
                    int index5 = 3;
                    object obj = myTable.DefaultView[index1]["UOM"];
                    objArray[index5] = obj;
                    str1 = string.Concat(objArray);
                }
            }
            return str1;
        }

        private void CalculateUOMOptions()
        {
            string aStr1 = string.Empty;
            string aStr2 = string.Empty;
            string aStr3 = string.Empty;
            string str1 = string.Empty;
            Decimal num1 = new Decimal();
            DataTable balanceStringTable = this.CreateBalanceStringTable();
            for (int index1 = 0; index1 < this.myResultDataSet.Tables["Detail"].Rows.Count; ++index1)
            {
                if (string.Compare(aStr1.TrimEnd(new char[0]), this.myResultDataSet.Tables["Detail"].Rows[index1]["ItemCode"].ToString().TrimEnd(new char[0]), true) != 0 || string.Compare(aStr2.TrimEnd(new char[0]), this.myResultDataSet.Tables["Detail"].Rows[index1]["Location"].ToString().TrimEnd(new char[0]), true) != 0 || string.Compare(aStr3.TrimEnd(new char[0]), this.myResultDataSet.Tables["Detail"].Rows[index1]["BatchNo"].ToString().TrimEnd(new char[0]), true) != 0)
                {
                    balanceStringTable.Clear();
                    aStr1 = this.myResultDataSet.Tables["Detail"].Rows[index1]["ItemCode"].ToString();
                    aStr2 = this.myResultDataSet.Tables["Detail"].Rows[index1]["Location"].ToString();
                    aStr3 = this.myResultDataSet.Tables["Detail"].Rows[index1]["BatchNo"].ToString();
                    DataRow dataRow1 = this.myResultDataSet.Tables["UOMRate"].Rows.Find(new object[3]
                    {
            (object) aStr2,
            (object) aStr1,
            (object) aStr3
                    });
                    string index2 = "UOM";
                    str1 = dataRow1[index2].ToString();
                    string index3 = "Rate";
                    num1 = BCE.Data.Convert.ToDecimal(dataRow1[index3]);
                    DataTable dataTable = this.myResultDataSet.Tables["MasterCopy"];
                    string[] strArray = new string[7];
                    int index4 = 0;
                    string str2 = "Location = '";
                    strArray[index4] = str2;
                    int index5 = 1;
                    string str3 = StringHelper.ToSingleQuoteString(aStr2);
                    strArray[index5] = str3;
                    int index6 = 2;
                    string str4 = "' AND ItemCode = '";
                    strArray[index6] = str4;
                    int index7 = 3;
                    string str5 = StringHelper.ToSingleQuoteString(aStr1);
                    strArray[index7] = str5;
                    int index8 = 4;
                    string str6 = "' AND BatchNo = '";
                    strArray[index8] = str6;
                    int index9 = 5;
                    string str7 = StringHelper.ToSingleQuoteString(aStr3);
                    strArray[index9] = str7;
                    int index10 = 6;
                    string str8 = "'";
                    strArray[index10] = str8;
                    string filterExpression = string.Concat(strArray);
                    DataRow[] dataRowArray = dataTable.Select(filterExpression);
                    for (int index11 = 0; index11 < dataRowArray.Length; ++index11)
                    {
                        DataRow dataRow2 = balanceStringTable.Rows.Find(dataRowArray[index11]["UOM"]);
                        if (dataRow2 == null)
                        {
                            DataRow row = balanceStringTable.NewRow();
                            row.BeginEdit();
                            row["UOM"] = dataRowArray[index11]["UOM"];
                            row["Qty"] = dataRowArray[index11]["Balance"];
                            row["Rate"] = dataRowArray[index11]["Rate"];
                            row.EndEdit();
                            balanceStringTable.Rows.Add(row);
                        }
                        else
                        {
                            Decimal num2 = BCE.Data.Convert.ToDecimal(dataRow2["Qty"]) + BCE.Data.Convert.ToDecimal(dataRowArray[index11]["Balance"]);
                            dataRow2.BeginEdit();
                            dataRow2["Qty"] = (object)num2;
                            dataRow2.EndEdit();
                        }
                    }
                }
                if (this.myResultDataSet.Tables["Detail"].Rows[index1]["FIFOSeq"] == DBNull.Value || BCE.Data.Convert.ToInt32(this.myResultDataSet.Tables["Detail"].Rows[index1]["FIFOSeq"]) == 1)
                {
                    DataRow dataRow = balanceStringTable.Rows.Find(this.myResultDataSet.Tables["Detail"].Rows[index1]["UOM"]);
                    if (dataRow == null)
                    {
                        DataRow row = balanceStringTable.NewRow();
                        row.BeginEdit();
                        row["UOM"] = this.myResultDataSet.Tables["Detail"].Rows[index1]["UOM"];
                        row["Qty"] = this.myResultDataSet.Tables["Detail"].Rows[index1]["Qty"];
                        row["Rate"] = this.myResultDataSet.Tables["Detail"].Rows[index1]["Rate"];
                        row.EndEdit();
                        balanceStringTable.Rows.Add(row);
                    }
                    else
                    {
                        Decimal num2 = BCE.Data.Convert.ToDecimal(dataRow["Qty"]) + BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Detail"].Rows[index1]["Qty"]);
                        dataRow.BeginEdit();
                        dataRow["Qty"] = (object)num2;
                        dataRow.EndEdit();
                    }
                }
                this.myResultDataSet.Tables["Detail"].Rows[index1].BeginEdit();
                this.myResultDataSet.Tables["Detail"].Rows[index1]["BalanceString"] = (object)this.GetBalanceString(balanceStringTable);
                if (string.Compare(this.myResultDataSet.Tables["Detail"].Rows[index1]["UOM"].ToString().TrimEnd(new char[0]), str1.TrimEnd(new char[0]), true) != 0)
                {
                    this.myResultDataSet.Tables["Detail"].Rows[index1]["UOM"] = (object)str1;
                    this.myResultDataSet.Tables["Detail"].Rows[index1]["Qty"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Detail"].Rows[index1]["Qty"]) * BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Detail"].Rows[index1]["Rate"]) / num1);
                    this.myResultDataSet.Tables["Detail"].Rows[index1]["Cost"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Detail"].Rows[index1]["Cost"]) / BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Detail"].Rows[index1]["Rate"]) / num1);
                    if (this.myResultDataSet.Tables["Detail"].Rows[index1]["FIFOQty"] != DBNull.Value)
                        this.myResultDataSet.Tables["Detail"].Rows[index1]["FIFOQty"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Detail"].Rows[index1]["FIFOQty"]) * BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Detail"].Rows[index1]["Rate"]) / num1);
                    if (this.myResultDataSet.Tables["Detail"].Rows[index1]["FIFOCost"] != DBNull.Value)
                        this.myResultDataSet.Tables["Detail"].Rows[index1]["FIFOCost"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Detail"].Rows[index1]["FIFOCost"]) / BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Detail"].Rows[index1]["Rate"]) / num1);
                }
                this.myResultDataSet.Tables["Detail"].Rows[index1].EndEdit();
            }
            DataTable dataTable1 = this.myResultDataSet.Tables["MasterCopy"];
            this.myResultDataSet.Tables.Remove("MasterCopy");
        }

        private DataTable CreateBalanceStringTable()
        {
            DataTable dataTable = new DataTable("BalanceStringTable");
            dataTable.Columns.Add(new DataColumn("UOM", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Qty", typeof(Decimal)));
            dataTable.Columns.Add(new DataColumn("Rate", typeof(Decimal)));
            DataColumn[] dataColumnArray1 = new DataColumn[1];
            int index = 0;
            DataColumn dataColumn = dataTable.Columns["UOM"];
            dataColumnArray1[index] = dataColumn;
            DataColumn[] dataColumnArray2 = dataColumnArray1;
            dataTable.PrimaryKey = dataColumnArray2;
            dataTable.DefaultView.Sort = "Rate DESC";
            return dataTable;
        }

        private void CalculateMasterUOMOption()
        {
            this.myResultDataSet.Tables["Master"].Columns.Add(this.AddColumn("BalanceString", "System.String", true, "Master"));
            this.CopyMasterTable();
            string str1 = string.Empty;
            string str2 = string.Empty;
            string str3 = string.Empty;
            string str4 = string.Empty;
            Decimal num1 = new Decimal();
            DataTable balanceStringTable = this.CreateBalanceStringTable();
            for (int index1 = 0; index1 < this.myResultDataSet.Tables["Master"].Rows.Count; ++index1)
            {
                if (string.Compare(str1.TrimEnd(new char[0]), this.myResultDataSet.Tables["Master"].Rows[index1]["ItemCode"].ToString().TrimEnd(new char[0]), true) != 0 || string.Compare(str2.TrimEnd(new char[0]), this.myResultDataSet.Tables["Master"].Rows[index1]["Location"].ToString().TrimEnd(new char[0]), true) != 0 || string.Compare(str3.TrimEnd(new char[0]), this.myResultDataSet.Tables["Master"].Rows[index1]["BatchNo"].ToString().TrimEnd(new char[0]), true) != 0)
                {
                    balanceStringTable.Clear();
                    str1 = this.myResultDataSet.Tables["Master"].Rows[index1]["ItemCode"].ToString();
                    str2 = this.myResultDataSet.Tables["Master"].Rows[index1]["Location"].ToString();
                    str3 = this.myResultDataSet.Tables["Master"].Rows[index1]["BatchNo"].ToString();
                    DataRow dataRow = this.myResultDataSet.Tables["UOMRate"].Rows.Find(new object[3]
                    {
            (object) str2,
            (object) str1,
            (object) str3
                    });
                    string index2 = "UOM";
                    str4 = dataRow[index2].ToString();
                    string index3 = "Rate";
                    num1 = BCE.Data.Convert.ToDecimal(dataRow[index3]);
                }
                DataRow dataRow1 = balanceStringTable.Rows.Find(this.myResultDataSet.Tables["Master"].Rows[index1]["UOM"]);
                if (dataRow1 == null)
                {
                    DataRow row = balanceStringTable.NewRow();
                    row.BeginEdit();
                    row["UOM"] = this.myResultDataSet.Tables["Master"].Rows[index1]["UOM"];
                    row["Qty"] = this.myResultDataSet.Tables["Master"].Rows[index1]["PastBalance"];
                    row["Rate"] = this.myResultDataSet.Tables["Master"].Rows[index1]["Rate"];
                    row.EndEdit();
                    balanceStringTable.Rows.Add(row);
                }
                else
                {
                    Decimal num2 = BCE.Data.Convert.ToDecimal(dataRow1["Qty"]) + BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["PastBalance"]);
                    dataRow1.BeginEdit();
                    dataRow1["Qty"] = (object)num2;
                    dataRow1.EndEdit();
                }
                this.myResultDataSet.Tables["Master"].Rows[index1].BeginEdit();
                this.myResultDataSet.Tables["Master"].Rows[index1]["BalanceString"] = (object)this.GetBalanceString(balanceStringTable);
                if (string.Compare(this.myResultDataSet.Tables["Master"].Rows[index1]["UOM"].ToString().TrimEnd(new char[0]), str4.TrimEnd(new char[0])) != 0)
                {
                    Decimal num2 = BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["Rate"]);
                    if (num2 != Decimal.One)
                    {
                        this.myResultDataSet.Tables["Master"].Rows[index1]["Balance"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["Balance"]) * num2 / num1);
                        this.myResultDataSet.Tables["Master"].Rows[index1]["BalanceQty"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["BalanceQty"]) * num2 / num1);
                    }
                    else
                    {
                        this.myResultDataSet.Tables["Master"].Rows[index1]["Balance"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["Balance"]) / num1);
                        this.myResultDataSet.Tables["Master"].Rows[index1]["BalanceQty"] = (object)(BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["BalanceQty"]) / num1);
                    }
                    DataRow dataRow2 = this.myResultDataSet.Tables["Master"].Rows.Find(new object[4]
                    {
            (object) str2,
            (object) str1,
            (object) str3,
            (object) str4
                    });
                    if (dataRow2 != null)
                    {
                        dataRow2.BeginEdit();
                        dataRow2["Balance"] = (object)(BCE.Data.Convert.ToDecimal(dataRow2["Balance"]) + BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["Balance"]));
                        dataRow2["TotalCost"] = (object)(BCE.Data.Convert.ToDecimal(dataRow2["TotalCost"]) + BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["TotalCost"]));
                        dataRow2["BalanceQty"] = (object)(BCE.Data.Convert.ToDecimal(dataRow2["BalanceQty"]) + BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["BalanceQty"]));
                        dataRow2["BalanceCost"] = (object)(BCE.Data.Convert.ToDecimal(dataRow2["BalanceCost"]) + BCE.Data.Convert.ToDecimal(this.myResultDataSet.Tables["Master"].Rows[index1]["BalanceCost"]));
                        dataRow2.EndEdit();
                        this.myResultDataSet.Tables["Master"].Rows[index1].Delete();
                    }
                    else
                        this.myResultDataSet.Tables["Master"].Rows[index1]["UOM"] = (object)str4;
                }
                this.myResultDataSet.Tables["Master"].Rows[index1].EndEdit();
            }
            this.myResultDataSet.Tables["Master"].AcceptChanges();
        }

        private void CopyMasterTable()
        {
            DataTable table = this.myResultDataSet.Tables["Master"].Copy();
            table.TableName = "MasterCopy";
            this.myResultDataSet.Tables.Add(table);
            this.myResultDataSet.Tables["MasterCopy"].AcceptChanges();
            this.myResultDataSet.Tables["MasterCopy"].DefaultView.Sort = "ItemCode,Location, UOM,WONo";
        }

        protected virtual void QueryMasterTable()
        {
        }

        protected virtual void QueryDetailTable()
        {
        }

        protected virtual void QueryItemSNTable()
        {
        }

        protected virtual void QueryMissingMasterTable()
        {
        }

        protected virtual void QueryUOMOptions()
        {
        }

        protected virtual void QueryUOMNonTransactionOptions()
        {
        }

        protected virtual void QueryMissingCSGN()
        {
        }
    }

}
