﻿
using BCE.AutoCount;
using BCE.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.AutoCount.RegistryID.CostingOption;
namespace Production.StockCard
{
    public class StockCosting
    {
        protected DBSetting myDBSetting;
        protected DataTable myUpdateItemUOMTable;
        protected DataTable myUpdateItemBalQtyTable;
        protected DataTable myUpdateItemBatchBalQtyTable;
        protected DataTable myUpdateItemBatchTable;
        protected StockHelper myStockHelper;
        protected bool myIgnoreLocationInCostingCalculation;
        protected bool myIgnoreBatchNumberInCostingCalculation;
        private string myDefaultLocationInCostingCalculation;
        private DecimalSetting myDecimalSetting;

        internal StockCosting()
        {
        }

        public static StockCosting Create(DBSetting dbSetting)
        {
            if (dbSetting.ServerType == DBServerType.SQL2000)
            {
                StockCosting stockCosting = (StockCosting)new StockCostingSQL();
                stockCosting.myDBSetting = dbSetting;
                stockCosting.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
                DBRegistry dbRegistry = DBRegistry.Create(dbSetting);
                stockCosting.myIgnoreLocationInCostingCalculation = dbRegistry.GetBoolean((IRegistryID)new IgnoreLocationInCostingCalculation());
                stockCosting.myIgnoreBatchNumberInCostingCalculation = dbRegistry.GetBoolean((IRegistryID)new IgnoreBatchNumberInCostingCalculation());
                stockCosting.myDefaultLocationInCostingCalculation = dbRegistry.GetString((IRegistryID)new DefaultLocationInCostingCalculation());
                return stockCosting;
            }
            else
                throw new ArgumentException("Server type: " + (object)dbSetting.ServerType + " not supported.");
        }

        protected void CreateUpdateItemUOMTable()
        {
            DataTable dataTable1 = new DataTable();
            dataTable1.Columns.Add("ItemCode", Type.GetType("System.String"));
            dataTable1.Columns.Add("UOM", Type.GetType("System.String"));
            dataTable1.Columns.Add("Qty", Type.GetType("System.Decimal"));
            DataTable dataTable2 = dataTable1;
            DataColumn[] dataColumnArray = new DataColumn[2];
            int index1 = 0;
            DataColumn dataColumn1 = dataTable1.Columns["ItemCode"];
            dataColumnArray[index1] = dataColumn1;
            int index2 = 1;
            DataColumn dataColumn2 = dataTable1.Columns["UOM"];
            dataColumnArray[index2] = dataColumn2;
            dataTable2.PrimaryKey = dataColumnArray;
            this.myUpdateItemUOMTable = dataTable1;
        }

        protected void AddItemUOMQty(string itemCode, string UOM, Decimal diffQty)
        {
            DataRowCollection rows = this.myUpdateItemUOMTable.Rows;
            object[] keys = new object[2];
            int index1 = 0;
            string str1 = itemCode;
            keys[index1] = (object)str1;
            int index2 = 1;
            string str2 = UOM;
            keys[index2] = (object)str2;
            DataRow row = rows.Find(keys);
            if (row == null)
            {
                row = this.myUpdateItemUOMTable.NewRow();
                row["ItemCode"] = (object)itemCode;
                row["UOM"] = (object)UOM;
                this.myUpdateItemUOMTable.Rows.Add(row);
            }
            row["Qty"] = (object)(BCE.Data.Convert.ToDecimal(row["Qty"]) + diffQty);
            row.EndEdit();
        }

        protected void CreateUpdateItemBalQtyTable()
        {
            DataTable dataTable1 = new DataTable();
            dataTable1.Columns.Add("ItemCode", Type.GetType("System.String"));
            dataTable1.Columns.Add("UOM", Type.GetType("System.String"));
            dataTable1.Columns.Add("Location", Type.GetType("System.String"));
            dataTable1.Columns.Add("Qty", Type.GetType("System.Decimal"));
            DataTable dataTable2 = dataTable1;
            DataColumn[] dataColumnArray = new DataColumn[3];
            int index1 = 0;
            DataColumn dataColumn1 = dataTable1.Columns["ItemCode"];
            dataColumnArray[index1] = dataColumn1;
            int index2 = 1;
            DataColumn dataColumn2 = dataTable1.Columns["UOM"];
            dataColumnArray[index2] = dataColumn2;
            int index3 = 2;
            DataColumn dataColumn3 = dataTable1.Columns["Location"];
            dataColumnArray[index3] = dataColumn3;
            dataTable2.PrimaryKey = dataColumnArray;
            this.myUpdateItemBalQtyTable = dataTable1;
        }

        protected void AddItemBalQty(string itemCode, string UOM, string location, Decimal diffQty)
        {
            DataRowCollection rows = this.myUpdateItemBalQtyTable.Rows;
            object[] keys = new object[3];
            int index1 = 0;
            string str1 = itemCode;
            keys[index1] = (object)str1;
            int index2 = 1;
            string str2 = UOM;
            keys[index2] = (object)str2;
            int index3 = 2;
            string str3 = location;
            keys[index3] = (object)str3;
            DataRow row = rows.Find(keys);
            if (row == null)
            {
                row = this.myUpdateItemBalQtyTable.NewRow();
                row["ItemCode"] = (object)itemCode;
                row["UOM"] = (object)UOM;
                row["Location"] = (object)location;
                this.myUpdateItemBalQtyTable.Rows.Add(row);
            }
            row["Qty"] = (object)(BCE.Data.Convert.ToDecimal(row["Qty"]) + diffQty);
            row.EndEdit();
        }

        protected void CreateUpdateItemBatchBalQtyTable()
        {
            DataTable dataTable1 = new DataTable();
            dataTable1.Columns.Add("ItemCode", Type.GetType("System.String"));
            dataTable1.Columns.Add("UOM", Type.GetType("System.String"));
            dataTable1.Columns.Add("Location", Type.GetType("System.String"));
            dataTable1.Columns.Add("BatchNo", Type.GetType("System.String"));
            dataTable1.Columns.Add("Qty", Type.GetType("System.Decimal"));
            DataTable dataTable2 = dataTable1;
            DataColumn[] dataColumnArray = new DataColumn[4];
            int index1 = 0;
            DataColumn dataColumn1 = dataTable1.Columns["ItemCode"];
            dataColumnArray[index1] = dataColumn1;
            int index2 = 1;
            DataColumn dataColumn2 = dataTable1.Columns["UOM"];
            dataColumnArray[index2] = dataColumn2;
            int index3 = 2;
            DataColumn dataColumn3 = dataTable1.Columns["Location"];
            dataColumnArray[index3] = dataColumn3;
            int index4 = 3;
            DataColumn dataColumn4 = dataTable1.Columns["BatchNo"];
            dataColumnArray[index4] = dataColumn4;
            dataTable2.PrimaryKey = dataColumnArray;
            this.myUpdateItemBatchBalQtyTable = dataTable1;
        }

        protected void AddItemBatchBalQty(string itemCode, string UOM, string location, string batchNo, Decimal diffQty)
        {
            DataRowCollection rows = this.myUpdateItemBatchBalQtyTable.Rows;
            object[] keys = new object[4];
            int index1 = 0;
            string str1 = itemCode;
            keys[index1] = (object)str1;
            int index2 = 1;
            string str2 = UOM;
            keys[index2] = (object)str2;
            int index3 = 2;
            string str3 = location;
            keys[index3] = (object)str3;
            int index4 = 3;
            string str4 = batchNo;
            keys[index4] = (object)str4;
            DataRow row = rows.Find(keys);
            if (row == null)
            {
                row = this.myUpdateItemBatchBalQtyTable.NewRow();
                row["ItemCode"] = (object)itemCode;
                row["UOM"] = (object)UOM;
                row["Location"] = (object)location;
                row["BatchNo"] = (object)batchNo;
                this.myUpdateItemBatchBalQtyTable.Rows.Add(row);
            }
            row["Qty"] = (object)(BCE.Data.Convert.ToDecimal(row["Qty"]) + diffQty);
            row.EndEdit();
        }

        protected void CreateUpdateItemBatchTable()
        {
            DataTable dataTable1 = new DataTable();
            dataTable1.Columns.Add("ItemCode", Type.GetType("System.String"));
            dataTable1.Columns.Add("BatchNo", Type.GetType("System.String"));
            dataTable1.Columns.Add("Qty", Type.GetType("System.Decimal"));
            DataTable dataTable2 = dataTable1;
            DataColumn[] dataColumnArray = new DataColumn[2];
            int index1 = 0;
            DataColumn dataColumn1 = dataTable1.Columns["ItemCode"];
            dataColumnArray[index1] = dataColumn1;
            int index2 = 1;
            DataColumn dataColumn2 = dataTable1.Columns["BatchNo"];
            dataColumnArray[index2] = dataColumn2;
            dataTable2.PrimaryKey = dataColumnArray;
            this.myUpdateItemBatchTable = dataTable1;
        }

        protected void AddItemBatch(string itemCode, string batchNo, Decimal diffQty)
        {
            if (batchNo.Length != 0)
            {
                DataRowCollection rows = this.myUpdateItemBatchTable.Rows;
                object[] keys = new object[2];
                int index1 = 0;
                string str1 = itemCode;
                keys[index1] = (object)str1;
                int index2 = 1;
                string str2 = batchNo;
                keys[index2] = (object)str2;
                DataRow row = rows.Find(keys);
                if (row == null)
                {
                    row = this.myUpdateItemBatchTable.NewRow();
                    row["ItemCode"] = (object)itemCode;
                    row["BatchNo"] = (object)batchNo;
                    this.myUpdateItemBatchTable.Rows.Add(row);
                }
                row["Qty"] = (object)(BCE.Data.Convert.ToDecimal(row["Qty"]) + diffQty);
                row.EndEdit();
            }
        }

        protected void CreateAllBalQtyTable()
        {
            this.CreateUpdateItemUOMTable();
            this.CreateUpdateItemBalQtyTable();
            this.CreateUpdateItemBatchBalQtyTable();
            this.CreateUpdateItemBatchTable();
        }

        protected void AddAllBalQty(string itemCode, string UOM, string location, string batchNo, Decimal diffQty)
        {
            if (!(diffQty == Decimal.Zero))
            {
                this.AddItemUOMQty(itemCode, UOM, diffQty);
                this.AddItemBalQty(itemCode, UOM, location, diffQty);
                this.AddItemBatchBalQty(itemCode, UOM, location, batchNo, diffQty);
                if (batchNo != null && batchNo.Length > 0)
                {
                    Decimal num = this.myStockHelper.GetItemUOMRate(itemCode, UOM);
                    if (num == Decimal.Zero)
                        num = Decimal.One;
                    diffQty = this.myDecimalSetting.RoundQuantity(diffQty * num);
                    this.AddItemBatch(itemCode, batchNo, diffQty);
                }
            }
        }

        public virtual void Add(StockTrans trans)
        {
        }

        public virtual void Remove(string docType, long docKey)
        {
        }

        public long GetStockDTLKeyForDtlKey(long dtlKey)
        {
            long num = 0L;
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT StockDTLKey FROM StockDTL WHERE DtlKey=@DtlKey";
            object[] objArray = new object[1];
            int index = 0;
            SqlParameter sqlParameter = new SqlParameter("@DtlKey", (object)dtlKey);
            objArray[index] = (object)sqlParameter;
            object obj = dbSetting.ExecuteScalar(cmdText, objArray);
            if (obj != null)
                num = BCE.Data.Convert.ToInt64(obj);
            return num;
        }

        public FIFOCost GetFIFOCostForDtlKey(long dtlKey)
        {
            return FIFOCost.Create(this.myDBSetting, this.GetStockDTLKeyForDtlKey(dtlKey));
        }

        protected bool IsStockDetailChanged(DataRow dbRow, DataRow row, out bool isMainColumnChanged, out bool isMainColumnChanged2)
        {
            isMainColumnChanged = !this.myIgnoreLocationInCostingCalculation && dbRow["Location"].ToString() != row["Location"].ToString() || (dbRow["UOM"].ToString() != row["UOM"].ToString() || dbRow["ItemCode"].ToString() != row["ItemCode"].ToString()) || !this.myIgnoreBatchNumberInCostingCalculation && dbRow["BatchNo"].ToString() != row["BatchNo"].ToString();
            if (isMainColumnChanged)
            {
                isMainColumnChanged2 = true;
                return true;
            }
            else
            {
                isMainColumnChanged2 = dbRow["Location"].ToString() != row["Location"].ToString() || dbRow["UOM"].ToString() != row["UOM"].ToString() || dbRow["ItemCode"].ToString() != row["ItemCode"].ToString() || dbRow["BatchNo"].ToString() != row["BatchNo"].ToString();
                Decimal num1 = BCE.Data.Convert.ToDecimal(dbRow["Qty"]);
                long num2 = BCE.Data.Convert.ToInt64(row["ReferTo"]);
                if (num1 != BCE.Data.Convert.ToDecimal(row["Qty"]) || num2 == 0L && num1 >= Decimal.Zero && BCE.Data.Convert.ToDecimal(dbRow["Cost"]) != BCE.Data.Convert.ToDecimal(row["Cost"]))
                    return true;
                else
                    return false;
            }
        }

        protected UTDCostHelper CreateUTDCostHelper(DBSetting newDBSetting, DataRow dbRow)
        {
            return UTDCostHelper.Create(newDBSetting, dbRow["ItemCode"].ToString(), dbRow["UOM"].ToString(), this.myIgnoreLocationInCostingCalculation ? this.myDefaultLocationInCostingCalculation : dbRow["Location"].ToString(), this.myIgnoreBatchNumberInCostingCalculation ? (object)DBNull.Value : dbRow["BatchNo"], BCE.Data.Convert.ToDateTime(dbRow["DocDate"]));
        }

        protected static Decimal GetCostFromStockDTLKey(DBSetting dbSetting, long stockDTLKey)
        {
            DBSetting dbSetting1 = dbSetting;
            string cmdText = "SELECT Cost FROM StockDTL WHERE StockDtlKey=@StockDtlKey";
            object[] objArray = new object[1];
            int index = 0;
            SqlParameter sqlParameter = new SqlParameter("@stockDtlKey", (object)stockDTLKey);
            objArray[index] = (object)sqlParameter;
            object obj = dbSetting1.ExecuteScalar(cmdText, objArray);
            return obj == null ? new Decimal() : BCE.Data.Convert.ToDecimal(obj);
        }
    }

}
