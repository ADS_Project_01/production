﻿using BCE.AutoCount;
using BCE.AutoCount.ARAP;
using BCE.AutoCount.ARAP.Report;
using BCE.AutoCount.Common;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Report;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using DevExpress.XtraEditors;
using System;
using System.Data;


namespace Production.StockCard
{
    public class StockCardFormHelper
    {
        public const string ReportType = "Stock Card WIP";
        protected DBSetting myDBSetting;
        protected StockCardFormCriteria myCriteria;
        private BasicReportOption myReportOption;
        private DataSet myResultDataSet;
        private DataTable myResultTable;
        private DateEdit myFromDate;
        private DateEdit myToDate;

        public StockCardFormCriteria Criteria
        {
            get
            {
                return this.myCriteria;
            }
            set
            {
                this.myCriteria = value;
            }
        }

        public BasicReportOption ReportOption
        {
            get
            {
                return this.myReportOption;
            }
        }

        public DataSet ResultDataSet
        {
            get
            {
                return this.myResultDataSet;
            }
        }

        public DataTable ResultTable
        {
            get
            {
                return this.myResultTable;
            }
        }

        public StockCardFormHelper(DBSetting dbSetting)
        {
            this.myDBSetting = dbSetting;
            try
            {
                this.myCriteria = (StockCardFormCriteria)PersistenceUtil.LoadCriteriaData(this.myDBSetting, "StockCardFormCriteria2.setting");
            }
            catch
            {
            }
            if (this.myCriteria == null)
                this.myCriteria = new StockCardFormCriteria(dbSetting);
            else
                this.myCriteria.DBSetting = this.myDBSetting;
            try
            {
                this.myReportOption = (BasicReportOption)PersistenceUtil.LoadUserSetting("StockCardReportOption2.setting");
            }
            catch
            {
            }
            if (this.myReportOption == null)
                this.myReportOption = new BasicReportOption();
        }

        public void Inquire(DateEdit fromDate, DateEdit toDate)
        {
            this.myFromDate = fromDate;
            this.myToDate = toDate;
            StockCard stockCard = StockCard.Create(this.myDBSetting);
            stockCard.Inquire(this.myFromDate, this.myToDate, this.myCriteria.LocationFilter, this.myCriteria.StockItemFilter, this.myCriteria.WONoFilter, this.myCriteria.StockGroupFilter, this.myCriteria.StockItemTypeFilter, this.myCriteria.MergeSameCost, this.myCriteria.ActiveItem, this.myCriteria.InactiveItem, this.myCriteria.UOMOption, this.myCriteria.BatchOption, this.myCriteria.IncludeZeroBalance, this.myCriteria.IncludeNonTransactionItem);
            this.myResultDataSet = stockCard.ResultDataSet;
            this.myResultTable = stockCard.ResultDataSet.Tables["Master"];
            DataTable dataTable = this.myDBSetting.GetDataTable("SELECT ItemCode, FurtherDescription FROM Item", true, new object[0]);
            this.myResultTable.Columns.Add("FurtherDescription", typeof(string));
            foreach (DataRow dataRow1 in (InternalDataCollectionBase)this.myResultTable.Rows)
            {
                DataRow dataRow2 = dataTable.Rows.Find(dataRow1["ItemCode"]);
                dataRow1["FurtherDescription"] = dataRow2["FurtherDescription"];
            }
        }

        public void InquireReportBuilder()
        {
            this.myFromDate = new DateEdit();
            this.myFromDate.DateTime = new DateTime(DateTime.Now.Year, 1, 1);
            DateTime actualDataStartDate = FiscalYear.GetOrCreate(this.myDBSetting).ActualDataStartDate;
            if (this.myFromDate.DateTime < actualDataStartDate)
                this.myFromDate.DateTime = actualDataStartDate;
            this.myToDate = new DateEdit();
            this.myToDate.DateTime = DateTime.Now.Date;
            BCE.AutoCount.SearchFilter.Filter locationFilter = new BCE.AutoCount.SearchFilter.Filter("A", "Location", Localizer.GetString((Enum)StockStringId.Location, new object[0]), FilterControlType.Location);
            BCE.AutoCount.SearchFilter.Filter itemFilter = new BCE.AutoCount.SearchFilter.Filter("A", "ItemCode", Localizer.GetString((Enum)StockStringId.StockItem, new object[0]), FilterControlType.Item);
            BCE.AutoCount.SearchFilter.Filter wonoFilter = new BCE.AutoCount.SearchFilter.Filter("A", "WONo");

            BCE.AutoCount.SearchFilter.Filter groupFilter = new BCE.AutoCount.SearchFilter.Filter("Item", "ItemGroup", Localizer.GetString((Enum)StockStringId.StockGroup, new object[0]), FilterControlType.ItemGroup);
            BCE.AutoCount.SearchFilter.Filter itemTypeFilter = new BCE.AutoCount.SearchFilter.Filter("Item", "ItemType", Localizer.GetString((Enum)StockStringId.ItemType, new object[0]), FilterControlType.ItemType);
            locationFilter.Type = FilterType.None;
            itemFilter.Type = FilterType.ByRange;
            groupFilter.Type = FilterType.None;
            itemTypeFilter.Type = FilterType.None;
            ReportBuilderFilter reportBuilderFilter = ReportBuilderFilter.Create(this.myDBSetting);
            reportBuilderFilter.GetFilter(25, "ItemCode", "Item");
            itemFilter.From = (object)reportBuilderFilter.FirstItem;
            itemFilter.To = (object)reportBuilderFilter.LastItem;
            StockCard stockCard = StockCard.Create(this.myDBSetting);
            stockCard.Inquire(this.myFromDate, this.myToDate, locationFilter, itemFilter,wonoFilter, groupFilter, itemTypeFilter, false, true, false, ShowUOMOption.ShowMultiUOM, PrintBatchOption.PrintActiveBatch);
            this.myResultDataSet = stockCard.ResultDataSet;
            this.myResultTable = stockCard.ResultDataSet.Tables["Master"];
        }

        public void SaveSetting()
        {
            PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting)this.myCriteria, "StockCardFormCriteria2.setting");
        }

        public void SaveReportOption()
        {
            PersistenceUtil.SaveUserSetting((object)this.myReportOption, "StockCardReportOption2.setting");
        }

        public void PrintReport()
        {
            ReportTool.PrintReport("Stock Card", this.GetReportDataSource(), this.myDBSetting, true, this.myReportOption, new ReportInfo(Localizer.GetString((Enum)StockCardStringId.StockCardReport, new object[0]), "STK_REPORT_STKCARD_PRINT", "STK_REPORT_STKCARD_EXPORT", this.myCriteria.ReadableText));
        }

        public void PreviewReport(BCE.AutoCount.Controls.PrintEventArgs e)
        {
            ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum)StockCardStringId.StockCardReport, new object[0]), "STK_REPORT_STKCARD_PRINT", "STK_REPORT_STKCARD_EXPORT", this.myCriteria.ReadableText);
            ReportTool.PreviewReport("Stock Card", this.GetReportDataSource(), this.myDBSetting, e.DefaultReport, false, this.myReportOption, reportInfo);
        }

        public void DesignReport()
        {
            ReportTool.DesignReport("Stock Card", this.GetReportDataSource(), this.myDBSetting);
        }

        public object GetReportDesignerDataSource()
        {
            return this.GetReportDataSource();
        }

        private object GetReportDataSource()
        {
            DocumentReportDataSet dsReport = new DocumentReportDataSet(this.myDBSetting, "Stock Card WIP", "Master", "Detail", "SubDetail");
            dsReport.Tables.Add(this.myResultDataSet.Tables["Master"].Copy());
            dsReport.Tables.Add(this.myResultDataSet.Tables["Detail"].Copy());
          //  dsReport.Tables.Add(this.myResultDataSet.Tables["SubDetail"].Copy());
         //   dsReport.Tables.Add(this.myResultDataSet.Tables["ItemSerialNo"].Copy());
            DataColumn[] dataColumnArray1 = new DataColumn[3];
            //int index1 = 0;
            //DataColumn dataColumn1 = dsReport.Tables["Master"].Columns["Location"];
            //dataColumnArray1[index1] = dataColumn1;
            int index2 =0;
            DataColumn dataColumn2 = dsReport.Tables["Master"].Columns["ItemCode"];
            dataColumnArray1[index2] = dataColumn2;
            int index3 = 1;
            DataColumn dataColumn3 = dsReport.Tables["Master"].Columns["WONo"];
            dataColumnArray1[index3] = dataColumn3;
            int index4 = 2;
            DataColumn dataColumn4 = dsReport.Tables["Master"].Columns["UOM"];
            dataColumnArray1[index4] = dataColumn4;
            DataColumn[] parentColumns1 = dataColumnArray1;
            DataColumn[] dataColumnArray2 = new DataColumn[3];
            //int index5 = 0;
            //DataColumn dataColumn5 = dsReport.Tables["Detail"].Columns["Location"];
            //dataColumnArray2[index5] = dataColumn5;
            int index6 = 0;
            DataColumn dataColumn6 = dsReport.Tables["Detail"].Columns["ItemCode"];
            dataColumnArray2[index6] = dataColumn6;
            int index7 = 1;
            DataColumn dataColumn7 = dsReport.Tables["Detail"].Columns["WONo"];
            dataColumnArray2[index7] = dataColumn7;
            int index8 = 2;
            DataColumn dataColumn8 = dsReport.Tables["Detail"].Columns["UOM"];
            dataColumnArray2[index8] = dataColumn8;
            DataColumn[] childColumns1 = dataColumnArray2;
            dsReport.Relations.Add("MasterDetailRelation", parentColumns1, childColumns1);
            //DataColumn parentColumn = dsReport.Tables["Detail"].Columns["StockDTLKey"];
           // DataColumn childColumn = dsReport.Tables["SubDetail"].Columns["StockDTLKey"];
          //  dsReport.Relations.Add("DetailSubdetailRelation", parentColumn, childColumn);
            //DataColumn[] dataColumnArray3 = new DataColumn[3];
            //int index9 = 0;
            //DataColumn dataColumn9 = dsReport.Tables["Master"].Columns["ItemCode"];
            //dataColumnArray3[index9] = dataColumn9;
            //int index10 = 1;
            //DataColumn dataColumn10 = dsReport.Tables["Master"].Columns["BatchNo"];
            //dataColumnArray3[index10] = dataColumn10;
            //int index11 = 2;
            //DataColumn dataColumn11 = dsReport.Tables["Master"].Columns["Location"];
            //dataColumnArray3[index11] = dataColumn11;
            //DataColumn[] parentColumns2 = dataColumnArray3;
            //DataColumn[] dataColumnArray4 = new DataColumn[3];
            //int index12 = 0;
            //DataColumn dataColumn12 = dsReport.Tables["ItemSerialNo"].Columns["ItemCode"];
            //dataColumnArray4[index12] = dataColumn12;
            //int index13 = 1;
            //DataColumn dataColumn13 = dsReport.Tables["ItemSerialNo"].Columns["BatchNo"];
            //dataColumnArray4[index13] = dataColumn13;
            //int index14 = 2;
            //DataColumn dataColumn14 = dsReport.Tables["ItemSerialNo"].Columns["Location"];
            //dataColumnArray4[index14] = dataColumn14;
            //DataColumn[] childColumns2 = dataColumnArray4;
            //dsReport.Relations.Add("MasterItemSNRelation", parentColumns2, childColumns2, false);
            dsReport.Tables.Add(this.GenerateReportOptions());
            this.GenerateGrouping(dsReport);
            dsReport.AddDefaultTables();
            return (object)dsReport;
        }

        private DataTable GenerateReportOptions()
        {
            DataTable dataTable = new DataTable("Report Option");
            dataTable.Columns.Add("FromDate", typeof(DateTime));
            dataTable.Columns.Add("ToDate", typeof(DateTime));
            dataTable.Columns.Add("Criteria", typeof(string));
            dataTable.Columns.Add("GroupBy", typeof(string));
            dataTable.Columns.Add("PrintActiveItem", typeof(string));
            dataTable.Columns.Add("PrintInactiveItem", typeof(string));
            dataTable.Columns.Add("MergeSameCost", typeof(string));
            dataTable.Columns.Add("IncludeZeroBalance", typeof(string));
            dataTable.Columns.Add("IncludeNonTransactionItem", typeof(string));
            dataTable.Columns.Add("UOMOption", typeof(string));
            dataTable.Columns.Add("BatchOption", typeof(string));
            dataTable.Columns.Add("ShowCriteria", typeof(string));
            if (this.myCriteria != null)
            {
                string str1 = "";
                foreach (string str2 in this.myCriteria.ReadableTextArray)
                    str1 = str1 + str2 + Environment.NewLine;
                DataRow row = dataTable.NewRow();
                row.BeginEdit();
                row["FromDate"] = (object)this.myFromDate.DateTime.Date;
                row["ToDate"] = (object)this.myToDate.DateTime.Date;
                row["Criteria"] = (object)str1;
                row["GroupBy"] = (object)Localizer.GetString((Enum)this.myCriteria.GroupBy, new object[0]);
                row["PrintActiveItem"] = !this.myCriteria.ActiveItem ? (object)"No" : (object)"Yes";
                row["PrintInactiveItem"] = !this.myCriteria.InactiveItem ? (object)"No" : (object)"Yes";
                row["MergeSameCost"] = !this.myCriteria.MergeSameCost ? (object)"No" : (object)"Yes";
                row["IncludeZeroBalance"] = !this.myCriteria.IncludeZeroBalance ? (object)"No" : (object)"Yes";
                row["IncludeNonTransactionItem"] = !this.myCriteria.IncludeNonTransactionItem ? (object)"No" : (object)"Yes";
                row["UOMOption"] = this.myCriteria.UOMOption != ShowUOMOption.ShowMultiUOM ? (this.myCriteria.UOMOption != ShowUOMOption.ShowDefaultReportUOM ? (object)"Show Smallest UOM" : (object)"Show Default Report UOM") : (object)"Show Multi-UOM";
                row["BatchOption"] = (object)Localizer.GetString((Enum)this.myCriteria.BatchOption, new object[0]);
                row["ShowCriteria"] = !this.myCriteria.IsShowCriteria ? (object)"No" : (object)"Yes";
                row.EndEdit();
                dataTable.Rows.Add(row);
            }
            else
            {
                DataRow row = dataTable.NewRow();
                row.BeginEdit();
                row["FromDate"] = (object)this.myFromDate.DateTime.Date;
                row["ToDate"] = (object)this.myToDate.DateTime.Date;
                row["Criteria"] = (object)"Default Criteria";
                row["GroupBy"] = (object)"Stock Group";
                row["PrintActiveItem"] = (object)"Yes";
                row["PrintInactiveItem"] = (object)"No";
                row["MergeSameCost"] = (object)"Yes";
                row["IncludeZeroBalance"] = (object)"Yes";
                row["IncludeNonTransactionItem"] = (object)"No";
                row["UOMOption"] = (object)"Show Multi-UOM";
                row["BatchOptions"] = (object)"Print Active Batch";
                row["ShowCriteria"] = (object)"No";
                row.EndEdit();
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }

        private void GenerateGrouping(DocumentReportDataSet dsReport)
        {
            dsReport.Tables[0].Columns.Add("GroupID", Type.GetType("System.String"));
            dsReport.Tables[0].Columns.Add("GroupIDName", Type.GetType("System.String"));
            dsReport.Tables[0].Columns.Add("GroupIDDescription", Type.GetType("System.String"));
            DataTable dataTable = new DataTable();
            if (this.myCriteria != null)
            {
                if (this.myCriteria.GroupBy == GroupByItemGroupTypeLocationOption.ItemGroup)
                {
                    string[] primaryKeys = new string[1];
                    int index = 0;
                    string str = "ItemGroup";
                    primaryKeys[index] = str;
                    dataTable = Helper.GetAllGroupIDDescription(this.myDBSetting, "ItemGroup", "ItemGroup, Description ", primaryKeys).Tables["ItemGroup"];
                }
                else if (this.myCriteria.GroupBy == GroupByItemGroupTypeLocationOption.ItemType)
                {
                    string[] primaryKeys = new string[1];
                    int index = 0;
                    string str = "ItemType";
                    primaryKeys[index] = str;
                    dataTable = Helper.GetAllGroupIDDescription(this.myDBSetting, "ItemType", "ItemType, Description ", primaryKeys).Tables["ItemType"];
                }
                else if (this.myCriteria.GroupBy == GroupByItemGroupTypeLocationOption.Location)
                {
                    string[] primaryKeys = new string[1];
                    int index = 0;
                    string str = "Location";
                    primaryKeys[index] = str;
                    dataTable = Helper.GetAllGroupIDDescription(this.myDBSetting, "Location", "Location, Description ", primaryKeys).Tables["Location"];
                }
            }
            for (int index = 0; index < dsReport.Tables["Master"].Rows.Count; ++index)
            {
                DataRow dataRow1 = dsReport.Tables["Master"].Rows[index];
                dsReport.Tables["Master"].Rows[index].BeginEdit();
                if (this.myCriteria != null && this.myCriteria.GroupBy != GroupByItemGroupTypeLocationOption.None)
                {
                    if (this.myCriteria.GroupBy == GroupByItemGroupTypeLocationOption.ItemGroup)
                    {
                        dsReport.Tables["Master"].Rows[index]["GroupID"] = dsReport.Tables["Master"].Rows[index]["ItemGroup"];
                        dsReport.Tables["Master"].Rows[index]["GroupIDName"] = (object)(Localizer.GetString((Enum)this.myCriteria.GroupBy, new object[0]) + " :");
                        DataRow dataRow2 = dataTable.Rows.Find(dataRow1["ItemGroup"]);
                        if (dataRow2 != null)
                            dsReport.Tables["Master"].Rows[index]["GroupIDDescription"] = dataRow2["Description"];
                    }
                    else if (this.myCriteria.GroupBy == GroupByItemGroupTypeLocationOption.ItemType)
                    {
                        dsReport.Tables["Master"].Rows[index]["GroupID"] = dsReport.Tables["Master"].Rows[index]["ItemType"];
                        dsReport.Tables["Master"].Rows[index]["GroupIDName"] = (object)(Localizer.GetString((Enum)this.myCriteria.GroupBy, new object[0]) + " :");
                        DataRow dataRow2 = dataTable.Rows.Find(dataRow1["ItemType"]);
                        if (dataRow2 != null)
                            dsReport.Tables["Master"].Rows[index]["GroupIDDescription"] = dataRow2["Description"];
                    }
                    else if (this.myCriteria.GroupBy == GroupByItemGroupTypeLocationOption.Location)
                    {
                        dsReport.Tables["Master"].Rows[index]["GroupID"] = dsReport.Tables["Master"].Rows[index]["Location"];
                        dsReport.Tables["Master"].Rows[index]["GroupIDName"] = (object)(Localizer.GetString((Enum)this.myCriteria.GroupBy, new object[0]) + " :");
                        DataRow dataRow2 = dataTable.Rows.Find(dataRow1["Location"]);
                        if (dataRow2 != null)
                            dsReport.Tables["Master"].Rows[index]["GroupIDDescription"] = dataRow2["Description"];
                    }
                }
                else
                {
                    dsReport.Tables["Master"].Rows[index]["GroupID"] = (object)string.Empty;
                    dsReport.Tables["Master"].Rows[index]["GroupIDName"] = (object)Localizer.GetString((Enum)GroupByItemGroupTypeLocationOption.None, new object[0]);
                    dsReport.Tables["Master"].Rows[index]["GroupIDDescription"] = (object)string.Empty;
                }
                dsReport.Tables["Master"].Rows[index].EndEdit();
            }
        }
    }

}
