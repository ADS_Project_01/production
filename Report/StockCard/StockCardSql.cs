﻿using BCE.AutoCount.Common;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Misc;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Production.StockCard
{
    public class StockCardSql : StockCard
    {
        internal StockCardSql()
        {
        }

        private void SetBatchNoColumn(string tableName)
        {
            for (int index = 0; index < this.myResultDataSet.Tables[tableName].Rows.Count; ++index)
            {
                if (this.myResultDataSet.Tables[tableName].Rows[index]["BatchNo"] == DBNull.Value)
                {
                    this.myResultDataSet.Tables[tableName].Rows[index].BeginEdit();
                    this.myResultDataSet.Tables[tableName].Rows[index]["BatchNo"] = (object)"";
                    this.myResultDataSet.Tables[tableName].Rows[index].EndEdit();
                }
            }
        }

        private string JoinSqlGenerator(char prefixStockDtl, char prefixItemBatch)
        {
            string str1 = string.Empty;
            string[] strArray = new string[11];
            int index1 = 0;
            string str2 = " LEFT OUTER JOIN ItemBatch ";
            strArray[index1] = str2;
            int index2 = 1;
            string str3 = prefixItemBatch.ToString();
            strArray[index2] = str3;
            int index3 = 2;
            string str4 = " ON ";
            strArray[index3] = str4;
            int index4 = 3;
            string str5 = prefixItemBatch.ToString();
            strArray[index4] = str5;
            int index5 = 4;
            string str6 = ".BatchNo = ";
            strArray[index5] = str6;
            int index6 = 5;
            string str7 = prefixStockDtl.ToString();
            strArray[index6] = str7;
            int index7 = 6;
            string str8 = ".BatchNo  AND ";
            strArray[index7] = str8;
            int index8 = 7;
            string str9 = prefixItemBatch.ToString();
            strArray[index8] = str9;
            int index9 = 8;
            string str10 = ".ItemCode = ";
            strArray[index9] = str10;
            int index10 = 9;
            string str11 = prefixStockDtl.ToString();
            strArray[index10] = str11;
            int index11 = 10;
            string str12 = ".ItemCode ";
            strArray[index11] = str12;
            return string.Concat(strArray);
        }
        private string Join2SqlGenerator(char prefixStockDtl, char prefixItemProduct)
        {
            string str1 = string.Empty;
            string[] strArray = new string[11];
            int index1 = 0;
            string str2 = " LEFT OUTER JOIN Item ";
            strArray[index1] = str2;
            int index2 = 1;
            string str3 = prefixItemProduct.ToString();
            strArray[index2] = str3;
            int index3 = 2;
            string str4 = " ON ";
            strArray[index3] = str4;
            int index4 = 3;
            string str5 = prefixItemProduct.ToString();
            strArray[index4] = str5;
            int index5 = 4;
            string str6 = ".ItemCode = ";
            strArray[index5] = str6;
            int index6 = 5;
            string str7 = prefixStockDtl.ToString();
            strArray[index6] = str7;
            int index7 = 6;
            string str8 = ".ProductCode ";
            strArray[index7] = str8;
            //int index8 = 7;
            //string str9 = prefixItemProduct.ToString();
            //strArray[index8] = str9;
            //int index9 = 8;
            //string str10 = ".ItemCode = ";
            //strArray[index9] = str10;
            //int index10 = 9;
            //string str11 = prefixStockDtl.ToString();
            //strArray[index10] = str11;
            //int index11 = 10;
            //string str12 = ".ItemCode ";
            //strArray[index11] = str12;
            return string.Concat(strArray);
        }
        private string JoinSqlDetailItemGenerator(char prefixStockDtl, string prefixItem)
        {
            string str1 = string.Empty;
            string[] strArray = new string[7];
            int index1 = 0;
            string str2 = "LEFT OUTER JOIN Item ";
            strArray[index1] = str2;
            int index2 = 1;
            string str3 = prefixItem;
            strArray[index2] = str3;
            int index3 = 2;
            string str4 = " ON ";
            strArray[index3] = str4;
            int index4 = 3;
            string str5 = prefixStockDtl.ToString();
            strArray[index4] = str5;
            int index5 = 4;
            string str6 = ".ItemCode = ";
            strArray[index5] = str6;
            int index6 = 5;
            string str7 = prefixItem;
            strArray[index6] = str7;
            int index7 = 6;
            string str8 = ".ItemCode ";
            strArray[index7] = str8;
            return string.Concat(strArray);
        }

        private string JoinSqlItemItemUOMGenerator(string prefixItem, char prefixItemUOM)
        {
            return this.JoinSqlItemItemUOMGenerator(prefixItem, prefixItemUOM, false);
        }

        private string JoinSqlItemItemUOMGenerator(string prefixItem, char prefixItemUOM, bool checkUOMOption)
        {
            string str1 = string.Empty;
            if (!checkUOMOption || this.myUOMOption != ShowUOMOption.ShowMultiUOM)
            {
                string[] strArray = new string[7];
                int index1 = 0;
                string str2 = "INNER JOIN ItemUOM ";
                strArray[index1] = str2;
                int index2 = 1;
                string str3 = prefixItemUOM.ToString();
                strArray[index2] = str3;
                int index3 = 2;
                string str4 = " ON ";
                strArray[index3] = str4;
                int index4 = 3;
                string str5 = prefixItem;
                strArray[index4] = str5;
                int index5 = 4;
                string str6 = ".ItemCode = ";
                strArray[index5] = str6;
                int index6 = 5;
                string str7 = prefixItemUOM.ToString();
                strArray[index6] = str7;
                int index7 = 6;
                string str8 = ".ItemCode ";
                strArray[index7] = str8;
                str1 = string.Concat(strArray);
            }
            return str1;
        }

        private string WhereMasterSqlGenerator(char prefixStockDtl, char prefixItemBatch, string prefixItem)
        {
            return this.WhereMasterSqlGenerator(prefixStockDtl, prefixItemBatch, prefixItem, true);
        }

        private string WhereMasterSqlGenerator(char prefixStockDtl, char prefixItemBatch, string prefixItem, bool checkUOMOption)
        {
            string str1 = string.Empty;
            //if (this.myBatchOption == PrintBatchOption.PrintActiveBatch)
            //{
            //    string[] strArray = new string[6];
            //    int index1 = 0;
            //    string str2 = str1;
            //    strArray[index1] = str2;
            //    int index2 = 1;
            //    string str3 = "AND (";
            //    strArray[index2] = str3;
            //    int index3 = 2;
            //    string str4 = prefixStockDtl.ToString();
            //    strArray[index3] = str4;
            //    int index4 = 3;
            //    string str5 = ".BatchNo IS NULL OR ";
            //    strArray[index4] = str5;
            //    int index5 = 4;
            //    string str6 = prefixItemBatch.ToString();
            //    strArray[index5] = str6;
            //    int index6 = 5;
            //    string str7 = ".ExpiryDate >= @FromDate) ";
            //    strArray[index6] = str7;
            //    str1 = string.Concat(strArray);
            //}
            //else if (this.myBatchOption == PrintBatchOption.PrintEmptyBatch)
            //    str1 = str1 + "AND " + prefixItemBatch.ToString() + ".ExpiryDate IS NULL ";
            //else if (this.myBatchOption == PrintBatchOption.PrintExpiredBatch)
            //    str1 = str1 + "AND " + prefixItemBatch.ToString() + ".ExpiryDate < @FromDate ";
            if (this.IsPrintActive && !this.IsPrintInactive)
                str1 = str1 + "AND " + prefixItem + ".IsActive = 'T' ";
            else if (!this.IsPrintActive && this.IsPrintInactive)
                str1 = str1 + "AND " + prefixItem + ".IsActive = 'F' ";
            //if (checkUOMOption)
                //str1 = str1 + "AND A.UOM = D.UOM ";
            return str1;
        }

        private string WhereDetailSqlGenerator(char prefixStockDtl, char prefixItemBatch, string prefixItem)
        {
            return this.WhereDetailSqlGenerator(prefixStockDtl, prefixItemBatch, prefixItem, false);
        }

        private string WhereDetailSqlGenerator(char prefixStockDtl, char prefixItemBatch, string prefixItem, bool checkUOMOption)
        {
            string str1 = string.Empty;
            if (this.myToDate.EditValue != null)
                str1 = str1 + "AND " + prefixStockDtl.ToString() + ".DocDate <= @ToDate ";
            if (this.myBatchOption == PrintBatchOption.PrintActiveBatch)
            {
                string[] strArray = new string[6];
                int index1 = 0;
                string str2 = str1;
                strArray[index1] = str2;
                int index2 = 1;
                string str3 = "AND (";
                strArray[index2] = str3;
                int index3 = 2;
                string str4 = prefixStockDtl.ToString();
                strArray[index3] = str4;
                int index4 = 3;
                string str5 = ".BatchNo IS NULL OR ";
                strArray[index4] = str5;
                int index5 = 4;
                string str6 = prefixItemBatch.ToString();
                strArray[index5] = str6;
                int index6 = 5;
                string str7 = ".ExpiryDate >= @FromDate) ";
                strArray[index6] = str7;
                str1 = string.Concat(strArray);
            }
            else if (this.myBatchOption == PrintBatchOption.PrintEmptyBatch)
                str1 = str1 + "AND " + prefixItemBatch.ToString() + ".ExpiryDate IS NULL ";
            else if (this.myBatchOption == PrintBatchOption.PrintExpiredBatch)
                str1 = str1 + "AND " + prefixItemBatch.ToString() + ".ExpiryDate < @FromDate ";
            if (this.IsPrintActive && !this.IsPrintInactive)
                str1 = str1 + "AND " + prefixItem + ".IsActive = 'T' ";
            else if (!this.IsPrintActive && this.IsPrintInactive)
                str1 = str1 + "AND " + prefixItem + ".IsActive = 'F' ";
            if (!checkUOMOption || this.myUOMOption != ShowUOMOption.ShowMultiUOM)
                str1 = str1 + "AND A.UOM = E.UOM ";
            return str1;
        }

        private string OrderBySqlGenerator()
        {
            if (this.myUOMOption == ShowUOMOption.ShowMultiUOM)
                return "A.UOM, ";
            else
                return "";
        }

        private string SelectClauseGenerator(char prefixItemUOM)
        {
            string str = string.Empty;
            if (this.myUOMOption != ShowUOMOption.ShowMultiUOM)
                str = ", " + prefixItemUOM.ToString() + ".Rate ";
            return str;
        }

        private string IncludeConsignment(char prefixStockDtl, char prefixCSGNDtl, char prefixCSGN)
        {
            string str1 = string.Empty;
            if (this.IsConsignmentEnable)
            {
                string[] strArray = new string[69];
                int index1 = 0;
                string str2 = ",(ISNULL((SELECT SUM(Qty";
                strArray[index1] = str2;
                int index2 = 1;
                string uomMethod1 = this.GetUomMethod("u1", "r1");
                strArray[index2] = uomMethod1;
                int index3 = 2;
                string str3 = "FROM CSGNDtl ";
                strArray[index3] = str3;
                int index4 = 3;
                string str4 = prefixCSGNDtl.ToString();
                strArray[index4] = str4;
                int index5 = 4;
                string str5 = " INNER JOIN CSGN ";
                strArray[index5] = str5;
                int index6 = 5;
                string str6 = prefixCSGN.ToString();
                strArray[index6] = str6;
                int index7 = 6;
                string str7 = " ON ";
                strArray[index7] = str7;
                int index8 = 7;
                string str8 = prefixCSGNDtl.ToString();
                strArray[index8] = str8;
                int index9 = 8;
                string str9 = ".DocKey = ";
                strArray[index9] = str9;
                int index10 = 9;
                string str10 = prefixCSGN.ToString();
                strArray[index10] = str10;
                int index11 = 10;
                string str11 = ".DocKey ";
                strArray[index11] = str11;
                int index12 = 11;
                string str12 = this.ConsignmentHelper(prefixCSGNDtl);
                strArray[index12] = str12;
                int index13 = 12;
                string str13 = "WHERE ";
                strArray[index13] = str13;
                int index14 = 13;
                string str14 = prefixCSGNDtl.ToString();
                strArray[index14] = str14;
                int index15 = 14;
                string str15 = ".ItemCode = ";
                strArray[index15] = str15;
                int index16 = 15;
                string str16 = prefixStockDtl.ToString();
                strArray[index16] = str16;
                int index17 = 16;
                string str17 = ".ItemCode ";
                strArray[index17] = str17;
                int index18 = 17;
                string uomString1 = this.GetUOMString(prefixCSGNDtl, prefixStockDtl);
                strArray[index18] = uomString1;
                int index19 = 18;
                string str18 = "AND ";
                strArray[index19] = str18;
                int index20 = 19;
                string str19 = prefixCSGNDtl.ToString();
                strArray[index20] = str19;
                int index21 = 20;
                string str20 = ".Location = ";
                strArray[index21] = str20;
                int index22 = 21;
                string str21 = prefixStockDtl.ToString();
                strArray[index22] = str21;
                int index23 = 22;
                string str22 = ".Location AND (";
                strArray[index23] = str22;
                int index24 = 23;
                string str23 = prefixCSGNDtl.ToString();
                strArray[index24] = str23;
                int index25 = 24;
                string str24 = ".BatchNo = ";
                strArray[index25] = str24;
                int index26 = 25;
                string str25 = prefixStockDtl.ToString();
                strArray[index26] = str25;
                int index27 = 26;
                string str26 = ".BatchNo OR (";
                strArray[index27] = str26;
                int index28 = 27;
                string str27 = prefixCSGNDtl.ToString();
                strArray[index28] = str27;
                int index29 = 28;
                string str28 = ".BatchNo IS NULL AND ";
                strArray[index29] = str28;
                int index30 = 29;
                string str29 = prefixStockDtl.ToString();
                strArray[index30] = str29;
                int index31 = 30;
                string str30 = ".BatchNo IS NULL)) AND ";
                strArray[index31] = str30;
                int index32 = 31;
                string str31 = prefixCSGN.ToString();
                strArray[index32] = str31;
                int index33 = 32;
                string str32 = ".DocDate <= @ToDate AND ";
                strArray[index33] = str32;
                int index34 = 33;
                string str33 = prefixCSGN.ToString();
                strArray[index34] = str33;
                int index35 = 34;
                string str34 = ".Cancelled = 'F'), 0) + ISNULL((SELECT SUM(Qty";
                strArray[index35] = str34;
                int index36 = 35;
                string uomMethod2 = this.GetUomMethod("u1", "r1");
                strArray[index36] = uomMethod2;
                int index37 = 36;
                string str35 = "FROM SupplierCSGNDtl ";
                strArray[index37] = str35;
                int index38 = 37;
                string str36 = prefixCSGNDtl.ToString();
                strArray[index38] = str36;
                int index39 = 38;
                string str37 = " INNER JOIN SupplierCSGN ";
                strArray[index39] = str37;
                int index40 = 39;
                string str38 = prefixCSGN.ToString();
                strArray[index40] = str38;
                int index41 = 40;
                string str39 = " ON ";
                strArray[index41] = str39;
                int index42 = 41;
                string str40 = prefixCSGNDtl.ToString();
                strArray[index42] = str40;
                int index43 = 42;
                string str41 = ".DocKey = ";
                strArray[index43] = str41;
                int index44 = 43;
                string str42 = prefixCSGN.ToString();
                strArray[index44] = str42;
                int index45 = 44;
                string str43 = ".DocKey ";
                strArray[index45] = str43;
                int index46 = 45;
                string str44 = this.ConsignmentHelper(prefixCSGNDtl);
                strArray[index46] = str44;
                int index47 = 46;
                string str45 = "WHERE ";
                strArray[index47] = str45;
                int index48 = 47;
                string str46 = prefixCSGNDtl.ToString();
                strArray[index48] = str46;
                int index49 = 48;
                string str47 = ".ItemCode = ";
                strArray[index49] = str47;
                int index50 = 49;
                string str48 = prefixStockDtl.ToString();
                strArray[index50] = str48;
                int index51 = 50;
                string str49 = ".ItemCode ";
                strArray[index51] = str49;
                int index52 = 51;
                string uomString2 = this.GetUOMString(prefixCSGNDtl, prefixStockDtl);
                strArray[index52] = uomString2;
                int index53 = 52;
                string str50 = "AND ";
                strArray[index53] = str50;
                int index54 = 53;
                string str51 = prefixCSGNDtl.ToString();
                strArray[index54] = str51;
                int index55 = 54;
                string str52 = ".Location = ";
                strArray[index55] = str52;
                int index56 = 55;
                string str53 = prefixStockDtl.ToString();
                strArray[index56] = str53;
                int index57 = 56;
                string str54 = ".Location AND (";
                strArray[index57] = str54;
                int index58 = 57;
                string str55 = prefixCSGNDtl.ToString();
                strArray[index58] = str55;
                int index59 = 58;
                string str56 = ".BatchNo = ";
                strArray[index59] = str56;
                int index60 = 59;
                string str57 = prefixStockDtl.ToString();
                strArray[index60] = str57;
                int index61 = 60;
                string str58 = ".BatchNo OR (";
                strArray[index61] = str58;
                int index62 = 61;
                string str59 = prefixCSGNDtl.ToString();
                strArray[index62] = str59;
                int index63 = 62;
                string str60 = ".BatchNo IS NULL AND ";
                strArray[index63] = str60;
                int index64 = 63;
                string str61 = prefixStockDtl.ToString();
                strArray[index64] = str61;
                int index65 = 64;
                string str62 = ".BatchNo IS NULL)) AND ";
                strArray[index65] = str62;
                int index66 = 65;
                string str63 = prefixCSGN.ToString();
                strArray[index66] = str63;
                int index67 = 66;
                string str64 = ".DocDate <= @ToDate AND ";
                strArray[index67] = str64;
                int index68 = 67;
                string str65 = prefixCSGN.ToString();
                strArray[index68] = str65;
                int index69 = 68;
                string str66 = ".Cancelled = 'F'), 0)) AS CSGNBalQty, 0.0 AS BalQtyAfterCSGN, 0.0 AS CSGNCost, 0.0 AS BalCostAfterCSGN ";
                strArray[index69] = str66;
                str1 = string.Concat(strArray);
            }
            return str1;
        }

        private string ConsignmentHelper(char prefixCSGNDtl)
        {
            if (this.myUOMOption == ShowUOMOption.ShowSmallestUOM)
            {
                string[] strArray = new string[5];
                int index1 = 0;
                string str1 = "LEFT OUTER JOIN ItemUOM u1 ON ";
                strArray[index1] = str1;
                int index2 = 1;
                string str2 = prefixCSGNDtl.ToString();
                strArray[index2] = str2;
                int index3 = 2;
                string str3 = ".ItemCode = u1.ItemCode AND ";
                strArray[index3] = str3;
                int index4 = 3;
                string str4 = prefixCSGNDtl.ToString();
                strArray[index4] = str4;
                int index5 = 4;
                string str5 = ".UOM = u1.UOM ";
                strArray[index5] = str5;
                return string.Concat(strArray);
            }
            else if (this.myUOMOption == ShowUOMOption.ShowDefaultReportUOM)
            {
                string[] strArray = new string[7];
                int index1 = 0;
                string str1 = "INNER JOIN Item AS Item1 ON ";
                strArray[index1] = str1;
                int index2 = 1;
                string str2 = prefixCSGNDtl.ToString();
                strArray[index2] = str2;
                int index3 = 2;
                string str3 = ".ItemCode = Item1.ItemCode LEFT OUTER JOIN ItemUOM u1 ON ";
                strArray[index3] = str3;
                int index4 = 3;
                string str4 = prefixCSGNDtl.ToString();
                strArray[index4] = str4;
                int index5 = 4;
                string str5 = ".ItemCode = u1.ItemCode AND ";
                strArray[index5] = str5;
                int index6 = 5;
                string str6 = prefixCSGNDtl.ToString();
                strArray[index6] = str6;
                int index7 = 6;
                string str7 = ".UOM = u1.UOM LEFT OUTER JOIN ItemUOM r1 ON r1.ItemCode = Item1.ItemCode AND r1.UOM = Item1.ReportUOM ";
                strArray[index7] = str7;
                return string.Concat(strArray);
            }
            else
                return "";
        }

        private string MissingConsignmentHelper()
        {
            if (this.myUOMOption == ShowUOMOption.ShowSmallestUOM)
                return "INNER JOIN ItemUOM C ON A.ItemCode = C.ItemCode AND C.UOM = Item.BaseUOM LEFT OUTER JOIN ItemUOM u1 ON A.ItemCode = u1.ItemCode AND A.UOM = u1.UOM ";
            else if (this.myUOMOption == ShowUOMOption.ShowDefaultReportUOM)
                return "INNER JOIN ItemUOM C ON A.ItemCode = C.ItemCode AND C.UOM = Item.ReportUOM LEFT OUTER JOIN ItemUOM u1 ON A.ItemCode = u1.ItemCode AND A.UOM = u1.UOM ";
            else
                return "INNER JOIN ItemUOM C ON A.ItemCode = C.ItemCode AND A.UOM = C.UOM ";
        }

        private string GetUomMethod(string prefixUOM, string prefixReportUOM)
        {
            if (this.myUOMOption == ShowUOMOption.ShowSmallestUOM)
                return "*" + prefixUOM + ".Rate)";
            else if (this.myUOMOption == ShowUOMOption.ShowDefaultReportUOM)
            {
                string[] strArray = new string[5];
                int index1 = 0;
                string str1 = "*";
                strArray[index1] = str1;
                int index2 = 1;
                string str2 = prefixUOM;
                strArray[index2] = str2;
                int index3 = 2;
                string str3 = ".Rate/";
                strArray[index3] = str3;
                int index4 = 3;
                string str4 = prefixReportUOM;
                strArray[index4] = str4;
                int index5 = 4;
                string str5 = ".Rate) ";
                strArray[index5] = str5;
                return string.Concat(strArray);
            }
            else
                return ")";
        }

        private string GetUOMString(char prefixCSGNDtl, char prefixStockDtl)
        {
            if (this.myUOMOption == ShowUOMOption.ShowMultiUOM)
            {
                string[] strArray = new string[5];
                int index1 = 0;
                string str1 = "AND ";
                strArray[index1] = str1;
                int index2 = 1;
                string str2 = prefixCSGNDtl.ToString();
                strArray[index2] = str2;
                int index3 = 2;
                string str3 = ".UOM = ";
                strArray[index3] = str3;
                int index4 = 3;
                string str4 = prefixStockDtl.ToString();
                strArray[index4] = str4;
                int index5 = 4;
                string str5 = ".UOM ";
                strArray[index5] = str5;
                return string.Concat(strArray);
            }
            else
                return "";
        }

        private string GetItemWhereSql(SqlCommand cmd)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BCE.AutoCount.SearchFilter.Filter filter1 = this.myItemFilter;
            searchCriteria.AddFilter(filter1);
            BCE.AutoCount.SearchFilter.Filter filter2 = this.myGroupFilter;
            searchCriteria.AddFilter(filter2);
            BCE.AutoCount.SearchFilter.Filter filter3 = this.myItemTypeFilter;
            searchCriteria.AddFilter(filter3);
            BCE.AutoCount.SearchFilter.Filter filter4 = this.myWONoFilter;
            searchCriteria.AddFilter(filter4);
            SqlCommand sqlCommand = cmd;
            return searchCriteria.BuildSQL((IDbCommand)sqlCommand);
        }

        private string GetLocationWhereSql(SqlCommand cmd)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BCE.AutoCount.SearchFilter.Filter filter = this.myLocationFilter;
            searchCriteria.AddFilter(filter);
            SqlCommand sqlCommand = cmd;
            return searchCriteria.BuildSQL((IDbCommand)sqlCommand);
        }

        protected override void QueryMasterTable()
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            string str1 = this.GetItemWhereSql(sqlCommand);
            if (str1.Length != 0)
                str1 = "AND " + str1;
            string str2 = this.GetLocationWhereSql(sqlCommand);
            if (str2.Length != 0)
                str2 = "AND " + str2;
            string[] strArray = new string[9];
            int index1 = 0;
            string str3 = "SELECT A.WONo, A.Location, A.ItemCode, A.UOM, SUM(Qty) AS Balance, SUM(Qty) AS PastBalance, SUM(Qty) AS BalanceQty, TotalCost = CASE WHEN Item.CostingMethod = 0 THEN SUM(D.Cost * Qty) ELSE SUM(A.TotalCost - A.AdjustedCost) END, BalanceCost = CASE WHEN Item.CostingMethod = 0 THEN SUM(D.Cost * Qty) ELSE SUM(A.TotalCost - A.AdjustedCost) END, Item.ItemGroup, Item.ItemType, Item.Description, Item.Desc2, Item.ReportUOM, Item.CostingMethod, Location.Description AS LocationDesc, Location.Desc2 AS LocationDesc2, CostingMethodString = CASE WHEN Item.CostingMethod = 0 THEN 'Fixed Cost' WHEN Item.CostingMethod = 1 THEN 'Weighted Average' WHEN Item.CostingMethod = 2 THEN 'FIFO' WHEN Item.CostingMethod = 3 THEN 'LIFO' ELSE 'Most Recently' END , D.Rate, D.Shelf ";
            strArray[index1] = str3;
            int index2 = 1;
            string str4 = this.IncludeConsignment('A', 'E', 'F');
            strArray[index2] = str4;
            int index3 = 2;
            string str5 = "FROM RPA_TransRawMaterial A LEFT OUTER JOIN Item ON A.ItemCode = Item.ItemCode INNER JOIN Location ON A.Location = Location.Location ";
            strArray[index3] = str5;
            int index4 = 3;
            string str6 = this.JoinSqlGenerator('A', 'C');
            strArray[index4] = str6;
            //int index5 = 4;
            //string str7 = this.Join2SqlGenerator('A', 'H');
            //strArray[index5] = str7;
            int index6 = 4;
            string str8 = "LEFT OUTER JOIN ItemUOM D ON Item.ItemCode = D.ItemCode AND A.UOM = D.UOM WHERE A.DocDate < @FromDate ";
            strArray[index6] = str8;
            int index7 = 5;
            string str9 = this.WhereMasterSqlGenerator('A', 'C', "Item");
            strArray[index7] = str9;
            int index8 = 6;
            string str10 = str1;
            strArray[index8] = str10;
            int index9 = 7;
            string str11 = str2;
            strArray[index9] = str11;
            int index10= 8;
            string str12 = "AND Item.StockControl = 'T' GROUP BY A.WONo,A.Location, A.ItemCode, A.UOM, Item.ItemGroup, Item.ItemType, Item.Description, Item.Desc2, Item.ReportUOM, Item.CostingMethod, Location.Description, Location.Desc2 , D.Rate, D.Shelf ORDER BY A.ItemCode, A.Location ";
            strArray[index10] = str12;
            string str13 = string.Concat(strArray);
            sqlCommand.CommandText = str13;
            sqlCommand.CommandTimeout = this.myCommandTimeOut;
            try
            {
                if (this.myFromDate.EditValue != null)
                    sqlCommand.Parameters.AddWithValue("@FromDate", (object)this.myFromDate.DateTime.Date);
                else
                    sqlCommand.Parameters.AddWithValue("@FromDate", (object)FiscalYear.GetOrCreate(this.myDBSetting).ActualDataStartPeriod.Date);
                if (this.myToDate.EditValue != null)
                    sqlCommand.Parameters.AddWithValue("@ToDate", (object)this.myToDate.DateTime.Date);
                sqlConnection.Open();
                new SqlDataAdapter(sqlCommand).Fill(this.myResultDataSet, "Master");
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            if (this.IsIncludeNonTransactionItem)
                this.QueryNonTransactionTable();
           // this.SetBatchNoColumn("Master");
            DataColumn[] dataColumnArray = new DataColumn[4];
            int index11 = 0;
            DataColumn dataColumn1 = this.myResultDataSet.Tables["Master"].Columns["ItemCode"];
            dataColumnArray[index11] = dataColumn1;
            int index12 = 1;
            DataColumn dataColumn2 = this.myResultDataSet.Tables["Master"].Columns["Location"];
            dataColumnArray[index12] = dataColumn2;
            //int index13 = 2;
            //DataColumn dataColumn3 = this.myResultDataSet.Tables["Master"].Columns["BatchNo"];
            //dataColumnArray[index13] = dataColumn3;
            int index14 =2;
            DataColumn dataColumn4 = this.myResultDataSet.Tables["Master"].Columns["UOM"];
            dataColumnArray[index14] = dataColumn4;
            int index15 =3;
            DataColumn dataColumn5 = this.myResultDataSet.Tables["Master"].Columns["WONo"];
            dataColumnArray[index15] = dataColumn5;
            //int index16 = 5;
            //DataColumn dataColumn6 = this.myResultDataSet.Tables["Master"].Columns["ProductCode"];
            //dataColumnArray[index16] = dataColumn6;
            this.myResultDataSet.Tables["Master"].PrimaryKey = dataColumnArray;
        }

        protected void QueryNonTransactionTable()
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            string str1 = this.GetItemWhereSql(sqlCommand);
            if (str1.Length != 0)
                str1 = "AND " + str1;
            string str2 = this.GetLocationWhereSql(sqlCommand);
            if (str2.Length != 0)
                str2 = "AND " + str2;
            string[] strArray = new string[5];
            int index1 = 0;
            string str3 = "SELECT A.Location, A.ItemCode, A.UOM, Item.ItemGroup, Item.ItemType, Item.Description, Item.Desc2, Item.ReportUOM, Item.CostingMethod, A.BatchNo, A.ExpiryDate, Item.Description AS ItemBatchDescription, Location.Description AS LocationDesc, Location.Desc2 AS LocationDesc2, CostingMethodString = CASE WHEN Item.CostingMethod = 0 THEN 'Fixed Cost' WHEN Item.CostingMethod = 1 THEN 'Weighted Average' WHEN Item.CostingMethod = 2 THEN 'FIFO' WHEN Item.CostingMethod = 3 THEN 'LIFO' ELSE 'Most Recently' END , A.Rate, A.Shelf FROM (Select u.*, (SELECT SUM(s.Qty) FROM StockDtl s LEFT OUTER JOIN ItemBatch bc ON bc.ItemCode = s.ItemCode AND bc.BatchNo = s.BatchNo WHERE s.DocDate < @FromDate And s.ItemCode = i.ItemCode AND s.UOM = u.UOM AND ISNULL(s.BatchNo,'') = ISNULL(u.BatchNo,'') AND s.Location = u.Location GROUP BY s.ItemCode, s.UOM, s.BatchNo, s.Location) AS AvailableQty FROM (SELECT x.ItemCode, x.UOM, x.Rate, x.Shelf, ba.BatchNo, ba.ExpiryDate, l.Location from location l, itemuom x, itembatch ba where  x.itemcode=ba.itemcode union all SELECT x.ItemCode, x.UOM, x.Rate, x.Shelf, null as BatchNo, null as ExpiryDate, l.Location from Location l, ItemUom x) u LEFT OUTER JOIN Item i ON i.ItemCode=u.ItemCode WHERE i.StockControl = 'T') A LEFT OUTER JOIN Location ON Location.Location = A.Location LEFT OUTER JOIN Item ON Item.ItemCode = A.ItemCode Where A.AvailableQty is NULL ";
            strArray[index1] = str3;
            int index2 = 1;
            string str4 = this.WhereMasterSqlGenerator('A', 'A', "Item", false);
            strArray[index2] = str4;
            int index3 = 2;
            string str5 = str1;
            strArray[index3] = str5;
            int index4 = 3;
            string str6 = str2;
            strArray[index4] = str6;
            int index5 = 4;
            string str7 = "ORDER BY A.ItemCode, A.Location, A.BatchNo";
            strArray[index5] = str7;
            string str8 = string.Concat(strArray);
            sqlCommand.CommandText = str8;
            sqlCommand.CommandTimeout = this.myCommandTimeOut;
            try
            {
                if (this.myFromDate.EditValue != null)
                    sqlCommand.Parameters.AddWithValue("@FromDate", (object)this.myFromDate.DateTime.Date);
                else
                    sqlCommand.Parameters.AddWithValue("@FromDate", (object)FiscalYear.GetOrCreate(this.myDBSetting).ActualDataStartPeriod.Date);
                if (this.myToDate.EditValue != null)
                    sqlCommand.Parameters.AddWithValue("@ToDate", (object)this.myToDate.DateTime.Date);
                sqlConnection.Open();
                new SqlDataAdapter(sqlCommand).Fill(this.myResultDataSet, "Master");
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        protected override void QueryDetailTable()
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            string str1 = this.GetItemWhereSql(sqlCommand);
            if (str1.Length != 0)
                str1 = "AND " + str1;
            string str2 = this.GetLocationWhereSql(sqlCommand);
            if (str2.Length != 0)
                str2 = "AND " + str2;
            string str3 = string.Empty;
            string str4;
            if (!this.IsMergeSameCost)
            {
                string[] strArray = new string[15];
                int index1 = 0;
                string str5 = "SELECT A.ProductCode, H.Description as ProductName,A.WONo as DocNo,A.BOMCode,H.ItemType as ProductType,A.StockDTLKey, A.DocKey, A.ItemCode, A.UOM, A.UOM AS PastUOM, A.Location, A.ProjNo, A.DeptNo, A.DocDate, A.Seq, A.DocType, A.DtlKey, A.Qty, A.Qty AS PastQty, Cost = CASE WHEN Item.CostingMethod = 0 THEN E.Cost ELSE A.Cost END, AdjustedCost = CASE WHEN Item.CostingMethod = 0 THEN 0.0 ELSE A.AdjustedCost END, TotalCost = CASE WHEN Item.CostingMethod = 0 THEN E.Cost * A.Qty ELSE A.TotalCost END, A.CostType, A.LastModified, A.ReferTo, C.FIFOCostKey, C.Seq AS FIFOSeq, C.Qty AS FIFOQty, C.Cost AS FIFOCost, A.BatchNo, CostTypeString = CASE WHEN A.CostType = 0 THEN 'Use Fixed Cost' WHEN A.CostType = 1 THEN 'Use Most Recently Cost' WHEN A.CostType = 2 THEN 'Use UTD Cost' WHEN A.CostType = 3 THEN 'Use UTD And Most Recently Cost' WHEN A.CostType = 4 THEN 'New Input Cost' ELSE 'Use Refer Cost' END ";
                strArray[index1] = str5;
                int index2 = 1;
                string str6 = this.SelectClauseGenerator('E');
                strArray[index2] = str6;
                int index3 = 2;
                string str7 = "FROM RPA_TransRawMaterial A ";
                strArray[index3] = str7;
                int index4 = 3;
                string str8 = this.JoinSqlGenerator('A', 'B');
                strArray[index4] = str8;
                int index44 = 4;
                string str88 = this.Join2SqlGenerator('A', 'H');
                strArray[index44] = str88;
                int index5 = 5;
                string str9 = "LEFT OUTER JOIN FIFOCOST C ON A.StockDTLKey = C.StockDTLKey ";
                strArray[index5] = str9;
                int index6 = 6;
                string str10 = this.JoinSqlDetailItemGenerator('A', "Item");
                strArray[index6] = str10;
                int index7 = 7;
                string str11 = this.JoinSqlItemItemUOMGenerator("Item", 'E');
                strArray[index7] = str11;
                int index8 = 8;
                string str12 = "WHERE A.DocDate >= @FromDate ";
                strArray[index8] = str12;
                int index9 = 9;
                string str13 = this.WhereDetailSqlGenerator('A', 'B', "Item");
                strArray[index9] = str13;
                int index10 = 10;
                string str14 = str1;
                strArray[index10] = str14;
                int index11 = 11;
                string str15 = str2;
                strArray[index11] = str15;
                int index12 = 12;
                string str16 = "AND Item.StockControl = 'T' ORDER BY A.ItemCode, Location, A.BatchNo, ";
                strArray[index12] = str16;
                int index13 = 13;
                string str17 = this.OrderBySqlGenerator();
                strArray[index13] = str17;
                int index14 = 14;
                string str18 = "A.DocDate, A.Seq, A.StockDTLKey, C.Seq";
                strArray[index14] = str18;
                str4 = string.Concat(strArray);
            }
            else
            {
                string[] strArray = new string[28];
                int index1 = 0;
                string str5 = "SELECT A.ProductCode, H.Description as ProductName,A.WONo,A.BOMCode,H.ItemType as ProductType,A.StockDTLKey, A.DocKey, A.ItemCode, A.UOM, A.UOM AS PastUOM, A.Location, A.ProjNo, A.DeptNo, A.DocDate, A.Seq, A.DocType, A.DtlKey, A.Qty, A.Qty AS PastQty, A.Cost, A.AdjustedCost, A.TotalCost, A.CostType, A.LastModified, A.ReferTo, MIN(C.Seq) AS FIFOSeq, SUM(C.Qty) AS FIFOQty, C.Cost AS FIFOCost, A.BatchNo, CostTypeString = CASE WHEN A.CostType = 0 THEN 'Use Fixed Cost' WHEN A.CostType = 1 THEN 'Use Most Recently Cost' WHEN A.CostType = 2 THEN 'Use UTD Cost' WHEN A.CostType = 3 THEN 'Use UTD And Most Recently Cost' WHEN A.CostType = 4 THEN 'New Input Cost' ELSE 'Use Refer Cost' END ";
                strArray[index1] = str5;
                int index2 = 1;
                string str6 = this.SelectClauseGenerator('E');
                strArray[index2] = str6;
                int index3 = 2;
                string str7 = "FROM RPA_TransRawMaterial A ";
                strArray[index3] = str7;
                int index4 = 3;
                string str8 = this.JoinSqlGenerator('A', 'B');
                strArray[index4] = str8;
                int index441 = 4;
                string str881 = this.Join2SqlGenerator('A', 'H');
                strArray[index441] = str881;
                int index5 = 5;
                string str9 = "LEFT OUTER JOIN FIFOCOST C ON A.StockDTLKey = C.StockDTLKey ";
                strArray[index5] = str9;
                int index6 = 6;
                string str10 = this.JoinSqlDetailItemGenerator('A', "Item");
                strArray[index6] = str10;
                int index7 = 7;
                string str11 = this.JoinSqlItemItemUOMGenerator("Item", 'E', true);
                strArray[index7] = str11;
                int index8 = 8;
                string str12 = "WHERE A.DocDate >= @FromDate ";
                strArray[index8] = str12;
                int index9 = 9;
                string str13 = this.WhereDetailSqlGenerator('A', 'B', "Item", true);
                strArray[index9] = str13;
                int index10 = 10;
                string str14 = str1;
                strArray[index10] = str14;
                int index11 = 11;
                string str15 = str2;
                strArray[index11] = str15;
                int index12 = 12;
                string str16 = "AND Item.StockControl = 'T' AND Item.CostingMethod BETWEEN 2 AND 3 GROUP BY A.ProductCode, H.Description,A.WONo,A.BOMCode,H.ItemType,A.StockDTLKey, A.DocKey, A.ItemCode, A.UOM, A.Location, A.BatchNo, A.ProjNo, A.DeptNo, A.DocDate, A.Seq, A.DocType, A.DtlKey, A.Qty, A.Cost, A.AdjustedCost, A.TotalCost, A.CostType, A.LastModified, A.ReferTo, C.Cost ";
                strArray[index12] = str16;
                int index13 = 13;
                string str17 = this.SelectClauseGenerator('E');
                strArray[index13] = str17;
                int index14 = 14;
                string str18 = "UNION ALL SELECT  A.ProductCode, H.Description as ProductName,A.WONo,A.BOMCode,H.ItemType as ProductType,A.StockDTLKey, A.DocKey, A.ItemCode, A.UOM, A.UOM AS PastUOM, A.Location, A.ProjNo, A.DeptNo, A.DocDate, A.Seq, A.DocType, A.DtlKey, A.Qty, A.Qty AS PastQty, Cost = CASE WHEN Item.CostingMethod = 0 THEN E.Cost ELSE A.Cost END, AdjustedCost = CASE WHEN Item.CostingMethod = 0 THEN 0.0 ELSE A.AdjustedCost END, TotalCost = CASE WHEN Item.CostingMethod = 0 THEN E.Cost * A.Qty ELSE A.TotalCost END, A.CostType, A.LastModified, A.ReferTo, NULL AS FIFOSeq, NULL AS FIFOQty, NULL AS FIFOCost, A.BatchNo, CostTypeString = CASE WHEN A.CostType = 0 THEN 'Use Fixed Cost' WHEN A.CostType = 1 THEN 'Use Most Recently Cost' WHEN A.CostType = 2 THEN 'Use UTD Cost' WHEN A.CostType = 3 THEN 'Use UTD And Most Recently Cost' WHEN A.CostType = 4 THEN 'New Input Cost' ELSE 'Use Refer Cost' END ";
                strArray[index14] = str18;
                int index15 = 15;
                string str19 = this.SelectClauseGenerator('E');
                strArray[index15] = str19;
                int index16 = 16;
                string str20 = "FROM RPA_TransRawMaterial A ";
                strArray[index16] = str20;
                int index17 = 17;
                string str21 = this.JoinSqlGenerator('A', 'B');
                strArray[index17] = str21;
                int index177 = 18;
                string str211 = this.Join2SqlGenerator('A', 'H');
                strArray[index177] = str211;
                int index18 = 19;
                string str22 = this.JoinSqlDetailItemGenerator('A', "Item");
                strArray[index18] = str22;
                int index19 = 20;
                string str23 = this.JoinSqlItemItemUOMGenerator("Item", 'E');
                strArray[index19] = str23;
                int index20 = 21;
                string str24 = "WHERE A.DocDate >= @FromDate ";
                strArray[index20] = str24;
                int index21 = 22;
                string str25 = this.WhereDetailSqlGenerator('A', 'B', "Item");
                strArray[index21] = str25;
                int index22 = 23;
                string str26 = str1;
                strArray[index22] = str26;
                int index23 = 24;
                string str27 = str2;
                strArray[index23] = str27;
                int index24 = 25;
                string str28 = "AND Item.StockControl = 'T' AND Item.CostingMethod NOT BETWEEN 2 AND 3 ORDER BY A.ItemCode, Location, A.BatchNo, ";
                strArray[index24] = str28;
                int index25 = 26;
                string str29 = this.OrderBySqlGenerator();
                strArray[index25] = str29;
                int index26 = 27;
                string str30 = "A.DocDate, A.Seq, A.StockDTLKey, FIFOSeq";
                strArray[index26] = str30;
                str4 = string.Concat(strArray);
            }
            sqlCommand.CommandText = str4;
            sqlCommand.CommandTimeout = this.myCommandTimeOut;
            try
            {
                if (this.myFromDate.EditValue != null)
                    sqlCommand.Parameters.AddWithValue("@FromDate", (object)this.myFromDate.DateTime.Date);
                else
                    sqlCommand.Parameters.AddWithValue("@FromDate", (object)FiscalYear.GetOrCreate(this.myDBSetting).ActualDataStartPeriod.Date);
                if (this.myToDate.EditValue != null)
                    sqlCommand.Parameters.AddWithValue("@ToDate", (object)this.myToDate.DateTime.Date);
                sqlConnection.Open();
                new SqlDataAdapter(sqlCommand).Fill(this.myResultDataSet, "Detail");
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            this.SetBatchNoColumn("Detail");
        }

        protected override void QueryMissingMasterTable()
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            string str1 = this.GetItemWhereSql(sqlCommand);
            if (str1.Length != 0)
                str1 = "AND " + str1;
            string str2 = this.GetLocationWhereSql(sqlCommand);
            if (str2.Length != 0)
                str2 = "AND " + str2;
            string[] strArray = new string[6];
            int index1 = 0;
            string str3 = "SELECT DISTINCT A.WONo,A.Location, Item.ItemCode, A.UOM, 0.0 AS Balance, 0.0 AS PastBalance, 0.0 AS BalanceQty, 0.0 AS TotalCost, 0.0 AS BalanceCost, Item.ItemGroup, Item.ItemType, Item.Description, Item.Desc2, Item.ReportUOM, Item.CostingMethod, Location.Description AS LocationDesc, Location.Desc2 AS LocationDesc2 , D.Shelf, CostingMethodString = CASE WHEN Item.CostingMethod = 0 THEN 'Fixed Cost' WHEN Item.CostingMethod = 1 THEN 'Weighted Average' WHEN Item.CostingMethod = 2 THEN 'FIFO' WHEN Item.CostingMethod = 3 THEN 'LIFO' ELSE 'Most Recently' END ";
            strArray[index1] = str3;
            int index2 = 1;
            string str4 = this.IncludeConsignment('A', 'B', 'C');
            strArray[index2] = str4;
            int index3 = 2;
            string str5 = "FROM RPA_TransRawMaterial A LEFT OUTER JOIN Item ON A.ItemCode = Item.ItemCode INNER JOIN Location ON A.Location = Location.Location LEFT OUTER JOIN ItemUOM D ON A.UOM = D.UOM AND A.ItemCode = D.ItemCode WHERE A.DocDate >= @FromDate AND A.DocDate <= @ToDate ";
            strArray[index3] = str5;
            int index4 = 3;
            string str6 = str1;
            strArray[index4] = str6;
            int index5 = 4;
            string str7 = str2;
            strArray[index5] = str7;
            int index6 = 5;
            string str8 = "AND Item.StockControl = 'T' ";
            strArray[index6] = str8;
            string str9 = string.Concat(strArray);
            if (this.myFromDate.EditValue != null)
                sqlCommand.Parameters.AddWithValue("@FromDate", (object)this.myFromDate.DateTime.Date);
            else
                sqlCommand.Parameters.AddWithValue("@FromDate", (object)FiscalYear.GetOrCreate(this.myDBSetting).ActualDataStartDate);
            if (this.myToDate.EditValue != null)
                sqlCommand.Parameters.AddWithValue("@ToDate", (object)this.myToDate.DateTime.Date);
            if (this.IsPrintActive && !this.IsPrintInactive)
                str9 = str9 + "AND Item.IsActive = 'T' ";
            else if (!this.IsPrintActive && this.IsPrintInactive)
                str9 = str9 + "AND Item.IsActive = 'F' ";
            sqlCommand.CommandText = str9;
            sqlCommand.CommandTimeout = this.myCommandTimeOut;
            try
            {
                sqlConnection.Open();
                new SqlDataAdapter(sqlCommand).Fill(this.myResultDataSet, "MasterMissing");
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            //this.SetBatchNoColumn("MasterMissing");
            DataColumn[] dataColumnArray = new DataColumn[4];
            int index7 = 0;
            DataColumn dataColumn1 = this.myResultDataSet.Tables["MasterMissing"].Columns["ItemCode"];
            dataColumnArray[index7] = dataColumn1;
            int index8 = 1;
            DataColumn dataColumn2 = this.myResultDataSet.Tables["MasterMissing"].Columns["Location"];
            dataColumnArray[index8] = dataColumn2;
            //int index9 = 2;
            //DataColumn dataColumn3 = this.myResultDataSet.Tables["MasterMissing"].Columns["BatchNo"];
            //dataColumnArray[index9] = dataColumn3;
            int index10 = 2;
            DataColumn dataColumn4 = this.myResultDataSet.Tables["MasterMissing"].Columns["UOM"];
            dataColumnArray[index10] = dataColumn4;
            int index11 = 3;
            DataColumn dataColumn5= this.myResultDataSet.Tables["MasterMissing"].Columns["WONo"];
            dataColumnArray[index11] = dataColumn5;
            //int index12 = 5;
            //DataColumn dataColumn6 = this.myResultDataSet.Tables["MasterMissing"].Columns["ProductCode"];
            //dataColumnArray[index12] = dataColumn6;
            this.myResultDataSet.Tables["MasterMissing"].PrimaryKey = dataColumnArray;
        }

        protected override void QueryUOMOptions()
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            string str1 = this.GetItemWhereSql(sqlCommand);
            if (str1.Length != 0)
                str1 = "AND " + str1;
            string str2 = this.GetLocationWhereSql(sqlCommand);
            if (str2.Length != 0)
                str2 = "AND " + str2;
            string str3 = string.Empty;
            string str4 = this.myUOMOption != ShowUOMOption.ShowDefaultReportUOM ? "SELECT DISTINCT A.Location, A.ItemCode, C.UOM, C.Rate, A.BatchNo FROM RPA_TransRawMaterial A LEFT OUTER JOIN Item ON A.ItemCode = Item.ItemCode INNER JOIN ItemUOM C ON Item.ItemCode = C.ItemCode WHERE C.UOM = Item.BaseUOM " + str1 + str2 + "AND Item.StockControl = 'T' " : "SELECT DISTINCT A.Location, A.ItemCode, Item.ReportUOM AS UOM, C.Rate, A.BatchNo FROM RPA_TransRawMaterial A LEFT OUTER JOIN Item ON A.ItemCode = Item.ItemCode INNER JOIN ItemUOM C ON Item.ItemCode = C.ItemCode WHERE Item.ReportUOM = C.UOM " + str1 + str2 + "AND Item.StockControl = 'T' ";
            if (this.IsPrintActive && !this.IsPrintInactive)
                str4 = str4 + "AND Item.IsActive = 'T' ";
            else if (!this.IsPrintActive && this.IsPrintInactive)
                str4 = str4 + "AND Item.IsActive = 'F' ";
            sqlCommand.CommandText = str4;
            sqlCommand.CommandTimeout = this.myCommandTimeOut;
            try
            {
                sqlConnection.Open();
                new SqlDataAdapter(sqlCommand).Fill(this.myResultDataSet, "UOMRate");
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            this.SetBatchNoColumn("UOMRate");
            DataColumn[] dataColumnArray = new DataColumn[3];
            int index1 = 0;
            DataColumn dataColumn1 = this.myResultDataSet.Tables["UOMRate"].Columns["Location"];
            dataColumnArray[index1] = dataColumn1;
            int index2 = 1;
            DataColumn dataColumn2 = this.myResultDataSet.Tables["UOMRate"].Columns["ItemCode"];
            dataColumnArray[index2] = dataColumn2;
            int index3 = 2;
            DataColumn dataColumn3 = this.myResultDataSet.Tables["UOMRate"].Columns["BatchNo"];
            dataColumnArray[index3] = dataColumn3;
            this.myResultDataSet.Tables["UOMRate"].PrimaryKey = dataColumnArray;
        }

        protected override void QueryUOMNonTransactionOptions()
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            string str1 = this.GetItemWhereSql(sqlCommand);
            if (str1.Length != 0)
                str1 = "AND " + str1;
            string str2 = this.GetLocationWhereSql(sqlCommand);
            if (str2.Length != 0)
                str2 = "AND " + str2;
            string str3 = string.Empty;
            string str4 = this.myUOMOption != ShowUOMOption.ShowDefaultReportUOM ? "SELECT DISTINCT A.Location, A.ItemCode, A.UOM, A.Rate, A.BatchNo From (SELECT x.ItemCode, x.UOM, x.Rate, x.Shelf, ba.BatchNo, ba.ExpiryDate, l.Location from location l, itemuom x, itembatch ba, Item i where x.itemcode=ba.itemcode and x.itemcode=i.itemcode and x.uom=i.baseuom union all SELECT x.ItemCode, x.UOM, x.Rate, x.Shelf, null as BatchNo, null as ExpiryDate, l.Location from Location l, ItemUom x, Item i Where x.itemcode=i.itemcode and x.uom=i.baseuom)A LEFT OUTER JOIN Item ON Item.ItemCode=A.ItemCode WHERE Item.StockControl = 'T' " + str1 + str2 : "SELECT DISTINCT A.Location, A.ItemCode, A.UOM, A.Rate, A.BatchNo From (SELECT x.ItemCode, x.UOM, x.Rate, x.Shelf, ba.BatchNo, ba.ExpiryDate, l.Location from location l, itemuom x, itembatch ba, Item i where x.itemcode=ba.itemcode and x.itemcode=i.itemcode and x.uom=i.reportuom union all SELECT x.ItemCode, x.UOM, x.Rate, x.Shelf, null as BatchNo, null as ExpiryDate, l.Location from Location l, ItemUom x, Item i Where x.itemcode=i.itemcode and x.uom=i.reportuom)A LEFT OUTER JOIN Item ON Item.ItemCode=A.ItemCode WHERE Item.StockControl = 'T' " + str1 + str2;
            if (this.IsPrintActive && !this.IsPrintInactive)
                str4 = str4 + " AND Item.IsActive = 'T' ";
            else if (!this.IsPrintActive && this.IsPrintInactive)
                str4 = str4 + " AND Item.IsActive = 'F' ";
            sqlCommand.CommandText = str4;
            sqlCommand.CommandTimeout = this.myCommandTimeOut;
            try
            {
                sqlConnection.Open();
                new SqlDataAdapter(sqlCommand).Fill(this.myResultDataSet, "UOMRate");
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            this.SetBatchNoColumn("UOMRate");
            DataColumn[] dataColumnArray = new DataColumn[3];
            int index1 = 0;
            DataColumn dataColumn1 = this.myResultDataSet.Tables["UOMRate"].Columns["Location"];
            dataColumnArray[index1] = dataColumn1;
            int index2 = 1;
            DataColumn dataColumn2 = this.myResultDataSet.Tables["UOMRate"].Columns["ItemCode"];
            dataColumnArray[index2] = dataColumn2;
            int index3 = 2;
            DataColumn dataColumn3 = this.myResultDataSet.Tables["UOMRate"].Columns["BatchNo"];
            dataColumnArray[index3] = dataColumn3;
            this.myResultDataSet.Tables["UOMRate"].PrimaryKey = dataColumnArray;
        }

        protected override void QueryMissingCSGN()
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            string str1 = this.GetItemWhereSql(sqlCommand);
            if (str1.Length != 0)
                str1 = "AND " + str1;
            string str2 = this.GetLocationWhereSql(sqlCommand);
            if (str2.Length != 0)
                str2 = "AND " + str2;
            string[] strArray = new string[23];
            int index1 = 0;
            string str3 = "SELECT A.Location, A.ItemCode, C.UOM, A.BatchNo, SUM(A.Qty ";
            strArray[index1] = str3;
            int index2 = 1;
            string uomMethod1 = this.GetUomMethod("u1", "C");
            strArray[index2] = uomMethod1;
            int index3 = 2;
            string str4 = " AS CSGNBalQty, 0.0 AS BalQty, 0.0 AS Balance, 0.0 AS PastBalance, 0.0 AS BalanceQty, 0.0 AS TotalCost, 0.0 AS BalanceCost, Item.ItemGroup, Item.ItemType, Item.Description, Item.Desc2, Item.ReportUOM, Item.CostingMethod, Location.Description AS LocationDesc, Location.Desc2 AS LocationDesc2, C.Cost AS AverageBalanceCost, CostingMethodString = CASE WHEN Item.CostingMethod = 0 THEN 'Fixed Cost' WHEN Item.CostingMethod = 1 THEN 'Weighted Average' WHEN Item.CostingMethod = 2 THEN 'FIFO' WHEN Item.CostingMethod = 3 THEN 'LIFO' ELSE 'Most Recently' END FROM (SELECT B.ItemCode, B.Location, B.UOM, B.BatchNo, B.Qty FROM (SELECT Distinct A.ItemCode, A.Location, A.UOM, A.BatchNo FROM CSGNDTL A INNER JOIN CSGN B ON A.DocKey = B.DocKey INNER JOIN Item ON A.ItemCode = Item.ItemCode  inner join RPA_TransRawMaterial AA ON Item.ItemCode=AA.ItemCode LEFT OUTER JOIN ItemBatch C ON A.BatchNo = C.BatchNo WHERE B.DocDate <= @ToDate AND B.Cancelled = 'F' AND Item.StockControl = 'T' ";
            strArray[index3] = str4;
            int index4 = 3;
            string str5 = str1;
            strArray[index4] = str5;
            int index5 = 4;
            string str6 = str2;
            strArray[index5] = str6;
            int index6 = 5;
            string str7 = this.WhereMasterSqlGenerator('A', 'C', "Item", false);
            strArray[index6] = str7;
            int index7 = 6;
            string str8 = "AND NOT EXISTS (SELECT 1 FROM (SELECT Distinct A.ItemCode, A.Location, A.UOM, A.BatchNo FROM RPA_TransRawMaterial A INNER JOIN Item ON A.ItemCode = Item.ItemCode LEFT OUTER JOIN ItemBatch C ON A.BatchNo = C.BatchNo WHERE A.DocDate <= @ToDate AND Item.StockControl = 'T' ";
            strArray[index7] = str8;
            int index8 = 7;
            string str9 = str1;
            strArray[index8] = str9;
            int index9 = 8;
            string str10 = str2;
            strArray[index9] = str10;
            int index10 = 9;
            string str11 = this.WhereMasterSqlGenerator('A', 'C', "Item", false);
            strArray[index10] = str11;
            int index11 = 10;
            string str12 = ")S WHERE ISNull(A.ItemCode, '') = ISNull(S.ItemCode, '') AND ISNull(A.Location, '') = ISNull(S.Location, '') AND ISNull(A.UOM, '') = ISNull(S.UOM, '') AND ISNull(A.BatchNo, '') = ISNull(S.BatchNo, '')) ) A INNER JOIN CSGNDtl B ON A.ItemCode = B.ItemCode AND A.Location = B.Location AND A.UOM = B.UOM AND ((A.BatchNo IS NULL AND B.BatchNo IS NULL) OR A.BatchNo = B.BatchNo) INNER JOIN CSGN C ON B.DocKey = C.DocKey AND C.Cancelled = 'F' UNION ALL SELECT B.ItemCode, B.Location, B.UOM, B.BatchNo, B.Qty FROM (SELECT Distinct A.ItemCode, A.Location, A.UOM, A.BatchNo FROM SupplierCSGNDTL A INNER JOIN SupplierCSGN B ON A.DocKey = B.DocKey INNER JOIN Item ON A.ItemCode = Item.ItemCode LEFT OUTER JOIN ItemBatch C ON A.BatchNo = C.BatchNo WHERE B.DocDate <= @ToDate AND B.Cancelled = 'F' AND Item.StockControl = 'T' ";
            strArray[index11] = str12;
            int index12 = 11;
            string str13 = str1;
            strArray[index12] = str13;
            int index13 = 12;
            string str14 = str2;
            strArray[index13] = str14;
            int index14 = 13;
            string str15 = this.WhereMasterSqlGenerator('A', 'C', "Item", false);
            strArray[index14] = str15;
            int index15 = 14;
            string str16 = "AND NOT EXISTS (SELECT 1 FROM (SELECT Distinct A.ItemCode, A.Location, A.UOM, A.BatchNo FROM RPA_TransRawMaterial A INNER JOIN Item ON A.ItemCode = Item.ItemCode LEFT OUTER JOIN ItemBatch C ON A.BatchNo = C.BatchNo WHERE A.DocDate <= @ToDate AND Item.StockControl = 'T' ";
            strArray[index15] = str16;
            int index16 = 15;
            string str17 = str1;
            strArray[index16] = str17;
            int index17 = 16;
            string str18 = str2;
            strArray[index17] = str18;
            int index18 = 17;
            string str19 = this.WhereMasterSqlGenerator('A', 'C', "Item", false);
            strArray[index18] = str19;
            int index19 = 18;
            string str20 = ")S WHERE ISNull(A.ItemCode, '') = ISNull(S.ItemCode, '') AND ISNull(A.Location, '') = ISNull(S.Location, '') AND ISNull(A.UOM, '') = ISNull(S.UOM, '') AND ISNull(A.BatchNo, '') = ISNull(S.BatchNo, '')) ) A INNER JOIN SupplierCSGNDtl B ON A.ItemCode = B.ItemCode AND A.Location = B.Location AND A.UOM = B.UOM AND ((A.BatchNo IS NULL AND B.BatchNo IS NULL) OR A.BatchNo = B.BatchNo) INNER JOIN SupplierCSGN C ON B.DocKey = C.DocKey AND C.Cancelled = 'F' ) A INNER JOIN Item ON A.ItemCode = Item.ItemCode ";
            strArray[index19] = str20;
            int index20 = 19;
            string str21 = this.MissingConsignmentHelper();
            strArray[index20] = str21;
            int index21 = 20;
            string str22 = "INNER JOIN Location ON A.Location = Location.Location GROUP BY A.Location, A.ItemCode, C.UOM, A.BatchNo, Item.ItemGroup, Item.ItemType, Item.Description, Item.Desc2, Item.ReportUOM, Item.CostingMethod, Location.Description, Location.Desc2, Item.CostingMethod, C.Cost HAVING SUM(A.Qty ";
            strArray[index21] = str22;
            int index22 = 21;
            string uomMethod2 = this.GetUomMethod("u1", "C");
            strArray[index22] = uomMethod2;
            int index23 = 22;
            string str23 = " != 0 ";
            strArray[index23] = str23;
            string str24 = string.Concat(strArray);
            sqlCommand.CommandText = str24;
            sqlCommand.CommandTimeout = this.myCommandTimeOut;
            if (this.myFromDate.EditValue != null)
                sqlCommand.Parameters.AddWithValue("@FromDate", (object)this.myFromDate.DateTime.Date);
            else
                sqlCommand.Parameters.AddWithValue("@FromDate", (object)FiscalYear.GetOrCreate(this.myDBSetting).ActualDataStartDate);
            sqlCommand.Parameters.AddWithValue("@ToDate", (object)this.myToDate.DateTime.Date);
            try
            {
                sqlConnection.Open();
                new SqlDataAdapter(sqlCommand).Fill(this.myResultDataSet, "MissingCSGN");
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            this.SetBatchNoColumn("MissingCSGN");
        }

        protected override void QueryItemSNTable()
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            string str1 = this.GetLocationWhereSql(sqlCommand);
            if (str1.Length != 0)
                str1 = "AND " + str1;
            string str2 = "SELECT B.SerialNumber, B.ItemCode, B.BatchNo, B.ManufacturedDate, B.ExpiryDate, B.LastSalesDate, B.Remarks, A.Location, SUM(ISNULL(A.Qty, 0)) AS Qty, SUM(ISNULL(A.CSGNQty, 0)) AS CSGNQty FROM ItemSerialNo B INNER JOIN ItemSerialNoDtl A ON B.SerialNumber = A.SerialNumber AND B.ItemCode = A.ItemCode WHERE B.ItemCode IN (SELECT * FROM LIST2(@ItemCode)) " + str1 + "GROUP BY B.SerialNumber, B.ItemCode, B.BatchNo, B.ManufacturedDate, B.ExpiryDate, B.LastSalesDate, B.Remarks, A.Location HAVING SUM(ISNULL(A.Qty, 0)) != 0 OR SUM(ISNULL(A.CSGNQty, 0)) != 0 ";
            string str3 = "";
            string aStr = "";
            foreach (DataRow dataRow in (InternalDataCollectionBase)this.myResultDataSet.Tables["Master"].Rows)
            {
                if (aStr != dataRow["ItemCode"].ToString())
                {
                    aStr = dataRow["ItemCode"].ToString();
                    str3 = str3 + StringHelper.ToSingleQuoteString(aStr) + "\n\r";
                }
            }
            sqlCommand.CommandText = str2;
            sqlCommand.CommandTimeout = this.myCommandTimeOut;
            sqlCommand.Parameters.AddWithValue("@ItemCode", (object)str3);
            try
            {
                sqlConnection.Open();
                new SqlDataAdapter(sqlCommand).Fill(this.myResultDataSet, "ItemSerialNo");
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            this.SetBatchNoColumn("ItemSerialNo");
        }
    }

}
