﻿// Type: BCE.AutoCount.Stock.StockTrans
// Assembly: BCE.AutoCount, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.DLL

using BCE.AutoCount.Settings;
using BCE.Data;
using System;
using System.Data;
using BCE.AutoCount.Stock;
namespace Production.StockCard
{
    public class StockTrans
    {
        private string myDocType;
        private long myDocKey;
        private DataTable myDataTableStockTrans;
        private DecimalSetting myDecimalSetting;

        internal string DocType
        {
            get
            {
                return this.myDocType;
            }
        }

        internal long DocKey
        {
            get
            {
                return this.myDocKey;
            }
        }

        internal DataTable StockTransTable
        {
            get
            {
                return this.myDataTableStockTrans;
            }
        }

        public StockTrans(DBSetting dbSetting, string docType, long docKey)
        {
            this.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
            this.myDocType = docType;
            this.myDocKey = docKey;
            this.myDataTableStockTrans = new DataTable();
            this.myDataTableStockTrans.Columns.Add("DtlKey", Type.GetType("System.Int64"));
            this.myDataTableStockTrans.Columns.Add("ProductCode", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("DocNo", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("WONo", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("BOMCode", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("ItemCode", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("UOM", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("Location", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("BatchNo", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("BatchNoProduct", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("ProjNo", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("DeptNo", Type.GetType("System.String"));
            this.myDataTableStockTrans.Columns.Add("DocDate", Type.GetType("System.DateTime"));
            this.myDataTableStockTrans.Columns.Add("Qty", Type.GetType("System.Decimal"));
            this.myDataTableStockTrans.Columns.Add("Cost", Type.GetType("System.Decimal"));
            this.myDataTableStockTrans.Columns.Add("ReferTo", Type.GetType("System.Int64"));
            this.myDataTableStockTrans.Columns.Add("TotalCost", Type.GetType("System.Decimal"));
            DataTable dataTable = this.myDataTableStockTrans;
            DataColumn[] dataColumnArray = new DataColumn[1];
            int index = 0;
            DataColumn dataColumn = this.myDataTableStockTrans.Columns["DtlKey"];
            dataColumnArray[index] = dataColumn;
            dataTable.PrimaryKey = dataColumnArray;
        }

        public void Add(long dtlKey, string docNo, string woNo, string productCode, string bomCode, string itemCode, string UOM, string location, object batchNo, object batchNoProduct, object projNo, object deptNo, DateTime docDate, Decimal qty, Decimal cost, long referTo)
        {
            DataRow row = this.myDataTableStockTrans.NewRow();
            row["DtlKey"] = (object)dtlKey;
            row["ItemCode"] = (object)itemCode;
            row["DocNo"] = (object)docNo;
            row["WONo"] = (object)woNo;
            row["ProductCode"] = (object)productCode;
            row["BOMCode"] = (object)bomCode;
            row["UOM"] = (object)UOM;
            row["Location"] = (object)location;
            row["BatchNo"] = batchNo;
            row["BatchNoProduct"] = batchNoProduct;

            row["ProjNo"] = projNo;
            row["DeptNo"] = deptNo;
            row["DocDate"] = (object)docDate.Date;
            row["Qty"] = (object)this.myDecimalSetting.RoundQuantity(qty);
            row["Cost"] = (object)this.myDecimalSetting.RoundCost(cost);
            row["ReferTo"] = (object)referTo;
            this.myDataTableStockTrans.Rows.Add(row);
        }

        public bool GetTotalCost(long dtlKey, ref Decimal totalCost)
        {
            DataRow dataRow = this.myDataTableStockTrans.Rows.Find((object)dtlKey);
            if (dataRow != null && dataRow["TotalCost"] != DBNull.Value)
            {
                totalCost = this.myDecimalSetting.RoundCost(dataRow["TotalCost"]);
                return true;
            }
            else
                return false;
        }
    }
}
