﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.FormMultiSelectItemBOM
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using BCE.Localization;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Production.StockCard
{
  public class FormMultiSelectWODateFilter: XtraForm
  {
    private DBSetting myDBSetting;
    private BCE.AutoCount.SearchFilter.Filter myFilter;
    private WOFilterDateBuilder myLookupEditBuilder;
    private DataTable myDataTable;
    private IContainer components;
    private ComboBoxEdit cbEdtFilter;
    private RepositoryItemCheckEdit repositoryItemCheckEdit_Check;
    private SimpleButton sbtnCancel;
    private SimpleButton sbtnOK;
    private SimpleButton sbtnSelectAll;
    private SimpleButton sbtnUnselectAll;
    private GridColumn colDescription;
    private GridColumn gridColCheck;
    private GridControl gridControl1;
    private GridView gridView1;
    private Label label1;
    private Label lblSelectedCount;
        private GridColumn colCheck;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private GridColumn colDocDate;
        private GridColumn colDocNo;
        private Panel panel1;

    public FormMultiSelectWODateFilter(DBSetting dbSetting, BCE.AutoCount.SearchFilter.Filter filter)
    {
      this.InitializeComponent();
      this.Icon = BCE.AutoCount.Application.Icon;
      this.myFilter = filter;
      this.myDBSetting = dbSetting;
      this.myLookupEditBuilder = new WOFilterDateBuilder();
      this.myDataTable = this.myLookupEditBuilder.BuildDataTable(dbSetting);
      this.gridControl1.DataSource = (object) this.myDataTable;
      this.myDataTable.Columns.Add(new DataColumn("Check", System.Type.GetType("System.Boolean")));
    }

    private void FormParameterCustom_Load(object sender, EventArgs e)
    {
      if (this.myFilter != null)
      {
        for (int index = 0; index < this.myFilter.Count; ++index)
        {
          string str = (string) this.myFilter[index];
          foreach (DataRow dataRow in (InternalDataCollectionBase) this.myDataTable.Rows)
          {
            if (dataRow["DocNo"].ToString() == str)
              dataRow["Check"] = (object) true;
          }
        }
        this.UpdateSelectedCount();
      }
    }

    private void gridView1_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
    {
      int[] selectedRows = this.gridView1.GetSelectedRows();
      if (selectedRows != null && selectedRows.Length > 1)
      {
        for (int index = 0; index < selectedRows.Length; ++index)
          this.gridView1.GetDataRow(selectedRows[index])["Check"] = (object) true;
        this.UpdateSelectedCount();
      }
    }

    private void sbtnOK_Click(object sender, EventArgs e)
    {
      GridViewUtils.UpdateData(this.gridView1);
      DataRow[] dataRowArray = this.myDataTable.Select("Check = true");
      if (dataRowArray.Length == 0)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this,BCE.Localization.Localizer.GetString((Enum)Production.GeneralMaintenance.ItemBOM.ItemBOMMaintStringId.ErrorMessage_RecordNotSelected, new object[0]));
        this.DialogResult = DialogResult.None;
      }
      else
      {
        this.myFilter.Clear();
        foreach (DataRow dataRow in dataRowArray)
          this.myFilter.Add((object) dataRow["DocNo"].ToString());
      }
    }

    private void sbtnUnselectAll_Click(object sender, EventArgs e)
    {
      this.gridView1.BeginUpdate();
      foreach (DataRow dataRow in this.myDataTable.Select(this.myDataTable.DefaultView.RowFilter))
        dataRow["Check"] = (object) false;
      this.gridView1.EndUpdate();
      this.UpdateSelectedCount();
    }

    private void sbtnSelectAll_Click(object sender, EventArgs e)
    {
      this.gridView1.BeginUpdate();
      foreach (DataRow dataRow in this.myDataTable.Select(this.myDataTable.DefaultView.RowFilter))
        dataRow["Check"] = (object) true;
      this.gridView1.EndUpdate();
      this.UpdateSelectedCount();
    }

    private void gridView1_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
      {
        e.Handled = true;
        DataRow dataRow = this.gridView1.GetDataRow(this.gridView1.FocusedRowHandle);
        if (dataRow != null)
        {
          dataRow["Check"] = (object) (!BCE.Data.Convert.ToBoolean(dataRow["Check"]) ? 1 : 0);
          dataRow.EndEdit();
          this.UpdateSelectedCount();
        }
      }
    }

    private void cbEdtFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
      GridViewUtils.UpdateData(this.gridView1);
      if (this.cbEdtFilter.SelectedIndex == 1)
        this.myDataTable.DefaultView.RowFilter = "Check = True";
      else if (this.cbEdtFilter.SelectedIndex == 2)
        this.myDataTable.DefaultView.RowFilter = "Check IS NULL OR Check = False";
      else
        this.myDataTable.DefaultView.RowFilter = "";
    }

    private void UpdateSelectedCount()
    {
      GridViewUtils.UpdateData(this.gridView1);
      int num = 0;
      for (int rowHandle = 0; rowHandle < this.gridView1.RowCount; ++rowHandle)
      {
        if (BCE.Data.Convert.ToBoolean(((ColumnView) this.gridView1).GetRowCellValue(rowHandle, this.gridColCheck)))
          ++num;
      }
      Label label = this.lblSelectedCount;
      Production.GeneralMaintenance.ItemBOM.ItemBOMMaintStringId local = Production.GeneralMaintenance.ItemBOM.ItemBOMMaintStringId._Selected;
      object[] objArray = new object[1];
      int index = 0;
      string str = num.ToString();
      objArray[index] = (object) str;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      label.Text = @string;
    }

    private void repositoryItemCheckEdit_Check_CheckedChanged(object sender, EventArgs e)
    {
      this.UpdateSelectedCount();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit_Check = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSelectedCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbEdtFilter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.sbtnUnselectAll = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSelectAll = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.colCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit_Check)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEdtFilter.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit_Check});
            this.gridControl1.Size = new System.Drawing.Size(676, 309);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColCheck,
            this.colDocDate,
            this.colDocNo});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged_1);
            this.gridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView1_KeyDown);
            // 
            // gridColCheck
            // 
            this.gridColCheck.ColumnEdit = this.repositoryItemCheckEdit_Check;
            this.gridColCheck.FieldName = "Check";
            this.gridColCheck.Name = "gridColCheck";
            this.gridColCheck.Visible = true;
            this.gridColCheck.VisibleIndex = 0;
            this.gridColCheck.Width = 218;
            // 
            // repositoryItemCheckEdit_Check
            // 
            this.repositoryItemCheckEdit_Check.Caption = "Check";
            this.repositoryItemCheckEdit_Check.Name = "repositoryItemCheckEdit_Check";
            this.repositoryItemCheckEdit_Check.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit_Check.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEdit_Check_CheckedChanged);
            // 
            // colDocDate
            // 
            this.colDocDate.Caption = "DocDate";
            this.colDocDate.FieldName = "DocDate";
            this.colDocDate.Name = "colDocDate";
            this.colDocDate.Visible = true;
            this.colDocDate.VisibleIndex = 2;
            this.colDocDate.Width = 530;
            // 
            // colDocNo
            // 
            this.colDocNo.Caption = "DocNo";
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 1;
            this.colDocNo.Width = 285;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 'T';
            this.repositoryItemCheckEdit1.ValueUnchecked = 'F';
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblSelectedCount);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbEdtFilter);
            this.panel1.Controls.Add(this.sbtnUnselectAll);
            this.panel1.Controls.Add(this.sbtnSelectAll);
            this.panel1.Controls.Add(this.sbtnCancel);
            this.panel1.Controls.Add(this.sbtnOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 309);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(676, 45);
            this.panel1.TabIndex = 1;
            // 
            // lblSelectedCount
            // 
            this.lblSelectedCount.ForeColor = System.Drawing.Color.Navy;
            this.lblSelectedCount.Location = new System.Drawing.Point(396, 14);
            this.lblSelectedCount.Name = "lblSelectedCount";
            this.lblSelectedCount.Size = new System.Drawing.Size(106, 23);
            this.lblSelectedCount.TabIndex = 0;
            this.lblSelectedCount.Text = "Count";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(190, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Filter :";
            // 
            // cbEdtFilter
            // 
            this.cbEdtFilter.EditValue = "Show All Items";
            this.cbEdtFilter.Location = new System.Drawing.Point(241, 11);
            this.cbEdtFilter.Name = "cbEdtFilter";
            this.cbEdtFilter.Properties.Items.AddRange(new object[] {
            "Show All Items",
            "Show Selected Items Only",
            "Show Non-selected Items Only"});
            this.cbEdtFilter.Size = new System.Drawing.Size(149, 20);
            this.cbEdtFilter.TabIndex = 2;
            this.cbEdtFilter.SelectedIndexChanged += new System.EventHandler(this.cbEdtFilter_SelectedIndexChanged);
            // 
            // sbtnUnselectAll
            // 
            this.sbtnUnselectAll.Location = new System.Drawing.Point(93, 10);
            this.sbtnUnselectAll.Name = "sbtnUnselectAll";
            this.sbtnUnselectAll.Size = new System.Drawing.Size(75, 23);
            this.sbtnUnselectAll.TabIndex = 3;
            this.sbtnUnselectAll.Text = "Unselect All";
            this.sbtnUnselectAll.Click += new System.EventHandler(this.sbtnUnselectAll_Click);
            // 
            // sbtnSelectAll
            // 
            this.sbtnSelectAll.Location = new System.Drawing.Point(12, 10);
            this.sbtnSelectAll.Name = "sbtnSelectAll";
            this.sbtnSelectAll.Size = new System.Drawing.Size(75, 23);
            this.sbtnSelectAll.TabIndex = 4;
            this.sbtnSelectAll.Text = "Select All";
            this.sbtnSelectAll.Click += new System.EventHandler(this.sbtnSelectAll_Click);
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnCancel.Location = new System.Drawing.Point(589, 10);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.sbtnCancel.TabIndex = 5;
            this.sbtnCancel.Text = "Cancel";
            // 
            // sbtnOK
            // 
            this.sbtnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnOK.Location = new System.Drawing.Point(508, 10);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(75, 23);
            this.sbtnOK.TabIndex = 6;
            this.sbtnOK.Text = "OK";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // colCheck
            // 
            this.colCheck.Caption = "Check";
            this.colCheck.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCheck.FieldName = "Check";
            this.colCheck.Name = "colCheck";
            this.colCheck.Visible = true;
            this.colCheck.VisibleIndex = 0;
            this.colCheck.Width = 78;
            // 
            // FormMultiSelectWODateFilter
            // 
            this.AcceptButton = this.sbtnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnCancel;
            this.ClientSize = new System.Drawing.Size(676, 354);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMultiSelectWODateFilter";
            this.ShowInTaskbar = false;
            this.Text = "Multi-select WO";
            this.Load += new System.EventHandler(this.FormParameterCustom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit_Check)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbEdtFilter.Properties)).EndInit();
            this.ResumeLayout(false);

    }
  }
}
