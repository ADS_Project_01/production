﻿using BCE.AutoCount;
using BCE.AutoCount.Common;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Collections;
using BCE.AutoCount.Settings;
using BCE.AutoCount.LicenseControl;
namespace Production.StockCard
{
    [Serializable]
    public class StockCardFormCriteria : ReportCriteria
    {
        private BCE.AutoCount.SearchFilter.Filter myLocationFilter = new BCE.AutoCount.SearchFilter.Filter("A", "Location", Localizer.GetString((Enum)StockStringId.Location, new object[0]), FilterControlType.Location);
        private BCE.AutoCount.SearchFilter.Filter myStockItemFilter = new BCE.AutoCount.SearchFilter.Filter("A", "ItemCode", Localizer.GetString((Enum)StockStringId.StockItem, new object[0]), FilterControlType.Item);
        private BCE.AutoCount.SearchFilter.Filter myStockGroupFilter = new BCE.AutoCount.SearchFilter.Filter("Item", "ItemGroup", Localizer.GetString((Enum)StockStringId.StockGroup, new object[0]), FilterControlType.ItemGroup);
        private BCE.AutoCount.SearchFilter.Filter myStockItemTypeFilter = new BCE.AutoCount.SearchFilter.Filter("Item", "ItemType", Localizer.GetString((Enum)StockStringId.ItemType, new object[0]), FilterControlType.ItemType);
        private BCE.AutoCount.SearchFilter.Filter myWONoFilter = new BCE.AutoCount.SearchFilter.Filter("A", "WONo");

        private bool myActiveItem = true;
        private bool myMergeSameCost = true;
        private bool myIncludeZeroBalance = true;
        protected PrintBatchOption myBatchOption = PrintBatchOption.PrintAllBatch;
        private object myFromDate;
        private object myToDate;
        private bool myInactiveItem;
        private bool myShowZeroBalance;
        private bool myIncludeNonTransactionItem;
        protected ShowUOMOption myUOMOption;
        protected GroupByItemGroupTypeLocationOption myGroupBy;
        private bool myAdvancedOptions;
        [NonSerialized]
        private DBSetting myDBSetting;

        public DBSetting DBSetting
        {
            get
            {
                return this.myDBSetting;
            }
            set
            {
                this.myDBSetting = value;
            }
        }

        public object FromDate
        {
            get
            {
                return this.myFromDate;
            }
            set
            {
                this.myFromDate = value;
            }
        }

        public object ToDate
        {
            get
            {
                return this.myToDate;
            }
            set
            {
                this.myToDate = value;
            }
        }

        public BCE.AutoCount.SearchFilter.Filter LocationFilter
        {
            get
            {
                return this.myLocationFilter;
            }
        }

        public BCE.AutoCount.SearchFilter.Filter StockItemFilter
        {
            get
            {
                return this.myStockItemFilter;
            }
        }

        public BCE.AutoCount.SearchFilter.Filter WONoFilter
        {
            get
            {
                return this.myWONoFilter;
            }
        }

        public BCE.AutoCount.SearchFilter.Filter StockGroupFilter
        {
            get
            {
                return this.myStockGroupFilter;
            }
        }

        public BCE.AutoCount.SearchFilter.Filter StockItemTypeFilter
        {
            get
            {
                return this.myStockItemTypeFilter;
            }
        }

        public bool ActiveItem
        {
            get
            {
                return this.myActiveItem;
            }
            set
            {
                this.myActiveItem = value;
            }
        }

        public bool InactiveItem
        {
            get
            {
                return this.myInactiveItem;
            }
            set
            {
                this.myInactiveItem = value;
            }
        }
        public bool ShowZeroBalance
        {
            get
            {
                return this.myShowZeroBalance;
            }
            set
            {
                this.myShowZeroBalance = value;
            }
        }
        public bool MergeSameCost
        {
            get
            {
                return this.myMergeSameCost;
            }
            set
            {
                this.myMergeSameCost = value;
            }
        }

        public bool IncludeZeroBalance
        {
            get
            {
                return this.myIncludeZeroBalance;
            }
            set
            {
                this.myIncludeZeroBalance = value;
            }
        }

        public bool IncludeNonTransactionItem
        {
            get
            {
                return this.myIncludeNonTransactionItem;
            }
            set
            {
                this.myIncludeNonTransactionItem = value;
            }
        }

        public ShowUOMOption UOMOption
        {
            get
            {
                return this.myUOMOption;
            }
            set
            {
                this.myUOMOption = value;
            }
        }

        public PrintBatchOption BatchOption
        {
            get
            {
                return this.myBatchOption;
            }
            set
            {
                this.myBatchOption = value;
            }
        }

        public GroupByItemGroupTypeLocationOption GroupBy
        {
            get
            {
                return this.myGroupBy;
            }
            set
            {
                this.myGroupBy = value;
            }
        }

        public bool AdvancedOptions
        {
            get
            {
                return this.myAdvancedOptions;
            }
            set
            {
                this.myAdvancedOptions = value;
            }
        }

        public StockCardFormCriteria(DBSetting dbSetting)
        {
            this.myDBSetting = dbSetting;
            this.myVersion = 2;
        }

        public override void InitCurrentVersion()
        {
            this.myCurrentVersion = 2;
        }

        protected override bool IsVersionMatch()
        {
            if (!Enum.IsDefined(this.myUOMOption.GetType(), (object)this.myUOMOption))
                this.myUOMOption = ShowUOMOption.ShowMultiUOM;
            if (!Enum.IsDefined(this.myBatchOption.GetType(), (object)this.myBatchOption))
                this.myBatchOption = PrintBatchOption.PrintAllBatch;
            if (!Enum.IsDefined(this.myGroupBy.GetType(), (object)this.myGroupBy))
                this.myGroupBy = GroupByItemGroupTypeLocationOption.None;
            return base.IsVersionMatch();
        }

        protected override ArrayList GetFilterOptionsText()
        {
            ArrayList filterOptionsText = base.GetFilterOptionsText();
            string str1 = string.Empty;
            DateTime dateTime;
            string str2;
            if (this.myFromDate != null)
            {
                dateTime = (DateTime)this.myFromDate;
                str2 = dateTime.ToLongDateString();
            }
            else
                str2 = FiscalYear.GetOrCreate(this.myDBSetting).ActualDataStartDate.ToLongDateString();
            ArrayList arrayList1 = filterOptionsText;
            // ISSUE: variable of a boxed type
            StockStringId local1 = StockStringId.FromDate;
            object[] objArray1 = new object[1];
            int index1 = 0;
            string str3 = str2;
            objArray1[index1] = (object)str3;
            string string1 = Localizer.GetString((Enum)local1, objArray1);
            arrayList1.Add((object)string1);
            if (this.myToDate != null)
            {
                ArrayList arrayList2 = filterOptionsText;
                // ISSUE: variable of a boxed type
                StockStringId local2 = StockStringId.ToDate;
                object[] objArray2 = new object[1];
                int index2 = 0;
                dateTime = (DateTime)this.myToDate;
                string str4 = dateTime.ToLongDateString();
                objArray2[index2] = (object)str4;
                string string2 = Localizer.GetString((Enum)local2, objArray2);
                arrayList2.Add((object)string2);
            }
            string str5 = string.Empty;
            if (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.MultiLocationStock.Enable)
            {
                string str4 = this.myLocationFilter.BuildReadableText(false);
                if (str4.Length > 0)
                    filterOptionsText.Add((object)str4);
            }
            string str6 = this.myStockItemFilter.BuildReadableText(false);
            if (str6.Length > 0)
                filterOptionsText.Add((object)str6);
            string str7 = this.myStockGroupFilter.BuildReadableText(false);
            if (str7.Length > 0)
                filterOptionsText.Add((object)str7);
            string str8 = this.myStockItemTypeFilter.BuildReadableText(false);
            if (str8.Length > 0)
                filterOptionsText.Add((object)str8);
            string str9 = this.myWONoFilter.BuildReadableText(false);
            if (str9.Length > 0)
                filterOptionsText.Add((object)str9);
            if (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.BatchNo.Enable)
            {
                ArrayList arrayList2 = filterOptionsText;
                // ISSUE: variable of a boxed type
                StockStringId local2 = StockStringId.BatchOption;
                object[] objArray2 = new object[1];
                int index2 = 0;
                string string2 = Localizer.GetString((Enum)this.myBatchOption, new object[0]);
                objArray2[index2] = (object)string2;
                string string3 = Localizer.GetString((Enum)local2, objArray2);
                arrayList2.Add((object)string3);
            }
            filterOptionsText.Add((object)Utils.BooleanToReadableText(Localizer.GetString((Enum)StockStringId.PrintActiveItem, new object[0]), this.myActiveItem));
            filterOptionsText.Add((object)Utils.BooleanToReadableText(Localizer.GetString((Enum)StockStringId.PrintInactiveItem, new object[0]), this.myInactiveItem));
            if (filterOptionsText.Count == 0)
                filterOptionsText.Add((object)Localizer.GetString((Enum)ReportCriteriaStringId.NoFilter, new object[0]));
            return filterOptionsText;
        }

        protected override ArrayList GetOtherOptionsText()
        {
            ArrayList otherOptionsText = base.GetOtherOptionsText();
            otherOptionsText.Add((object)Utils.BooleanToReadableText(Localizer.GetString((Enum)StockStringId.MergeSameCostFIFOAndLIFO, new object[0]), this.myMergeSameCost));
            otherOptionsText.Add((object)Utils.BooleanToReadableText(Localizer.GetString((Enum)StockStringId.IncludeZeroBalance, new object[0]), this.myIncludeZeroBalance));
            otherOptionsText.Add((object)Utils.BooleanToReadableText(Localizer.GetString((Enum)StockStringId.IncludeNonTransactionItem, new object[0]), this.myIncludeNonTransactionItem));
            if (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.MultiUOM.Enable)
            {
                if (this.myUOMOption == ShowUOMOption.ShowMultiUOM)
                {
                    ArrayList arrayList = otherOptionsText;
                    // ISSUE: variable of a boxed type
                   StockStringId local = StockStringId.UOMOption;
                    object[] objArray = new object[1];
                    int index = 0;
                    string string1 = Localizer.GetString((Enum)ShowUOMOption.ShowMultiUOM, new object[0]);
                    objArray[index] = (object)string1;
                    string string2 = Localizer.GetString((Enum)local, objArray);
                    arrayList.Add((object)string2);
                }
                else if (this.myUOMOption == ShowUOMOption.ShowDefaultReportUOM)
                {
                    ArrayList arrayList = otherOptionsText;
                    // ISSUE: variable of a boxed type
                    StockStringId local = StockStringId.UOMOption;
                    object[] objArray = new object[1];
                    int index = 0;
                    string string1 = Localizer.GetString((Enum)ShowUOMOption.ShowDefaultReportUOM, new object[0]);
                    objArray[index] = (object)string1;
                    string string2 = Localizer.GetString((Enum)local, objArray);
                    arrayList.Add((object)string2);
                }
                else
                {
                    ArrayList arrayList = otherOptionsText;
                    // ISSUE: variable of a boxed type
                    StockStringId local = StockStringId.UOMOption;
                    object[] objArray = new object[1];
                    int index = 0;
                    string string1 = Localizer.GetString((Enum)ShowUOMOption.ShowSmallestUOM, new object[0]);
                    objArray[index] = (object)string1;
                    string string2 = Localizer.GetString((Enum)local, objArray);
                    arrayList.Add((object)string2);
                }
            }
            return otherOptionsText;
        }

        protected override ArrayList GetReportOptionsText()
        {
            ArrayList reportOptionsText = base.GetReportOptionsText();
            if (this.myGroupBy < GroupByItemGroupTypeLocationOption.None)
                this.myGroupBy = GroupByItemGroupTypeLocationOption.None;
            reportOptionsText.Add((object)this.GetGroupByText(Localizer.GetString((Enum)this.myGroupBy, new object[0])));
            return reportOptionsText;
        }
    }

}
