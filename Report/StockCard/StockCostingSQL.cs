﻿using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Stock;
namespace Production.StockCard
{
    public class StockCostingSQL : StockCosting
    {
        public override void Add(StockTrans stockTrans)
        {
            DBSetting dbSetting = this.myDBSetting.StartTransaction();
            try
            {
                UTDCosting utdCosting = UTDCosting.Create(dbSetting);
                this.myStockHelper = StockHelper.Create(dbSetting);
                SqlCommand command = dbSetting.CreateCommand("SELECT * FROM RPA_TransRawMaterial WHERE DocType=@DocType AND DocKey=@DocKey", new object[0]);
                command.Parameters.AddWithValue("@DocType", (object)stockTrans.DocType);
                command.Parameters.AddWithValue("@DocKey", (object)stockTrans.DocKey);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dataTable1 = new DataTable();
                adapter.Fill(dataTable1);
                DataTable dataTable2 = dataTable1;
                DataColumn[] dataColumnArray = new DataColumn[1];
                int index = 0;
                DataColumn dataColumn = dataTable1.Columns["DtlKey"];
                dataColumnArray[index] = dataColumn;
                dataTable2.PrimaryKey = dataColumnArray;
                this.CreateAllBalQtyTable();
                bool isWriteOff = stockTrans.DocType == "WO";
                bool isItemOpening = stockTrans.DocType == "OB";
                foreach (DataRow row in (InternalDataCollectionBase)stockTrans.StockTransTable.Rows)
                {
                    long stockDTLKey = BCE.Data.Convert.ToInt64(row["ReferTo"]);
                    if (stockDTLKey < 0L)
                    {
                        DataRow dataRow = dataTable1.Rows.Find((object)-stockDTLKey);
                        if (dataRow == null)
                        {
                            throw new InvalidReferToException();
                        }
                        else
                        {

                            row["Cost"] = (object)(-BCE.Data.Convert.ToDecimal(dataRow["Cost"]) * BCE.Data.Convert.ToDecimal(dataRow["Qty"]) / BCE.Data.Convert.ToDecimal(row["Qty"]));
                            row["ReferTo"] = dataRow["StockDTLKey"];
                        }
                    }
                    else if (stockDTLKey > 0L)
                    {
                        row["Cost"] = (object)StockCosting.GetCostFromStockDTLKey(dbSetting, stockDTLKey);
                       // if (row["Cost"] != null && row["Cost"] != DBNull.Value)
                         //   row["TotalCost"] = BCE.Data.Convert.ToDecimal(dataRow1["Qty"]) * BCE.Data.Convert.ToDecimal(dataRow1["Cost"]);

                    }
                    DataRow dataRow1 = dataTable1.Rows.Find((object)BCE.Data.Convert.ToInt64(row["DtlKey"]));
                    if (dataRow1 != null)
                    {
                        bool isMainColumnChanged;
                        bool isMainColumnChanged2;
                        if (this.IsStockDetailChanged(dataRow1, row, out isMainColumnChanged, out isMainColumnChanged2))
                        {
                            if (isMainColumnChanged)
                            {
                                //this.UpdateDeletedDBRow(dbSetting, dataRow1, utdCosting, isWriteOff);
                                dataRow1["ItemCode"] = row["ItemCode"];
                                dataRow1["UOM"] = row["UOM"];
                                dataRow1["DocNo"] = (object)row["DocNo"];
                                dataRow1["WONo"] = (object)row["WONo"];
                                dataRow1["ProductCode"] = (object)row["ProductCode"];
                                dataRow1["BOMCode"] = (object)row["BOMCode"];
                                dataRow1["Location"] = row["Location"];
                                dataRow1["BatchNo"] = row["BatchNo"];
                                dataRow1["BatchNoProduct"] = row["BatchNoProduct"];
                                dataRow1["Qty"] = row["Qty"];
                                dataRow1["Cost"] = row["Cost"];
                                if(row["Cost"]!=null && row["Cost"]!=DBNull.Value)
                                    dataRow1["TotalCost"] = BCE.Data.Convert.ToDecimal(dataRow1["Qty"]) * BCE.Data.Convert.ToDecimal(dataRow1["Cost"]);
                                //this.UpdateNewDBRow(dataRow1, row, utdCosting, dbSetting, isWriteOff, isItemOpening);
                            }
                            else
                            {
                                Decimal oldQty = BCE.Data.Convert.ToDecimal(dataRow1["Qty"]);
                                Decimal num = BCE.Data.Convert.ToDecimal(row["Qty"]);
                                Decimal diffQty = num - oldQty;
                                if (isMainColumnChanged2)
                                {
                                    this.AddAllBalQty(dataRow1["ItemCode"].ToString(), dataRow1["UOM"].ToString(), dataRow1["Location"].ToString(), dataRow1["BatchNo"].ToString(), -oldQty);
                                    this.AddAllBalQty(row["ItemCode"].ToString(), row["UOM"].ToString(), row["Location"].ToString(), row["BatchNo"].ToString(), num);
                                }
                                else
                                    this.AddAllBalQty(dataRow1["ItemCode"].ToString(), dataRow1["UOM"].ToString(), dataRow1["Location"].ToString(), dataRow1["BatchNo"].ToString(), diffQty);
                                Decimal oldCost = BCE.Data.Convert.ToDecimal(dataRow1["Cost"]);
                                FIFOCost fifoCost = FIFOCost.Create(dbSetting, BCE.Data.Convert.ToInt64(dataRow1["StockDTLKey"]));
                                UTDCostHelper utdCostHelper = this.CreateUTDCostHelper(dbSetting, dataRow1);
                                ComputedCost computedCost;
                                if (((num >= Decimal.Zero ? 1 : (oldQty >= Decimal.Zero ? 1 : 0)) | (isItemOpening ? 1 : 0)) != 0)
                                {
                                    Decimal newCost = BCE.Data.Convert.ToDecimal(row["Cost"]);
                                    computedCost = utdCosting.EditInQty(utdCostHelper, oldQty, oldCost, num, newCost);
                                    if (utdCostHelper.CostingMethod == CostingMethod.FixedCost)
                                    {
                                        Decimal fixedCost = utdCosting.Helper.GetFixedCost(utdCostHelper.ItemCode, utdCostHelper.UOM);
                                        dataRow1["Cost"] = (object)fixedCost;
                                        computedCost.TotalCost = num * fixedCost;
                                        computedCost.CostType = CostType.UseFixedCost;
                                    }
                                    else if (utdCostHelper.CostingMethod == CostingMethod.MostRecently)
                                    {
                                        MostRecentlyCost mostRecentlyCost = utdCostHelper.GetMostRecentlyCost(BCE.Data.Convert.ToInt64(row["DtlKey"]));
                                        if (mostRecentlyCost.CostType == CostType.UseMostRecentlyCost)
                                            newCost = mostRecentlyCost.Cost;
                                        dataRow1["Cost"] = (object)newCost;
                                        computedCost.TotalCost = num * newCost;
                                        if (mostRecentlyCost.CostType != CostType.UseMostRecentlyCost)
                                            computedCost.CostType = CostType.NewInputCost;
                                    }
                                    else
                                        dataRow1["Cost"] = row["Cost"];
                                    fifoCost.DeleteAll();
                                }
                                else
                                {
                                    computedCost = utdCosting.EditOutQty(utdCostHelper, -oldQty, -num, oldCost, fifoCost, isWriteOff);
                                    dataRow1["Cost"] = !(num != Decimal.Zero) ? (object)0 : (object)(computedCost.TotalCost / num);
                                }
                                fifoCost.CopyFIFOTable(computedCost.FIFOTable);
                                dataRow1["TotalCost"] = (object)computedCost.TotalCost;
                                dataRow1["AdjustedCost"] = (object)computedCost.AdjustedCost;
                                row["TotalCost"] = (object)computedCost.TotalCost;
                                dataRow1["Qty"] = row["Qty"];
                                fifoCost.Save();
                            }
                        }
                        else
                        {
                            row["TotalCost"] = dataRow1["TotalCost"];
                            if (isMainColumnChanged2)
                            {
                                Decimal num = BCE.Data.Convert.ToDecimal(dataRow1["Qty"]);
                                Decimal diffQty = BCE.Data.Convert.ToDecimal(row["Qty"]);
                                this.AddAllBalQty(dataRow1["ItemCode"].ToString(), dataRow1["UOM"].ToString(), dataRow1["Location"].ToString(), dataRow1["BatchNo"].ToString(), -num);
                                this.AddAllBalQty(row["ItemCode"].ToString(), row["UOM"].ToString(), row["Location"].ToString(), row["BatchNo"].ToString(), diffQty);
                            }
                        }
                    }
                    else
                    {
                        dataRow1 = dataTable1.NewRow();
                        DBRegistry dbRegistry = DBRegistry.Create(dbSetting);
                        dataRow1["StockDTLKey"] = (object)dbRegistry.IncOne((IRegistryID)new StockDTLKey());
                        dataRow1["DocType"] = (object)stockTrans.DocType;
                        dataRow1["DocKey"] = (object)stockTrans.DocKey;
                        dataRow1["DtlKey"] = row["DtlKey"];
                        dataRow1["DocNo"] = (object)row["DocNo"];
                        dataRow1["WONo"] = (object)row["WONo"];
                        dataRow1["ProductCode"] = (object)row["ProductCode"];
                        dataRow1["BOMCode"] = (object)row["BOMCode"];
                        dataRow1["ItemCode"] = row["ItemCode"];
                        dataRow1["UOM"] = row["UOM"];
                        dataRow1["Location"] = row["Location"];
                        dataRow1["BatchNo"] = row["BatchNo"];
                        dataRow1["BatchNoProduct"] = row["BatchNoProduct"];
                        dataRow1["DocDate"] = row["DocDate"];
                        dataRow1["Qty"] = row["Qty"];
                        dataRow1["Cost"] = row["Cost"];
                        dataRow1["ReferTo"] = (object)0;
                        if (row["Cost"] != null && row["Cost"] != DBNull.Value)
                            dataRow1["TotalCost"] = BCE.Data.Convert.ToDecimal(dataRow1["Qty"]) * BCE.Data.Convert.ToDecimal(dataRow1["Cost"]);

                        dataTable1.Rows.Add(dataRow1);
                      //  this.UpdateNewDBRow(dataRow1, row, utdCosting, dbSetting, isWriteOff, isItemOpening);
                    }
                    if (!dataRow1["Location"].Equals(row["Location"]))
                        dataRow1["Location"] = row["Location"];
                    if (!dataRow1["BatchNo"].Equals(row["BatchNo"]))
                        dataRow1["BatchNo"] = row["BatchNo"];
                    if (!dataRow1["BatchNoProduct"].Equals(row["BatchNoProduct"]))
                        dataRow1["BatchNoProduct"] = row["BatchNoProduct"];
                    if (dataRow1["ProjNo"].ToString() != row["ProjNo"].ToString())
                        dataRow1["ProjNo"] = row["ProjNo"];
                    if (dataRow1["DeptNo"].ToString() != row["DeptNo"].ToString())
                        dataRow1["DeptNo"] = row["DeptNo"];
                    if (BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]) != BCE.Data.Convert.ToDateTime(row["DocDate"]))
                        dataRow1["DocDate"] = row["DocDate"];
                    dataRow1["InputCost"] = row["Cost"];
                    long num1 = BCE.Data.Convert.ToInt64(row["DtlKey"]);
                    long num2 = BCE.Data.Convert.ToInt64(row["ReferTo"]);
                    if (BCE.Data.Convert.ToInt64(dataRow1["ReferTo"]) != num2)
                        dataRow1["ReferTo"] = (object)num2;
                    if (num2 == 0L && BCE.Data.Convert.ToDecimal(row["Qty"]) > Decimal.Zero && stockTrans.DocType != "SA")
                        num1 -= long.MaxValue;
                    if (BCE.Data.Convert.ToInt64(dataRow1["Seq"]) != num1)
                        dataRow1["Seq"] = (object)num1;
                }
                foreach (DataRow dbRow in (InternalDataCollectionBase)dataTable1.Rows)
                {
                    if (stockTrans.StockTransTable.Rows.Find((object)BCE.Data.Convert.ToInt64(dbRow["DtlKey"])) == null)
                    {
                      //  this.UpdateDeletedDBRow(dbSetting, dbRow, utdCosting, isWriteOff);
                        dbRow.Delete();
                    }
                }
                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(adapter);
                adapter.Update(dataTable1);
               // this.UpdateAllItemBalQty(dbSetting);
                dbSetting.Commit();
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                dbSetting.EndTransaction();
            }
        }

        public override void Remove(string docType, long docKey)
        {
            DBSetting dbSetting = this.myDBSetting.StartTransaction();
            try
            {
                UTDCosting utdCosting = UTDCosting.Create(dbSetting);
                this.myStockHelper = StockHelper.Create(dbSetting);
                SqlCommand command1 = dbSetting.CreateCommand("SELECT * FROM RPA_TransRawMaterial WHERE DocType=@DocType AND DocKey=@DocKey", new object[0]);
                command1.Parameters.AddWithValue("@DocType", (object)docType);
                command1.Parameters.AddWithValue("@DocKey", (object)docKey);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command1);
                DataTable dataTable1 = new DataTable();
                DataTable dataTable2 = dataTable1;
                sqlDataAdapter.Fill(dataTable2);
                this.CreateAllBalQtyTable();
                foreach (DataRow dbRow in (InternalDataCollectionBase)dataTable1.Rows)
                {
                    bool isWriteOff = dbRow["DocType"].ToString() == "WO";
                   // this.UpdateDeletedDBRow(dbSetting, dbRow, utdCosting, isWriteOff);
                }
                //this.UpdateAllItemBalQty(dbSetting);
                SqlCommand command2 = dbSetting.CreateCommand("DELETE FROM RPA_TransRawMaterial WHERE DocType=@DocType AND DocKey=@DocKey", new object[0]);
                command2.Parameters.AddWithValue("@DocType", (object)docType);
                command2.Parameters.AddWithValue("@DocKey", (object)docKey);
                command2.ExecuteNonQuery();
                dbSetting.Commit();
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                dbSetting.EndTransaction();
            }
        }

        //private void UpdateDeletedDBRow(DBSetting newDBSetting, DataRow dbRow, UTDCosting utdCosting, bool isWriteOff)
        //{
        //    Decimal qty = BCE.Data.Convert.ToDecimal(dbRow["Qty"]);
        //    if (qty != Decimal.Zero)
        //        this.AddAllBalQty(dbRow["ItemCode"].ToString(), dbRow["UOM"].ToString(), dbRow["Location"].ToString(), dbRow["BatchNo"].ToString(), -qty);
        //    Decimal cost = BCE.Data.Convert.ToDecimal(dbRow["Cost"]);
        //    UTDCostHelper utdCostHelper = this.CreateUTDCostHelper(newDBSetting, dbRow);
        //    if (qty > Decimal.Zero)
        //        utdCosting.DeleteInQty(utdCostHelper, qty, cost);
        //    else if (qty < Decimal.Zero)
        //    {
        //        FIFOCost fifoCost = FIFOCost.Create(newDBSetting, BCE.Data.Convert.ToInt64(dbRow["StockDTLKey"]));
        //        utdCosting.DeleteOutQty(utdCostHelper, -qty, cost, fifoCost, isWriteOff);
        //        fifoCost.Save();
        //    }
        //}

        //private void UpdateNewDBRow(DataRow dbRow, DataRow row, UTDCosting utdCosting, DBSetting newDBSetting, bool isWriteOff, bool isItemOpening)
        //{
        //    FIFOCost fifoCost = FIFOCost.Create(newDBSetting, BCE.Data.Convert.ToInt64(dbRow["StockDTLKey"]));
        //    Decimal num1 = BCE.Data.Convert.ToDecimal(row["Qty"]);
        //    UTDCostHelper utdCostHelper = this.CreateUTDCostHelper(newDBSetting, row);
        //    ComputedCost computedCost;
        //    if (num1 >= Decimal.Zero | isItemOpening)
        //    {
        //        Decimal cost = BCE.Data.Convert.ToDecimal(row["Cost"]);
        //        computedCost = utdCosting.AddInQty(utdCostHelper, num1, cost);
        //        if (utdCostHelper.CostingMethod == CostingMethod.FixedCost)
        //        {
        //            Decimal fixedCost = utdCosting.Helper.GetFixedCost(utdCostHelper.ItemCode, utdCostHelper.UOM);
        //            dbRow["Cost"] = (object)fixedCost;
        //            computedCost.TotalCost = num1 * fixedCost;
        //            dbRow["CostType"] = (object)0;
        //        }
        //        else if (utdCostHelper.CostingMethod == CostingMethod.MostRecently)
        //        {
        //            MostRecentlyCost mostRecentlyCost = utdCostHelper.GetMostRecentlyCost();
        //            Decimal num2 = mostRecentlyCost.Cost;
        //            dbRow["Cost"] = (object)num2;
        //            computedCost.TotalCost = num1 * num2;
        //            dbRow["CostType"] = (object)(sbyte)mostRecentlyCost.CostType;
        //        }
        //        else
        //        {
        //            dbRow["Cost"] = row["Cost"];
        //            dbRow["CostType"] = BCE.Data.Convert.ToInt64(row["ReferTo"]) <= 0L ? (object)4 : (object)5;
        //        }
        //    }
        //    else
        //    {
        //        computedCost = utdCosting.AddOutQty(utdCostHelper, -num1, isWriteOff);
        //        dbRow["Cost"] = !(num1 != Decimal.Zero) ? (object)0 : (object)(computedCost.TotalCost / num1);
        //        dbRow["CostType"] = (object)(sbyte)computedCost.CostType;
        //    }
        //    dbRow["TotalCost"] = (object)computedCost.TotalCost;
        //    DataTable fifoTable = computedCost.FIFOTable;
        //    fifoCost.CopyFIFOTable(fifoTable);
        //    dbRow["AdjustedCost"] = (object)computedCost.AdjustedCost;
        //    row["TotalCost"] = (object)computedCost.TotalCost;
        //    fifoCost.Save();
        //    if (num1 != Decimal.Zero)
        //        this.AddAllBalQty(row["ItemCode"].ToString(), row["UOM"].ToString(), row["Location"].ToString(), row["BatchNo"].ToString(), num1);
        //}

        //private void UpdateAllItemBalQty(DBSetting dbSetting)
        //{
        //    this.UpdateItemUOM(dbSetting);
        //    this.UpdateItemBalQty(dbSetting);
        //    this.UpdateItemBatchBalQty(dbSetting);
        //    this.UpdateItemBatch(dbSetting);
        //}

        //private void UpdateItemUOM(DBSetting dbSetting)
        //{
        //    SqlCommand command = dbSetting.CreateCommand("UPDATE ItemUOM SET BalQty=BalQty+@DiffQty WHERE ItemCode=@ItemCode AND UOM=@UOM", new object[0]);
        //    foreach (DataRow dataRow in (InternalDataCollectionBase)this.myUpdateItemUOMTable.Rows)
        //    {
        //        if (BCE.Data.Convert.ToDecimal(dataRow["Qty"]) != Decimal.Zero)
        //        {
        //            command.Parameters.Clear();
        //            command.Parameters.AddWithValue("@DiffQty", (object)BCE.Data.Convert.ToDecimal(dataRow["Qty"]));
        //            command.Parameters.AddWithValue("@ItemCode", (object)dataRow["ItemCode"].ToString());
        //            command.Parameters.AddWithValue("@UOM", (object)dataRow["UOM"].ToString());
        //            command.ExecuteNonQuery();
        //        }
        //    }
        //}

        //private void UpdateItemBalQty(DBSetting dbSetting)
        //{
        //    SqlCommand command1 = dbSetting.CreateCommand("UPDATE ItemBalQty SET BalQty=BalQty+@DiffQty WHERE ItemCode=@ItemCode AND UOM=@UOM AND Location=@Location", new object[0]);
        //    SqlCommand command2 = dbSetting.CreateCommand("INSERT INTO ItemBalQty (ItemCode, UOM, Location, BalQty) VALUES (@ItemCode, @UOM, @Location, @BalQty)", new object[0]);
        //    foreach (DataRow dataRow in (InternalDataCollectionBase)this.myUpdateItemBalQtyTable.Rows)
        //    {
        //        if (BCE.Data.Convert.ToDecimal(dataRow["Qty"]) != Decimal.Zero)
        //        {
        //            command1.Parameters.Clear();
        //            command1.Parameters.AddWithValue("@DiffQty", (object)BCE.Data.Convert.ToDecimal(dataRow["Qty"]));
        //            command1.Parameters.AddWithValue("@ItemCode", (object)dataRow["ItemCode"].ToString());
        //            command1.Parameters.AddWithValue("@UOM", (object)dataRow["UOM"].ToString());
        //            command1.Parameters.AddWithValue("@Location", (object)dataRow["Location"].ToString());
        //            if (command1.ExecuteNonQuery() == 0)
        //            {
        //                command2.Parameters.Clear();
        //                command2.Parameters.AddWithValue("@BalQty", (object)BCE.Data.Convert.ToDecimal(dataRow["Qty"]));
        //                command2.Parameters.AddWithValue("@ItemCode", (object)dataRow["ItemCode"].ToString());
        //                command2.Parameters.AddWithValue("@UOM", (object)dataRow["UOM"].ToString());
        //                command2.Parameters.AddWithValue("@Location", (object)dataRow["Location"].ToString());
        //                command2.ExecuteNonQuery();
        //            }
        //        }
        //    }
        //}

        //private void UpdateItemBatch(DBSetting dbSetting)
        //{
        //    SqlCommand command = dbSetting.CreateCommand("UPDATE ItemBatch SET BalQty=ISNULL(BalQty,0)+@DiffQty WHERE ItemCode=@ItemCode AND BatchNo=@BatchNo", new object[0]);
        //    foreach (DataRow dataRow in (InternalDataCollectionBase)this.myUpdateItemBatchTable.Rows)
        //    {
        //        if (BCE.Data.Convert.ToDecimal(dataRow["Qty"]) != Decimal.Zero)
        //        {
        //            command.Parameters.Clear();
        //            command.Parameters.AddWithValue("@DiffQty", (object)BCE.Data.Convert.ToDecimal(dataRow["Qty"]));
        //            command.Parameters.AddWithValue("@ItemCode", (object)dataRow["ItemCode"].ToString());
        //            command.Parameters.AddWithValue("@BatchNo", (object)dataRow["BatchNo"].ToString());
        //            command.ExecuteNonQuery();
        //        }
        //    }
        //}

        //private void UpdateItemBatchBalQty(DBSetting dbSetting)
        //{
        //    SqlCommand command1 = dbSetting.CreateCommand("UPDATE ItemBatchBalQty SET BalQty=BalQty+@DiffQty WHERE ItemCode=@ItemCode AND UOM=@UOM AND Location=@Location AND BatchNo=@BatchNo", new object[0]);
        //    SqlCommand command2 = dbSetting.CreateCommand("INSERT INTO ItemBatchBalQty (ItemCode, UOM, Location, BatchNo, BalQty) VALUES (@ItemCode, @UOM, @Location, @BatchNo, @BalQty)", new object[0]);
        //    foreach (DataRow dataRow in (InternalDataCollectionBase)this.myUpdateItemBatchBalQtyTable.Rows)
        //    {
        //        if (BCE.Data.Convert.ToDecimal(dataRow["Qty"]) != Decimal.Zero)
        //        {
        //            command1.Parameters.Clear();
        //            command1.Parameters.AddWithValue("@DiffQty", (object)BCE.Data.Convert.ToDecimal(dataRow["Qty"]));
        //            command1.Parameters.AddWithValue("@ItemCode", (object)dataRow["ItemCode"].ToString());
        //            command1.Parameters.AddWithValue("@UOM", (object)dataRow["UOM"].ToString());
        //            command1.Parameters.AddWithValue("@Location", (object)dataRow["Location"].ToString());
        //            command1.Parameters.AddWithValue("@BatchNo", (object)dataRow["BatchNo"].ToString());
        //            if (command1.ExecuteNonQuery() == 0)
        //            {
        //                command2.Parameters.Clear();
        //                command2.Parameters.AddWithValue("@BalQty", (object)BCE.Data.Convert.ToDecimal(dataRow["Qty"]));
        //                command2.Parameters.AddWithValue("@ItemCode", (object)dataRow["ItemCode"].ToString());
        //                command2.Parameters.AddWithValue("@UOM", (object)dataRow["UOM"].ToString());
        //                command2.Parameters.AddWithValue("@Location", (object)dataRow["Location"].ToString());
        //                command2.Parameters.AddWithValue("@BatchNo", (object)dataRow["BatchNo"].ToString());
        //                command2.ExecuteNonQuery();
        //            }
        //        }
        //    }
        //}
    }

}
