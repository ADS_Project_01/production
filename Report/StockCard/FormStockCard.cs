﻿// Type: BCE.AutoCount.Stock.StockCard.FormStockCard
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Production.StockCard
{
    [BCE.AutoCount.PlugIn.MenuItem("Stock Card WIP", BeginNewGroup = false, MenuOrder = 1, OpenAccessRight = "RPA_REPORTS_STOCKCARDWIP_OPEN", ParentBeginNewGroup = false, ParentMenuCaption = "Report", ParentMenuOrder = 7, ShowAsDialog = false, VisibleAccessRight = "RPA_REPORTS_STOCKCARDWIP_SHOW")]

    [SingleInstanceThreadForm]

    public class FormStockCard : XtraForm
    {
        private string myGroupSummaryId = "";
        protected DecimalSetting myDecimalSetting;
        protected UserAuthentication myUserAuthentication;
        private IContainer components;
        private DBSetting myDBSetting;
        private StockCardFormHelper myFormHelper;
      //  private ScriptObject myScriptObject;
        private MouseDownHelper myMouseDownHelper;
        private CustomizeGridLayout myMasterGridLayout;
        private CustomizeGridLayout myDetailGridLayout;
        private CustomizeGridLayout mySubDetailGridLayout;
        private bool myFilterByLocation;
        //private BarManager barManager1;
        private Bar bar1;
        private GridView gvMaster;
        private BarSubItem barSubItem1;
        private BarButtonItem barBtnDesignReport;
        private BarButtonItem barButtonItem2;
        private GridView gvDetail;
        private GridView gvSubdetail;
        private GridColumn colItemCode1;
        private GridColumn colUOM1;
        private GridColumn colLocation1;
        private GridColumn colItemGroup1;
        private GridColumn colItemType1;
        private GridColumn colDescription1;
        private GridColumn colDesc21;
        private GridColumn colCostingMethodString1;
        private GridColumn colBalance1;
        private GridColumn colItemCode2;
        private GridColumn colUOM2;
        private GridColumn colLocation2;
        private GridColumn colProjNo2;
        private GridColumn colDeptNo2;
        private GridColumn colDocDate2;
        private GridColumn colDocType2;
        private GridColumn colQty2;
        private GridColumn colCost2;
        private GridColumn colTotalCost2;
        private GridColumn colLastModified2;
        private GridColumn colReferTo2;
        private GridColumn colFIFOSeq2;
        private GridColumn colSeq2;
        private GridColumn colFIFOQty2;
        private GridColumn colDocNo2;
        private GridColumn colFIFOCost2;
        private GridColumn colInQty2;
        private GridColumn colOutQty2;
        private GridColumn colBalance2;
        private GridColumn colBalanceString2;
        private GridColumn colCostTypeString2;
        private GridColumn colDescription2;
        private GridColumn colRefDocNo2;
        private GridColumn colAdjustedCost2;
        private GridColumn colFIFOSeq3;
        private GridColumn colFIFOQty3;
        private GridColumn colFIFOCost3;
        private GridColumn colBalanceBFWord1;
        private GridColumn colBFCost;
        private GridColumn colBalanceQty1;
        private GridColumn colBalanceCost1;
        private GridColumn colBalanceCost2;
        private GridColumn colBatchNo2;
        private BarButtonItem barbtnViewSourceDoc;
        private PopupMenu popupMenuReport;
        private GridColumn colAvgBFCost;
        private GridColumn colAvgCost1;
        private GridColumn colAvgCost2;
        private BarButtonItem barbtnAdvancedOptions;
        private GridColumn colCSGNBalQty;
        private GridColumn colBalQtyAfterCSGN;
        private GridColumn colCSGNCost;
        private GridColumn colBalCostAfterCSGN;
        private GridColumn colLocationDesc;
        private GridColumn colLocationDesc2;
        private GridColumn colSerialNoList;
        private GridView gvItemSN;
        private GridColumn colSerialNo4;
        private GridColumn colItemCode4;
        private GridColumn colBatchNo4;
        private GridColumn colManufacturedDate4;
        private GridColumn colExpiryDate4;
        private GridColumn colLastSalesDate4;
        private GridColumn colRemark4;
        private GridColumn colQty4;
        private GridColumn colLocation4;
        private GridColumn colCSGNQty4;
        private GridColumn colShelf1;
        private GridColumn colFurtherDescription;
        private RepositoryItemButtonEdit repositoryFurtherDescription;
        private PanelControl pnMedium;
        private Label label3;
        private UCLocationSelector ucLocationSelector1;
        private CheckEdit chkedtMergeSameCost;
        private XtraTabControl tabControl1;
        private XtraTabPage tabPageResult;
        private XtraTabPage tabPageCriteria;
        private GridControl gctrReport;
        private SimpleButton sbtnInquiry;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private MemoEdit memoEditCriteria;
        private ComboBoxEdit cbUOMOption;
        private Label label7;
        private SimpleButton sbtnToggleOptions;
        private Label label9;
        private ComboBoxEdit cbGroupBy;
        private PanelControl pnBottom;
        private SimpleButton sbtnClose;
        private CheckEdit chkedtShowCriteria;
        private PanelControl pnLocation;
        private Label label8;
        private ComboBoxEdit cbBatchNo;
        private PanelControl pnBatch;
        private PanelControl pnFilterTop;
        private PanelControl pnOptionBottom;
        private PanelControl pnOptionTop;
        private GroupControl gbFilter;
        private GroupControl gbOtherOptions;
        private GroupControl gbReportOptions;
        private Panel pnFilterBottom;
        private CheckEdit chkedtInactiveItem;
        private CheckEdit chkedtActiveItem;
        private Label label10;
        private PanelControl pnCriteriaBasic;
        private GroupControl groupBox1;
        private UCItemSelector ucItemSelector2;
        private Label label11;
        private SimpleButton sbtnAdvOptions;
        private DateEdit dateEditFrom2;
        private Label label12;
        private Label label13;
        private DateEdit dateEditTo2;
        private PanelControl pnCriteriaAdvanced;
        private DateEdit dateEditFrom;
        private Label label2;
        private Label label1;
        private DateEdit dateEditTo;
        private Label label6;
        private Label label5;
        private Label label4;
        private UCItemTypeSelector ucItemTypeSelector1;
        private UCItemGroupSelector ucItemGroupSelector1;
        private UCItemSelector ucItemSelector1;
        private PrintButton sbtnPrint;
        private PreviewButton sbtnPreview;
        private PanelHeader panelHeader1;
        private CheckEdit chkIncludeNoTransactionItem;
        private XtraTabPage xtraTabPage1;
        private GridColumn gridColumn1;
        private GridColumn gridColumn2;
        private GridColumn gridColumn3;
        private GridColumn gridColumn4;
        private GridColumn gridColumn5;
        private GridColumn gridColumn6;
        private BarSubItem barSubItem2;
        private BarButtonItem barDesignReport2;
        private BarButtonItem barReportOptions;
        private BarButtonItem barSwitchOptions;
        private GridColumn gridColumn7;
        private Label label14;
        private UCWODateFilterSelector ucWOSelector2;
        private UCWODateFilterSelector ucWOSelector1;
        private Label label15;
        private BarManager barManager1;
        private Bar bar3;
        private BarSubItem barSubItem3;
        private BarButtonItem barButtonItem1;
        private BarButtonItem barButtonItem3;
        private BarButtonItem barButtonItem4;
        private BarDockControl barDockControl1;
        private BarDockControl barDockControl2;
        private BarDockControl barDockControl3;
        private BarDockControl barDockControl4;
        private CheckEdit chkIncludeZeroBalance;

        public FormStockCard(DBSetting dbSetting)
        {
            this.InitializeComponent();
            this.cbBatchNo.Properties.Items.Clear();
            this.cbBatchNo.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(PrintBatchOption)));
            this.cbBatchNo.SelectedIndex = 1;
            this.cbUOMOption.Properties.Items.Clear();
            this.cbUOMOption.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(ShowUOMOption)));
            this.cbUOMOption.SelectedIndex = 0;
            this.cbGroupBy.Properties.Items.Clear();
            this.cbGroupBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(GroupByItemGroupTypeLocationOption)));
            this.cbGroupBy.SelectedIndex = 0;
            this.myDBSetting = dbSetting;
            this.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
            this.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
            this.myFormHelper = new StockCardFormHelper(this.myDBSetting);
           // this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockCard");
            this.sbtnPreview.ReportType = "Stock Card WIP";
            this.sbtnPreview.SetDBSetting(this.myDBSetting);
            this.sbtnPrint.ReportType = "Stock Card WIP";
            this.sbtnPrint.SetDBSetting(this.myDBSetting);
            this.InitializeUserControls();
            this.InitializeSettings();
            this.InitializeFormControl();
            DateEdit dateEdit1 = this.dateEditFrom;
            DateTime now1 = DateTime.Now;
            int year1 = now1.Year;
            now1 = DateTime.Now;
            int month1 = now1.Month;
            int day1 = 1;
            DateTime dateTime1 = new DateTime(year1, month1, day1);
            dateEdit1.DateTime = dateTime1;
            this.dateEditTo.EditValue = (object)DateTime.Now;
            DateEdit dateEdit2 = this.dateEditFrom2;
            DateTime now2 = DateTime.Now;
            int year2 = now2.Year;
            now2 = DateTime.Now;
            int month2 = now2.Month;
            int day2 = 1;
            DateTime dateTime2 = new DateTime(year2, month2, day2);
            dateEdit2.DateTime = dateTime2;
            this.dateEditTo2.EditValue = (object)DateTime.Now;
            this.barDesignReport2.Enabled = false;
            this.myMouseDownHelper = new MouseDownHelper();
            this.myMouseDownHelper.Init(this.gvMaster);
            this.myMouseDownHelper.Init(this.gvDetail);
            this.myMouseDownHelper.Init(this.gvSubdetail);
            this.myMouseDownHelper.Init(this.gvItemSN);
            BCE.XtraUtils.GridViewUtils.EnableRightMouseDownAsRowSelect(this.gvMaster);
            BCE.XtraUtils.GridViewUtils.EnableRightMouseDownAsRowSelect(this.gvDetail);
            BCE.XtraUtils.GridViewUtils.EnableRightMouseDownAsRowSelect(this.gvSubdetail);
            BCE.XtraUtils.GridViewUtils.EnableRightMouseDownAsRowSelect(this.gvItemSN);
            BCE.AutoCount.XtraUtils.GridViewUtils.SetEvenOddRowAppearance(this.gvMaster);
            BCE.AutoCount.XtraUtils.GridViewUtils.SetEvenOddRowAppearanceForDetailView(this.gvDetail);
            BCE.AutoCount.XtraUtils.GridViewUtils.SetEvenOddRowAppearanceForDetailView(this.gvSubdetail);
            BCE.AutoCount.XtraUtils.GridViewUtils.SetEvenOddRowAppearanceForDetailView(this.gvItemSN);
            XtraEditors.SetValidSqlServerDateRange((IEnumerable)this.Controls);
            this.RefreshDesignReport();
           // this.myUserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.RefreshDesignReport), (Component)this);
            BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Card_WIP_Report.htm");
            DBSetting dbSetting1 = dbSetting;
            string docType = "";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            StockCardStringId local = StockCardStringId.OpenedStockCardWindow;
            object[] objArray = new object[1];
            int index = 0;
            string loginUserId = this.myUserAuthentication.LoginUserID;
            objArray[index] = (object)loginUserId;
            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            string detail = "";
            Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
        }

        private void InitializeUserControls()
        {
            if (this.myFormHelper.Criteria.AdvancedOptions)
            {
                this.barSwitchOptions.Caption = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_BasicOptions, new object[0]);
                this.pnCriteriaAdvanced.Visible = true;
                this.pnCriteriaBasic.Visible = false;
            }
            else
            {
                this.barSwitchOptions.Caption = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_AdvancedOptions, new object[0]);
                this.pnCriteriaAdvanced.Visible = false;
                this.pnCriteriaBasic.Visible = true;
            }
           // this.ucWOSelector1.Initialize(this.myDBSetting, this.myFormHelper.Criteria.WONoFilter);
            this.ucItemSelector1.Initialize(this.myDBSetting, this.myFormHelper.Criteria.StockItemFilter);
            this.ucItemSelector2.Initialize(this.myDBSetting, this.myFormHelper.Criteria.StockItemFilter);
            this.ucWOSelector1.Initialize(this.myDBSetting, this.myFormHelper.Criteria.WONoFilter);
            this.ucWOSelector2.Initialize(this.myDBSetting, this.myFormHelper.Criteria.WONoFilter);

            this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myFormHelper.Criteria.StockGroupFilter);
            this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myFormHelper.Criteria.StockItemTypeFilter);
            this.ucLocationSelector1.Initialize(this.myDBSetting, this.myFormHelper.Criteria.LocationFilter);
        }

        private void InitializeSettings()
        {
           // this.chkedZeroBalance1.Checked = this.myFormHelper.Criteria.ActiveItem;

            this.chkedtActiveItem.Checked = this.myFormHelper.Criteria.ActiveItem;
            this.chkedtInactiveItem.Checked = this.myFormHelper.Criteria.InactiveItem;
            this.chkedtMergeSameCost.Checked = this.myFormHelper.Criteria.MergeSameCost;
           // this.chkIncludeZeroBalance.Checked = this.myFormHelper.Criteria.IncludeZeroBalance;
           // this.chkIncludeNoTransactionItem.Checked = this.myFormHelper.Criteria.IncludeNonTransactionItem;
            if (!this.chkIncludeZeroBalance.Checked)
            {
                this.chkIncludeNoTransactionItem.Checked = false;
                this.chkIncludeNoTransactionItem.Enabled = false;
            }
            this.cbUOMOption.SelectedIndex = !ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.MultiUOM.Enable ? 0 : (int)this.myFormHelper.Criteria.UOMOption;
            this.cbBatchNo.SelectedIndex = !ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.BatchNo.Enable ? 1 : (int)this.myFormHelper.Criteria.BatchOption;
            this.cbGroupBy.SelectedIndex = (int)this.myFormHelper.Criteria.GroupBy;
            this.chkedtShowCriteria.Checked = this.myFormHelper.Criteria.IsShowCriteria;
            if (!ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.MultiLocationStock.Enable)
                this.myFormHelper.Criteria.LocationFilter.Type = FilterType.None;
        }

        private void InitializeFormControl()
        {
            FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting, false);
            string fieldname1 = "Balance";
            string fieldtype1 = "Quantity";
            formControlUtil.AddField(fieldname1, fieldtype1);
            string fieldname2 = "InQty";
            string fieldtype2 = "Quantity";
            formControlUtil.AddField(fieldname2, fieldtype2);
            string fieldname3 = "OutQty";
            string fieldtype3 = "Quantity";
            formControlUtil.AddField(fieldname3, fieldtype3);
            string fieldname4 = "Qty";
            string fieldtype4 = "Quantity";
            formControlUtil.AddField(fieldname4, fieldtype4);
            string fieldname5 = "FIFOQty";
            string fieldtype5 = "Quantity";
            formControlUtil.AddField(fieldname5, fieldtype5);
            string fieldname6 = "BalanceQty";
            string fieldtype6 = "Quantity";
            formControlUtil.AddField(fieldname6, fieldtype6);
            string fieldname7 = "CSGNBalQty";
            string fieldtype7 = "Quantity";
            formControlUtil.AddField(fieldname7, fieldtype7);
            string fieldname8 = "BalQtyAfterCSGN";
            string fieldtype8 = "Quantity";
            formControlUtil.AddField(fieldname8, fieldtype8);
            string fieldname9 = "BalanceCost";
            string fieldtype9 = "Cost";
            formControlUtil.AddField(fieldname9, fieldtype9);
            string fieldname10 = "TotalCost";
            string fieldtype10 = "Cost";
            formControlUtil.AddField(fieldname10, fieldtype10);
            string fieldname11 = "DocDate";
            string fieldtype11 = "Date";
            formControlUtil.AddField(fieldname11, fieldtype11);
            string fieldname12 = "ExpiryDate";
            string fieldtype12 = "Date";
            formControlUtil.AddField(fieldname12, fieldtype12);
            string fieldname13 = "Cost";
            string fieldtype13 = "Cost";
            formControlUtil.AddField(fieldname13, fieldtype13);
            string fieldname14 = "FIFOCost";
            string fieldtype14 = "Cost";
            formControlUtil.AddField(fieldname14, fieldtype14);
            string fieldname15 = "AdjustedCost";
            string fieldtype15 = "Cost";
            formControlUtil.AddField(fieldname15, fieldtype15);
            string fieldname16 = "CSGNCost";
            string fieldtype16 = "Cost";
            formControlUtil.AddField(fieldname16, fieldtype16);
            string fieldname17 = "BalCostAfterCSGN";
            string fieldtype17 = "Cost";
            formControlUtil.AddField(fieldname17, fieldtype17);
            string fieldname18 = "AverageBFCost";
            string fieldtype18 = "Cost";
            formControlUtil.AddField(fieldname18, fieldtype18);
            string fieldname19 = "AverageBalanceCost";
            string fieldtype19 = "Cost";
            formControlUtil.AddField(fieldname19, fieldtype19);
            string fieldname20 = "LastModified";
            string fieldtype20 = "DateTime";
            formControlUtil.AddField(fieldname20, fieldtype20);
            FormStockCard formStockCard = this;
            formControlUtil.InitControls((Control)formStockCard);
        }

        private void InitGroupSummary()
        {
            StringBuilder stringBuilder = new StringBuilder(40);
            stringBuilder.Append("ItemCode");
            stringBuilder.Append("Balance");
            if (this.colBFCost.Visible)
                stringBuilder.Append("TotalCost");
            stringBuilder.Append("BalanceQty");
            if (this.colBalanceCost1.Visible)
                stringBuilder.Append("BalanceCost");
            string str1 = ((object)stringBuilder).ToString();
            if (str1 != this.myGroupSummaryId)
            {
                this.myGroupSummaryId = str1;
                this.gvMaster.GroupSummary.Clear();
                GridGroupSummaryItemCollection groupSummary1 = this.gvMaster.GroupSummary;
                int num1 = 3;
                string fieldName1 = "ItemCode";
                // ISSUE: variable of the null type
                object local1 = null;
                // ISSUE: variable of a boxed type
                GridGroupSummaryItemStringId local2 = GridGroupSummaryItemStringId.Count;
                object[] objArray1 = new object[1];
                int index1 = 0;
                string str2 = "{0}";
                objArray1[index1] = (object)str2;
                string string1 = BCE.Localization.Localizer.GetString((Enum)local2, objArray1);
                groupSummary1.Add((SummaryItemType)num1, fieldName1, (GridColumn)local1, string1);
                GridGroupSummaryItemCollection groupSummary2 = this.gvMaster.GroupSummary;
                int num2 = 0;
                string fieldName2 = "Balance";
                // ISSUE: variable of the null type
                object local3 = null;
                // ISSUE: variable of a boxed type
               GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.BFQty;
                object[] objArray2 = new object[1];
                int index2 = 0;
                string quantityFormatString1 = this.myDecimalSetting.GetQuantityFormatString(0);
                objArray2[index2] = (object)quantityFormatString1;
                string string2 = BCE.Localization.Localizer.GetString((Enum)local4, objArray2);
                groupSummary2.Add((SummaryItemType)num2, fieldName2, (GridColumn)local3, string2);
                if (this.colBFCost.Visible)
                {
                    GridGroupSummaryItemCollection groupSummary3 = this.gvMaster.GroupSummary;
                    int num3 = 0;
                    string fieldName3 = "TotalCost";
                    // ISSUE: variable of the null type
                    object local5 = null;
                    // ISSUE: variable of a boxed type
                   GridGroupSummaryItemStringId local6 = GridGroupSummaryItemStringId.BFCost;
                    object[] objArray3 = new object[1];
                    int index3 = 0;
                    string currencyFormatString = this.myDecimalSetting.GetCurrencyFormatString(0);
                    objArray3[index3] = (object)currencyFormatString;
                    string string3 = BCE.Localization.Localizer.GetString((Enum)local6, objArray3);
                    groupSummary3.Add((SummaryItemType)num3, fieldName3, (GridColumn)local5, string3);
                }
                GridGroupSummaryItemCollection groupSummary4 = this.gvMaster.GroupSummary;
                int num4 = 0;
                string fieldName4 = "BalanceQty";
                // ISSUE: variable of the null type
                object local7 = null;
                // ISSUE: variable of a boxed type
               GridGroupSummaryItemStringId local8 = GridGroupSummaryItemStringId.BalanceQty;
                object[] objArray4 = new object[1];
                int index4 = 0;
                string quantityFormatString2 = this.myDecimalSetting.GetQuantityFormatString(0);
                objArray4[index4] = (object)quantityFormatString2;
                string string4 = BCE.Localization.Localizer.GetString((Enum)local8, objArray4);
                groupSummary4.Add((SummaryItemType)num4, fieldName4, (GridColumn)local7, string4);
                if (this.colBalanceCost1.Visible)
                {
                    GridGroupSummaryItemCollection groupSummary3 = this.gvMaster.GroupSummary;
                    int num3 = 0;
                    string fieldName3 = "BalanceCost";
                    // ISSUE: variable of the null type
                    object local5 = null;
                    // ISSUE: variable of a boxed type
                    GridGroupSummaryItemStringId local6 = GridGroupSummaryItemStringId.BalanceCost;
                    object[] objArray3 = new object[1];
                    int index3 = 0;
                    string currencyFormatString = this.myDecimalSetting.GetCurrencyFormatString(0);
                    objArray3[index3] = (object)currencyFormatString;
                    string string3 = BCE.Localization.Localizer.GetString((Enum)local6, objArray3);
                    groupSummary3.Add((SummaryItemType)num3, fieldName3, (GridColumn)local5, string3);
                }
            }
        }

        private void InitializeGridLayout()
        {
            this.myMasterGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name+"Rev1", this.gvMaster);
            this.myDetailGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name + "Rev1", this.gvDetail);
            this.mySubDetailGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name+"Rev1", this.gvSubdetail);
            CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name + "Rev1", this.gvItemSN);
        }

        private void InvokeModuleFeature(ModuleController controller)
        {
            this.BeginInvoke((Delegate)new MethodInvoker(this.SetModuleFeature));
        }

        private void SetModuleFeature()
        {
            ModuleController moduleController = ModuleControl.GetOrCreate(this.myDBSetting).ModuleController;
            this.SuspendLayout();
            if (!moduleController.MultiUOM.Enable)
            {
                this.pnOptionBottom.Visible = false;
                this.colUOM1.VisibleIndex = -1;
                this.colUOM2.VisibleIndex = -1;
            }
            else
            {
                this.pnOptionBottom.Visible = true;
                this.colUOM1.VisibleIndex = 3;
                this.colUOM2.VisibleIndex = 3;
            }
            if (!moduleController.MultiLocationStock.Enable)
            {
                this.pnLocation.Visible = false;
                if (this.cbGroupBy.Properties.Items.Contains((object)BCE.Localization.Localizer.GetString((Enum)GroupByItemGroupTypeLocationOption.Location, new object[0])))
                {
                    this.cbGroupBy.Properties.Items.Clear();
                    ComboBoxItemCollection items = this.cbGroupBy.Properties.Items;
                    System.Type type = typeof(GroupByItemGroupTypeLocationOption);
                    Enum[] enumArray = new Enum[1];
                    int index = 0;
                    // ISSUE: variable of a boxed type
                   GroupByItemGroupTypeLocationOption local = GroupByItemGroupTypeLocationOption.Location;
                    enumArray[index] = (Enum)local;
                    string[] stringsWithExclude = BCE.Localization.Localizer.GetEnumStringsWithExclude(type, enumArray);
                    items.AddRange((object[])stringsWithExclude);
                }
                this.colLocation1.VisibleIndex = -1;
                this.colLocation2.VisibleIndex = -1;
                this.colLocation1.OptionsColumn.ShowInCustomizationForm = false;
                this.colLocation2.OptionsColumn.ShowInCustomizationForm = false;
                this.colLocationDesc.VisibleIndex = -1;
                this.colLocationDesc2.VisibleIndex = -1;
                this.colLocationDesc.OptionsColumn.ShowInCustomizationForm = false;
                this.colLocationDesc2.OptionsColumn.ShowInCustomizationForm = false;
            }
            else
            {
                this.pnLocation.Visible = true;
                if (!this.cbGroupBy.Properties.Items.Contains((object)BCE.Localization.Localizer.GetString((Enum)GroupByItemGroupTypeLocationOption.Location, new object[0])))
                {
                    this.cbGroupBy.Properties.Items.Clear();
                    this.cbGroupBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(GroupByItemGroupTypeLocationOption)));
                }
                this.colLocation1.VisibleIndex = 1;
                this.colLocation2.VisibleIndex = 1;
                this.colLocation1.OptionsColumn.ShowInCustomizationForm = true;
                this.colLocation2.OptionsColumn.ShowInCustomizationForm = true;
                this.colLocationDesc.OptionsColumn.ShowInCustomizationForm = true;
                this.colLocationDesc2.OptionsColumn.ShowInCustomizationForm = true;
            }
            if (!moduleController.BatchNo.Enable)
            {
                this.pnBatch.Visible = false;
              //  this.colBatchNo1.VisibleIndex = -1;
                this.colBatchNo2.VisibleIndex = -1;
               // this.colBatchExpiryDate.VisibleIndex = -1;
              //  this.colItemBatchDescription.VisibleIndex = -1;
              //  this.colBatchNo1.OptionsColumn.ShowInCustomizationForm = false;
                this.colBatchNo2.OptionsColumn.ShowInCustomizationForm = false;
              //  this.colBatchExpiryDate.OptionsColumn.ShowInCustomizationForm = false;
              //  this.colItemBatchDescription.OptionsColumn.ShowInCustomizationForm = false;
            }
            else
            {
                this.pnBatch.Visible = true;
              //  this.colBatchNo1.VisibleIndex = 2;
                this.colBatchNo2.VisibleIndex = 2;
               // this.colBatchExpiryDate.VisibleIndex = 3;
               // this.colItemBatchDescription.VisibleIndex = 4;
               // this.colBatchNo1.OptionsColumn.ShowInCustomizationForm = true;
                this.colBatchNo2.OptionsColumn.ShowInCustomizationForm = true;
               // this.colBatchExpiryDate.OptionsColumn.ShowInCustomizationForm = true;
              //  this.colItemBatchDescription.OptionsColumn.ShowInCustomizationForm = true;
            }
            if (!moduleController.Consignment.Enable)
            {
                this.colCSGNBalQty.Visible = false;
                this.colCSGNCost.Visible = false;
                this.colBalQtyAfterCSGN.Visible = false;
                this.colBalCostAfterCSGN.Visible = false;
                this.colCSGNBalQty.OptionsColumn.ShowInCustomizationForm = false;
                this.colCSGNCost.OptionsColumn.ShowInCustomizationForm = false;
                this.colBalQtyAfterCSGN.OptionsColumn.ShowInCustomizationForm = false;
                this.colBalCostAfterCSGN.OptionsColumn.ShowInCustomizationForm = false;
            }
            else
            {
                this.colCSGNBalQty.Visible = true;
                this.colCSGNCost.Visible = true;
                this.colBalQtyAfterCSGN.Visible = true;
                this.colBalCostAfterCSGN.Visible = true;
                this.colCSGNBalQty.OptionsColumn.ShowInCustomizationForm = true;
                this.colCSGNCost.OptionsColumn.ShowInCustomizationForm = true;
                this.colBalQtyAfterCSGN.OptionsColumn.ShowInCustomizationForm = true;
                this.colBalCostAfterCSGN.OptionsColumn.ShowInCustomizationForm = true;
            }
            if (!this.pnBatch.Visible && !this.pnLocation.Visible)
            {
                this.gbFilter.Size = new Size(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 488.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 136.0)));
                this.pnCriteriaAdvanced.Size = new Size(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 783.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 145.0)));
            }
            else if (!this.pnBatch.Visible || !this.pnLocation.Visible)
            {
                this.gbFilter.Size = new Size(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 488.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 158.0)));
                this.pnCriteriaAdvanced.Size = new Size(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 783.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 167.0)));
            }
            else
            {
                this.gbFilter.Size = new Size(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 488.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 180.0)));
                this.pnCriteriaAdvanced.Size = new Size(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 783.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 189.0)));
            }
            //if (!this.pnOptionBottom.Visible)
            //{
            //    this.gbOtherOptions.Size = new Size(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 280.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 81.0)));
            //    this.gbReportOptions.Location = new Point(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 496.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 87.0)));
            //}
            //else
            //{
            //    this.gbOtherOptions.Size = new Size(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 280.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 103.0)));
            //    this.gbReportOptions.Location = new Point(BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width / 96.0 * 496.0)), BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 109.0)));
            //}
            //if (this.gbOtherOptions.Height + this.gbReportOptions.Height + 10 > this.pnCriteriaAdvanced.Height)
            //    this.pnCriteriaAdvanced.Height = this.gbOtherOptions.Height + this.gbReportOptions.Height + 10;
            this.ResumeLayout();
            if (this.myMasterGridLayout != null)
                this.myMasterGridLayout.SaveDefaultLayout();
            if (this.myDetailGridLayout != null)
                this.myDetailGridLayout.SaveDefaultLayout();
            if (this.mySubDetailGridLayout != null)
                this.mySubDetailGridLayout.SaveDefaultLayout();
        }

        private void ToggleGridButtons()
        {
            this.sbtnToggleOptions.Enabled = this.gvMaster.OptionsView.ShowGroupPanel && this.gctrReport.DataSource != null;
        }

        private bool CheckIsConditionPassed()
        {
            bool flag = true;
            if (this.dateEditFrom.EditValue == DBNull.Value)
                this.dateEditFrom.EditValue = (object)null;
            if (this.dateEditTo.EditValue == DBNull.Value)
                this.dateEditTo.EditValue = (object)null;
            if (this.dateEditFrom.EditValue != null)
            {
                DateTime actualDataStartDate = FiscalYear.GetOrCreate(this.myDBSetting).ActualDataStartDate;
                if (this.dateEditFrom.DateTime < actualDataStartDate)
                {
                    FormStockCard formStockCard = this;
                    // ISSUE: variable of a boxed type
                    StockCardStringId local = StockCardStringId.ErrorMessage_FromDateEqualOrLarger;
                    object[] objArray = new object[1];
                    int index = 0;
                    string str = actualDataStartDate.ToLongDateString();
                    objArray[index] = (object)str;
                    string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                    AppMessage.ShowErrorMessage((IWin32Window)formStockCard, @string);
                    this.dateEditFrom.DateTime = actualDataStartDate;
                    this.dateEditFrom.Focus();
                    flag = false;
                }
            }
            if (this.dateEditTo.EditValue != null && this.dateEditFrom.DateTime > this.dateEditTo.DateTime)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockCardStringId.ErrorMessage_FromDateLargerThanToDate, new object[0]));
                this.dateEditFrom.Focus();
                flag = false;
            }
            if (!this.chkedtActiveItem.Checked && !this.chkedtInactiveItem.Checked)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockCardStringId.ErrorMessage_ChooseItemActive, new object[0]));
                this.chkedtActiveItem.Focus();
                flag = false;
            }
            return flag;
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnToggleOptions_Click(object sender, EventArgs e)
        {
            if (this.myFormHelper.Criteria.AdvancedOptions)
            {
                this.pnCriteriaAdvanced.Visible = !this.pnCriteriaAdvanced.Visible;
                if (this.pnCriteriaAdvanced.Visible)
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_HideOptions, new object[0]);
                else
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_ShowOptions, new object[0]);
            }
            else
            {
                this.pnCriteriaBasic.Visible = !this.pnCriteriaBasic.Visible;
                if (this.pnCriteriaBasic.Visible)
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_HideOptions, new object[0]);
                else
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_ShowOptions, new object[0]);
            }
        }

        private void gvMaster_Layout(object sender, EventArgs e)
        {
            if (this.gctrReport.DataSource != null)
                this.InitGroupSummary();
            this.ToggleGridButtons();
        }

        private void gvDetail_Layout(object sender, EventArgs e)
        {
            this.ToggleGridButtons();
        }

        private void gvSubdetail_Layout(object sender, EventArgs e)
        {
            this.ToggleGridButtons();
        }

        private void sbtnPrint_Print(object sender, PrintEventArgs e)
        {
            if (this.gctrReport.DataSource == null)
            {
                this.sbtnInquiry.PerformClick();
                if (this.gctrReport.DataSource == null)
                    return;
            }
            if (this.myUserAuthentication.AccessRight.IsAccessible("STK_REPORT_STKCARD_PRINT", (XtraForm)this))
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.myFormHelper.Criteria.GroupBy = (GroupByItemGroupTypeLocationOption)this.cbGroupBy.SelectedIndex;
                this.myFormHelper.Criteria.IsShowCriteria = this.chkedtShowCriteria.Checked;
                this.myFormHelper.PrintReport();
                Cursor.Current = current;
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            this.myFormHelper.Criteria.GroupBy = (GroupByItemGroupTypeLocationOption)this.cbGroupBy.SelectedIndex;
            this.myFormHelper.Criteria.IsShowCriteria = this.chkedtShowCriteria.Checked;
            this.myFormHelper.DesignReport();
            Cursor.Current = current;
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormBasicReportOption.ShowReportOption(this.myFormHelper.ReportOption);

            this.myFormHelper.SaveReportOption();
        }

        private void sbtnInquiry_Click(object sender, EventArgs e)
        {
            if (!this.myFormHelper.Criteria.AdvancedOptions)
            {
                this.dateEditFrom.DateTime = this.dateEditFrom2.DateTime;
                this.dateEditTo.DateTime = this.dateEditTo2.DateTime;
            }
            if (this.CheckIsConditionPassed())
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.SaveCriteria();
                try
                {
                    this.myFormHelper.Inquire(this.dateEditFrom, this.dateEditTo);
                    FormStockCard.FormInquiryEventArgs inquiryEventArgs1 = new FormStockCard.FormInquiryEventArgs(this, this.myFormHelper.ResultDataSet);
                   /// ScriptObject scriptObject = this.myScriptObject;
                    //string name = "OnFormInquiry";
                    //System.Type[] types = new System.Type[2];
                    //int index1 = 0;
                    //System.Type type1 = typeof(object);
                    //types[index1] = type1;
                    //int index2 = 1;
                    //System.Type type2 = inquiryEventArgs1.GetType();
                    //types[index2] = type2;
                    //object[] objArray1 = new object[2];
                    //int index3 = 0;
                    //FormStockCard formStockCard = this;
                    //objArray1[index3] = (object)formStockCard;
                    //int index4 = 1;
                    //FormStockCard.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
                    //objArray1[index4] = (object)inquiryEventArgs2;
                   // scriptObject.RunMethod(name, types, objArray1);
                    DBSetting dbSetting = this.myDBSetting;
                    string docType = "";
                    long docKey = 0L;
                    long eventKey = 0L;
                    // ISSUE: variable of a boxed type
                    StockCardStringId local = StockCardStringId.InquiredStockCard;
                    object[] objArray2 = new object[1];
                    int index5 = 0;
                    string loginUserId = this.myUserAuthentication.LoginUserID;
                    objArray2[index5] = (object)loginUserId;
                    string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray2);
                    string readableText = this.myFormHelper.Criteria.ReadableText;
                    Activity.Log(dbSetting, docType, docKey, eventKey, @string, readableText);
                }
                catch (DataAccessException ex)
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                    return;
                }
                this.gctrReport.DataSource = (object)this.myFormHelper.ResultTable;
                this.gctrReport.LevelTree.Nodes.Clear();
                this.gctrReport.LevelTree.Nodes.Add("MasterDetailRelation", (BaseView)this.gvDetail);
                this.gctrReport.LevelTree.Nodes.Add("MasterItemSNRelation", (BaseView)this.gvItemSN);
                this.gctrReport.LevelTree.Nodes["MasterDetailRelation"].Nodes.Add("DetailSubdetailRelation", (BaseView)this.gvSubdetail);
                this.gvDetail.ViewCaption = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Detail, new object[0]);
                this.gvItemSN.ViewCaption = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.ItemSerialNo, new object[0]);
                if (this.cbUOMOption.SelectedIndex == 1)
                    this.SetExtraDecimal(2);
                else
                    this.SetExtraDecimal(0);
                this.barDesignReport2.Enabled = true;
                this.memoEditCriteria.Lines = this.myFormHelper.Criteria.ReadableTextArray;
                Cursor.Current = current;
            }
        }

        private void SaveCriteria()
        {
            this.myFormHelper.Criteria.FromDate = this.dateEditFrom.EditValue == null ? (object)null : (object)this.dateEditFrom.DateTime.Date;
            this.myFormHelper.Criteria.ToDate = this.dateEditTo.EditValue == null ? (object)null : (object)this.dateEditTo.DateTime.Date;
            this.myFormHelper.Criteria.GroupBy = (GroupByItemGroupTypeLocationOption)this.cbGroupBy.SelectedIndex;
            this.myFormHelper.Criteria.IsShowCriteria = this.chkedtShowCriteria.Checked;
            this.myFormHelper.Criteria.ActiveItem = this.chkedtActiveItem.Checked;
            this.myFormHelper.Criteria.InactiveItem = this.chkedtInactiveItem.Checked;
            this.myFormHelper.Criteria.MergeSameCost = this.chkedtMergeSameCost.Checked;
           // this.myFormHelper.Criteria.IncludeZeroBalance = this.chkIncludeZeroBalance.Checked;
           // this.myFormHelper.Criteria.IncludeNonTransactionItem = this.chkIncludeNoTransactionItem.Checked;
            this.myFormHelper.Criteria.UOMOption = (ShowUOMOption)this.cbUOMOption.SelectedIndex;
            this.myFormHelper.Criteria.BatchOption = (PrintBatchOption)this.cbBatchNo.SelectedIndex;
        }

        private void sbtnPreview_Preview(object sender, PrintEventArgs e)
        {
            if (this.gctrReport.DataSource == null)
            {
                this.sbtnInquiry.PerformClick();
                if (this.gctrReport.DataSource == null)
                    return;
            }
            if (this.myUserAuthentication.AccessRight.IsAccessible("STK_REPORT_STKCARD_PREVIEW", (XtraForm)this))
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.myFormHelper.Criteria.GroupBy = (GroupByItemGroupTypeLocationOption)this.cbGroupBy.SelectedIndex;
                this.myFormHelper.Criteria.IsShowCriteria = this.chkedtShowCriteria.Checked;
                this.myFormHelper.PreviewReport(e);
                Cursor.Current = current;
            }
        }

        private void FormStockCards_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)120)
            {
                if (this.sbtnInquiry.Enabled)
                    this.sbtnInquiry.PerformClick();
            }
            else if (e.KeyCode == (Keys)119)
            {
                if (this.sbtnPreview.Enabled)
                    this.sbtnPreview.PerformClick();
            }
            else if (e.KeyCode == (Keys)118)
            {
                if (this.sbtnPrint.Enabled)
                    this.sbtnPrint.PerformClick();
            }
            else if (e.KeyCode == (Keys)117 && this.sbtnToggleOptions.Enabled)
                this.sbtnToggleOptions.PerformClick();
        }

        private void SetExtraDecimal(int extraDecimal)
        {
            GridColumn[] gridColumnArray = new GridColumn[7];
            int index1 = 0;
            GridColumn gridColumn1 = this.colBalance1;
            gridColumnArray[index1] = gridColumn1;
            int index2 = 1;
            GridColumn gridColumn2 = this.colBalance2;
            gridColumnArray[index2] = gridColumn2;
            int index3 = 2;
            GridColumn gridColumn3 = this.colInQty2;
            gridColumnArray[index3] = gridColumn3;
            int index4 = 3;
            GridColumn gridColumn4 = this.colOutQty2;
            gridColumnArray[index4] = gridColumn4;
            int index5 = 4;
            GridColumn gridColumn5 = this.colQty2;
            gridColumnArray[index5] = gridColumn5;
            int index6 = 5;
            GridColumn gridColumn6 = this.colFIFOQty2;
            gridColumnArray[index6] = gridColumn6;
            int index7 = 6;
            GridColumn gridColumn7 = this.colFIFOQty3;
            gridColumnArray[index7] = gridColumn7;
            foreach (GridColumn gridColumn8 in gridColumnArray)
            {
                if (gridColumn8.ColumnEdit != null && gridColumn8.ColumnEdit.DisplayFormat != null && gridColumn8.ColumnEdit.DisplayFormat.Format is DecimalNumberFormatInfo)
                    ((DecimalNumberFormatInfo)gridColumn8.ColumnEdit.DisplayFormat.Format).ExtraDecimal = extraDecimal;
            }
        }

        private void FormStockCards_Load(object sender, EventArgs e)
        {
            this.SetModuleFeature();
           // ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.AddListener(new ModuleControllerListenerDelegate(this.InvokeModuleFeature), (Component)this);
            this.InitializeGridLayout();
            this.FormInitialize();
        }

        private void FormInitialize()
        {
           // FormStockCard.FormInitializeEventArgs initializeEventArgs1 = new FormStockCard.FormInitializeEventArgs(this);
            //ScriptObject scriptObject = this.myScriptObject;
            //string name = "OnFormInitialize";
            //System.Type[] types = new System.Type[2];
            //int index1 = 0;
            //System.Type type1 = typeof(object);
            //types[index1] = type1;
            //int index2 = 1;
            //System.Type type2 = initializeEventArgs1.GetType();
            //types[index2] = type2;
            //object[] objArray = new object[2];
            //int index3 = 0;
            //FormStockCard formStockCard = this;
            //objArray[index3] = (object)formStockCard;
            //int index4 = 1;
            //FormStockCard.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
            //objArray[index4] = (object)initializeEventArgs2;
            //scriptObject.RunMethod(name, types, objArray);
        }

        private void FormStockCards_Activated(object sender, EventArgs e)
        {
            this.ucItemSelector1.RefreshLookupEditBuilder();
            this.ucWOSelector1.RefreshLookupEditBuilder();
            this.ucItemGroupSelector1.RefreshLookupEditBuilder();
            this.ucItemTypeSelector1.RefreshLookupEditBuilder();
            this.ucLocationSelector1.RefreshLookupEditBuilder();
        }

        private void GoToDocument()
        {
            ColumnView columnView = (ColumnView)this.gctrReport.FocusedView;
            int focusedRowHandle = columnView.FocusedRowHandle;
            DataRow dataRow = columnView.GetDataRow(focusedRowHandle);
            if (dataRow != null)
                DocumentDispatcher.Open(this.myDBSetting, dataRow["DocType"].ToString(), BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
        }

        private void barbtnViewSourceDoc_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.GoToDocument();
        }

        private void barManager1_QueryShowPopupMenu(object sender, QueryShowPopupMenuEventArgs e)
        {
            if (e.Menu == this.popupMenuReport)
            {
                if (!this.myUserAuthentication.AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
                {
                    e.Cancel = true;
                }
                else
                {
                    GridView view = new GridView();
                    GridHitInfo gridHitInfo = BCE.XtraUtils.GridViewUtils.GetGridHitInfo(this.gctrReport, ref view);
                    if (gridHitInfo == null)
                        e.Cancel = true;
                    else if (gridHitInfo.InRow && view != this.gvMaster)
                        this.barbtnViewSourceDoc.Visibility = BarItemVisibility.Always;
                    else if (!gridHitInfo.InColumnPanel && !gridHitInfo.InGroupPanel)
                        this.barbtnViewSourceDoc.Visibility = BarItemVisibility.Never;
                    else
                        e.Cancel = true;
                }
            }
        }

        private void gvDetail_DoubleClick(object sender, EventArgs e)
        {
            if (this.myMouseDownHelper.IsLeftMouseDown && BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView)sender).InRow && this.myUserAuthentication.AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
                this.GoToDocument();
        }

        private void gvDetail_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            bool flag = ((BaseView)sender).RowCount > 0;
            if (this.barbtnViewSourceDoc != null)
                this.barbtnViewSourceDoc.Enabled = flag;
        }

        private void gvSubdetail_DoubleClick(object sender, EventArgs e)
        {
            if (this.myMouseDownHelper.IsLeftMouseDown && BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView)sender).InRow && this.myUserAuthentication.AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
                this.GoToDocument();
        }

        private void gvSubdetail_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            bool flag = ((BaseView)sender).RowCount > 0;
            if (this.barbtnViewSourceDoc != null)
                this.barbtnViewSourceDoc.Enabled = flag;
        }

        private void barbtnAdvancedOptions_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!this.pnCriteriaAdvanced.Visible)
            {
                this.dateEditFrom.DateTime = this.dateEditFrom2.DateTime;
                this.dateEditTo.DateTime = this.dateEditTo2.DateTime;
            }
            else
            {
                this.dateEditFrom2.DateTime = this.dateEditFrom.DateTime;
                this.dateEditTo2.DateTime = this.dateEditTo.DateTime;
            }
            this.SaveCriteria();
            this.SuspendLayout();
            if (this.myFormHelper.Criteria.AdvancedOptions)
            {
                this.barSwitchOptions.Caption = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_AdvancedOptions, new object[0]);
                this.pnCriteriaBasic.Visible = true;
                this.pnCriteriaAdvanced.Visible = false;
                this.myFormHelper.Criteria.AdvancedOptions = false;
                this.ucItemSelector2.ApplyFilter(this.myFormHelper.Criteria.StockItemFilter);
                this.ucWOSelector2.ApplyFilter(this.myFormHelper.Criteria.WONoFilter);
            }
            else
            {
                this.barSwitchOptions.Caption = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_BasicOptions, new object[0]);
                this.pnCriteriaBasic.Visible = false;
                this.pnCriteriaAdvanced.Visible = true;
                this.myFormHelper.Criteria.AdvancedOptions = true;
                this.SetModuleFeature();
                this.ucItemGroupSelector1.ApplyFilter(this.myFormHelper.Criteria.StockGroupFilter);
                this.ucItemSelector1.ApplyFilter(this.myFormHelper.Criteria.StockItemFilter);
                this.ucWOSelector1.ApplyFilter(this.myFormHelper.Criteria.WONoFilter);
                this.ucItemTypeSelector1.ApplyFilter(this.myFormHelper.Criteria.StockItemTypeFilter);
                this.ucLocationSelector1.ApplyFilter(this.myFormHelper.Criteria.LocationFilter);
            }
            this.ResumeLayout();
        }

        private void sbtnAdvOptions_Click(object sender, EventArgs e)
        {
            this.SaveCriteria();
            //using (FormAdvOptions formAdvOptions = new FormAdvOptions(this.myDBSetting, this.myFormHelper))
            //{
            //    formAdvOptions.SetFilterByLocation(this.myFilterByLocation);
            //    if (formAdvOptions.ShowDialog() == DialogResult.OK)
            //    {
            //        this.chkedtActiveItem.Checked = formAdvOptions.PrintActiveItem;
            //        this.chkedtInactiveItem.Checked = formAdvOptions.PrintInactiveItem;
            //        this.cbBatchNo.SelectedIndex = (int)formAdvOptions.BatchOptions;
            //        this.cbUOMOption.SelectedIndex = (int)formAdvOptions.UOMOption;
            //        this.cbGroupBy.SelectedIndex = (int)formAdvOptions.GroupBy;
            //        this.chkedtShowCriteria.Checked = formAdvOptions.ShowCriteria;
            //        this.chkedtMergeSameCost.Checked = formAdvOptions.MergeSameCost;
            //        this.chkIncludeZeroBalance.Checked = formAdvOptions.IncludeZeroBalance;
            //        this.chkIncludeNoTransactionItem.Checked = formAdvOptions.IncludeNoTransactionItem;
            //    }
            //}
        }

        private void RefreshDesignReport()
        {
            this.barDesignReport2.Visibility = XtraBarsUtils.ToBarItemVisibility(this.myUserAuthentication.AccessRight.IsAccessible("TOOLS_RPT_SHOW"));
            this.myFilterByLocation = SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnableFilterByCurrentUserLocationAccessRight;
            if (this.myFilterByLocation)
            {
                this.ucLocationSelector1.Filter.Type = FilterType.ByRange;
                this.ucLocationSelector1.Filter.From = (object)this.myUserAuthentication.MainLocation;
                this.ucLocationSelector1.Filter.To = (object)this.myUserAuthentication.MainLocation;
                this.ucLocationSelector1.ApplyFilter();
                this.ucLocationSelector1.Enabled = false;
            }
            else
                this.ucLocationSelector1.Enabled = true;
        }

        private void repositoryFurtherDescription_ButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            int focusedRowHandle = this.gvMaster.FocusedRowHandle;
            if (this.gvMaster.GetDataRow(focusedRowHandle) != null)
            {
                using (FormRichTextEditor formRichTextEditor = new FormRichTextEditor(this.myDBSetting, this.myFormHelper.ResultTable, this.gvMaster.GetDataSourceRowIndex(focusedRowHandle), "FurtherDescription", "Further Description", false))
                {
                    int num = (int)formRichTextEditor.ShowDialog((IWin32Window)this);
                }
            }
        }

        private void chkIncludeZeroBalance_CheckedChanged(object sender, EventArgs e)
        {
           // this.myFormHelper.Criteria.IncludeZeroBalance = this.chkIncludeZeroBalance.Checked;
            if (!this.chkIncludeZeroBalance.Checked)
                this.chkIncludeNoTransactionItem.Checked = false;
            this.chkIncludeNoTransactionItem.Enabled = this.chkIncludeZeroBalance.Checked;
           // this.myFormHelper.Criteria.IncludeNonTransactionItem = this.chkIncludeNoTransactionItem.Checked;
        }

        private void chkIncludeNoTransactionItem_CheckedChanged(object sender, EventArgs e)
        {
           // this.myFormHelper.Criteria.IncludeNonTransactionItem = this.chkIncludeNoTransactionItem.Checked;
            if (this.chkIncludeNoTransactionItem.Checked)
                this.chkIncludeZeroBalance.Checked = true;
        }

        private void FormStockCard_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.myFormHelper != null)
            {
                try
                {
                    this.myFormHelper.SaveSetting();
                }
                catch
                {
                }
                this.myFormHelper = (StockCardFormHelper)null;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInQty2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIFOSeq2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIFOQty2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIFOCost2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocType2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocNo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesc21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeptNo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTypeString2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCost2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBatchNo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceString2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceCost2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalance2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgCost2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdjustedCost2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutQty2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjNo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQty2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefDocNo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferTo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeq2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUOM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gctrReport = new DevExpress.XtraGrid.GridControl();
            this.gvMaster = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemGroup1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceQty1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalance1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgBFCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalQtyAfterCSGN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceBFWord1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalCostAfterCSGN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFurtherDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryFurtherDescription = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colUOM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNoList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvSubdetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gvItemSN = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLocation2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModified2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIFOSeq3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIFOQty3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIFOCost3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNo4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBatchNo4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManufacturedDate4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastSalesDate4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQty4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCSGNQty4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShelf1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostingMethodString1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBFCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCSGNBalQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCSGNCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationDesc2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnCriteriaAdvanced = new DevExpress.XtraEditors.PanelControl();
            this.label8 = new System.Windows.Forms.Label();
            this.cbBatchNo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.pnOptionTop = new DevExpress.XtraEditors.PanelControl();
            this.gbFilter = new DevExpress.XtraEditors.GroupControl();
            this.pnFilterBottom = new System.Windows.Forms.Panel();
            this.ucWOSelector1 = new Production.StockCard.UCWODateFilterSelector();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ucItemSelector1 = new BCE.AutoCount.FilterUI.UCItemSelector();
            this.ucItemGroupSelector1 = new BCE.AutoCount.FilterUI.UCItemGroupSelector();
            this.label10 = new System.Windows.Forms.Label();
            this.ucItemTypeSelector1 = new BCE.AutoCount.FilterUI.UCItemTypeSelector();
            this.chkedtActiveItem = new DevExpress.XtraEditors.CheckEdit();
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.chkedtInactiveItem = new DevExpress.XtraEditors.CheckEdit();
            this.pnBatch = new DevExpress.XtraEditors.PanelControl();
            this.gbReportOptions = new DevExpress.XtraEditors.GroupControl();
            this.chkedtShowCriteria = new DevExpress.XtraEditors.CheckEdit();
            this.cbGroupBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.gbOtherOptions = new DevExpress.XtraEditors.GroupControl();
            this.cbUOMOption = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.chkedtMergeSameCost = new DevExpress.XtraEditors.CheckEdit();
            this.chkIncludeZeroBalance = new DevExpress.XtraEditors.CheckEdit();
            this.pnLocation = new DevExpress.XtraEditors.PanelControl();
            this.ucLocationSelector1 = new BCE.AutoCount.FilterUI.UCLocationSelector();
            this.pnFilterTop = new DevExpress.XtraEditors.PanelControl();
            this.label4 = new System.Windows.Forms.Label();
            this.chkIncludeNoTransactionItem = new DevExpress.XtraEditors.CheckEdit();
            this.pnOptionBottom = new DevExpress.XtraEditors.PanelControl();
            this.sbtnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.pnMedium = new DevExpress.XtraEditors.PanelControl();
            this.tabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageResult = new DevExpress.XtraTab.XtraTabPage();
            this.tabPageCriteria = new DevExpress.XtraTab.XtraTabPage();
            this.memoEditCriteria = new DevExpress.XtraEditors.MemoEdit();
            this.pnBottom = new DevExpress.XtraEditors.PanelControl();
            this.sbtnPrint = new BCE.AutoCount.Controls.PrintButton();
            this.sbtnPreview = new BCE.AutoCount.Controls.PreviewButton();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnToggleOptions = new DevExpress.XtraEditors.SimpleButton();
            this.pnCriteriaBasic = new DevExpress.XtraEditors.PanelControl();
            this.groupBox1 = new DevExpress.XtraEditors.GroupControl();
            this.ucWOSelector2 = new Production.StockCard.UCWODateFilterSelector();
            this.label14 = new System.Windows.Forms.Label();
            this.dateEditFrom2 = new DevExpress.XtraEditors.DateEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dateEditTo2 = new DevExpress.XtraEditors.DateEdit();
            this.ucItemSelector2 = new BCE.AutoCount.FilterUI.UCItemSelector();
            this.label11 = new System.Windows.Forms.Label();
            this.sbtnAdvOptions = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barDesignReport2 = new DevExpress.XtraBars.BarButtonItem();
            this.barReportOptions = new DevExpress.XtraBars.BarButtonItem();
            this.barSwitchOptions = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnDesignReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnAdvancedOptions = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnViewSourceDoc = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuReport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gctrReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryFurtherDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSubdetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvItemSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaAdvanced)).BeginInit();
            this.pnCriteriaAdvanced.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbBatchNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnOptionTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbFilter)).BeginInit();
            this.gbFilter.SuspendLayout();
            this.pnFilterBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtActiveItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtInactiveItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbReportOptions)).BeginInit();
            this.gbReportOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtShowCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGroupBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbOtherOptions)).BeginInit();
            this.gbOtherOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbUOMOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtMergeSameCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeZeroBalance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLocation)).BeginInit();
            this.pnLocation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnFilterTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeNoTransactionItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnOptionBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMedium)).BeginInit();
            this.pnMedium.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageResult.SuspendLayout();
            this.tabPageCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnBottom)).BeginInit();
            this.pnBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaBasic)).BeginInit();
            this.pnCriteriaBasic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemCode2,
            this.colInQty2,
            this.colFIFOSeq2,
            this.colFIFOQty2,
            this.colFIFOCost2,
            this.colDocType2,
            this.colDocNo2,
            this.colDocDate2,
            this.colDescription2,
            this.colDesc21,
            this.colDeptNo2,
            this.colCostTypeString2,
            this.colCost2,
            this.colBatchNo2,
            this.colBalanceString2,
            this.colBalanceCost2,
            this.colBalance2,
            this.colAvgCost2,
            this.colAdjustedCost2,
            this.colOutQty2,
            this.colProjNo2,
            this.colQty2,
            this.colRefDocNo2,
            this.colReferTo2,
            this.colSeq2,
            this.colTotalCost2,
            this.colUOM2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gvDetail.GridControl = this.gctrReport;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsBehavior.Editable = false;
            this.gvDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDocDate2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gvDetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvDetail_FocusedRowChanged);
            this.gvDetail.DoubleClick += new System.EventHandler(this.gvDetail_DoubleClick);
            this.gvDetail.Layout += new System.EventHandler(this.gvDetail_Layout);
            // 
            // colItemCode2
            // 
            this.colItemCode2.Caption = "ItemCode";
            this.colItemCode2.FieldName = "ItemCode";
            this.colItemCode2.Name = "colItemCode2";
            this.colItemCode2.Visible = true;
            this.colItemCode2.VisibleIndex = 5;
            this.colItemCode2.Width = 52;
            // 
            // colInQty2
            // 
            this.colInQty2.Caption = "InQty";
            this.colInQty2.FieldName = "InQty";
            this.colInQty2.Name = "colInQty2";
            this.colInQty2.Visible = true;
            this.colInQty2.VisibleIndex = 8;
            this.colInQty2.Width = 52;
            // 
            // colFIFOSeq2
            // 
            this.colFIFOSeq2.Caption = "FIFOSeq2";
            this.colFIFOSeq2.FieldName = "FIFOSeq";
            this.colFIFOSeq2.Name = "colFIFOSeq2";
            // 
            // colFIFOQty2
            // 
            this.colFIFOQty2.Caption = "FIFOQty";
            this.colFIFOQty2.FieldName = "FIFOQty";
            this.colFIFOQty2.Name = "colFIFOQty2";
            // 
            // colFIFOCost2
            // 
            this.colFIFOCost2.Caption = "FIFO Cost";
            this.colFIFOCost2.FieldName = "FIFOCost";
            this.colFIFOCost2.Name = "colFIFOCost2";
            // 
            // colDocType2
            // 
            this.colDocType2.Caption = "DocType";
            this.colDocType2.FieldName = "DocType";
            this.colDocType2.Name = "colDocType2";
            this.colDocType2.Visible = true;
            this.colDocType2.VisibleIndex = 1;
            this.colDocType2.Width = 52;
            // 
            // colDocNo2
            // 
            this.colDocNo2.Caption = "DocNo";
            this.colDocNo2.FieldName = "DocNo";
            this.colDocNo2.Name = "colDocNo2";
            this.colDocNo2.Visible = true;
            this.colDocNo2.VisibleIndex = 2;
            this.colDocNo2.Width = 52;
            // 
            // colDocDate2
            // 
            this.colDocDate2.Caption = "DocDate";
            this.colDocDate2.FieldName = "DocDate";
            this.colDocDate2.Name = "colDocDate2";
            this.colDocDate2.Visible = true;
            this.colDocDate2.VisibleIndex = 3;
            this.colDocDate2.Width = 52;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Description";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 6;
            this.colDescription2.Width = 52;
            // 
            // colDesc21
            // 
            this.colDesc21.Caption = "Desc2";
            this.colDesc21.FieldName = "Desc2";
            this.colDesc21.Name = "colDesc21";
            // 
            // colDeptNo2
            // 
            this.colDeptNo2.Caption = "DeptNo";
            this.colDeptNo2.FieldName = "DeptNo";
            this.colDeptNo2.Name = "colDeptNo2";
            // 
            // colCostTypeString2
            // 
            this.colCostTypeString2.Caption = "CostTypeString";
            this.colCostTypeString2.FieldName = "CostTypeString";
            this.colCostTypeString2.Name = "colCostTypeString2";
            // 
            // colCost2
            // 
            this.colCost2.Caption = "Cost";
            this.colCost2.FieldName = "Cost";
            this.colCost2.Name = "colCost2";
            this.colCost2.Visible = true;
            this.colCost2.VisibleIndex = 12;
            this.colCost2.Width = 43;
            // 
            // colBatchNo2
            // 
            this.colBatchNo2.Caption = "BatchNo";
            this.colBatchNo2.FieldName = "BatchNo";
            this.colBatchNo2.Name = "colBatchNo2";
            // 
            // colBalanceString2
            // 
            this.colBalanceString2.Caption = "BalanceString";
            this.colBalanceString2.FieldName = "BalanceString";
            this.colBalanceString2.Name = "colBalanceString2";
            this.colBalanceString2.Visible = true;
            this.colBalanceString2.VisibleIndex = 15;
            this.colBalanceString2.Width = 50;
            // 
            // colBalanceCost2
            // 
            this.colBalanceCost2.Caption = "BalanceCost";
            this.colBalanceCost2.FieldName = "BalanceCost";
            this.colBalanceCost2.Name = "colBalanceCost2";
            this.colBalanceCost2.Visible = true;
            this.colBalanceCost2.VisibleIndex = 11;
            this.colBalanceCost2.Width = 69;
            // 
            // colBalance2
            // 
            this.colBalance2.Caption = "Balance Qty";
            this.colBalance2.FieldName = "Balance";
            this.colBalance2.Name = "colBalance2";
            this.colBalance2.Visible = true;
            this.colBalance2.VisibleIndex = 10;
            this.colBalance2.Width = 81;
            // 
            // colAvgCost2
            // 
            this.colAvgCost2.Caption = "AverageBalanceCost";
            this.colAvgCost2.FieldName = "AverageBalanceCost";
            this.colAvgCost2.Name = "colAvgCost2";
            this.colAvgCost2.Visible = true;
            this.colAvgCost2.VisibleIndex = 14;
            this.colAvgCost2.Width = 43;
            // 
            // colAdjustedCost2
            // 
            this.colAdjustedCost2.Caption = "AdjustedCost";
            this.colAdjustedCost2.FieldName = "AdjustedCost";
            this.colAdjustedCost2.Name = "colAdjustedCost2";
            // 
            // colOutQty2
            // 
            this.colOutQty2.Caption = "OutQty";
            this.colOutQty2.FieldName = "OutQty";
            this.colOutQty2.Name = "colOutQty2";
            this.colOutQty2.Visible = true;
            this.colOutQty2.VisibleIndex = 9;
            this.colOutQty2.Width = 52;
            // 
            // colProjNo2
            // 
            this.colProjNo2.Caption = "ProjNo";
            this.colProjNo2.FieldName = "ProjNo";
            this.colProjNo2.Name = "colProjNo2";
            // 
            // colQty2
            // 
            this.colQty2.Caption = "Qty";
            this.colQty2.FieldName = "Qty";
            this.colQty2.Name = "colQty2";
            // 
            // colRefDocNo2
            // 
            this.colRefDocNo2.Caption = "RefDocNo";
            this.colRefDocNo2.FieldName = "RefDocNo";
            this.colRefDocNo2.Name = "colRefDocNo2";
            // 
            // colReferTo2
            // 
            this.colReferTo2.Caption = "ReferTo";
            this.colReferTo2.FieldName = "ReferTo";
            this.colReferTo2.Name = "colReferTo2";
            // 
            // colSeq2
            // 
            this.colSeq2.Caption = "Seq";
            this.colSeq2.FieldName = "Seq";
            this.colSeq2.Name = "colSeq2";
            // 
            // colTotalCost2
            // 
            this.colTotalCost2.Caption = "Total Cost";
            this.colTotalCost2.FieldName = "TotalCost";
            this.colTotalCost2.Name = "colTotalCost2";
            this.colTotalCost2.Visible = true;
            this.colTotalCost2.VisibleIndex = 13;
            this.colTotalCost2.Width = 43;
            // 
            // colUOM2
            // 
            this.colUOM2.Caption = "UOM";
            this.colUOM2.FieldName = "UOM";
            this.colUOM2.Name = "colUOM2";
            this.colUOM2.Visible = true;
            this.colUOM2.VisibleIndex = 7;
            this.colUOM2.Width = 52;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Product Code";
            this.gridColumn3.FieldName = "ProductCode";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 52;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "BOMCode";
            this.gridColumn4.FieldName = "BOMCode";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            this.gridColumn4.Width = 52;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "ProductType";
            this.gridColumn5.FieldName = "ProductType";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 16;
            this.gridColumn5.Width = 57;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Product Name";
            this.gridColumn6.FieldName = "ProductName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 17;
            this.gridColumn6.Width = 57;
            // 
            // gctrReport
            // 
            this.gctrReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gctrReport.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gctrReport.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gctrReport.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gctrReport.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gctrReport.EmbeddedNavigator.Buttons.Remove.Visible = false;
            gridLevelNode1.LevelTemplate = this.gvDetail;
            gridLevelNode1.RelationName = "Level1";
            this.gctrReport.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gctrReport.Location = new System.Drawing.Point(0, 0);
            this.gctrReport.MainView = this.gvMaster;
            this.gctrReport.Name = "gctrReport";
            this.gctrReport.Size = new System.Drawing.Size(935, 124);
            this.gctrReport.TabIndex = 0;
            this.gctrReport.UseEmbeddedNavigator = true;
            this.gctrReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMaster,
            this.gvSubdetail,
            this.gvItemSN,
            this.gvDetail});
            // 
            // gvMaster
            // 
            this.gvMaster.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemCode1,
            this.colItemGroup1,
            this.colItemType1,
            this.colLocation1,
            this.colLocationDesc,
            this.colDescription1,
            this.colBalanceQty1,
            this.colBalance1,
            this.colAvgCost1,
            this.colAvgBFCost,
            this.colBalQtyAfterCSGN,
            this.colBalanceBFWord1,
            this.colBalCostAfterCSGN,
            this.colFurtherDescription,
            this.colUOM1,
            this.colSerialNoList,
            this.gridColumn7});
            this.gvMaster.GridControl = this.gctrReport;
            this.gvMaster.Name = "gvMaster";
            this.gvMaster.OptionsBehavior.Editable = false;
            this.gvMaster.Layout += new System.EventHandler(this.gvMaster_Layout);
            // 
            // colItemCode1
            // 
            this.colItemCode1.Caption = "Item Code";
            this.colItemCode1.FieldName = "ItemCode";
            this.colItemCode1.Name = "colItemCode1";
            this.colItemCode1.Visible = true;
            this.colItemCode1.VisibleIndex = 0;
            this.colItemCode1.Width = 93;
            // 
            // colItemGroup1
            // 
            this.colItemGroup1.Caption = "Item Group";
            this.colItemGroup1.FieldName = "ItemGroup";
            this.colItemGroup1.Name = "colItemGroup1";
            this.colItemGroup1.Visible = true;
            this.colItemGroup1.VisibleIndex = 2;
            this.colItemGroup1.Width = 78;
            // 
            // colItemType1
            // 
            this.colItemType1.Caption = "Item Type";
            this.colItemType1.FieldName = "ItemType";
            this.colItemType1.Name = "colItemType1";
            this.colItemType1.Visible = true;
            this.colItemType1.VisibleIndex = 3;
            this.colItemType1.Width = 78;
            // 
            // colLocation1
            // 
            this.colLocation1.Caption = "Location";
            this.colLocation1.FieldName = "Location";
            this.colLocation1.Name = "colLocation1";
            this.colLocation1.OptionsColumn.AllowEdit = false;
            // 
            // colLocationDesc
            // 
            this.colLocationDesc.Caption = "Location Desc";
            this.colLocationDesc.FieldName = "LocationDesc";
            this.colLocationDesc.Name = "colLocationDesc";
            this.colLocationDesc.OptionsColumn.AllowEdit = false;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Description";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 1;
            this.colDescription1.Width = 196;
            // 
            // colBalanceQty1
            // 
            this.colBalanceQty1.Caption = "End Balance";
            this.colBalanceQty1.FieldName = "BalanceQty";
            this.colBalanceQty1.Name = "colBalanceQty1";
            this.colBalanceQty1.Visible = true;
            this.colBalanceQty1.VisibleIndex = 6;
            this.colBalanceQty1.Width = 78;
            // 
            // colBalance1
            // 
            this.colBalance1.Caption = "Begin Balance";
            this.colBalance1.FieldName = "Balance";
            this.colBalance1.Name = "colBalance1";
            this.colBalance1.Visible = true;
            this.colBalance1.VisibleIndex = 5;
            this.colBalance1.Width = 90;
            // 
            // colAvgCost1
            // 
            this.colAvgCost1.Caption = "Avg. Bal. Cost";
            this.colAvgCost1.FieldName = "AverageBalanceCost";
            this.colAvgCost1.Name = "colAvgCost1";
            // 
            // colAvgBFCost
            // 
            this.colAvgBFCost.Caption = "Avg. BF. Cost";
            this.colAvgBFCost.FieldName = "AverageBFCost";
            this.colAvgBFCost.Name = "colAvgBFCost";
            // 
            // colBalQtyAfterCSGN
            // 
            this.colBalQtyAfterCSGN.Caption = "Bal. Qty After CSGN";
            this.colBalQtyAfterCSGN.FieldName = "BalQtyAfterCSGN";
            this.colBalQtyAfterCSGN.Name = "colBalQtyAfterCSGN";
            // 
            // colBalanceBFWord1
            // 
            this.colBalanceBFWord1.Caption = "Balance String";
            this.colBalanceBFWord1.FieldName = "BalanceString";
            this.colBalanceBFWord1.Name = "colBalanceBFWord1";
            // 
            // colBalCostAfterCSGN
            // 
            this.colBalCostAfterCSGN.Caption = "Bal Cost After CSGN";
            this.colBalCostAfterCSGN.FieldName = "BalCostAfterCSGN";
            this.colBalCostAfterCSGN.Name = "colBalCostAfterCSGN";
            // 
            // colFurtherDescription
            // 
            this.colFurtherDescription.Caption = "FurtherDescription";
            this.colFurtherDescription.ColumnEdit = this.repositoryFurtherDescription;
            this.colFurtherDescription.FieldName = "FurtherDescription";
            this.colFurtherDescription.Name = "colFurtherDescription";
            // 
            // repositoryFurtherDescription
            // 
            this.repositoryFurtherDescription.Name = "repositoryFurtherDescription";
            this.repositoryFurtherDescription.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryFurtherDescription.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryFurtherDescription_ButtonPressed);
            this.repositoryFurtherDescription.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryFurtherDescription_ButtonPressed);
            // 
            // colUOM1
            // 
            this.colUOM1.Caption = "UOM";
            this.colUOM1.FieldName = "UOM";
            this.colUOM1.Name = "colUOM1";
            this.colUOM1.Visible = true;
            this.colUOM1.VisibleIndex = 4;
            this.colUOM1.Width = 78;
            // 
            // colSerialNoList
            // 
            this.colSerialNoList.Caption = "SerialNoList";
            this.colSerialNoList.FieldName = "SerialNoList";
            this.colSerialNoList.Name = "colSerialNoList";
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "WONo";
            this.gridColumn7.FieldName = "WONo";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // gvSubdetail
            // 
            this.gvSubdetail.GridControl = this.gctrReport;
            this.gvSubdetail.Name = "gvSubdetail";
            this.gvSubdetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvSubdetail_FocusedRowChanged);
            this.gvSubdetail.DoubleClick += new System.EventHandler(this.gvSubdetail_DoubleClick);
            this.gvSubdetail.Layout += new System.EventHandler(this.gvSubdetail_Layout);
            // 
            // gvItemSN
            // 
            this.gvItemSN.GridControl = this.gctrReport;
            this.gvItemSN.Name = "gvItemSN";
            // 
            // colLocation2
            // 
            this.colLocation2.FieldName = "Location";
            this.colLocation2.Name = "colLocation2";
            // 
            // colLastModified2
            // 
            this.colLastModified2.FieldName = "LastModified";
            this.colLastModified2.Name = "colLastModified2";
            // 
            // colFIFOSeq3
            // 
            this.colFIFOSeq3.FieldName = "FIFOSeq";
            this.colFIFOSeq3.Name = "colFIFOSeq3";
            // 
            // colFIFOQty3
            // 
            this.colFIFOQty3.FieldName = "FIFOQty";
            this.colFIFOQty3.Name = "colFIFOQty3";
            // 
            // colFIFOCost3
            // 
            this.colFIFOCost3.FieldName = "FIFOCost";
            this.colFIFOCost3.Name = "colFIFOCost3";
            // 
            // colSerialNo4
            // 
            this.colSerialNo4.FieldName = "SerialNumber";
            this.colSerialNo4.Name = "colSerialNo4";
            // 
            // colItemCode4
            // 
            this.colItemCode4.FieldName = "ItemCode";
            this.colItemCode4.Name = "colItemCode4";
            // 
            // colBatchNo4
            // 
            this.colBatchNo4.FieldName = "BatchNo";
            this.colBatchNo4.Name = "colBatchNo4";
            // 
            // colManufacturedDate4
            // 
            this.colManufacturedDate4.FieldName = "ManufacturedDate";
            this.colManufacturedDate4.Name = "colManufacturedDate4";
            // 
            // colExpiryDate4
            // 
            this.colExpiryDate4.FieldName = "ExpiryDate";
            this.colExpiryDate4.Name = "colExpiryDate4";
            // 
            // colLastSalesDate4
            // 
            this.colLastSalesDate4.FieldName = "LastSalesDate";
            this.colLastSalesDate4.Name = "colLastSalesDate4";
            // 
            // colRemark4
            // 
            this.colRemark4.FieldName = "Remarks";
            this.colRemark4.Name = "colRemark4";
            // 
            // colQty4
            // 
            this.colQty4.FieldName = "Qty";
            this.colQty4.Name = "colQty4";
            // 
            // colLocation4
            // 
            this.colLocation4.FieldName = "Location";
            this.colLocation4.Name = "colLocation4";
            // 
            // colCSGNQty4
            // 
            this.colCSGNQty4.FieldName = "CSGNQty";
            this.colCSGNQty4.Name = "colCSGNQty4";
            // 
            // colShelf1
            // 
            this.colShelf1.FieldName = "Shelf";
            this.colShelf1.Name = "colShelf1";
            // 
            // colCostingMethodString1
            // 
            this.colCostingMethodString1.FieldName = "CostingMethodString";
            this.colCostingMethodString1.Name = "colCostingMethodString1";
            // 
            // colBFCost
            // 
            this.colBFCost.FieldName = "TotalCost";
            this.colBFCost.Name = "colBFCost";
            // 
            // colBalanceCost1
            // 
            this.colBalanceCost1.FieldName = "BalanceCost";
            this.colBalanceCost1.Name = "colBalanceCost1";
            // 
            // colCSGNBalQty
            // 
            this.colCSGNBalQty.FieldName = "CSGNBalQty";
            this.colCSGNBalQty.Name = "colCSGNBalQty";
            // 
            // colCSGNCost
            // 
            this.colCSGNCost.FieldName = "CSGNCost";
            this.colCSGNCost.Name = "colCSGNCost";
            // 
            // colLocationDesc2
            // 
            this.colLocationDesc2.FieldName = "LocationDesc2";
            this.colLocationDesc2.Name = "colLocationDesc2";
            // 
            // pnCriteriaAdvanced
            // 
            this.pnCriteriaAdvanced.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnCriteriaAdvanced.Controls.Add(this.label8);
            this.pnCriteriaAdvanced.Controls.Add(this.cbBatchNo);
            this.pnCriteriaAdvanced.Controls.Add(this.pnOptionTop);
            this.pnCriteriaAdvanced.Controls.Add(this.gbFilter);
            this.pnCriteriaAdvanced.Controls.Add(this.pnBatch);
            this.pnCriteriaAdvanced.Controls.Add(this.gbReportOptions);
            this.pnCriteriaAdvanced.Controls.Add(this.gbOtherOptions);
            this.pnCriteriaAdvanced.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnCriteriaAdvanced.Location = new System.Drawing.Point(0, 118);
            this.pnCriteriaAdvanced.Name = "pnCriteriaAdvanced";
            this.pnCriteriaAdvanced.Size = new System.Drawing.Size(941, 190);
            this.pnCriteriaAdvanced.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(812, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 23);
            this.label8.TabIndex = 1;
            // 
            // cbBatchNo
            // 
            this.cbBatchNo.Location = new System.Drawing.Point(826, 121);
            this.cbBatchNo.Name = "cbBatchNo";
            this.cbBatchNo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbBatchNo.Size = new System.Drawing.Size(100, 20);
            this.cbBatchNo.TabIndex = 0;
            this.cbBatchNo.Visible = false;
            // 
            // pnOptionTop
            // 
            this.pnOptionTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnOptionTop.Location = new System.Drawing.Point(947, 18);
            this.pnOptionTop.Name = "pnOptionTop";
            this.pnOptionTop.Size = new System.Drawing.Size(32, 100);
            this.pnOptionTop.TabIndex = 0;
            this.pnOptionTop.Visible = false;
            // 
            // gbFilter
            // 
            this.gbFilter.Controls.Add(this.pnFilterBottom);
            this.gbFilter.Location = new System.Drawing.Point(3, 3);
            this.gbFilter.Name = "gbFilter";
            this.gbFilter.Size = new System.Drawing.Size(506, 178);
            this.gbFilter.TabIndex = 0;
            this.gbFilter.Text = "Advanced Filter";
            // 
            // pnFilterBottom
            // 
            this.pnFilterBottom.Controls.Add(this.ucWOSelector1);
            this.pnFilterBottom.Controls.Add(this.label15);
            this.pnFilterBottom.Controls.Add(this.label5);
            this.pnFilterBottom.Controls.Add(this.label6);
            this.pnFilterBottom.Controls.Add(this.label1);
            this.pnFilterBottom.Controls.Add(this.ucItemSelector1);
            this.pnFilterBottom.Controls.Add(this.ucItemGroupSelector1);
            this.pnFilterBottom.Controls.Add(this.label10);
            this.pnFilterBottom.Controls.Add(this.ucItemTypeSelector1);
            this.pnFilterBottom.Controls.Add(this.chkedtActiveItem);
            this.pnFilterBottom.Controls.Add(this.dateEditTo);
            this.pnFilterBottom.Controls.Add(this.label2);
            this.pnFilterBottom.Controls.Add(this.dateEditFrom);
            this.pnFilterBottom.Controls.Add(this.label3);
            this.pnFilterBottom.Controls.Add(this.chkedtInactiveItem);
            this.pnFilterBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnFilterBottom.Location = new System.Drawing.Point(2, 21);
            this.pnFilterBottom.Name = "pnFilterBottom";
            this.pnFilterBottom.Size = new System.Drawing.Size(502, 155);
            this.pnFilterBottom.TabIndex = 0;
            // 
            // ucWOSelector1
            // 
            this.ucWOSelector1.Location = new System.Drawing.Point(131, 96);
            this.ucWOSelector1.Name = "ucWOSelector1";
            this.ucWOSelector1.Size = new System.Drawing.Size(368, 26);
            this.ucWOSelector1.TabIndex = 12;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(13, 101);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 16);
            this.label15.TabIndex = 11;
            this.label15.Text = "WO No";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(13, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Item Group";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(13, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 15);
            this.label6.TabIndex = 7;
            this.label6.Text = "Item Type";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(272, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "to";
            // 
            // ucItemSelector1
            // 
            this.ucItemSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemSelector1.Location = new System.Drawing.Point(134, 31);
            this.ucItemSelector1.Name = "ucItemSelector1";
            this.ucItemSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucItemSelector1.TabIndex = 5;
            // 
            // ucItemGroupSelector1
            // 
            this.ucItemGroupSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemGroupSelector1.Location = new System.Drawing.Point(134, 75);
            this.ucItemGroupSelector1.Name = "ucItemGroupSelector1";
            this.ucItemGroupSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucItemGroupSelector1.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(13, 124);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 18);
            this.label10.TabIndex = 2;
            this.label10.Text = "Item Active Options";
            // 
            // ucItemTypeSelector1
            // 
            this.ucItemTypeSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemTypeSelector1.Location = new System.Drawing.Point(134, 53);
            this.ucItemTypeSelector1.Name = "ucItemTypeSelector1";
            this.ucItemTypeSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucItemTypeSelector1.TabIndex = 3;
            // 
            // chkedtActiveItem
            // 
            this.chkedtActiveItem.Location = new System.Drawing.Point(132, 121);
            this.chkedtActiveItem.Name = "chkedtActiveItem";
            this.chkedtActiveItem.Properties.Caption = "Print Active Item";
            this.chkedtActiveItem.Size = new System.Drawing.Size(107, 19);
            this.chkedtActiveItem.TabIndex = 1;
            // 
            // dateEditTo
            // 
            this.dateEditTo.EditValue = new System.DateTime(2019, 9, 29, 0, 0, 0, 0);
            this.dateEditTo.Location = new System.Drawing.Point(297, 9);
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Size = new System.Drawing.Size(128, 20);
            this.dateEditTo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Date Range";
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = new System.DateTime(2019, 9, 29, 0, 0, 0, 0);
            this.dateEditFrom.Location = new System.Drawing.Point(134, 9);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Size = new System.Drawing.Size(130, 20);
            this.dateEditFrom.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(13, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Stock Item";
            // 
            // chkedtInactiveItem
            // 
            this.chkedtInactiveItem.Location = new System.Drawing.Point(243, 121);
            this.chkedtInactiveItem.Name = "chkedtInactiveItem";
            this.chkedtInactiveItem.Properties.Caption = "Print Inactive Item";
            this.chkedtInactiveItem.Size = new System.Drawing.Size(132, 19);
            this.chkedtInactiveItem.TabIndex = 0;
            // 
            // pnBatch
            // 
            this.pnBatch.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBatch.Location = new System.Drawing.Point(900, 56);
            this.pnBatch.Name = "pnBatch";
            this.pnBatch.Size = new System.Drawing.Size(29, 100);
            this.pnBatch.TabIndex = 1;
            this.pnBatch.Visible = false;
            // 
            // gbReportOptions
            // 
            this.gbReportOptions.Controls.Add(this.chkedtShowCriteria);
            this.gbReportOptions.Controls.Add(this.cbGroupBy);
            this.gbReportOptions.Controls.Add(this.label9);
            this.gbReportOptions.Location = new System.Drawing.Point(513, 83);
            this.gbReportOptions.Name = "gbReportOptions";
            this.gbReportOptions.Size = new System.Drawing.Size(236, 72);
            this.gbReportOptions.TabIndex = 1;
            this.gbReportOptions.Text = "Report Options";
            // 
            // chkedtShowCriteria
            // 
            this.chkedtShowCriteria.Location = new System.Drawing.Point(5, 51);
            this.chkedtShowCriteria.Name = "chkedtShowCriteria";
            this.chkedtShowCriteria.Properties.Caption = "Show Criteria In Report";
            this.chkedtShowCriteria.Size = new System.Drawing.Size(160, 19);
            this.chkedtShowCriteria.TabIndex = 0;
            // 
            // cbGroupBy
            // 
            this.cbGroupBy.EditValue = "None";
            this.cbGroupBy.Location = new System.Drawing.Point(65, 26);
            this.cbGroupBy.Name = "cbGroupBy";
            this.cbGroupBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGroupBy.Properties.Items.AddRange(new object[] {
            "None",
            "Item Group",
            "Item Type"});
            this.cbGroupBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGroupBy.Size = new System.Drawing.Size(123, 20);
            this.cbGroupBy.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(5, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 17);
            this.label9.TabIndex = 2;
            this.label9.Text = "Group By";
            // 
            // gbOtherOptions
            // 
            this.gbOtherOptions.Controls.Add(this.cbUOMOption);
            this.gbOtherOptions.Controls.Add(this.label7);
            this.gbOtherOptions.Controls.Add(this.chkedtMergeSameCost);
            this.gbOtherOptions.Location = new System.Drawing.Point(513, 3);
            this.gbOtherOptions.Name = "gbOtherOptions";
            this.gbOtherOptions.Size = new System.Drawing.Size(236, 76);
            this.gbOtherOptions.TabIndex = 2;
            this.gbOtherOptions.Text = "Other Options";
            // 
            // cbUOMOption
            // 
            this.cbUOMOption.EditValue = "Show Multi-UOM";
            this.cbUOMOption.Location = new System.Drawing.Point(88, 48);
            this.cbUOMOption.Name = "cbUOMOption";
            this.cbUOMOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbUOMOption.Properties.Items.AddRange(new object[] {
            "Show Multi-UOM",
            "Show Default Report UOM",
            "Show Smallest UOM"});
            this.cbUOMOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbUOMOption.Size = new System.Drawing.Size(120, 20);
            this.cbUOMOption.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(5, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 18);
            this.label7.TabIndex = 0;
            this.label7.Text = "UOM Options";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // chkedtMergeSameCost
            // 
            this.chkedtMergeSameCost.Location = new System.Drawing.Point(6, 24);
            this.chkedtMergeSameCost.Name = "chkedtMergeSameCost";
            this.chkedtMergeSameCost.Properties.Caption = "Merge Same Cost";
            this.chkedtMergeSameCost.Size = new System.Drawing.Size(123, 19);
            this.chkedtMergeSameCost.TabIndex = 2;
            // 
            // chkIncludeZeroBalance
            // 
            this.chkIncludeZeroBalance.Location = new System.Drawing.Point(563, 82);
            this.chkIncludeZeroBalance.Name = "chkIncludeZeroBalance";
            this.chkIncludeZeroBalance.Properties.Caption = "Show Include Zero Balance";
            this.chkIncludeZeroBalance.Size = new System.Drawing.Size(210, 19);
            this.chkIncludeZeroBalance.TabIndex = 1;
            this.chkIncludeZeroBalance.CheckedChanged += new System.EventHandler(this.chkIncludeZeroBalance_CheckedChanged);
            // 
            // pnLocation
            // 
            this.pnLocation.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnLocation.Controls.Add(this.ucLocationSelector1);
            this.pnLocation.Location = new System.Drawing.Point(617, 15);
            this.pnLocation.Name = "pnLocation";
            this.pnLocation.Size = new System.Drawing.Size(200, 10);
            this.pnLocation.TabIndex = 2;
            this.pnLocation.Visible = false;
            // 
            // ucLocationSelector1
            // 
            this.ucLocationSelector1.Appearance.Options.UseBackColor = true;
            this.ucLocationSelector1.Location = new System.Drawing.Point(19, 19);
            this.ucLocationSelector1.LocationType = BCE.AutoCount.FilterUI.UCLocationType.ItemLocation;
            this.ucLocationSelector1.Name = "ucLocationSelector1";
            this.ucLocationSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucLocationSelector1.TabIndex = 0;
            // 
            // pnFilterTop
            // 
            this.pnFilterTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnFilterTop.Location = new System.Drawing.Point(636, 41);
            this.pnFilterTop.Name = "pnFilterTop";
            this.pnFilterTop.Size = new System.Drawing.Size(200, 13);
            this.pnFilterTop.TabIndex = 3;
            this.pnFilterTop.Visible = false;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(767, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 9;
            // 
            // chkIncludeNoTransactionItem
            // 
            this.chkIncludeNoTransactionItem.Location = new System.Drawing.Point(823, 31);
            this.chkIncludeNoTransactionItem.Name = "chkIncludeNoTransactionItem";
            this.chkIncludeNoTransactionItem.Size = new System.Drawing.Size(75, 19);
            this.chkIncludeNoTransactionItem.TabIndex = 0;
            this.chkIncludeNoTransactionItem.Visible = false;
            // 
            // pnOptionBottom
            // 
            this.pnOptionBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnOptionBottom.Location = new System.Drawing.Point(579, 63);
            this.pnOptionBottom.Name = "pnOptionBottom";
            this.pnOptionBottom.Size = new System.Drawing.Size(231, 22);
            this.pnOptionBottom.TabIndex = 1;
            this.pnOptionBottom.Visible = false;
            // 
            // sbtnInquiry
            // 
            this.sbtnInquiry.Location = new System.Drawing.Point(12, 9);
            this.sbtnInquiry.Name = "sbtnInquiry";
            this.sbtnInquiry.Size = new System.Drawing.Size(75, 23);
            this.sbtnInquiry.TabIndex = 3;
            this.sbtnInquiry.Text = "Inquiry";
            this.sbtnInquiry.Click += new System.EventHandler(this.sbtnInquiry_Click);
            // 
            // pnMedium
            // 
            this.pnMedium.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnMedium.Controls.Add(this.tabControl1);
            this.pnMedium.Controls.Add(this.pnBottom);
            this.pnMedium.Controls.Add(this.pnCriteriaAdvanced);
            this.pnMedium.Controls.Add(this.pnCriteriaBasic);
            this.pnMedium.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMedium.Location = new System.Drawing.Point(0, 66);
            this.pnMedium.Name = "pnMedium";
            this.pnMedium.Size = new System.Drawing.Size(941, 502);
            this.pnMedium.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 350);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabPage = this.tabPageResult;
            this.tabControl1.Size = new System.Drawing.Size(941, 152);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageResult,
            this.tabPageCriteria});
            // 
            // tabPageResult
            // 
            this.tabPageResult.Controls.Add(this.gctrReport);
            this.tabPageResult.Name = "tabPageResult";
            this.tabPageResult.Size = new System.Drawing.Size(935, 124);
            this.tabPageResult.Text = "Result";
            // 
            // tabPageCriteria
            // 
            this.tabPageCriteria.Controls.Add(this.memoEditCriteria);
            this.tabPageCriteria.Name = "tabPageCriteria";
            this.tabPageCriteria.Size = new System.Drawing.Size(935, 124);
            this.tabPageCriteria.Text = "Criteria";
            // 
            // memoEditCriteria
            // 
            this.memoEditCriteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEditCriteria.Location = new System.Drawing.Point(0, 0);
            this.memoEditCriteria.Name = "memoEditCriteria";
            this.memoEditCriteria.Properties.Appearance.Options.UseFont = true;
            this.memoEditCriteria.Properties.ReadOnly = true;
            this.memoEditCriteria.Size = new System.Drawing.Size(935, 124);
            this.memoEditCriteria.TabIndex = 0;
            this.memoEditCriteria.UseOptimizedRendering = true;
            // 
            // pnBottom
            // 
            this.pnBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBottom.Controls.Add(this.sbtnPrint);
            this.pnBottom.Controls.Add(this.sbtnPreview);
            this.pnBottom.Controls.Add(this.sbtnClose);
            this.pnBottom.Controls.Add(this.sbtnInquiry);
            this.pnBottom.Controls.Add(this.sbtnToggleOptions);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnBottom.Location = new System.Drawing.Point(0, 308);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(941, 42);
            this.pnBottom.TabIndex = 1;
            // 
            // sbtnPrint
            // 
            this.sbtnPrint.Location = new System.Drawing.Point(172, 9);
            this.sbtnPrint.Name = "sbtnPrint";
            this.sbtnPrint.ReportType = "";
            this.sbtnPrint.Size = new System.Drawing.Size(72, 23);
            this.sbtnPrint.TabIndex = 0;
            this.sbtnPrint.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.sbtnPrint_Print);
            // 
            // sbtnPreview
            // 
            this.sbtnPreview.Location = new System.Drawing.Point(94, 9);
            this.sbtnPreview.Name = "sbtnPreview";
            this.sbtnPreview.ReportType = "";
            this.sbtnPreview.Size = new System.Drawing.Size(72, 23);
            this.sbtnPreview.TabIndex = 1;
            this.sbtnPreview.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.sbtnPreview_Preview);
            // 
            // sbtnClose
            // 
            this.sbtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnClose.Location = new System.Drawing.Point(331, 9);
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Size = new System.Drawing.Size(75, 23);
            this.sbtnClose.TabIndex = 2;
            this.sbtnClose.Text = "Close";
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // sbtnToggleOptions
            // 
            this.sbtnToggleOptions.Location = new System.Drawing.Point(250, 9);
            this.sbtnToggleOptions.Name = "sbtnToggleOptions";
            this.sbtnToggleOptions.Size = new System.Drawing.Size(75, 23);
            this.sbtnToggleOptions.TabIndex = 4;
            this.sbtnToggleOptions.Text = "Options";
            this.sbtnToggleOptions.Click += new System.EventHandler(this.sbtnToggleOptions_Click);
            // 
            // pnCriteriaBasic
            // 
            this.pnCriteriaBasic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnCriteriaBasic.Controls.Add(this.chkIncludeNoTransactionItem);
            this.pnCriteriaBasic.Controls.Add(this.chkIncludeZeroBalance);
            this.pnCriteriaBasic.Controls.Add(this.label4);
            this.pnCriteriaBasic.Controls.Add(this.pnOptionBottom);
            this.pnCriteriaBasic.Controls.Add(this.pnFilterTop);
            this.pnCriteriaBasic.Controls.Add(this.groupBox1);
            this.pnCriteriaBasic.Controls.Add(this.pnLocation);
            this.pnCriteriaBasic.Controls.Add(this.sbtnAdvOptions);
            this.pnCriteriaBasic.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnCriteriaBasic.Location = new System.Drawing.Point(0, 0);
            this.pnCriteriaBasic.Name = "pnCriteriaBasic";
            this.pnCriteriaBasic.Size = new System.Drawing.Size(941, 118);
            this.pnCriteriaBasic.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ucWOSelector2);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.dateEditFrom2);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.dateEditTo2);
            this.groupBox1.Controls.Add(this.ucItemSelector2);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(557, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.Text = "Basic Filter";
            // 
            // ucWOSelector2
            // 
            this.ucWOSelector2.Location = new System.Drawing.Point(68, 68);
            this.ucWOSelector2.Name = "ucWOSelector2";
            this.ucWOSelector2.Size = new System.Drawing.Size(449, 26);
            this.ucWOSelector2.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(7, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 16);
            this.label14.TabIndex = 9;
            this.label14.Text = "WO No";
            // 
            // dateEditFrom2
            // 
            this.dateEditFrom2.EditValue = new System.DateTime(2019, 9, 29, 0, 0, 0, 0);
            this.dateEditFrom2.Location = new System.Drawing.Point(71, 25);
            this.dateEditFrom2.Name = "dateEditFrom2";
            this.dateEditFrom2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom2.Size = new System.Drawing.Size(128, 20);
            this.dateEditFrom2.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(7, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 16);
            this.label12.TabIndex = 1;
            this.label12.Text = "Date Range";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(7, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 23);
            this.label13.TabIndex = 2;
            this.label13.Text = "Stock Item";
            // 
            // dateEditTo2
            // 
            this.dateEditTo2.EditValue = new System.DateTime(2019, 9, 29, 0, 0, 0, 0);
            this.dateEditTo2.Location = new System.Drawing.Point(238, 25);
            this.dateEditTo2.Name = "dateEditTo2";
            this.dateEditTo2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo2.Size = new System.Drawing.Size(128, 20);
            this.dateEditTo2.TabIndex = 3;
            // 
            // ucItemSelector2
            // 
            this.ucItemSelector2.Appearance.Options.UseBackColor = true;
            this.ucItemSelector2.Location = new System.Drawing.Point(71, 47);
            this.ucItemSelector2.Name = "ucItemSelector2";
            this.ucItemSelector2.Size = new System.Drawing.Size(362, 20);
            this.ucItemSelector2.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(212, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 16);
            this.label11.TabIndex = 5;
            this.label11.Text = "to";
            // 
            // sbtnAdvOptions
            // 
            this.sbtnAdvOptions.Location = new System.Drawing.Point(0, 0);
            this.sbtnAdvOptions.Name = "sbtnAdvOptions";
            this.sbtnAdvOptions.Size = new System.Drawing.Size(75, 23);
            this.sbtnAdvOptions.TabIndex = 1;
            this.sbtnAdvOptions.Click += new System.EventHandler(this.sbtnAdvOptions_Click);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(935, 133);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Report";
            this.barSubItem2.Id = 7;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barDesignReport2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barReportOptions),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSwitchOptions)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barDesignReport2
            // 
            this.barDesignReport2.Caption = "Design Stock Card Report";
            this.barDesignReport2.Id = 8;
            this.barDesignReport2.Name = "barDesignReport2";
            this.barDesignReport2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick_1);
            // 
            // barReportOptions
            // 
            this.barReportOptions.Caption = "Report Options";
            this.barReportOptions.Id = 9;
            this.barReportOptions.Name = "barReportOptions";
            this.barReportOptions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barSwitchOptions
            // 
            this.barSwitchOptions.Caption = "Switch to Basic Options";
            this.barSwitchOptions.Id = 10;
            this.barSwitchOptions.Name = "barSwitchOptions";
            this.barSwitchOptions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 22);
            this.barDockControlTop.Size = new System.Drawing.Size(941, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 568);
            this.barDockControlBottom.Size = new System.Drawing.Size(941, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 546);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(941, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 546);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 1;
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnDesignReport
            // 
            this.barBtnDesignReport.Id = 2;
            this.barBtnDesignReport.Name = "barBtnDesignReport";
            this.barBtnDesignReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barbtnAdvancedOptions
            // 
            this.barbtnAdvancedOptions.Id = 5;
            this.barbtnAdvancedOptions.Name = "barbtnAdvancedOptions";
            this.barbtnAdvancedOptions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnAdvancedOptions_ItemClick);
            // 
            // barbtnViewSourceDoc
            // 
            this.barbtnViewSourceDoc.Id = 4;
            this.barbtnViewSourceDoc.Name = "barbtnViewSourceDoc";
            this.barbtnViewSourceDoc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnViewSourceDoc_ItemClick);
            // 
            // popupMenuReport
            // 
            this.popupMenuReport.Manager = this.barManager1;
            this.popupMenuReport.Name = "popupMenuReport";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem3,
            this.barButtonItem1,
            this.barButtonItem3,
            this.barButtonItem4});
            this.barManager1.MainMenu = this.bar3;
            this.barManager1.MaxItemId = 4;
            this.barManager1.QueryShowPopupMenu += new DevExpress.XtraBars.QueryShowPopupMenuEventHandler(this.barManager1_QueryShowPopupMenu);
            // 
            // bar3
            // 
            this.bar3.BarName = "Main menu";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem3)});
            this.bar3.OptionsBar.MultiLine = true;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Main menu";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Report";
            this.barSubItem3.Id = 0;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Design Stock Card WIP Report";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Report Options";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Switch to Advanced Options";
            this.barButtonItem4.Id = 3;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnAdvancedOptions_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(941, 22);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 568);
            this.barDockControl2.Size = new System.Drawing.Size(941, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 22);
            this.barDockControl3.Size = new System.Drawing.Size(0, 546);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(941, 22);
            this.barDockControl4.Size = new System.Drawing.Size(0, 546);
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Stock Card WIP";
            this.panelHeader1.HelpTopicId = "Stock_Card_Report.htm";
            this.panelHeader1.Location = new System.Drawing.Point(0, 22);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(941, 44);
            this.panelHeader1.TabIndex = 2;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // FormStockCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnClose;
            this.ClientSize = new System.Drawing.Size(941, 568);
            this.Controls.Add(this.pnMedium);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.KeyPreview = true;
            this.Name = "FormStockCard";
            this.Text = "Stock Card WIP";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.FormStockCards_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormStockCard_FormClosed);
            this.Load += new System.EventHandler(this.FormStockCards_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormStockCards_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gctrReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryFurtherDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSubdetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvItemSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaAdvanced)).EndInit();
            this.pnCriteriaAdvanced.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbBatchNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnOptionTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbFilter)).EndInit();
            this.gbFilter.ResumeLayout(false);
            this.pnFilterBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkedtActiveItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtInactiveItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbReportOptions)).EndInit();
            this.gbReportOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkedtShowCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGroupBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbOtherOptions)).EndInit();
            this.gbOtherOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbUOMOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtMergeSameCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeZeroBalance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLocation)).EndInit();
            this.pnLocation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnFilterTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeNoTransactionItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnOptionBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMedium)).EndInit();
            this.pnMedium.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageResult.ResumeLayout(false);
            this.tabPageCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnBottom)).EndInit();
            this.pnBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaBasic)).EndInit();
            this.pnCriteriaBasic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        public class FormEventArgs
        {
            private FormStockCard myForm;

            public DBSetting DBSetting
            {
                get
                {
                    return this.myForm.myDBSetting;
                }
            }

            public PanelControl PanelCriteriaBasic
            {
                get
                {
                    return this.myForm.pnCriteriaBasic;
                }
            }

            public PanelControl PanelCriteriaAdvance
            {
                get
                {
                    return this.myForm.pnCriteriaAdvanced;
                }
            }

            public PanelControl PanelButtons
            {
                get
                {
                    return this.myForm.pnBottom;
                }
            }

            public XtraTabControl TabControl
            {
                get
                {
                    return this.myForm.tabControl1;
                }
            }

            public FormEventArgs(FormStockCard form)
            {
                this.myForm = form;
            }
        }

        public class FormInitializeEventArgs : FormStockCard.FormEventArgs
        {
            public FormInitializeEventArgs(FormStockCard form)
              : base(form)
            {
            }
        }

        public class FormInquiryEventArgs : FormStockCard.FormEventArgs
        {
            private DataSet myResultDataSet;

            public DataSet ResultDataSet
            {
                get
                {
                    return this.myResultDataSet;
                }
            }

            public DataTable ResultTable
            {
                get
                {
                    return this.myResultDataSet.Tables["Master"];
                }
            }

            public DataTable DetailTable
            {
                get
                {
                    return this.myResultDataSet.Tables["Detail"];
                }
            }

            public DataTable SubDetailTable
            {
                get
                {
                    return this.myResultDataSet.Tables["SubDetail"];
                }
            }

            public DataTable ItemSerialNoTable
            {
                get
                {
                    return this.myResultDataSet.Tables["ItemSerialNo"];
                }
            }

            public FormInquiryEventArgs(FormStockCard form, DataSet resultDataSet)
              : base(form)
            {
                this.myResultDataSet = resultDataSet;
            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void barButtonItem1_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            this.myFormHelper.Criteria.GroupBy = (GroupByItemGroupTypeLocationOption)this.cbGroupBy.SelectedIndex;
            this.myFormHelper.Criteria.IsShowCriteria = this.chkedtShowCriteria.Checked;
            this.myFormHelper.DesignReport();
            Cursor.Current = current;
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormBasicReportOption.ShowReportOption(this.myFormHelper.ReportOption);
            this.myFormHelper.SaveReportOption();
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!this.pnCriteriaAdvanced.Visible)
            {
                this.dateEditFrom.DateTime = this.dateEditFrom2.DateTime;
                this.dateEditTo.DateTime = this.dateEditTo2.DateTime;
            }
            else
            {
                this.dateEditFrom2.DateTime = this.dateEditFrom.DateTime;
                this.dateEditTo2.DateTime = this.dateEditTo.DateTime;
            }
            this.SaveCriteria();
            this.SuspendLayout();
            if (this.myFormHelper.Criteria.AdvancedOptions)
            {
                this.barSwitchOptions.Caption = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_AdvancedOptions, new object[0]);
                this.pnCriteriaBasic.Visible = true;
                this.pnCriteriaAdvanced.Visible = false;
                this.myFormHelper.Criteria.AdvancedOptions = false;
                this.ucItemSelector2.ApplyFilter(this.myFormHelper.Criteria.StockItemFilter);
                this.ucWOSelector2.ApplyFilter(this.myFormHelper.Criteria.WONoFilter);
            }
            else
            {
                this.barSwitchOptions.Caption = BCE.Localization.Localizer.GetString((Enum)StockCardStringId.Code_BasicOptions, new object[0]);
                this.pnCriteriaBasic.Visible = false;
                this.pnCriteriaAdvanced.Visible = true;
                this.myFormHelper.Criteria.AdvancedOptions = true;
                this.SetModuleFeature();
                this.ucItemGroupSelector1.ApplyFilter(this.myFormHelper.Criteria.StockGroupFilter);
                this.ucItemSelector1.ApplyFilter(this.myFormHelper.Criteria.StockItemFilter);
                this.ucWOSelector1.ApplyFilter(this.myFormHelper.Criteria.WONoFilter);
                this.ucItemTypeSelector1.ApplyFilter(this.myFormHelper.Criteria.StockItemTypeFilter);
                this.ucLocationSelector1.ApplyFilter(this.myFormHelper.Criteria.LocationFilter);
            }
            this.ResumeLayout();
        }
    }
}
