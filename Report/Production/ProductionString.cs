﻿// Type: BCE.AutoCount.Manufacturing.Production.ProductionString
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Localization;

namespace Production.Report.Production
{
  [LocalizableString]
  public enum ProductionString
  {
    [DefaultString("Batch Print Production / Print Production Listing")] BatchPrintProduction_PrintProductionListing,
    [DefaultString("Print Production Detail Listing")] PrintProductionDetailListing,
    [DefaultString("Print Outstanding Production Listing")] PrintOutstandingProductionListing,
    [DefaultString("{0} opened Production List window.")] OpenProduction,
    [DefaultString("Production {0}")] Production,
    [DefaultString("{0} viewed Production {1} in edit mode.")] ViewProductionInEditMode,
    [DefaultString("{0} viewed Production {1}.")] ViewProduction,
    [DefaultString("{0} edited Production {1}.")] EditedProduction,
    [DefaultString("{0} cancels Production {1}.")] CancelProduction,
    [DefaultString("{0} un-cancels Production {1}.")] UncancelProduction,
    [DefaultString("{0} created new Production {1}.")] CreatedNewProduction,
    [DefaultString("{0} updated Production {1}.")] UpdatedProduction,
    [DefaultString("{0} deleted Production {1}.")] DeletedProduction,
    [DefaultString("{0} opened Print Production Detail Listing window.")] OpenedPrintProductionDetailListing,
    [DefaultString("{0} inquired on Print Production Detail Listing.")] InquiredPrintProductionDetailListing,
    [DefaultString("Production Detail Listing")] ProductionDetailListing,
    [DefaultString("{0} opened Print Production Listing window.")] OpenedPrintProductionListing,
    [DefaultString("{0} inquired on Print Production Listing.")] InquiredPrintProductionListing,
    [DefaultString("{0} inquired on Batch Print Production.")] InquiredBatchPrintProduction,
    [DefaultString("Batch Production")] BatchProduction,
    [DefaultString("Production Listing")] ProductionListing,
    [DefaultString("{0} opened Print Outstanding Production Listing window.")] OpenedPrintOutstandingProductionListing,
    [DefaultString("{0} inquired on Print Outstanding Production Listing.")] InquiredPrintOutstandingProductionListing,
    [DefaultString("Outstanding Production Listing.")] OutstandingProductionListing,
  }
}
