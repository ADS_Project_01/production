﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderReportCommand
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount;
using BCE.AutoCount.Report;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using BCE.Localization;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Production.Report.Production
{
  public class ProductionReportCommand
  {
    private BasicReportOption myReportOption;
    protected DBSetting myDBSetting;
    protected const string MasterTableName = "Master";
    protected const string DetailTableName = "Detail";
    public const string ListingReportStyle = "Production Listing";

    public BasicReportOption ReportOption
    {
      get
      {
        return this.myReportOption;
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    public static ProductionReportCommand Create(DBSetting dbSetting, BasicReportOption reportOption)
    {
      ProductionReportCommand orderReportCommand = (ProductionReportCommand) null;
      if (dbSetting.ServerType == DBServerType.SQL2000)
        orderReportCommand = (ProductionReportCommand) new ProductionReportCommandSQL();
      else
        dbSetting.ThrowServerTypeNotSupportedException();
      orderReportCommand.myDBSetting = dbSetting;
      orderReportCommand.myReportOption = reportOption;
      return orderReportCommand;
    }

    protected string GetWhereSQL(ProductionReportingCriteria reportingCriteria, SqlCommand cmd)
    {
      string str1 = "";
      SearchCriteria searchCriteria = new SearchCriteria();
      BCE.AutoCount.SearchFilter.Filter dateFilter = reportingCriteria.DateFilter;
      searchCriteria.AddFilter(dateFilter);
      BCE.AutoCount.SearchFilter.Filter documentFilter = reportingCriteria.DocumentFilter;
      searchCriteria.AddFilter(documentFilter);
            BCE.AutoCount.SearchFilter.Filter itemCodeFilter = reportingCriteria.ItemCodeFilter;
            searchCriteria.AddFilter(itemCodeFilter);
            //BCE.AutoCount.SearchFilter.Filter itemGroupFilter = reportingCriteria.ItemGroupFilter;
            //searchCriteria.AddFilter(itemGroupFilter);
            //BCE.AutoCount.SearchFilter.Filter itemTypeFilter = reportingCriteria.ItemTypeFilter;
            //searchCriteria.AddFilter(itemTypeFilter);
            //BCE.AutoCount.SearchFilter.Filter locationFilter = reportingCriteria.LocationFilter;
            //searchCriteria.AddFilter(locationFilter);
            //BCE.AutoCount.SearchFilter.Filter projecNoFilter = reportingCriteria.ProjecNoFilter;
            //searchCriteria.AddFilter(projecNoFilter);
            //BCE.AutoCount.SearchFilter.Filter deptNoFilter = reportingCriteria.DeptNoFilter;
            //searchCriteria.AddFilter(deptNoFilter);
            int num = 1;
      searchCriteria.MatchAll = num != 0;
      SqlCommand sqlCommand = cmd;
      string str2 = searchCriteria.BuildSQL((IDbCommand) sqlCommand);
      if (str2.Length > 0)
        str1 = str2;
      //if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.UnCancelled)
      //  str1 = str1.Length <= 0 ? " RPA_WO.Cancelled = 'F' " : str1 + " And RPA_WO.Cancelled = 'F' ";
      //else if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
      //  str1 = str1.Length <= 0 ? " RPA_WO.Cancelled = 'T' " : str1 + " And RPA_WO.Cancelled = 'T' ";
      return str1;
    }

    public void PrintListingReport(string dtlKeys, ProductionReportingCriteria criteria)
    {
      string criteriaText = "";
      foreach (string str in criteria.ReadableTextArray)
        criteriaText = criteriaText + str + "\n";
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) ProductionString.ProductionListing, new object[0]), "RPA_WO_DTLLIST_REPORT_PRINT", "RPA_WO_DTLLIST_REPORT_EXPORT", criteriaText);
      ReportTool.PrintReport("Production Listing", this.GetListingReportDataSource(dtlKeys, criteria), this.myDBSetting, false, this.myReportOption, reportInfo);
    }

    public void PrintListingReport(string dtlKeys, ProductionReportingCriteria criteria, bool defaultValue)
    {
      string criteriaText = "";
      foreach (string str in criteria.ReadableTextArray)
        criteriaText = criteriaText + str + "\n";
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) ProductionString.ProductionListing, new object[0]), "RPA_WO_DTLLIST_REPORT_PRINT", "RPA_WO_DTLLIST_REPORT_EXPORT", criteriaText);
      ReportTool.PrintReport("Production Listing", this.GetListingReportDataSource(dtlKeys, criteria), this.myDBSetting, defaultValue, this.myReportOption, reportInfo);
    }

    public void PreviewListingReport(string dtlKeys, ProductionReportingCriteria criteria)
    {
      string criteriaText = "";
      foreach (string str in criteria.ReadableTextArray)
        criteriaText = criteriaText + str + "\n";
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) ProductionString.ProductionListing, new object[0]), "RPA_WO_DTLLIST_REPORT_PRINT", "RPA_WO_DTLLIST_REPORT_EXPORT", criteriaText);
      ReportTool.PreviewReport("Production Listing", this.GetListingReportDataSource(dtlKeys, criteria), this.myDBSetting, true, false, this.myReportOption, reportInfo);
    }

    public void PreviewListingReport(string dtlKeys, ProductionReportingCriteria criteria, bool defaultValue)
    {
      string criteriaText = "";
      foreach (string str in criteria.ReadableTextArray)
        criteriaText = criteriaText + str + "\n";
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) ProductionString.ProductionListing, new object[0]), "RPA_WO_DTLLIST_REPORT_PRINT", "RPA_WO_DTLLIST_REPORT_EXPORT", criteriaText);
      ReportTool.PreviewReport("Production Listing", this.GetListingReportDataSource(dtlKeys, criteria), this.myDBSetting, defaultValue, false, this.myReportOption, reportInfo);
    }

    private DocumentReportDataSet PreparingListingReportDataSet(DataSet dsReportData, ProductionReportingCriteria criteria, string dataName)
    {
      DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, dataName, "Production Master", "Production Detail");
      documentReportDataSet.Tables.Add(dsReportData.Tables[0].Copy());
            documentReportDataSet.Tables.Add(dsReportData.Tables[1].Copy());
            documentReportDataSet.Relations.Add("relMasterDtl", documentReportDataSet.Tables[0].Columns["DtlKey"], documentReportDataSet.Tables[1].Columns["FromDocDtlKey"], false);

            DataTable table = new DataTable("Report Option");
      DataColumn column1 = new DataColumn("Criteria", typeof (string));
      table.Columns.Add(column1);
      DataColumn column2 = new DataColumn("ShowCriteria", typeof (string));
      table.Columns.Add(column2);
      DataColumn column3 = new DataColumn("GroupBy", typeof (string));
      table.Columns.Add(column3);
      DataColumn column4 = new DataColumn("SortBy", typeof (string));
      table.Columns.Add(column4);
      if (criteria != null)
      {
        DataRow row = table.NewRow();
        string str1 = "";
        foreach (string str2 in criteria.ReadableTextArray)
          str1 = str1 + str2 + "\n";
        row["Criteria"] = (object) str1;
        row["ShowCriteria"] = criteria.IsShowCriteria ? (object) "Yes" : (object) "No";
        row["GroupBy"] = (object) criteria.GroupBy;
        row["SortBy"] = (object) criteria.SortBy;
        table.Rows.Add(row);
      }
      else
      {
        DataRow row = table.NewRow();
        row["Criteria"] = (object) "Default criteria";
        row["ShowCriteria"] = (object) "Yes";
        row["GroupBy"] = (object) "Item Code";
        row["SortBy"] = (object) "Date";
        table.Rows.Add(row);
      }
      documentReportDataSet.Tables.Add(table);
      documentReportDataSet.AddDefaultTables();
      return documentReportDataSet;
    }

    public object GetListingReportDataSource(string dtlKeys, ProductionReportingCriteria criteria)
    {
      return (object) this.PreparingListingReportDataSet(this.LoadListingReportData(dtlKeys, criteria), criteria, "Production Listing");
    }

    public object GetListingReportDesignerDataSource()
    {
      return (object) this.PreparingListingReportDataSet(this.LoadListingReportDesignerData(), (ProductionReportingCriteria) null, "Production Listing");
    }

    protected virtual DataSet LoadListingReportData(string dtlKeys, ProductionReportingCriteria criteria)
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadListingReportDesignerData()
    {
      return (DataSet) null;
    }

    public virtual void BasicSearch(ProductionReportingCriteria criteria, string columnName, DataSet newDS, string chkEditColumnName)
    {
    }

    
  }
}
