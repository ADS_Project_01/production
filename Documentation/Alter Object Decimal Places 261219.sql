alter table rpa_WODtl
alter column RateUOM decimal(25, 20)
GO
alter table rpa_WODtl
alter column Rate decimal(25, 20)
GO
alter table rpa_WODtl
alter column Qty decimal(25, 20)
GO
alter table rpa_WODtl
alter column ProductRatio decimal(25, 20)
GO
alter table RPA_WOProduct
alter column Qty decimal(25, 20)
GO
alter table RPA_WOProduct
alter column Rate decimal(25, 20)