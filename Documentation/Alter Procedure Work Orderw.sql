
ALTER PROCEDURE [dbo].[RPA_BOM_Hierarchy_p](@BOMCode nvarchar(100))
as
WITH Tree
AS (
SELECT
    a.BOMCode id,
    a.ItemCode,
    a.BOMCode as parent,
    0 AS Level,
    a.BOMCode AS Root,
    CAST(a.BOMCode AS VARCHAR(MAX)
    ) AS Sort
from 
RPA_ItemBOM a with(NOLOCK) inner join RPA_ItemBOMDtl b  with(NOLOCK) on a.DocKey=b.DocKey where a.BOMCode=@BOMCode
UNION ALL
SELECT 
    st.ChildBOMCode id,
    st.ItemCode2 as ItemCode,
    st.BOMCode as parent,
    Level + 1 AS Level,
    st.BOMCode  AS Root,
   -- st.hasRights AS HasRights,
    uh.sort + CASE 1 WHEN 0 THEN '' ELSE '/' + CAST(st.ChildBOMCode AS VARCHAR(20)) END AS Sort
FROM (select a.*,b.BOMCode as ChildBOMCode,B.ItemCode AS ItemCode2 from RPA_ItemBOM a with(NOLOCK) inner join RPA_ItemBOMDtl b  with(NOLOCK) on a.DocKey=b.DocKey) AS st
    JOIN Tree uh ON uh.id = st.BOMCode    
)
SELECT distinct id,parent,ItemCode,Level,Root FROM Tree AS t   where Sort is not null 