USE [AED_PRODUKSI_141119]
GO

/****** Object:  View [dbo].[vRPA_StockReceive]    Script Date: 12/17/2019 06:34:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vRPA_StockReceive] AS
SELECT A.DocKey, A.DocNo, A.DocDate, A.Description, A.Total, A.Note,A.DocType,
A.Remark1, A.Remark2, A.Remark3, A.Remark4, A.PrintCount, A.Cancelled,
A.LastModified, A.LastModifiedUserID, B.UserName AS LastModifiedUserName,
A.CreatedTimeStamp, A.CreatedUserID, C.UserName AS CreatedUserName,
A.ExternalLink, A.RefDocNo, A.CanSync
,A.MachineCode, D.Description as MachineName,A.JENo,A.JEKey
FROM RPA_RCV A
LEFT OUTER JOIN Users B ON (A.LastModifiedUserID=B.UserID)
LEFT OUTER JOIN Users C ON (A.CreatedUserID=C.UserID)
LEFT OUTER JOIN RPA_Machine D ON (A.MachineCode=D.MachineCode)

GO
ALTER VIEW [dbo].[vRPA_StockReceiveDetail] AS
SELECT A.DtlKey, A.DocKey, A.Seq, A.Numbering,A.FromDocNo,A.FromDocType,A.FromDocDtlKey,
B.*,
C.*,
D.*,
A.Description, A.FurtherDescription,
E.*,
F.*,
G.*,
A.Qty, A.UnitCost, A.SubTotal, A.PrintOut, A.SerialNoList,
H.BalQty AS LocationBalQty, I.BalQty AS BatchBalQty
, A.Debit, A.Credit
FROM RPA_RCVDTL A
LEFT OUTER JOIN vItem B ON (A.ItemCode=B.ItemCode)
LEFT OUTER JOIN vItemBatch C ON (A.ItemCode=C.BatchItemCode AND A.BatchNo=C.BatchNo)
LEFT OUTER JOIN vLocation D ON (A.Location=D.Location)
LEFT OUTER JOIN vProject E ON (A.ProjNo=E.ProjNo)
LEFT OUTER JOIN vDept F ON (A.DeptNo=F.DeptNo)
LEFT OUTER JOIN vItemUOM G ON (A.ItemCode=G.UOMItemCode AND A.UOM=G.UOM)
LEFT OUTER JOIN ItemBalQty H ON (A.ItemCode=H.ItemCode AND A.UOM=H.UOM AND A.Location=H.Location)
LEFT OUTER JOIN ItemBatchBalQty I ON (A.ItemCode=I.ItemCode AND A.UOM=I.UOM AND A.Location=I.Location AND A.BatchNo=I.BatchNo)

GO
ALTER VIEW [dbo].[vRPA_StockReceiveRM] AS
SELECT A.DocKey, A.DocNo, A.DocDate, A.Description, A.Total, A.Note, A.BatchNo,A.DocType,
A.Remark1, A.Remark2, A.Remark3, A.Remark4, A.PrintCount, A.Cancelled,
A.LastModified, A.LastModifiedUserID, B.UserName AS LastModifiedUserName,
A.CreatedTimeStamp, A.CreatedUserID, C.UserName AS CreatedUserName,
A.ExternalLink, A.RefDocNo, A.CanSync
,A.JENo,A.JEKey
FROM RPA_RCVRM A
LEFT OUTER JOIN Users B ON (A.LastModifiedUserID=B.UserID)
LEFT OUTER JOIN Users C ON (A.CreatedUserID=C.UserID)

GO
ALTER VIEW [dbo].[vRPA_StockReceiveRMDetail] AS
SELECT A.DtlKey, A.DocKey, A.Seq, A.Numbering,A.FromDocNo,A.FromDocType,A.FromDocDtlKey,
B.*,
C.*,
D.*,
A.Description, A.FurtherDescription,
E.*,
F.*,
G.*,
A.Qty, A.UnitCost, A.SubTotal, A.PrintOut, A.SerialNoList,
H.BalQty AS LocationBalQty, I.BalQty AS BatchBalQty
, A.Debit, A.Credit
FROM RPA_RCVRMDTL A
LEFT OUTER JOIN vItem B ON (A.ItemCode=B.ItemCode)
LEFT OUTER JOIN vItemBatch C ON (A.ItemCode=C.BatchItemCode AND A.BatchNo=C.BatchNo)
LEFT OUTER JOIN vLocation D ON (A.Location=D.Location)
LEFT OUTER JOIN vProject E ON (A.ProjNo=E.ProjNo)
LEFT OUTER JOIN vDept F ON (A.DeptNo=F.DeptNo)
LEFT OUTER JOIN vItemUOM G ON (A.ItemCode=G.UOMItemCode AND A.UOM=G.UOM)
LEFT OUTER JOIN ItemBalQty H ON (A.ItemCode=H.ItemCode AND A.UOM=H.UOM AND A.Location=H.Location)
LEFT OUTER JOIN ItemBatchBalQty I ON (A.ItemCode=I.ItemCode AND A.UOM=I.UOM AND A.Location=I.Location AND A.BatchNo=I.BatchNo)



