ALTER view [dbo].[vRPA_BatchNoWO]
AS
SELECT a.DocNo,a.DocDate,a.DocKey, c.ItemCode,c.Description as ItemDescription,d.UOM,d.BatchNo,e.Description,e.ManufacturedDate,e.ExpiryDate,e.BalQty FROM RPA_WO a inner join RPA_WOProduct b on a.DocKey=b.DocKey inner join RPA_WODtl c on b.DtlKey=c.FromDocDtlKey inner join ItemBatchBalQty d on c.ItemCode=d.ItemCode and d.UOM=c.UOM inner join ItemBatch e on e.ItemCode=d.ItemCode and e.BatchNo=d.BatchNo where d.BalQty>0 
GO


