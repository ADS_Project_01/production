ALTER view [dbo].[vRPA_WorkOrderProduct]
as
select [DtlKey]
      ,[DocKey]
      ,[Seq]
      ,[Numbering]
      ,[Description]
      ,[UOM]
      ,[Rate]
      ,[Qty]
      ,[BOMCode]
      ,[TransferedQty]
      ,[Status]
      ,[COGM]
      ,[FromDocType]
      ,[FromDocNo]
      ,[FromDocDtlKey]
      ,[DebtorCode]
      ,[DebtorName]
      ,[Style]
      ,[Length]
      ,[UOMLength]
      ,[Width]
      ,[UOMWidth]
      ,[Size]
      ,[Tickness]
      ,[PrintOut]
      ,[Guid]
      ,[Weight]
      ,[WeightUOM]
      ,[Volume]
      ,[VolumeUOM]
      ,[Cancelled]
      ,[CancelledUserID]
      ,[CancelledDate]
      ,[ActHpp],b.*,c.* from RPA_WOProduct a with(nolock) 
left outer join 
(select FromDocDtlKey as FromDocDtlKey2,SUM(ActQty) as ActQty from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
GROUP BY rcvdtl.FromDocDtlKey) subb group by subb.FromDocDtlKey) b on a.DtlKey=b.FromDocDtlKey2
left outer join vItem c on a.ItemCode=c.ItemCode 

GO

ALTER view [dbo].[vRPA_WorkOrderDetail]
as
SELECT DocKey,DtlKey, Seq
      ,Numbering
      ,[Description]
      ,[UOM]
      ,[Rate]
      ,[Qty]
      ,[TransferedQty]
      ,[BalQty]
      ,[UnitCost]
      ,[TotalCost]
      ,[PrintOut]
      ,[StockReceived]
      ,[Guid]
      ,[FromBOMCode]
      ,[ParentBOMCode]
      ,[BOMCode]
      ,[Level]
      ,[ProductRatio]
      ,[RateUOM]
      ,[FromItemCode]
      ,b.ActQty,b.ActUnitCost,b.FromDocDtlKey as FromDocDtlKey2,
b.FromDocNo as FromDocNo2,b.OrgActQty,b.ReturQty,
c.* FROM RPA_WODtl a with(nolock) 
left outer join(
select FromDocNo,FromDocDtlKey,FromDocType,SUM(COALESCE(rmdtl.Qty,0)) as OrgActQty,
SUM(COALESCE(rmdtl.TransferedQty,0)) ReturQty,
SUM(COALESCE(rmdtl.Qty,0)-COALESCE(rmdtl.TransferedQty,0)) ActQty,
AVG(COALESCE(rmdtl.UnitCost,0)) as ActUnitCost  from RPA_RMDTL rmdtl with(nolock) 
inner join RPA_RM rm  with(nolock) on rmdtl.DocKey=rm.DocKey where Cancelled='F' GROUP BY FromDocNo,FromDocDtlKey,FromDocType)
b  on a.DtlKey=b.FromDocDtlKey and b.FromDocType='WO'
left outer join vItem c on a.ItemCode=c.ItemCode

