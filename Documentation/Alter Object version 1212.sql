CREATE view [dbo].[vRPA_BatchNoWO]
AS
SELECT a.DocNo,a.DocDate,a.DocKey, c.ItemCode,c.Description as ItemDescription,d.UOM,d.BatchNo,e.Description,e.ManufacturedDate,e.ExpiryDate,e.BalQty FROM RPA_WO a inner join RPA_WOProduct b on a.DocKey=b.DocKey inner join RPA_WODtl c on b.DtlKey=c.FromDocDtlKey inner join ItemBatchBalQty d on c.ItemCode=d.ItemCode and d.UOM=c.UOM inner join ItemBatch e on e.ItemCode=d.ItemCode where d.BalQty>0
GO
ALTER view [dbo].[vRPA_TransWOtoRCV]
AS
select * from(
select  b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Others Product' as DocType,MachineCode,'' AS BOMCode, b.ItemCode,b.Description
from RPA_WO a with(nolock) inner join RPA_WOBS b with(nolock) on a.DocKey=b.DocKey where a.status in ('InProcess','Assembly') 
and Cancelled='F'
union all
select a.DocKey as Code,a.DocNo,a.Status,a.DocDate,'Main Product' as DocType,MachineCode,B.BOMCode, b.ItemCode,b.Description
from RPA_WO a with(nolock) inner join RPA_WOProduct b with(nolock) on a.DocKey=b.DocKey inner join Item c with(nolock) on b.ItemCode=c.ItemCode where b.status in ('InProcess','Assembly') 
and Cancelled='F'
) result
GO
ALTER TABLE RPA_RMDTL
ADD BOMCode [dbo].[d_ItemCode] NULL
GO
ALTER TABLE RPA_ItemBOMDtl
ADD ItemBOMCode [nvarchar](70) NULL
GO
ALTER TABLE RPA_WODtl
ADD BOMCode [dbo].[d_ItemCode] NULL
GO
ALTER TABLE RPA_WODtl
ADD [Level] [int] NULL
GO
ALTER TABLE RPA_WOProduct
ADD [Cancelled] [char](1) NULL
GO
ALTER TABLE RPA_WOProduct
ADD [CancelledUserID] [dbo].[d_UserID] NULL
GO
ALTER TABLE RPA_WOProduct
ADD [CancelledDate] [datetime] NULL
GO
CREATE TABLE [dbo].[RPA_RMSubDtl](
	[SubDtlKey] [bigint] NOT NULL,
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[UOM] [dbo].[d_UOM] NULL,
	[BatchNo] [dbo].[d_BatchNo] NULL,
	[Description] [nvarchar](40) NULL,
	[ManufacturedDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[Qty] [dbo].[d_Qty] NULL,
 CONSTRAINT [PK_RPA_RMSubDtl] PRIMARY KEY CLUSTERED 
(
	[SubDtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RPA_RMSubDtl]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RMSubDtl_RPA_RMDTL] FOREIGN KEY([DtlKey])
REFERENCES [dbo].[RPA_RMDTL] ([DtlKey])
GO

ALTER TABLE [dbo].[RPA_RMSubDtl] CHECK CONSTRAINT [FK_RPA_RMSubDtl_RPA_RMDTL]
GO

ALTER TABLE [dbo].[RPA_RMSubDtl] ADD  CONSTRAINT [DF_Table_1_BalQty]  DEFAULT ((0)) FOR [Qty]
GO
CREATE TABLE [dbo].[RPA_WOProduct](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[UOM] [dbo].[d_UOM] NULL,
	[Rate] [dbo].[d_Qty] NULL,
	[Qty] [dbo].[d_Qty] NULL,
	[BOMCode] [dbo].[d_ItemCode] NOT NULL,
	[TransferedQty] [dbo].[d_Qty] NULL,
	[Status] [nvarchar](30) NOT NULL,
	[COGM] [dbo].[d_Cost] NULL,
	[FromDocType] [dbo].[d_DocType] NULL,
	[FromDocNo] [dbo].[d_DocNo] NULL,
	[FromDocDtlKey] [bigint] NULL,
	[DebtorCode] [dbo].[d_AccNo] NOT NULL,
	[DebtorName] [dbo].[d_AccDescription] NULL,
	[Style] [nvarchar](50) NULL,
	[Length] [decimal](18, 2) NULL,
	[UOMLength] [nvarchar](10) NULL,
	[Width] [decimal](18, 2) NULL,
	[UOMWidth] [nvarchar](10) NULL,
	[Size] [nvarchar](50) NULL,
	[Tickness] [decimal](18, 4) NULL,
	[Weight] [dbo].[d_Number] NULL,
	[WeightUOM] [dbo].[d_UOM] NULL,
	[Volume] [dbo].[d_Number] NULL,
	[VolumeUOM] [dbo].[d_UOM] NULL,
	[PrintOut] [dbo].[d_Boolean] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Cancelled] [char](1) NULL,
	[CancelledUserID] [dbo].[d_UserID] NULL,
	[CancelledDate] [datetime] NULL,
 CONSTRAINT [PK_RPA_WOProduct] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


