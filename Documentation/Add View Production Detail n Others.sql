ALTER VIEW [dbo].[vRPA_ProductionReport]
as
select *
    from
    (
--    select
--d.DocNo as WONo, d.ProductionStartDate,
--d.ProductionEndDate,
--d.DocDate as WODate,a.DocNo,a.DocDate,'RM' as DocType,
--c.ItemCode,c.Description,c.UOM,c.Rate,c.Qty as WOQty,
--B.UnitCost AS WOCost,c.UnitCost as ProdCost,
--b.BOMCode,b.Qty as ProdQty
-- from RPA_RM a with(nolock)
--inner join
--RPA_RMDTL b with(nolock)
--on a.DocKey=b.DocKey 
--inner join
--RPA_WODtl c  with(nolock)
--on b.FromDocDtlKey=c.DtlKey and b.FromDocType='WO'
--INNER JOIN  RPA_WO d  with(nolock) 
--on d.DocKey	=c.DocKey and b.FromDocNo=d.DocNo
--where a.Cancelled='F' and d.Cancelled='F'
--union all
--select d.DocNo as WONo, d.ProductionStartDate,
--d.ProductionEndDate,
--d.DocDate as WODate,a.DocNo,a.DocDate,'SR' as DocType,
--c.ItemCode,c.Description,c.UOM,c.Rate,c.Qty as WOQty,
--B.UnitCost AS WOCost,
--e.UnitCost as ProdCost,
--b.BOMCode,e.Qty as ProdQty
-- from 
-- RPA_RM a with(nolock)
--inner join
--RPA_RMDTL b with(nolock)
--on a.DocKey=b.DocKey 
--inner join
--RPA_WODtl c  with(nolock)
--on b.FromDocDtlKey=c.DtlKey and b.FromDocType='WO'
--INNER JOIN  RPA_WO d  with(nolock) 
--on d.DocKey	=c.DocKey and b.FromDocNo=d.DocNo
--inner join
--RPA_RCVRMDTL  e with(nolock)
--on e.FromDocDtlKey=b.DtlKey and e.FromDocType='RM' and e.FromDocNo=a.DocNo
--inner join
--RPA_RCVRM  f with(nolock)
--on f.DocKey=e.DocKey 
--where a.Cancelled='F' and d.Cancelled='F' and f.Cancelled='F'
--union all
select a.DocNo as WONo, a.ProductionStartDate,
a.ProductionEndDate,
a.DocDate as WODate,a.DocNo,d.DocDate,'RV' as DocType,
b.ItemCode,b.Description,b.UOM,b.Rate,b.Qty as WOQty,
b.ActHpp AS WOCost,c.UnitCost as ProdCost,
b.BOMCode,c.Qty as ProdQty,
b.DtlKey,
b.DocKey
 from 
 RPA_WO a  with(nolock) 
 inner join
 RPA_WOProduct b  with(nolock)
on a.DocKey=b.DocKey
inner join
 RPA_RCVDTL c with(nolock)
 on c.FromDocDtlKey=b.DtlKey and c.FromDocNo=a.DocNo and c.FromDocType='Main Product'
inner join
RPA_RCV d with(nolock)
on d.DocKey=c.DocKey 
where a.Cancelled='F' and d.Cancelled='F'
)result
GO
CREATE VIEW [dbo].[vRPA_ProductionDetailReport]
as
select 
 ROW_NUMBER() OVER(ORDER BY ItemCode ASC)  
    AS  DtlKey,*
    from
    (
    select
d.DocNo as WONo, d.ProductionStartDate,
d.ProductionEndDate,
d.DocDate as WODate,a.DocNo,a.DocDate,'RM' as DocType,
c.ItemCode,c.Description,c.UOM,c.Rate,c.Qty as WOQty,
B.UnitCost AS WOCost,c.UnitCost as ProdCost,
b.BOMCode,b.Qty as ProdQty,
C.FromDocDtlKey,
c.DocKey
 from RPA_RM a with(nolock)
inner join
RPA_RMDTL b with(nolock)
on a.DocKey=b.DocKey 
inner join
RPA_WODtl c  with(nolock)
on b.FromDocDtlKey=c.DtlKey and b.FromDocType='WO'
INNER JOIN  RPA_WO d  with(nolock) 
on d.DocKey	=c.DocKey and b.FromDocNo=d.DocNo
where a.Cancelled='F' and d.Cancelled='F'
union all
select d.DocNo as WONo, d.ProductionStartDate,
d.ProductionEndDate,
d.DocDate as WODate,a.DocNo,a.DocDate,'SR' as DocType,
c.ItemCode,c.Description,c.UOM,c.Rate,c.Qty as WOQty,
B.UnitCost AS WOCost,
e.UnitCost as ProdCost,
b.BOMCode,e.Qty as ProdQty,
C.FromDocDtlKey,
c.DocKey
 from 
 RPA_RM a with(nolock)
inner join
RPA_RMDTL b with(nolock)
on a.DocKey=b.DocKey 
inner join
RPA_WODtl c  with(nolock)
on b.FromDocDtlKey=c.DtlKey and b.FromDocType='WO'
INNER JOIN  RPA_WO d  with(nolock) 
on d.DocKey	=c.DocKey and b.FromDocNo=d.DocNo
inner join
RPA_RCVRMDTL  e with(nolock)
on e.FromDocDtlKey=b.DtlKey and e.FromDocType='RM' and e.FromDocNo=a.DocNo
inner join
RPA_RCVRM  f with(nolock)
on f.DocKey=e.DocKey 
where a.Cancelled='F' and d.Cancelled='F' and f.Cancelled='F'
)result
GO
ALTER view [dbo].[vRPA_TransWOtoRCV]
AS
select * from(
select  b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Others Product' as DocType,MachineCode,B.FromBOMCode AS BOMCode, b.ItemCode,b.Description,a.Description as DescHeader
from RPA_WO a with(nolock) inner join RPA_WOBS b with(nolock) on a.DocKey=b.DocKey where a.status like '%InProcess%' or a.status like '%Assembly%'
and Cancelled='F' 
union all
select b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Main Product' as DocType,MachineCode,B.BOMCode, b.ItemCode,b.Description,a.Description as DescHeader
from RPA_WO a with(nolock) inner join RPA_WOProduct b with(nolock) on a.DocKey=b.DocKey inner join Item c with(nolock) on b.ItemCode=c.ItemCode where b.status in ('InProcess','Assembly') 
and a.Cancelled='F' and ISNULL(b.Cancelled,'F')='F'
union all
select  b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Alt. Product' as DocType,MachineCode,'' AS BOMCode, b.ItemCode,b.Description,a.Description as DescHeader
from RPA_WO a with(nolock) inner join RPA_WOAP b with(nolock) on a.DocKey=b.DocKey where a.status like '%InProcess%' or a.status like '%Assembly%'
and Cancelled='F' 
) result
GO
