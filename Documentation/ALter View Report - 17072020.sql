USE [AED_CARAKA_1]
GO
/****** Object:  View [dbo].[vRPA_StockReceiveRM]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_StockReceiveRM] AS
SELECT A.DocKey, A.DocNo, A.DocDate, A.Description, A.Total, A.Note, A.BatchNo,A.DocType,
A.Remark1, A.Remark2, A.Remark3, A.Remark4, A.PrintCount, A.Cancelled,
A.LastModified, A.LastModifiedUserID, B.UserName AS LastModifiedUserName,
A.CreatedTimeStamp, A.CreatedUserID, C.UserName AS CreatedUserName,
A.ExternalLink, A.RefDocNo, A.CanSync
,A.JENo,A.JEKey
FROM RPA_RCVRM A
LEFT OUTER JOIN Users B ON (A.LastModifiedUserID=B.UserID)
LEFT OUTER JOIN Users C ON (A.CreatedUserID=C.UserID)
GO
/****** Object:  View [dbo].[vRPA_WorkOrder]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_WorkOrder]
as
select a.*
 from rpa_wo a with(nolock)
where Cancelled='F'
GO
/****** Object:  View [dbo].[vRM]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRM] AS
SELECT A.DocKey, A.DocNo, A.DocDate, A.Description, A.Total, A.Note,
A.Remark1, A.Remark2, A.Remark3, A.Remark4, A.PrintCount, A.Cancelled,
A.LastModified, A.LastModifiedUserID, B.UserName AS LastModifiedUserName,
A.CreatedTimeStamp, A.CreatedUserID, C.UserName AS CreatedUserName,
A.ExternalLink, A.RefDocNo, A.CanSync
, A.DocType , A.AccNo, A.JEKey, A.JENo
FROM RPA_RM A
LEFT OUTER JOIN Users B ON (A.LastModifiedUserID=B.UserID)
LEFT OUTER JOIN Users C ON (A.CreatedUserID=C.UserID)
GO
/****** Object:  View [dbo].[vRPA_WorkOrderBS]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_WorkOrderBS]
as
select * from RPA_WOBS a with(nolock) 
left outer join 
(select FromDocDtlKey FromDocDtlKey2,SUM(ActQty) as ActQty,SUM(ActUnitCost) as ActUnitCost from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty,AVG(COALESCE(rcvdtl.UnitCost,0)) as ActUnitCost  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
AND FromDocType in ('BS','Product sortiran')
GROUP BY rcvdtl.FromDocDtlKey) subb group by subb.FromDocDtlKey) b on a.DtlKey=b.FromDocDtlKey2
GO
/****** Object:  View [dbo].[vRPA_WorkOrderAP]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_WorkOrderAP]
as
select a.*,b.ActQty from RPA_WOAP a with(nolock) 
left outer join 
(select FromDocDtlKey,SUM(ActQty) as ActQty from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
AND FromDocType in ('AP','Alternatif Product')
--AND rcv.ActType='FRESH'
GROUP BY rcvdtl.FromDocDtlKey
--union all
--select rcvdtl.FromDocDtlKey,
--SUM(COALESCE(rcvdtl.Qty,0)) as ActQty  from RPA_RCVFRDTL rcvdtl with(nolock) 
--inner join RPA_RCVFR rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
--where Cancelled='F' 
--AND FromDocType in ('AP','Alternatif Product')
--GROUP BY rcvdtl.FromDocDtlKey
) subb group by subb.FromDocDtlKey
) b on a.DtlKey=b.FromDocDtlKey
GO
/****** Object:  View [dbo].[vRPA_StockReceive]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_StockReceive] AS
SELECT A.DocKey, A.DocNo, A.DocDate, A.Description, A.Total, A.Note,A.DocType,
A.Remark1, A.Remark2, A.Remark3, A.Remark4, A.PrintCount, A.Cancelled,
A.LastModified, A.LastModifiedUserID, B.UserName AS LastModifiedUserName,
A.CreatedTimeStamp, A.CreatedUserID, C.UserName AS CreatedUserName,
A.ExternalLink, A.RefDocNo, A.CanSync
,A.MachineCode, D.Description as MachineName,A.JENo,A.JEKey
FROM RPA_RCV A
LEFT OUTER JOIN Users B ON (A.LastModifiedUserID=B.UserID)
LEFT OUTER JOIN Users C ON (A.CreatedUserID=C.UserID)
LEFT OUTER JOIN RPA_Machine D ON (A.MachineCode=D.MachineCode)
GO
/****** Object:  View [dbo].[vRPA_WorkOrderDetail]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_WorkOrderDetail]
as
SELECT * FROM RPA_WODtl a with(nolock) 
left outer join(
select FromDocNo,FromDocDtlKey,FromDocType,SUM(COALESCE(rmdtl.Qty,0)) as OrgActQty,
SUM(COALESCE(rmdtl.TransferedQty,0)) ReturQty,
SUM(COALESCE(rmdtl.Qty,0)-COALESCE(rmdtl.TransferedQty,0)) ActQty,
AVG(COALESCE(rmdtl.UnitCost,0)) as ActUnitCost  from RPA_RMDTL rmdtl with(nolock) 
inner join RPA_RM rm  with(nolock) on rmdtl.DocKey=rm.DocKey where Cancelled='F' GROUP BY FromDocNo,FromDocDtlKey,FromDocType)
b  on a.DtlKey=b.FromDocDtlKey and b.FromDocType='WO'
GO
/****** Object:  View [dbo].[vRPA_WorkOrderProduct]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_WorkOrderProduct]
as
select * from RPA_WOProduct a with(nolock) 
left outer join 
(select FromDocDtlKey as FromDocDtlKey2,SUM(ActQty) as ActQty from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
GROUP BY rcvdtl.FromDocDtlKey) subb group by subb.FromDocDtlKey) b on a.DtlKey=b.FromDocDtlKey2
GO
/****** Object:  View [dbo].[vRPA_StockReceiveRMDetail]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_StockReceiveRMDetail] AS
SELECT A.DtlKey, A.DocKey, A.Seq, A.Numbering,A.FromDocNo,A.FromDocType,A.FromDocDtlKey,
B.*,
C.*,
D.*,
A.Description, A.FurtherDescription,
E.*,
F.*,
G.*,
A.Qty, A.UnitCost, A.SubTotal, A.PrintOut, A.SerialNoList,
H.BalQty AS LocationBalQty, I.BalQty AS BatchBalQty
, A.Debit, A.Credit
FROM RPA_RCVRMDTL A
LEFT OUTER JOIN vItem B ON (A.ItemCode=B.ItemCode)
LEFT OUTER JOIN vItemBatch C ON (A.ItemCode=C.BatchItemCode AND A.BatchNo=C.BatchNo)
LEFT OUTER JOIN vLocation D ON (A.Location=D.Location)
LEFT OUTER JOIN vProject E ON (A.ProjNo=E.ProjNo)
LEFT OUTER JOIN vDept F ON (A.DeptNo=F.DeptNo)
LEFT OUTER JOIN vItemUOM G ON (A.ItemCode=G.UOMItemCode AND A.UOM=G.UOM)
LEFT OUTER JOIN ItemBalQty H ON (A.ItemCode=H.ItemCode AND A.UOM=H.UOM AND A.Location=H.Location)
LEFT OUTER JOIN ItemBatchBalQty I ON (A.ItemCode=I.ItemCode AND A.UOM=I.UOM AND A.Location=I.Location AND A.BatchNo=I.BatchNo)
GO
/****** Object:  View [dbo].[vRPA_StockReceiveDetail]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_StockReceiveDetail] AS
SELECT A.DtlKey, A.DocKey, A.Seq, A.Numbering,A.FromDocNo,A.FromDocType,A.FromDocDtlKey,
B.*,
C.*,
D.*,
A.Description, A.FurtherDescription,
E.*,
F.*,
G.*,
A.Qty, A.UnitCost, A.SubTotal, A.PrintOut, A.SerialNoList,
H.BalQty AS LocationBalQty, I.BalQty AS BatchBalQty
, A.Debit, A.Credit
FROM RPA_RCVDTL A
LEFT OUTER JOIN vItem B ON (A.ItemCode=B.ItemCode)
LEFT OUTER JOIN vItemBatch C ON (A.ItemCode=C.BatchItemCode AND A.BatchNo=C.BatchNo)
LEFT OUTER JOIN vLocation D ON (A.Location=D.Location)
LEFT OUTER JOIN vProject E ON (A.ProjNo=E.ProjNo)
LEFT OUTER JOIN vDept F ON (A.DeptNo=F.DeptNo)
LEFT OUTER JOIN vItemUOM G ON (A.ItemCode=G.UOMItemCode AND A.UOM=G.UOM)
LEFT OUTER JOIN ItemBalQty H ON (A.ItemCode=H.ItemCode AND A.UOM=H.UOM AND A.Location=H.Location)
LEFT OUTER JOIN ItemBatchBalQty I ON (A.ItemCode=I.ItemCode AND A.UOM=I.UOM AND A.Location=I.Location AND A.BatchNo=I.BatchNo)
GO
/****** Object:  View [dbo].[vRMDtl]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRMDtl] AS
SELECT A.DtlKey, A.DocKey, A.Seq, A.Numbering,
B.*,
C.*,
D.*,
A.Description, A.FurtherDescription,
E.*,
F.*,
G.*,
A.Qty, A.UnitCost, A.SubTotal, A.PrintOut, A.SerialNoList,
H.BalQty AS LocationBalQty, I.BalQty AS BatchBalQty
, A.Debit, A.Credit
FROM RPA_RMDTL A
LEFT OUTER JOIN vItem B ON (A.ItemCode=B.ItemCode)
LEFT OUTER JOIN vItemBatch C ON (A.ItemCode=C.BatchItemCode AND A.BatchNo=C.BatchNo)
LEFT OUTER JOIN vLocation D ON (A.Location=D.Location)
LEFT OUTER JOIN vProject E ON (A.ProjNo=E.ProjNo)
LEFT OUTER JOIN vDept F ON (A.DeptNo=F.DeptNo)
LEFT OUTER JOIN vItemUOM G ON (A.ItemCode=G.UOMItemCode AND A.UOM=G.UOM)
LEFT OUTER JOIN ItemBalQty H ON (A.ItemCode=H.ItemCode AND A.UOM=H.UOM AND A.Location=H.Location)
LEFT OUTER JOIN ItemBatchBalQty I ON (A.ItemCode=I.ItemCode AND A.UOM=I.UOM AND A.Location=I.Location AND A.BatchNo=I.BatchNo)
GO
/****** Object:  View [dbo].[vRPA_WorkOrderOvd]    Script Date: 07/17/2020 05:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[vRPA_WorkOrderOvd]
as
select * from RPA_WOOvd  with(nolock)
GO
