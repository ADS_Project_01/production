
alter table RPA_RCVDtl
add BOMCode [dbo].[d_ItemCode] NULL
GO
ALTER view [dbo].[vRPA_TransWOtoRCV]
AS
select * from(
select  b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Others Product' as DocType,MachineCode,'' AS BOMCode, b.ItemCode,b.Description
from RPA_WO a with(nolock) inner join RPA_WOBS b with(nolock) on a.DocKey=b.DocKey where a.status in ('InProcess','Assembly') 
and Cancelled='F' 
union all
select b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Main Product' as DocType,MachineCode,B.BOMCode, b.ItemCode,b.Description
from RPA_WO a with(nolock) inner join RPA_WOProduct b with(nolock) on a.DocKey=b.DocKey inner join Item c with(nolock) on b.ItemCode=c.ItemCode where b.status in ('InProcess','Assembly') 
and a.Cancelled='F' and ISNULL(b.Cancelled,'F')='F'
) result
GO

CREATE TABLE [dbo].[RPA_TransRawMaterial](
	[StockDTLKey] [bigint] IDENTITY(1,1) NOT NULL,
	[DocType] [dbo].[d_DocType] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[DtlKey] [bigint] NOT NULL,
	[DocNo] [dbo].[d_DocNo] NULL,
	[DocDate] [datetime] NULL,
	[WONo] [dbo].[d_DocNo] NOT NULL,
	[BOMCode] [dbo].[d_ItemCode] NOT NULL,
	[ProductCode] [dbo].[d_ItemCode] NOT NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[BatchNo] [dbo].[d_BatchNo] NULL,
	[Location] [dbo].[d_Location] NOT NULL,
	[UOM] [dbo].[d_UOM] NOT NULL,
	[Qty] [dbo].[d_Qty] NOT NULL,
	[Cost] [dbo].[d_Cost] NULL,
	[TotalCost] [dbo].[d_Cost] NULL,
	[LastModified] [datetime] NULL,
	[Seq] [bigint] NULL,
	[ProjNo] [dbo].[d_Project] NULL,
	[DeptNo] [dbo].[d_Dept] NULL,
	[AdjustedCost] [dbo].[d_Cost] NULL,
	[CostType] [dbo].[d_CostType] NULL,
	[ReferTo] [bigint] NOT NULL,
	[InputCost] [dbo].[d_Cost] NOT NULL,
 CONSTRAINT [PK_RPA_TransRawMaterial] PRIMARY KEY CLUSTERED 
(
	[StockDTLKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[RPA_TransRawMaterial] ADD  CONSTRAINT [DF_RPA_TransRawMaterial_InputCost]  DEFAULT ((0)) FOR [InputCost]
GO


