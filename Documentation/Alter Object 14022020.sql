ALTER TABLE RPA_RMDTL
ADD [ItemType] [nvarchar](30) NULL
GO
CREATE TABLE [dbo].[RPA_ItemBOMRep](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[ItemCodeRep] [dbo].[d_ItemCode] NOT NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[DescriptionRep] [dbo].[d_ItemDescription] NULL,
	[UOM] [dbo].[d_UOM] NOT NULL,
	[ProductRatio] [dbo].[d_Money] NULL,
	[Qty] [dbo].[d_Qty] NOT NULL,
 CONSTRAINT [PK_RPA_ItemBOMRep] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RPA_ItemBOMRep]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ItemBOMRep_Item] FOREIGN KEY([ItemCode])
REFERENCES [dbo].[Item] ([ItemCode])
GO

ALTER TABLE [dbo].[RPA_ItemBOMRep] CHECK CONSTRAINT [FK_RPA_ItemBOMRep_Item]
GO

ALTER TABLE [dbo].[RPA_ItemBOMRep]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ItemBOMRep_Item1] FOREIGN KEY([ItemCodeRep])
REFERENCES [dbo].[Item] ([ItemCode])
GO

ALTER TABLE [dbo].[RPA_ItemBOMRep] CHECK CONSTRAINT [FK_RPA_ItemBOMRep_Item1]
GO
CREATE TABLE [dbo].[RPA_WOAP](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[ActHPP] [dbo].[d_Money] NULL,
	[FromDocDtlKey] [bigint] NULL,
	[FromBOMCode] [nvarchar](30) NULL,
	[ParentBOMCode] [nvarchar](30) NULL,
 CONSTRAINT [PK_RPA_WOAP] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RPA_WOAP]  WITH CHECK ADD  CONSTRAINT [FK_RPA_WOAP_RPA_WO] FOREIGN KEY([DocKey])
REFERENCES [dbo].[RPA_WO] ([DocKey])
GO

ALTER TABLE [dbo].[RPA_WOAP] CHECK CONSTRAINT [FK_RPA_WOAP_RPA_WO]
GO

ALTER TABLE [dbo].[RPA_WOAP] ADD  CONSTRAINT [DF_RPA_WOAP_Guid]  DEFAULT (newid()) FOR [Guid]
GO
ALTER view [dbo].[vRPA_TransWOtoRCV]
AS
select * from(
select  b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Others Product' as DocType,MachineCode,'' AS BOMCode, b.ItemCode,b.Description,a.Description as DescHeader
from RPA_WO a with(nolock) inner join RPA_WOBS b with(nolock) on a.DocKey=b.DocKey where a.status like '%InProcess%' or a.status like '%Assembly%'
and Cancelled='F' 
union all
select b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Main Product' as DocType,MachineCode,B.BOMCode, b.ItemCode,b.Description,a.Description as DescHeader
from RPA_WO a with(nolock) inner join RPA_WOProduct b with(nolock) on a.DocKey=b.DocKey inner join Item c with(nolock) on b.ItemCode=c.ItemCode where b.status in ('InProcess','Assembly') 
and a.Cancelled='F' and ISNULL(b.Cancelled,'F')='F'
union all
select  b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Alt. Product' as DocType,MachineCode,'' AS BOMCode, b.ItemCode,b.Description,a.Description as DescHeader
from RPA_WO a with(nolock) inner join RPA_WOAP b with(nolock) on a.DocKey=b.DocKey where a.status like '%InProcess%' or a.status like '%Assembly%'
and Cancelled='F' 
) result
GO
CREATE view [dbo].[vRPA_TransWOtoSIRM]
AS
SELECT  DocNo,DocDate,b.ItemCode as ProductItemCode,b.Description as ProductItemDesc
,CAST(c.DocKey AS nvarchar(100))+';'+CAST(c.DtlKey AS nvarchar(100))+';0'   AS ID
,c.BOMCode
,c.ItemCode
,'Standard' as ItemType
,c.Description
,c.DtlKey
,c.DocKey
,c.FromBOMCode
,c.FromDocDtlKey
,c.ParentBOMCode
,c.Level
,c.ProductRatio
,c.UOM
,c.RateUOM
,c.BalQty
,c.UnitCost
,c.Qty
,c.TotalCost
,c.Qty as NewQty FROM RPA_WO a with(nolock) inner join RPA_WOProduct b with(nolock) on a.DocKey=b.DocKey inner join RPA_WODtl c with(nolock) on b.DtlKey=c.FromDocDtlKey where b.Status in ('Planned','InProcess','Assembly') and Level=0 AND a.Cancelled='F'
union all
SELECT  DocNo,DocDate,b.ItemCode as ProductItemCode,b.Description as ProductItemDesc
,CAST(c.DocKey AS nvarchar(100))+';'+CAST(c.DtlKey AS nvarchar(100))+';'+CAST(bb.DtlKey AS nvarchar(100))   AS ID
,c.BOMCode
,bb.ItemCodeRep as ItemCode
,'Replacement' as ItemType
,bb.DescriptionRep as Description
,c.DtlKey
,c.DocKey
,c.FromBOMCode
,c.FromDocDtlKey
,c.ParentBOMCode
,c.Level
,c.ProductRatio
,bb.UOM
,c.RateUOM
,c.BalQty
,c.UnitCost
,bb.Qty 
,bb.Qty*c.UnitCost as TotalCost
,bb.Qty as NewQty FROM RPA_WO a with(nolock) inner join RPA_WOProduct b with(nolock) on a.DocKey=b.DocKey inner join RPA_WODtl c with(nolock) on b.DtlKey=c.FromDocDtlKey inner join
(SELECT c1.DtlKey, a1.BOMCode,c1.ItemCode,c1.ItemCodeRep,c1.DescriptionRep,c1.UOM,c1.Qty,c1.ProductRatio FROM RPA_ItemBOM a1 with(nolock) 
inner join RPA_ItemBOMDtl b1 with(nolock)  on a1.DocKey=b1.DocKey
inner join RPA_ItemBOMRep c1 with(nolock)  on c1.DocKey=a1.DocKey and b1.ItemCode=c1.ItemCode) bb on bb.BOMCode=c.BOMCode and c.ItemCode=bb.ItemCode 
where b.Status in ('Planned','InProcess','Assembly') and Level=0 AND a.Cancelled='F'
GO
CREATE view [dbo].[vRPA_WorkOrderAP]
as
select * from RPA_WOAP a with(nolock) 
left outer join 
(select FromDocDtlKey,SUM(ActQty) as ActQty from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
AND FromDocType in ('AP','Alternatif Product')
GROUP BY rcvdtl.FromDocDtlKey) subb group by subb.FromDocDtlKey) b on a.DtlKey=b.FromDocDtlKey



SELECT * FROM RPA_RCVDTL