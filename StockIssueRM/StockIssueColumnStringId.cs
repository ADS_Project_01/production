﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueColumnStringId
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Localization;

namespace Production.StockIssueRM
{
  [LocalizableString]
  public enum StockIssueColumnStringId
  {
    [DefaultString("Stock Issue Raw Material")] StockIssue,
    [DefaultString("Master Search Fields")] MasterSearchFields,
    [DefaultString("Stock Issue Raw Material No.")] StockIssueNo,
    [DefaultString("Stock Issue Raw Material Date")] StockIssueDate,
    [DefaultString("Total")] Total,
    [DefaultString("Note")] Note,
    [DefaultString("Document No.")] DocumentNo,
    [DefaultString("Date")] Date,
    [DefaultString("Description")] Description,
    [DefaultString("Ref. Doc. No.")] RefDocNo,
    [DefaultString("Remark 1")] Remark1,
    [DefaultString("Remark 2")] Remark2,
    [DefaultString("Remark 3")] Remark3,
    [DefaultString("Remark 4")] Remark4,
    [DefaultString("Created User ID")] CreatedUserID,
    [DefaultString("Last Modified User ID")] LastModifiedUserID,
    [DefaultString("Created Timestamp")] CreatedTimestamp,
    [DefaultString("Last Modified")] LastModified,
    [DefaultString("Cancelled Document")] CancelledDocument,
  }
}
