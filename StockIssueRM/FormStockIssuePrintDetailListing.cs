﻿using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.CommonAccounting;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
//using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace Production.StockIssueRM
{
    [SingleInstanceThreadForm]
    public class FormStockIssuePrintDetailListing : XtraForm
    {
        private string myGroupSummaryId = "";
        public static string myColumnName;
        private DBSetting myDBSetting;
        private DataTable myDataTable;
        private DataTable myDetailDataTable;
        private DataSet myDataSet;
        private StockIssueReportCommand myCommand;
        private AdvancedStockIssueCriteria myCriteria;
        private bool myInSearch;
        private StockIssueDetailReportingCriteria myReportingCriteria;
        private const string LISTING = "Print Stock Issue Raw Material Detail Listing";
        private bool myLoadAllColumns;
        private bool myFilterByLocation;
        private ScriptObject myScriptObject;
        private MouseDownHelper myMouseDownHelper;
        protected DecimalSetting myDecimalSetting;
        protected UserAuthentication myUserAuthentication;
        private IContainer components;
        private GridView gvMaster;
        private BarManager barManager1;
        private Bar bar1;
        private BarSubItem barSubItem1;
        private BarButtonItem barbtnAdvancedFilter;
        private GridColumn colCheck;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private GridColumn colDocNo;
        private GridColumn colDocDate;
        private GridColumn colItemCode;
        private GridColumn colDescription;
        private GridColumn colTotal;
        private GridColumn colCancelled;
        private GridColumn colLocation;
        private GridColumn colBatchNo;
        private GridColumn colDtlDescription;
        private GridColumn colFurtherDescription;
        private GridColumn colProjNo;
        private GridColumn colDeptNo;
        private GridColumn colQty;
        private GridColumn colItemGroup;
        private GridColumn colItemType;
        private GridColumn colRemark1;
        private GridColumn colRemark2;
        private GridColumn colRemark3;
        private GridColumn colRemark4;
        private GridColumn colRefDocNo;
        private GridColumn colPrintCount;
        private GridColumn colLastModified;
        private GridColumn colCreatedTimeStamp;
        private GridColumn colCreatedUserID;
        private RepositoryItemTextEdit repositoryItemTextEdit_Cancelled;
        private GridColumn colLastModifiedUserID;
        private GridColumn colUnitCost;
        private GridColumn colSubTotal;
        private GridColumn colUOM;
        private BarButtonItem barBtnDesignDetailListingReport;
        private RepositoryItemButtonEdit repositoryItemFurtherDescription;
        private XtraTabControl xtraTabControl1;
        private XtraTabPage xtraTabPage1;
        private XtraTabPage xtraTabPage2;
        private Label label4;
        private Label label6;
        private Label label3;
        private Label label1;
        private Label label2;
        private Label label5;
        private Label label7;
        private Label label8;
        private Label label9;
        private UCDateSelector ucDateSelector1;
        private UCItemSelector ucItemSelector1;
        private UCItemGroupSelector ucItemGroupSelector1;
        private UCItemTypeSelector ucItemTypeSelector1;
        private UCLocationSelector ucLocationSelector1;
        private UCAllProjectsSelector ucProjectSelector1;
        private UCAllDepartmentsSelector ucDepartmentSelector1;
        private GroupControl gbFilter;
        private GroupControl gbReport;
        private Label label10;
        private Label label11;
        private PanelControl pnCriteriaBasic;
        private GroupControl gbBasic;
        private Label label12;
        private Label label13;
        private Label label15;
        private UCDateSelector ucDateSelector2;
        private UCItemSelector ucItemSelector2;
        private GroupControl groupControl1;
        private Label label14;
        private Label label16;
        private PanelControl panelCenter;
        private PanelControl panelControl2;
        private SimpleButton sbtnClose;
        private SimpleButton sbtnToggleOptions;
        private SimpleButton sbtnInquiry;
        private XtraTabControl xtraTabControl2;
        private XtraTabPage xtraTabPage3;
        private XtraTabPage xtraTabPage4;
        private GridControl gridControl1;
        private ComboBoxEdit cbCancelledOption;
        private MemoEdit memoEdit_Criteria;
        private ComboBoxEdit cbEditSortBy;
        private ComboBoxEdit cbEditGroupBy;
        private ComboBoxEdit cbEditSortBy2;
        private ComboBoxEdit cbEditGroupBy2;
        private CheckEdit chkEditShowCriteria;
        private CheckEdit chkEditShowCriteria2;
        private PanelControl panelCriteria;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private SimpleButton sbtnAdvOptions;
        private SimpleButton sbtnAdvanceSearch;
        private UCStockIssueSelector ucStockIssueSelector1;
        private UCStockIssueSelector ucStockIssueSelector2;
        private PreviewButton previewButton1;
        private PrintButton printButton1;
        private SimpleButton sbtnAdvancedSearch2;
        private PanelHeader panelHeader1;
        private GridColumn colProjDesc;
        private GridColumn colDeptDesc;
        private BarButtonItem barBtnConvertToPlainText;
        private XtraTabPage xtraTabPage5;
        private Label label17;
        private XtraTabPage xtraTabPage6;
        private GridColumn gridColumn1;
        private Bar bar2;
        private BarSubItem barSubItem2;
        private BarButtonItem barButtonItem1;
        private BarButtonItem barButtonItem2;
        private GridColumn colJENo;
        private GridColumn colAccNo;
        private GridColumn colDebit;
        private GridColumn colCredit;
        private GridColumn colFromDocNo;
        private GridColumn colFromDocType;
        private UCSearchResult ucSearchResult1;

        public DataTable MasterDataTable
        {
            get
            {
                return this.myDataTable;
            }
        }

        public DataTable DetailDataTable
        {
            get
            {
                return this.myDetailDataTable;
            }
        }

        public string SelectedDocNosInString
        {
            get
            {
                return this.GenerateDocNosToString();
            }
        }

        public string SelectedDocKeysInString
        {
            get
            {
                return StringHelper.ArrayListToCommaString(new ArrayList((ICollection)CommonFunction.GetDocKeyList("ToBeUpdate", this.myDataTable)));
            }
        }

        public string SelectedDtlKeysInString
        {
            get
            {
                return StringHelper.ArrayListToCommaString(new ArrayList((ICollection)CommonFunction.GetDtlKeyList("ToBeUpdate", this.myDataTable)));
            }
        }

        public string ColumnName
        {
            get
            {
                return FormStockIssuePrintDetailListing.myColumnName;
            }
        }

        public StockIssueDetailReportingCriteria StockIssueReportingCriteria
        {
            get
            {
                return this.myReportingCriteria;
            }
        }

        public FormStockIssuePrintDetailListing(DBSetting dbSetting)
        {
            this.InitializeComponent();
            this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockIssueDetailListing");
            this.myCommand = StockIssueReportCommand.Create(dbSetting, new BasicReportOption());
            this.myDBSetting = this.myCommand.DBSetting;
            this.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
            this.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
            this.previewButton1.ReportType = "Stock Issue Raw Material Detail Listing";
            this.previewButton1.SetDBSetting(this.myDBSetting);
            this.printButton1.ReportType = "Stock Issue Raw Material Detail Listing";
            this.printButton1.SetDBSetting(this.myDBSetting);
            this.myDataTable = new DataTable("Master");
            this.myDetailDataTable = new DataTable("Detail");
            this.myDataSet = new DataSet();
            this.myDataSet.Tables.Add(this.myDataTable);
            this.myDataSet.Tables.Add(this.myDetailDataTable);
            this.gridControl1.DataSource = (object)this.myDataTable;
            this.LoadCriteria();
            this.cbCancelledOption.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(CancelledDocumentOption)));
            this.cbEditGroupBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(DetailListingGroupByOption)));
            this.cbEditGroupBy2.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(DetailListingGroupByOption)));
            this.cbEditSortBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(DetailListingSortByOption)));
            this.cbEditSortBy2.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(DetailListingSortByOption)));
            this.cbEditGroupBy.SelectedIndex = 0;
            this.cbEditGroupBy2.SelectedIndex = 0;
            this.cbEditSortBy.SelectedIndex = 0;
            this.cbEditSortBy2.SelectedIndex = 0;
            this.SetModuleFeatureAndControlAccessRight(ModuleControl.GetOrCreate(this.myDBSetting).ModuleController);
            this.ucSearchResult1.Initialize(this.gvMaster, "ToBeUpdate");
            //new UDFUtil(this.myDBSetting).SetupDetailListingReportGrid(this.gvMaster, "RPA_RM", "RPA_RMDtl");
            CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gvMaster, new EventHandler(this.ReloadAllColumns));
            this.InitUserControls();
            this.InitFormControls();
            this.InitializeSettings();
            this.RefreshDesignReport();
            this.myMouseDownHelper = new MouseDownHelper();
            this.myMouseDownHelper.Init(this.gvMaster);
            ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.AddListener(new ModuleControllerListenerDelegate(this.SetModuleFeatureAndControlAccessRight), (Component)this);
            this.myUserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.RefreshDesignReport), (Component)this);
            BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Issue.htm#stk028");
            DBSetting dbSetting1 = dbSetting;
            string docType = "";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            StockIssueString local =StockIssueString.OpenedPrintStockIssueDetailListingWindow;
            object[] objArray = new object[1];
            int index = 0;
            string loginUserId = this.myUserAuthentication.LoginUserID;
            objArray[index] = (object)loginUserId;
            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            string detail = "";
            Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
        }

        private void InitFormControls()
        {
            FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
            string fieldname1 = "Total";
            string fieldtype1 = "Currency";
            formControlUtil.AddField(fieldname1, fieldtype1);
            string fieldname2 = "Qty";
            string fieldtype2 = "Quantity";
            formControlUtil.AddField(fieldname2, fieldtype2);
            string fieldname3 = "UnitCost";
            string fieldtype3 = "Currency";
            formControlUtil.AddField(fieldname3, fieldtype3);
            string fieldname4 = "SubTotal";
            string fieldtype4 = "Currency";
            formControlUtil.AddField(fieldname4, fieldtype4);
            string fieldname5 = "DocDate";
            string fieldtype5 = "Date";
            formControlUtil.AddField(fieldname5, fieldtype5);
            string fieldname6 = "LastModified";
            string fieldtype6 = "DateTime";
            formControlUtil.AddField(fieldname6, fieldtype6);
            string fieldname7 = "CreatedTimeStamp";
            string fieldtype7 = "DateTime";
            formControlUtil.AddField(fieldname7, fieldtype7);
            FormStockIssuePrintDetailListing printDetailListing = this;
            formControlUtil.InitControls((Control)printDetailListing);
        }

        private void InitGroupSummary()
        {
            StringBuilder stringBuilder = new StringBuilder(40);
            stringBuilder.Append("ItemCode");
            if (this.colTotal.Visible)
                stringBuilder.Append("Total");
            stringBuilder.Append("Qty");
            if (this.colUnitCost.Visible)
                stringBuilder.Append("UnitCost");
            if (this.colSubTotal.Visible)
                stringBuilder.Append("Subtotal");
            string str1 = ((object)stringBuilder).ToString();
            if (str1 != this.myGroupSummaryId)
            {
                this.myGroupSummaryId = str1;
                this.gvMaster.GroupSummary.Clear();
                GridGroupSummaryItemCollection groupSummary1 = this.gvMaster.GroupSummary;
                int num1 = 3;
                string fieldName1 = "ItemCode";
                // ISSUE: variable of the null type
                GridColumn local1 = null;
                // ISSUE: variable of a boxed type
                GridGroupSummaryItemStringId local2 = GridGroupSummaryItemStringId.ItemCodeCount;
                object[] objArray1 = new object[1];
                int index1 = 0;
                string str2 = "{0}";
                objArray1[index1] = (object)str2;
                string string1 = BCE.Localization.Localizer.GetString((Enum)local2, objArray1);
                groupSummary1.Add((SummaryItemType)num1, fieldName1, (GridColumn)local1, string1);
                if (this.colTotal.Visible)
                {
                    GridGroupSummaryItemCollection groupSummary2 = this.gvMaster.GroupSummary;
                    int num2 = 0;
                    string fieldName2 = "Total";
                    // ISSUE: variable of the null type
                    GridColumn local3 = null;
                    // ISSUE: variable of a boxed type
                    GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.Total;
                    object[] objArray2 = new object[1];
                    int index2 = 0;
                    string currencyFormatString = this.myDecimalSetting.GetCurrencyFormatString(0);
                    objArray2[index2] = (object)currencyFormatString;
                    string string2 = BCE.Localization.Localizer.GetString((Enum)local4, objArray2);
                    groupSummary2.Add((SummaryItemType)num2, fieldName2, (GridColumn)local3, string2);
                }
                GridGroupSummaryItemCollection groupSummary3 = this.gvMaster.GroupSummary;
                int num3 = 0;
                string fieldName3 = "Qty";
                // ISSUE: variable of the null type
                GridColumn local5 = null;
                // ISSUE: variable of a boxed type
                GridGroupSummaryItemStringId local6 = GridGroupSummaryItemStringId.Quantity;
                object[] objArray3 = new object[1];
                int index3 = 0;
                string quantityFormatString = this.myDecimalSetting.GetQuantityFormatString(0);
                objArray3[index3] = (object)quantityFormatString;
                string string3 = BCE.Localization.Localizer.GetString((Enum)local6, objArray3);
                groupSummary3.Add((SummaryItemType)num3, fieldName3, (GridColumn)local5, string3);
                if (this.colUnitCost.Visible)
                {
                    GridGroupSummaryItemCollection groupSummary2 = this.gvMaster.GroupSummary;
                    int num2 = 0;
                    string fieldName2 = "UnitCost";
                    // ISSUE: variable of the null type
                    GridColumn local3 = null;
                    // ISSUE: variable of a boxed type
                    GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.UnitCost;
                    object[] objArray2 = new object[1];
                    int index2 = 0;
                    string costFormatString = this.myDecimalSetting.GetCostFormatString(0);
                    objArray2[index2] = (object)costFormatString;
                    string string2 = BCE.Localization.Localizer.GetString((Enum)local4, objArray2);
                    groupSummary2.Add((SummaryItemType)num2, fieldName2, (GridColumn)local3, string2);
                }
                if (this.colSubTotal.Visible)
                {
                    GridGroupSummaryItemCollection groupSummary2 = this.gvMaster.GroupSummary;
                    int num2 = 0;
                    string fieldName2 = "Subtotal";
                    // ISSUE: variable of the null type
                    GridColumn local3 = null;
                    // ISSUE: variable of a boxed type
                    GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.SubTotal;
                    object[] objArray2 = new object[1];
                    int index2 = 0;
                    string currencyFormatString = this.myDecimalSetting.GetCurrencyFormatString(0);
                    objArray2[index2] = (object)currencyFormatString;
                    string string2 = BCE.Localization.Localizer.GetString((Enum)local4, objArray2);
                    groupSummary2.Add((SummaryItemType)num2, fieldName2, (GridColumn)local3, string2);
                }
            }
        }

        private void SaveCriteria()
        {
            PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting)this.myReportingCriteria, "StockIssueDocumentDetailListingReport2.setting");
        }

        private void LoadCriteria()
        {
            this.myReportingCriteria = (StockIssueDetailReportingCriteria)PersistenceUtil.LoadCriteriaData(this.myDBSetting, "StockIssueDocumentDetailListingReport2.setting");
            if (this.myReportingCriteria == null)
                this.myReportingCriteria = new StockIssueDetailReportingCriteria();
            this.cbEditGroupBy.EditValue = (object)this.myReportingCriteria.GroupBy;
            this.cbEditSortBy.EditValue = (object)this.myReportingCriteria.SortBy;
            this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
            this.chkEditShowCriteria.Checked = this.myReportingCriteria.IsShowCriteria;
            if (this.myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.All)
                this.cbCancelledOption.SelectedIndex = 0;
            else if (this.myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
                this.cbCancelledOption.SelectedIndex = 1;
            else
                this.cbCancelledOption.SelectedIndex = 2;
        }

        private void InitUserControls()
        {
            if (this.myReportingCriteria.AdvancedOptions)
            {
                this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.Code_BasicOptions, new object[0]);
                this.panelCriteria.Visible = true;
                this.pnCriteriaBasic.Visible = false;
                this.cbEditGroupBy.SelectedIndex = (int)this.myReportingCriteria.GroupBy;
                this.cbEditSortBy.SelectedIndex = (int)this.myReportingCriteria.SortBy;
            }
            else
            {
                this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.Code_AdvancedOptions, new object[0]);
                this.panelCriteria.Visible = false;
                this.pnCriteriaBasic.Visible = true;
                this.cbEditGroupBy2.SelectedIndex = (int)this.myReportingCriteria.GroupBy;
                this.cbEditSortBy2.SelectedIndex = (int)this.myReportingCriteria.SortBy;
            }
            this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
            this.ucStockIssueSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
            this.ucItemSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
            this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupFilter);
            this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeFilter);
            this.ucLocationSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.LocationFilter);
            this.ucProjectSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ProjecNoFilter);
            this.ucDepartmentSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DeptNoFilter);
            this.ucDateSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
            this.ucStockIssueSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
            this.ucItemSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
        }

        private void InitializeSettings()
        {
            this.GetGroupSortCriteria();
        }

        private void GetGroupSortCriteria()
        {
            if (this.myReportingCriteria.AdvancedOptions)
            {
                this.myReportingCriteria.GroupBy = (DetailListingGroupByOption)this.cbEditGroupBy.SelectedIndex;
                this.myReportingCriteria.SortBy = (DetailListingSortByOption)this.cbEditSortBy.SelectedIndex;
                this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
            }
            else
            {
                this.myReportingCriteria.GroupBy = (DetailListingGroupByOption)this.cbEditGroupBy2.SelectedIndex;
                this.myReportingCriteria.SortBy = (DetailListingSortByOption)this.cbEditSortBy2.SelectedIndex;
                this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria2.Checked;
            }
        }

        private void AddNewColumn()
        {
            this.myDataTable.Columns.Add(new DataColumn()
            {
                DataType = typeof(bool),
                AllowDBNull = true,
                Caption = "Check",
                ColumnName = "ToBeUpdate",
                DefaultValue = (object)false
            });
        }

        private void BasicSearch(bool isSearchAll)
        {
            if (!this.myInSearch)
            {
                this.myInSearch = true;
                BCE.XtraUtils.GridViewUtils.UpdateData(this.gvMaster);
                this.gridControl1.DataSource = (object)null;
                try
                {
                    this.gridControl1.MainView.UpdateCurrentRow();
                    this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
                    string columnName = CommonFunction.BuildSQLColumns(isSearchAll, this.gvMaster.Columns.View);
                    if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                        this.AddNewColumn();
                    this.myCommand.DetailListingBasicSearch(this.myReportingCriteria, columnName, this.myDataSet, "ToBeUpdate");
                    this.gridControl1.DataSource = (object)this.myDataTable;
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                }
                finally
                {
                    this.myInSearch = false;
                }
                FormStockIssuePrintDetailListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockIssuePrintDetailListing.FormInquiryEventArgs(this, this.myDataTable);
                ScriptObject scriptObject = this.myScriptObject;
                string name = "OnFormInquiry";
                System.Type[] types = new System.Type[1];
                int index1 = 0;
                System.Type type = inquiryEventArgs1.GetType();
                types[index1] = type;
                object[] objArray = new object[1];
                int index2 = 0;
                FormStockIssuePrintDetailListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
                objArray[index2] = (object)inquiryEventArgs2;
                scriptObject.RunMethod(name, types, objArray);
            }
        }

        private void AdvancedSearch()
        {
            if (this.myCriteria == null)
                this.myCriteria = new AdvancedStockIssueCriteria(this.myDBSetting);
            this.myCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
            using (FormAdvancedSearch formAdvancedSearch = new FormAdvancedSearch((SearchCriteria)this.myCriteria, this.myDBSetting))
            {
                if (formAdvancedSearch.ShowDialog((IWin32Window)this) == DialogResult.OK)
                {
                    if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                        this.AddNewColumn();
                    this.memoEdit_Criteria.Lines = this.myCriteria.BuildReadableTextArray();
                    this.ucSearchResult1.KeepSearchResult = this.myCriteria.KeepSearchResult;
                    Cursor current = Cursor.Current;
                    Cursor.Current = Cursors.WaitCursor;
                    this.myInSearch = true;
                    try
                    {
                        this.myCommand.DetailListingAdvanceSearch(this.myCriteria, this.myDataSet, "ToBeUpdate");
                    }
                    catch (AppException ex)
                    {
                        AppMessage.ShowErrorMessage(ex.Message);
                    }
                    finally
                    {
                        this.ucSearchResult1.CheckAll();
                        this.myInSearch = false;
                        Cursor.Current = current;
                    }
                    FormStockIssuePrintDetailListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockIssuePrintDetailListing.FormInquiryEventArgs(this, this.myDataTable);
                    ScriptObject scriptObject = this.myScriptObject;
                    string name = "OnFormInquiry";
                    System.Type[] types = new System.Type[1];
                    int index1 = 0;
                    System.Type type = inquiryEventArgs1.GetType();
                    types[index1] = type;
                    object[] objArray = new object[1];
                    int index2 = 0;
                    FormStockIssuePrintDetailListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
                    objArray[index2] = (object)inquiryEventArgs2;
                    scriptObject.RunMethod(name, types, objArray);
                }
            }
        }

        private string GenerateDocNosToString()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvMaster);
            DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = True");
            if (dataRowArray.Length == 0)
            {
                AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.ShowMessage_StockIssueNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
                return (string)null;
            }
            else
            {
                string str = "";
                foreach (DataRow dataRow in dataRowArray)
                {
                    if (str.Length != 0)
                        str = str + ", ";
                    str = str + "'" + dataRow["DocNo"].ToString() + "'";
                }
                return str;
            }
        }

        private void sbtnInquiry_Click(object sender, EventArgs e)
        {
            this.sbtnToggleOptions.Enabled = true;
            this.BasicSearch(this.myLoadAllColumns);
            this.SortByOption();
            this.ucSearchResult1.CheckAll();
            this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
            this.gridControl1.Focus();
            DBSetting dbSetting = this.myDBSetting;
            string docType = "";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            StockIssueString local =StockIssueString.InquiredPrintStockIssueDetailListing;
            object[] objArray = new object[1];
            int index = 0;
            string loginUserId = this.myUserAuthentication.LoginUserID;
            objArray[index] = (object)loginUserId;
            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            string text = this.memoEdit_Criteria.Text;
            Activity.Log(dbSetting, docType, docKey, eventKey, @string, text);
        }

        private void ReloadAllColumns(object sender, EventArgs e)
        {
            this.myLoadAllColumns = true;
            this.BasicSearch(true);
        }

        private void barbtnAdvancedFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.myReportingCriteria.AdvancedOptions)
            {
                this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.Code_AdvancedOptions, new object[0]);
                this.pnCriteriaBasic.Visible = true;
                this.panelCriteria.Visible = false;
                this.myReportingCriteria.AdvancedOptions = false;
                this.ucDateSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
                this.ucStockIssueSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
                this.ucItemSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
                this.cbEditGroupBy2.SelectedIndex = this.cbEditGroupBy.SelectedIndex;
                this.cbEditSortBy2.SelectedIndex = this.cbEditSortBy.SelectedIndex;
                this.chkEditShowCriteria2.Checked = this.chkEditShowCriteria.Checked;
            }
            else
            {
                this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.Code_BasicOptions, new object[0]);
                this.pnCriteriaBasic.Visible = false;
                this.panelCriteria.Visible = true;
                this.myReportingCriteria.AdvancedOptions = true;
                this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
                this.ucStockIssueSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
                this.ucItemSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
                this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupFilter);
                this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeFilter);
                this.ucLocationSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.LocationFilter);
                this.ucProjectSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ProjecNoFilter);
                this.ucDepartmentSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DeptNoFilter);
                this.cbEditGroupBy.SelectedIndex = this.cbEditGroupBy2.SelectedIndex;
                this.cbEditSortBy.SelectedIndex = this.cbEditSortBy2.SelectedIndex;
                this.chkEditShowCriteria.Checked = this.chkEditShowCriteria2.Checked;
            }
        }

        private void sbtnToggleOptions_Click(object sender, EventArgs e)
        {
            if (this.myReportingCriteria.AdvancedOptions)
            {
                this.panelCriteria.Visible = !this.panelCriteria.Visible;
                if (this.panelCriteria.Visible)
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.Code_HideOptions, new object[0]);
                else
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.Code_ShowOptions, new object[0]);
            }
            else
            {
                this.pnCriteriaBasic.Visible = !this.pnCriteriaBasic.Visible;
                if (this.pnCriteriaBasic.Visible)
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.Code_HideOptions, new object[0]);
                else
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.Code_ShowOptions, new object[0]);
            }
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbCancelledOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
            {
                if (this.cbCancelledOption.SelectedIndex == 0)
                    this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.All;
                else if (this.cbCancelledOption.SelectedIndex == 1)
                    this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.Cancelled;
                else
                    this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.UnCancelled;
            }
        }

        private void FormStockIssuePrintDetailListing_Closing(object sender, CancelEventArgs e)
        {
            this.SaveCriteria();
        }

        private void FormStockIssuePrintDetailListing_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)120)
            {
                if (this.sbtnInquiry.Enabled)
                    this.sbtnInquiry.PerformClick();
            }
            else if (e.KeyCode == (Keys)119)
            {
                if (this.previewButton1.Enabled)
                    this.previewButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)118)
            {
                if (this.printButton1.Enabled)
                    this.printButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)117 && this.sbtnToggleOptions.Enabled)
                this.sbtnToggleOptions.PerformClick();
        }

        private void cbEditSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
            {
                if (this.myReportingCriteria.SortBy != (DetailListingSortByOption)this.cbEditSortBy.SelectedIndex)
                    this.myReportingCriteria.SortBy = (DetailListingSortByOption)this.cbEditSortBy.SelectedIndex;
                this.SortByOption();
            }
        }

        private void cbEditSortBy2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
            {
                if (this.myReportingCriteria.SortBy != (DetailListingSortByOption)this.cbEditSortBy2.SelectedIndex)
                    this.myReportingCriteria.SortBy = (DetailListingSortByOption)this.cbEditSortBy2.SelectedIndex;
                this.SortByOption();
            }
        }

        private void cbEditGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.GroupBy = (DetailListingGroupByOption)this.cbEditGroupBy.SelectedIndex;
        }

        private void cbEditGroupBy2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.GroupBy = (DetailListingGroupByOption)this.cbEditGroupBy2.SelectedIndex;
        }

        private void sbtnAdvOptions_Click(object sender, EventArgs e)
        {
            using (FormStockIssueDetailListingAdvOptions listingAdvOptions = new FormStockIssueDetailListingAdvOptions(this.myDBSetting, this.myReportingCriteria))
            {
                listingAdvOptions.SetFilterByLocation(this.myFilterByLocation);
                if (listingAdvOptions.ShowDialog() == DialogResult.OK)
                    this.cbCancelledOption.SelectedItem = listingAdvOptions.IsPrintCancelled;
            }
        }

        private void sbtnAdvanceSearch_Click(object sender, EventArgs e)
        {
            this.AdvancedSearch();
        }

        private void gvMaster_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GridView gridView = (GridView)sender;
            int[] selectedRows = gridView.GetSelectedRows();
            if (selectedRows != null && selectedRows.Length != 0 && selectedRows.Length != 1)
            {
                for (int index = 0; index < selectedRows.Length; ++index)
                    gridView.GetDataRow(selectedRows[index])["ToBeUpdate"] = (object)true;
            }
        }

        private void chkEditShowCriteria_CheckedChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
        }

        private void repositoryItemTextEdit_Cancelled_FormatEditValue(object sender, ConvertEditValueEventArgs e)
        {
            if (e.Value != null)
            {
                if (e.Value.ToString() == "T")
                    e.Value = (object)BCE.Localization.Localizer.GetString((Enum)StockStringId.Cancelled, new object[0]);
                else
                    e.Value = (object)"";
            }
        }

        private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            string selectedDtlKeysInString = this.SelectedDtlKeysInString;
            if (selectedDtlKeysInString == "")
            {
                AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.ShowMessage_StockIssueNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            else if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_ISS_RM_DTLLIST_REPORT_PREVIEW"))
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.GetGroupSortCriteria();
                this.myCommand.PreviewDetailListingReport(selectedDtlKeysInString, this.myReportingCriteria, e.DefaultReport);
                Cursor.Current = current;
            }
        }

        private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            string selectedDtlKeysInString = this.SelectedDtlKeysInString;
            if (selectedDtlKeysInString == "")
            {
                AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.ShowMessage_StockIssueNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            else if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_ISS_RM_DTLLIST_REPORT_PRINT"))
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.GetGroupSortCriteria();
                this.myCommand.PrintDetailListingReport(selectedDtlKeysInString, this.myReportingCriteria, e.DefaultReport);
                Cursor.Current = current;
            }
        }

        private void barBtnDesignDetailListingReport_ItemClick(object sender, ItemClickEventArgs e)
        {
           // ReportTool.DesignReport("Stock Issue Raw Material Detail Listing", this.myDBSetting);
            string selectedDtlKeysInString = this.SelectedDtlKeysInString;
            ReportTool.DesignReport("Stock Issue Raw Material Detail Listing", this.myCommand.GetDetailListingReportDataSource(selectedDtlKeysInString, this.myReportingCriteria), this.myDBSetting);

        }

        public virtual void RefreshDesignReport()
        {
            this.barBtnDesignDetailListingReport.Visibility = XtraBarsUtils.ToBarItemVisibility(this.myUserAuthentication.AccessRight.IsAccessible("TOOLS_RPT_SHOW"));
            if (this.ucLocationSelector1 != null)
            {
                this.myFilterByLocation = SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnableFilterByCurrentUserLocationAccessRight;
                if (this.myFilterByLocation)
                {
                    this.ucLocationSelector1.Filter.Type = FilterType.ByRange;
                    this.ucLocationSelector1.Filter.From = (object)this.myUserAuthentication.MainLocation;
                    this.ucLocationSelector1.Filter.To = (object)this.myUserAuthentication.MainLocation;
                    this.ucLocationSelector1.ApplyFilter();
                    this.ucLocationSelector1.Enabled = false;
                }
                else
                    this.ucLocationSelector1.Enabled = true;
                this.SetModuleFeatureAndControlAccessRight(ModuleControl.GetOrCreate(this.myDBSetting).ModuleController);
            }
        }

        private void SetModuleFeatureAndControlAccessRight(ModuleController controller)
        {
            bool flag = this.myUserAuthentication.AccessRight.IsAccessible("RPA_ISS_RM_SHOW_TOTAL") && this.myUserAuthentication.AccessRight.IsAccessible("SYS_BHV_VIEW_COST") || controller.ExpressEdition.Has;
            this.colTotal.Visible = flag;
            this.colTotal.OptionsColumn.ShowInCustomizationForm = flag;
            this.colUnitCost.Visible = flag;
            this.colUnitCost.OptionsColumn.ShowInCustomizationForm = flag;
            this.colSubTotal.Visible = flag;
            this.colSubTotal.OptionsColumn.ShowInCustomizationForm = flag;
        }

        private void SortByOption()
        {
            DetailListingSortByOption listingSortByOption = !this.myReportingCriteria.AdvancedOptions ? (DetailListingSortByOption)this.cbEditSortBy2.SelectedIndex : (DetailListingSortByOption)this.cbEditSortBy.SelectedIndex;
            string[] strArray1 = new string[8];
            int index1 = 0;
            string str1 = "DocNo";
            strArray1[index1] = str1;
            int index2 = 1;
            string str2 = "DocDate";
            strArray1[index2] = str2;
            int index3 = 2;
            string str3 = "ItemCode";
            strArray1[index3] = str3;
            int index4 = 3;
            string str4 = "ItemGroup";
            strArray1[index4] = str4;
            int index5 = 4;
            string str5 = "ItemType";
            strArray1[index5] = str5;
            int index6 = 5;
            string str6 = "Location";
            strArray1[index6] = str6;
            int index7 = 6;
            string str7 = "ProjNo";
            strArray1[index7] = str7;
            int index8 = 7;
            string str8 = "DeptNo";
            strArray1[index8] = str8;
            string[] strArray2 = strArray1;
            for (int index9 = 0; index9 < strArray2.Length; ++index9)
                this.gvMaster.Columns[strArray2[index9]].SortOrder = (DetailListingSortByOption)index9 != listingSortByOption ? ColumnSortOrder.None : ColumnSortOrder.Ascending;
        }

        private void FormSIDetailPrintSearch_Load(object sender, EventArgs e)
        {
            this.FormInitialize();
        }

        private void FormInitialize()
        {
            FormStockIssuePrintDetailListing.FormInitializeEventArgs initializeEventArgs1 = new FormStockIssuePrintDetailListing.FormInitializeEventArgs(this);
            ScriptObject scriptObject = this.myScriptObject;
            string name = "OnFormInitialize";
            System.Type[] types = new System.Type[1];
            int index1 = 0;
            System.Type type = initializeEventArgs1.GetType();
            types[index1] = type;
            object[] objArray = new object[1];
            int index2 = 0;
            FormStockIssuePrintDetailListing.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
            objArray[index2] = (object)initializeEventArgs2;
            scriptObject.RunMethod(name, types, objArray);
        }

        private void repositoryItemFurtherDescription_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            int focusedRowHandle = this.gvMaster.FocusedRowHandle;
            if (this.gvMaster.GetDataRow(focusedRowHandle) != null)
            {
                using (FormRichTextEditor formRichTextEditor = new FormRichTextEditor(this.myDBSetting, this.myDataTable, this.gvMaster.GetDataSourceRowIndex(focusedRowHandle), "FurtherDescription", "Further Description", false))
                {
                    int num = (int)formRichTextEditor.ShowDialog((IWin32Window)this);
                }
            }
        }

        private void GoToDocument()
        {
            ColumnView columnView = (ColumnView)this.gridControl1.FocusedView;
            int focusedRowHandle = columnView.FocusedRowHandle;
            DataRow dataRow = columnView.GetDataRow(focusedRowHandle);
            if (dataRow != null)
                DocumentDispatcher.Open(this.myDBSetting, "RM", BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
        }

        private void gvMaster_DoubleClick(object sender, EventArgs e)
        {
            if (this.myMouseDownHelper.IsLeftMouseDown && BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView)sender).InRow && this.myUserAuthentication.AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
                this.GoToDocument();
        }

        private void gvMaster_CustomDrawGroupPanel(object sender, CustomDrawEventArgs e)
        {
            CommonGridViewHelper.DrawGroupPanelString(sender, e, this.myDBSetting);
        }

        private void barBtnConvertToPlainText_ItemClick(object sender, ItemClickEventArgs e)
        {
            RichTextHelper.ConvertToPlainText(this.gvMaster);
        }

        private void gvMaster_Layout(object sender, EventArgs e)
        {
            if (this.gridControl1.DataSource != null)
                this.InitGroupSummary();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelCriteria = new DevExpress.XtraEditors.PanelControl();
            this.gbFilter = new DevExpress.XtraEditors.GroupControl();
            this.sbtnAdvanceSearch = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.ucStockIssueSelector1 = new Production.StockIssueRM.UCStockIssueSelector();
            this.cbCancelledOption = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucDateSelector1 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.ucDepartmentSelector1 = new BCE.AutoCount.FilterUI.UCAllDepartmentsSelector();
            this.ucProjectSelector1 = new BCE.AutoCount.FilterUI.UCAllProjectsSelector();
            this.ucItemSelector1 = new BCE.AutoCount.FilterUI.UCItemSelector();
            this.ucLocationSelector1 = new BCE.AutoCount.FilterUI.UCLocationSelector();
            this.ucItemTypeSelector1 = new BCE.AutoCount.FilterUI.UCItemTypeSelector();
            this.ucItemGroupSelector1 = new BCE.AutoCount.FilterUI.UCItemGroupSelector();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.gbReport = new DevExpress.XtraEditors.GroupControl();
            this.chkEditShowCriteria = new DevExpress.XtraEditors.CheckEdit();
            this.cbEditSortBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbEditGroupBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.pnCriteriaBasic = new DevExpress.XtraEditors.PanelControl();
            this.sbtnAdvancedSearch2 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAdvOptions = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.chkEditShowCriteria2 = new DevExpress.XtraEditors.CheckEdit();
            this.cbEditSortBy2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbEditGroupBy2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.gbBasic = new DevExpress.XtraEditors.GroupControl();
            this.ucStockIssueSelector2 = new Production.StockIssueRM.UCStockIssueSelector();
            this.ucItemSelector2 = new BCE.AutoCount.FilterUI.UCItemSelector();
            this.ucDateSelector2 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panelCenter = new DevExpress.XtraEditors.PanelControl();
            this.printButton1 = new BCE.AutoCount.Controls.PrintButton();
            this.previewButton1 = new BCE.AutoCount.Controls.PreviewButton();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnToggleOptions = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gvMaster = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBatchNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit_Cancelled = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCreatedTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeptDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeptNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJENo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDtlDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFurtherDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemFurtherDescription = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDebit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCredit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDocType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ucSearchResult1 = new BCE.AutoCount.FilterUI.UCSearchResult();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit_Criteria = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnDesignDetailListingReport = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnAdvancedFilter = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnConvertToPlainText = new DevExpress.XtraBars.BarButtonItem();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).BeginInit();
            this.panelCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbFilter)).BeginInit();
            this.gbFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbReport)).BeginInit();
            this.gbReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaBasic)).BeginInit();
            this.pnCriteriaBasic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbBasic)).BeginInit();
            this.gbBasic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelCenter)).BeginInit();
            this.panelCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit_Cancelled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFurtherDescription)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCriteria
            // 
            this.panelCriteria.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCriteria.Controls.Add(this.gbFilter);
            this.panelCriteria.Controls.Add(this.gbReport);
            this.panelCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCriteria.Location = new System.Drawing.Point(0, 73);
            this.panelCriteria.Name = "panelCriteria";
            this.panelCriteria.Size = new System.Drawing.Size(967, 222);
            this.panelCriteria.TabIndex = 3;
            // 
            // gbFilter
            // 
            this.gbFilter.Controls.Add(this.sbtnAdvanceSearch);
            this.gbFilter.Controls.Add(this.xtraTabControl1);
            this.gbFilter.Location = new System.Drawing.Point(12, 6);
            this.gbFilter.Name = "gbFilter";
            this.gbFilter.Size = new System.Drawing.Size(527, 210);
            this.gbFilter.TabIndex = 0;
            this.gbFilter.Text = "Filter Options";
            // 
            // sbtnAdvanceSearch
            // 
            this.sbtnAdvanceSearch.Location = new System.Drawing.Point(389, 24);
            this.sbtnAdvanceSearch.Name = "sbtnAdvanceSearch";
            this.sbtnAdvanceSearch.Size = new System.Drawing.Size(130, 23);
            this.sbtnAdvanceSearch.TabIndex = 0;
            this.sbtnAdvanceSearch.Text = "&Advanced Filter...";
            this.sbtnAdvanceSearch.Click += new System.EventHandler(this.sbtnAdvanceSearch_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 36);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(527, 174);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.ucStockIssueSelector1);
            this.xtraTabPage1.Controls.Add(this.cbCancelledOption);
            this.xtraTabPage1.Controls.Add(this.ucDateSelector1);
            this.xtraTabPage1.Controls.Add(this.label2);
            this.xtraTabPage1.Controls.Add(this.label1);
            this.xtraTabPage1.Controls.Add(this.label3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(521, 146);
            this.xtraTabPage1.Text = "Master Level";
            // 
            // ucStockIssueSelector1
            // 
            this.ucStockIssueSelector1.Appearance.Options.UseBackColor = true;
            this.ucStockIssueSelector1.Location = new System.Drawing.Point(127, 37);
            this.ucStockIssueSelector1.Name = "ucStockIssueSelector1";
            this.ucStockIssueSelector1.Size = new System.Drawing.Size(362, 23);
            this.ucStockIssueSelector1.TabIndex = 0;
            // 
            // cbCancelledOption
            // 
            this.cbCancelledOption.Location = new System.Drawing.Point(130, 66);
            this.cbCancelledOption.Name = "cbCancelledOption";
            this.cbCancelledOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbCancelledOption.Size = new System.Drawing.Size(129, 20);
            this.cbCancelledOption.TabIndex = 1;
            this.cbCancelledOption.SelectedIndexChanged += new System.EventHandler(this.cbCancelledOption_SelectedIndexChanged);
            // 
            // ucDateSelector1
            // 
            this.ucDateSelector1.Appearance.Options.UseBackColor = true;
            this.ucDateSelector1.Location = new System.Drawing.Point(130, 14);
            this.ucDateSelector1.Name = "ucDateSelector1";
            this.ucDateSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucDateSelector1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(18, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Document Date";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(18, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "Document No.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(18, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cancelled Status";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.label17);
            this.xtraTabPage2.Controls.Add(this.ucDepartmentSelector1);
            this.xtraTabPage2.Controls.Add(this.ucProjectSelector1);
            this.xtraTabPage2.Controls.Add(this.ucItemSelector1);
            this.xtraTabPage2.Controls.Add(this.ucLocationSelector1);
            this.xtraTabPage2.Controls.Add(this.ucItemTypeSelector1);
            this.xtraTabPage2.Controls.Add(this.ucItemGroupSelector1);
            this.xtraTabPage2.Controls.Add(this.label7);
            this.xtraTabPage2.Controls.Add(this.label5);
            this.xtraTabPage2.Controls.Add(this.label6);
            this.xtraTabPage2.Controls.Add(this.label4);
            this.xtraTabPage2.Controls.Add(this.label8);
            this.xtraTabPage2.Controls.Add(this.label9);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(521, 146);
            this.xtraTabPage2.Text = "Detail Level";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(15, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 16);
            this.label17.TabIndex = 12;
            this.label17.Text = "Item Code";
            // 
            // ucDepartmentSelector1
            // 
            this.ucDepartmentSelector1.Appearance.Options.UseBackColor = true;
            this.ucDepartmentSelector1.Location = new System.Drawing.Point(114, 117);
            this.ucDepartmentSelector1.Name = "ucDepartmentSelector1";
            this.ucDepartmentSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucDepartmentSelector1.TabIndex = 0;
            // 
            // ucProjectSelector1
            // 
            this.ucProjectSelector1.Appearance.Options.UseBackColor = true;
            this.ucProjectSelector1.Location = new System.Drawing.Point(114, 95);
            this.ucProjectSelector1.Name = "ucProjectSelector1";
            this.ucProjectSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucProjectSelector1.TabIndex = 1;
            // 
            // ucItemSelector1
            // 
            this.ucItemSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemSelector1.Location = new System.Drawing.Point(114, 9);
            this.ucItemSelector1.Name = "ucItemSelector1";
            this.ucItemSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucItemSelector1.TabIndex = 5;
            // 
            // ucLocationSelector1
            // 
            this.ucLocationSelector1.Appearance.Options.UseBackColor = true;
            this.ucLocationSelector1.Location = new System.Drawing.Point(114, 73);
            this.ucLocationSelector1.LocationType = BCE.AutoCount.FilterUI.UCLocationType.ItemLocation;
            this.ucLocationSelector1.Name = "ucLocationSelector1";
            this.ucLocationSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucLocationSelector1.TabIndex = 2;
            // 
            // ucItemTypeSelector1
            // 
            this.ucItemTypeSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemTypeSelector1.Location = new System.Drawing.Point(114, 51);
            this.ucItemTypeSelector1.Name = "ucItemTypeSelector1";
            this.ucItemTypeSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucItemTypeSelector1.TabIndex = 3;
            // 
            // ucItemGroupSelector1
            // 
            this.ucItemGroupSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemGroupSelector1.Location = new System.Drawing.Point(114, 30);
            this.ucItemGroupSelector1.Name = "ucItemGroupSelector1";
            this.ucItemGroupSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucItemGroupSelector1.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(15, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Department No.";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(15, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Project No.";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(15, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "Location";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(15, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 16);
            this.label8.TabIndex = 10;
            this.label8.Text = "Item Type";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(15, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 16);
            this.label9.TabIndex = 11;
            this.label9.Text = "Item Group";
            // 
            // gbReport
            // 
            this.gbReport.Controls.Add(this.chkEditShowCriteria);
            this.gbReport.Controls.Add(this.cbEditSortBy);
            this.gbReport.Controls.Add(this.cbEditGroupBy);
            this.gbReport.Controls.Add(this.label11);
            this.gbReport.Controls.Add(this.label10);
            this.gbReport.Location = new System.Drawing.Point(545, 6);
            this.gbReport.Name = "gbReport";
            this.gbReport.Size = new System.Drawing.Size(232, 117);
            this.gbReport.TabIndex = 1;
            this.gbReport.Text = "Report Options";
            // 
            // chkEditShowCriteria
            // 
            this.chkEditShowCriteria.Location = new System.Drawing.Point(25, 88);
            this.chkEditShowCriteria.Name = "chkEditShowCriteria";
            this.chkEditShowCriteria.Properties.Caption = "Show Criteria in Report";
            this.chkEditShowCriteria.Size = new System.Drawing.Size(155, 19);
            this.chkEditShowCriteria.TabIndex = 0;
            this.chkEditShowCriteria.CheckedChanged += new System.EventHandler(this.chkEditShowCriteria_CheckedChanged);
            // 
            // cbEditSortBy
            // 
            this.cbEditSortBy.Location = new System.Drawing.Point(82, 56);
            this.cbEditSortBy.Name = "cbEditSortBy";
            this.cbEditSortBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditSortBy.Properties.DropDownRows = 10;
            this.cbEditSortBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditSortBy.Size = new System.Drawing.Size(116, 20);
            this.cbEditSortBy.TabIndex = 1;
            this.cbEditSortBy.SelectedIndexChanged += new System.EventHandler(this.cbEditSortBy_SelectedIndexChanged);
            // 
            // cbEditGroupBy
            // 
            this.cbEditGroupBy.Location = new System.Drawing.Point(82, 30);
            this.cbEditGroupBy.Name = "cbEditGroupBy";
            this.cbEditGroupBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditGroupBy.Properties.DropDownRows = 10;
            this.cbEditGroupBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditGroupBy.Size = new System.Drawing.Size(116, 20);
            this.cbEditGroupBy.TabIndex = 2;
            this.cbEditGroupBy.SelectedIndexChanged += new System.EventHandler(this.cbEditGroupBy_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(9, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Sort By :";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(9, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 14);
            this.label10.TabIndex = 4;
            this.label10.Text = "Group By : ";
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(521, 148);
            this.xtraTabPage5.Text = "xtraTabPage5";
            // 
            // pnCriteriaBasic
            // 
            this.pnCriteriaBasic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnCriteriaBasic.Controls.Add(this.sbtnAdvancedSearch2);
            this.pnCriteriaBasic.Controls.Add(this.sbtnAdvOptions);
            this.pnCriteriaBasic.Controls.Add(this.groupControl1);
            this.pnCriteriaBasic.Controls.Add(this.gbBasic);
            this.pnCriteriaBasic.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnCriteriaBasic.Location = new System.Drawing.Point(0, 295);
            this.pnCriteriaBasic.Name = "pnCriteriaBasic";
            this.pnCriteriaBasic.Size = new System.Drawing.Size(967, 145);
            this.pnCriteriaBasic.TabIndex = 2;
            // 
            // sbtnAdvancedSearch2
            // 
            this.sbtnAdvancedSearch2.Location = new System.Drawing.Point(657, 119);
            this.sbtnAdvancedSearch2.Name = "sbtnAdvancedSearch2";
            this.sbtnAdvancedSearch2.Size = new System.Drawing.Size(120, 23);
            this.sbtnAdvancedSearch2.TabIndex = 0;
            this.sbtnAdvancedSearch2.Text = "&Advanced Filter...";
            this.sbtnAdvancedSearch2.Click += new System.EventHandler(this.sbtnAdvanceSearch_Click);
            // 
            // sbtnAdvOptions
            // 
            this.sbtnAdvOptions.Location = new System.Drawing.Point(551, 119);
            this.sbtnAdvOptions.Name = "sbtnAdvOptions";
            this.sbtnAdvOptions.Size = new System.Drawing.Size(100, 23);
            this.sbtnAdvOptions.TabIndex = 1;
            this.sbtnAdvOptions.Text = "More Options";
            this.sbtnAdvOptions.Click += new System.EventHandler(this.sbtnAdvOptions_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.chkEditShowCriteria2);
            this.groupControl1.Controls.Add(this.cbEditSortBy2);
            this.groupControl1.Controls.Add(this.cbEditGroupBy2);
            this.groupControl1.Controls.Add(this.label14);
            this.groupControl1.Controls.Add(this.label16);
            this.groupControl1.Location = new System.Drawing.Point(545, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(232, 107);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Report Options";
            // 
            // chkEditShowCriteria2
            // 
            this.chkEditShowCriteria2.Location = new System.Drawing.Point(13, 80);
            this.chkEditShowCriteria2.Name = "chkEditShowCriteria2";
            this.chkEditShowCriteria2.Properties.Caption = "Show Criteria in Report";
            this.chkEditShowCriteria2.Size = new System.Drawing.Size(162, 19);
            this.chkEditShowCriteria2.TabIndex = 0;
            // 
            // cbEditSortBy2
            // 
            this.cbEditSortBy2.Location = new System.Drawing.Point(82, 53);
            this.cbEditSortBy2.Name = "cbEditSortBy2";
            this.cbEditSortBy2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditSortBy2.Properties.DropDownRows = 10;
            this.cbEditSortBy2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditSortBy2.Size = new System.Drawing.Size(116, 20);
            this.cbEditSortBy2.TabIndex = 1;
            this.cbEditSortBy2.SelectedIndexChanged += new System.EventHandler(this.cbEditSortBy2_SelectedIndexChanged);
            // 
            // cbEditGroupBy2
            // 
            this.cbEditGroupBy2.Location = new System.Drawing.Point(82, 27);
            this.cbEditGroupBy2.Name = "cbEditGroupBy2";
            this.cbEditGroupBy2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditGroupBy2.Properties.DropDownRows = 10;
            this.cbEditGroupBy2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditGroupBy2.Size = new System.Drawing.Size(116, 20);
            this.cbEditGroupBy2.TabIndex = 2;
            this.cbEditGroupBy2.SelectedIndexChanged += new System.EventHandler(this.cbEditGroupBy2_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(13, 56);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 17);
            this.label14.TabIndex = 3;
            this.label14.Text = "Sort By :";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(12, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 18);
            this.label16.TabIndex = 4;
            this.label16.Text = "Group By :";
            // 
            // gbBasic
            // 
            this.gbBasic.Controls.Add(this.ucStockIssueSelector2);
            this.gbBasic.Controls.Add(this.ucItemSelector2);
            this.gbBasic.Controls.Add(this.ucDateSelector2);
            this.gbBasic.Controls.Add(this.label15);
            this.gbBasic.Controls.Add(this.label12);
            this.gbBasic.Controls.Add(this.label13);
            this.gbBasic.Location = new System.Drawing.Point(13, 6);
            this.gbBasic.Name = "gbBasic";
            this.gbBasic.Size = new System.Drawing.Size(526, 136);
            this.gbBasic.TabIndex = 3;
            this.gbBasic.Text = "Basic Filter";
            // 
            // ucStockIssueSelector2
            // 
            this.ucStockIssueSelector2.Appearance.Options.UseBackColor = true;
            this.ucStockIssueSelector2.Location = new System.Drawing.Point(111, 49);
            this.ucStockIssueSelector2.Name = "ucStockIssueSelector2";
            this.ucStockIssueSelector2.Size = new System.Drawing.Size(362, 23);
            this.ucStockIssueSelector2.TabIndex = 0;
            // 
            // ucItemSelector2
            // 
            this.ucItemSelector2.Appearance.Options.UseBackColor = true;
            this.ucItemSelector2.Location = new System.Drawing.Point(114, 76);
            this.ucItemSelector2.Name = "ucItemSelector2";
            this.ucItemSelector2.Size = new System.Drawing.Size(362, 20);
            this.ucItemSelector2.TabIndex = 1;
            // 
            // ucDateSelector2
            // 
            this.ucDateSelector2.Appearance.Options.UseBackColor = true;
            this.ucDateSelector2.Location = new System.Drawing.Point(114, 28);
            this.ucDateSelector2.Name = "ucDateSelector2";
            this.ucDateSelector2.Size = new System.Drawing.Size(362, 20);
            this.ucDateSelector2.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(8, 79);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 15);
            this.label15.TabIndex = 3;
            this.label15.Text = "Item Code";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(8, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 14);
            this.label12.TabIndex = 4;
            this.label12.Text = "Document No.";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(8, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 23);
            this.label13.TabIndex = 5;
            this.label13.Text = "Document Date";
            // 
            // panelCenter
            // 
            this.panelCenter.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCenter.Controls.Add(this.printButton1);
            this.panelCenter.Controls.Add(this.previewButton1);
            this.panelCenter.Controls.Add(this.sbtnClose);
            this.panelCenter.Controls.Add(this.sbtnToggleOptions);
            this.panelCenter.Controls.Add(this.sbtnInquiry);
            this.panelCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCenter.Location = new System.Drawing.Point(0, 440);
            this.panelCenter.Name = "panelCenter";
            this.panelCenter.Size = new System.Drawing.Size(967, 34);
            this.panelCenter.TabIndex = 1;
            // 
            // printButton1
            // 
            this.printButton1.Location = new System.Drawing.Point(171, 6);
            this.printButton1.Name = "printButton1";
            this.printButton1.ReportType = "";
            this.printButton1.Size = new System.Drawing.Size(72, 23);
            this.printButton1.TabIndex = 0;
            this.printButton1.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // previewButton1
            // 
            this.previewButton1.Location = new System.Drawing.Point(93, 6);
            this.previewButton1.Name = "previewButton1";
            this.previewButton1.ReportType = "";
            this.previewButton1.Size = new System.Drawing.Size(72, 23);
            this.previewButton1.TabIndex = 1;
            this.previewButton1.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // sbtnClose
            // 
            this.sbtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnClose.Location = new System.Drawing.Point(330, 6);
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Size = new System.Drawing.Size(75, 23);
            this.sbtnClose.TabIndex = 2;
            this.sbtnClose.Text = "Close";
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // sbtnToggleOptions
            // 
            this.sbtnToggleOptions.Location = new System.Drawing.Point(249, 6);
            this.sbtnToggleOptions.Name = "sbtnToggleOptions";
            this.sbtnToggleOptions.Size = new System.Drawing.Size(75, 23);
            this.sbtnToggleOptions.TabIndex = 3;
            this.sbtnToggleOptions.Text = "Hide Options";
            this.sbtnToggleOptions.Click += new System.EventHandler(this.sbtnToggleOptions_Click);
            // 
            // sbtnInquiry
            // 
            this.sbtnInquiry.Location = new System.Drawing.Point(12, 6);
            this.sbtnInquiry.Name = "sbtnInquiry";
            this.sbtnInquiry.Size = new System.Drawing.Size(75, 23);
            this.sbtnInquiry.TabIndex = 4;
            this.sbtnInquiry.Text = "Inquiry";
            this.sbtnInquiry.Click += new System.EventHandler(this.sbtnInquiry_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.xtraTabControl2);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 474);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(967, 188);
            this.panelControl2.TabIndex = 0;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
            this.xtraTabControl2.Size = new System.Drawing.Size(967, 188);
            this.xtraTabControl2.TabIndex = 0;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl1);
            this.xtraTabPage3.Controls.Add(this.ucSearchResult1);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(961, 160);
            this.xtraTabPage3.Text = "Result";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 46);
            this.gridControl1.MainView = this.gvMaster;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(961, 114);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMaster});
            // 
            // gvMaster
            // 
            this.gvMaster.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBatchNo,
            this.colCancelled,
            this.colCheck,
            this.colCreatedTimeStamp,
            this.colCreatedUserID,
            this.colDeptDesc,
            this.colDeptNo,
            this.colJENo,
            this.colDescription,
            this.colAccNo,
            this.colDocDate,
            this.colDocNo,
            this.colDtlDescription,
            this.colFurtherDescription,
            this.colItemCode,
            this.colItemGroup,
            this.colItemType,
            this.colLastModified,
            this.colLastModifiedUserID,
            this.colLocation,
            this.colPrintCount,
            this.colProjDesc,
            this.colProjNo,
            this.colQty,
            this.colRefDocNo,
            this.colRemark1,
            this.colRemark2,
            this.colRemark3,
            this.colRemark4,
            this.colSubTotal,
            this.colTotal,
            this.colUnitCost,
            this.colDebit,
            this.colUOM,
            this.colCredit,
            this.colFromDocNo,
            this.colFromDocType});
            this.gvMaster.GridControl = this.gridControl1;
            this.gvMaster.Name = "gvMaster";
            this.gvMaster.CustomDrawGroupPanel += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gvMaster_CustomDrawGroupPanel);
            this.gvMaster.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gvMaster_SelectionChanged);
            this.gvMaster.DoubleClick += new System.EventHandler(this.gvMaster_DoubleClick);
            this.gvMaster.Layout += new System.EventHandler(this.gvMaster_Layout);
            // 
            // colBatchNo
            // 
            this.colBatchNo.Caption = "BatchNo";
            this.colBatchNo.FieldName = "BatchNo";
            this.colBatchNo.Name = "colBatchNo";
            // 
            // colCancelled
            // 
            this.colCancelled.Caption = "Cancelled";
            this.colCancelled.ColumnEdit = this.repositoryItemTextEdit_Cancelled;
            this.colCancelled.FieldName = "Cancelled";
            this.colCancelled.Name = "colCancelled";
            this.colCancelled.Visible = true;
            this.colCancelled.VisibleIndex = 4;
            this.colCancelled.Width = 64;
            // 
            // repositoryItemTextEdit_Cancelled
            // 
            this.repositoryItemTextEdit_Cancelled.Name = "repositoryItemTextEdit_Cancelled";
            this.repositoryItemTextEdit_Cancelled.FormatEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repositoryItemTextEdit_Cancelled_FormatEditValue);
            // 
            // colCheck
            // 
            this.colCheck.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCheck.FieldName = "ToBeUpdate";
            this.colCheck.Name = "colCheck";
            this.colCheck.Visible = true;
            this.colCheck.VisibleIndex = 0;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colCreatedTimeStamp
            // 
            this.colCreatedTimeStamp.Caption = "CreatedTimeStamp";
            this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
            this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
            this.colCreatedTimeStamp.Visible = true;
            this.colCreatedTimeStamp.VisibleIndex = 17;
            this.colCreatedTimeStamp.Width = 31;
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.Caption = "CreatedUserID";
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.Name = "colCreatedUserID";
            this.colCreatedUserID.Visible = true;
            this.colCreatedUserID.VisibleIndex = 16;
            this.colCreatedUserID.Width = 86;
            // 
            // colDeptDesc
            // 
            this.colDeptDesc.Caption = "DeptDesc";
            this.colDeptDesc.FieldName = "DeptDesc";
            this.colDeptDesc.Name = "colDeptDesc";
            // 
            // colDeptNo
            // 
            this.colDeptNo.Caption = "Dept No.";
            this.colDeptNo.FieldName = "DeptNo";
            this.colDeptNo.Name = "colDeptNo";
            // 
            // colJENo
            // 
            this.colJENo.Caption = "JENo";
            this.colJENo.FieldName = "JENo";
            this.colJENo.Name = "colJENo";
            this.colJENo.Visible = true;
            this.colJENo.VisibleIndex = 7;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 3;
            this.colDescription.Width = 64;
            // 
            // colAccNo
            // 
            this.colAccNo.Caption = "AccNo";
            this.colAccNo.FieldName = "AccNo";
            this.colAccNo.Name = "colAccNo";
            this.colAccNo.Visible = true;
            this.colAccNo.VisibleIndex = 9;
            // 
            // colDocDate
            // 
            this.colDocDate.Caption = "DocDate";
            this.colDocDate.FieldName = "DocDate";
            this.colDocDate.Name = "colDocDate";
            this.colDocDate.Visible = true;
            this.colDocDate.VisibleIndex = 2;
            this.colDocDate.Width = 64;
            // 
            // colDocNo
            // 
            this.colDocNo.Caption = "DocNo";
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 1;
            this.colDocNo.Width = 64;
            // 
            // colDtlDescription
            // 
            this.colDtlDescription.Caption = "Detail Description";
            this.colDtlDescription.FieldName = "DtlDescription";
            this.colDtlDescription.Name = "colDtlDescription";
            this.colDtlDescription.Visible = true;
            this.colDtlDescription.VisibleIndex = 8;
            this.colDtlDescription.Width = 64;
            // 
            // colFurtherDescription
            // 
            this.colFurtherDescription.Caption = "FurtherDescription";
            this.colFurtherDescription.ColumnEdit = this.repositoryItemFurtherDescription;
            this.colFurtherDescription.FieldName = "FurtherDescription";
            this.colFurtherDescription.Name = "colFurtherDescription";
            this.colFurtherDescription.Width = 103;
            // 
            // repositoryItemFurtherDescription
            // 
            this.repositoryItemFurtherDescription.Name = "repositoryItemFurtherDescription";
            this.repositoryItemFurtherDescription.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemFurtherDescription.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemFurtherDescription_ButtonClick);
            this.repositoryItemFurtherDescription.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemFurtherDescription_ButtonClick);
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "ItemCode";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 6;
            this.colItemCode.Width = 64;
            // 
            // colItemGroup
            // 
            this.colItemGroup.Caption = "ItemGroup";
            this.colItemGroup.FieldName = "ItemGroup";
            this.colItemGroup.Name = "colItemGroup";
            // 
            // colItemType
            // 
            this.colItemType.Caption = "ItemType";
            this.colItemType.FieldName = "ItemType";
            this.colItemType.Name = "colItemType";
            // 
            // colLastModified
            // 
            this.colLastModified.Caption = "Last Modified";
            this.colLastModified.FieldName = "LastModified";
            this.colLastModified.Name = "colLastModified";
            // 
            // colLastModifiedUserID
            // 
            this.colLastModifiedUserID.Caption = "LastModifiedUserID";
            this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
            this.colLastModifiedUserID.Name = "colLastModifiedUserID";
            // 
            // colLocation
            // 
            this.colLocation.Caption = "Location";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            // 
            // colPrintCount
            // 
            this.colPrintCount.Caption = "Print Count";
            this.colPrintCount.FieldName = "PrintCount";
            this.colPrintCount.Name = "colPrintCount";
            // 
            // colProjDesc
            // 
            this.colProjDesc.Caption = "Proj Desc";
            this.colProjDesc.FieldName = "ProjDesc";
            this.colProjDesc.Name = "colProjDesc";
            // 
            // colProjNo
            // 
            this.colProjNo.Caption = "ProjNo";
            this.colProjNo.FieldName = "ProjNo";
            this.colProjNo.Name = "colProjNo";
            this.colProjNo.Visible = true;
            this.colProjNo.VisibleIndex = 11;
            this.colProjNo.Width = 58;
            // 
            // colQty
            // 
            this.colQty.Caption = "Qty";
            this.colQty.FieldName = "Qty";
            this.colQty.Name = "colQty";
            this.colQty.Visible = true;
            this.colQty.VisibleIndex = 12;
            this.colQty.Width = 58;
            // 
            // colRefDocNo
            // 
            this.colRefDocNo.Caption = "RefDocNo";
            this.colRefDocNo.FieldName = "RefDocNo";
            this.colRefDocNo.Name = "colRefDocNo";
            this.colRefDocNo.Visible = true;
            this.colRefDocNo.VisibleIndex = 5;
            this.colRefDocNo.Width = 64;
            // 
            // colRemark1
            // 
            this.colRemark1.Caption = "Remark 1";
            this.colRemark1.FieldName = "Remark1";
            this.colRemark1.Name = "colRemark1";
            // 
            // colRemark2
            // 
            this.colRemark2.Caption = "Remark 2";
            this.colRemark2.FieldName = "Remark2";
            this.colRemark2.Name = "colRemark2";
            // 
            // colRemark3
            // 
            this.colRemark3.Caption = "Remark 3";
            this.colRemark3.FieldName = "Remark3";
            this.colRemark3.Name = "colRemark3";
            // 
            // colRemark4
            // 
            this.colRemark4.Caption = "Remark 4";
            this.colRemark4.FieldName = "Remark4";
            this.colRemark4.Name = "colRemark4";
            // 
            // colSubTotal
            // 
            this.colSubTotal.Caption = "SubTotal";
            this.colSubTotal.FieldName = "SubTotal";
            this.colSubTotal.Name = "colSubTotal";
            this.colSubTotal.Visible = true;
            this.colSubTotal.VisibleIndex = 14;
            this.colSubTotal.Width = 65;
            // 
            // colTotal
            // 
            this.colTotal.Caption = "Total";
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.Visible = true;
            this.colTotal.VisibleIndex = 15;
            this.colTotal.Width = 70;
            // 
            // colUnitCost
            // 
            this.colUnitCost.Caption = "UnitCost";
            this.colUnitCost.FieldName = "UnitCost";
            this.colUnitCost.Name = "colUnitCost";
            this.colUnitCost.Visible = true;
            this.colUnitCost.VisibleIndex = 13;
            this.colUnitCost.Width = 58;
            // 
            // colDebit
            // 
            this.colDebit.Caption = "Debit";
            this.colDebit.FieldName = "Debit";
            this.colDebit.Name = "colDebit";
            this.colDebit.Visible = true;
            this.colDebit.VisibleIndex = 18;
            // 
            // colUOM
            // 
            this.colUOM.Caption = "UOM";
            this.colUOM.FieldName = "UOM";
            this.colUOM.Name = "colUOM";
            this.colUOM.Visible = true;
            this.colUOM.VisibleIndex = 10;
            this.colUOM.Width = 64;
            // 
            // colCredit
            // 
            this.colCredit.Caption = "Credit";
            this.colCredit.FieldName = "Credit";
            this.colCredit.Name = "colCredit";
            this.colCredit.Visible = true;
            this.colCredit.VisibleIndex = 19;
            // 
            // colFromDocNo
            // 
            this.colFromDocNo.Caption = "FromDocNo";
            this.colFromDocNo.FieldName = "FromDocNo";
            this.colFromDocNo.Name = "colFromDocNo";
            this.colFromDocNo.Visible = true;
            this.colFromDocNo.VisibleIndex = 20;
            // 
            // colFromDocType
            // 
            this.colFromDocType.Caption = "FromDocType";
            this.colFromDocType.FieldName = "FromDocType";
            this.colFromDocType.Name = "colFromDocType";
            this.colFromDocType.Visible = true;
            this.colFromDocType.VisibleIndex = 21;
            // 
            // ucSearchResult1
            // 
            this.ucSearchResult1.Appearance.Options.UseBackColor = true;
            this.ucSearchResult1.ClearAllUncheckRecords = false;
            this.ucSearchResult1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucSearchResult1.KeepSearchResultVisible = false;
            this.ucSearchResult1.Location = new System.Drawing.Point(0, 0);
            this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ucSearchResult1.Name = "ucSearchResult1";
            this.ucSearchResult1.Size = new System.Drawing.Size(961, 46);
            this.ucSearchResult1.TabIndex = 1;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.memoEdit_Criteria);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(961, 160);
            this.xtraTabPage4.Text = "Criteria";
            // 
            // memoEdit_Criteria
            // 
            this.memoEdit_Criteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit_Criteria.Location = new System.Drawing.Point(0, 0);
            this.memoEdit_Criteria.Name = "memoEdit_Criteria";
            this.memoEdit_Criteria.Properties.Appearance.Options.UseFont = true;
            this.memoEdit_Criteria.Properties.ReadOnly = true;
            this.memoEdit_Criteria.Size = new System.Drawing.Size(961, 160);
            this.memoEdit_Criteria.TabIndex = 0;
            this.memoEdit_Criteria.UseOptimizedRendering = true;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(961, 156);
            this.xtraTabPage6.Text = "xtraTabPage6";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem2,
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 7;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 1";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.AllowRename = true;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Custom 1";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Report";
            this.barSubItem2.Id = 4;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Design Detail Listing Report";
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDetailListingReport_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Switch to Advanced Options";
            this.barButtonItem2.Id = 6;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnAdvancedFilter_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(967, 29);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 662);
            this.barDockControlBottom.Size = new System.Drawing.Size(967, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 29);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 633);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(967, 29);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 633);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnDesignDetailListingReport
            // 
            this.barBtnDesignDetailListingReport.Id = 2;
            this.barBtnDesignDetailListingReport.Name = "barBtnDesignDetailListingReport";
            this.barBtnDesignDetailListingReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDetailListingReport_ItemClick);
            // 
            // barbtnAdvancedFilter
            // 
            this.barbtnAdvancedFilter.Id = 1;
            this.barbtnAdvancedFilter.Name = "barbtnAdvancedFilter";
            this.barbtnAdvancedFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnAdvancedFilter_ItemClick);
            // 
            // barBtnConvertToPlainText
            // 
            this.barBtnConvertToPlainText.Id = 3;
            this.barBtnConvertToPlainText.Name = "barBtnConvertToPlainText";
            this.barBtnConvertToPlainText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnConvertToPlainText_ItemClick);
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Print Stock Issue Raw Material Detail Listing";
            this.panelHeader1.HelpNavigator = System.Windows.Forms.HelpNavigator.AssociateIndex;
            this.panelHeader1.HelpTopicId = "Stock_Issue.htm#stk028";
            this.panelHeader1.Location = new System.Drawing.Point(0, 29);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(967, 44);
            this.panelHeader1.TabIndex = 4;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // FormStockIssuePrintDetailListing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnClose;
            this.ClientSize = new System.Drawing.Size(967, 662);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelCenter);
            this.Controls.Add(this.pnCriteriaBasic);
            this.Controls.Add(this.panelCriteria);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormStockIssuePrintDetailListing";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormStockIssuePrintDetailListing_Closing);
            this.Load += new System.EventHandler(this.FormSIDetailPrintSearch_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormStockIssuePrintDetailListing_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).EndInit();
            this.panelCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbFilter)).EndInit();
            this.gbFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbReport)).EndInit();
            this.gbReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaBasic)).EndInit();
            this.pnCriteriaBasic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbBasic)).EndInit();
            this.gbBasic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelCenter)).EndInit();
            this.panelCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit_Cancelled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFurtherDescription)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        public class FormEventArgs
        {
            private FormStockIssuePrintDetailListing myForm;

            public StockIssueReportCommand Command
            {
                get
                {
                    return this.myForm.myCommand;
                }
            }

            public PanelControl PanelCriteria
            {
                get
                {
                    return this.myForm.panelCriteria;
                }
            }

            public PanelControl PanelButtons
            {
                get
                {
                    return this.myForm.panelCenter;
                }
            }

            public XtraTabControl TabControl
            {
                get
                {
                    return this.myForm.xtraTabControl2;
                }
            }

            public GridControl GridControl
            {
                get
                {
                    return this.myForm.gridControl1;
                }
            }

            public FormStockIssuePrintDetailListing Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public DBSetting DBSetting
            {
                get
                {
                    return this.myForm.myDBSetting;
                }
            }

            public FormEventArgs(FormStockIssuePrintDetailListing form)
            {
                this.myForm = form;
            }
        }

        public class FormInitializeEventArgs : FormStockIssuePrintDetailListing.FormEventArgs
        {
            public FormInitializeEventArgs(FormStockIssuePrintDetailListing form)
              : base(form)
            {
            }
        }

        public class FormInquiryEventArgs : FormStockIssuePrintDetailListing.FormEventArgs
        {
            private DataTable myResultTable;

            public DataTable ResultTable
            {
                get
                {
                    return this.myResultTable;
                }
            }

            public FormInquiryEventArgs(FormStockIssuePrintDetailListing form, DataTable resultTable)
              : base(form)
            {
                this.myResultTable = resultTable;
            }
        }
    }

}
