﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueDetail
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount.Data;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.SerialNumber2;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Data;

namespace Production.StockIssueRM
{
    public class StockIssueSubDetail : BaseRecord
    {
        private StockIssue myStockIssue;
        private StockIssueDetail myStockIssueDetail;
        protected DecimalSetting myDecimalSetting;

        public DataRow Row
        {
            get
            {
                return this.myRow;
            }
        }
        public int Seq
        {
            get
            {
                return BCE.Data.Convert.ToInt32(this.myRow["Seq"]);
            }
        }

        internal StockIssueSubDetail(DataRow row, StockIssue stockIssue, StockIssueDetail stockIssueDetail)
      : base(row)
        {
            this.myStockIssue = stockIssue;
            this.myStockIssueDetail = stockIssueDetail;
            this.myDecimalSetting = DecimalSetting.GetOrCreate(stockIssue.Command.DBSetting);
        }


    }
}
