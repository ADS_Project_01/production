﻿// Type: BCE.AutoCount.Stock.StockIssue.AdvancedStockIssueCriteria
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.SearchFilter;
//using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Localization;
using System;

namespace Production.StockIssueRM
{
  [Serializable]
  public class AdvancedStockIssueCriteria : SearchCriteria
  {
    public AdvancedStockIssueCriteria(DBSetting dbSetting)
    {
      this.Add((SearchElement) new SearchDecorator("", "", Localizer.GetString((Enum) StockIssueColumnStringId.MasterSearchFields, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "DocNo", Localizer.GetString((Enum) StockIssueColumnStringId.StockIssueNo, new object[0]), FilterControlType.StockIssue));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "DocDate", Localizer.GetString((Enum) StockIssueColumnStringId.StockIssueDate, new object[0]), FilterControlType.Date));
      this.Add((SearchElement) new TextSearch("A", "Description", Localizer.GetString((Enum) StockIssueColumnStringId.Description, new object[0])));
      this.Add((SearchElement) new NumberSearch("A", "Total", Localizer.GetString((Enum) StockIssueColumnStringId.Total, new object[0])));
      this.Add((SearchElement) new MemoSearch("A", "Note", Localizer.GetString((Enum) StockIssueColumnStringId.Note, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark1", Localizer.GetString((Enum) StockIssueColumnStringId.Remark1, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark2", Localizer.GetString((Enum) StockIssueColumnStringId.Remark2, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark3", Localizer.GetString((Enum) StockIssueColumnStringId.Remark3, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark4", Localizer.GetString((Enum) StockIssueColumnStringId.Remark4, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "RefDocNo", Localizer.GetString((Enum) StockIssueColumnStringId.RefDocNo, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "CreatedUserID", Localizer.GetString((Enum) StockIssueColumnStringId.CreatedUserID, new object[0]), FilterControlType.User));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "LastModifiedUserID", Localizer.GetString((Enum) StockIssueColumnStringId.LastModifiedUserID, new object[0]), FilterControlType.User));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "CreatedTimeStamp", Localizer.GetString((Enum) StockIssueColumnStringId.CreatedTimestamp, new object[0]), FilterControlType.DateTime));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "LastModified", Localizer.GetString((Enum) StockIssueColumnStringId.LastModified, new object[0]), FilterControlType.DateTime));
      this.Add((SearchElement) new BooleanSearch("A", "Cancelled", Localizer.GetString((Enum) StockIssueColumnStringId.CancelledDocument, new object[0])));
      //new UDFUtil(dbSetting).AddUDFIntoAdvancedSearch((SearchCriteria) this, "RPA_RM", "A");
      this.Add((SearchElement) new SearchDecorator("", "", Localizer.GetString((Enum) StockIssueDetailColumnStringId.DetailSearchFields, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "ItemCode", Localizer.GetString((Enum) StockIssueDetailColumnStringId.ItemCode, new object[0]), FilterControlType.Item));
      this.Add((SearchElement) new TextSearch("B", "Description", Localizer.GetString((Enum) StockIssueDetailColumnStringId.ItemDescription, new object[0])));
      this.Add((SearchElement) new MemoSearch("B", "FurtherDescription", Localizer.GetString((Enum) StockIssueDetailColumnStringId.ItemFurtherDescription, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "Location", Localizer.GetString((Enum) StockIssueDetailColumnStringId.Location, new object[0]), FilterControlType.Location));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "ProjNo", Localizer.GetString((Enum) StockIssueDetailColumnStringId.Project, new object[0]), FilterControlType.Project));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "DeptNo", Localizer.GetString((Enum) StockIssueDetailColumnStringId.Department, new object[0]), FilterControlType.Department));
      this.Add((SearchElement) new TextSearch("B", "BatchNo", Localizer.GetString((Enum) StockIssueDetailColumnStringId.BatchNo, new object[0])));
      this.Add((SearchElement) new TextSearch("B", "UOM", Localizer.GetString((Enum) StockIssueDetailColumnStringId.UOM, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "Qty", Localizer.GetString((Enum) StockIssueDetailColumnStringId.Quantity, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "UnitCost", Localizer.GetString((Enum) StockIssueDetailColumnStringId.UnitCost, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "SubTotal", Localizer.GetString((Enum) StockIssueDetailColumnStringId.SubTotal, new object[0])));
      this.Add((SearchElement) new TextSearch("B", "Numbering", Localizer.GetString((Enum) StockIssueDetailColumnStringId.Numbering, new object[0])));
     // new UDFUtil(dbSetting).AddUDFIntoAdvancedSearch((SearchCriteria) this, "RPA_RMDtl", "B");
    }
  }
}
