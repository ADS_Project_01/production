﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.ItemBOMLookupEditBuilder
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll
using BCE.AutoCount;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
namespace Production.StockIssueRM
{
  public class StockIssueRMLookupEditBuilder : LookupEditBuilderEx
  {
        private string myLastDocNo = "!@#!@#!!@#";
        public StockIssueRMLookupEditBuilder(string docno)
    {
      //this.myUseCaching = true;
      //this.myCacheFileName = "RPA_RM.Data";
      //this.myCmdString = "SELECT distinct RPA_RM.DocNo,RPA_RM.DocDate, RPA_RM.Description,FromDocNo as WONo FROM RPA_RM inner join RPA_RMDTL ON RPA_RM.DocKey = RPA_RMDTL.DocKey left outer Join RPA_WO ON RPA_WO.DocNo=RPA_RMDTL.FromDocNo where RPA_RM.cancelled='F' AND ISNULL(RPA_WO.Status,'')!='Closed'";

            if (docno.Length == 0)
            {
                this.myCmdString = "SELECT distinct RPA_RM.DocNo,RPA_RM.DocDate, RPA_RM.Description,FromDocNo as WONo FROM RPA_RM inner join RPA_RMDTL ON RPA_RM.DocKey = RPA_RMDTL.DocKey left outer Join RPA_WO ON RPA_WO.DocNo=RPA_RMDTL.FromDocNo where RPA_RM.cancelled='F' AND ISNULL(RPA_WO.Status,'')!='Closed' and exists(select 1 from RPA_RMDTL Z WHERE COALESCE(Qty, 0) - COALESCE(TransferedQty, 0) > 0 and Z.DocKey = RPA_RM.DocKey)";
            }
            else
            {
                this.myCmdString = "SELECT distinct RPA_RM.DocNo,RPA_RM.DocDate, RPA_RM.Description,FromDocNo as WONo FROM RPA_RM inner join RPA_RMDTL ON RPA_RM.DocKey = RPA_RMDTL.DocKey left outer Join RPA_WO ON RPA_WO.DocNo=RPA_RMDTL.FromDocNo where RPA_RM.cancelled='F' and RPA_RM.DocNo='"+ docno + "'";
            }
        }

    protected override void SetupLookupEdit(RepositoryItemLookUpEdit myLookupEdit)
    {
      base.SetupLookupEdit(myLookupEdit);
      myLookupEdit.DisplayMember = "DocNo";
      myLookupEdit.ValueMember = "DocNo";
      myLookupEdit.DataSource = (object) this.myDataTable;
      myLookupEdit.Columns.Clear();
            myLookupEdit.Columns.Add(new LookUpColumnInfo("DocNo", "No",150));
            myLookupEdit.Columns.Add(new LookUpColumnInfo("DocDate", "Date",100));
            myLookupEdit.Columns.Add(new LookUpColumnInfo("WONo", "WO No", 100));
            myLookupEdit.Columns.Add(new LookUpColumnInfo("Description", BCE.Localization.Localizer.GetString((Enum)Production.GeneralMaintenance.ItemBOM.ItemBOMString.Description, new object[0]), 150));
      myLookupEdit.PopupWidth = 400;
      myLookupEdit.AutoSearchColumnIndex = 1;
      myLookupEdit.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
      myLookupEdit.KeyPress += new KeyPressEventHandler(this.LookupEdt_KeyPress);
    }
        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastDocNo = "!@#!@#!!@#";
        }

        public void FilterLookup(string docno)
        {
            //if (!(docno == this.myLastDocNo) || this.NeedUpdate())
            {
                this.myLastDocNo = docno;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();

                        SqlCommand command = sqlConnection.CreateCommand();
                        if (docno.Length == 0)
                        {
                            string str = "SELECT distinct RPA_RM.DocNo,RPA_RM.DocDate, RPA_RM.Description,FromDocNo as WONo FROM RPA_RM inner join RPA_RMDTL ON RPA_RM.DocKey = RPA_RMDTL.DocKey left outer Join RPA_WO ON RPA_WO.DocNo=RPA_RMDTL.FromDocNo where RPA_RM.cancelled='F' AND ISNULL(RPA_WO.Status,'')!='Closed' and exists(select 1 from RPA_RMDTL Z WHERE COALESCE(Qty, 0) - COALESCE(TransferedQty, 0) > 0 and Z.DocKey = RPA_RM.DocKey) ";
                            command.CommandText = str;
                        }
                        else
                        {
                            string str = "SELECT distinct RPA_RM.DocNo,RPA_RM.DocDate, RPA_RM.Description,FromDocNo as WONo FROM RPA_RM inner join RPA_RMDTL ON RPA_RM.DocKey = RPA_RMDTL.DocKey left outer Join RPA_WO ON RPA_WO.DocNo=RPA_RMDTL.FromDocNo where RPA_RM.cancelled='F' and RPA_RM.DocNo='@DocNo'";
                            command.CommandText = str;
                            command.Parameters.AddWithValue("@DocNo", (object)docno);
                        }
                        //command.Parameters.AddWithValue("@ItemCode", (object)docno);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        StockIssueRMLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }

        protected override int GetChangeCount()
    {
      return this.myChangeCount.GetChangeCount("RPA_WO");
    }

    public override void UnlinkEventHandlers(RepositoryItemLookUpEdit lookupEdit)
    {
      base.UnlinkEventHandlers(lookupEdit);
      lookupEdit.KeyPress -= new KeyPressEventHandler(this.LookupEdt_KeyPress);
    }

    private void LookupEdt_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (sender is LookUpEdit)
      {
        LookUpEdit lookUpEdit = sender as LookUpEdit;
        if (!lookUpEdit.IsPopupOpen)
        {
          BaseView baseView = (BaseView) null;
          if ((int) e.KeyChar >= 32 && (int) e.KeyChar <= 126)
          {
            if (baseView != null)
            {
              baseView.ShowEditorByKey(new KeyEventArgs((Keys) 115));
              baseView.ShowEditorByKeyPress(new KeyPressEventArgs(e.KeyChar));
            }
            else
            {
              lookUpEdit.ShowPopup();
              lookUpEdit.SendKey((object) new Message(), new KeyPressEventArgs(e.KeyChar));
            }
            e.Handled = true;
          }
        }
      }
    }
  }
}
