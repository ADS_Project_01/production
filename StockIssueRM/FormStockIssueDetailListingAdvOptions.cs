﻿using BCE.AutoCount;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using BCE.Localization;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTab;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
namespace Production.StockIssueRM
{
    public class FormStockIssueDetailListingAdvOptions : XtraForm
    {
        private StockIssueDetailReportingCriteria myReportingCriteriaDetail;
        private DBSetting myDBSetting;
        private IContainer components;
        private PanelControl panelControl1;
        private PanelControl panelControl2;
        private GroupControl gbFilter;
        private XtraTabControl xtraTabControl1;
        private XtraTabPage xtraTabPage1;
        private XtraTabPage xtraTabPage2;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private UCItemGroupSelector ucItemGroupSelector1;
        private UCItemTypeSelector ucItemTypeSelector1;
        private UCLocationSelector ucLocationSelector1;
        private UCAllProjectsSelector ucProjectSelector1;
        private UCAllDepartmentsSelector ucDepartmentSelector1;
        private SimpleButton sbtnOK;
        private SimpleButton sbtnClose;
        private ComboBoxEdit cbCancelledOption;

        public object IsPrintCancelled
        {
            get
            {
                return this.cbCancelledOption.SelectedItem;
            }
        }

        public FormStockIssueDetailListingAdvOptions(DBSetting dbSetting, StockIssueDetailReportingCriteria myReportingCriteria)
        {
            this.InitializeComponent();
            this.Icon = BCE.AutoCount.Application.Icon;
            this.myDBSetting = dbSetting;
            this.myReportingCriteriaDetail = myReportingCriteria;
            this.cbCancelledOption.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(CancelledDocumentOption)));
            this.ucItemGroupSelector1.Initialize(this.myDBSetting, myReportingCriteria.ItemGroupFilter);
            this.ucItemTypeSelector1.Initialize(this.myDBSetting, myReportingCriteria.ItemTypeFilter);
            this.ucLocationSelector1.Initialize(this.myDBSetting, myReportingCriteria.LocationFilter);
            this.ucProjectSelector1.Initialize(this.myDBSetting, myReportingCriteria.ProjecNoFilter);
            this.ucDepartmentSelector1.Initialize(this.myDBSetting, myReportingCriteria.DeptNoFilter);
        }

        public void SetFilterByLocation(bool filterByLocation)
        {
            this.ucLocationSelector1.Enabled = !filterByLocation;
        }

        private void InitializeSettings(StockIssueDetailReportingCriteria myReportingCriteria)
        {
            this.ucItemGroupSelector1.ApplyFilter(myReportingCriteria.ItemGroupFilter);
            this.ucItemTypeSelector1.ApplyFilter(myReportingCriteria.ItemTypeFilter);
            this.ucLocationSelector1.ApplyFilter(myReportingCriteria.LocationFilter);
            this.ucProjectSelector1.ApplyFilter(myReportingCriteria.ProjecNoFilter);
            this.ucDepartmentSelector1.ApplyFilter(myReportingCriteria.DeptNoFilter);
            if (myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.All)
                this.cbCancelledOption.SelectedIndex = 0;
            else if (myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
                this.cbCancelledOption.SelectedIndex = 1;
            else
                this.cbCancelledOption.SelectedIndex = 2;
        }

        private void FormStockIssueDetailListingAdvOptions_Load(object sender, EventArgs e)
        {
            this.InitializeSettings(this.myReportingCriteriaDetail);
        }

        private void sbtnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FormStockIssueDetailListingAdvOptions));
            this.panelControl1 = new PanelControl();
            this.gbFilter = new GroupControl();
            this.xtraTabControl1 = new XtraTabControl();
            this.xtraTabPage1 = new XtraTabPage();
            this.cbCancelledOption = new ComboBoxEdit();
            this.label1 = new Label();
            this.xtraTabPage2 = new XtraTabPage();
            this.ucProjectSelector1 = new UCAllProjectsSelector();
            this.ucLocationSelector1 = new UCLocationSelector();
            this.ucItemTypeSelector1 = new UCItemTypeSelector();
            this.ucItemGroupSelector1 = new UCItemGroupSelector();
            this.label2 = new Label();
            this.label4 = new Label();
            this.label3 = new Label();
            this.label6 = new Label();
            this.label5 = new Label();
            this.ucDepartmentSelector1 = new UCAllDepartmentsSelector();
            this.panelControl2 = new PanelControl();
            this.sbtnClose = new SimpleButton();
            this.sbtnOK = new SimpleButton();
            this.panelControl1.BeginInit();
            this.panelControl1.SuspendLayout();
            this.gbFilter.BeginInit();
            this.gbFilter.SuspendLayout();
            this.xtraTabControl1.BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.cbCancelledOption.Properties.BeginInit();
            this.xtraTabPage2.SuspendLayout();
            this.panelControl2.BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            componentResourceManager.ApplyResources((object)this.panelControl1, "panelControl1");
            this.panelControl1.BorderStyle = BorderStyles.NoBorder;
            this.panelControl1.Controls.Add((Control)this.gbFilter);
            this.panelControl1.Name = "panelControl1";
            componentResourceManager.ApplyResources((object)this.gbFilter, "gbFilter");
            this.gbFilter.Controls.Add((Control)this.xtraTabControl1);
            this.gbFilter.Name = "gbFilter";
            componentResourceManager.ApplyResources((object)this.xtraTabControl1, "xtraTabControl1");
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            XtraTabPageCollection tabPages = this.xtraTabControl1.TabPages;
            XtraTabPage[] pages = new XtraTabPage[2];
            int index1 = 0;
            XtraTabPage xtraTabPage1 = this.xtraTabPage1;
            pages[index1] = xtraTabPage1;
            int index2 = 1;
            XtraTabPage xtraTabPage2 = this.xtraTabPage2;
            pages[index2] = xtraTabPage2;
            tabPages.AddRange(pages);
            componentResourceManager.ApplyResources((object)this.xtraTabPage1, "xtraTabPage1");
            this.xtraTabPage1.Controls.Add((Control)this.cbCancelledOption);
            this.xtraTabPage1.Controls.Add((Control)this.label1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            componentResourceManager.ApplyResources((object)this.cbCancelledOption, "cbCancelledOption");
            this.cbCancelledOption.Name = "cbCancelledOption";
            this.cbCancelledOption.Properties.AccessibleDescription = componentResourceManager.GetString("cbCancelledOption.Properties.AccessibleDescription");
            this.cbCancelledOption.Properties.AccessibleName = componentResourceManager.GetString("cbCancelledOption.Properties.AccessibleName");
            this.cbCancelledOption.Properties.AutoHeight = (bool)componentResourceManager.GetObject("cbCancelledOption.Properties.AutoHeight");
            EditorButtonCollection buttons1 = this.cbCancelledOption.Properties.Buttons;
            EditorButton[] buttons2 = new EditorButton[1];
            int index3 = 0;
            EditorButton editorButton = new EditorButton((ButtonPredefines)componentResourceManager.GetObject("cbCancelledOption.Properties.Buttons"));
            buttons2[index3] = editorButton;
            buttons1.AddRange(buttons2);
            this.cbCancelledOption.Properties.NullValuePrompt = componentResourceManager.GetString("cbCancelledOption.Properties.NullValuePrompt");
            this.cbCancelledOption.Properties.NullValuePromptShowForEmptyValue = (bool)componentResourceManager.GetObject("cbCancelledOption.Properties.NullValuePromptShowForEmptyValue");
            this.cbCancelledOption.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            componentResourceManager.ApplyResources((object)this.label1, "label1");
            this.label1.Name = "label1";
            componentResourceManager.ApplyResources((object)this.xtraTabPage2, "xtraTabPage2");
            this.xtraTabPage2.Controls.Add((Control)this.ucProjectSelector1);
            this.xtraTabPage2.Controls.Add((Control)this.ucLocationSelector1);
            this.xtraTabPage2.Controls.Add((Control)this.ucItemTypeSelector1);
            this.xtraTabPage2.Controls.Add((Control)this.ucItemGroupSelector1);
            this.xtraTabPage2.Controls.Add((Control)this.label2);
            this.xtraTabPage2.Controls.Add((Control)this.label4);
            this.xtraTabPage2.Controls.Add((Control)this.label3);
            this.xtraTabPage2.Controls.Add((Control)this.label6);
            this.xtraTabPage2.Controls.Add((Control)this.label5);
            this.xtraTabPage2.Controls.Add((Control)this.ucDepartmentSelector1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            componentResourceManager.ApplyResources((object)this.ucProjectSelector1, "ucProjectSelector1");
            this.ucProjectSelector1.Appearance.BackColor = (Color)componentResourceManager.GetObject("ucProjectSelector1.Appearance.BackColor");
            this.ucProjectSelector1.Appearance.GradientMode = (LinearGradientMode)componentResourceManager.GetObject("ucProjectSelector1.Appearance.GradientMode");
            this.ucProjectSelector1.Appearance.Image = (Image)componentResourceManager.GetObject("ucProjectSelector1.Appearance.Image");
            this.ucProjectSelector1.Appearance.Options.UseBackColor = true;
            this.ucProjectSelector1.Name = "ucProjectSelector1";
            componentResourceManager.ApplyResources((object)this.ucLocationSelector1, "ucLocationSelector1");
            this.ucLocationSelector1.Appearance.BackColor = (Color)componentResourceManager.GetObject("ucLocationSelector1.Appearance.BackColor");
            this.ucLocationSelector1.Appearance.GradientMode = (LinearGradientMode)componentResourceManager.GetObject("ucLocationSelector1.Appearance.GradientMode");
            this.ucLocationSelector1.Appearance.Image = (Image)componentResourceManager.GetObject("ucLocationSelector1.Appearance.Image");
            this.ucLocationSelector1.Appearance.Options.UseBackColor = true;
            this.ucLocationSelector1.LocationType = UCLocationType.ItemLocation;
            this.ucLocationSelector1.Name = "ucLocationSelector1";
            componentResourceManager.ApplyResources((object)this.ucItemTypeSelector1, "ucItemTypeSelector1");
            this.ucItemTypeSelector1.Appearance.BackColor = (Color)componentResourceManager.GetObject("ucItemTypeSelector1.Appearance.BackColor");
            this.ucItemTypeSelector1.Appearance.GradientMode = (LinearGradientMode)componentResourceManager.GetObject("ucItemTypeSelector1.Appearance.GradientMode");
            this.ucItemTypeSelector1.Appearance.Image = (Image)componentResourceManager.GetObject("ucItemTypeSelector1.Appearance.Image");
            this.ucItemTypeSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemTypeSelector1.Name = "ucItemTypeSelector1";
            componentResourceManager.ApplyResources((object)this.ucItemGroupSelector1, "ucItemGroupSelector1");
            this.ucItemGroupSelector1.Appearance.BackColor = (Color)componentResourceManager.GetObject("ucItemGroupSelector1.Appearance.BackColor");
            this.ucItemGroupSelector1.Appearance.GradientMode = (LinearGradientMode)componentResourceManager.GetObject("ucItemGroupSelector1.Appearance.GradientMode");
            this.ucItemGroupSelector1.Appearance.Image = (Image)componentResourceManager.GetObject("ucItemGroupSelector1.Appearance.Image");
            this.ucItemGroupSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemGroupSelector1.Name = "ucItemGroupSelector1";
            componentResourceManager.ApplyResources((object)this.label2, "label2");
            this.label2.Name = "label2";
            componentResourceManager.ApplyResources((object)this.label4, "label4");
            this.label4.Name = "label4";
            componentResourceManager.ApplyResources((object)this.label3, "label3");
            this.label3.Name = "label3";
            componentResourceManager.ApplyResources((object)this.label6, "label6");
            this.label6.Name = "label6";
            componentResourceManager.ApplyResources((object)this.label5, "label5");
            this.label5.Name = "label5";
            componentResourceManager.ApplyResources((object)this.ucDepartmentSelector1, "ucDepartmentSelector1");
            this.ucDepartmentSelector1.Appearance.BackColor = (Color)componentResourceManager.GetObject("ucDepartmentSelector1.Appearance.BackColor");
            this.ucDepartmentSelector1.Appearance.GradientMode = (LinearGradientMode)componentResourceManager.GetObject("ucDepartmentSelector1.Appearance.GradientMode");
            this.ucDepartmentSelector1.Appearance.Image = (Image)componentResourceManager.GetObject("ucDepartmentSelector1.Appearance.Image");
            this.ucDepartmentSelector1.Appearance.Options.UseBackColor = true;
            this.ucDepartmentSelector1.Name = "ucDepartmentSelector1";
            componentResourceManager.ApplyResources((object)this.panelControl2, "panelControl2");
            this.panelControl2.BorderStyle = BorderStyles.NoBorder;
            this.panelControl2.Controls.Add((Control)this.sbtnClose);
            this.panelControl2.Controls.Add((Control)this.sbtnOK);
            this.panelControl2.Name = "panelControl2";
            componentResourceManager.ApplyResources((object)this.sbtnClose, "sbtnClose");
            this.sbtnClose.DialogResult = DialogResult.Cancel;
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Click += new EventHandler(this.sbtnClose_Click);
            componentResourceManager.ApplyResources((object)this.sbtnOK, "sbtnOK");
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Click += new EventHandler(this.sbtnOK_Click);
            componentResourceManager.ApplyResources((object)this, "$this");
            this.AutoScaleMode = AutoScaleMode.Dpi;
            this.CancelButton = (IButtonControl)this.sbtnClose;
            this.Controls.Add((Control)this.panelControl2);
            this.Controls.Add((Control)this.panelControl1);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormStockIssueDetailListingAdvOptions";
            this.Load += new EventHandler(this.FormStockIssueDetailListingAdvOptions_Load);
            this.panelControl1.EndInit();
            this.panelControl1.ResumeLayout(false);
            this.gbFilter.EndInit();
            this.gbFilter.ResumeLayout(false);
            this.xtraTabControl1.EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            this.cbCancelledOption.Properties.EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            this.panelControl2.EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);
        }
    }

}
