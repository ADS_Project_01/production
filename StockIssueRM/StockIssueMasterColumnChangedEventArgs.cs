﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueMasterColumnChangedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueMasterColumnChangedEventArgs
  {
    private string myColumnName;
    private StockIssue myStock;

    public string ChangedColumnName
    {
      get
      {
        return this.myColumnName;
      }
    }

    public StockIssueRecord MasterRecord
    {
      get
      {
        return new StockIssueRecord(this.myStock);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.myDBSetting;
      }
    }

    internal StockIssueMasterColumnChangedEventArgs(string columnName, StockIssue doc)
    {
      this.myColumnName = columnName;
      this.myStock = doc;
    }
  }
}
