﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueDetailColumnChangedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;
using System.Data;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueDetailColumnChangedEventArgs
  {
    private string myColumnName;
    private DataRow myDetailRow;
    private StockIssue myStock;

    public string ChangedColumnName
    {
      get
      {
        return this.myColumnName;
      }
    }

    public StockIssueRecord MasterRecord
    {
      get
      {
        return new StockIssueRecord(this.myStock);
      }
    }

    public StockIssue StockIssue
    {
      get
      {
        return this.myStock;
      }
    }

    public StockIssueDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockIssueDetailRecord(this.myStock.Command.DBSetting, this.myDetailRow);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.myDBSetting;
      }
    }

    internal StockIssueDetailColumnChangedEventArgs(string columnName, StockIssue doc, DataRow detailRow)
    {
      this.myColumnName = columnName;
      this.myStock = doc;
      this.myDetailRow = detailRow;
    }
  }
}
