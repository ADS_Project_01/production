﻿// Type: BCE.AutoCount.Stock.MaterialRequest.FormMaterialRequestPrintListing
// Assembly: BCE.AutoCount.Stock, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Common;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PurchaseRequest
{
    public class FormMaterialRequestPrintListing : XtraForm, IAppForm
    {
        private string[] myDocumentStyleOptions = new string[2]
    {
      BCE.Localization.Localizer.GetString((Enum) MaterialRequestStringId.BatchPrintMaterialRequest, new object[0]),
      BCE.Localization.Localizer.GetString((Enum) MaterialRequestStringId.PrintMaterialRequestListing, new object[0])
    };
        private ScriptObject myScriptObject = ScriptManager.CreateObject("MaterialRequestListing");
        public static string myColumnName;
        private DBSetting myDBSetting;
        private DataTable myDataTable;
        private MaterialRequestCommand myCommand;
        private FormAdvancedSearch myFormAdvancedSearch;
        private AdvancedMaterialRequestCriteria myCriteria;
        private bool myInSearch;
        private long[] mySelectedDocKeylist;
        private MaterialRequestReportingCriteria myReportingCriteria;
        private MouseDownHelper myMouseDownHelper;
        private SimpleButton simpleButtonAdvanceSearch;
        private UCDateSelector ucDateSelector1;
        private Label label3;
        private Label label6;
        private GroupControl groupBox_SearchCriteria;
        private Label label4;
        private Label label7;
        private Label label8;
        private Label label9;
        private ComboBoxEdit cbEditGroupBy;
        private ComboBoxEdit cbEditSortBy;
        private ComboBoxEdit cbEditReportType;
        private XtraTabControl tabControl1;
        private XtraTabPage tabPage1;
        private XtraTabPage tabPage2;
        private PanelControl panel_Center;
        private MemoEdit memoEdit_Criteria;
        private UCSearchResult ucSearchResult1;
        private UCStockIssueSelector ucMaterialRequestSelector1;
        private ComboBoxEdit cbCancelledOption;
        private CheckEdit chkEditShowCriteria;
        private GroupControl gbReportOption;
        private PanelControl panelCriteria;
        private PanelControl panelControl1;
        private SimpleButton sbtnToggleOptions;
        private SimpleButton sbtnClose;
        private SimpleButton sbtnInquiry;
        private PreviewButton previewButton1;
        private PrintButton printButton1;
        private BarManager barManager1;
        private Bar bar2;
        private BarSubItem barSubItem1;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private BarButtonItem barBtnDesignDocumentStyleReport;
        private BarButtonItem barBtnDesignListingStyleReport;
        private PanelHeader panelHeader1;
        private MaterialRequestGrid stockIssueGrid1;
        private IContainer components;

        public DataTable MasterDataTable
        {
            get
            {
                return this.myDataTable;
            }
        }

        public long[] SelectedDocKeys
        {
            get
            {
                return this.mySelectedDocKeylist;
            }
        }

        public string SelectedDocNosInString
        {
            get
            {
                return this.GenerateDocNosToString();
            }
        }

        public string SelectedDocKeysInString
        {
            get
            {
                return StringHelper.ArrayListToCommaString(new ArrayList((ICollection)this.mySelectedDocKeylist));
            }
        }

        public string ColumnName
        {
            get
            {
                return FormMaterialRequestPrintListing.myColumnName;
            }
        }

        public MaterialRequestReportingCriteria MaterialRequestReportingCriteria
        {
            get
            {
                return this.myReportingCriteria;
            }
        }

        public FormMaterialRequestPrintListing(DBSetting dbSetting)
        {
            this.InitializeComponent();
            this.myDBSetting = dbSetting;
            this.myCommand = MaterialRequestCommand.Create(dbSetting);
            this.previewButton1.ReportType = "Material Request Document";
            this.printButton1.ReportType = "Material Request Document";
            this.myDataTable = new DataTable();
            this.stockIssueGrid1.Initialize(this.myDBSetting);
            this.stockIssueGrid1.DataSource = (object)this.myDataTable;
            this.LoadCriteria();
            this.ucSearchResult1.Initialize(this.stockIssueGrid1.GridView, "ToBeUpdate");
            this.InitUserControls();
            this.cbEditGroupBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(ListingGroupByOption)));
            this.cbEditSortBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(ListingSortByOption)));
            this.cbCancelledOption.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(CancelledDocumentOption)));
            this.cbEditReportType.Properties.Items.AddRange((object[])this.myDocumentStyleOptions);
            this.cbCancelledOption.SelectedIndex = 2;
            this.cbEditReportType.SelectedIndex = 1;
            this.RefreshDesignReport();
            this.myMouseDownHelper = new MouseDownHelper();
            this.myMouseDownHelper.Init(this.stockIssueGrid1.GridView);
            this.stockIssueGrid1.GridView.DoubleClick += new EventHandler(this.GridView_DoubleClick);
            BCE.AutoCount.Application.SetHelpTopic(this.panelHeader1, "Stock_Issue.htm#stk028");
            Activity.Log(dbSetting, "", 0L, 0L, BCE.Localization.Localizer.GetString((Enum)MaterialRequestString.OpenedPrintMaterialRequestListingWindow, new object[1]
      {
        (object) BCE.AutoCount.Application.DBSession.LoginUserID
      }), "");
        }

        protected override void Dispose(bool disposing)
        {
            this.SaveCriteria();
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void SaveCriteria()
        {
            BCE.AutoCount.Application.SaveCriteriaData((BCE.AutoCount.CustomSetting)this.myReportingCriteria, "MaterialRequestDocumentListingReport.setting");
        }

        private void LoadCriteria()
        {
           // this.myReportingCriteria = (MaterialRequestReportingCriteria)BCE.AutoCount.Application.LoadCriteriaData("MaterialRequestDocumentListingReport.setting");
            if (this.myReportingCriteria == null)
                this.myReportingCriteria = new MaterialRequestReportingCriteria();
            this.cbEditGroupBy.EditValue = (object)this.myReportingCriteria.GroupBy;
            this.cbEditSortBy.EditValue = (object)this.myReportingCriteria.SortBy;
            this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
            this.cbCancelledOption.SelectedIndex = (int)this.myReportingCriteria.IsPrintCancelled;
            this.chkEditShowCriteria.Checked = this.myReportingCriteria.IsShowCriteria;
        }

        private void InitializeComponent()
        {
            this.panelCriteria = new DevExpress.XtraEditors.PanelControl();
            this.cbEditReportType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox_SearchCriteria = new DevExpress.XtraEditors.GroupControl();
            this.cbCancelledOption = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucMaterialRequestSelector1 = new BCE.AutoCount.FilterUI.UCStockIssueSelector();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ucDateSelector1 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.simpleButtonAdvanceSearch = new DevExpress.XtraEditors.SimpleButton();
            this.gbReportOption = new DevExpress.XtraEditors.GroupControl();
            this.label7 = new System.Windows.Forms.Label();
            this.cbEditGroupBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbEditSortBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chkEditShowCriteria = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.tabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.stockIssueGrid1 = new PurchaseRequest.MaterialRequestGrid();
            this.ucSearchResult1 = new BCE.AutoCount.FilterUI.UCSearchResult();
            this.tabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit_Criteria = new DevExpress.XtraEditors.MemoEdit();
            this.panel_Center = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.printButton1 = new BCE.AutoCount.Controls.PrintButton();
            this.previewButton1 = new BCE.AutoCount.Controls.PreviewButton();
            this.sbtnToggleOptions = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnDesignDocumentStyleReport = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnDesignListingStyleReport = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).BeginInit();
            this.panelCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditReportType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox_SearchCriteria)).BeginInit();
            this.groupBox_SearchCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbReportOption)).BeginInit();
            this.gbReportOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Center)).BeginInit();
            this.panel_Center.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCriteria
            // 
            this.panelCriteria.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCriteria.Controls.Add(this.cbEditReportType);
            this.panelCriteria.Controls.Add(this.label9);
            this.panelCriteria.Controls.Add(this.groupBox_SearchCriteria);
            this.panelCriteria.Controls.Add(this.gbReportOption);
            this.panelCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCriteria.Location = new System.Drawing.Point(0, 44);
            this.panelCriteria.Name = "panelCriteria";
            this.panelCriteria.Size = new System.Drawing.Size(1009, 158);
            this.panelCriteria.TabIndex = 2;
            // 
            // cbEditReportType
            // 
            this.cbEditReportType.Location = new System.Drawing.Point(105, 6);
            this.cbEditReportType.Name = "cbEditReportType";
            this.cbEditReportType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditReportType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditReportType.Size = new System.Drawing.Size(168, 20);
            this.cbEditReportType.TabIndex = 0;
            this.cbEditReportType.SelectedIndexChanged += new System.EventHandler(this.cbEditReportType_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(8, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 23);
            this.label9.TabIndex = 1;
            this.label9.Text = "Report Type";
            // 
            // groupBox_SearchCriteria
            // 
            this.groupBox_SearchCriteria.Controls.Add(this.cbCancelledOption);
            this.groupBox_SearchCriteria.Controls.Add(this.ucMaterialRequestSelector1);
            this.groupBox_SearchCriteria.Controls.Add(this.label4);
            this.groupBox_SearchCriteria.Controls.Add(this.label6);
            this.groupBox_SearchCriteria.Controls.Add(this.label3);
            this.groupBox_SearchCriteria.Controls.Add(this.ucDateSelector1);
            this.groupBox_SearchCriteria.Controls.Add(this.simpleButtonAdvanceSearch);
            this.groupBox_SearchCriteria.Location = new System.Drawing.Point(11, 32);
            this.groupBox_SearchCriteria.Name = "groupBox_SearchCriteria";
            this.groupBox_SearchCriteria.Size = new System.Drawing.Size(596, 107);
            this.groupBox_SearchCriteria.TabIndex = 2;
            this.groupBox_SearchCriteria.Text = "Filter Options";
            // 
            // cbCancelledOption
            // 
            this.cbCancelledOption.Location = new System.Drawing.Point(134, 77);
            this.cbCancelledOption.Name = "cbCancelledOption";
            this.cbCancelledOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCancelledOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbCancelledOption.Size = new System.Drawing.Size(128, 20);
            this.cbCancelledOption.TabIndex = 0;
            this.cbCancelledOption.SelectedIndexChanged += new System.EventHandler(this.cbCancelledOption_SelectedIndexChanged);
            // 
            // ucMaterialRequestSelector1
            // 
            this.ucMaterialRequestSelector1.Location = new System.Drawing.Point(134, 50);
            this.ucMaterialRequestSelector1.Name = "ucMaterialRequestSelector1";
            this.ucMaterialRequestSelector1.Size = new System.Drawing.Size(441, 20);
            this.ucMaterialRequestSelector1.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 2;
            this.label4.Text = "Document Date";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(16, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 23);
            this.label6.TabIndex = 3;
            this.label6.Text = "Document Type";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cancelled Status";
            // 
            // ucDateSelector1
            // 
            this.ucDateSelector1.Location = new System.Drawing.Point(134, 24);
            this.ucDateSelector1.Name = "ucDateSelector1";
            this.ucDateSelector1.Size = new System.Drawing.Size(441, 20);
            this.ucDateSelector1.TabIndex = 5;
            // 
            // simpleButtonAdvanceSearch
            // 
            this.simpleButtonAdvanceSearch.Location = new System.Drawing.Point(463, 77);
            this.simpleButtonAdvanceSearch.Name = "simpleButtonAdvanceSearch";
            this.simpleButtonAdvanceSearch.Size = new System.Drawing.Size(126, 25);
            this.simpleButtonAdvanceSearch.TabIndex = 6;
            this.simpleButtonAdvanceSearch.Text = "Advance Search";
            this.simpleButtonAdvanceSearch.Click += new System.EventHandler(this.simpleButtonAdvanceSearch_Click);
            // 
            // gbReportOption
            // 
            this.gbReportOption.Controls.Add(this.label7);
            this.gbReportOption.Controls.Add(this.cbEditGroupBy);
            this.gbReportOption.Controls.Add(this.cbEditSortBy);
            this.gbReportOption.Controls.Add(this.chkEditShowCriteria);
            this.gbReportOption.Controls.Add(this.label8);
            this.gbReportOption.Location = new System.Drawing.Point(613, 32);
            this.gbReportOption.Name = "gbReportOption";
            this.gbReportOption.Size = new System.Drawing.Size(317, 107);
            this.gbReportOption.TabIndex = 3;
            this.gbReportOption.Text = "Report Options";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(5, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Group By";
            // 
            // cbEditGroupBy
            // 
            this.cbEditGroupBy.Location = new System.Drawing.Point(98, 24);
            this.cbEditGroupBy.Name = "cbEditGroupBy";
            this.cbEditGroupBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditGroupBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditGroupBy.Size = new System.Drawing.Size(145, 20);
            this.cbEditGroupBy.TabIndex = 1;
            this.cbEditGroupBy.SelectedIndexChanged += new System.EventHandler(this.cbEditGroupBy_SelectedIndexChanged);
            // 
            // cbEditSortBy
            // 
            this.cbEditSortBy.Location = new System.Drawing.Point(98, 51);
            this.cbEditSortBy.Name = "cbEditSortBy";
            this.cbEditSortBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditSortBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditSortBy.Size = new System.Drawing.Size(145, 20);
            this.cbEditSortBy.TabIndex = 2;
            this.cbEditSortBy.SelectedIndexChanged += new System.EventHandler(this.cbEditSortBy_SelectedIndexChanged);
            // 
            // chkEditShowCriteria
            // 
            this.chkEditShowCriteria.Location = new System.Drawing.Point(96, 78);
            this.chkEditShowCriteria.Name = "chkEditShowCriteria";
            this.chkEditShowCriteria.Properties.Caption = "Show Criteria in Report";
            this.chkEditShowCriteria.Size = new System.Drawing.Size(147, 19);
            this.chkEditShowCriteria.TabIndex = 3;
            this.chkEditShowCriteria.CheckedChanged += new System.EventHandler(this.chkEditShowCriteria_CheckedChanged);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(5, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 23);
            this.label8.TabIndex = 4;
            this.label8.Text = "Sort By";
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabPage = this.tabPage1;
            this.tabControl1.Size = new System.Drawing.Size(1009, 356);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPage1,
            this.tabPage2});
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.stockIssueGrid1);
            this.tabPage1.Controls.Add(this.ucSearchResult1);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1000, 325);
            this.tabPage1.Text = "Result";
            // 
            // stockIssueGrid1
            // 
            this.stockIssueGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stockIssueGrid1.Location = new System.Drawing.Point(0, 0);
            this.stockIssueGrid1.Name = "stockIssueGrid1";
            this.stockIssueGrid1.ShowTickColumn = true;
            this.stockIssueGrid1.Size = new System.Drawing.Size(1000, 325);
            this.stockIssueGrid1.TabIndex = 0;
            this.stockIssueGrid1.TickColumnFieldName = "ToBeUpdate";
            this.stockIssueGrid1.Load += new System.EventHandler(this.stockIssueGrid1_Load);
            // 
            // ucSearchResult1
            // 
            this.ucSearchResult1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(159)))), ((int)(((byte)(248)))));
            this.ucSearchResult1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(103)))), ((int)(((byte)(245)))));
            this.ucSearchResult1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.ucSearchResult1.Appearance.Options.UseBackColor = true;
            this.ucSearchResult1.ClearAllUncheckRecords = false;
            this.ucSearchResult1.KeepSearchResult = true;
            this.ucSearchResult1.Location = new System.Drawing.Point(0, 0);
            this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ucSearchResult1.Name = "ucSearchResult1";
            this.ucSearchResult1.Size = new System.Drawing.Size(664, 46);
            this.ucSearchResult1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.memoEdit_Criteria);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1000, 325);
            this.tabPage2.Text = "Crteria";
            // 
            // memoEdit_Criteria
            // 
            this.memoEdit_Criteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit_Criteria.Location = new System.Drawing.Point(0, 0);
            this.memoEdit_Criteria.Name = "memoEdit_Criteria";
            this.memoEdit_Criteria.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit_Criteria.Properties.Appearance.Options.UseFont = true;
            this.memoEdit_Criteria.Properties.ReadOnly = true;
            this.memoEdit_Criteria.Size = new System.Drawing.Size(1000, 325);
            this.memoEdit_Criteria.TabIndex = 0;
            // 
            // panel_Center
            // 
            this.panel_Center.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel_Center.Controls.Add(this.tabControl1);
            this.panel_Center.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Center.Location = new System.Drawing.Point(0, 234);
            this.panel_Center.Name = "panel_Center";
            this.panel_Center.Size = new System.Drawing.Size(1009, 356);
            this.panel_Center.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.printButton1);
            this.panelControl1.Controls.Add(this.previewButton1);
            this.panelControl1.Controls.Add(this.sbtnToggleOptions);
            this.panelControl1.Controls.Add(this.sbtnClose);
            this.panelControl1.Controls.Add(this.sbtnInquiry);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 202);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1009, 32);
            this.panelControl1.TabIndex = 1;
            // 
            // printButton1
            // 
            this.printButton1.Location = new System.Drawing.Point(170, 4);
            this.printButton1.Name = "printButton1";
            this.printButton1.ReportType = "";
            this.printButton1.Size = new System.Drawing.Size(72, 23);
            this.printButton1.TabIndex = 0;
            this.printButton1.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // previewButton1
            // 
            this.previewButton1.Location = new System.Drawing.Point(92, 4);
            this.previewButton1.Name = "previewButton1";
            this.previewButton1.ReportType = "";
            this.previewButton1.Size = new System.Drawing.Size(72, 23);
            this.previewButton1.TabIndex = 1;
            this.previewButton1.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // sbtnToggleOptions
            // 
            this.sbtnToggleOptions.Location = new System.Drawing.Point(248, 4);
            this.sbtnToggleOptions.Name = "sbtnToggleOptions";
            this.sbtnToggleOptions.Size = new System.Drawing.Size(83, 23);
            this.sbtnToggleOptions.TabIndex = 2;
            this.sbtnToggleOptions.Text = "Toggle Options";
            this.sbtnToggleOptions.Click += new System.EventHandler(this.sbtnToggleOptions_Click);
            // 
            // sbtnClose
            // 
            this.sbtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnClose.Location = new System.Drawing.Point(337, 4);
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Size = new System.Drawing.Size(75, 23);
            this.sbtnClose.TabIndex = 3;
            this.sbtnClose.Text = "Close";
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // sbtnInquiry
            // 
            this.sbtnInquiry.Location = new System.Drawing.Point(11, 4);
            this.sbtnInquiry.Name = "sbtnInquiry";
            this.sbtnInquiry.Size = new System.Drawing.Size(75, 23);
            this.sbtnInquiry.TabIndex = 4;
            this.sbtnInquiry.Text = "Inquiry";
            this.sbtnInquiry.Click += new System.EventHandler(this.sbtnInquiry_Click);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnDesignDocumentStyleReport),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnDesignListingStyleReport)});
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnDesignDocumentStyleReport
            // 
            this.barBtnDesignDocumentStyleReport.Id = 1;
            this.barBtnDesignDocumentStyleReport.Name = "barBtnDesignDocumentStyleReport";
            this.barBtnDesignDocumentStyleReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDocumentStyleReport_ItemClick);
            // 
            // barBtnDesignListingStyleReport
            // 
            this.barBtnDesignListingStyleReport.Id = 2;
            this.barBtnDesignListingStyleReport.Name = "barBtnDesignListingStyleReport";
            this.barBtnDesignListingStyleReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignListingStyleReport_ItemClick);
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Print Material Request";
            this.panelHeader1.HelpTopicId = "Stock_Issue.htm#stk028";
            this.panelHeader1.Location = new System.Drawing.Point(0, 0);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1009, 44);
            this.panelHeader1.TabIndex = 3;
            // 
            // FormMaterialRequestPrintListing
            // 
            this.CancelButton = this.sbtnClose;
            this.ClientSize = new System.Drawing.Size(1009, 590);
            this.Controls.Add(this.panel_Center);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelCriteria);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormMaterialRequestPrintListing";
            this.ShowInTaskbar = false;
            this.Text = "Print Material Request";
            this.Load += new System.EventHandler(this.FormSIPrintSearch_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormMaterialRequestList_Closing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormMaterialRequestPrintSearch_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).EndInit();
            this.panelCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbEditReportType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox_SearchCriteria)).EndInit();
            this.groupBox_SearchCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbReportOption)).EndInit();
            this.gbReportOption.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Center)).EndInit();
            this.panel_Center.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitUserControls()
        {
            this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
            this.ucMaterialRequestSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
        }

        private void AddNewColumn()
        {
            this.myDataTable.Columns.Add(new DataColumn()
            {
                DataType = System.Type.GetType("System.Boolean"),
                AllowDBNull = true,
                Caption = "Check",
                ColumnName = "ToBeUpdate",
                DefaultValue = (object)false
            });
        }

        private void BasicSearch(bool isSearchAll)
        {
            if (!this.myInSearch)
            {
                this.myInSearch = true;
                GridViewUtils.UpdateData(this.stockIssueGrid1.GridView);
                this.stockIssueGrid1.DataSource = (object)null;
                try
                {
                    this.stockIssueGrid1.GridControl.MainView.UpdateCurrentRow();
                    this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
                    this.myCommand.DocumentListingBasicSearch(this.myReportingCriteria, CommonFunction.BuildSQLColumns(isSearchAll, this.stockIssueGrid1.GridView.Columns.View), this.myDataTable, "ToBeUpdate");
                    if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                        this.AddNewColumn();
                    DevExpress.XtraGrid.Columns.GridColumn colStatus=new DevExpress.XtraGrid.Columns.GridColumn();
                    colStatus.Caption = "Status";
                    colStatus.FieldName = "Status";

                    if (this.stockIssueGrid1.GridView.Columns.IndexOf(colStatus) < 0)
                    {
                        stockIssueGrid1.GridView.Columns.Add(colStatus);
                        colStatus.Visible = true;
                        colStatus.VisibleIndex = 6;
                        colStatus.Width = 100;
                    }

                    //this.myDataTable.Columns.Add(new DataColumn()
                    //{
                    //    DataType = System.Type.GetType("System.String"),
                    //    AllowDBNull = true,
                    //    Caption = "Status",
                    //    ColumnName = "Status",
                    //    DefaultValue = (object)"Not Completed"
                    //});

                    this.stockIssueGrid1.DataSource = (object)this.myDataTable;
                }
                finally
                {
                    this.myInSearch = false;
                }
                FormMaterialRequestPrintListing.FormInquiryEventArgs inquiryEventArgs = new FormMaterialRequestPrintListing.FormInquiryEventArgs(this.panelCriteria, this.panelControl1, this.tabControl1, this.stockIssueGrid1.GridControl, this, this.myDataTable, this.myCommand);
                this.myScriptObject.RunMethod("OnFormInquiry", new System.Type[1]
        {
          inquiryEventArgs.GetType()
        }, new object[1]
        {
          (object) inquiryEventArgs
        });
            }
        }

        private void AdvancedSearch()
        {
            if (this.myFormAdvancedSearch == null)
            {
                this.myCriteria = new AdvancedMaterialRequestCriteria(this.myDBSetting);
                try
                {
                    this.myFormAdvancedSearch = new FormAdvancedSearch((SearchCriteria)this.myCriteria, this.myDBSetting);
                }
                catch (DataAccessException ex)
                {
                    AppMessage.ShowErrorMessage((Form)this, ex.Message);
                    return;
                }
            }
            this.myCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
            if (this.myFormAdvancedSearch.ShowDialog((IWin32Window)this) == DialogResult.OK)
            {
                if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                    this.AddNewColumn();
                this.memoEdit_Criteria.Lines = this.myCriteria.BuildReadableTextArray();
                this.ucSearchResult1.KeepSearchResult = this.myCriteria.KeepSearchResult;
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.myInSearch = true;
                try
                {
                    this.myCommand.AdvanceSearch(this.myCriteria, CommonFunction.BuildSQLColumns(false, this.stockIssueGrid1.GridView.Columns.View), this.myDataTable, "ToBeUpdate");
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage((Form)this, ex.Message);
                }
                finally
                {
                    this.ucSearchResult1.CheckAll();
                    this.myInSearch = false;
                    Cursor.Current = current;
                }
                FormMaterialRequestPrintListing.FormInquiryEventArgs inquiryEventArgs = new FormMaterialRequestPrintListing.FormInquiryEventArgs(this.panelCriteria, this.panelControl1, this.tabControl1, this.stockIssueGrid1.GridControl, this, this.myDataTable, this.myCommand);
                this.myScriptObject.RunMethod("OnFormInquiry", new System.Type[1]
        {
          inquiryEventArgs.GetType()
        }, new object[1]
        {
          (object) inquiryEventArgs
        });
            }
        }

        private string GenerateDocNosToString()
        {
            GridViewUtils.UpdateData(this.stockIssueGrid1.GridView);
            DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = True");
            if (dataRowArray.Length <= 0)
            {
                AppMessage.ShowMessage((Form)this, BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.ShowMessage_MaterialRequestNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
                return (string)null;
            }
            else
            {
                string str = "";
                foreach (DataRow dataRow in dataRowArray)
                {
                    if (str.Length != 0)
                        str = str + ", ";
                    str = str + "'" + dataRow["DocNo"].ToString() + "'";
                }
                return str;
            }
        }

        private void simpleButtonAdvanceSearch_Click(object sender, EventArgs e)
        {
            this.AdvancedSearch();
        }

        private void FormMaterialRequestList_Closing(object sender, CancelEventArgs e)
        {
            this.SaveCriteria();
        }

        private void ReloadAllColumns(object sender, EventArgs e)
        {
            this.BasicSearch(true);
        }

        private void sbtnInquiry_Click(object sender, EventArgs e)
        {
            this.BasicSearch(false);
            if (this.cbEditSortBy.Text != "")
                this.SortByOption();
            this.ucSearchResult1.CheckAll();
            this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
            this.sbtnToggleOptions.Enabled = this.stockIssueGrid1.DataSource != null;
            this.stockIssueGrid1.GridControl.Focus();
            Activity.Log(this.myDBSetting, "", 0L, 0L, BCE.Localization.Localizer.GetString((Enum)MaterialRequestString.InquiredPrintMaterialRequestListing, new object[1]
      {
        (object) BCE.AutoCount.Application.DBSession.LoginUserID
      }), this.myReportingCriteria.ReadableText);
        }

        private void cbEditGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.GroupBy = (ListingGroupByOption)this.cbEditGroupBy.SelectedIndex;
        }

        private void cbEditSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
            {
                if (this.myReportingCriteria.SortBy != (ListingSortByOption)this.cbEditSortBy.SelectedIndex)
                    this.myReportingCriteria.SortBy = (ListingSortByOption)this.cbEditSortBy.SelectedIndex;
                if (this.cbEditSortBy.Text != "")
                    this.SortByOption();
            }
        }

        private void cbEditReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.myReportingCriteria.ReportTypeOption = this.cbEditReportType.SelectedIndex;
            this.Text = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_PrintMaterialRequest, new object[1]
      {
        (object) this.cbEditReportType.Text
      });
            if (this.cbEditReportType.SelectedIndex == 0)
            {
                this.previewButton1.ReportType = "Material Request Document";
                this.printButton1.ReportType = "Material Request Document";
            }
            else
            {
                this.previewButton1.ReportType = "Material Request Listing";
                this.printButton1.ReportType = "Material Request Listing";
            }
        }

        private void cbCancelledOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.IsPrintCancelled = (CancelledDocumentOption)this.cbCancelledOption.SelectedIndex;
        }

        private void chkEditShowCriteria_CheckedChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
        }

        private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GridView gridView = (GridView)sender;
            int[] selectedRows = gridView.GetSelectedRows();
            if (selectedRows != null && selectedRows.Length != 0 && selectedRows.Length != 1)
            {
                for (int index = 0; index < selectedRows.Length; ++index)
                    gridView.GetDataRow(selectedRows[index])["ToBeUpdate"] = (object)true;
            }
        }

        private string GetSelectedDocKeys()
        {
            GridViewUtils.UpdateData(this.stockIssueGrid1.GridView);
            if (this.stockIssueGrid1.GridView.FocusedRowHandle >= 0 && this.myDataTable.Select("ToBeUpdate = True").Length > 0)
            {
                this.mySelectedDocKeylist = DataTableHelper.GetSelectedKeyArray("ToBeUpdate", "DocKey", this.myDataTable);
                return this.SelectedDocKeysInString;
            }
            else
                return "";
        }

        private void sbtnToggleOptions_Click(object sender, EventArgs e)
        {
            this.panelCriteria.Visible = !this.panelCriteria.Visible;
            if (this.panelCriteria.Visible)
                this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_HideOptions, new object[0]);
            else
                this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_ShowOptions, new object[0]);
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormMaterialRequestPrintSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)120)
            {
                if (this.sbtnInquiry.Enabled)
                    this.sbtnInquiry.PerformClick();
            }
            else if (e.KeyCode == (Keys)119)
            {
                if (this.previewButton1.Enabled)
                    this.previewButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)118)
            {
                if (this.printButton1.Enabled)
                    this.printButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)117 && this.sbtnToggleOptions.Enabled)
                this.sbtnToggleOptions.PerformClick();
        }

        private void previewButton1_Preview(object sender, PrintEventArgs e)
        {
            string selectedDocKeys = this.GetSelectedDocKeys();
            if (selectedDocKeys.Length == 0)
            {
                AppMessage.ShowMessage((Form)this, BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.ShowMessage_MaterialRequestNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            else if (this.cbEditReportType.SelectedIndex == 0)
            {
                if (BCE.AutoCount.Application.AccessRight.IsAccessible("PRPLUGINS_MR_RPT_PREVIEW", (XtraForm)this))
                    ReportTool.PreviewReport("Material Request Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, false, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum)MaterialRequestString.BatchPrintMaterialRequest, new object[0]), "PRPLUGINS_MR_RPT_PRINT", "PRPLUGINS_MR_RPT_EXPORT", this.myReportingCriteria.ReadableText)
                    {
                        UpdatePrintCountTableName = "MR"
                    });
            }
            else if (this.cbEditReportType.SelectedIndex == 1 && BCE.AutoCount.Application.AccessRight.IsAccessible("PRPLUGINS_MR_LIST_RPT_PREVIEW", (XtraForm)this))
            {
                ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum)MaterialRequestString.PrintMaterialRequestListing, new object[0]), "PRPLUGINS_MR_LIST_RPT_PRINT", "PRPLUGINS_MR_LIST_RPT_EXPORT", this.myReportingCriteria.ReadableText);
                ReportTool.PreviewReport("Material Request Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.MaterialRequestReportingCriteria), this.myDBSetting, e.DefaultReport, false, this.myCommand.ReportOption, reportInfo);
            }
        }

        private void printButton1_Print(object sender, PrintEventArgs e)
        {
            string selectedDocKeys = this.GetSelectedDocKeys();
            if (selectedDocKeys.Length == 0)
            {
                AppMessage.ShowMessage((Form)this, BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.ShowMessage_MaterialRequestNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            else if (this.cbEditReportType.SelectedIndex == 0)
            {
                if (BCE.AutoCount.Application.AccessRight.IsAccessible("PRPLUGINS_MR_RPT_PRINT", (XtraForm)this))
                    ReportTool.PrintReport("Material Request Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum)MaterialRequestString.BatchPrintMaterialRequest, new object[0]), "PRPLUGINS_MR_RPT_PRINT", "PRPLUGINS_MR_RPT_EXPORT", this.myReportingCriteria.ReadableText)
                    {
                        UpdatePrintCountTableName = "MR"
                    });
            }
            else if (this.cbEditReportType.SelectedIndex == 1 && BCE.AutoCount.Application.AccessRight.IsAccessible("PRPLUGINS_MR_LIST_RPT_PRINT", (XtraForm)this))
            {
                ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum)MaterialRequestString.PrintMaterialRequestListing, new object[0]), "PRPLUGINS_MR_LIST_RPT_PRINT", "PRPLUGINS_MR_LIST_RPT_EXPORT", this.myReportingCriteria.ReadableText);
                ReportTool.PrintReport("Material Request Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.MaterialRequestReportingCriteria), this.myDBSetting, e.DefaultReport, this.myCommand.ReportOption, reportInfo);
            }
        }

        private void barBtnDesignDocumentStyleReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportTool.DesignReport("Material Request Document", this.myDBSetting);
        }

        private void barBtnDesignListingStyleReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportTool.DesignReport("Material Request Listing", this.myDBSetting);
        }

        public virtual void RefreshDesignReport()
        {
            bool show = BCE.AutoCount.Application.AccessRight.IsAccessible("TOOLS_RPT_SHOW");
            this.barBtnDesignDocumentStyleReport.Visibility = XtraBarsUtils.ToBarItemVisibility(show);
            this.barBtnDesignListingStyleReport.Visibility = XtraBarsUtils.ToBarItemVisibility(show);
        }

        private void SortByOption()
        {
            ListingSortByOption listingSortByOption = (ListingSortByOption)this.cbEditSortBy.SelectedIndex;
            string[] strArray = new string[2]
      {
        "DocNo",
        "DocDate"
      };
            for (int index = 0; index < strArray.Length; ++index)
                this.stockIssueGrid1.GridView.Columns[strArray[index]].SortOrder = (ListingSortByOption)index != listingSortByOption ? ColumnSortOrder.None : ColumnSortOrder.Ascending;
        }

        private void FormSIPrintSearch_Load(object sender, EventArgs e)
        {
            this.FormInitialize();
        }

        private void FormInitialize()
        {
            FormMaterialRequestPrintListing.FormInitializeEventArgs initializeEventArgs = new FormMaterialRequestPrintListing.FormInitializeEventArgs(this.panelCriteria, this.panelControl1, this.tabControl1, this.stockIssueGrid1.GridControl, this, this.myCommand);
            this.myScriptObject.RunMethod("OnFormInitialize", new System.Type[1]
      {
        initializeEventArgs.GetType()
      }, new object[1]
      {
        (object) initializeEventArgs
      });
        }

        private void GridView_DoubleClick(object sender, EventArgs e)
        {
            if (this.myMouseDownHelper.IsLeftMouseDown && GridViewUtils.GetGridHitInfo((GridView)sender).InRow && BCE.AutoCount.Application.AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
                this.GoToDocument();
        }

        private void GoToDocument()
        {
            ColumnView columnView = (ColumnView)this.stockIssueGrid1.GridControl.FocusedView;
            DataRow dataRow = columnView.GetDataRow(columnView.FocusedRowHandle);
            if (dataRow != null)
                DocumentDispatcher.Open("MR", BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
        }

        public class FormEventArgs
        {
            private PanelControl myPanelCriteria;
            private PanelControl myPanelButtons;
            private XtraTabControl myTabControl;
            private GridControl myGridControl;
            private FormMaterialRequestPrintListing myForm;
            private MaterialRequestCommand myCommand;

            public MaterialRequestCommand Command
            {
                get
                {
                    return this.myCommand;
                }
            }

            public PanelControl PanelCriteria
            {
                get
                {
                    return this.myPanelCriteria;
                }
            }

            public PanelControl PanelButtons
            {
                get
                {
                    return this.myPanelButtons;
                }
            }

            public XtraTabControl TabControl
            {
                get
                {
                    return this.myTabControl;
                }
            }

            public GridControl GridControl
            {
                get
                {
                    return this.myGridControl;
                }
            }

            public FormMaterialRequestPrintListing Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public FormEventArgs(PanelControl pnlCriteria, PanelControl pnlButtons, XtraTabControl tabControl, GridControl gridControl, FormMaterialRequestPrintListing form, MaterialRequestCommand cmd)
            {
                this.myPanelCriteria = pnlCriteria;
                this.myPanelButtons = pnlButtons;
                this.myTabControl = tabControl;
                this.myGridControl = gridControl;
                this.myForm = form;
                this.myCommand = cmd;
            }
        }

        public class FormInitializeEventArgs : FormMaterialRequestPrintListing.FormEventArgs
        {
            public FormInitializeEventArgs(PanelControl pnlCriteria, PanelControl pnlButtons, XtraTabControl tabControl, GridControl gridControl, FormMaterialRequestPrintListing form, MaterialRequestCommand cmd)
                : base(pnlCriteria, pnlButtons, tabControl, gridControl, form, cmd)
            {
            }
        }

        public class FormInquiryEventArgs : FormMaterialRequestPrintListing.FormEventArgs
        {
            private DataTable myResultTable;

            public DataTable ResultTable
            {
                get
                {
                    return this.myResultTable;
                }
            }

            public FormInquiryEventArgs(PanelControl pnlCriteria, PanelControl pnlButtons, XtraTabControl tabControl, GridControl gridControl, FormMaterialRequestPrintListing form, DataTable resultTable, MaterialRequestCommand cmd)
                : base(pnlCriteria, pnlButtons, tabControl, gridControl, form, cmd)
            {
                this.myResultTable = resultTable;
            }
        }

        private void stockIssueGrid1_Load(object sender, EventArgs e)
        {

        }
    }
}
