﻿// Type: BCE.AutoCount.Stock.MaterialRequest.FormMaterialRequestPrintDetailListing
// Assembly: BCE.AutoCount.Stock, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Common;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PurchaseRequest
{
    public class FormMaterialRequestPrintDetailListing : XtraForm, IAppForm
    {
        private ScriptObject myScriptObject = ScriptManager.CreateObject("MaterialRequestDetailListing");
        private const string LISTING = "Print Material Request Detail Listing";
        public static string myColumnName;
        private DBSetting myDBSetting;
        private DataTable myDataTable;
        private DataTable myDetailDataTable;
        private DataSet myDataSet;
        private MaterialRequestReportCommand myCommand;
        private FormAdvancedSearch myFormAdvancedSearch;
        private AdvancedMaterialRequestCriteria myCriteria;
        private bool myInSearch;
        private MaterialRequestDetailReportingCriteria myReportingCriteria;
        private bool myLoadAllColumns;
        private BCE.AutoCount.Stock.StockIssue.FormStockIssueDetailListingAdvOptions myFormAdvOptions;
        private bool myFilterByLocation;
        private MouseDownHelper myMouseDownHelper;
        private XtraTabControl xtraTabControl1;
        private XtraTabPage xtraTabPage1;
        private XtraTabPage xtraTabPage2;
        private Label label4;
        private Label label6;
        private Label label3;
        private Label label1;
        private Label label2;
        private Label label5;
        private Label label7;
        private Label label8;
        private Label label9;
        private UCDateSelector ucDateSelector1;
        private UCItemSelector ucItemSelector1;
        private UCItemGroupSelector ucItemGroupSelector1;
        private UCItemTypeSelector ucItemTypeSelector1;
        private UCLocationSelector ucLocationSelector1;
        private UCProjectSelector ucProjectSelector1;
        private UCDepartmentSelector ucDepartmentSelector1;
        private GroupControl gbFilter;
        private GroupControl gbReport;
        private Label label10;
        private Label label11;
        private PanelControl pnCriteriaBasic;
        private GroupControl gbBasic;
        private Label label12;
        private Label label13;
        private Label label15;
        private UCDateSelector ucDateSelector2;
        private UCItemSelector ucItemSelector2;
        private GroupControl groupControl1;
        private Label label14;
        private Label label16;
        private PanelControl panelCenter;
        private PanelControl panelControl2;
        private SimpleButton sbtnClose;
        private SimpleButton sbtnToggleOptions;
        private SimpleButton sbtnInquiry;
        private XtraTabControl xtraTabControl2;
        private XtraTabPage xtraTabPage3;
        private XtraTabPage xtraTabPage4;
        private GridControl gridControl1;
        private ComboBoxEdit cbCancelledOption;
        private MemoEdit memoEdit_Criteria;
        private ComboBoxEdit cbEditSortBy;
        private ComboBoxEdit cbEditGroupBy;
        private ComboBoxEdit cbEditSortBy2;
        private ComboBoxEdit cbEditGroupBy2;
        private UCSearchResult ucSearchResult1;
        private CheckEdit chkEditShowCriteria;
        private CheckEdit chkEditShowCriteria2;
        private GridView gvMaster;
        private PanelControl panelCriteria;
        private BarManager barManager1;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private Bar bar1;
        private BarSubItem barSubItem1;
        private BarButtonItem barbtnAdvancedFilter;
        private GridColumn colCheck;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private GridColumn colDocNo;
        private GridColumn colDocDate;
        private GridColumn colItemCode;
        private GridColumn colDescription;
        private GridColumn colTotal;
        private GridColumn colCancelled;
        private GridColumn colLocation;
        private GridColumn colBatchNo;
        private GridColumn colDtlDescription;
        private GridColumn colFurtherDescription;
        private GridColumn colProjNo;
        private GridColumn colDeptNo;
        private GridColumn colQty;
        private GridColumn colItemGroup;
        private GridColumn colItemType;
        private SimpleButton sbtnAdvOptions;
        private SimpleButton sbtnAdvanceSearch;
        private GridColumn colRemark1;
        private GridColumn colRemark2;
        private GridColumn colRemark3;
        private GridColumn colRemark4;
        private GridColumn colRefDocNo;
        private GridColumn colPrintCount;
        private GridColumn colLastModified;
        private GridColumn colCreatedTimeStamp;
        private GridColumn colCreatedUserID;
        private RepositoryItemTextEdit repositoryItemTextEdit_Cancelled;
        private GridColumn colLastModifiedUserID;
        private UCStockIssueSelector ucMaterialRequestSelector1;
        private UCStockIssueSelector ucMaterialRequestSelector2;
        private GridColumn colUnitCost;
        private GridColumn colSubTotal;
        private GridColumn colUOM;
        private PreviewButton previewButton1;
        private PrintButton printButton1;
        private BarButtonItem barBtnDesignDetailListingReport;
        private SimpleButton sbtnAdvancedSearch2;
        private PanelHeader panelHeader1;
        private RepositoryItemButtonEdit repositoryItemFurtherDescription;
        private IContainer components;

        public DataTable MasterDataTable
        {
            get
            {
                return this.myDataTable;
            }
        }

        public DataTable DetailDataTable
        {
            get
            {
                return this.myDetailDataTable;
            }
        }

        public string SelectedDocNosInString
        {
            get
            {
                return this.GenerateDocNosToString();
            }
        }

        public string SelectedDocKeysInString
        {
            get
            {
                return StringHelper.ArrayListToCommaString(new ArrayList((ICollection)CommonFunction.GetDocKeyList("ToBeUpdate", this.myDataTable)));
            }
        }

        public string SelectedDtlKeysInString
        {
            get
            {
                return StringHelper.ArrayListToCommaString(new ArrayList((ICollection)CommonFunction.GetDtlKeyList("ToBeUpdate", this.myDataTable)));
            }
        }

        public string ColumnName
        {
            get
            {
                return FormMaterialRequestPrintDetailListing.myColumnName;
            }
        }

        public MaterialRequestDetailReportingCriteria MaterialRequestReportingCriteria
        {
            get
            {
                return this.myReportingCriteria;
            }
        }

        public FormMaterialRequestPrintDetailListing(DBSetting dbSetting)
        {
            this.InitializeComponent();
            this.myCommand = MaterialRequestReportCommand.Create(dbSetting, new BasicReportOption());
            this.myDBSetting = this.myCommand.DBSetting;
            this.previewButton1.ReportType = "Material Request Detail Listing";
            this.printButton1.ReportType = "Material Request Detail Listing";
            this.myDataTable = new DataTable("Master");
            this.myDetailDataTable = new DataTable("Detail");
            this.myDataSet = new DataSet();
            this.myDataSet.Tables.Add(this.myDataTable);
            this.myDataSet.Tables.Add(this.myDetailDataTable);
            this.gridControl1.DataSource = (object)this.myDataTable;
            this.LoadCriteria();
            this.cbCancelledOption.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(CancelledDocumentOption)));
            this.cbEditGroupBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(DetailListingGroupByOption)));
            this.cbEditGroupBy2.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(DetailListingGroupByOption)));
            this.cbEditSortBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(DetailListingSortByOption)));
            this.cbEditSortBy2.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(DetailListingSortByOption)));
            this.cbEditGroupBy.SelectedIndex = 0;
            this.cbEditGroupBy2.SelectedIndex = 0;
            this.cbEditSortBy.SelectedIndex = 0;
            this.cbEditSortBy2.SelectedIndex = 0;
            this.ucSearchResult1.Initialize(this.gvMaster, "ToBeUpdate");
            new UDFUtil(this.myDBSetting).SetupDetailListingReportGrid(this.gvMaster, "MR", "MRDTL");
            CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gvMaster, new EventHandler(this.ReloadAllColumns));
            this.InitUserControls();
            this.InitFormControls();
            this.InitializeSettings();
            this.RefreshDesignReport();
            this.myMouseDownHelper = new MouseDownHelper();
            this.myMouseDownHelper.Init(this.gvMaster);
            BCE.AutoCount.Application.AccessRight.AddListener(new AccessRightListenerDelegate(this.RefreshDesignReport), (Form)this);
            BCE.AutoCount.Application.SetHelpTopic(this.panelHeader1, "Stock_Issue.htm#stk028");
            Activity.Log(dbSetting, "", 0L, 0L, BCE.Localization.Localizer.GetString((Enum)MaterialRequestString.OpenedPrintMaterialRequestDetailListingWindow, new object[1]
      {
        (object) BCE.AutoCount.Application.DBSession.LoginUserID
      }), "");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitFormControls()
        {
            FormControlUtil formControlUtil = new FormControlUtil();
            formControlUtil.AddField("Total", "Currency");
            formControlUtil.AddField("Qty", "Quantity");
            formControlUtil.AddField("UnitCost", "Currency");
            formControlUtil.AddField("SubTotal", "Currency");
            formControlUtil.AddField("DocDate", "Date");
            formControlUtil.AddField("LastModified", "DateTime");
            formControlUtil.AddField("CreatedTimeStamp", "DateTime");
            formControlUtil.InitControls((Control)this);
            this.gvMaster.GroupSummary.Add(SummaryItemType.Count, "ItemCode", (GridColumn)null, "Item Code Count = {0}");
            this.gvMaster.GroupSummary.Add(SummaryItemType.Sum, "Total", (GridColumn)null, "Total = " + BCE.AutoCount.Application.GetCurrencyFormatString(0));
            this.gvMaster.GroupSummary.Add(SummaryItemType.Sum, "Qty", (GridColumn)null, "Quantity = " + BCE.AutoCount.Application.GetQuantityFormatString(0));
            this.gvMaster.GroupSummary.Add(SummaryItemType.Sum, "UnitCost", (GridColumn)null, "Unit Cost = " + BCE.AutoCount.Application.GetCostFormatString(0));
            this.gvMaster.GroupSummary.Add(SummaryItemType.Sum, "Subtotal", (GridColumn)null, "Sub Total = " + BCE.AutoCount.Application.GetCurrencyFormatString(0));
        }

        private void SaveCriteria()
        {
            BCE.AutoCount.Application.SaveCriteriaData((BCE.AutoCount.CustomSetting)this.myReportingCriteria, "MaterialRequestDocumentDetailListingReport.setting");
        }

        private void LoadCriteria()
        {
           // this.myReportingCriteria = (MaterialRequestDetailReportingCriteria)BCE.AutoCount.Application.LoadCriteriaData("MaterialRequestDocumentDetailListingReport.setting");
            if (this.myReportingCriteria == null)
                this.myReportingCriteria = new MaterialRequestDetailReportingCriteria();
            this.cbEditGroupBy.EditValue = (object)this.myReportingCriteria.GroupBy;
            this.cbEditSortBy.EditValue = (object)this.myReportingCriteria.SortBy;
            this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
            this.chkEditShowCriteria.Checked = this.myReportingCriteria.IsShowCriteria;
            if (this.myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.All)
                this.cbCancelledOption.SelectedIndex = 0;
            else if (this.myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
                this.cbCancelledOption.SelectedIndex = 1;
            else
                this.cbCancelledOption.SelectedIndex = 2;
        }

        private void InitializeComponent()
        {
            this.panelCriteria = new DevExpress.XtraEditors.PanelControl();
            this.gbFilter = new DevExpress.XtraEditors.GroupControl();
            this.sbtnAdvanceSearch = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.ucMaterialRequestSelector1 = new BCE.AutoCount.FilterUI.UCStockIssueSelector();
            this.cbCancelledOption = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucDateSelector1 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.ucDepartmentSelector1 = new BCE.AutoCount.FilterUI.UCDepartmentSelector();
            this.ucProjectSelector1 = new BCE.AutoCount.FilterUI.UCProjectSelector();
            this.ucLocationSelector1 = new BCE.AutoCount.FilterUI.UCLocationSelector();
            this.ucItemTypeSelector1 = new BCE.AutoCount.FilterUI.UCItemTypeSelector();
            this.ucItemGroupSelector1 = new BCE.AutoCount.FilterUI.UCItemGroupSelector();
            this.ucItemSelector1 = new BCE.AutoCount.FilterUI.UCItemSelector();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.gbReport = new DevExpress.XtraEditors.GroupControl();
            this.chkEditShowCriteria = new DevExpress.XtraEditors.CheckEdit();
            this.cbEditSortBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbEditGroupBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnCriteriaBasic = new DevExpress.XtraEditors.PanelControl();
            this.sbtnAdvancedSearch2 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAdvOptions = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.chkEditShowCriteria2 = new DevExpress.XtraEditors.CheckEdit();
            this.cbEditSortBy2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbEditGroupBy2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.gbBasic = new DevExpress.XtraEditors.GroupControl();
            this.ucMaterialRequestSelector2 = new BCE.AutoCount.FilterUI.UCStockIssueSelector();
            this.ucItemSelector2 = new BCE.AutoCount.FilterUI.UCItemSelector();
            this.ucDateSelector2 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panelCenter = new DevExpress.XtraEditors.PanelControl();
            this.printButton1 = new BCE.AutoCount.Controls.PrintButton();
            this.previewButton1 = new BCE.AutoCount.Controls.PreviewButton();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnToggleOptions = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gvMaster = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit_Cancelled = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBatchNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDtlDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFurtherDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemFurtherDescription = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colProjNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeptNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ucSearchResult1 = new BCE.AutoCount.FilterUI.UCSearchResult();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit_Criteria = new DevExpress.XtraEditors.MemoEdit();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnDesignDetailListingReport = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnAdvancedFilter = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).BeginInit();
            this.panelCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbFilter)).BeginInit();
            this.gbFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbReport)).BeginInit();
            this.gbReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaBasic)).BeginInit();
            this.pnCriteriaBasic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbBasic)).BeginInit();
            this.gbBasic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelCenter)).BeginInit();
            this.panelCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit_Cancelled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFurtherDescription)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCriteria
            // 
            this.panelCriteria.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCriteria.Controls.Add(this.gbFilter);
            this.panelCriteria.Controls.Add(this.gbReport);
            this.panelCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCriteria.Location = new System.Drawing.Point(0, 44);
            this.panelCriteria.Name = "panelCriteria";
            this.panelCriteria.Size = new System.Drawing.Size(986, 216);
            this.panelCriteria.TabIndex = 3;
            this.panelCriteria.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCriteria_Paint);
            // 
            // gbFilter
            // 
            this.gbFilter.Controls.Add(this.sbtnAdvanceSearch);
            this.gbFilter.Controls.Add(this.xtraTabControl1);
            this.gbFilter.Location = new System.Drawing.Point(12, 6);
            this.gbFilter.Name = "gbFilter";
            this.gbFilter.Size = new System.Drawing.Size(504, 206);
            this.gbFilter.TabIndex = 0;
            this.gbFilter.Text = "Filter Options";
            // 
            // sbtnAdvanceSearch
            // 
            this.sbtnAdvanceSearch.Location = new System.Drawing.Point(380, 20);
            this.sbtnAdvanceSearch.Name = "sbtnAdvanceSearch";
            this.sbtnAdvanceSearch.Size = new System.Drawing.Size(106, 25);
            this.sbtnAdvanceSearch.TabIndex = 0;
            this.sbtnAdvanceSearch.Text = "Advanced Filter";
            this.sbtnAdvanceSearch.Click += new System.EventHandler(this.sbtnAdvanceSearch_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(5, 26);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(494, 174);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.ucMaterialRequestSelector1);
            this.xtraTabPage1.Controls.Add(this.cbCancelledOption);
            this.xtraTabPage1.Controls.Add(this.ucDateSelector1);
            this.xtraTabPage1.Controls.Add(this.label2);
            this.xtraTabPage1.Controls.Add(this.label1);
            this.xtraTabPage1.Controls.Add(this.label3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(485, 143);
            this.xtraTabPage1.Text = "Master Level";
            // 
            // ucMaterialRequestSelector1
            // 
            this.ucMaterialRequestSelector1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaterialRequestSelector1.Appearance.Options.UseBackColor = true;
            this.ucMaterialRequestSelector1.Location = new System.Drawing.Point(106, 27);
            this.ucMaterialRequestSelector1.Name = "ucMaterialRequestSelector1";
            this.ucMaterialRequestSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucMaterialRequestSelector1.TabIndex = 0;
            // 
            // cbCancelledOption
            // 
            this.cbCancelledOption.Location = new System.Drawing.Point(106, 51);
            this.cbCancelledOption.Name = "cbCancelledOption";
            this.cbCancelledOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCancelledOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbCancelledOption.Size = new System.Drawing.Size(127, 20);
            this.cbCancelledOption.TabIndex = 1;
            this.cbCancelledOption.SelectedIndexChanged += new System.EventHandler(this.cbCancelledOption_SelectedIndexChanged);
            // 
            // ucDateSelector1
            // 
            this.ucDateSelector1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDateSelector1.Appearance.Options.UseBackColor = true;
            this.ucDateSelector1.Location = new System.Drawing.Point(106, 3);
            this.ucDateSelector1.Name = "ucDateSelector1";
            this.ucDateSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucDateSelector1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(4, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cancelled Status";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Document Date";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(4, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Document No";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.ucDepartmentSelector1);
            this.xtraTabPage2.Controls.Add(this.ucProjectSelector1);
            this.xtraTabPage2.Controls.Add(this.ucLocationSelector1);
            this.xtraTabPage2.Controls.Add(this.ucItemTypeSelector1);
            this.xtraTabPage2.Controls.Add(this.ucItemGroupSelector1);
            this.xtraTabPage2.Controls.Add(this.ucItemSelector1);
            this.xtraTabPage2.Controls.Add(this.label7);
            this.xtraTabPage2.Controls.Add(this.label5);
            this.xtraTabPage2.Controls.Add(this.label6);
            this.xtraTabPage2.Controls.Add(this.label4);
            this.xtraTabPage2.Controls.Add(this.label8);
            this.xtraTabPage2.Controls.Add(this.label9);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(485, 143);
            this.xtraTabPage2.Text = "Detail Level";
            // 
            // ucDepartmentSelector1
            // 
            this.ucDepartmentSelector1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDepartmentSelector1.Appearance.Options.UseBackColor = true;
            this.ucDepartmentSelector1.Location = new System.Drawing.Point(100, 116);
            this.ucDepartmentSelector1.Name = "ucDepartmentSelector1";
            this.ucDepartmentSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucDepartmentSelector1.TabIndex = 0;
            // 
            // ucProjectSelector1
            // 
            this.ucProjectSelector1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucProjectSelector1.Appearance.Options.UseBackColor = true;
            this.ucProjectSelector1.Location = new System.Drawing.Point(100, 93);
            this.ucProjectSelector1.Name = "ucProjectSelector1";
            this.ucProjectSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucProjectSelector1.TabIndex = 1;
            // 
            // ucLocationSelector1
            // 
            this.ucLocationSelector1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucLocationSelector1.Appearance.Options.UseBackColor = true;
            this.ucLocationSelector1.Location = new System.Drawing.Point(100, 70);
            this.ucLocationSelector1.LocationType = BCE.AutoCount.FilterUI.UCLocationType.ItemLocation;
            this.ucLocationSelector1.Name = "ucLocationSelector1";
            this.ucLocationSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucLocationSelector1.TabIndex = 2;
            // 
            // ucItemTypeSelector1
            // 
            this.ucItemTypeSelector1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucItemTypeSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemTypeSelector1.Location = new System.Drawing.Point(100, 48);
            this.ucItemTypeSelector1.Name = "ucItemTypeSelector1";
            this.ucItemTypeSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucItemTypeSelector1.TabIndex = 3;
            // 
            // ucItemGroupSelector1
            // 
            this.ucItemGroupSelector1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucItemGroupSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemGroupSelector1.Location = new System.Drawing.Point(100, 26);
            this.ucItemGroupSelector1.Name = "ucItemGroupSelector1";
            this.ucItemGroupSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucItemGroupSelector1.TabIndex = 4;
            // 
            // ucItemSelector1
            // 
            this.ucItemSelector1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucItemSelector1.Appearance.Options.UseBackColor = true;
            this.ucItemSelector1.Location = new System.Drawing.Point(100, 3);
            this.ucItemSelector1.Name = "ucItemSelector1";
            this.ucItemSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucItemSelector1.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(6, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "Department";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(6, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 18);
            this.label5.TabIndex = 7;
            this.label5.Text = "Proj No";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(6, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 17);
            this.label6.TabIndex = 8;
            this.label6.Text = "Location";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 18);
            this.label4.TabIndex = 9;
            this.label4.Text = "Item Type";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(6, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 17);
            this.label8.TabIndex = 10;
            this.label8.Text = "Item Group";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(6, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 19);
            this.label9.TabIndex = 11;
            this.label9.Text = "Item";
            // 
            // gbReport
            // 
            this.gbReport.Controls.Add(this.chkEditShowCriteria);
            this.gbReport.Controls.Add(this.cbEditSortBy);
            this.gbReport.Controls.Add(this.cbEditGroupBy);
            this.gbReport.Controls.Add(this.label11);
            this.gbReport.Controls.Add(this.label10);
            this.gbReport.Location = new System.Drawing.Point(522, 6);
            this.gbReport.Name = "gbReport";
            this.gbReport.Size = new System.Drawing.Size(223, 106);
            this.gbReport.TabIndex = 1;
            this.gbReport.Text = "Report Options";
            this.gbReport.Paint += new System.Windows.Forms.PaintEventHandler(this.gbReport_Paint);
            // 
            // chkEditShowCriteria
            // 
            this.chkEditShowCriteria.Location = new System.Drawing.Point(129, 80);
            this.chkEditShowCriteria.Name = "chkEditShowCriteria";
            this.chkEditShowCriteria.Properties.Caption = "ShowCriteria";
            this.chkEditShowCriteria.Size = new System.Drawing.Size(99, 19);
            this.chkEditShowCriteria.TabIndex = 0;
            this.chkEditShowCriteria.CheckedChanged += new System.EventHandler(this.chkEditShowCriteria_CheckedChanged);
            // 
            // cbEditSortBy
            // 
            this.cbEditSortBy.Location = new System.Drawing.Point(83, 48);
            this.cbEditSortBy.Name = "cbEditSortBy";
            this.cbEditSortBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditSortBy.Properties.DropDownRows = 10;
            this.cbEditSortBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditSortBy.Size = new System.Drawing.Size(130, 20);
            this.cbEditSortBy.TabIndex = 1;
            this.cbEditSortBy.SelectedIndexChanged += new System.EventHandler(this.cbEditSortBy_SelectedIndexChanged);
            // 
            // cbEditGroupBy
            // 
            this.cbEditGroupBy.Location = new System.Drawing.Point(83, 25);
            this.cbEditGroupBy.Name = "cbEditGroupBy";
            this.cbEditGroupBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditGroupBy.Properties.DropDownRows = 10;
            this.cbEditGroupBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditGroupBy.Size = new System.Drawing.Size(130, 20);
            this.cbEditGroupBy.TabIndex = 2;
            this.cbEditGroupBy.SelectedIndexChanged += new System.EventHandler(this.cbEditGroupBy_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(5, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 18);
            this.label11.TabIndex = 3;
            this.label11.Text = "Sort By";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(5, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "Group By";
            // 
            // pnCriteriaBasic
            // 
            this.pnCriteriaBasic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnCriteriaBasic.Controls.Add(this.sbtnAdvancedSearch2);
            this.pnCriteriaBasic.Controls.Add(this.sbtnAdvOptions);
            this.pnCriteriaBasic.Controls.Add(this.groupControl1);
            this.pnCriteriaBasic.Controls.Add(this.gbBasic);
            this.pnCriteriaBasic.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnCriteriaBasic.Location = new System.Drawing.Point(0, 260);
            this.pnCriteriaBasic.Name = "pnCriteriaBasic";
            this.pnCriteriaBasic.Size = new System.Drawing.Size(986, 142);
            this.pnCriteriaBasic.TabIndex = 2;
            // 
            // sbtnAdvancedSearch2
            // 
            this.sbtnAdvancedSearch2.Location = new System.Drawing.Point(630, 112);
            this.sbtnAdvancedSearch2.Name = "sbtnAdvancedSearch2";
            this.sbtnAdvancedSearch2.Size = new System.Drawing.Size(103, 23);
            this.sbtnAdvancedSearch2.TabIndex = 0;
            this.sbtnAdvancedSearch2.Text = "Advanced Search";
            this.sbtnAdvancedSearch2.Click += new System.EventHandler(this.sbtnAdvanceSearch_Click);
            // 
            // sbtnAdvOptions
            // 
            this.sbtnAdvOptions.Location = new System.Drawing.Point(522, 112);
            this.sbtnAdvOptions.Name = "sbtnAdvOptions";
            this.sbtnAdvOptions.Size = new System.Drawing.Size(102, 23);
            this.sbtnAdvOptions.TabIndex = 1;
            this.sbtnAdvOptions.Text = "Adv. Options";
            this.sbtnAdvOptions.Click += new System.EventHandler(this.sbtnAdvOptions_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.chkEditShowCriteria2);
            this.groupControl1.Controls.Add(this.cbEditSortBy2);
            this.groupControl1.Controls.Add(this.cbEditGroupBy2);
            this.groupControl1.Controls.Add(this.label14);
            this.groupControl1.Controls.Add(this.label16);
            this.groupControl1.Location = new System.Drawing.Point(522, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(211, 100);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Report Options";
            // 
            // chkEditShowCriteria2
            // 
            this.chkEditShowCriteria2.Location = new System.Drawing.Point(97, 76);
            this.chkEditShowCriteria2.Name = "chkEditShowCriteria2";
            this.chkEditShowCriteria2.Properties.Caption = "Show Criteria";
            this.chkEditShowCriteria2.Size = new System.Drawing.Size(98, 19);
            this.chkEditShowCriteria2.TabIndex = 0;
            // 
            // cbEditSortBy2
            // 
            this.cbEditSortBy2.Location = new System.Drawing.Point(82, 51);
            this.cbEditSortBy2.Name = "cbEditSortBy2";
            this.cbEditSortBy2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditSortBy2.Properties.DropDownRows = 10;
            this.cbEditSortBy2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditSortBy2.Size = new System.Drawing.Size(120, 20);
            this.cbEditSortBy2.TabIndex = 1;
            this.cbEditSortBy2.SelectedIndexChanged += new System.EventHandler(this.cbEditSortBy2_SelectedIndexChanged);
            // 
            // cbEditGroupBy2
            // 
            this.cbEditGroupBy2.Location = new System.Drawing.Point(82, 24);
            this.cbEditGroupBy2.Name = "cbEditGroupBy2";
            this.cbEditGroupBy2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditGroupBy2.Properties.DropDownRows = 10;
            this.cbEditGroupBy2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditGroupBy2.Size = new System.Drawing.Size(120, 20);
            this.cbEditGroupBy2.TabIndex = 2;
            this.cbEditGroupBy2.SelectedIndexChanged += new System.EventHandler(this.cbEditGroupBy2_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(8, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 17);
            this.label14.TabIndex = 3;
            this.label14.Text = "Sort By";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(8, 27);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 23);
            this.label16.TabIndex = 4;
            this.label16.Text = "Group By";
            // 
            // gbBasic
            // 
            this.gbBasic.Controls.Add(this.ucMaterialRequestSelector2);
            this.gbBasic.Controls.Add(this.ucItemSelector2);
            this.gbBasic.Controls.Add(this.ucDateSelector2);
            this.gbBasic.Controls.Add(this.label15);
            this.gbBasic.Controls.Add(this.label12);
            this.gbBasic.Controls.Add(this.label13);
            this.gbBasic.Location = new System.Drawing.Point(12, 6);
            this.gbBasic.Name = "gbBasic";
            this.gbBasic.Size = new System.Drawing.Size(504, 100);
            this.gbBasic.TabIndex = 3;
            this.gbBasic.Text = "Filter Options";
            // 
            // ucMaterialRequestSelector2
            // 
            this.ucMaterialRequestSelector2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaterialRequestSelector2.Appearance.Options.UseBackColor = true;
            this.ucMaterialRequestSelector2.Location = new System.Drawing.Point(100, 48);
            this.ucMaterialRequestSelector2.Name = "ucMaterialRequestSelector2";
            this.ucMaterialRequestSelector2.Size = new System.Drawing.Size(362, 20);
            this.ucMaterialRequestSelector2.TabIndex = 0;
            // 
            // ucItemSelector2
            // 
            this.ucItemSelector2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucItemSelector2.Appearance.Options.UseBackColor = true;
            this.ucItemSelector2.Location = new System.Drawing.Point(100, 73);
            this.ucItemSelector2.Name = "ucItemSelector2";
            this.ucItemSelector2.Size = new System.Drawing.Size(362, 20);
            this.ucItemSelector2.TabIndex = 1;
            // 
            // ucDateSelector2
            // 
            this.ucDateSelector2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDateSelector2.Appearance.Options.UseBackColor = true;
            this.ucDateSelector2.Location = new System.Drawing.Point(100, 23);
            this.ucDateSelector2.Name = "ucDateSelector2";
            this.ucDateSelector2.Size = new System.Drawing.Size(362, 20);
            this.ucDateSelector2.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(5, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(93, 18);
            this.label15.TabIndex = 3;
            this.label15.Text = "Document No";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(5, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 17);
            this.label12.TabIndex = 4;
            this.label12.Text = "Document Date";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(5, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 17);
            this.label13.TabIndex = 5;
            this.label13.Text = "Item Code";
            // 
            // panelCenter
            // 
            this.panelCenter.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCenter.Controls.Add(this.printButton1);
            this.panelCenter.Controls.Add(this.previewButton1);
            this.panelCenter.Controls.Add(this.sbtnClose);
            this.panelCenter.Controls.Add(this.sbtnToggleOptions);
            this.panelCenter.Controls.Add(this.sbtnInquiry);
            this.panelCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCenter.Location = new System.Drawing.Point(0, 402);
            this.panelCenter.Name = "panelCenter";
            this.panelCenter.Size = new System.Drawing.Size(986, 37);
            this.panelCenter.TabIndex = 1;
            // 
            // printButton1
            // 
            this.printButton1.Location = new System.Drawing.Point(159, 6);
            this.printButton1.Name = "printButton1";
            this.printButton1.ReportType = "";
            this.printButton1.Size = new System.Drawing.Size(72, 23);
            this.printButton1.TabIndex = 0;
            this.printButton1.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // previewButton1
            // 
            this.previewButton1.Location = new System.Drawing.Point(87, 6);
            this.previewButton1.Name = "previewButton1";
            this.previewButton1.ReportType = "";
            this.previewButton1.Size = new System.Drawing.Size(72, 23);
            this.previewButton1.TabIndex = 1;
            this.previewButton1.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // sbtnClose
            // 
            this.sbtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnClose.Location = new System.Drawing.Point(231, 6);
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Size = new System.Drawing.Size(75, 23);
            this.sbtnClose.TabIndex = 2;
            this.sbtnClose.Text = "Close";
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // sbtnToggleOptions
            // 
            this.sbtnToggleOptions.Location = new System.Drawing.Point(306, 6);
            this.sbtnToggleOptions.Name = "sbtnToggleOptions";
            this.sbtnToggleOptions.Size = new System.Drawing.Size(92, 23);
            this.sbtnToggleOptions.TabIndex = 3;
            this.sbtnToggleOptions.Text = "Toggle Options";
            this.sbtnToggleOptions.Click += new System.EventHandler(this.sbtnToggleOptions_Click);
            // 
            // sbtnInquiry
            // 
            this.sbtnInquiry.Location = new System.Drawing.Point(12, 6);
            this.sbtnInquiry.Name = "sbtnInquiry";
            this.sbtnInquiry.Size = new System.Drawing.Size(75, 23);
            this.sbtnInquiry.TabIndex = 4;
            this.sbtnInquiry.Text = "Inquiry";
            this.sbtnInquiry.Click += new System.EventHandler(this.sbtnInquiry_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.xtraTabControl2);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 439);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(986, 177);
            this.panelControl2.TabIndex = 0;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
            this.xtraTabControl2.Size = new System.Drawing.Size(986, 177);
            this.xtraTabControl2.TabIndex = 0;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl1);
            this.xtraTabPage3.Controls.Add(this.ucSearchResult1);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(977, 146);
            this.xtraTabPage3.Text = "Result";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.Name = "";
            this.gridControl1.Location = new System.Drawing.Point(0, 46);
            this.gridControl1.MainView = this.gvMaster;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit_Cancelled,
            this.repositoryItemFurtherDescription});
            this.gridControl1.Size = new System.Drawing.Size(977, 100);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMaster});
            this.gridControl1.Click += new System.EventHandler(this.gridControl1_Click);
            // 
            // gvMaster
            // 
            this.gvMaster.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCheck,
            this.colDocNo,
            this.colDocDate,
            this.colDescription,
            this.colTotal,
            this.colCancelled,
            this.colItemCode,
            this.colLocation,
            this.colBatchNo,
            this.colDtlDescription,
            this.colFurtherDescription,
            this.colProjNo,
            this.colDeptNo,
            this.colUOM,
            this.colQty,
            this.colUnitCost,
            this.colSubTotal,
            this.colItemGroup,
            this.colItemType,
            this.colRemark1,
            this.colRemark2,
            this.colRemark3,
            this.colRemark4,
            this.colRefDocNo,
            this.colPrintCount,
            this.colLastModified,
            this.colLastModifiedUserID,
            this.colCreatedTimeStamp,
            this.colCreatedUserID});
            this.gvMaster.CustomizationFormBounds = new System.Drawing.Rectangle(635, 224, 216, 178);
            this.gvMaster.GridControl = this.gridControl1;
            this.gvMaster.Name = "gvMaster";
            this.gvMaster.OptionsSelection.MultiSelect = true;
            this.gvMaster.OptionsView.ColumnAutoWidth = false;
            this.gvMaster.OptionsView.ShowFooter = true;
            this.gvMaster.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gvMaster_SelectionChanged);
            this.gvMaster.DoubleClick += new System.EventHandler(this.gvMaster_DoubleClick);
            // 
            // colCheck
            // 
            this.colCheck.Caption = "Check";
            this.colCheck.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCheck.FieldName = "ToBeUpdate";
            this.colCheck.Name = "colCheck";
            this.colCheck.Visible = true;
            this.colCheck.VisibleIndex = 0;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colDocNo
            // 
            this.colDocNo.Caption = "DocNo";
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.OptionsColumn.AllowEdit = false;
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 1;
            // 
            // colDocDate
            // 
            this.colDocDate.Caption = "DocDate";
            this.colDocDate.FieldName = "DocDate";
            this.colDocDate.Name = "colDocDate";
            this.colDocDate.OptionsColumn.AllowEdit = false;
            this.colDocDate.Visible = true;
            this.colDocDate.VisibleIndex = 2;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 3;
            // 
            // colTotal
            // 
            this.colTotal.Caption = "Total";
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.OptionsColumn.AllowEdit = false;
            this.colTotal.Visible = true;
            this.colTotal.VisibleIndex = 15;
            // 
            // colCancelled
            // 
            this.colCancelled.Caption = "Cancelled";
            this.colCancelled.ColumnEdit = this.repositoryItemTextEdit_Cancelled;
            this.colCancelled.FieldName = "Cancelled";
            this.colCancelled.Name = "colCancelled";
            this.colCancelled.OptionsColumn.AllowEdit = false;
            this.colCancelled.Visible = true;
            this.colCancelled.VisibleIndex = 4;
            // 
            // repositoryItemTextEdit_Cancelled
            // 
            this.repositoryItemTextEdit_Cancelled.Name = "repositoryItemTextEdit_Cancelled";
            this.repositoryItemTextEdit_Cancelled.FormatEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repositoryItemTextEdit_Cancelled_FormatEditValue);
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "ItemCode";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 5;
            // 
            // colLocation
            // 
            this.colLocation.Caption = "Location";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 10;
            // 
            // colBatchNo
            // 
            this.colBatchNo.Caption = "BachNo";
            this.colBatchNo.FieldName = "BatchNo";
            this.colBatchNo.Name = "colBatchNo";
            this.colBatchNo.OptionsColumn.AllowEdit = false;
            // 
            // colDtlDescription
            // 
            this.colDtlDescription.Caption = "Detail Description";
            this.colDtlDescription.FieldName = "DtlDescription";
            this.colDtlDescription.Name = "colDtlDescription";
            this.colDtlDescription.OptionsColumn.AllowEdit = false;
            this.colDtlDescription.Visible = true;
            this.colDtlDescription.VisibleIndex = 7;
            // 
            // colFurtherDescription
            // 
            this.colFurtherDescription.Caption = "FurtherDescription";
            this.colFurtherDescription.ColumnEdit = this.repositoryItemFurtherDescription;
            this.colFurtherDescription.FieldName = "FurtherDescription";
            this.colFurtherDescription.Name = "colFurtherDescription";
            this.colFurtherDescription.OptionsColumn.ReadOnly = true;
            this.colFurtherDescription.Visible = true;
            this.colFurtherDescription.VisibleIndex = 8;
            // 
            // repositoryItemFurtherDescription
            // 
            this.repositoryItemFurtherDescription.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "BLOB", -1, true, true, false, DevExpress.Utils.HorzAlignment.Center, null)});
            this.repositoryItemFurtherDescription.DisplayFormat.FormatString = "BLOB";
            this.repositoryItemFurtherDescription.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemFurtherDescription.Name = "repositoryItemFurtherDescription";
            this.repositoryItemFurtherDescription.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemFurtherDescription.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemFurtherDescription_ButtonClick);
            this.repositoryItemFurtherDescription.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemFurtherDescription_ButtonClick);
            // 
            // colProjNo
            // 
            this.colProjNo.Caption = "ProjNo";
            this.colProjNo.FieldName = "ProjNo";
            this.colProjNo.Name = "colProjNo";
            this.colProjNo.OptionsColumn.AllowEdit = false;
            this.colProjNo.Visible = true;
            this.colProjNo.VisibleIndex = 9;
            // 
            // colDeptNo
            // 
            this.colDeptNo.Caption = "DeptNo";
            this.colDeptNo.FieldName = "DeptNo";
            this.colDeptNo.Name = "colDeptNo";
            this.colDeptNo.OptionsColumn.AllowEdit = false;
            // 
            // colUOM
            // 
            this.colUOM.Caption = "UOM";
            this.colUOM.FieldName = "UOM";
            this.colUOM.Name = "colUOM";
            this.colUOM.OptionsColumn.AllowEdit = false;
            this.colUOM.Visible = true;
            this.colUOM.VisibleIndex = 11;
            // 
            // colQty
            // 
            this.colQty.Caption = "Qty";
            this.colQty.FieldName = "Qty";
            this.colQty.Name = "colQty";
            this.colQty.OptionsColumn.AllowEdit = false;
            this.colQty.Visible = true;
            this.colQty.VisibleIndex = 12;
            // 
            // colUnitCost
            // 
            this.colUnitCost.Caption = "UnitCost";
            this.colUnitCost.FieldName = "UnitCost";
            this.colUnitCost.Name = "colUnitCost";
            this.colUnitCost.OptionsColumn.AllowEdit = false;
            this.colUnitCost.Visible = true;
            this.colUnitCost.VisibleIndex = 13;
            // 
            // colSubTotal
            // 
            this.colSubTotal.Caption = "Subtotal";
            this.colSubTotal.FieldName = "Subtotal";
            this.colSubTotal.Name = "colSubTotal";
            this.colSubTotal.OptionsColumn.AllowEdit = false;
            this.colSubTotal.Visible = true;
            this.colSubTotal.VisibleIndex = 14;
            // 
            // colItemGroup
            // 
            this.colItemGroup.Caption = "ItemGroup";
            this.colItemGroup.FieldName = "ItemGroup";
            this.colItemGroup.Name = "colItemGroup";
            this.colItemGroup.OptionsColumn.AllowEdit = false;
            this.colItemGroup.Visible = true;
            this.colItemGroup.VisibleIndex = 6;
            // 
            // colItemType
            // 
            this.colItemType.Caption = "Item Type";
            this.colItemType.FieldName = "ItemType";
            this.colItemType.Name = "colItemType";
            this.colItemType.OptionsColumn.AllowEdit = false;
            // 
            // colRemark1
            // 
            this.colRemark1.Caption = "Remark1";
            this.colRemark1.FieldName = "Remark1";
            this.colRemark1.Name = "colRemark1";
            this.colRemark1.OptionsColumn.AllowEdit = false;
            // 
            // colRemark2
            // 
            this.colRemark2.Caption = "Remark2";
            this.colRemark2.FieldName = "Remark2";
            this.colRemark2.Name = "colRemark2";
            this.colRemark2.OptionsColumn.AllowEdit = false;
            // 
            // colRemark3
            // 
            this.colRemark3.Caption = "Remark3";
            this.colRemark3.FieldName = "Remark3";
            this.colRemark3.Name = "colRemark3";
            this.colRemark3.OptionsColumn.AllowEdit = false;
            // 
            // colRemark4
            // 
            this.colRemark4.Caption = "Remark4";
            this.colRemark4.FieldName = "Remark4";
            this.colRemark4.Name = "colRemark4";
            this.colRemark4.OptionsColumn.AllowEdit = false;
            // 
            // colRefDocNo
            // 
            this.colRefDocNo.Caption = "RefDocNo";
            this.colRefDocNo.FieldName = "RefDocNo";
            this.colRefDocNo.Name = "colRefDocNo";
            this.colRefDocNo.OptionsColumn.AllowEdit = false;
            // 
            // colPrintCount
            // 
            this.colPrintCount.Caption = "PrintCount";
            this.colPrintCount.FieldName = "PrintCount";
            this.colPrintCount.Name = "colPrintCount";
            this.colPrintCount.OptionsColumn.AllowEdit = false;
            // 
            // colLastModified
            // 
            this.colLastModified.Caption = "Last Modified";
            this.colLastModified.FieldName = "LastModified";
            this.colLastModified.Name = "colLastModified";
            this.colLastModified.OptionsColumn.AllowEdit = false;
            // 
            // colLastModifiedUserID
            // 
            this.colLastModifiedUserID.Caption = "Last Modified User ID";
            this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
            this.colLastModifiedUserID.Name = "colLastModifiedUserID";
            this.colLastModifiedUserID.OptionsColumn.AllowEdit = false;
            // 
            // colCreatedTimeStamp
            // 
            this.colCreatedTimeStamp.Caption = "Created Time Stamp";
            this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
            this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
            this.colCreatedTimeStamp.OptionsColumn.AllowEdit = false;
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.Caption = "Created User ID";
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.Name = "colCreatedUserID";
            this.colCreatedUserID.OptionsColumn.AllowEdit = false;
            // 
            // ucSearchResult1
            // 
            this.ucSearchResult1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(159)))), ((int)(((byte)(248)))));
            this.ucSearchResult1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(103)))), ((int)(((byte)(245)))));
            this.ucSearchResult1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.ucSearchResult1.Appearance.Options.UseBackColor = true;
            this.ucSearchResult1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucSearchResult1.Location = new System.Drawing.Point(0, 0);
            this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ucSearchResult1.Name = "ucSearchResult1";
            this.ucSearchResult1.Size = new System.Drawing.Size(977, 46);
            this.ucSearchResult1.TabIndex = 1;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.memoEdit_Criteria);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(977, 146);
            this.xtraTabPage4.Text = "Criteria";
            // 
            // memoEdit_Criteria
            // 
            this.memoEdit_Criteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit_Criteria.Location = new System.Drawing.Point(0, 0);
            this.memoEdit_Criteria.Name = "memoEdit_Criteria";
            this.memoEdit_Criteria.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit_Criteria.Properties.Appearance.Options.UseFont = true;
            this.memoEdit_Criteria.Properties.ReadOnly = true;
            this.memoEdit_Criteria.Size = new System.Drawing.Size(977, 146);
            this.memoEdit_Criteria.TabIndex = 0;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1, true)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnDesignDetailListingReport),
            new DevExpress.XtraBars.LinkPersistInfo(this.barbtnAdvancedFilter, true)});
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnDesignDetailListingReport
            // 
            this.barBtnDesignDetailListingReport.Id = 2;
            this.barBtnDesignDetailListingReport.Name = "barBtnDesignDetailListingReport";
            this.barBtnDesignDetailListingReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDetailListingReport_ItemClick);
            // 
            // barbtnAdvancedFilter
            // 
            this.barbtnAdvancedFilter.Id = 1;
            this.barbtnAdvancedFilter.Name = "barbtnAdvancedFilter";
            this.barbtnAdvancedFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnAdvancedFilter_ItemClick);
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Print Material Request Detail Listing";
            this.panelHeader1.HelpTopicId = "Stock_Issue.htm#stk028";
            this.panelHeader1.Location = new System.Drawing.Point(0, 0);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(986, 44);
            this.panelHeader1.TabIndex = 4;
            // 
            // FormMaterialRequestPrintDetailListing
            // 
            this.CancelButton = this.sbtnClose;
            this.ClientSize = new System.Drawing.Size(986, 616);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelCenter);
            this.Controls.Add(this.pnCriteriaBasic);
            this.Controls.Add(this.panelCriteria);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormMaterialRequestPrintDetailListing";
            this.Text = "Print Material Request Detail Listing";
            this.Load += new System.EventHandler(this.FormSIDetailPrintSearch_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormMaterialRequestPrintDetailListing_Closing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormMaterialRequestPrintDetailListing_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).EndInit();
            this.panelCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbFilter)).EndInit();
            this.gbFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbReport)).EndInit();
            this.gbReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCriteriaBasic)).EndInit();
            this.pnCriteriaBasic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbBasic)).EndInit();
            this.gbBasic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelCenter)).EndInit();
            this.panelCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit_Cancelled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFurtherDescription)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        private void InitUserControls()
        {
            if (this.myReportingCriteria.AdvancedOptions)
            {
                this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_BasicOptions, new object[0]);
                this.panelCriteria.Visible = true;
                this.pnCriteriaBasic.Visible = false;
                this.cbEditGroupBy.SelectedIndex = (int)this.myReportingCriteria.GroupBy;
                this.cbEditSortBy.SelectedIndex = (int)this.myReportingCriteria.SortBy;
            }
            else
            {
                this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_AdvancedOptions, new object[0]);
                this.panelCriteria.Visible = false;
                this.pnCriteriaBasic.Visible = true;
                this.cbEditGroupBy2.SelectedIndex = (int)this.myReportingCriteria.GroupBy;
                this.cbEditSortBy2.SelectedIndex = (int)this.myReportingCriteria.SortBy;
            }
            this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
            this.ucMaterialRequestSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
            this.ucItemSelector1.Initialize(this.myReportingCriteria.ItemCodeFilter);
            this.ucItemGroupSelector1.Initialize(this.myReportingCriteria.ItemGroupFilter);
            this.ucItemTypeSelector1.Initialize(this.myReportingCriteria.ItemTypeFilter);
            this.ucLocationSelector1.Initialize(this.myReportingCriteria.LocationFilter);
            this.ucProjectSelector1.Initialize(this.myReportingCriteria.ProjecNoFilter);
            this.ucDepartmentSelector1.Initialize(this.myReportingCriteria.DeptNoFilter);
            this.ucDateSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
            this.ucMaterialRequestSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
            this.ucItemSelector2.Initialize(this.myReportingCriteria.ItemCodeFilter);
        }

        private void InitializeSettings()
        {
            this.GetGroupSortCriteria();
        }

        private void GetGroupSortCriteria()
        {
            if (this.myReportingCriteria.AdvancedOptions)
            {
                this.myReportingCriteria.GroupBy = (DetailListingGroupByOption)this.cbEditGroupBy.SelectedIndex;
                this.myReportingCriteria.SortBy = (DetailListingSortByOption)this.cbEditSortBy.SelectedIndex;
                this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
            }
            else
            {
                this.myReportingCriteria.GroupBy = (DetailListingGroupByOption)this.cbEditGroupBy2.SelectedIndex;
                this.myReportingCriteria.SortBy = (DetailListingSortByOption)this.cbEditSortBy2.SelectedIndex;
                this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria2.Checked;
            }
        }

        private void AddNewColumn()
        {
            this.myDataTable.Columns.Add(new DataColumn()
            {
                DataType = System.Type.GetType("System.Boolean"),
                AllowDBNull = true,
                Caption = "Check",
                ColumnName = "ToBeUpdate",
                DefaultValue = (object)false
            });
        }

        private void BasicSearch(bool isSearchAll)
        {
            if (!this.myInSearch)
            {
                this.myInSearch = true;
                BCE.XtraUtils.GridViewUtils.UpdateData(this.gvMaster);
                this.gridControl1.DataSource = (object)null;
                try
                {
                    this.gridControl1.MainView.UpdateCurrentRow();
                    this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
                    string columnName = CommonFunction.BuildSQLColumns(isSearchAll, this.gvMaster.Columns.View);
                    if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                        this.AddNewColumn();
                    this.myCommand.DetailListingBasicSearch(this.myReportingCriteria, columnName, this.myDataSet, "ToBeUpdate");
                    this.gridControl1.DataSource = (object)this.myDataTable;
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                }
                finally
                {
                    this.myInSearch = false;
                }
                FormMaterialRequestPrintDetailListing.FormInquiryEventArgs inquiryEventArgs = new FormMaterialRequestPrintDetailListing.FormInquiryEventArgs(this.panelCriteria, this.panelCenter, this.xtraTabControl2, this.gridControl1, this, this.myDataTable, this.myCommand);
                this.myScriptObject.RunMethod("OnFormInquiry", new System.Type[1]
        {
          inquiryEventArgs.GetType()
        }, new object[1]
        {
          (object) inquiryEventArgs
        });
            }
        }

        private void AdvancedSearch()
        {
            if (this.myFormAdvancedSearch == null)
            {
                this.myCriteria = new AdvancedMaterialRequestCriteria(this.myDBSetting);
                try
                {
                    this.myFormAdvancedSearch = new FormAdvancedSearch((SearchCriteria)this.myCriteria, this.myDBSetting);
                }
                catch (Exception ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                    return;
                }
            }
            this.myCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
            if (this.myFormAdvancedSearch.ShowDialog((IWin32Window)this) == DialogResult.OK)
            {
                if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                    this.AddNewColumn();
                this.memoEdit_Criteria.Lines = this.myCriteria.BuildReadableTextArray();
                this.ucSearchResult1.KeepSearchResult = this.myCriteria.KeepSearchResult;
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.myInSearch = true;
                try
                {
                    this.myCommand.DetailListingAdvanceSearch(this.myCriteria, this.myDataSet, "ToBeUpdate");
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                }
                finally
                {
                    this.ucSearchResult1.CheckAll();
                    this.myInSearch = false;
                    Cursor.Current = current;
                }
                FormMaterialRequestPrintDetailListing.FormInquiryEventArgs inquiryEventArgs = new FormMaterialRequestPrintDetailListing.FormInquiryEventArgs(this.panelCriteria, this.panelCenter, this.xtraTabControl2, this.gridControl1, this, this.myDataTable, this.myCommand);
                this.myScriptObject.RunMethod("OnFormInquiry", new System.Type[1]
        {
          inquiryEventArgs.GetType()
        }, new object[1]
        {
          (object) inquiryEventArgs
        });
            }
        }

        private string GenerateDocNosToString()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvMaster);
            DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = True");
            if (dataRowArray.Length <= 0)
            {
                AppMessage.ShowMessage((Form)this, BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.ShowMessage_MaterialRequestNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
                return (string)null;
            }
            else
            {
                string str = "";
                foreach (DataRow dataRow in dataRowArray)
                {
                    if (str.Length != 0)
                        str = str + ", ";
                    str = str + "'" + dataRow["DocNo"].ToString() + "'";
                }
                return str;
            }
        }

        private void sbtnInquiry_Click(object sender, EventArgs e)
        {
            this.sbtnToggleOptions.Enabled = true;
            this.BasicSearch(this.myLoadAllColumns);
            this.SortByOption();
            this.ucSearchResult1.CheckAll();
            this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
            this.gridControl1.Focus();
            Activity.Log(this.myDBSetting, "", 0L, 0L, BCE.Localization.Localizer.GetString((Enum)MaterialRequestString.InquiredPrintMaterialRequestDetailListing, new object[1]
      {
        (object) BCE.AutoCount.Application.DBSession.LoginUserID
      }), this.memoEdit_Criteria.Text);
        }

        private void ReloadAllColumns(object sender, EventArgs e)
        {
            this.myLoadAllColumns = true;
            this.BasicSearch(true);
        }

        private void barbtnAdvancedFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.myReportingCriteria.AdvancedOptions)
            {
                this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_AdvancedOptions, new object[0]);
                this.pnCriteriaBasic.Visible = true;
                this.panelCriteria.Visible = false;
                this.myReportingCriteria.AdvancedOptions = false;
                this.ucDateSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
                this.ucMaterialRequestSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
                this.ucItemSelector2.Initialize(this.myReportingCriteria.ItemCodeFilter);
                this.cbEditGroupBy2.SelectedIndex = this.cbEditGroupBy.SelectedIndex;
                this.cbEditSortBy2.SelectedIndex = this.cbEditSortBy.SelectedIndex;
                this.chkEditShowCriteria2.Checked = this.chkEditShowCriteria.Checked;
            }
            else
            {
                this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_BasicOptions, new object[0]);
                this.pnCriteriaBasic.Visible = false;
                this.panelCriteria.Visible = true;
                this.myReportingCriteria.AdvancedOptions = true;
                this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
                this.ucMaterialRequestSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
                this.ucItemSelector1.Initialize(this.myReportingCriteria.ItemCodeFilter);
                this.ucItemGroupSelector1.Initialize(this.myReportingCriteria.ItemGroupFilter);
                this.ucItemTypeSelector1.Initialize(this.myReportingCriteria.ItemTypeFilter);
                this.ucLocationSelector1.Initialize(this.myReportingCriteria.LocationFilter);
                this.ucProjectSelector1.Initialize(this.myReportingCriteria.ProjecNoFilter);
                this.ucDepartmentSelector1.Initialize(this.myReportingCriteria.DeptNoFilter);
                this.cbEditGroupBy.SelectedIndex = this.cbEditGroupBy2.SelectedIndex;
                this.cbEditSortBy.SelectedIndex = this.cbEditSortBy2.SelectedIndex;
                this.chkEditShowCriteria.Checked = this.chkEditShowCriteria2.Checked;
            }
        }

        private void sbtnToggleOptions_Click(object sender, EventArgs e)
        {
            if (this.myReportingCriteria.AdvancedOptions)
            {
                this.panelCriteria.Visible = !this.panelCriteria.Visible;
                if (this.panelCriteria.Visible)
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_HideOptions, new object[0]);
                else
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_ShowOptions, new object[0]);
            }
            else
            {
                this.pnCriteriaBasic.Visible = !this.pnCriteriaBasic.Visible;
                if (this.pnCriteriaBasic.Visible)
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_HideOptions, new object[0]);
                else
                    this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.Code_ShowOptions, new object[0]);
            }
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbCancelledOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
            {
                if (this.cbCancelledOption.SelectedIndex == 0)
                    this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.All;
                else if (this.cbCancelledOption.SelectedIndex == 1)
                    this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.Cancelled;
                else
                    this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.UnCancelled;
            }
        }

        private void FormMaterialRequestPrintDetailListing_Closing(object sender, CancelEventArgs e)
        {
            this.SaveCriteria();
        }

        private void FormMaterialRequestPrintDetailListing_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)120)
            {
                if (this.sbtnInquiry.Enabled)
                    this.sbtnInquiry.PerformClick();
            }
            else if (e.KeyCode == (Keys)119)
            {
                if (this.previewButton1.Enabled)
                    this.previewButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)118)
            {
                if (this.printButton1.Enabled)
                    this.printButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)117 && this.sbtnToggleOptions.Enabled)
                this.sbtnToggleOptions.PerformClick();
        }

        private void cbEditSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
            {
                if (this.myReportingCriteria.SortBy != (DetailListingSortByOption)this.cbEditSortBy.SelectedIndex)
                    this.myReportingCriteria.SortBy = (DetailListingSortByOption)this.cbEditSortBy.SelectedIndex;
                this.SortByOption();
            }
        }

        private void cbEditSortBy2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
            {
                if (this.myReportingCriteria.SortBy != (DetailListingSortByOption)this.cbEditSortBy2.SelectedIndex)
                    this.myReportingCriteria.SortBy = (DetailListingSortByOption)this.cbEditSortBy2.SelectedIndex;
                this.SortByOption();
            }
        }

        private void cbEditGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.GroupBy = (DetailListingGroupByOption)this.cbEditGroupBy.SelectedIndex;
        }

        private void cbEditGroupBy2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.GroupBy = (DetailListingGroupByOption)this.cbEditGroupBy2.SelectedIndex;
        }

        private void sbtnAdvOptions_Click(object sender, EventArgs e)
        {
            //if (this.myFormAdvOptions == null)
            ////    this.myFormAdvOptions = new BCE.AutoCount.Stock.MaterialRequest.FormMaterialRequestDetailListingAdvOptions(this.myReportingCriteria);
            //this.myFormAdvOptions.SetFilterByLocation(this.myFilterByLocation);
            //if (this.myFormAdvOptions.ShowDialog() == DialogResult.OK)
            //    this.cbCancelledOption.SelectedItem = this.myFormAdvOptions.IsPrintCancelled;
        }

        private void sbtnAdvanceSearch_Click(object sender, EventArgs e)
        {
            this.AdvancedSearch();
        }

        private void gvMaster_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GridView gridView = (GridView)sender;
            int[] selectedRows = gridView.GetSelectedRows();
            if (selectedRows != null && selectedRows.Length != 0 && selectedRows.Length != 1)
            {
                for (int index = 0; index < selectedRows.Length; ++index)
                    gridView.GetDataRow(selectedRows[index])["ToBeUpdate"] = (object)true;
            }
        }

        private void chkEditShowCriteria_CheckedChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
        }

        private void repositoryItemTextEdit_Cancelled_FormatEditValue(object sender, ConvertEditValueEventArgs e)
        {
            if (e.Value != null)
            {
                if (e.Value.ToString() == "T")
                    e.Value = (object)"Cancelled";
                else
                    e.Value = (object)"";
            }
        }

        private void previewButton1_Preview(object sender, PrintEventArgs e)
        {
            string selectedDtlKeysInString = this.SelectedDtlKeysInString;
            if (selectedDtlKeysInString == "")
            {
                AppMessage.ShowMessage((Form)this, BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.ShowMessage_MaterialRequestNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            else if (BCE.AutoCount.Application.AccessRight.IsAccessible("PRPLUGINS_MR_DTLLIST_RPT_PREVIEW"))
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.GetGroupSortCriteria();
                this.myCommand.PreviewDetailListingReport(selectedDtlKeysInString, this.myReportingCriteria, e.DefaultReport);
                Cursor.Current = current;
            }
        }

        private void printButton1_Print(object sender, PrintEventArgs e)
        {
            string selectedDtlKeysInString = this.SelectedDtlKeysInString;
            if (selectedDtlKeysInString == "")
            {
                AppMessage.ShowMessage((Form)this, BCE.Localization.Localizer.GetString((Enum)MaterialRequestStringId.ShowMessage_MaterialRequestNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            else if (BCE.AutoCount.Application.AccessRight.IsAccessible("PRPLUGINS_MR_DTLLIST_RPT_PRINT"))
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.GetGroupSortCriteria();
                this.myCommand.PrintDetailListingReport(selectedDtlKeysInString, this.myReportingCriteria, e.DefaultReport);
                Cursor.Current = current;
            }
        }

        private void barBtnDesignDetailListingReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportTool.DesignReport("Material Request Detail Listing", this.myDBSetting);
        }

        public virtual void RefreshDesignReport()
        {
            this.barBtnDesignDetailListingReport.Visibility = XtraBarsUtils.ToBarItemVisibility(BCE.AutoCount.Application.AccessRight.IsAccessible("TOOLS_RPT_SHOW"));
            if (this.ucLocationSelector1 != null)
            {
                this.myFilterByLocation = SystemOptionPolicy.GeneralSystemOptionPolicy.EnableFilterByCurrentUserLocationAccessRight;
                if (this.myFilterByLocation)
                {
                    this.ucLocationSelector1.Filter.Type = FilterType.ByRange;
                    this.ucLocationSelector1.Filter.From = (object)BCE.AutoCount.Application.MainLocation;
                    this.ucLocationSelector1.Filter.To = (object)BCE.AutoCount.Application.MainLocation;
                    this.ucLocationSelector1.ApplyFilter();
                    this.ucLocationSelector1.Enabled = false;
                }
                else
                    this.ucLocationSelector1.Enabled = true;
            }
        }

        private void SortByOption()
        {
            DetailListingSortByOption listingSortByOption = !this.myReportingCriteria.AdvancedOptions ? (DetailListingSortByOption)this.cbEditSortBy2.SelectedIndex : (DetailListingSortByOption)this.cbEditSortBy.SelectedIndex;
            string[] strArray = new string[8]
      {
        "DocNo",
        "DocDate",
        "ItemCode",
        "ItemGroup",
        "ItemType",
        "Location",
        "ProjNo",
        "DeptNo"
      };
            for (int index = 0; index < strArray.Length; ++index)
                this.gvMaster.Columns[strArray[index]].SortOrder = (DetailListingSortByOption)index != listingSortByOption ? ColumnSortOrder.None : ColumnSortOrder.Ascending;
        }

        private void FormSIDetailPrintSearch_Load(object sender, EventArgs e)
        {
            this.FormInitialize();
        }

        private void FormInitialize()
        {
            FormMaterialRequestPrintDetailListing.FormInitializeEventArgs initializeEventArgs = new FormMaterialRequestPrintDetailListing.FormInitializeEventArgs(this.panelCriteria, this.panelCenter, this.xtraTabControl2, this.gridControl1, this, this.myCommand);
            this.myScriptObject.RunMethod("OnFormInitialize", new System.Type[1]
      {
        initializeEventArgs.GetType()
      }, new object[1]
      {
        (object) initializeEventArgs
      });
        }

        private void repositoryItemFurtherDescription_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            int focusedRowHandle = this.gvMaster.FocusedRowHandle;
            if (this.gvMaster.GetDataRow(focusedRowHandle) != null)
            {
                using (FormRichTextEditor formRichTextEditor = new FormRichTextEditor(this.myDataTable, this.gvMaster.GetDataSourceRowIndex(focusedRowHandle), "FurtherDescription", "Further Description", false))
                {
                    int num = (int)formRichTextEditor.ShowDialog((IWin32Window)this);
                }
            }
        }

        private void GoToDocument()
        {
            ColumnView columnView = (ColumnView)this.gridControl1.FocusedView;
            DataRow dataRow = columnView.GetDataRow(columnView.FocusedRowHandle);
            if (dataRow != null)
                DocumentDispatcher.Open("MR", BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
        }

        private void gvMaster_DoubleClick(object sender, EventArgs e)
        {
            if (this.myMouseDownHelper.IsLeftMouseDown && BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView)sender).InRow && BCE.AutoCount.Application.AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
                this.GoToDocument();
        }

        public class FormEventArgs
        {
            private PanelControl myPanelCriteria;
            private PanelControl myPanelButtons;
            private XtraTabControl myTabControl;
            private GridControl myGridControl;
            private FormMaterialRequestPrintDetailListing myForm;
            private MaterialRequestReportCommand myCommand;

            public MaterialRequestReportCommand Command
            {
                get
                {
                    return this.myCommand;
                }
            }

            public PanelControl PanelCriteria
            {
                get
                {
                    return this.myPanelCriteria;
                }
            }

            public PanelControl PanelButtons
            {
                get
                {
                    return this.myPanelButtons;
                }
            }

            public XtraTabControl TabControl
            {
                get
                {
                    return this.myTabControl;
                }
            }

            public GridControl GridControl
            {
                get
                {
                    return this.myGridControl;
                }
            }

            public FormMaterialRequestPrintDetailListing Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public FormEventArgs(PanelControl pnlCriteria, PanelControl pnlButtons, XtraTabControl tabControl, GridControl gridControl, FormMaterialRequestPrintDetailListing form, MaterialRequestReportCommand cmd)
            {
                this.myPanelCriteria = pnlCriteria;
                this.myPanelButtons = pnlButtons;
                this.myTabControl = tabControl;
                this.myGridControl = gridControl;
                this.myForm = form;
                this.myCommand = cmd;
            }
        }

        public class FormInitializeEventArgs : FormMaterialRequestPrintDetailListing.FormEventArgs
        {
            public FormInitializeEventArgs(PanelControl pnlCriteria, PanelControl pnlButtons, XtraTabControl tabControl, GridControl gridControl, FormMaterialRequestPrintDetailListing form, MaterialRequestReportCommand cmd)
                : base(pnlCriteria, pnlButtons, tabControl, gridControl, form, cmd)
            {
            }
        }

        public class FormInquiryEventArgs : FormMaterialRequestPrintDetailListing.FormEventArgs
        {
            private DataTable myResultTable;

            public DataTable ResultTable
            {
                get
                {
                    return this.myResultTable;
                }
            }

            public FormInquiryEventArgs(PanelControl pnlCriteria, PanelControl pnlButtons, XtraTabControl tabControl, GridControl gridControl, FormMaterialRequestPrintDetailListing form, DataTable resultTable, MaterialRequestReportCommand cmd)
                : base(pnlCriteria, pnlButtons, tabControl, gridControl, form, cmd)
            {
                this.myResultTable = resultTable;
            }
        }

        private void gbReport_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelCriteria_Paint(object sender, PaintEventArgs e)
        {

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }
    }
}
