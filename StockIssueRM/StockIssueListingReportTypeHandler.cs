﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueListingReportTypeHandler
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Stock;
using BCE.Data;

namespace BCE.AutoCount.Stock.StockIssue
{
  public class StockIssueListingReportTypeHandler : StockDocumentReportTypeHandler
  {
    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      return StockIssueCommand.Create(dbSetting).GetDocumentListingReportDesignerDataSource((StockIssueReportingCriteria) null);
    }
  }
}
