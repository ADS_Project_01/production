﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueDetailReportingCriteria
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Localization;
using System;
using System.Collections;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueDetailReportingCriteria : StockIssueReportCommonCriteria
  {
    protected BCE.AutoCount.SearchFilter.Filter myItemCode = new BCE.AutoCount.SearchFilter.Filter("RPA_RMDtl", "ItemCode", Localizer.GetString((Enum) StockStringId.ItemCode, new object[0]), FilterControlType.Item);
    protected BCE.AutoCount.SearchFilter.Filter myItemGroup = new BCE.AutoCount.SearchFilter.Filter("D", "ItemGroup", Localizer.GetString((Enum) StockStringId.ItemGroup, new object[0]), FilterControlType.ItemGroup);
    protected BCE.AutoCount.SearchFilter.Filter myItemType = new BCE.AutoCount.SearchFilter.Filter("D", "ItemType", Localizer.GetString((Enum) StockStringId.ItemType, new object[0]), FilterControlType.ItemType);
    protected BCE.AutoCount.SearchFilter.Filter myLocation = new BCE.AutoCount.SearchFilter.Filter("RPA_RMDtl", "Location", Localizer.GetString((Enum) StockStringId.Location, new object[0]), FilterControlType.Location);
    protected BCE.AutoCount.SearchFilter.Filter myProjNo = new BCE.AutoCount.SearchFilter.Filter("RPA_RMDtl", "ProjNo", Localizer.GetString((Enum) StockStringId.ProjectNo, new object[0]), FilterControlType.Project);
    protected BCE.AutoCount.SearchFilter.Filter myDeptNo = new BCE.AutoCount.SearchFilter.Filter("RPA_RMDtl", "DeptNo", Localizer.GetString((Enum) StockStringId.DepartmentNo, new object[0]), FilterControlType.Department);
    protected DetailListingSortByOption mySortBy = DetailListingSortByOption.Date;
    private bool myAdvancedOptions;
    private bool myCheckAll;
    protected DetailListingGroupByOption myGroupBy;

    public BCE.AutoCount.SearchFilter.Filter ItemCodeFilter
    {
      get
      {
        return this.myItemCode;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ItemGroupFilter
    {
      get
      {
        return this.myItemGroup;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ItemTypeFilter
    {
      get
      {
        return this.myItemType;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter LocationFilter
    {
      get
      {
        return this.myLocation;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ProjecNoFilter
    {
      get
      {
        return this.myProjNo;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter DeptNoFilter
    {
      get
      {
        return this.myDeptNo;
      }
    }

    public bool AdvancedOptions
    {
      get
      {
        return this.myAdvancedOptions;
      }
      set
      {
        this.myAdvancedOptions = value;
      }
    }

    public bool CheckAll
    {
      get
      {
        return this.myCheckAll;
      }
      set
      {
        this.myCheckAll = value;
      }
    }

    public DetailListingGroupByOption GroupBy
    {
      get
      {
        return this.myGroupBy;
      }
      set
      {
        this.myGroupBy = value;
      }
    }

    public DetailListingSortByOption SortBy
    {
      get
      {
        return this.mySortBy;
      }
      set
      {
        this.mySortBy = value;
      }
    }

    protected override ArrayList GetFilterOptionsText()
    {
      ArrayList filterOptionsText = base.GetFilterOptionsText();
      string str1 = this.myItemCode.BuildReadableText(false);
      if (str1.Length > 0)
        filterOptionsText.Add((object) str1);
      string str2 = this.myItemGroup.BuildReadableText(false);
      if (str2.Length > 0)
        filterOptionsText.Add((object) str2);
      string str3 = this.myItemType.BuildReadableText(false);
      if (str3.Length > 0)
        filterOptionsText.Add((object) str3);
      string str4 = this.myLocation.BuildReadableText(false);
      if (str4.Length > 0)
        filterOptionsText.Add((object) str4);
      string str5 = this.myProjNo.BuildReadableText(false);
      if (str5.Length > 0)
        filterOptionsText.Add((object) str5);
      string str6 = this.myDeptNo.BuildReadableText(false);
      if (str6.Length > 0)
        filterOptionsText.Add((object) str6);
      filterOptionsText.Add((object) this.GetCancelledDocumentOptionText(this.myIsPrintCancelled));
      if (filterOptionsText.Count == 0)
        filterOptionsText.Add((object) Localizer.GetString((Enum) ReportCriteriaStringId.NoFilter, new object[0]));
      return filterOptionsText;
    }

    protected override ArrayList GetReportOptionsText()
    {
      ArrayList reportOptionsText = base.GetReportOptionsText();
      reportOptionsText.Add((object) this.GetSortByText(Localizer.GetString((Enum) this.mySortBy, new object[0])));
      reportOptionsText.Add((object) this.GetGroupByText(Localizer.GetString((Enum) this.myGroupBy, new object[0])));
      return reportOptionsText;
    }
  }
}
