﻿
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;

namespace Production.StockIssueRM
{
    [Obsolete("Use FormStockIssueEntry.FormInitializeEventArgs instead")]
    public class FormInitializeEventArgs : FormStockIssueEventArgs
    {
        public FormInitializeEventArgs(StockIssue doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
          : base(doc, headerPanel, gridControl, tabControl)
        {
        }
    }
}
