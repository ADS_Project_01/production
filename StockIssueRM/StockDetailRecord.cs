﻿using BCE.AutoCount.Data;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Misc;
using System.Data;

namespace Production.StockIssueRM
{
    public abstract class StockDetailRecord : BaseRecord
    {
        protected DecimalSetting myDecimalSetting;

        public long DtlKey
        {
            get
            {
                return Convert.ToInt64(this.myRow["DtlKey"]);
            }
        }

        public int Seq
        {
            get
            {
                return Convert.ToInt32(this.myRow["Seq"]);
            }
        }

        public DBString Numbering
        {
            get
            {
                return Convert.ToDBString(this.myRow["Numbering"]);
            }
            set
            {
                this.myRow["Numbering"] = Convert.ToDBObject(value);
            }
        }

        public DBString ItemCode
        {
            get
            {
                return Convert.ToDBString(this.myRow["ItemCode"]);
            }
            set
            {
                this.myRow["ItemCode"] = Convert.ToDBObject(value);
            }
        }

        public DBString UOM
        {
            get
            {
                return Convert.ToDBString(this.myRow["UOM"]);
            }
            set
            {
                this.myRow["UOM"] = Convert.ToDBObject(value);
            }
        }

        public DBString Location
        {
            get
            {
                return Convert.ToDBString(this.myRow["Location"]);
            }
            set
            {
                this.myRow["Location"] = Convert.ToDBObject(value);
            }
        }

        public DBString BatchNo
        {
            get
            {
                return Convert.ToDBString(this.myRow["BatchNo"]);
            }
            set
            {
                this.myRow["BatchNo"] = Convert.ToDBObject(value);
            }
        }

        public DBString Description
        {
            get
            {
                return Convert.ToDBString(this.myRow["Description"]);
            }
            set
            {
                this.myRow["Description"] = Convert.ToDBObject(value);
            }
        }

        public DBString FurtherDescription
        {
            get
            {
                return Convert.ToDBString(this.myRow["FurtherDescription"]);
            }
            set
            {
                if (value.HasValue)
                    this.myRow["FurtherDescription"] = (object)Rtf.ToArialRichText((string)value);
                else
                    this.myRow["FurtherDescription"] = Convert.ToDBObject(value);
            }
        }

        public DBString ProjNo
        {
            get
            {
                return Convert.ToDBString(this.myRow["ProjNo"]);
            }
            set
            {
                this.myRow["ProjNo"] = Convert.ToDBObject(value);
            }
        }

        public DBString DeptNo
        {
            get
            {
                return Convert.ToDBString(this.myRow["DeptNo"]);
            }
            set
            {
                this.myRow["DeptNo"] = Convert.ToDBObject(value);
            }
        }

        public DBDecimal Qty
        {
            get
            {
                return Convert.ToDBDecimal(this.myRow["Qty"]);
            }
            set
            {
                this.myRow["Qty"] = this.myDecimalSetting.RoundToQuantityDBObject(value);
            }
        }
      

        public DBDecimal UnitCost
        {
            get
            {
                return Convert.ToDBDecimal(this.myRow["UnitCost"]);
            }
            set
            {
                this.myRow["UnitCost"] = this.myDecimalSetting.RoundToCostDBObject(value);
            }
        }

        public DBDecimal SubTotal
        {
            get
            {
                return Convert.ToDBDecimal(this.myRow["SubTotal"]);
            }
            set
            {
                this.myRow["SubTotal"] = this.myDecimalSetting.RoundToCurrencyDBObject(value);
            }
        }

        public bool PrintOut
        {
            get
            {
                return Convert.TextToBoolean(this.myRow["PrintOut"]);
            }
            set
            {
                this.myRow["PrintOut"] = (object)Convert.BooleanToText(value);
            }
        }

        internal StockDetailRecord(DBSetting dbSetting, DataRow row)
          : base(row)
        {
            this.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
        }
    }

}
