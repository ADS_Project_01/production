﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueAction
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

namespace Production.StockIssueRM
{
  public enum StockIssueAction
  {
    New,
    View,
    Edit,
  }
}
