﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueRecord
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Stock;
using BCE.Data;
using System;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueRecord : StockDocumentRecord
  {
    private StockIssue myStock;

    public object UserData
    {
      get
      {
        return this.myStock.UserData;
      }
      set
      {
        this.myStock.UserData = value;
      }
    }

    internal StockIssueRecord(StockIssue doc)
      : base(doc.DataTableMaster.Rows[0], doc.GetValidDetailRows())
    {
      this.myStock = doc;
    }

    public StockIssueDetailRecord GetDetailRecord(int index)
    {
      return new StockIssueDetailRecord(this.myStock.Command.DBSetting, this.myDetailRows[index]);
    }

    public StockIssueDetailRecord NewDetail()
    {
      StockIssueDetail stockIssueDetail = this.myStock.AddDetail(null);
      this.myDetailRows = this.myStock.GetValidDetailRows();
      return new StockIssueDetailRecord(this.myStock.Command.DBSetting, stockIssueDetail.Row);
    }

    public bool DeleteDetailRecord(int index)
    {
      if (index < 0 || index >= this.myDetailRows.Length)
        throw new ArgumentException("index not in valid range.");
      else if (this.myStock.DeleteDetail(BCE.Data.Convert.ToInt64(this.myDetailRows[index]["DtlKey"])))
      {
        this.myDetailRows = this.myStock.GetValidDetailRows();
        return true;
      }
      else
        return false;
    }

    public void SetDocNoFormatName(string formatName)
    {
      this.myStock.DocNoFormatName = formatName;
    }
  }
}
