﻿
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;

namespace Production.StockIssueRM
{
    [Obsolete("Use FormStockIssueEntry.FormClosedEventArgs instead")]
    public class FormClosedEventArgs : FormStockIssueEventArgs
    {
        public FormClosedEventArgs(StockIssue doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
          : base(doc, headerPanel, gridControl, tabControl)
        {
        }
    }
}
