﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueString
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Localization;

namespace Production.StockIssueRM
{
  [LocalizableString]
  public enum StockIssueString
  {
    [DefaultString("Post from Stock Issue Raw Material: {0}")] PostFromStockIssue,
    [DefaultString("{0} opened Stock Issue Raw Material window.")] OpenedStockIssueWindow,
    [DefaultString("Stock Issue Raw Material {0}")] StockIssue,
    [DefaultString("{0} viewed Stock Issue Raw Material {1} in edit mode.")] ViewStockIssueInEditMode,
    [DefaultString("{0} viewed Stock Issue Raw Material {1}.")] ViewStockIssue,
    [DefaultString("{0} cancels Stock Issue Raw Material {1}.")] CancelStockIssue,
    [DefaultString("{0} un-cancels Stock Issue Raw Material {1}.")] UncancelStockIssue,
    [DefaultString("{0} edited Stock Issue Raw Material {1}.")] EditedStockIssue,
    [DefaultString("{0} opened Print Stock Issue Raw Material Detail Listing window.")] OpenedPrintStockIssueDetailListingWindow,
    [DefaultString("{0} inquired on Print Stock Issue Raw Material Detail Listing.")] InquiredPrintStockIssueDetailListing,
    [DefaultString("Stock Issue Raw Material Detail Listing")] StockIssueDetailListing,
    [DefaultString("{0} opened Print Stock Issue Raw Material Listing window.")] OpenedPrintStockIssueListingWindow,
    [DefaultString("{0} inquired on Print Stock Issue Raw Material Listing.")] InquiredPrintStockIssueListing,
    [DefaultString("Batch Stock Issue Raw Material")] BatchPrintStockIssue,
    [DefaultString("Stock Issue Raw Material Listing")] PrintStockIssueListing,
    [DefaultString("{0} created new Stock Issue Raw Material {1}.")] CreateNewStockIssue,
    [DefaultString("{0} updated Stock Issue Raw Material {1}.")] UpdatedStockIssue,
    [DefaultString("{0} deleted Stock Issue Raw Material {1}.")] DeletedStockIssue,
    [DefaultString("Batch Print Stock Issue Raw Material / Print Stock Issue Raw Material Listing")] BatchPrintStockIssueOrStockIssueListing,
    [DefaultString("Print Stock Issue Raw Material Detail Listing")] PrintStockIssueDetailListing,
  }
}
