﻿// Type: BCE.AutoCount.Stock.StockIssue.FormEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Document;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;
using System.Data;

namespace Production.StockIssueRM
{
    [Obsolete("Use FormStockIssueEntry.FormMREventArgs instead")]
    public class FormStockIssueEventArgs
    {
        private StockIssue myStockIssue;
        private PanelControl myHeaderPanel;
        private GridControl myGridControl;
        private XtraTabControl myTabControl;

        public PanelControl HeaderPanel
        {
            get
            {
                return this.myHeaderPanel;
            }
        }

        public GridControl GridControl
        {
            get
            {
                return this.myGridControl;
            }
        }

        public XtraTabControl TabControl
        {
            get
            {
                return this.myTabControl;
            }
        }

        public StockIssue StockIssue
        {
            get
            {
                return this.myStockIssue;
            }
        }

        public DataTable MasterTable
        {
            get
            {
                return this.myStockIssue.DataTableMaster;
            }
        }

        public DataTable DetailTable
        {
            get
            {
                return this.myStockIssue.DataTableDetail;
            }
        }

        public EditWindowMode EditWindowMode
        {
            get
            {
                if (this.myStockIssue.Action == StockIssueAction.New)
                    return EditWindowMode.New;
                else if (this.myStockIssue.Action == StockIssueAction.Edit)
                    return EditWindowMode.Edit;
                else
                    return EditWindowMode.View;
            }
        }

        public FormStockIssueEventArgs(StockIssue doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
        {
            this.myStockIssue = doc;
            this.myHeaderPanel = headerPanel;
            this.myGridControl = gridControl;
            this.myTabControl = tabControl;
        }
    }
}
