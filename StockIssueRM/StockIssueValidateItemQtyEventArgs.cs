﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueValidateItemQtyEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueValidateItemQtyEventArgs : StockIssueEventArgs
  {
    private bool myHandled;

    public bool Handled
    {
      get
      {
        return this.myHandled;
      }
      set
      {
        this.myHandled = value;
      }
    }

    internal StockIssueValidateItemQtyEventArgs(StockIssue doc)
      : base(doc)
    {
    }
  }
}
