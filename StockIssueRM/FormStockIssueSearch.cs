﻿using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.CommonAccounting;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.XtraUtils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Production.StockIssueRM
{
    public class FormStockIssueSearch : XtraForm
    {
        private IContainer components;
        private StockIssueCommand myStockIssueCmd;
        private DataTable mySearchDataTable;
        private long[] mySelectedDocKeys;
        private string mySelectedDocNos;
        private bool myInSearch;
        private AdvancedStockIssueCriteria myCriteria;
        private bool myMultiSelect;
        private DBSetting myDBSetting;
        private ScriptObject myScriptObject;
        private PanelControl panel1;
        private Label labelKeyWord;
        private SimpleButton sbtnClear;
        private SimpleButton sbtnSearch;
        private TextEdit textEditKeyWord;
        private GroupControl groupBox1;
        private CheckEdit checkEditDescription;
        private CheckEdit checkEditDocDate;
        private CheckEdit checkEditRefDocNo;
        private PanelControl panel3;
        private SimpleButton btnCancel;
        private SimpleButton btnOK;
        private SimpleButton btnAdvSearch;
        private UCSearchResult ucSearchResult1;
        private CheckEdit checkEditSINo;
        private StockIssueGrid stockIssueGrid1;

        public DataTable SearchDataTable
        {
            get
            {
                return this.mySearchDataTable;
            }
        }

        public long[] SelectedDocKeys
        {
            get
            {
                return this.mySelectedDocKeys;
            }
        }

        public string SelectedDocNos
        {
            get
            {
                return this.mySelectedDocNos;
            }
        }

        public FormStockIssueSearch(StockIssueCommand stockIssueCmd, DBSetting dbSetting, string caption, bool multiSelect)
        {
            this.InitializeComponent();
            this.Icon = BCE.AutoCount.Application.Icon;
            this.myInSearch = false;
            this.mySearchDataTable = new DataTable();
            this.myStockIssueCmd = stockIssueCmd;
            this.myDBSetting = dbSetting;
            this.myMultiSelect = multiSelect;
            this.myScriptObject = ScriptManager.CreateObject(this.myDBSetting, "StockIssueSearch");
            StockIssueUICriteria stockIssueUiCriteria = (StockIssueUICriteria)PersistenceUtil.LoadUserSetting("StockIssueCmd.setting");
            if (stockIssueUiCriteria != null)
            {
                this.checkEditDescription.Checked = stockIssueUiCriteria.Description;
                this.checkEditSINo.Checked = stockIssueUiCriteria.DocNo;
                this.checkEditDocDate.Checked = stockIssueUiCriteria.DocDate;
                this.checkEditRefDocNo.Checked = stockIssueUiCriteria.RefDocNo;
            }
            this.stockIssueGrid1.Initialize(this.myDBSetting);
            this.stockIssueGrid1.GridView.DoubleClick += new EventHandler(this.gridViewStockIssue_DoubleClick);
            this.Tag = (object)EnterKeyMessageFilter.NoFilter;
            this.ucSearchResult1.Initialize(this.stockIssueGrid1.GridView, "Delete");
            this.stockIssueGrid1.DataSource = (object)this.mySearchDataTable;
            this.ucSearchResult1.ShowButtons = this.myMultiSelect;
            this.stockIssueGrid1.GridView.Columns.ColumnByName("colTick").VisibleIndex = this.myMultiSelect ? 0 : -1;
            if (!this.myMultiSelect)
                this.stockIssueGrid1.GridView.Columns.ColumnByName("colTick").OptionsColumn.ShowInCustomizationForm = false;
            this.Text = caption;
            CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.stockIssueGrid1.GridView, new EventHandler(this.ReloadAllColumns));
            this.FormInitialize();
        }

        private void FormInitialize()
        {
            FormStockIssueSearch.FormInitializeEventArgs initializeEventArgs1 = new FormStockIssueSearch.FormInitializeEventArgs(this);
            ScriptObject scriptObject = this.myScriptObject;
            string name = "OnFormInitialize";
            System.Type[] types = new System.Type[1];
            int index1 = 0;
            System.Type type = initializeEventArgs1.GetType();
            types[index1] = type;
            object[] objArray = new object[1];
            int index2 = 0;
            FormStockIssueSearch.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
            objArray[index2] = (object)initializeEventArgs2;
            scriptObject.RunMethod(name, types, objArray);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!this.myMultiSelect)
            {
                if (this.stockIssueGrid1.GridView.FocusedRowHandle >= 0)
                {
                    DataRow dataRow = this.stockIssueGrid1.GridView.GetDataRow(this.stockIssueGrid1.GridView.FocusedRowHandle);
                    this.mySelectedDocKeys = new long[1];
                    this.mySelectedDocKeys[0] = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
                }
                else
                {
                    AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.ShowMessage_StockIssueNotSelected, new object[0]));
                    this.DialogResult = DialogResult.None;
                    this.textEditKeyWord.Focus();
                }
            }
            else
            {
                BCE.XtraUtils.GridViewUtils.UpdateData(this.stockIssueGrid1.GridView);
                DataRow[] dataRowArray = this.mySearchDataTable.Select("Delete = True");
                if (dataRowArray.Length == 0)
                {
                    AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockIssueStringId.ShowMessage_StockIssueNotSelected, new object[0]));
                    this.DialogResult = DialogResult.None;
                }
                else
                {
                    int num = 0;
                    this.mySelectedDocKeys = new long[dataRowArray.Length];
                    this.mySelectedDocNos = "";
                    foreach (DataRow dataRow in dataRowArray)
                    {
                        this.mySelectedDocKeys[num++] = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
                        if (this.mySelectedDocNos.Length != 0)
                            this.mySelectedDocNos = this.mySelectedDocNos + ", ";
                        this.mySelectedDocNos = this.mySelectedDocNos + "'" + dataRow["DocNo"].ToString() + "'";
                    }
                }
            }
        }

        public void DeleteDocKeys(long[] docKeys)
        {
            foreach (long num in docKeys)
            {
                DataRow dataRow = this.mySearchDataTable.Rows.Find((object)num);
                if (dataRow != null)
                    dataRow.Delete();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SearchMaster(SearchCriteria criteria, string columnSQL)
        {
            if (!this.myInSearch)
            {
                this.myInSearch = true;
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    BCE.XtraUtils.GridViewUtils.UpdateData(this.stockIssueGrid1.GridView);
                    this.stockIssueGrid1.DataSource = (object)null;
                    this.myStockIssueCmd.SearchMaster(criteria, columnSQL, this.mySearchDataTable, "Delete");
                }
                catch (DataAccessException ex)
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                    return;
                }
                finally
                {
                    this.stockIssueGrid1.DataSource = (object)this.mySearchDataTable;
                    Cursor.Current = current;
                    this.myInSearch = false;
                }
                if (this.myMultiSelect && this.mySearchDataTable.Columns.IndexOf("Delete") < 0)
                    this.mySearchDataTable.Columns.Add(new DataColumn()
                    {
                        DataType = typeof(bool),
                        AllowDBNull = true,
                        Caption = "Delete",
                        ColumnName = "Delete",
                        DefaultValue = (object)false
                    });
                FormStockIssueSearch.FormLoadDataEventArgs loadDataEventArgs1 = new FormStockIssueSearch.FormLoadDataEventArgs(this, this.mySearchDataTable);
                ScriptObject scriptObject = this.myScriptObject;
                string name = "OnFormLoadData";
                System.Type[] types = new System.Type[1];
                int index1 = 0;
                System.Type type = loadDataEventArgs1.GetType();
                types[index1] = type;
                object[] objArray = new object[1];
                int index2 = 0;
                FormStockIssueSearch.FormLoadDataEventArgs loadDataEventArgs2 = loadDataEventArgs1;
                objArray[index2] = (object)loadDataEventArgs2;
                scriptObject.RunMethod(name, types, objArray);
            }
        }

        private void Search(bool allColumns)
        {
            if (!this.myInSearch)
            {
                StockIssueCriteria stockIssueCriteria = new StockIssueCriteria();
                stockIssueCriteria.MatchAll = false;
                stockIssueCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
                if (this.checkEditDescription.Checked)
                    stockIssueCriteria.Description = this.textEditKeyWord.Text;
                if (this.checkEditSINo.Checked)
                    stockIssueCriteria.DocNo = this.textEditKeyWord.Text;
                if (this.checkEditDocDate.Checked)
                    stockIssueCriteria.DocDate = this.textEditKeyWord.Text;
                if (this.checkEditRefDocNo.Checked)
                    stockIssueCriteria.RefDocNo = this.textEditKeyWord.Text;
                this.SearchMaster((SearchCriteria)stockIssueCriteria, this.stockIssueGrid1.BuildSQLColumns(allColumns));
            }
        }

        private void sbtnSearch_Click(object sender, EventArgs e)
        {
            this.Search(false);
        }

        private void btnAdvSearch_Click(object sender, EventArgs e)
        {
            if (this.myCriteria == null)
                this.myCriteria = new AdvancedStockIssueCriteria(this.myDBSetting);
            this.myCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
            using (FormAdvancedSearch formAdvancedSearch = new FormAdvancedSearch((SearchCriteria)this.myCriteria, this.myDBSetting))
            {
                if (formAdvancedSearch.ShowDialog((IWin32Window)this) == DialogResult.OK)
                {
                    this.ucSearchResult1.KeepSearchResult = this.myCriteria.KeepSearchResult;
                    this.SearchMaster((SearchCriteria)this.myCriteria, this.stockIssueGrid1.BuildSQLColumns(false));
                }
            }
        }

        private void sbtnClear_Click(object sender, EventArgs e)
        {
            this.textEditKeyWord.Text = string.Empty;
            this.checkEditDescription.Checked = true;
            this.checkEditSINo.Checked = true;
            this.checkEditDocDate.Checked = true;
            this.checkEditRefDocNo.Checked = false;
        }

        private void gridViewStockIssue_DoubleClick(object sender, EventArgs e)
        {
            this.btnOK.PerformClick();
        }

        private void ReloadAllColumns(object sender, EventArgs e)
        {
            this.Search(true);
        }

        private void FormStockIssueList_Activated(object sender, EventArgs e)
        {
        }

        private void stockIssueGrid_DrawGroupPanelEvent(object sender, CustomDrawEventArgs e)
        {
            CommonGridViewHelper.DrawGroupPanelString(sender, e, this.myDBSetting);
        }

        protected override void Dispose(bool disposing)
        {
            StockIssueUICriteria stockIssueUiCriteria = new StockIssueUICriteria();
            int num1 = this.checkEditDescription.Checked ? 1 : 0;
            stockIssueUiCriteria.Description = num1 != 0;
            int num2 = this.checkEditSINo.Checked ? 1 : 0;
            stockIssueUiCriteria.DocNo = num2 != 0;
            int num3 = this.checkEditDocDate.Checked ? 1 : 0;
            stockIssueUiCriteria.DocDate = num3 != 0;
            int num4 = this.checkEditRefDocNo.Checked ? 1 : 0;
            stockIssueUiCriteria.RefDocNo = num4 != 0;
            string fileName = "StockIssueCmd.setting";
            PersistenceUtil.SaveUserSetting((object)stockIssueUiCriteria, fileName);
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FormStockIssueSearch));
            this.panel1 = new PanelControl();
            this.btnAdvSearch = new SimpleButton();
            this.groupBox1 = new GroupControl();
            this.checkEditSINo = new CheckEdit();
            this.checkEditDescription = new CheckEdit();
            this.checkEditDocDate = new CheckEdit();
            this.checkEditRefDocNo = new CheckEdit();
            this.sbtnClear = new SimpleButton();
            this.sbtnSearch = new SimpleButton();
            this.textEditKeyWord = new TextEdit();
            this.labelKeyWord = new Label();
            this.panel3 = new PanelControl();
            this.btnCancel = new SimpleButton();
            this.btnOK = new SimpleButton();
            this.ucSearchResult1 = new UCSearchResult();
            this.stockIssueGrid1 = new StockIssueGrid();
            this.panel1.BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.BeginInit();
            this.groupBox1.SuspendLayout();
            this.checkEditSINo.Properties.BeginInit();
            this.checkEditDescription.Properties.BeginInit();
            this.checkEditDocDate.Properties.BeginInit();
            this.checkEditRefDocNo.Properties.BeginInit();
            this.textEditKeyWord.Properties.BeginInit();
            this.panel3.BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            componentResourceManager.ApplyResources((object)this.panel1, "panel1");
            this.panel1.BorderStyle = BorderStyles.NoBorder;
            this.panel1.Controls.Add((Control)this.btnAdvSearch);
            this.panel1.Controls.Add((Control)this.groupBox1);
            this.panel1.Controls.Add((Control)this.sbtnClear);
            this.panel1.Controls.Add((Control)this.sbtnSearch);
            this.panel1.Controls.Add((Control)this.textEditKeyWord);
            this.panel1.Controls.Add((Control)this.labelKeyWord);
            this.panel1.Name = "panel1";
            componentResourceManager.ApplyResources((object)this.btnAdvSearch, "btnAdvSearch");
            this.btnAdvSearch.Name = "btnAdvSearch";
            this.btnAdvSearch.Click += new EventHandler(this.btnAdvSearch_Click);
            componentResourceManager.ApplyResources((object)this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add((Control)this.checkEditSINo);
            this.groupBox1.Controls.Add((Control)this.checkEditDescription);
            this.groupBox1.Controls.Add((Control)this.checkEditDocDate);
            this.groupBox1.Controls.Add((Control)this.checkEditRefDocNo);
            this.groupBox1.Name = "groupBox1";
            componentResourceManager.ApplyResources((object)this.checkEditSINo, "checkEditSINo");
            this.checkEditSINo.Name = "checkEditSINo";
            this.checkEditSINo.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditSINo.Properties.AccessibleDescription");
            this.checkEditSINo.Properties.AccessibleName = componentResourceManager.GetString("checkEditSINo.Properties.AccessibleName");
            this.checkEditSINo.Properties.AutoHeight = (bool)componentResourceManager.GetObject("checkEditSINo.Properties.AutoHeight");
            this.checkEditSINo.Properties.Caption = componentResourceManager.GetString("checkEditSINo.Properties.Caption");
            this.checkEditSINo.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditSINo.Properties.DisplayValueChecked");
            this.checkEditSINo.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditSINo.Properties.DisplayValueGrayed");
            this.checkEditSINo.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditSINo.Properties.DisplayValueUnchecked");
            componentResourceManager.ApplyResources((object)this.checkEditDescription, "checkEditDescription");
            this.checkEditDescription.Name = "checkEditDescription";
            this.checkEditDescription.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditDescription.Properties.AccessibleDescription");
            this.checkEditDescription.Properties.AccessibleName = componentResourceManager.GetString("checkEditDescription.Properties.AccessibleName");
            this.checkEditDescription.Properties.AutoHeight = (bool)componentResourceManager.GetObject("checkEditDescription.Properties.AutoHeight");
            this.checkEditDescription.Properties.Caption = componentResourceManager.GetString("checkEditDescription.Properties.Caption");
            this.checkEditDescription.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditDescription.Properties.DisplayValueChecked");
            this.checkEditDescription.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditDescription.Properties.DisplayValueGrayed");
            this.checkEditDescription.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditDescription.Properties.DisplayValueUnchecked");
            componentResourceManager.ApplyResources((object)this.checkEditDocDate, "checkEditDocDate");
            this.checkEditDocDate.Name = "checkEditDocDate";
            this.checkEditDocDate.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditDocDate.Properties.AccessibleDescription");
            this.checkEditDocDate.Properties.AccessibleName = componentResourceManager.GetString("checkEditDocDate.Properties.AccessibleName");
            this.checkEditDocDate.Properties.AutoHeight = (bool)componentResourceManager.GetObject("checkEditDocDate.Properties.AutoHeight");
            this.checkEditDocDate.Properties.Caption = componentResourceManager.GetString("checkEditDocDate.Properties.Caption");
            this.checkEditDocDate.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditDocDate.Properties.DisplayValueChecked");
            this.checkEditDocDate.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditDocDate.Properties.DisplayValueGrayed");
            this.checkEditDocDate.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditDocDate.Properties.DisplayValueUnchecked");
            componentResourceManager.ApplyResources((object)this.checkEditRefDocNo, "checkEditRefDocNo");
            this.checkEditRefDocNo.Name = "checkEditRefDocNo";
            this.checkEditRefDocNo.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditRefDocNo.Properties.AccessibleDescription");
            this.checkEditRefDocNo.Properties.AccessibleName = componentResourceManager.GetString("checkEditRefDocNo.Properties.AccessibleName");
            this.checkEditRefDocNo.Properties.AutoHeight = (bool)componentResourceManager.GetObject("checkEditRefDocNo.Properties.AutoHeight");
            this.checkEditRefDocNo.Properties.Caption = componentResourceManager.GetString("checkEditRefDocNo.Properties.Caption");
            this.checkEditRefDocNo.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditRefDocNo.Properties.DisplayValueChecked");
            this.checkEditRefDocNo.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditRefDocNo.Properties.DisplayValueGrayed");
            this.checkEditRefDocNo.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditRefDocNo.Properties.DisplayValueUnchecked");
            componentResourceManager.ApplyResources((object)this.sbtnClear, "sbtnClear");
            this.sbtnClear.Name = "sbtnClear";
            this.sbtnClear.Click += new EventHandler(this.sbtnClear_Click);
            componentResourceManager.ApplyResources((object)this.sbtnSearch, "sbtnSearch");
            this.sbtnSearch.Name = "sbtnSearch";
            this.sbtnSearch.Click += new EventHandler(this.sbtnSearch_Click);
            componentResourceManager.ApplyResources((object)this.textEditKeyWord, "textEditKeyWord");
            this.textEditKeyWord.Name = "textEditKeyWord";
            this.textEditKeyWord.Properties.AccessibleDescription = componentResourceManager.GetString("textEditKeyWord.Properties.AccessibleDescription");
            this.textEditKeyWord.Properties.AccessibleName = componentResourceManager.GetString("textEditKeyWord.Properties.AccessibleName");
            this.textEditKeyWord.Properties.AutoHeight = (bool)componentResourceManager.GetObject("textEditKeyWord.Properties.AutoHeight");
            this.textEditKeyWord.Properties.Mask.AutoComplete = (AutoCompleteType)componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.AutoComplete");
            this.textEditKeyWord.Properties.Mask.BeepOnError = (bool)componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.BeepOnError");
            this.textEditKeyWord.Properties.Mask.EditMask = componentResourceManager.GetString("textEditKeyWord.Properties.Mask.EditMask");
            this.textEditKeyWord.Properties.Mask.IgnoreMaskBlank = (bool)componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.IgnoreMaskBlank");
            this.textEditKeyWord.Properties.Mask.MaskType = (MaskType)componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.MaskType");
            this.textEditKeyWord.Properties.Mask.PlaceHolder = (char)componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.PlaceHolder");
            this.textEditKeyWord.Properties.Mask.SaveLiteral = (bool)componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.SaveLiteral");
            this.textEditKeyWord.Properties.Mask.ShowPlaceHolders = (bool)componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.ShowPlaceHolders");
            this.textEditKeyWord.Properties.Mask.UseMaskAsDisplayFormat = (bool)componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.UseMaskAsDisplayFormat");
            this.textEditKeyWord.Properties.NullValuePrompt = componentResourceManager.GetString("textEditKeyWord.Properties.NullValuePrompt");
            this.textEditKeyWord.Properties.NullValuePromptShowForEmptyValue = (bool)componentResourceManager.GetObject("textEditKeyWord.Properties.NullValuePromptShowForEmptyValue");
            componentResourceManager.ApplyResources((object)this.labelKeyWord, "labelKeyWord");
            this.labelKeyWord.Name = "labelKeyWord";
            componentResourceManager.ApplyResources((object)this.panel3, "panel3");
            this.panel3.BorderStyle = BorderStyles.NoBorder;
            this.panel3.Controls.Add((Control)this.btnCancel);
            this.panel3.Controls.Add((Control)this.btnOK);
            this.panel3.Name = "panel3";
            componentResourceManager.ApplyResources((object)this.btnCancel, "btnCancel");
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            componentResourceManager.ApplyResources((object)this.btnOK, "btnOK");
            this.btnOK.DialogResult = DialogResult.OK;
            this.btnOK.Name = "btnOK";
            this.btnOK.Click += new EventHandler(this.btnOK_Click);
            componentResourceManager.ApplyResources((object)this.ucSearchResult1, "ucSearchResult1");
            this.ucSearchResult1.Appearance.BackColor = (Color)componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor");
            this.ucSearchResult1.Appearance.BackColor2 = (Color)componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor2");
            this.ucSearchResult1.Appearance.GradientMode = (LinearGradientMode)componentResourceManager.GetObject("ucSearchResult1.Appearance.GradientMode");
            this.ucSearchResult1.Appearance.Image = (Image)componentResourceManager.GetObject("ucSearchResult1.Appearance.Image");
            this.ucSearchResult1.Appearance.Options.UseBackColor = true;
            this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ucSearchResult1.Name = "ucSearchResult1";
            componentResourceManager.ApplyResources((object)this.stockIssueGrid1, "stockIssueGrid1");
            this.stockIssueGrid1.Name = "stockIssueGrid1";
            this.stockIssueGrid1.ShowTickColumn = true;
            this.stockIssueGrid1.DrawGroupPanelEvent2 += new CustomDrawEventHandler(this.stockIssueGrid_DrawGroupPanelEvent);
            this.stockIssueGrid1.DoubleClick += new EventHandler(this.gridViewStockIssue_DoubleClick);
            this.AcceptButton = (IButtonControl)this.sbtnSearch;
            componentResourceManager.ApplyResources((object)this, "$this");
            this.AutoScaleMode = AutoScaleMode.Dpi;
            this.CancelButton = (IButtonControl)this.btnCancel;
            this.Controls.Add((Control)this.stockIssueGrid1);
            this.Controls.Add((Control)this.ucSearchResult1);
            this.Controls.Add((Control)this.panel3);
            this.Controls.Add((Control)this.panel1);
            this.Name = "FormStockIssueSearch";
            this.ShowInTaskbar = false;
            this.Activated += new EventHandler(this.FormStockIssueList_Activated);
            this.panel1.EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.EndInit();
            this.groupBox1.ResumeLayout(false);
            this.checkEditSINo.Properties.EndInit();
            this.checkEditDescription.Properties.EndInit();
            this.checkEditDocDate.Properties.EndInit();
            this.checkEditRefDocNo.Properties.EndInit();
            this.textEditKeyWord.Properties.EndInit();
            this.panel3.EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        public class FormEventArgs
        {
            private FormStockIssueSearch myForm;

            public PanelControl PanelFilter
            {
                get
                {
                    return this.myForm.panel1;
                }
            }

            public PanelControl PanelButton
            {
                get
                {
                    return this.myForm.panel3;
                }
            }

            public GridControl GridControl
            {
                get
                {
                    return this.myForm.stockIssueGrid1.GridControl;
                }
            }

            public FormStockIssueSearch Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public DBSetting DBSetting
            {
                get
                {
                    return this.myForm.myDBSetting;
                }
            }

            public FormEventArgs(FormStockIssueSearch form)
            {
                this.myForm = form;
            }
        }

        public class FormInitializeEventArgs : FormStockIssueSearch.FormEventArgs
        {
            public FormInitializeEventArgs(FormStockIssueSearch form)
              : base(form)
            {
            }
        }

        public class FormLoadDataEventArgs : FormStockIssueSearch.FormEventArgs
        {
            private DataTable myGridDataTable;

            public DataTable GridDataTable
            {
                get
                {
                    return this.myGridDataTable;
                }
            }

            public FormLoadDataEventArgs(FormStockIssueSearch form, DataTable gridDataTable)
              : base(form)
            {
                this.myGridDataTable = gridDataTable;
            }
        }
    }

}
