﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueReportingCriteria
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Localization;
using System;
using System.Collections;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueReportingCriteria : StockIssueReportCommonCriteria
  {
    protected ListingSortByOption mySortBy = ListingSortByOption.Date;
    protected ListingGroupByOption myGroupBy;

    public ListingGroupByOption GroupBy
    {
      get
      {
        return this.myGroupBy;
      }
      set
      {
        this.myGroupBy = value;
      }
    }

    public ListingSortByOption SortBy
    {
      get
      {
        return this.mySortBy;
      }
      set
      {
        this.mySortBy = value;
      }
    }

    protected override ArrayList GetFilterOptionsText()
    {
      ArrayList filterOptionsText = base.GetFilterOptionsText();
      filterOptionsText.Add((object) this.GetCancelledDocumentOptionText(this.myIsPrintCancelled));
      if (filterOptionsText.Count == 0)
        filterOptionsText.Add((object) Localizer.GetString((Enum) ReportCriteriaStringId.NoFilter, new object[0]));
      return filterOptionsText;
    }

    protected override ArrayList GetReportOptionsText()
    {
      ArrayList reportOptionsText = base.GetReportOptionsText();
      reportOptionsText.Add((object) this.GetSortByText(Localizer.GetString((Enum) this.mySortBy, new object[0])));
      reportOptionsText.Add((object) this.GetGroupByText(Localizer.GetString((Enum) this.myGroupBy, new object[0])));
      return reportOptionsText;
    }
  }
}
