﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueEventArgs
  {
    private StockIssue myStock;

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.DBSetting;
      }
    }

    public StockIssueRecord MasterRecord
    {
      get
      {
        return new StockIssueRecord(this.myStock);
      }
    }

    internal StockIssueEventArgs(StockIssue doc)
    {
      this.myStock = doc;
    }
  }
}
