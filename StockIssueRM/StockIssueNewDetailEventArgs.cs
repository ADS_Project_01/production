﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueNewDetailEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;
using System.Data;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueNewDetailEventArgs
  {
    private DataRow myDetailRow;
    private StockIssue myStock;

    public StockIssueRecord MasterRecord
    {
      get
      {
        return new StockIssueRecord(this.myStock);
      }
    }

    public StockIssueDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockIssueDetailRecord(this.myStock.Command.DBSetting, this.myDetailRow);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.myDBSetting;
      }
    }

    internal StockIssueNewDetailEventArgs(StockIssue doc, DataRow detailRow)
    {
      this.myStock = doc;
      this.myDetailRow = detailRow;
    }
  }
}
