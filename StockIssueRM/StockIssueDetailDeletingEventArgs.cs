﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueDetailDeletingEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;
using System.Data;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueDetailDeletingEventArgs : StockIssueNewDetailEventArgs
  {
    internal StockIssueDetailDeletingEventArgs(StockIssue doc, DataRow detailRow)
      : base(doc, detailRow)
    {
    }
  }
}
