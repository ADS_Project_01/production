﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueOnDeleteEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueOnDeleteEventArgs : StockIssueEventArgs
  {
    private DBSetting myDBSetting;

    public new DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    internal StockIssueOnDeleteEventArgs(StockIssue doc, DBSetting dbSetting)
      : base(doc)
    {
      this.myDBSetting = dbSetting;
    }
  }
}
