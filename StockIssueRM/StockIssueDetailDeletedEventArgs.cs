﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueDetailDeletedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueDetailDeletedEventArgs : StockIssueEventArgs
  {
    internal StockIssueDetailDeletedEventArgs(StockIssue doc)
      : base(doc)
    {
    }
  }
}
