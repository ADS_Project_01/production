﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueOnSaveEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueOnSaveEventArgs : StockIssueEventArgs
  {
    private DBSetting myDBSetting;

    public new DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    internal StockIssueOnSaveEventArgs(StockIssue doc, DBSetting dbSetting)
      : base(doc)
    {
      this.myDBSetting = dbSetting;
    }
  }
}
