﻿// Type: BCE.AutoCount.Stock.StockIssue.StockIssueRecalculateDetailEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;
using System.Data;

namespace Production.StockIssueRM
{
  [Serializable]
  public class StockIssueRecalculateDetailEventArgs
  {
    private DataRow myDetailRow;
    private DBSetting myDBSetting;

    public StockIssueDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockIssueDetailRecord(this.myDBSetting, this.myDetailRow);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    internal StockIssueRecalculateDetailEventArgs(DBSetting dbSetting, DataRow detailRow)
    {
      this.myDetailRow = detailRow;
      this.myDBSetting = dbSetting;
    }
  }
}
