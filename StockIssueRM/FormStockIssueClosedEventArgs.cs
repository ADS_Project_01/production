﻿// Type: BCE.AutoCount.Stock.StockIssue.FormClosedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.Stock.dll

using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;

namespace Production.StockIssueRM
{
    [Obsolete("Use FormStockIssueEntry.FormStockIssueClosedEventArgs instead")]
    public class FormStockIssueClosedEventArgs : FormStockIssueEventArgs
    {
        public FormStockIssueClosedEventArgs(StockIssue doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
            : base(doc, headerPanel, gridControl, tabControl)
        {
        }
    }
}
