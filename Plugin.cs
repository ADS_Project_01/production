using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using BCE.AutoCount.PlugIn;
using BCE.Data;
using BCE.AutoCount.Authentication;
namespace Production
{

    /// <summary>
    /// Summary description for PlugInsInitializer.
    /// </summary>
    public class Plugin : BCE.AutoCount.PlugIn.BasePlugIn
    {
        protected UserAuthentication myUserAuthentication;
        public Plugin() : base(Production.GLib.G.Globalkey, "Production PlugIns", new AssemblyInfoEntity().AssemblyVersion)
        {
            // this.myUserAuthentication = UserAuthentication.GetOrCreate(BCE.AutoCount.Application.DBSetting);
            DBSetting dbSetting = BCE.AutoCount.Application.DBSetting;
            base.SetManufacturer("");
            base.SetCopyright("Copyright 2019");

            base.SetMinimumAccountingVersionRequired("1.8.2");
            base.SetIsFreeLicense(true);
            BCE.AutoCount.PlugIn.BeforeLoadArgs e = new BCE.AutoCount.PlugIn.BeforeLoadArgs(dbSetting, base.Guid, System.Windows.Forms.Application.StartupPath, "PT. BUNGA MAS", "JL. SUNTER AGUNG UTARA BLOK M No.26", BCE.AutoCount.LicenseControl.LicenseControlType.LicenseCode, new byte[0]);
            base.BeforeLoad(e);
            //
            // TODO: Add constructor logic here
            //
        }
        public static bool IsValidDatabase(DBSetting dbSetting)
        {
            object obj = dbSetting.ExecuteScalar("select count(*) from dbo.sysobjects where id = object_id(N'[dbo].[RPA_ItemBOM]') and OBJECTPROPERTY(id, N'IsUserTable') = 1");
            if (obj != null && obj != DBNull.Value)
                return System.Convert.ToInt32(obj) > 0;
            else
                return false;
        }
        public override void AfterUnload(BCE.AutoCount.PlugIn.BaseArgs e)
        {
            BCE.AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.RPA.ToString());
            BCE.AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.RPA_GEN.ToString());
            //BCE.AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.RPA_GEN_MACHINE.ToString());
            //BCE.AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_DELETE.ToString());
            //BCE.AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_EDIT.ToString());
            //BCE.AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_NEW.ToString());
           // BCE.AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_OPEN.ToString());
           // BCE.AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_SHOW.ToString());

        }

        public override bool BeforeLoad(BCE.AutoCount.PlugIn.BeforeLoadArgs e)
        {
            //e.CompanyName="";
            if (!IsValidDatabase(e.DBSetting))
            {
                if (!BCE.Application.AppMessage.ShowConfirmMessage("Do you want to install Production plug -in to this database ?"))
                    return false;
                if (!BCE.Application.AppMessage.ShowConfirmMessage("Do you really want to install Production plug -in to this database ?\n(Note: Once installed, you cannot remove it.)"))
                    return false;
                BCE.Data.DBUtils dbUtils = BCE.Data.DBUtils.Create(e.DBSetting);
                
                dbUtils.ExecuteDDL(Path.Combine(e.PlugInFolder, "Production.SQ$"));
            }
            
            e.MainMenuCaption = "Production";
            //e.AccessRightFilename = "Production.acd";
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
    new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA.ToString(), "RPA System", "PlugIns Production", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
    new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN.ToString(), AccessRightConst.RPA.ToString(), "General Maintenance", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
  new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_SETTINGS.ToString(), AccessRightConst.RPA_GEN.ToString(), "General Settings", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_SETTINGS_SHOW.ToString(), AccessRightConst.RPA_SETTINGS.ToString(), "Show General Settings", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_SETTINGS_OPEN.ToString(), AccessRightConst.RPA_SETTINGS.ToString(), "Open General Settings", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
   new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_TOOLS.ToString(), AccessRightConst.RPA.ToString(), "Tools", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_TOOLS_RECALCULATE.ToString(), AccessRightConst.RPA_TOOLS.ToString(), "Recalculate Stock Costing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_TOOLS_RECALCULATE_SHOW.ToString(), AccessRightConst.RPA_TOOLS_RECALCULATE.ToString(), "Show Recalculate Stock Costing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_TOOLS_RECALCULATE_OPEN.ToString(), AccessRightConst.RPA_TOOLS_RECALCULATE.ToString(), "Open Recalculate Stock Costing", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_REPORTS.ToString(), AccessRightConst.RPA.ToString(), "Reports", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_REPORTS_STOCKCARDWIP.ToString(), AccessRightConst.RPA_REPORTS.ToString(), "Stock Card WIP Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_REPORTS_STOCKCARDWIP_SHOW.ToString(), AccessRightConst.RPA_REPORTS_STOCKCARDWIP.ToString(), "Show Stock Card WIP Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_REPORTS_STOCKCARDWIP_OPEN.ToString(), AccessRightConst.RPA_REPORTS_STOCKCARDWIP.ToString(), "Open Stock Card WIP Report", BCE.AutoCount.Controller.ModuleOptions.None));


            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
   new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ROUTING.ToString(), AccessRightConst.RPA_GEN.ToString(), "Routing Maintenance", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ROUTING_SHOW.ToString(), AccessRightConst.RPA_GEN_ROUTING.ToString(), "Show Routing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ROUTING_OPEN.ToString(), AccessRightConst.RPA_GEN_ROUTING.ToString(), "Open Routing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ROUTING_NEW.ToString(), AccessRightConst.RPA_GEN_ROUTING.ToString(), "New Routing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ROUTING_EDIT.ToString(), AccessRightConst.RPA_GEN_ROUTING.ToString(), "Edit Routing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ROUTING_VIEW.ToString(), AccessRightConst.RPA_GEN_ROUTING.ToString(), "View Routing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ROUTING_DELETE.ToString(), AccessRightConst.RPA_GEN_ROUTING.ToString(), "Delete Routing", BCE.AutoCount.Controller.ModuleOptions.None));


            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_MACHINE.ToString(), AccessRightConst.RPA_GEN.ToString(), "Machine Maintenance", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_SHOW.ToString(), AccessRightConst.RPA_GEN_MACHINE.ToString(), "Show Machine", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_OPEN.ToString(), AccessRightConst.RPA_GEN_MACHINE.ToString(), "Open Machine", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_NEW.ToString(), AccessRightConst.RPA_GEN_MACHINE.ToString(), "New Machine", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_EDIT.ToString(), AccessRightConst.RPA_GEN_MACHINE.ToString(), "Edit Machine", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_VIEW.ToString(), AccessRightConst.RPA_GEN_MACHINE.ToString(), "View Machine", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_MACHINE_DELETE.ToString(), AccessRightConst.RPA_GEN_MACHINE.ToString(), "Delete Machine", BCE.AutoCount.Controller.ModuleOptions.None));


            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_OVERHEAD.ToString(), AccessRightConst.RPA_GEN.ToString(), "Overhead Maintenance", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_OVERHEAD_SHOW.ToString(), AccessRightConst.RPA_GEN_OVERHEAD.ToString(), "Show Overhead", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_OVERHEAD_OPEN.ToString(), AccessRightConst.RPA_GEN_OVERHEAD.ToString(), "Open Overhead", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_OVERHEAD_NEW.ToString(), AccessRightConst.RPA_GEN_OVERHEAD.ToString(), "New Overhead", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_OVERHEAD_EDIT.ToString(), AccessRightConst.RPA_GEN_OVERHEAD.ToString(), "Edit Overhead", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_OVERHEAD_VIEW.ToString(), AccessRightConst.RPA_GEN_OVERHEAD.ToString(), "View Overhead", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_OVERHEAD_DELETE.ToString(), AccessRightConst.RPA_GEN_OVERHEAD.ToString(), "Delete Overhead", BCE.AutoCount.Controller.ModuleOptions.None));


            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_CUSTOMERLOC.ToString(), AccessRightConst.RPA_GEN.ToString(), "Customer Location Maintenance", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_CUSTOMERLOC_SHOW.ToString(), AccessRightConst.RPA_GEN_CUSTOMERLOC.ToString(), "Show Customer Location", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_CUSTOMERLOC_OPEN.ToString(), AccessRightConst.RPA_GEN_CUSTOMERLOC.ToString(), "Open Customer Location", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_CUSTOMERLOC_NEW.ToString(), AccessRightConst.RPA_GEN_CUSTOMERLOC.ToString(), "New Customer Location", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_CUSTOMERLOC_EDIT.ToString(), AccessRightConst.RPA_GEN_CUSTOMERLOC.ToString(), "Edit Customer Location", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_CUSTOMERLOC_VIEW.ToString(), AccessRightConst.RPA_GEN_CUSTOMERLOC.ToString(), "View Customer Location", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_CUSTOMERLOC_DELETE.ToString(), AccessRightConst.RPA_GEN_CUSTOMERLOC.ToString(), "Delete Customer Location", BCE.AutoCount.Controller.ModuleOptions.None));


            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ITEMBOM.ToString(), AccessRightConst.RPA_GEN.ToString(), "Item BOM Maintenance", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ITEMBOM_SHOW.ToString(), AccessRightConst.RPA_GEN_ITEMBOM.ToString(), "Show Item BOM", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ITEMBOM_OPEN.ToString(), AccessRightConst.RPA_GEN_ITEMBOM.ToString(), "Open Item BOM", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ITEMBOM_NEW.ToString(), AccessRightConst.RPA_GEN_ITEMBOM.ToString(), "New Item BOM", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ITEMBOM_EDIT.ToString(), AccessRightConst.RPA_GEN_ITEMBOM.ToString(), "Edit Item BOM", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ITEMBOM_VIEW.ToString(), AccessRightConst.RPA_GEN_ITEMBOM.ToString(), "View Item BOM", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ITEMBOM_DELETE.ToString(), AccessRightConst.RPA_GEN_ITEMBOM.ToString(), "Delete Item BOM", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_GEN_ITEMBOM_PRINT.ToString(), AccessRightConst.RPA_GEN_ITEMBOM.ToString(), "Can Access Item BOM Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));

            /// Access Right Work Order
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO.ToString(), AccessRightConst.RPA.ToString(), "Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_SHOW.ToString(), AccessRightConst.RPA_WO.ToString(), "Show Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_OPEN.ToString(), AccessRightConst.RPA_WO.ToString(), "Open Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_NEW.ToString(), AccessRightConst.RPA_WO.ToString(), "New Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_EDIT.ToString(), AccessRightConst.RPA_WO.ToString(), "Edit Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_VIEW.ToString(), AccessRightConst.RPA_WO.ToString(), "View Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_CLOSE.ToString(), AccessRightConst.RPA_WO.ToString(), "Close Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DELETE.ToString(), AccessRightConst.RPA_WO.ToString(), "Delete Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_CANCEL.ToString(), AccessRightConst.RPA_WO.ToString(), "Cancel/Uncancel Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DOC_REPORT.ToString(), AccessRightConst.RPA_WO.ToString(), "Stock Work Order Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DOC_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_WO_DOC_REPORT.ToString(), "Preview Stock Work Order Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DOC_REPORT_PRINT.ToString(), AccessRightConst.RPA_WO_DOC_REPORT.ToString(), "Print Stock Work Order Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DOC_REPORT_EXPORT.ToString(), AccessRightConst.RPA_WO_DOC_REPORT.ToString(), "Export Stock Work Order Report", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_PRINTED.ToString(), AccessRightConst.RPA_WO.ToString(), "Printed Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_PRINTED_CANCEL.ToString(), AccessRightConst.RPA_WO_PRINTED.ToString(), "Can Cancel/Uncancel Printed Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_PRINTED_EDIT.ToString(), AccessRightConst.RPA_WO_PRINTED.ToString(), "Can Edit Printed Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_PRINTED_DELETE.ToString(), AccessRightConst.RPA_WO_PRINTED.ToString(), "Can Delete Printed Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_PRINTED_PREVIEW.ToString(), AccessRightConst.RPA_WO_PRINTED.ToString(), "Can Preview Printed Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_PRINTED_PRINT.ToString(), AccessRightConst.RPA_WO_PRINTED.ToString(), "Can Print Printed Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
           new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_PRINTED_EXPORT.ToString(), AccessRightConst.RPA_WO_PRINTED.ToString(), "Can Export Printed Stock Work Order", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
     new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_LISTING_REPORT.ToString(), AccessRightConst.RPA_WO.ToString(), "Stock Work Order Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_LISTING_REPORT_SHOW.ToString(), AccessRightConst.RPA_WO_LISTING_REPORT.ToString(), "Show Stock Work Order Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_LISTING_REPORT_OPEN.ToString(), AccessRightConst.RPA_WO_LISTING_REPORT.ToString(), "Open Stock Work Order Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_LISTING_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_WO_LISTING_REPORT.ToString(), "Preview Stock Work Order Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_LISTING_REPORT_PRINT.ToString(), AccessRightConst.RPA_WO_LISTING_REPORT.ToString(), "Print Stock Work Order Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_LISTING_REPORT_EXPORT.ToString(), AccessRightConst.RPA_WO_LISTING_REPORT.ToString(), "Export Stock Work Order Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DTLLIST_REPORT.ToString(), AccessRightConst.RPA_WO.ToString(), "Stock Work Order Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DTLLIST_REPORT_SHOW.ToString(), AccessRightConst.RPA_WO_DTLLIST_REPORT.ToString(), "Show Stock Work Order Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DTLLIST_REPORT_OPEN.ToString(), AccessRightConst.RPA_WO_DTLLIST_REPORT.ToString(), "Open Stock Work Order Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DTLLIST_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_WO_DTLLIST_REPORT.ToString(), "Preview Stock Work Order Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_WO_DTLLIST_REPORT_PRINT.ToString(), AccessRightConst.RPA_WO_DTLLIST_REPORT.ToString(), "Print Stock Work Order Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));



            /// Access Right Stock Issue Raw Material
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM.ToString(), AccessRightConst.RPA.ToString(), "Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_SHOW.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "Show Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_OPEN.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "Open Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_NEW.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "New Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_EDIT.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "Edit Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_VIEW.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "View Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DELETE.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "Delete Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_CANCEL.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "Cancel/Uncancel Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DOC_REPORT.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "Stock Issue Raw Material Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DOC_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_ISS_RM_DOC_REPORT.ToString(), "Preview Stock Issue Raw Material Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DOC_REPORT_PRINT.ToString(), AccessRightConst.RPA_ISS_RM_DOC_REPORT.ToString(), "Print Stock Issue Raw Material Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DOC_REPORT_EXPORT.ToString(), AccessRightConst.RPA_ISS_RM_DOC_REPORT.ToString(), "Export Stock Issue Raw Material Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_LISTING_REPORT.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "Stock Stock Issue Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_LISTING_REPORT_SHOW.ToString(), AccessRightConst.RPA_ISS_RM_LISTING_REPORT.ToString(), "Show Stock Stock Issue Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_LISTING_REPORT_OPEN.ToString(), AccessRightConst.RPA_ISS_RM_LISTING_REPORT.ToString(), "Open Stock Stock Issue Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_LISTING_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_ISS_RM_LISTING_REPORT.ToString(), "Preview Stock Stock Issue Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_LISTING_REPORT_PRINT.ToString(), AccessRightConst.RPA_ISS_RM_LISTING_REPORT.ToString(), "Print Stock Stock Issue Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_LISTING_REPORT_EXPORT.ToString(), AccessRightConst.RPA_ISS_RM_LISTING_REPORT.ToString(), "Export Stock Stock Issue Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DTLLIST_REPORT.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "Stock Stock Issue Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DTLLIST_REPORT_SHOW.ToString(), AccessRightConst.RPA_ISS_RM_DTLLIST_REPORT.ToString(), "Show Stock Stock Issue Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DTLLIST_REPORT_OPEN.ToString(), AccessRightConst.RPA_ISS_RM_DTLLIST_REPORT.ToString(), "Open Stock Stock Issue Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DTLLIST_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_ISS_RM_DTLLIST_REPORT.ToString(), "Preview Stock Stock Issue Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_DTLLIST_REPORT_PRINT.ToString(), AccessRightConst.RPA_ISS_RM_DTLLIST_REPORT.ToString(), "Print Stock Stock Issue Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));


            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_PRINTED.ToString(), AccessRightConst.RPA_ISS_RM.ToString(), "Printed Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_PRINTED_CANCEL.ToString(), AccessRightConst.RPA_ISS_RM_PRINTED.ToString(), "Can Cancel/Uncancel Printed Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_PRINTED_EDIT.ToString(), AccessRightConst.RPA_ISS_RM_PRINTED.ToString(), "Can Edit Printed Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_PRINTED_DELETE.ToString(), AccessRightConst.RPA_ISS_RM_PRINTED.ToString(), "Can Delete Printed Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_PRINTED_PREVIEW.ToString(), AccessRightConst.RPA_ISS_RM_PRINTED.ToString(), "Can Preview Printed Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_PRINTED_PRINT.ToString(), AccessRightConst.RPA_ISS_RM_PRINTED.ToString(), "Can Print Printed Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
           new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ISS_RM_PRINTED_EXPORT.ToString(), AccessRightConst.RPA_ISS_RM_PRINTED.ToString(), "Can Export Printed Stock Issue Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));


            /// Access Right Stock Receive Finish Goods
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV.ToString(), AccessRightConst.RPA.ToString(), "Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_SHOW.ToString(), AccessRightConst.RPA_RCV.ToString(), "Show Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_OPEN.ToString(), AccessRightConst.RPA_RCV.ToString(), "Open Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_NEW.ToString(), AccessRightConst.RPA_RCV.ToString(), "New Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_EDIT.ToString(), AccessRightConst.RPA_RCV.ToString(), "Edit Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_VIEW.ToString(), AccessRightConst.RPA_RCV.ToString(), "View Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DELETE.ToString(), AccessRightConst.RPA_RCV.ToString(), "Delete Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_CANCEL.ToString(), AccessRightConst.RPA_RCV.ToString(), "Cancel/Uncancel Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DOC_REPORT.ToString(), AccessRightConst.RPA_RCV.ToString(), "Stock Receive Finish Goods Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DOC_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_RCV_DOC_REPORT.ToString(), "Preview Stock Receive Finish Goods Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DOC_REPORT_PRINT.ToString(), AccessRightConst.RPA_RCV_DOC_REPORT.ToString(), "Print Stock Receive Finish Goods Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DOC_REPORT_EXPORT.ToString(), AccessRightConst.RPA_RCV_DOC_REPORT.ToString(), "Export Stock Receive Finish Goods Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_LISTING_REPORT.ToString(), AccessRightConst.RPA_RCV.ToString(), "Stock Receive Finish Goods Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_LISTING_REPORT_SHOW.ToString(), AccessRightConst.RPA_RCV_LISTING_REPORT.ToString(), "Show Stock Receive Finish Goods Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_LISTING_REPORT_OPEN.ToString(), AccessRightConst.RPA_RCV_LISTING_REPORT.ToString(), "Open Stock Receive Finish Goods Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_LISTING_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_RCV_LISTING_REPORT.ToString(), "Preview Stock Receive Finish Goods Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_LISTING_REPORT_PRINT.ToString(), AccessRightConst.RPA_RCV_LISTING_REPORT.ToString(), "Print Stock Receive Finish Goods Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_LISTING_REPORT_EXPORT.ToString(), AccessRightConst.RPA_RCV_LISTING_REPORT.ToString(), "Export Stock Receive Finish Goods Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DTLLIST_REPORT.ToString(), AccessRightConst.RPA_RCV.ToString(), "Stock Receive Finish Goods Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DTLLIST_REPORT_SHOW.ToString(), AccessRightConst.RPA_RCV_DTLLIST_REPORT.ToString(), "Show Stock Receive Finish Goods Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DTLLIST_REPORT_OPEN.ToString(), AccessRightConst.RPA_RCV_DTLLIST_REPORT.ToString(), "Open Stock Receive Finish Goods Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DTLLIST_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_RCV_DTLLIST_REPORT.ToString(), "Preview Stock Receive Finish Goods Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_DTLLIST_REPORT_PRINT.ToString(), AccessRightConst.RPA_RCV_DTLLIST_REPORT.ToString(), "Print Stock Receive Finish Goods Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
           


            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_PRINTED.ToString(), AccessRightConst.RPA_RCV.ToString(), "Printed Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_PRINTED_CANCEL.ToString(), AccessRightConst.RPA_RCV_PRINTED.ToString(), "Can Cancel/Uncancel Printed Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_PRINTED_EDIT.ToString(), AccessRightConst.RPA_RCV_PRINTED.ToString(), "Can Edit Printed Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_PRINTED_DELETE.ToString(), AccessRightConst.RPA_RCV_PRINTED.ToString(), "Can Delete Printed Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_PRINTED_PREVIEW.ToString(), AccessRightConst.RPA_RCV_PRINTED.ToString(), "Can Preview Printed Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_PRINTED_PRINT.ToString(), AccessRightConst.RPA_RCV_PRINTED.ToString(), "Can Print Printed Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
           new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCV_PRINTED_EXPORT.ToString(), AccessRightConst.RPA_RCV_PRINTED.ToString(), "Can Export Printed Stock Receive Finish Goods", BCE.AutoCount.Controller.ModuleOptions.None));

            //////////////////////


            /// Access Right Stock Return Raw Material
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM.ToString(), AccessRightConst.RPA.ToString(), "Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_SHOW.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "Show Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_OPEN.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "Open Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_NEW.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "New Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_EDIT.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "Edit Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_VIEW.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "View Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DELETE.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "Delete Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_CANCEL.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "Cancel/Uncancel Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DOC_REPORT.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "Stock Return Raw Material Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DOC_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_RCVRM_DOC_REPORT.ToString(), "Preview Stock Return Raw Material Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DOC_REPORT_PRINT.ToString(), AccessRightConst.RPA_RCVRM_DOC_REPORT.ToString(), "Print Stock Return Raw Material Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DOC_REPORT_EXPORT.ToString(), AccessRightConst.RPA_RCVRM_DOC_REPORT.ToString(), "Export Stock Return Raw Material Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_LISTING_REPORT.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "Stock Return Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_LISTING_REPORT_SHOW.ToString(), AccessRightConst.RPA_RCVRM_LISTING_REPORT.ToString(), "Show Stock Return Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_LISTING_REPORT_OPEN.ToString(), AccessRightConst.RPA_RCVRM_LISTING_REPORT.ToString(), "Open Stock Return Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_LISTING_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_RCVRM_LISTING_REPORT.ToString(), "Preview Stock Return Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_LISTING_REPORT_PRINT.ToString(), AccessRightConst.RPA_RCVRM_LISTING_REPORT.ToString(), "Print Stock Return Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_LISTING_REPORT_EXPORT.ToString(), AccessRightConst.RPA_RCVRM_LISTING_REPORT.ToString(), "Export Stock Return Raw Material Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DTLLIST_REPORT.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "Stock Return Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DTLLIST_REPORT_SHOW.ToString(), AccessRightConst.RPA_RCVRM_DTLLIST_REPORT.ToString(), "Show Stock Return Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DTLLIST_REPORT_OPEN.ToString(), AccessRightConst.RPA_RCVRM_DTLLIST_REPORT.ToString(), "Open Stock Return Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DTLLIST_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_RCVRM_DTLLIST_REPORT.ToString(), "Preview Stock Return Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_DTLLIST_REPORT_PRINT.ToString(), AccessRightConst.RPA_RCVRM_DTLLIST_REPORT.ToString(), "Print Stock Return Raw Material Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));



            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_PRINTED.ToString(), AccessRightConst.RPA_RCVRM.ToString(), "Printed Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_PRINTED_CANCEL.ToString(), AccessRightConst.RPA_RCVRM_PRINTED.ToString(), "Can Cancel/Uncancel Printed Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_PRINTED_EDIT.ToString(), AccessRightConst.RPA_RCVRM_PRINTED.ToString(), "Can Edit Printed Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_PRINTED_DELETE.ToString(), AccessRightConst.RPA_RCVRM_PRINTED.ToString(), "Can Delete Printed Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_PRINTED_PREVIEW.ToString(), AccessRightConst.RPA_RCVRM_PRINTED.ToString(), "Can Preview Printed Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_PRINTED_PRINT.ToString(), AccessRightConst.RPA_RCVRM_PRINTED.ToString(), "Can Print Printed Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
           new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVRM_PRINTED_EXPORT.ToString(), AccessRightConst.RPA_RCVRM_PRINTED.ToString(), "Can Export Printed Stock Return Raw Material", BCE.AutoCount.Controller.ModuleOptions.None));

            //////////////////////

            /// Access Right Stock Receive After Frozen
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR.ToString(), AccessRightConst.RPA.ToString(), "Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_SHOW.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "Show Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_OPEN.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "Open Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_NEW.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "New Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_EDIT.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "Edit Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_VIEW.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "View Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DELETE.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "Delete Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_CANCEL.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "Cancel/Uncancel Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DOC_REPORT.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "Stock Receive After Frozen Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DOC_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_RCVFR_DOC_REPORT.ToString(), "Preview Stock Receive After Frozen Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DOC_REPORT_PRINT.ToString(), AccessRightConst.RPA_RCVFR_DOC_REPORT.ToString(), "Print Stock Receive After Frozen Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DOC_REPORT_EXPORT.ToString(), AccessRightConst.RPA_RCVFR_DOC_REPORT.ToString(), "Export Stock Receive After Frozen Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_LISTING_REPORT.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "Stock Receive After Frozen Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_LISTING_REPORT_SHOW.ToString(), AccessRightConst.RPA_RCVFR_LISTING_REPORT.ToString(), "Show Stock Receive After Frozen Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_LISTING_REPORT_OPEN.ToString(), AccessRightConst.RPA_RCVFR_LISTING_REPORT.ToString(), "Open Stock Receive After Frozen Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_LISTING_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_RCVFR_LISTING_REPORT.ToString(), "Preview Stock Receive After Frozen Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_LISTING_REPORT_PRINT.ToString(), AccessRightConst.RPA_RCVFR_LISTING_REPORT.ToString(), "Print Stock Receive After Frozen Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_LISTING_REPORT_EXPORT.ToString(), AccessRightConst.RPA_RCVFR_LISTING_REPORT.ToString(), "Export Stock Receive After Frozen Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DTLLIST_REPORT.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "Stock Receive After Frozen Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DTLLIST_REPORT_SHOW.ToString(), AccessRightConst.RPA_RCVFR_DTLLIST_REPORT.ToString(), "Show Stock Receive After Frozen Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DTLLIST_REPORT_OPEN.ToString(), AccessRightConst.RPA_RCVFR_DTLLIST_REPORT.ToString(), "Open Stock Receive After Frozen Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DTLLIST_REPORT_PREVIEW.ToString(), AccessRightConst.RPA_RCVFR_DTLLIST_REPORT.ToString(), "Preview Stock Receive After Frozen Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
          new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_DTLLIST_REPORT_PRINT.ToString(), AccessRightConst.RPA_RCVFR_DTLLIST_REPORT.ToString(), "Print Stock Receive After Frozen Detail Listing Report", BCE.AutoCount.Controller.ModuleOptions.None));
            

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_PRINTED.ToString(), AccessRightConst.RPA_RCVFR.ToString(), "Printed Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_PRINTED_CANCEL.ToString(), AccessRightConst.RPA_RCVFR_PRINTED.ToString(), "Can Cancel/Uncancel Printed Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_PRINTED_EDIT.ToString(), AccessRightConst.RPA_RCVFR_PRINTED.ToString(), "Can Edit Printed Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_PRINTED_DELETE.ToString(), AccessRightConst.RPA_RCVFR_PRINTED.ToString(), "Can Delete Printed Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_PRINTED_PREVIEW.ToString(), AccessRightConst.RPA_RCVFR_PRINTED.ToString(), "Can Preview Printed Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_PRINTED_PRINT.ToString(), AccessRightConst.RPA_RCVFR_PRINTED.ToString(), "Can Print Printed Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
           new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_RCVFR_PRINTED_EXPORT.ToString(), AccessRightConst.RPA_RCVFR_PRINTED.ToString(), "Can Export Printed Stock Receive After Frozen", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_PRODUCTION_LISTING.ToString(), AccessRightConst.RPA_REPORTS.ToString(), "Production Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_PRODUCTION_LISTING_SHOW.ToString(), AccessRightConst.RPA_PRODUCTION_LISTING.ToString(), "Show Production Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_PRODUCTION_LISTING_OPEN.ToString(), AccessRightConst.RPA_PRODUCTION_LISTING.ToString(), "Open Production Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_PRODUCTION_LISTING_PREVIEW.ToString(), AccessRightConst.RPA_PRODUCTION_LISTING.ToString(), "Preview Production Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_PRODUCTION_LISTING_PRINT.ToString(), AccessRightConst.RPA_PRODUCTION_LISTING.ToString(), "Print Production Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));

            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ITEMWASTE_LISTING.ToString(), AccessRightConst.RPA_REPORTS.ToString(), "Item Waste Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ITEMWASTE_LISTING_SHOW.ToString(), AccessRightConst.RPA_ITEMWASTE_LISTING.ToString(), "Show Item Waste Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ITEMWASTE_LISTING_OPEN.ToString(), AccessRightConst.RPA_ITEMWASTE_LISTING.ToString(), "Open Item Waste Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ITEMWASTE_LISTING_PREVIEW.ToString(), AccessRightConst.RPA_ITEMWASTE_LISTING.ToString(), "Preview Item Waste Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));
            BCE.AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new BCE.AutoCount.Authentication.AccessRightRecord(AccessRightConst.RPA_ITEMWASTE_LISTING_PRINT.ToString(), AccessRightConst.RPA_ITEMWASTE_LISTING.ToString(), "Print Item Waste Print Listing", BCE.AutoCount.Controller.ModuleOptions.None));



            //////////////////////
            // BCE.AutoCount.Report.AutoCountReport.RegisterReportType(
            //FormDealerListing.ReportType, typeof(DealerListingReportTypeHandler));
            e.SystemReportFilename = "Production.dat";
            
            base.BeforeLoad(e);
            return true;
        }
        //Guid myGuid = Guid.NewGuid();
        //[BCE.AutoCount.PlugIn.BasePlugIn(Guid.NewGuid,"","")]
        //BCE.AutoCount.PlugIn.BasePlugInAttribute(myGuid,"RPA System","1.0.0");
        ////[BCE.AutoCount.PlugIns.PlugInsInitializer("AutoCount Accounting Sample Plug-Ins", "Sample")]
        //public static bool Initialize(BCE.AutoCount.PlugIn.BeforeLoadArgs e)
        //{
        //    //e.DBSetting = BCE.AutoCount.Application.DBSetting;
        //    if (!IsValidDatabase(e.DBSetting))
        //    {
        //        if (!BCE.Application.AppMessage.ShowConfirmMessage("Do you want to install RPA System to this database?"))
        //            return false;

        //        if (!BCE.Application.AppMessage.ShowConfirmMessage("Do you really want to install RPA System to this database?\n(Note: Once installed, you cannot remove it.)"))
        //            return false;

        //        BCE.Data.DBUtils dbUtils = BCE.Data.DBUtils.Create(e.DBSetting);

        //        dbUtils.ExecuteDDL(Path.Combine(e.PlugInFolder, "Production.SQ$"));
        //    }

        //    e.MainMenuCaption = "RPA System";
        //    e.AccessRightFilename = "Production.acd";

        //    //BCE.AutoCount.Report.AutoCountReport.RegisterReportType(FormDealerListing.ReportType, typeof(DealerListingReportTypeHandler));

        //    return true;
        //}
        public override void AfterLoad(BCE.AutoCount.PlugIn.BaseArgs e)
        {
           
         //   BCE.Application.AppMessage.ShowMessage(string.Format("Account book {0}\nhas loaded RPA System Plugin", e.CompanyName));
          //  base.AfterLoad(e);
        }
      
    }
}
