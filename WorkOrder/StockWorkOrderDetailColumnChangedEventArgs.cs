﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderDetailColumnChangedEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using System.Data;

namespace Production.WorkOrder
{
  public class StockWorkOrderDetailColumnChangedEventArgs
  {
    private string myColumnName;
    private DataRow myDetailRow;
    private StockWorkOrder myStock;

    public string ChangedColumnName
    {
      get
      {
        return this.myColumnName;
      }
    }

    public StockWorkOrderRecord MasterRecord
    {
      get
      {
        return new StockWorkOrderRecord(this.myStock);
      }
    }

    public StockWorkOrderDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockWorkOrderDetailRecord(this.myStock.Command.DBSetting, this.myDetailRow);
      }
    }

    internal StockWorkOrderDetailColumnChangedEventArgs(string columnName, StockWorkOrder doc, DataRow detailRow)
    {
      this.myColumnName = columnName;
      this.myStock = doc;
      this.myDetailRow = detailRow;
    }
  }
}
