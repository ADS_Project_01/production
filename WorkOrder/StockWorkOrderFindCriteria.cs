﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderFindCriteria
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using System;

namespace Production.WorkOrder
{
  [Serializable]
  public class StockWorkOrderFindCriteria
  {
    public bool Description;
    public bool DocNo;
    public bool DocDate;
    public bool RefDocNo;
    public bool UDF;
    public bool DetailUDF;
    public bool ShowOnlyOutstaning;
  }
}
