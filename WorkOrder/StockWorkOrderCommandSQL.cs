﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderCommandSQL
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount.Common;
using BCE.AutoCount.ContextException;
using BCE.AutoCount.Data;
using BCE.AutoCount.Document;
using BCE.AutoCount.Invoicing;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.RegistryID.LastSavedDescriptionID;
using BCE.AutoCount.RegistryID.Misc;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Misc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Production.WorkOrder
{
    public class StockWorkOrderCommandSQL : StockWorkOrderCommand
    {
        protected override DataSet LoadData(long docKey)
        {
            DataSet dataSet = new DataSet();
            DataTable myItemBOMOvdTable = new DataTable();
            DataTable myItemBOMBSTable = new DataTable();
            DataTable myItemBOMPITable = new DataTable();
            DataTable myItemBOMAPTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            string str1 = string.Empty;
            dataSet = BCE.AutoCount.Data.SqlDataAccess.LoadMasterDetailData(this.myDBSetting, docKey, "SELECT * FROM RPA_WO WHERE DocKey=@DocKey", "SELECT * FROM RPA_WODtl WHERE DocKey=@DocKey");
            SqlConnection myconn = new SqlConnection(myDBSetting.ConnectionString);
            string sSQLItemBOMOvd = "SELECT * FROM RPA_WOOvd WHERE DocKey=@DocKey ORDER BY DtlKey";
            string sSQLItemBOMBS = "SELECT * FROM RPA_WOBS WHERE DocKey=@DocKey ORDER BY DtlKey";
            string sSQLItemBOMPI = "SELECT * FROM RPA_WOProduct WHERE DocKey=@DocKey ORDER BY DtlKey";
            string sSQLItemBOMAP = "SELECT * FROM RPA_WOAP WHERE DocKey=@DocKey ORDER BY DtlKey";
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
            try
            {
                using (SqlCommand cmdItemBOMOvd = new SqlCommand(sSQLItemBOMOvd, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMOvd);
                    cmdItemBOMOvd.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMOvd.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMOvdTable);
                }
                DataColumn[] keyItemBOMOvd = new DataColumn[1];
                keyItemBOMOvd[0] = myItemBOMOvdTable.Columns["DtlKey"];
                myItemBOMOvdTable.PrimaryKey = keyItemBOMOvd;
                myItemBOMOvdTable.TableName = "OvdDetail";

                using (SqlCommand cmdItemBOMBS = new SqlCommand(sSQLItemBOMBS, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMBS);
                    cmdItemBOMBS.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMBS.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMBSTable);
                }
                DataColumn[] keycmdItemBOMBS = new DataColumn[1];
                keycmdItemBOMBS[0] = myItemBOMBSTable.Columns["DtlKey"];
                myItemBOMBSTable.PrimaryKey = keycmdItemBOMBS;
                myItemBOMBSTable.TableName = "BSDetail";

                using (SqlCommand cmdItemBOMPI = new SqlCommand(sSQLItemBOMPI, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMPI);
                    cmdItemBOMPI.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMPI.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMPITable);
                }
                DataColumn[] keycmdItemBOMPI = new DataColumn[1];
                keycmdItemBOMPI[0] = myItemBOMPITable.Columns["DtlKey"];
                myItemBOMPITable.PrimaryKey = keycmdItemBOMPI;
                myItemBOMPITable.TableName = "PIDetail";

                using (SqlCommand cmdItemBOMAP = new SqlCommand(sSQLItemBOMAP, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMAP);
                    cmdItemBOMAP.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMAP.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMAPTable);
                }
                DataColumn[] keycmdItemBOMAP = new DataColumn[1];
                keycmdItemBOMAP[0] = myItemBOMAPTable.Columns["DtlKey"];
                myItemBOMAPTable.PrimaryKey = keycmdItemBOMAP;
                myItemBOMAPTable.TableName = "APDetail";

               
                dataSet.Tables.Add(myItemBOMPITable);
                dataSet.Tables.Add(myItemBOMBSTable);
                dataSet.Tables.Add(myItemBOMOvdTable);
                dataSet.Tables.Add(myItemBOMAPTable);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataSet;
        }

        protected override DataSet LoadData(string docNo)
        {
            Int64 docKey = 0;
            DataSet dataSet = new DataSet();
            DataTable myItemBOMOvdTable = new DataTable();
            DataTable myItemBOMBSTable = new DataTable();
            DataTable myItemBOMPITable = new DataTable();
            DataTable myItemBOMAPTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            object obj = myDBSetting.ExecuteScalar("select DocKey from RPA_WO where DocNo=?",(object)docNo);
            if(obj!=null && obj!=DBNull.Value)
                docKey = BCE.Data.Convert.ToInt64(obj);

            string str1 = string.Empty;
            dataSet = BCE.AutoCount.Data.SqlDataAccess.LoadMasterDetailData(this.myDBSetting, docKey, "SELECT * FROM RPA_WO WHERE DocKey=@DocKey", "SELECT * FROM RPA_WODtl WHERE DocKey=@DocKey");
            SqlConnection myconn = new SqlConnection(myDBSetting.ConnectionString);
            string sSQLItemBOMOvd = "SELECT * FROM RPA_WOOvd WHERE DocKey=@DocKey ORDER BY DtlKey";
            string sSQLItemBOMBS = "SELECT * FROM RPA_WOBS WHERE DocKey=@DocKey ORDER BY DtlKey";

            string sSQLItemBOMPI = "SELECT * FROM RPA_WOProduct WHERE DocKey=@DocKey ORDER BY DtlKey";
            string sSQLItemBOMAP = "SELECT * FROM RPA_WOAP WHERE DocKey=@DocKey ORDER BY DtlKey";
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
            try
            {
                using (SqlCommand cmdItemBOMOvd = new SqlCommand(sSQLItemBOMOvd, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMOvd);
                    cmdItemBOMOvd.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMOvd.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMOvdTable);
                }
                DataColumn[] keyItemBOMOvd = new DataColumn[1];
                keyItemBOMOvd[0] = myItemBOMOvdTable.Columns["DtlKey"];
                myItemBOMOvdTable.PrimaryKey = keyItemBOMOvd;
                myItemBOMOvdTable.TableName = "OvdDetail";

                using (SqlCommand cmdItemBOMBS = new SqlCommand(sSQLItemBOMBS, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMBS);
                    cmdItemBOMBS.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMBS.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMBSTable);
                }
                DataColumn[] keycmdItemBOMBS = new DataColumn[1];
                keycmdItemBOMBS[0] = myItemBOMBSTable.Columns["DtlKey"];
                myItemBOMBSTable.PrimaryKey = keycmdItemBOMBS;
                myItemBOMBSTable.TableName = "BSDetail";

                using (SqlCommand cmdItemBOMPI = new SqlCommand(sSQLItemBOMPI, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMPI);
                    cmdItemBOMPI.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMPI.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMPITable);
                }
                DataColumn[] keycmdItemBOMPI = new DataColumn[1];
                keycmdItemBOMPI[0] = myItemBOMPITable.Columns["DtlKey"];
                myItemBOMPITable.PrimaryKey = keycmdItemBOMPI;
                myItemBOMPITable.TableName = "PIDetail";

                using (SqlCommand cmdItemBOMAP = new SqlCommand(sSQLItemBOMAP, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMAP);
                    cmdItemBOMAP.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMAP.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMAPTable);
                }
                DataColumn[] keycmdItemBOMAP = new DataColumn[1];
                keycmdItemBOMAP[0] = myItemBOMAPTable.Columns["DtlKey"];
                myItemBOMAPTable.PrimaryKey = keycmdItemBOMAP;
                myItemBOMAPTable.TableName = "APDetail";

                dataSet.Tables.Add(myItemBOMPITable);
                dataSet.Tables.Add(myItemBOMBSTable);
                dataSet.Tables.Add(myItemBOMOvdTable);
                dataSet.Tables.Add(myItemBOMAPTable);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataSet;
        }

        private DataTable GetASMBOMOptional(DataSet dsASM)
        {
            List<string> list = new List<string>();
            foreach (DataTable dataTable in (InternalDataCollectionBase)dsASM.Tables)
            {
                string index = "";
                if (dataTable.Columns.Contains("DtlKey"))
                    index = "DtlKey";
                else if (dataTable.Columns.Contains("DocKey"))
                    index = "DocKey";
                foreach (DataRow dataRow in (InternalDataCollectionBase)dataTable.Rows)
                    list.Add(dataRow[index].ToString());
            }
            string str1 = StringHelper.IListToCommaString((IList)list);
            DataTable dataTable1 = new DataTable();
            DataTable dataTable2;
            if (list.Count == 0)
            {
                dataTable2 = this.myDBSetting.GetDataTable("SELECT TOP 0 * FROM ASMBOMOptional", false, new object[0]);
            }
            else
            {
                DBSetting dbSetting = this.myDBSetting;
                string cmdText = "SELECT * FROM ASMBOMOptional WHERE DocKey IN(SELECT * FROM List(?))";
                int num = 0;
                object[] objArray = new object[1];
                int index = 0;
                string str2 = str1;
                objArray[index] = (object)str2;
                dataTable2 = dbSetting.GetDataTable(cmdText, num != 0, objArray);
            }
            dataTable2.TableName = "ASMBOMOptional";
            return dataTable2;
        }

        protected override long LoadFirst()
        {
            return DocumentHelper.GetFirstDocumentKey(this.myDBSetting, "RPA_WO");
        }

        protected override long LoadLast()
        {
            return DocumentHelper.GetLastDocumentKey(this.myDBSetting, "RPA_WO");
        }

        protected override long LoadNext(string docNo)
        {
            return DocumentHelper.GetNextDocumentKey(this.myDBSetting, "RPA_WO", docNo);
        }

        protected override long LoadPrev(string docNo)
        {
            return DocumentHelper.GetPrevDocumentKey(this.myDBSetting, "RPA_WO", docNo);
        }

        protected internal override void SaveData(StockWorkOrder stockAsmOrder)
        {
            string str1 = "";
            DataSet dataSet = (DataSet)null;
            bool flag = false;
            TransactionControl.DisableTransactionCounter();
            DBSetting dbSetting = this.myDBSetting.StartTransaction();
            try
            {
                //StockWorkOrderBeforeSaveEventArgs beforeSaveEventArgs1 = new StockWorkOrderBeforeSaveEventArgs(stockAsmOrder, dbSetting);
                //ScriptObject scriptObject1 = stockAsmOrder.ScriptObject;
                //string name1 = "BeforeSave";
                //Type[] types1 = new Type[1];
                //int index1 = 0;
                //Type type1 = beforeSaveEventArgs1.GetType();
                //types1[index1] = type1;
                //object[] objArray1 = new object[1];
                //int index2 = 0;
                //StockWorkOrderBeforeSaveEventArgs beforeSaveEventArgs2 = beforeSaveEventArgs1;
                //objArray1[index2] = (object)beforeSaveEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray1);
                //if (beforeSaveEventArgs1.ErrorMessage != null && beforeSaveEventArgs1.ErrorMessage.Length > 0)
                //    throw new StandardApplicationException(beforeSaveEventArgs1.ErrorMessage);
                //else if (beforeSaveEventArgs1.myAbort)
                //{
                //    throw new StandardApplicationException("");
                //}
                //else
                {
                    dataSet = stockAsmOrder.StockWorkOrderDataSet.Copy();
                    DataRow masterRow = dataSet.Tables["Master"].Rows[0];
                    flag = masterRow["DocNo"].ToString() == "<<New>>";
                    if (flag)
                    {
                        str1 = Document.CreateDocument(dbSetting).IncreaseNextNumber("SW", stockAsmOrder.DocNoFormatName, BCE.Data.Convert.ToDateTime(masterRow["DocDate"]));
                        masterRow["DocNo"] = (object)str1;
                        //StockWorkOrderEventArgs assemblyOrderEventArgs1 = new StockWorkOrderEventArgs(new StockWorkOrder((StockWorkOrderCommand)this, dataSet, stockAsmOrder.Action));
                        //ScriptObject scriptObject2 = stockAsmOrder.ScriptObject;
                        //string name2 = "OnGetNewDocumentNo";
                        //Type[] types2 = new Type[1];
                        //int index3 = 0;
                        //Type type2 = assemblyOrderEventArgs1.GetType();
                        //types2[index3] = type2;
                        //object[] objArray2 = new object[1];
                        //int index4 = 0;
                        ////StockWorkOrderEventArgs assemblyOrderEventArgs2 = assemblyOrderEventArgs1;
                        //objArray2[index4] = (object)assemblyOrderEventArgs2;
                        //scriptObject2.RunMethod(name2, types2, objArray2);
                    }
                    DBRegistry dbRegistry = DBRegistry.Create(dbSetting);
                    BaseRegistryID baseRegistryId1 = (BaseRegistryID)new StockAssemblyOrderDescriptionID();
                    baseRegistryId1.NewValue = (object)masterRow["Description"].ToString();
                    BaseRegistryID baseRegistryId2 = baseRegistryId1;
                    dbRegistry.SetValue((IRegistryID)baseRegistryId2);
            //        DataTable dataTable1 = new DataTable();
            //        string str2 = string.Empty;
            //        DataTable dataTable2 = new DataTable();
            //        DataColumn[] columns = new DataColumn[2]
            //        {
            //new DataColumn("DtlKey", typeof (long)),
            //new DataColumn("Qty", typeof (Decimal))
            //        };
            //        dataTable2.Columns.AddRange(columns);
            //        DataTable dataTable3 = dataTable2;
            //        DataColumn[] dataColumnArray = new DataColumn[1];
            //        int index5 = 0;
            //        DataColumn dataColumn = dataTable2.Columns["DtlKey"];
            //        dataColumnArray[index5] = dataColumn;
            //        dataTable3.PrimaryKey = dataColumnArray;
                    //if (masterRow["FromDocDtlKey"].ToString().Length != 0)
                    //{
                    //    DataRow dataRow1 = dataTable2.Rows.Find(masterRow["FromDocDtlKey"]);
                    //    if (dataRow1 == null)
                    //    {
                    //        DataRow row = dataTable2.NewRow();
                    //        row["Qty"] = (object)0;
                    //        row["DtlKey"] = masterRow["FromDocDtlKey"];
                    //        dataTable2.Rows.Add(row);
                    //        dataRow1 = row;
                    //    }
                    //    if (masterRow.RowState == DataRowState.Added)
                    //    {
                    //        this.CheckTransferedQty(masterRow, BCE.Data.Convert.ToDecimal(masterRow["Qty"]));
                    //        dataRow1["Qty"] = (object)(BCE.Data.Convert.ToDecimal(dataRow1["Qty"]) + BCE.Data.Convert.ToDecimal(masterRow["Qty"]));
                    //    }
                    //    else if (masterRow.RowState == DataRowState.Modified && !BCE.Data.Convert.TextToBoolean(masterRow["Cancelled"]) && BCE.Data.Convert.TextToBoolean(masterRow["Cancelled", DataRowVersion.Original]) == BCE.Data.Convert.TextToBoolean(masterRow["Cancelled", DataRowVersion.Current]))
                    //    {
                    //        if (BCE.Data.Convert.ToInt64(masterRow["FromDocDtlKey", DataRowVersion.Original]) == BCE.Data.Convert.ToInt64(masterRow["FromDocDtlKey", DataRowVersion.Current]))
                    //        {
                    //            Decimal qty = BCE.Data.Convert.ToDecimal(masterRow["Qty"]) - BCE.Data.Convert.ToDecimal(masterRow["Qty", DataRowVersion.Original]);
                    //            this.CheckTransferedQty(masterRow, qty);
                    //            dataRow1["Qty"] = (object)(BCE.Data.Convert.ToDecimal(dataRow1["Qty"]) + qty);
                    //        }
                    //        else
                    //        {
                    //            this.CheckTransferedQty(masterRow, BCE.Data.Convert.ToDecimal(masterRow["Qty"]));
                    //            dataRow1["Qty"] = (object)(BCE.Data.Convert.ToDecimal(dataRow1["Qty"]) + BCE.Data.Convert.ToDecimal(masterRow["Qty"]));
                    //            if (masterRow["FromDocDtlKey", DataRowVersion.Original] != DBNull.Value && this.LoadSODtlWithLock(masterRow["FromDocDtlKey", DataRowVersion.Original], true).Rows.Count > 0)
                    //            {
                    //                DataRow dataRow2 = dataTable2.Rows.Find(masterRow["FromDocDtlKey", DataRowVersion.Original]);
                    //                if (dataRow2 == null)
                    //                {
                    //                    DataRow row = dataTable2.NewRow();
                    //                    row["Qty"] = (object)0;
                    //                    row["DtlKey"] = masterRow["FromDocDtlKey", DataRowVersion.Original];
                    //                    dataTable2.Rows.Add(row);
                    //                    dataRow2 = row;
                    //                }
                    //                dataRow2["Qty"] = (object)(BCE.Data.Convert.ToDecimal(dataRow2["Qty"]) - BCE.Data.Convert.ToDecimal(masterRow["Qty", DataRowVersion.Original]));
                    //            }
                    //        }
                    //    }
                    //    else if (masterRow.RowState == DataRowState.Modified && BCE.Data.Convert.TextToBoolean(masterRow["Cancelled", DataRowVersion.Original]) != BCE.Data.Convert.TextToBoolean(masterRow["Cancelled", DataRowVersion.Current]))
                    //    {
                    //        if (BCE.Data.Convert.TextToBoolean(masterRow["Cancelled"]))
                    //        {
                    //            if (this.LoadSODtlWithLock(masterRow["FromDocDtlKey"], true).Rows.Count > 0)
                    //            {
                    //                DataRow dataRow2 = dataTable2.Rows.Find(masterRow["FromDocDtlKey"]);
                    //                if (dataRow2 == null)
                    //                {
                    //                    DataRow row = dataTable2.NewRow();
                    //                    row["Qty"] = (object)0;
                    //                    row["DtlKey"] = masterRow["FromDocDtlKey"];
                    //                    dataTable2.Rows.Add(row);
                    //                    dataRow2 = row;
                    //                }
                    //                dataRow2["Qty"] = (object)(BCE.Data.Convert.ToDecimal(dataRow2["Qty"]) - BCE.Data.Convert.ToDecimal(masterRow["Qty"]));
                    //            }
                    //        }
                    //        else
                    //        {
                    //            long num = BCE.Data.Convert.ToInt64(masterRow["FromDocDtlKey"]);
                    //            long docKey = this.SelectSODocKey((object)num);
                    //            BCE.Data.Convert.ToDecimal(masterRow["Qty"]);
                    //            DataSet dsDoc = DocumentHelper.LoadMasterDetailData(dbSetting, docKey, "SELECT * FROM SO WHERE DocKey = @DocKey", "SELECT * FROM SODtl WHERE DocKey = @DocKey");
                    //            if (dsDoc == null || dsDoc.Tables.Count == 0 || dsDoc.Tables["Master"].Rows.Count == 0)
                    //                throw new MissingTransferedDocException();
                    //            else if (BCE.Data.Convert.TextToBoolean(dsDoc.Tables["Master"].Rows[0]["Cancelled"]))
                    //                throw new TransferedFromDocIsCancelledException();
                    //            else if (!BCE.Data.Convert.TextToBoolean(dsDoc.Tables["Master"].Rows[0]["Transferable"]))
                    //            {
                    //                throw new TransferedFromDocTransferableIsFalseException();
                    //            }
                    //            else
                    //            {
                    //                DataRow dataRow2 = dsDoc.Tables["Detail"].Rows.Find((object)num);
                    //                if (dataRow2 == null)
                    //                    throw new MissingTransferedFromDocDetailRecordException();
                    //                else if (!BCE.Data.Convert.TextToBoolean(dataRow2["Transferable"]))
                    //                    throw new TransferedFromDocDetailTransferableIsFalseException();
                    //                else if (BCE.Data.Convert.ToDecimal(dataRow2["TransferedAOQty"]) > BCE.Data.Convert.ToDecimal(dataRow2["SmallestQty"]) + BCE.Data.Convert.ToDecimal(dataRow2["FOCQty"]))
                    //                    throw new TransferedQtyIsExceedLimit();
                    //                else if (BCE.Data.Convert.ToDecimal(dataRow2["TransferedAOQty"]) + BCE.Data.Convert.ToDecimal(masterRow["Qty"]) > BCE.Data.Convert.ToDecimal(dataRow2["SmallestQty"]) + BCE.Data.Convert.ToDecimal(dataRow2["FOCQty"]) && !DBRegistry.Create(this.myDBSetting).GetBoolean((IRegistryID)new AllowOverTransferOfQtyFromSO()))
                    //                {
                    //                    throw new TransferedQtyIsExceedLimit();
                    //                }
                    //                else
                    //                {
                    //                    dataRow2["TransferedAOQty"] = (object)(BCE.Data.Convert.ToDecimal(dataRow2["TransferedAOQty"]) + BCE.Data.Convert.ToDecimal(masterRow["Qty"]));
                    //                    this.SaveUncancelData(dsDoc, dbSetting);
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    //string cmdText = "SELECT * FROM SODTL ";
                    //foreach (DataRow dataRow in (InternalDataCollectionBase)dataTable2.Rows)
                    //{
                    //    DataTable dataTable4 = new DataTable();
                    //    new SqlDataAdapter(dbSetting.CreateCommand(cmdText + "WHERE DtlKey = " + dataRow["DtlKey"].ToString(), new object[0])).Fill(dataTable4);
                    //    if (dataTable4.Rows.Count > 0)
                    //    {
                    //        dataTable4.Rows[0].BeginEdit();
                    //        dataTable4.Rows[0]["TransferedAOQty"] = (object)(BCE.Data.Convert.ToDecimal(dataRow["Qty"]) + BCE.Data.Convert.ToDecimal(dataTable4.Rows[0]["TransferedAOQty"]));
                    //        dataTable4.Rows[0].EndEdit();
                    //        SqlDataAdapter adapter = new SqlDataAdapter(dbSetting.CreateCommand(cmdText, new object[0]));
                    //        SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(adapter);
                    //        DataTable dataTable5 = dataTable4;
                    //        adapter.Update(dataTable5);
                    //    }
                    //}
                    //this.PostAuditLog(dataSet, dbSetting, DocumentHelper.DetermineEventType(masterRow));
                    dbSetting.SimpleSaveDataTable(dataSet.Tables["Master"], "SELECT * FROM RPA_WO");
                    dbSetting.SimpleSaveDataTable(dataSet.Tables["Detail"], "SELECT * FROM RPA_WODTL");
                    dbSetting.SimpleSaveDataTable(dataSet.Tables["BSDetail"], "SELECT * FROM RPA_WOBS");
                    dbSetting.SimpleSaveDataTable(dataSet.Tables["PIDetail"], "SELECT * FROM RPA_WOProduct");
                    dbSetting.SimpleSaveDataTable(dataSet.Tables["OvdDetail"], "SELECT * FROM RPA_WOOvd");
                    dbSetting.SimpleSaveDataTable(dataSet.Tables["APDetail"], "SELECT * FROM RPA_WOAP");
                    TempDocument.Delete(dbSetting, BCE.Data.Convert.ToInt64(masterRow["DocKey"]));
                    //if (stockAsmOrder.UpdateBusinessFlowEvent != null)
                    //{
                    //    int num1 = stockAsmOrder.UpdateBusinessFlowEvent((object)stockAsmOrder, dbSetting) ? 1 : 0;
                    //}
                    dbSetting.Commit();
                }
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                dbSetting.EndTransaction();
                TransactionControl.EnableTransactionCounter();
            }
            if (flag)
                stockAsmOrder.DocNo = (DBString)str1;
            stockAsmOrder.StockWorkOrderDataSet.AcceptChanges();
            this.UpdateStockUOMConv(stockAsmOrder);
            TransactionControl.IncTransactionCount(this.myDBSetting);
            StockWorkOrderCommand.DataSetUpdate.Update(this.myDBSetting, dataSet, AsyncDataSetUpdateAction.Update);
            //StockWorkOrderEventArgs assemblyOrderEventArgs3 = new StockWorkOrderEventArgs(stockAsmOrder);
            //ScriptObject scriptObject = stockAsmOrder.ScriptObject;
            //string name = "AfterSave";
            //Type[] types = new Type[1];
            //int index6 = 0;
            //Type type = assemblyOrderEventArgs3.GetType();
            //types[index6] = type;
            //object[] objArray = new object[1];
            //int index7 = 0;
            //StockWorkOrderEventArgs assemblyOrderEventArgs4 = assemblyOrderEventArgs3;
            //objArray[index7] = (object)assemblyOrderEventArgs4;
            //scriptObject.RunMethod(name, types, objArray);
        }

        private void CheckTransferedQty(DataRow masterRow, Decimal qty)
        {
            DataTable dataTable = this.LoadSODtlWithLock(masterRow["FromDocDtlKey"], false);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                if (BCE.Data.Convert.ToDecimal(dataRow["TransferedAOQty"]) + qty > BCE.Data.Convert.ToDecimal(dataRow["SmallestQty"]) + BCE.Data.Convert.ToDecimal(dataRow["FOCQty"]) && !DBRegistry.Create(this.myDBSetting).GetBoolean((IRegistryID)new AllowOverTransferOfQtyFromSO()))
                    throw new TransferedQtyIsExceedLimit();
            }
        }

        protected override void DeleteData(long docKey)
        {
            StockWorkOrder doc = this.View(docKey);
            if (doc == null)
            {
                throw new DBConcurrencyException();
            }
            else
            {
                //this.myFiscalYear.CheckTransactionDate((DateTime) doc.DocDate, "StockAssemblyOrder", this.myDBSetting);
                //string str1 = "SELECT 1 WHERE EXISTS(SELECT * FROM Asm WHERE FromAsmOrderDocKey = ? AND Cancelled = 'F')";
                DBSetting dbSetting1 = this.myDBSetting;
                //string cmdText1 = str1;
                object[] objArray1 = new object[1];
                int index1 = 0;
                // ISSUE: variable of a boxed type
                long local = docKey;
                objArray1[index1] = (object)local;
                //if (dbSetting1.ExecuteScalar(cmdText1, objArray1) != null)
                //{
                //  throw new TransferedDocNotAllowDeleteException();
                //}
                //else
                {
                    DBSetting dbSetting2 = this.myDBSetting.StartTransaction();
                    try
                    {
                        //StockWorkOrderBeforeDeleteEventArgs beforeDeleteEventArgs1 = new StockWorkOrderBeforeDeleteEventArgs(doc, dbSetting2);
                        //ScriptObject scriptObject = doc.ScriptObject;
                        //string name = "BeforeDelete";
                        //Type[] types = new Type[1];
                        //int index2 = 0;
                        //Type type = beforeDeleteEventArgs1.GetType();
                        //types[index2] = type;
                        //object[] objArray2 = new object[1];
                        //int index3 = 0;
                        //StockWorkOrderBeforeDeleteEventArgs beforeDeleteEventArgs2 = beforeDeleteEventArgs1;
                        //objArray2[index3] = (object) beforeDeleteEventArgs2;
                        //scriptObject.RunMethod(name, types, objArray2);
                        //if (beforeDeleteEventArgs1.ErrorMessage != null && beforeDeleteEventArgs1.ErrorMessage.Length > 0)
                        //{
                        //  throw new StandardApplicationException(beforeDeleteEventArgs1.ErrorMessage);
                        //}
                        //else
                        {
                            //this.PostAuditLog(doc.StockWorkOrderDataSet, dbSetting2, AuditTrail.EventType.Delete);
                            DataTable dataTable1 = new DataTable();
                            string str2 = string.Empty;
                            DataTable dataTable2 = new DataTable();
                            dataTable2.Columns.Add("DtlKey", typeof(long));
                            dataTable2.Columns.Add("Qty", typeof(Decimal));
                            DataTable dataTable3 = dataTable2;
                            DataColumn[] dataColumnArray = new DataColumn[1];
                            int index4 = 0;
                            DataColumn dataColumn = dataTable2.Columns["DtlKey"];
                            dataColumnArray[index4] = dataColumn;
                            dataTable3.PrimaryKey = dataColumnArray;
                            // if (!doc.Cancelled && doc.FromDocDtlKey > 0L && this.LoadSODtlWithLock((object) doc.FromDocDtlKey, true).Rows.Count > 0)
                            //{
                            //  DataRow dataRow = dataTable2.Rows.Find((object) doc.FromDocDtlKey);
                            //  if (dataRow == null)
                            //  {
                            //    DataRow row = dataTable2.NewRow();
                            //    row["Qty"] = (object) 0;
                            //    row["DtlKey"] = (object) doc.FromDocDtlKey;
                            //    dataTable2.Rows.Add(row);
                            //    dataRow = row;
                            //  }
                            //  dataRow["Qty"] = (object) (BCE.Data.Convert.ToDecimal(dataRow["Qty"]) - (Decimal) doc.Qty);
                            //}
                            //string cmdText2 = "SELECT * FROM SODTL ";
                            //foreach (DataRow dataRow in (InternalDataCollectionBase) dataTable2.Rows)
                            //{
                            //  DataTable dataTable4 = new DataTable();
                            //  new SqlDataAdapter(dbSetting2.CreateCommand(cmdText2 + "WHERE DtlKey = " + dataRow["DtlKey"].ToString(), new object[0])).Fill(dataTable4);
                            //  dataTable4.Rows[0].BeginEdit();
                            //  dataTable4.Rows[0]["TransferedAOQty"] = (object) (BCE.Data.Convert.ToDecimal(dataRow["Qty"]) + BCE.Data.Convert.ToDecimal(dataTable4.Rows[0]["TransferedAOQty"]));
                            //  dataTable4.Rows[0].EndEdit();
                            //  SqlDataAdapter adapter = new SqlDataAdapter(dbSetting2.CreateCommand(cmdText2, new object[0]));
                            //  SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(adapter);
                            //  DataTable dataTable5 = dataTable4;
                            //  adapter.Update(dataTable5);
                            //}
                            DBSetting dbSetting4 = dbSetting2;
                            string cmdText4 = "DELETE FROM RPA_WODTL WHERE DocKey=@DocKey";
                            object[] objArray4 = new object[1];
                            int index6 = 0;
                            SqlParameter sqlParameter2 = new SqlParameter("@DocKey", (object)docKey);
                            objArray4[index6] = (object)sqlParameter2;
                            dbSetting4.ExecuteNonQuery(cmdText4, objArray4);

                            DBSetting dbSetting5 = dbSetting2;
                            string cmdTextBS = "DELETE FROM RPA_WOBS WHERE DocKey=@DocKey";
                            object[] objArrayBS = new object[1];
                            int indexBS = 0;
                            SqlParameter sqlParameterBS = new SqlParameter("@DocKey", (object)docKey);
                            objArrayBS[indexBS] = (object)sqlParameterBS;
                            dbSetting5.ExecuteNonQuery(cmdTextBS, objArrayBS);

                            DBSetting dbSetting6 = dbSetting2;
                            string cmdTextPI = "DELETE FROM RPA_WOProduct WHERE DocKey=@DocKey";
                            object[] objArrayPI = new object[1];
                            int indexPI = 0;
                            SqlParameter sqlParameterPI = new SqlParameter("@DocKey", (object)docKey);
                            objArrayPI[indexPI] = (object)sqlParameterPI;
                            dbSetting6.ExecuteNonQuery(cmdTextPI, objArrayPI);

                            DBSetting dbSetting7 = dbSetting2;
                            string cmdTextOvd = "DELETE FROM RPA_WOOvd WHERE DocKey=@DocKey";
                            object[] objArrayOvd = new object[1];
                            int indexOvd = 0;
                            SqlParameter sqlParameterOvd = new SqlParameter("@DocKey", (object)docKey);
                            objArrayOvd[indexOvd] = (object)sqlParameterOvd;
                            dbSetting7.ExecuteNonQuery(cmdTextOvd, objArrayOvd);

                            DBSetting dbSetting9 = dbSetting2;
                            string cmdTextAP = "DELETE FROM RPA_WOAP WHERE DocKey=@DocKey";
                            object[] objArrayAP = new object[1];
                            int indexAP = 0;
                            SqlParameter sqlParameterAP = new SqlParameter("@DocKey", (object)docKey);
                            objArrayAP[indexAP] = (object)sqlParameterAP;
                            dbSetting9.ExecuteNonQuery(cmdTextAP, objArrayAP);

                            DBSetting dbSetting3 = dbSetting2;
                            string cmdText3 = "DELETE FROM RPA_WO WHERE DocKey=@DocKey";
                            object[] objArray3 = new object[1];
                            int index5 = 0;
                            SqlParameter sqlParameter1 = new SqlParameter("@DocKey", (object)docKey);
                            objArray3[index5] = (object)sqlParameter1;
                            dbSetting3.ExecuteNonQuery(cmdText3, objArray3);

                           
                            dbSetting2.Commit();
                        }
                    }
                    catch (SqlException ex)
                    {
                        BCE.Data.DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        dbSetting2.EndTransaction();
                    }
                    StockWorkOrderCommand.DataSetUpdate.Update(this.myDBSetting, doc.StockWorkOrderDataSet, AsyncDataSetUpdateAction.Delete);
                    //StockWorkOrderEventArgs assemblyOrderEventArgs1 = new StockWorkOrderEventArgs(doc);
                    //ScriptObject scriptObject1 = doc.ScriptObject;
                    //string name1 = "AfterDelete";
                    //Type[] types1 = new Type[1];
                    //int index7 = 0;
                    //Type type1 = assemblyOrderEventArgs1.GetType();
                    //types1[index7] = type1;
                    //object[] objArray5 = new object[1];
                    //int index8 = 0;
                    //StockWorkOrderEventArgs assemblyOrderEventArgs2 = assemblyOrderEventArgs1;
                    //objArray5[index8] = (object)assemblyOrderEventArgs2;
                    //scriptObject1.RunMethod(name1, types1, objArray5);
                }
            }
        }

        //public override DataTable LoadAllBOMItems()
        //{
        //  DataTable dataTable1 = this.myDBSetting.GetDataTable("SELECT DISTINCT ItemCode FROM ItemBOM", false, new object[0]);
        //  DataTable dataTable2 = dataTable1;
        //  DataColumn[] dataColumnArray = new DataColumn[1];
        //  int index = 0;
        //  DataColumn dataColumn = dataTable1.Columns["ItemCode"];
        //  dataColumnArray[index] = dataColumn;
        //  dataTable2.PrimaryKey = dataColumnArray;
        //  return dataTable1;
        //}

        internal override DataRow LoadItemUOMDataRow(string itemCode)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT a.Rate, a.UOM, a.Cost FROM ITEMUOM a, ITEM b WHERE a.ItemCode=b.ItemCode AND a.UOM=b.BaseUOM AND a.ItemCode=@ItemCode";
            object[] objArray = new object[1];
            int index = 0;
            SqlParameter sqlParameter = new SqlParameter("@ItemCode", (object)itemCode);
            objArray[index] = (object)sqlParameter;
            return dbSetting.GetFirstDataRow(cmdText, objArray);
        }

        internal override DataTable LoadItemBOMData(string bomcode)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT A.* FROM RPA_ItemBOMDtl A inner join RPA_ItemBOM B on A.DocKey=B.DocKey WHERE B.BomCode=@BomCode ORDER BY A.ItemCode";
            int num = 0;
            object[] objArray = new object[1];
            int index = 0;
            SqlParameter sqlParameter = new SqlParameter("@BomCode", (object)bomcode);
            objArray[index] = (object)sqlParameter;
            DataTable dataTable = dbSetting.GetDataTable(cmdText, num != 0, objArray);
            if (dataTable.Rows.Count > 0)
                return dataTable;
            else
                return (DataTable)null;
        }
        internal override DataTable LoadItemBSData(string bomcode)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT A.* FROM RPA_ItemBOMBS A inner join RPA_ItemBOM B on A.DocKey=B.DocKey WHERE BomCode=@BomCode ORDER BY A.ItemCode";
            int num = 0;
            object[] objArray = new object[1];
            int index = 0;
            SqlParameter sqlParameter = new SqlParameter("@BomCode", (object)bomcode);
            objArray[index] = (object)sqlParameter;
            DataTable dataTable = dbSetting.GetDataTable(cmdText, num != 0, objArray);
            if (dataTable.Rows.Count > 0)
                return dataTable;
            else
                return (DataTable)null;
        }
        internal override DataTable LoadItemAPData(string bomcode)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT A.* FROM RPA_ItemBOMAP A inner join RPA_ItemBOM B on A.DocKey=B.DocKey WHERE BomCode=@BomCode ORDER BY A.ItemCode";
            int num = 0;
            object[] objArray = new object[1];
            int index = 0;
            SqlParameter sqlParameter = new SqlParameter("@BomCode", (object)bomcode);
            objArray[index] = (object)sqlParameter;
            DataTable dataTable = dbSetting.GetDataTable(cmdText, num != 0, objArray);
            if (dataTable.Rows.Count > 0)
                return dataTable;
            else
                return (DataTable)null;
        }

        internal override DataTable LoadItemPIData(string bomcode)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT A.* FROM RPA_ItemBOMPI A inner join RPA_ItemBOM B on A.DocKey=B.DocKey WHERE BomCode=@BomCode ORDER BY A.ItemCode";
            int num = 0;
            object[] objArray = new object[1];
            int index = 0;
            SqlParameter sqlParameter = new SqlParameter("@BomCode", (object)bomcode);
            objArray[index] = (object)sqlParameter;
            DataTable dataTable = dbSetting.GetDataTable(cmdText, num != 0, objArray);
            if (dataTable.Rows.Count > 0)
                return dataTable;
            else
                return (DataTable)null;
        }

        internal override DataTable LoadItemOvdData(string bomcode)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT A.* FROM RPA_ItemBOMOvd A inner join RPA_ItemBOM B on A.DocKey=B.DocKey WHERE BomCode=@BomCode ORDER BY A.OverheadCode";
            int num = 0;
            object[] objArray = new object[1];
            int index = 0;
            SqlParameter sqlParameter = new SqlParameter("@BomCode", (object)bomcode);
            objArray[index] = (object)sqlParameter;
            DataTable dataTable = dbSetting.GetDataTable(cmdText, num != 0, objArray);
            if (dataTable.Rows.Count > 0)
                return dataTable;
            else
                return (DataTable)null;
        }

        internal override DataTable LoadPartialTransferItemFromSO(object debtorcode)
        {
            string swhere = "";
            if (debtorcode != null && debtorcode != DBNull.Value)
            {
                if (debtorcode.ToString().Length > 0)
                    swhere = " and C.DebtorCode='" + debtorcode + "'";
            }
            
            string strquery = string.Format("SELECT DISTINCT A.DtlKey, A.Qty, A.UOM, A.Rate, A.SmallestQty, A.FOCQty, A.ItemCode, A.Description, A.DeptNo, A.Discount, A.Location, A.Numbering, A.ProjNo, A.SubTotal, A.UnitPrice, C.Cancelled, C.DocNo, C.DocDate, C.DebtorCode,C.DebtorName, (ISNULL(A.SmallestQty, 0) + ISNULL(A.FOCQty, 0) - ISNULL(A.TransferedAOQty, 0) - ISNULL(D.CancelledQty, 0)) AS RemainingQty, (ISNULL(A.SmallestQty, 0) + ISNULL(A.FOCQty, 0)) AS TotalQty FROM SODTL A INNER JOIN RPA_ItemBOM B ON A.ItemCode = B.ItemCode INNER JOIN SO C ON A.DocKey = C.DocKey LEFT OUTER JOIN(Select SUM(ISNULL(B.SmallestQty, 0) + ISNULL(B.FOCQty, 0)) AS CancelledQty, B.FromDocDtlKey From XS A INNER JOIN XSDTL B ON A.DocKey = B.DocKey Where A.Cancelled = 'F'  AND B.FromDocType = '{0}' AND(B.FullTransferOption is null OR B.FullTransferOption = 0) Group By B.FromDocDtlKey)D ON A.DtlKey = D.FromDocDtlKey WHERE ISNULL(C.ToDocType, '') <> '{1}' AND C.Cancelled = 'F' " + swhere + " AND A.Transferable = 'T' AND C.Transferable = 'T' AND((ISNULL(A.SmallestQty, 0) + ISNULL(A.FOCQty, 0) - ISNULL(A.TransferedAOQty, 0) - ISNULL(D.CancelledQty, 0) > 0))", (object)"SO", (object)"XS");
            return this.myDBSetting.GetDataTable(strquery, false, new object[0]);
        }

        internal override DataSet LoadPartialTransferStatus(long docKey)
        {
            string cmdText1 = "SELECT DocKey, DocNo, DocDate, Description, ItemCode, Qty, Total, AssemblyCost, NetTotal FROM ASM WHERE FromRPA_WODocKey = @FromRPA_WODocKey AND Cancelled = 'F' ";
            string cmdText2 = "SELECT A.DocKey, A.Seq, A.ItemCode, A.Description, A.Rate, A.Qty, A.ItemCost, A.OverHeadCost, A.SubTotalCost FROM ASMDtl A INNER JOIN ASM B ON A.DocKey = B.DocKey WHERE B.FromRPA_WODocKey = @FromRPA_WODocKey AND B.Cancelled = 'F' AND A.FromRPA_WODtlKey IS NOT NULL ";
            SqlConnection connection1 = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlConnection connection2 = connection1;
            SqlCommand selectCommand1 = new SqlCommand(cmdText1, connection2);
            int searchCommandTimeout1 = this.myGeneralSetting.SearchCommandTimeout;
            selectCommand1.CommandTimeout = searchCommandTimeout1;
            selectCommand1.Parameters.AddWithValue("@FromRPA_WODocKey", (object)docKey);
            SqlDataAdapter sqlDataAdapter1 = new SqlDataAdapter(selectCommand1);
            SqlCommand selectCommand2 = new SqlCommand(cmdText2, connection1);
            int searchCommandTimeout2 = this.myGeneralSetting.SearchCommandTimeout;
            selectCommand2.CommandTimeout = searchCommandTimeout2;
            selectCommand2.Parameters.AddWithValue("@FromRPA_WODocKey", (object)docKey);
            SqlDataAdapter sqlDataAdapter2 = new SqlDataAdapter(selectCommand2);
            DataSet dataSet = new DataSet();
            try
            {
                connection1.Open();
                sqlDataAdapter1.Fill(dataSet, "Master");
                sqlDataAdapter2.Fill(dataSet, "Detail");
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                connection1.Close();
                connection1.Dispose();
            }
            dataSet.Relations.Add("MasterDetail", dataSet.Tables["Master"].Columns["DocKey"], dataSet.Tables["Detail"].Columns["DocKey"]);
            return dataSet;
        }

        private DataTable LoadSODtlWithLock(object fromDocDtlKey, bool withLock)
        {
            string cmdText = !withLock ? "SELECT * FROM SODTL WHERE DtlKey = @FromDocDtlKey" : "SELECT * FROM SODTL WITH (UPDLOCK, ROWLOCK) WHERE DtlKey = @FromDocDtlKey";
            SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand selectCommand = new SqlCommand(cmdText, connection);
            selectCommand.Parameters.AddWithValue("@FromDocDtlKey", (object)SqlDbType.BigInt).Value = fromDocDtlKey;
            try
            {
                connection.Open();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
                DataTable dataTable1 = new DataTable();
                DataTable dataTable2 = dataTable1;
                sqlDataAdapter.Fill(dataTable2);
                return dataTable1;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                return (DataTable)null;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        private long SelectSODocKey(object fromDocDtlKey)
        {
            string cmdText = "SELECT A.DocKey FROM SO A INNER JOIN SODTL B ON A.DocKey = B.DocKey WHERE B.DtlKey = @FromDocDtlKey";
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlConnection connection = sqlConnection;
            SqlCommand sqlCommand = new SqlCommand(cmdText, connection);
            sqlCommand.Parameters.AddWithValue("@FromDocDtlKey", (object)SqlDbType.BigInt).Value = fromDocDtlKey;
            try
            {
                sqlConnection.Open();
                object obj = sqlCommand.ExecuteScalar();
                if (obj != null)
                    return BCE.Data.Convert.ToInt64(obj);
                else
                    return -1L;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                return -1L;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        private void SaveUncancelData(DataSet dsDoc, DBSetting dbSetting)
        {
            dbSetting.SimpleSaveDataTable(dsDoc.Tables["Master"], "SELECT * FROM SO");
            dbSetting.SimpleSaveDataTable(dsDoc.Tables["Detail"], "SELECT * FROM SODTL");
        }

        public override int InquireAllMaster(string columnSQL)
        {
            SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);

            columnSQL = columnSQL.Replace(",RPA_WO.Status2", "");
            try
            {
                connection.Open();
                SqlCommand selectCommand = new SqlCommand(string.Format("SELECT {0},ISNULL(dbo.F_RPA_WOStatus (RPA_WO.DocKey),Status) as Status2 FROM RPA_WO order by DocDate Desc", (object)columnSQL), connection);
                int commandTimeOut = this.myDBSetting.CommandTimeOut;
                selectCommand.CommandTimeout = commandTimeOut;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
                this.myDataTableAllMaster.Clear();
                DataTable dataTable1 = this.myDataTableAllMaster;
                sqlDataAdapter.Fill(dataTable1);
                if (this.myDataTableAllMaster.PrimaryKey.Length == 0)
                {
                    DataTable dataTable2 = this.myDataTableAllMaster;
                    DataColumn[] dataColumnArray = new DataColumn[1];
                    int index = 0;
                    DataColumn dataColumn = this.myDataTableAllMaster.Columns["DocKey"];
                    dataColumnArray[index] = dataColumn;
                    dataTable2.PrimaryKey = dataColumnArray;
                }
                //if (!this.myDataTableAllMaster.Columns.Contains("TransferFrom") && !this.myDataTableAllMaster.Columns.Contains("TransferTo"))
                //{
                //  this.myDataTableAllMaster.Columns.Add("TransferFrom", typeof (string));
                //  this.myDataTableAllMaster.Columns.Add("TransferTo", typeof (string));
                //}
                // DataTable dataTable3 = this.myDBSetting.GetDataTable("SELECT DocKey, From RPA_WO DocKey, DocNo FROM ASM WHERE Cancelled = 'F'", true, new object[0]);
                // DataTable dataTable4 = this.myDBSetting.GetDataTable("SELECT RPA_WO.DocKey, SO.DocNo FROM RPA_WO LEFT OUTER JOIN SODTL ON RPA_WO.FROMDocDtlKey = SODTL.DtlKey LEFT OUTER JOIN SO ON SODTL.DocKey = SO.DocKey ", true, new object[0]);
                //foreach (DataRow dataRow1 in (InternalDataCollectionBase) this.myDataTableAllMaster.Rows)
                //{
                //  DataRow[] dataRowArray1 = dataTable3.Select("FromRPA_WODocKey = '" + dataRow1["DocKey"].ToString() + "'");
                //  DataRow[] dataRowArray2 = dataTable4.Select("DocKey = '" + dataRow1["DocKey"].ToString() + "'");
                //  string str1 = "";
                //  string str2 = "";
                //  foreach (DataRow dataRow2 in dataRowArray1)
                //    str1 = !(str1 == "") ? str1 + (object) ", " + (string) dataRow2["DocNo"] : str1 + dataRow2["DocNo"];
                //  foreach (DataRow dataRow2 in dataRowArray2)
                //    str2 = !(str2 == "") ? str2 + (object) ", " + (string) dataRow2["DocNo"] : str2 + dataRow2["DocNo"];
                //  dataRow1["TransferTo"] = (object) str1;
                //  dataRow1["TransferFrom"] = (object) str2;
                //}
                
                return this.myDataTableAllMaster.Rows.Count;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        public override int SearchMaster(SearchCriteria criteria, string columnSQL, DataTable resultTable, string MultiSelectColumnName)
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            try
            {
                sqlConnection.Open();
                SqlCommand sqlCommand1 = new SqlCommand();
                sqlCommand1.Connection = sqlConnection;
                sqlCommand1.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
                string str1 = criteria.BuildSQL((IDbCommand)sqlCommand1);
                if (criteria is StockWorkOrderCriteria)
                {
                    StockWorkOrderCriteria assemblyOrderCriteria = criteria as StockWorkOrderCriteria;
                    if (assemblyOrderCriteria.UDF.Length > 0)
                    {
                        string str2 = new UDFUtil(this.myDBSetting).GenerateUDFSearchString(sqlCommand1, "RPA_WO", "A", assemblyOrderCriteria.UDF);
                        if (str2.Trim().Length > 0)
                            str1 = str1.Length != 0 ? str1 + " OR " + str2 : str2;
                    }
                    if (assemblyOrderCriteria.DetailUDF.Length > 0)
                    {
                        string str2 = new UDFUtil(this.myDBSetting).GenerateUDFSearchString(sqlCommand1, "RPA_WODTL", "B", assemblyOrderCriteria.DetailUDF);
                        if (str2.Trim().Length > 0)
                            str1 = str1.Length != 0 ? str1 + " OR " + str2 : str2;
                    }
                }
                if (str1.IndexOf("B.") >= 0)
                {
                    sqlCommand1.CommandText = string.Format("SELECT DISTINCT {0} FROM RPA_WO A, RPA_WODTL B WHERE (A.DocKey=B.DocKey)", (object)columnSQL);
                    if (str1.Length > 0)
                        sqlCommand1.CommandText = sqlCommand1.CommandText + " AND (" + str1 + ")";
                }
                else
                {
                    sqlCommand1.CommandText = string.Format("SELECT {0} FROM RPA_WO A", (object)columnSQL);
                    if (str1.Length > 0)
                    {
                        sqlCommand1.CommandText = sqlCommand1.CommandText + " WHERE (" + str1 + ") ";
                        if (criteria.GetType() == typeof(StockWorkOrderCriteria) && ((StockWorkOrderCriteria)criteria).ShowOnlyOutstanding)
                        {
                            SqlCommand sqlCommand2 = sqlCommand1;
                            string str2 = sqlCommand2.CommandText + "AND ISNULL(A.TransferedQty, 0) < A.Qty ";
                            sqlCommand2.CommandText = str2;
                        }
                    }
                    else if (criteria.GetType() == typeof(StockWorkOrderCriteria) && ((StockWorkOrderCriteria)criteria).ShowOnlyOutstanding)
                    {
                        SqlCommand sqlCommand2 = sqlCommand1;
                        string str2 = sqlCommand2.CommandText + " WHERE ISNULL(A.TransferedQty, 0) < A.Qty ";
                        sqlCommand2.CommandText = str2;
                    }
                }
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand1);
                long[] numArray = (long[])null;
                if (!criteria.KeepSearchResult)
                    resultTable.Clear();
                else if (MultiSelectColumnName.Length > 0 && resultTable.Rows.Count > 0 && resultTable.Columns[MultiSelectColumnName] != null)
                {
                    DataRow[] dataRowArray = resultTable.Select(string.Format("{0} = true", (object)MultiSelectColumnName));
                    numArray = new long[dataRowArray.Length];
                    for (int index = 0; index < dataRowArray.Length; ++index)
                        numArray[index] = BCE.Data.Convert.ToInt64(dataRowArray[index]["DocKey"]);
                }
                sqlDataAdapter.Fill(resultTable);
                if (resultTable.PrimaryKey.Length == 0)
                {
                    DataTable dataTable = resultTable;
                    DataColumn[] dataColumnArray = new DataColumn[1];
                    int index = 0;
                    DataColumn dataColumn = resultTable.Columns["DocKey"];
                    dataColumnArray[index] = dataColumn;
                    dataTable.PrimaryKey = dataColumnArray;
                }
                if (numArray != null)
                {
                    foreach (long num in numArray)
                    {
                        DataRow dataRow = resultTable.Rows.Find((object)num);
                        if (dataRow != null)
                            dataRow[MultiSelectColumnName] = (object)true;
                    }
                }
                return resultTable.Rows.Count;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        protected override DataSet LoadDesignReportData()
        {
            SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
            try
            {
                DataSet dataSet = new DataSet();
                connection.Open();
                new SqlDataAdapter(new SqlCommand("SELECT TOP 5 * FROM vRPA_WorkOrder ORDER BY DocKey", connection)).Fill(dataSet, "Master");
                new SqlDataAdapter(new SqlCommand("SELECT * FROM vRPA_WorkOrderDetail WHERE DocKey IN (SELECT TOP 5 DocKey FROM vRPA_WorkOrder ORDER BY DocKey)  ORDER BY DocKey, Seq", connection)).Fill(dataSet, "Detail");
                new SqlDataAdapter(new SqlCommand("SELECT * FROM vRPA_WorkOrderProduct WHERE DocKey IN (SELECT TOP 5 DocKey FROM vRPA_WorkOrder ORDER BY DocKey)  ORDER BY DocKey, Seq", connection)).Fill(dataSet, "PI");
                new SqlDataAdapter(new SqlCommand("SELECT * FROM vRPA_WorkOrderBS WHERE DocKey IN (SELECT TOP 5 DocKey FROM vRPA_WorkOrder ORDER BY DocKey)  ORDER BY DocKey, Seq", connection)).Fill(dataSet, "BS");
                new SqlDataAdapter(new SqlCommand("SELECT * FROM vRPA_WorkOrderOvd WHERE DocKey IN (SELECT TOP 5 DocKey FROM vRPA_WorkOrder ORDER BY DocKey)  ORDER BY DocKey, Seq", connection)).Fill(dataSet, "Ovd");
                new SqlDataAdapter(new SqlCommand("SELECT * FROM vRPA_WorkOrderAP WHERE DocKey IN (SELECT TOP 5 DocKey FROM vRPA_WorkOrder ORDER BY DocKey)  ORDER BY DocKey, Seq", connection)).Fill(dataSet, "AP");


                return dataSet;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        protected override DataSet LoadReportData(long docKey)
        {
            SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
            try
            {
                DataSet dataSet = new DataSet();
                connection.Open();
                SqlCommand selectCommand1 = new SqlCommand("SELECT * FROM vRPA_WorkOrder WHERE DocKey=@DocKey", connection);
                int searchCommandTimeout1 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand1.CommandTimeout = searchCommandTimeout1;
                selectCommand1.Parameters.AddWithValue("@DocKey", (object)docKey);
                new SqlDataAdapter(selectCommand1).Fill(dataSet, "Master");

                SqlCommand selectCommand2 = new SqlCommand("SELECT * FROM vRPA_WorkOrderDetail WHERE DocKey=@DocKey ORDER BY Seq", connection);
                int searchCommandTimeout2 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand2.CommandTimeout = searchCommandTimeout2;
                selectCommand2.Parameters.AddWithValue("@DocKey", (object)docKey);
                new SqlDataAdapter(selectCommand2).Fill(dataSet, "Detail");

                SqlCommand selectCommand3 = new SqlCommand("SELECT * FROM vRPA_WorkOrderProduct WHERE DocKey=@DocKey ORDER BY Seq", connection);
                int searchCommandTimeout3 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand3.CommandTimeout = searchCommandTimeout3;
                selectCommand3.Parameters.AddWithValue("@DocKey", (object)docKey);
                new SqlDataAdapter(selectCommand3).Fill(dataSet, "PIDetail");

                SqlCommand selectCommand4 = new SqlCommand("SELECT * FROM vRPA_WorkOrderBS WHERE DocKey=@DocKey ORDER BY Seq", connection);
                int searchCommandTimeout4 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand4.CommandTimeout = searchCommandTimeout4;
                selectCommand4.Parameters.AddWithValue("@DocKey", (object)docKey);
                new SqlDataAdapter(selectCommand4).Fill(dataSet, "BSDetail");

                SqlCommand selectCommand5 = new SqlCommand("SELECT * FROM vRPA_WorkOrderOvd WHERE DocKey=@DocKey ORDER BY Seq", connection);
                int searchCommandTimeout5 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand5.CommandTimeout = searchCommandTimeout5;
                selectCommand5.Parameters.AddWithValue("@DocKey", (object)docKey);
                new SqlDataAdapter(selectCommand5).Fill(dataSet, "OvdDetail");

                SqlCommand selectCommand6 = new SqlCommand("SELECT * FROM vRPA_WorkOrderAP WHERE DocKey=@DocKey ORDER BY Seq", connection);
                int searchCommandTimeout6 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand6.CommandTimeout = searchCommandTimeout6;
                selectCommand6.Parameters.AddWithValue("@DocKey", (object)docKey);
                new SqlDataAdapter(selectCommand6).Fill(dataSet, "APDetail");

                return dataSet;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        public override void DocumentListingBasicSearch(StockWorkOrderReportingCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName,bool bClose)
        {
            string strStatus="";
            if (bClose)
                strStatus = " AND dbo.F_RPA_WOStatus (RPA_WO.DocKey) in ('Pending','Planned','InProcess','Assembly','InProcess','Partial Assembly','Partial InProcess','Full Assembly','Full InProcess') and RPA_WO.Cancelled='F'";

            string str1 = columnName;
            char[] chArray = new char[1];
            int index1 = 0;
            int num1 = 44;
            chArray[index1] = (char)num1;
            string[] strArray1 = str1.Split(chArray);
            columnName = string.Empty;
            foreach (string oldValue in strArray1)
            {
                bool flag = false;
                //foreach (UDFColumn udfColumn in new UDFUtil(this.myDBSetting).GetUDF("RPA_WO"))
                //{
                //    if ("RPA_WO" + udfColumn.FieldName == oldValue)
                //    {
                //        string[] strArray2 = new string[6];
                //        int index2 = 0;
                //        string str2 = columnName;
                //        strArray2[index2] = str2;
                //        int index3 = 1;
                //        string str3 = "RPA_WO.";
                //        strArray2[index3] = str3;
                //        int index4 = 2;
                //        string fieldName1 = udfColumn.FieldName;
                //        strArray2[index4] = fieldName1;
                //        int index5 = 3;
                //        string str4 = " AS RPA_WO";
                //        strArray2[index5] = str4;
                //        int index6 = 4;
                //        string fieldName2 = udfColumn.FieldName;
                //        strArray2[index6] = fieldName2;
                //        int index7 = 5;
                //        string str5 = ",";
                //        strArray2[index7] = str5;
                //        columnName = string.Concat(strArray2);
                //        flag = true;
                //        break;
                //    }
                    

                //}
                if (oldValue == "NewStatus")
                    columnName = columnName + oldValue.Replace(oldValue, "");
                else if (oldValue == "Status")
                    columnName = columnName + oldValue.Replace(oldValue, "");
                //else if (oldValue == "ItemCode")
                //    columnName = columnName + oldValue.Replace(oldValue, "b." + oldValue + ",");
                //else if (oldValue == "BOMCode")
                //    columnName = columnName + oldValue.Replace(oldValue, "b." + oldValue + ",");
                else if (oldValue == "Cancelled")
                    columnName = columnName + oldValue.Replace(oldValue, "RPA_WO." + oldValue + ",");
                else
                {
                    if (!flag)
                        columnName = columnName + oldValue.Replace(oldValue, "RPA_WO." + oldValue + ",");
                }
            }
            if (columnName.EndsWith(","))
                columnName = columnName.Remove(columnName.Length - 1, 1);
            columnName = columnName.Trim();
            string str6 = string.Empty;
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                string whereSql = this.GetWhereSQL(criteria, sqlCommand);
                string str2 = !(whereSql == string.Empty) ? string.Format("SELECT DISTINCT {0},'' as NewStatus,RPA_WO.TotalAssemblyRequestQty,dbo.F_RPA_WOStatus (RPA_WO.DocKey)  as Status FROM RPA_WO  with(NOLOCK) INNER JOIN vRPA_WorkOrderProduct b with(NOLOCK) ON RPA_WO.DocKey=b.DocKey WHERE {1} {2} order by RPA_WO.TotalAssemblyRequestQty Desc", (object)columnName, (object)whereSql, (object)strStatus): string.Format("SELECT DISTINCT {0},'' as NewStatus,RPA_WO.TotalAssemblyRequestQty,dbo.F_RPA_WOStatus (RPA_WO.DocKey) as Status FROM RPA_WO with(NOLOCK) INNER JOIN vRPA_WorkOrderProduct b with(NOLOCK) ON RPA_WO.DocKey=b.DocKey Where (1=1) {1} order by RPA_WO.TotalAssemblyRequestQty Desc", (object)columnName,(Object)strStatus);
                sqlCommand.CommandText = str2;
                sqlCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                long[] numArray = (long[])null;
                if (!criteria.KeepSearchResult)
                    resultDataTable.Clear();
                else if (checkEditColumnName.Length > 0 && resultDataTable.Rows.Count > 0)
                {
                    DataRow[] dataRowArray = resultDataTable.Select(string.Format("{0} = true", (object)checkEditColumnName));
                    numArray = new long[dataRowArray.Length];
                    for (int index2 = 0; index2 < dataRowArray.Length; ++index2)
                        numArray[index2] = BCE.Data.Convert.ToInt64(dataRowArray[index2]["DocKey"]);
                }
                sqlConnection.Open();
                sqlDataAdapter.Fill(resultDataTable);
                if (resultDataTable.PrimaryKey.Length == 0)
                {
                    DataTable dataTable = resultDataTable;
                    DataColumn[] dataColumnArray = new DataColumn[1];
                    int index2 = 0;
                    DataColumn dataColumn = resultDataTable.Columns["DocKey"];
                    dataColumnArray[index2] = dataColumn;
                    dataTable.PrimaryKey = dataColumnArray;
                }
                if (criteria.SortBy != "")
                    resultDataTable.DefaultView.Sort = !(criteria.SortBy == "Document No") ? "DocDate" : "DocNo";
                if (numArray != null)
                {
                    foreach (long num2 in numArray)
                    {
                        DataRow dataRow = resultDataTable.Rows.Find((object)num2);
                        if (dataRow != null)
                            dataRow[checkEditColumnName] = (object)true;
                    }
                }
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        public override void ProcessClose(StockWorkOrderReportingCriteria criteria, DataTable resultDataTable)
        {
            //string str1 = columnName;
            char[] chArray = new char[1];
            int index1 = 0;
            int num1 = 44;
            chArray[index1] = (char)num1;
            //string[] strArray1 = str1.Split(chArray);

            string str6 = string.Empty;
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                string whereSql = this.GetWhereSQL(criteria, sqlCommand);
                string str2 = !(whereSql == string.Empty) ? string.Format("SELECT * FROM RPA_WO WHERE {0}", (object)whereSql) : string.Format("Select DISTINCT * From  RPA_WO Where (1=1)", (object)"");
                sqlCommand.CommandText = str2;
                sqlCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                //  long[] numArray = (long[])null;
                if (!criteria.KeepSearchResult)
                    resultDataTable.Clear();

                sqlConnection.Open();
                sqlDataAdapter.Fill(resultDataTable);
                if (resultDataTable.PrimaryKey.Length == 0)
                {
                    DataTable dataTable = resultDataTable;
                    DataColumn[] dataColumnArray = new DataColumn[1];
                    int index2 = 0;
                    DataColumn dataColumn = resultDataTable.Columns["DocKey"];
                    dataColumnArray[index2] = dataColumn;
                    dataTable.PrimaryKey = dataColumnArray;
                }
                if (criteria.SortBy != "")
                    resultDataTable.DefaultView.Sort = !(criteria.SortBy == "Document No") ? "DocDate" : "DocNo";




                //  if (numArray != null)
                {
                    foreach (DataRow MasterRow in resultDataTable.Rows)
                    {

                        decimal dA = 0;//Qty Finish Goods Main Product
                        decimal dB = 0;//Total Pemakaian BOM
                        decimal dC = 0;//Qty barang Sortiran
                        decimal dD = 0;//Qty barang alternatif
                        decimal dE = 0;//Biaya Overhead
                        decimal dF = 0;//Percentage barang sortiran
                        decimal dG = 0;//Max Harga barang sortiran
                        decimal dH = 0;//Total B+E
                        decimal dHpp1C = 0;//BS: ((B+E)*F)/C
                        decimal dHpp2C = 0;//BS: Jika 1C>G Maka, 2C=G
                                           //    Jika 1C<=G Maka,2c=Hpp1C
                        decimal dHppBS = 0;//Total Hpp2C
                        decimal dHppFG = 0;//H-Hpp2C
                        decimal dQtyFG = 0;//A+D                        
                        decimal dHppA = 0;//((A/dQtyFG)*dHppFG)/A
                        decimal dHppD = 0;//((D/dQtyFG)*dHppFG)/D
                                          // DataRow dataRow = resultDataTable.Rows.Find((object)num2);
                                          // if (dataRow != null)
                                          //    dataRow[checkEditColumnName] = (object)true;
                        DBSetting TransDBSetting = myDBSetting.StartTransaction();
                        try
                        {

                            StockWorkOrderCommand wocmd = StockWorkOrderCommand.Create(myDBSetting);
                            StockWorkOrder woentity = wocmd.Edit(BCE.Data.Convert.ToInt64(MasterRow["DocKey"]));
                            woentity.Status = WorkOrderStatusOptions.Closed.ToString();
                            object obj = myDBSetting.ExecuteScalar("select SUM(QTY) from RPA_RCV A with(nolock) inner join RPA_RCVDTL B with(nolock) ON A.DocKey = B.DocKey where FromDocNo =? and FromDocType='WO' and A.Cancelled = 'F' AND ItemCode =? ", (object)MasterRow["DocNo"], (object)MasterRow["ItemCode"]);
                            if (obj != null && obj != DBNull.Value)
                            {
                                dA = BCE.Data.Convert.ToDecimal(obj);
                            }
                            obj = myDBSetting.ExecuteScalar("select SUM((Qty-coalesce(TransferedQty,0))*UnitCost) from RPA_RM A with (nolock) inner join RPA_RMDTL B with (nolock) ON A.DocKey=B.DocKey where FromDocNo=? and Cancelled='F'", (object)MasterRow["DocNo"]);
                            if (obj != null && obj != DBNull.Value)
                            {
                                dB = BCE.Data.Convert.ToDecimal(obj);
                            }
                            obj = myDBSetting.ExecuteScalar("select SUM(QTY) from RPA_RCV A with(nolock) inner join RPA_RCVDTL B with(nolock) ON A.DocKey = B.DocKey where FromDocNo =? and A.Cancelled = 'F' AND FromDocType='AP'", (object)MasterRow["DocNo"]);
                            if (obj != null && obj != DBNull.Value)
                            {
                                dD = BCE.Data.Convert.ToDecimal(obj);
                            }
                            obj = myDBSetting.ExecuteScalar("SELECT SUM(COALESCE(Amount,0)) FROM RPA_WOOvd where DocKey=?", (object)MasterRow["DocKey"]);
                            if (obj != null && obj != DBNull.Value)
                            {
                                dE = BCE.Data.Convert.ToDecimal(obj);
                            }
                            dH = dB + dE;
                            dQtyFG = dA + dD;
                            obj = myDBSetting.ExecuteScalar("select SUM(QTY) from RPA_RCV A with(nolock) inner join RPA_RCVDTL B with(nolock) ON A.DocKey = B.DocKey where FromDocNo =? and A.Cancelled = 'F' AND FromDocType='BS'", (object)MasterRow["DocNo"]);
                            if (obj != null && obj != DBNull.Value)
                            {
                                dC = BCE.Data.Convert.ToDecimal(obj);
                            }
                           
                            dHpp1C = ((dB + dE) * dF) / dC;
                            foreach (DataRow BSRow in woentity.GetValidBSDetailRows())
                            {                              
                                dF = 0;//Percentage barang sortiran
                                dG = 0;//Max Harga barang sortiran                               
                                dHpp2C = 0;//BS: Jika 1C>G Maka, 2C=G
                                           //    Jika 1C<=G Maka,2c=Hpp1C
                                dF = BCE.Data.Convert.ToDecimal(BSRow["CostPercent"]);
                                dG = BCE.Data.Convert.ToDecimal(BSRow["CostMax"]);
                                if (dHpp1C > dG)
                                    dHpp2C = dG;
                                else
                                    dHpp2C = dHpp1C;
                                BSRow["ActHPP"] = dHpp2C;
                                dHppBS += dHpp2C;
                            }
                            dHppFG = dH - dHppBS;
                            dHppA = ((dA / dQtyFG) * dHppFG) / dA;
                            //woentity.ActHPP = dHppA;

                            foreach (DataRow APRow in woentity.GetValidAPDetailRows())
                            {
                                dD = 0;
                                dHppD = 0;//((D/dQtyFG)*dHppFG)/D
                                obj = myDBSetting.ExecuteScalar("select SUM(QTY) from RPA_RCV A with(nolock) inner join RPA_RCVDTL B with(nolock) ON A.DocKey = B.DocKey where FromDocNo =? and A.Cancelled = 'F' AND FromDocType='AP' and FromDocDtlKey=?", (object)MasterRow["DocNo"], (object)APRow["DtlKey"]);
                                if (obj != null && obj != DBNull.Value)
                                {
                                    dD = BCE.Data.Convert.ToDecimal(obj);
                                }
                                if(dD>0)
                                    dHppD = ((dD / dQtyFG) * dHppFG) / dD;
                                APRow["ActHPP"] = dHppD;

                            }
                            woentity.Save(BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID);
                        }
                        catch (SqlException ex)
                        {

                            BCE.Data.DataError.HandleSqlException(ex);
                            throw;
                        }
                        finally
                        {
                            TransDBSetting.EndTransaction();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        protected string GetWhereSQL(StockWorkOrderReportingCriteria reportingCriteria, SqlCommand cmd)
        {
            string str1 = "";
            SearchCriteria searchCriteria = new SearchCriteria();
            BCE.AutoCount.SearchFilter.Filter dateFilter = reportingCriteria.DateFilter;
            searchCriteria.AddFilter(dateFilter);
            BCE.AutoCount.SearchFilter.Filter documentFilter = reportingCriteria.DocumentFilter;
            searchCriteria.AddFilter(documentFilter);
            int num = 1;
            searchCriteria.MatchAll = num != 0;
            SqlCommand sqlCommand = cmd;
            string str2 = searchCriteria.BuildSQL((IDbCommand)sqlCommand);
            if (str2.Length > 0)
                str1 = str2;
            if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.UnCancelled)
                str1 = str1.Length <= 0 ? " RPA_WO.Cancelled = 'F' " : str1 + " And RPA_WO.Cancelled = 'F' ";
            else if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
                str1 = str1.Length <= 0 ? " RPA_WO.Cancelled = 'T' " : str1 + " And RPA_WO.Cancelled = 'T' ";
            return str1;
        }

        public override void BasicSearch(StockWorkOrderCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
        {
            string str1 = columnName;
            char[] chArray = new char[1];
            int index1 = 0;
            int num1 = 44;
            chArray[index1] = (char)num1;
            string[] strArray = str1.Split(chArray);
            columnName = string.Empty;
            foreach (string oldValue in strArray)
                columnName = columnName + oldValue.Replace(oldValue, "RPA_WO." + oldValue + ",");
            if (columnName.EndsWith(","))
                columnName = columnName.Remove(columnName.Length - 1, 1);
            columnName = columnName.Trim();
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
                criteria.MatchAll = false;
                string str2 = criteria.BuildSQL((IDbCommand)sqlCommand);
                if (criteria.UDF.Length > 0)
                {
                    string str3 = new UDFUtil(this.myDBSetting).GenerateUDFSearchString(sqlCommand, "RPA_WO", "RPA_WO", criteria.UDF);
                    if (str3.Trim().Length > 0)
                        str2 = str2.Length != 0 ? str2 + " OR " + str3 : str3;
                }
                if (criteria.DetailUDF.Length > 0)
                {
                    string str3 = new UDFUtil(this.myDBSetting).GenerateUDFSearchString(sqlCommand, "RPA_WODTL", "RPA_WODTL", criteria.DetailUDF);
                    if (str3.Trim().Length > 0)
                        str2 = str2.Length != 0 ? str2 + " OR " + str3 : str3;
                }
                string str4 = string.Empty;
                string str5 = !(str2 == string.Empty) ? string.Format(" SELECT {0} FROM RPA_WO WHERE DocKey In (SELECT DISTINCT RPA_WO.DocKey FROM RPA_WO INNER JOIN RPA_WODTL ON RPA_WO.DocKey = RPA_WODTL.DocKey WHERE {1})", (object)columnName, (object)str2) : string.Format("Select DISTINCT {0}, DocKey From RPA_WO Where (1=1)", (object)columnName);
                sqlCommand.CommandText = str5;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                long[] numArray = (long[])null;
                if (!criteria.KeepSearchResult)
                    resultDataTable.Clear();
                else if (checkEditColumnName.Length > 0 && resultDataTable.Rows.Count > 0)
                {
                    DataRow[] dataRowArray = resultDataTable.Select(string.Format("{0} = true", (object)checkEditColumnName));
                    numArray = new long[dataRowArray.Length];
                    for (int index2 = 0; index2 < dataRowArray.Length; ++index2)
                        numArray[index2] = BCE.Data.Convert.ToInt64(dataRowArray[index2]["DocKey"]);
                }
                sqlConnection.Open();
                sqlDataAdapter.Fill(resultDataTable);
                if (resultDataTable.PrimaryKey.Length == 0)
                {
                    DataTable dataTable = resultDataTable;
                    DataColumn[] dataColumnArray = new DataColumn[1];
                    int index2 = 0;
                    DataColumn dataColumn = resultDataTable.Columns["DocKey"];
                    dataColumnArray[index2] = dataColumn;
                    dataTable.PrimaryKey = dataColumnArray;
                }
                if (numArray != null)
                {
                    foreach (long num2 in numArray)
                    {
                        DataRow dataRow = resultDataTable.Rows.Find((object)num2);
                        if (dataRow != null)
                            dataRow[checkEditColumnName] = (object)true;
                    }
                }
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        public override void AdvanceSearch(AdvancedStockWorkOrderCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
        {
            string str1 = columnName;
            char[] chArray = new char[1];
            int index1 = 0;
            int num1 = 44;
            chArray[index1] = (char)num1;
            string[] strArray1 = str1.Split(chArray);
            columnName = string.Empty;
            foreach (string oldValue in strArray1)
            {
                bool flag = false;
                foreach (UDFColumn udfColumn in new UDFUtil(this.myDBSetting).GetUDF("RPA_WO"))
                {
                    if ("RPA_WO" + udfColumn.FieldName == oldValue.Trim())
                    {
                        string[] strArray2 = new string[6];
                        int index2 = 0;
                        string str2 = columnName;
                        strArray2[index2] = str2;
                        int index3 = 1;
                        string str3 = "A.";
                        strArray2[index3] = str3;
                        int index4 = 2;
                        string fieldName1 = udfColumn.FieldName;
                        strArray2[index4] = fieldName1;
                        int index5 = 3;
                        string str4 = " AS RPA_WO";
                        strArray2[index5] = str4;
                        int index6 = 4;
                        string fieldName2 = udfColumn.FieldName;
                        strArray2[index6] = fieldName2;
                        int index7 = 5;
                        string str5 = ",";
                        strArray2[index7] = str5;
                        columnName = string.Concat(strArray2);
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                    columnName = columnName + oldValue.Replace(oldValue, "A." + oldValue + ",");
            }
            if (columnName.EndsWith(","))
                columnName = columnName.Remove(columnName.Length - 1, 1);
            columnName = columnName.Trim();
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            try
            {
                SqlCommand selectCommand = new SqlCommand();
                selectCommand.Connection = sqlConnection;
                selectCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
                string str2 = criteria.BuildSQL((IDbCommand)selectCommand);
                string str3 = string.Empty;
                string str4 = !(str2 != string.Empty) ? string.Format("Select DISTINCT {0} From RPA_WO A, RPA_WODTL B where A.DocKey = B.DocKey ", (object)columnName) : string.Format("Select DISTINCT {0} From RPA_WO A, RPA_WODTL B where A.DocKey = B.DocKey and {1}", (object)columnName, (object)str2);
                selectCommand.CommandText = str4;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
                long[] numArray = (long[])null;
                if (!criteria.KeepSearchResult)
                    resultDataTable.Clear();
                else if (checkEditColumnName.Length > 0 && resultDataTable.Rows.Count > 0)
                {
                    DataRow[] dataRowArray = resultDataTable.Select(string.Format("{0} = true", (object)checkEditColumnName));
                    numArray = new long[dataRowArray.Length];
                    for (int index2 = 0; index2 < dataRowArray.Length; ++index2)
                        numArray[index2] = BCE.Data.Convert.ToInt64(dataRowArray[index2]["DocKey"]);
                }
                sqlConnection.Open();
                sqlDataAdapter.Fill(resultDataTable);
                if (resultDataTable.PrimaryKey.Length == 0)
                {
                    DataTable dataTable = resultDataTable;
                    DataColumn[] dataColumnArray = new DataColumn[1];
                    int index2 = 0;
                    DataColumn dataColumn = resultDataTable.Columns["DocKey"];
                    dataColumnArray[index2] = dataColumn;
                    dataTable.PrimaryKey = dataColumnArray;
                }
                if (numArray != null)
                {
                    foreach (long num2 in numArray)
                    {
                        DataRow dataRow = resultDataTable.Rows.Find((object)num2);
                        if (dataRow != null)
                            dataRow[checkEditColumnName] = (object)true;
                    }
                }
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        protected override DataSet LoadDocumentListingReportDesignerData()
        {
            SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
            try
            {
                DataSet dataSet = new DataSet();
                connection.Open();
                SqlCommand selectCommand1 = new SqlCommand("SELECT TOP 100 * FROM vRPA_WorkOrder ORDER BY DocKey", connection);
                int searchCommandTimeout1 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand1.CommandTimeout = searchCommandTimeout1;
                new SqlDataAdapter(selectCommand1).Fill(dataSet, "Master");
                SqlCommand selectCommand2 = new SqlCommand("SELECT * FROM vRPA_WorkOrderDetail WHERE DocKey IN (SELECT TOP 100 DocKey FROM vRPA_WorkOrder ORDER BY DocKey) ORDER BY DocKey, Seq", connection);
                int searchCommandTimeout2 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand2.CommandTimeout = searchCommandTimeout2;
                new SqlDataAdapter(selectCommand2).Fill(dataSet, "Detail");
                SqlCommand selectCommand3 = new SqlCommand("SELECT * FROM vRPA_WorkOrderProduct ORDER BY Seq", connection);
                int searchCommandTimeout3 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand3.CommandTimeout = searchCommandTimeout3;
                new SqlDataAdapter(selectCommand3).Fill(dataSet, "PIDetail");

                SqlCommand selectCommand4 = new SqlCommand("SELECT * FROM vRPA_WorkOrderBS ORDER BY Seq", connection);
                int searchCommandTimeout4 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand4.CommandTimeout = searchCommandTimeout4;
                new SqlDataAdapter(selectCommand4).Fill(dataSet, "BSDetail");

                SqlCommand selectCommand5 = new SqlCommand("SELECT * FROM vRPA_WorkOrderOvd ORDER BY Seq", connection);
                int searchCommandTimeout5 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand5.CommandTimeout = searchCommandTimeout5;
                new SqlDataAdapter(selectCommand5).Fill(dataSet, "OvdDetail");

                SqlCommand selectCommand6 = new SqlCommand("SELECT * FROM vRPA_WorkOrderAP ORDER BY Seq", connection);
                int searchCommandTimeout6 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand6.CommandTimeout = searchCommandTimeout6;
                new SqlDataAdapter(selectCommand6).Fill(dataSet, "APDetail");

                return dataSet;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        protected override DataSet LoadDocumentListingReportData(string docKeys)
        {
            SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
            string cmdText1 = string.Format("SELECT * FROM vRPA_WorkOrder WHERE DocKey IN ({0}) ", (object)docKeys);
            string cmdText2 = string.Format("SELECT * FROM vRPA_WorkOrderDetail WHERE DocKey IN ({0}) ORDER BY Seq", (object)docKeys);
            string cmdText3 = string.Format("SELECT * FROM vRPA_WorkOrderProduct WHERE DocKey IN ({0}) ORDER BY Seq", (object)docKeys);
            string cmdText4 = string.Format("SELECT * FROM vRPA_WorkOrderBS WHERE DocKey IN ({0}) ORDER BY Seq", (object)docKeys);
            string cmdText5 = string.Format("SELECT * FROM vRPA_WorkOrderOvd WHERE DocKey IN ({0}) ORDER BY Seq", (object)docKeys);
            string cmdText6 = string.Format("SELECT * FROM vRPA_WorkOrderAP WHERE DocKey IN ({0}) ORDER BY Seq", (object)docKeys);



            try
            {
                DataSet dataSet = new DataSet();
                connection.Open();
                SqlCommand selectCommand1 = new SqlCommand(cmdText1, connection);
                int searchCommandTimeout1 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand1.CommandTimeout = searchCommandTimeout1;
                new SqlDataAdapter(selectCommand1).Fill(dataSet, "Master");

                SqlCommand selectCommand2 = new SqlCommand(cmdText2, connection);
                int searchCommandTimeout2 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand2.CommandTimeout = searchCommandTimeout2;
                new SqlDataAdapter(selectCommand2).Fill(dataSet, "Detail");

                SqlCommand selectCommand3 = new SqlCommand(cmdText3, connection);
                int searchCommandTimeout3 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand3.CommandTimeout = searchCommandTimeout3;
                new SqlDataAdapter(selectCommand3).Fill(dataSet, "PIDetail");

                SqlCommand selectCommand4 = new SqlCommand(cmdText4, connection);
                int searchCommandTimeout4 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand4.CommandTimeout = searchCommandTimeout4;
                new SqlDataAdapter(selectCommand4).Fill(dataSet, "BSDetail");

                SqlCommand selectCommand5 = new SqlCommand(cmdText5, connection);
                int searchCommandTimeout5 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand5.CommandTimeout = searchCommandTimeout5;
                new SqlDataAdapter(selectCommand5).Fill(dataSet, "OvdDetail");

                SqlCommand selectCommand6 = new SqlCommand(cmdText6, connection);
                int searchCommandTimeout6 = this.myGeneralSetting.SearchCommandTimeout;
                selectCommand6.CommandTimeout = searchCommandTimeout6;
                new SqlDataAdapter(selectCommand6).Fill(dataSet, "APDetail");

                return dataSet;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }
    }
}
