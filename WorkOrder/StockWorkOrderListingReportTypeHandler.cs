﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderListingReportTypeHandler
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Data;

namespace Production.WorkOrder
{
  public class StockWorkOrderListingReportTypeHandler : StockWorkOrderReportTypeHandler
  {
    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      return StockWorkOrderCommand.Create(dbSetting).GetDocumentListingReportDesignerDataSource((StockWorkOrderReportingCriteria) null);
    }
  }
}
