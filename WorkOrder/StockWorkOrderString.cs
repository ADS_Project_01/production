﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.StockWorkOrderString
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Localization;

namespace Production.WorkOrder
{
  [LocalizableString]
  public enum StockWorkOrderString
  {
    [DefaultString("Batch Print Stock Work Order / Print Stock Work Order Listing")] BatchPrintStockWorkOrder_PrintStockWorkOrderListing,
    [DefaultString("Print Stock Work Order Detail Listing")] PrintStockWorkOrderDetailListing,
    [DefaultString("Print Outstanding Stock Work Order Listing")] PrintOutstandingStockWorkOrderListing,
    [DefaultString("{0} opened Stock Work Order List window.")] OpenStockWorkOrder,
    [DefaultString("Stock Work Order {0}")] StockWorkOrder,
    [DefaultString("{0} viewed Stock Work Order {1} in edit mode.")] ViewStockWorkOrderInEditMode,
    [DefaultString("{0} viewed Stock Work Order {1}.")] ViewStockWorkOrder,
    [DefaultString("{0} edited Stock Work Order {1}.")] EditedStockWorkOrder,
    [DefaultString("{0} cancels Stock Work Order {1}.")] CancelStockWorkOrder,
    [DefaultString("{0} un-cancels Stock Work Order {1}.")] UncancelStockWorkOrder,
    [DefaultString("{0} created new Stock Work Order {1}.")] CreatedNewStockWorkOrder,
    [DefaultString("{0} updated Stock Work Order {1}.")] UpdatedStockWorkOrder,
    [DefaultString("{0} deleted Stock Work Order {1}.")] DeletedStockWorkOrder,
    [DefaultString("{0} opened Print Stock Work Order Detail Listing window.")] OpenedPrintStockWorkOrderDetailListing,
    [DefaultString("{0} inquired on Print Stock Work Order Detail Listing.")] InquiredPrintStockWorkOrderDetailListing,
    [DefaultString("Stock Work Order Detail Listing")] StockWorkOrderDetailListing,
    [DefaultString("{0} opened Print Stock Work Order Listing window.")] OpenedPrintStockWorkOrderListing,
    [DefaultString("{0} inquired on Print Stock Work Order Listing.")] InquiredPrintStockWorkOrderListing,
    [DefaultString("{0} inquired on Batch Print Stock Work Order.")] InquiredBatchPrintStockWorkOrder,
    [DefaultString("Batch Stock Work Order")] BatchStockWorkOrder,
    [DefaultString("Stock Work Order Listing")] StockWorkOrderListing,
    [DefaultString("{0} opened Print Outstanding Stock Work Order Listing window.")] OpenedPrintOutstandingStockWorkOrderListing,
    [DefaultString("{0} inquired on Print Outstanding Stock Work Order Listing.")] InquiredPrintOutstandingStockWorkOrderListing,
    [DefaultString("Outstanding Stock Work Order Listing.")] OutstandingStockWorkOrderListing,
  }
}
