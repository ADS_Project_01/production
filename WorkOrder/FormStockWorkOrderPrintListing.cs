﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.FormStockWorkOrderPrintListing
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Production.WorkOrder
{
  [SingleInstanceThreadForm]
  public class FormStockWorkOrderPrintListing : XtraForm
  {
    public static string myColumnName;
    private DBSetting myDBSetting;
    private DataTable myDataTable;
    private StockWorkOrderCommand myCommand;
    private AdvancedStockWorkOrderCriteria myCriteria;
    private bool myInSearch;
    private long[] mySelectedDocKeylist;
    private StockWorkOrderReportingCriteria myReportingCriteria;
    private string[] myGroupBy;
    private string[] mySortBy;
    private string[] myDocumentStyleOptions;
    private ScriptObject myScriptObject;
    private MouseDownHelper myMouseDownHelper;
    private IContainer components;
    private PanelHeader panelHeader1;
    private PreviewButton previewButton1;
    private PrintButton printButton1;
    private UCDateSelector ucDateSelector1;
    private UCSearchResult ucSearchResult1;
    private UCWOSelector ucStockWorkOrderSelector1;
    private Bar bar2;
    private BarButtonItem barBtnDesignDocumentStyleReport;
    private BarButtonItem barBtnDesignListingStyleReport;
    private BarDockControl barDockControlBottom;
    private BarDockControl barDockControlLeft;
    private BarDockControl barDockControlRight;
    private BarDockControl barDockControlTop;
    private BarManager barManager1;
    private BarSubItem barSubItem1;
    private CheckEdit chkEditShowCriteria;
    private ComboBoxEdit cbCancelledOption;
    private ComboBoxEdit cbEditGroupBy;
    private ComboBoxEdit cbEditReportType;
    private ComboBoxEdit cbEditSortBy;
    private GroupControl gbReportOption;
    private GroupControl groupBox_SearchCriteria;
    private MemoEdit memoEdit_Criteria;
    private PanelControl panel_Center;
    private PanelControl panelControl1;
    private PanelControl panelCriteria;
    private RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private RepositoryItemTextEdit repositoryItemTextEdit_Cancelled;
    private SimpleButton sbtnClose;
    private SimpleButton sbtnInquiry;
    private SimpleButton sbtnToggleOptions;
    private SimpleButton simpleButtonAdvanceSearch;
    private GridColumn colCancelled;
    private GridColumn colCheck;
    private GridColumn colCreatedTimeStamp;
    private GridColumn colCreatedUserID;
    private GridColumn colStatus;
    private GridColumn colDescription;
    private GridColumn colDocDate;
    private GridColumn colDocNo;
    private GridColumn colLastModified;
    private GridColumn colLastModifiedUserID;
    private GridColumn colLocation;
    private GridColumn colPrintCount;
    private GridColumn colProjNo;
    private GridColumn colRefDocNo;
    private GridColumn colRemark1;
    private GridColumn colRemark2;
    private GridColumn colRemark3;
    private GridColumn colRemark4;
    private GridControl gridControl1;
    private GridView gridView1;
    private XtraTabControl tabControl1;
    private XtraTabPage tabPage1;
    private XtraTabPage tabPage2;
    private Label label3;
    private Label label4;
    private Label label6;
    private Label label7;
    private Label label8;
    private Label label9;
        private XtraTabPage xtraTabPage1;
        private GridColumn gridColumn1;
        private GridColumn colMachineCode;
        private BarSubItem barSubItem2;
        private BarButtonItem barButtonItem1;
        private Bar bar3;
        private BarButtonItem barButtonItem2;
        private Bar bar1;
        private BarSubItem barSubItem3;
        private BarButtonItem barButtonItem3;
        private BarButtonItem barButtonItem4;
        private GridColumn colProductionStartDate;
        private GridColumn colProductionEndDate;
        private GridColumn colExpCompletedDate;

    public DataTable MasterDataTable
    {
      get
      {
        return this.myDataTable;
      }
    }

    public long[] SelectedDocKeys
    {
      get
      {
        return this.mySelectedDocKeylist;
      }
    }

    public string SelectedDocNosInString
    {
      get
      {
        return this.GenerateDocNosToString();
      }
    }

    public string SelectedDocKeysInString
    {
      get
      {
        return StringHelper.ArrayListToCommaString(new ArrayList((ICollection) this.mySelectedDocKeylist));
      }
    }

    public string ColumnName
    {
      get
      {
        return FormStockWorkOrderPrintListing.myColumnName;
      }
    }

    public StockWorkOrderReportingCriteria StockWorkOrderReportingCriteria
    {
      get
      {
        return this.myReportingCriteria;
      }
    }

    public FormStockWorkOrderPrintListing(DBSetting dbSetting)
    {
      FormStockWorkOrderPrintListing orderPrintListing1 = this;
      string[] strArray1 = new string[4];
      int index1 = 0;
      string str1 = "None";
      strArray1[index1] = str1;
      int index2 = 1;
      string str2 = "Date";
      strArray1[index2] = str2;
      int index3 = 2;
      string str3 = "Month";
      strArray1[index3] = str3;
      int index4 = 3;
      string str4 = "Year";
      strArray1[index4] = str4;
      orderPrintListing1.myGroupBy = strArray1;
      FormStockWorkOrderPrintListing orderPrintListing2 = this;
      string[] strArray2 = new string[2];
      int index5 = 0;
      string str5 = "Document No";
      strArray2[index5] = str5;
      int index6 = 1;
      string str6 = "Date";
      strArray2[index6] = str6;
      orderPrintListing2.mySortBy = strArray2;
      FormStockWorkOrderPrintListing orderPrintListing3 = this;
      string[] strArray3 = new string[2];
      int index7 = 0;
      string str7 = "Batch Print Stock Work Order";
      strArray3[index7] = str7;
      int index8 = 1;
      string str8 = "Print Stock Work Order Listing";
      strArray3[index8] = str8;
      orderPrintListing3.myDocumentStyleOptions = strArray3;
      // ISSUE: explicit constructor call
      //base.\u002Ector();
      this.InitializeComponent();
      this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockWorkOrderListing");
      this.myDBSetting = dbSetting;
      this.myCommand = StockWorkOrderCommand.Create(dbSetting);
      this.previewButton1.ReportType = "Stock Work Order Document";
      this.previewButton1.SetDBSetting(this.myDBSetting);
      this.printButton1.ReportType = "Stock Work Order Document";
      this.printButton1.SetDBSetting(this.myDBSetting);
      this.myDataTable = new DataTable();
      this.gridControl1.DataSource = (object) this.myDataTable;
      this.LoadCriteria();
      this.ucSearchResult1.Initialize(this.gridView1, "ToBeUpdate");
      CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gridView1, new EventHandler(this.ReloadAllColumns));
      this.InitUserControls();
      this.InitFormControls();
      this.cbEditGroupBy.Properties.Items.AddRange((object[]) this.myGroupBy);
      this.cbEditSortBy.Properties.Items.AddRange((object[]) this.mySortBy);
      this.cbEditReportType.Properties.Items.AddRange((object[]) this.myDocumentStyleOptions);
      this.cbEditReportType.SelectedIndex = 1;
      new UDFUtil(this.myDBSetting).SetupListingReportGrid(this.gridView1, "RPA_WO");
      this.RefreshDesignReport();
      this.myMouseDownHelper = new MouseDownHelper();
      this.myMouseDownHelper.Init(this.gridView1);
      this.myCommand.UserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.RefreshDesignReport), (Component) this);
      BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Work_Order.htm");
      DBSetting dbSetting1 = dbSetting;
      string docType = "";
      long docKey = 0L;
      long eventKey = 0L;
      // ISSUE: variable of a boxed type
      StockWorkOrderString local =  StockWorkOrderString.OpenedPrintStockWorkOrderListing;
      object[] objArray = new object[1];
      int index9 = 0;
      string loginUserId = this.myCommand.UserAuthentication.LoginUserID;
      objArray[index9] = (object) loginUserId;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      string detail = "";
      Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
            string fieldname1 = "DocDate";
            string fieldtype1 = "Date";
            formControlUtil.AddField(fieldname1, fieldtype1);
            string fieldname2 = "ActQty";
            string fieldtype2 = "Quantity";
            formControlUtil.AddField(fieldname2, fieldtype2);
            string fieldname3 = "EstQty";
            string fieldtype3 = "Quantity";
            formControlUtil.AddField(fieldname3, fieldtype3);
            string fieldname4 = "EstHPP";
            string fieldtype4 = "Currency";
            formControlUtil.AddField(fieldname4, fieldtype4);
            string fieldname5 = "ActHPP";
            string fieldtype5 = "Currency";
            formControlUtil.AddField(fieldname5, fieldtype5);
            string fieldname6 = "LastModified";
            string fieldtype6 = "DateTime";
            formControlUtil.AddField(fieldname6, fieldtype6);
            string fieldname7 = "CreatedTimeStamp";
            string fieldtype7 = "DateTime";
            formControlUtil.AddField(fieldname7, fieldtype7);
            string fieldname8 = "ProductionDate";
            string fieldtype8 = "DateTime";
            formControlUtil.AddField(fieldname8, fieldtype8);
            string fieldname9 = "TargetDate";
            string fieldtype9 = "DateTime";
            formControlUtil.AddField(fieldname9, fieldtype9);
            FormStockWorkOrderPrintListing orderPrintListing = this;
      formControlUtil.InitControls((Control) orderPrintListing);
      this.InitGroupSummary();
    }

    private void InitGroupSummary()
    {
      this.gridView1.GroupSummary.Clear();
      GridGroupSummaryItemCollection groupSummary1 = this.gridView1.GroupSummary;
      int num1 = 3;
      string fieldName1 = "DocNo";
      // ISSUE: variable of the null type
      GridColumn local1 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local2 = GridGroupSummaryItemStringId.Count;
      object[] objArray1 = new object[1];
      int index1 = 0;
      string str = "{0}";
      objArray1[index1] = (object) str;
      string string1 = BCE.Localization.Localizer.GetString((Enum) local2, objArray1);
      groupSummary1.Add((SummaryItemType) num1, fieldName1, (GridColumn) local1, string1);
      //if (this.colActHPP.Visible)
      //{
      //  GridGroupSummaryItemCollection groupSummary2 = this.gridView1.GroupSummary;
      //  int num2 = 0;
      //  string fieldName2 = "NetTotal";
      //  // ISSUE: variable of the null type
      //  GridColumn local3 = null;
      //  // ISSUE: variable of a boxed type
      //  GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.NetTotal;
      //  object[] objArray2 = new object[1];
      //  int index2 = 0;
      //  string currencyFormatString = this.myCommand.DecimalSetting.GetCurrencyFormatString(0);
      //  objArray2[index2] = (object) currencyFormatString;
      //  string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
      //  groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      //}
      //if (this.colEstHPP.Visible)
      //{
      //  GridGroupSummaryItemCollection groupSummary2 = this.gridView1.GroupSummary;
      //  int num2 = 0;
      //  string fieldName2 = "Total";
      //  // ISSUE: variable of the null type
      //  GridColumn local3 = null;
      //  // ISSUE: variable of a boxed type
      //  GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.Total;
      //  object[] objArray2 = new object[1];
      //  int index2 = 0;
      //  string currencyFormatString = this.myCommand.DecimalSetting.GetCurrencyFormatString(0);
      //  objArray2[index2] = (object) currencyFormatString;
      //  string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
      //  groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      //}
      //if (this.colEstQty.Visible)
      //{
      //  GridGroupSummaryItemCollection groupSummary2 = this.gridView1.GroupSummary;
      //  int num2 = 0;
      //  string fieldName2 = "AssemblyCost";
      //  // ISSUE: variable of the null type
      //  GridColumn local3 = null;
      //  // ISSUE: variable of a boxed type
      //  GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.AssemblyCost;
      //  object[] objArray2 = new object[1];
      //  int index2 = 0;
      //  string costFormatString = this.myCommand.DecimalSetting.GetCostFormatString(0);
      //  objArray2[index2] = (object) costFormatString;
      //  string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
      //  groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      //}
    }

    private void SaveCriteria()
    {
      PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting) this.myReportingCriteria, "StockWorkOrderDocumentListingReport.setting");
    }

    private void LoadCriteria()
    {
      try
      {
        this.myReportingCriteria = (StockWorkOrderReportingCriteria) PersistenceUtil.LoadCriteriaData(this.myDBSetting, "StockWorkOrderDocumentListingReport.setting");
      }
      catch
      {
      }
      if (this.myReportingCriteria == null)
        this.myReportingCriteria = new StockWorkOrderReportingCriteria();
      this.cbEditGroupBy.EditValue = (object) this.myReportingCriteria.GroupBy;
      this.cbEditSortBy.EditValue = (object) this.myReportingCriteria.SortBy;
      this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
      this.cbCancelledOption.SelectedIndex = (int) this.myReportingCriteria.IsPrintCancelled;
      this.chkEditShowCriteria.Checked = this.myReportingCriteria.IsShowCriteria;
    }

    private void InitUserControls()
    {
      this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
      this.ucStockWorkOrderSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter,false);
    }

    private void AddNewColumn()
    {
      this.myDataTable.Columns.Add(new DataColumn()
      {
        DataType = typeof (bool),
        AllowDBNull = true,
        Caption = "Check",
        ColumnName = "ToBeUpdate",
        DefaultValue = (object) false
      });
    }

    private void BasicSearch(bool isSearchAll)
    {
      if (!this.myInSearch)
      {
        this.myInSearch = true;
        BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
        this.gridControl1.DataSource = (object) null;
        try
        {
          this.gridControl1.MainView.UpdateCurrentRow();
          this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
          this.myCommand.DocumentListingBasicSearch(this.myReportingCriteria, CommonFunction.BuildSQLColumns(isSearchAll, this.gridView1.Columns.View), this.myDataTable, "ToBeUpdate",false);
          if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
            this.AddNewColumn();
          this.gridControl1.DataSource = (object) this.myDataTable;
        }
        finally
        {
          this.myInSearch = false;
        }
        FormStockWorkOrderPrintListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockWorkOrderPrintListing.FormInquiryEventArgs(this, this.myDataTable);
        ScriptObject scriptObject = this.myScriptObject;
        string name = "OnFormInquiry";
        System.Type[] types = new System.Type[1];
        int index1 = 0;
        System.Type type = inquiryEventArgs1.GetType();
        types[index1] = type;
        object[] objArray = new object[1];
        int index2 = 0;
        FormStockWorkOrderPrintListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
        objArray[index2] = (object) inquiryEventArgs2;
        scriptObject.RunMethod(name, types, objArray);
      }
    }

    private void AdvanceSearch()
    {
      if (this.myCriteria == null)
        this.myCriteria = new AdvancedStockWorkOrderCriteria(this.myDBSetting);
      this.myCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
      using (FormAdvancedSearch formAdvancedSearch = new FormAdvancedSearch((SearchCriteria) this.myCriteria, this.myDBSetting))
      {
        if (formAdvancedSearch.ShowDialog((IWin32Window) this) == DialogResult.OK)
        {
          if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
            this.AddNewColumn();
          this.memoEdit_Criteria.Lines = this.myCriteria.BuildReadableTextArray();
          this.ucSearchResult1.KeepSearchResult = this.myCriteria.KeepSearchResult;
          Cursor current = Cursor.Current;
          Cursor.Current = Cursors.WaitCursor;
          this.myInSearch = true;
          try
          {
            this.myCommand.AdvanceSearch(this.myCriteria, CommonFunction.BuildSQLColumns(false, this.gridView1.Columns.View), this.myDataTable, "ToBeUpdate");
          }
          catch (AppException ex)
          {
            AppMessage.ShowErrorMessage(ex.Message);
          }
          finally
          {
            this.ucSearchResult1.CheckAll();
            this.myInSearch = false;
            Cursor.Current = current;
          }
          FormStockWorkOrderPrintListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockWorkOrderPrintListing.FormInquiryEventArgs(this, this.myDataTable);
          ScriptObject scriptObject = this.myScriptObject;
          string name = "OnFormInquiry";
          System.Type[] types = new System.Type[1];
          int index1 = 0;
          System.Type type = inquiryEventArgs1.GetType();
          types[index1] = type;
          object[] objArray = new object[1];
          int index2 = 0;
          FormStockWorkOrderPrintListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
          objArray[index2] = (object) inquiryEventArgs2;
          scriptObject.RunMethod(name, types, objArray);
        }
      }
    }

    private string GenerateDocNosToString()
    {
      BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
      DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = True");
      if (dataRowArray.Length == 0)
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderSelected, new object[0]));
        this.DialogResult = DialogResult.None;
        return (string) null;
      }
      else
      {
        string str = "";
        foreach (DataRow dataRow in dataRowArray)
        {
          if (str.Length != 0)
            str = str + ", ";
          str = str + "'" + dataRow["DocNo"].ToString() + "'";
        }
        return str;
      }
    }

    private void simpleButtonAdvanceSearch_Click(object sender, EventArgs e)
    {
      this.AdvanceSearch();
    }

    private void FormStockAssemblyList_Closing(object sender, CancelEventArgs e)
    {
      this.SaveCriteria();
    }

    private void ReloadAllColumns(object sender, EventArgs e)
    {
      this.BasicSearch(true);
    }

    private void sbtnInquiry_Click(object sender, EventArgs e)
    {
      this.BasicSearch(false);
      if (this.cbEditSortBy.Text != "")
      {
        if (this.cbEditSortBy.Text == "Date")
        {
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        }
        else
        {
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        }
      }
      this.ucSearchResult1.CheckAll();
      this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
      this.sbtnToggleOptions.Enabled = this.gridControl1.DataSource != null;
      this.gridControl1.Focus();
      if (this.cbEditReportType.SelectedIndex == 0)
      {
        DBSetting dbSetting = this.myDBSetting;
        string docType = "";
        long docKey = 0L;
        long eventKey = 0L;
        // ISSUE: variable of a boxed type
        StockWorkOrderString local =  StockWorkOrderString.InquiredBatchPrintStockWorkOrder;
        object[] objArray = new object[1];
        int index = 0;
        string loginUserId = this.myCommand.UserAuthentication.LoginUserID;
        objArray[index] = (object) loginUserId;
        string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
        string text = this.memoEdit_Criteria.Text;
        Activity.Log(dbSetting, docType, docKey, eventKey, @string, text);
      }
      else
      {
        DBSetting dbSetting = this.myDBSetting;
        string docType = "";
        long docKey = 0L;
        long eventKey = 0L;
        // ISSUE: variable of a boxed type
        StockWorkOrderString local =  StockWorkOrderString.InquiredPrintStockWorkOrderListing;
        object[] objArray = new object[1];
        int index = 0;
        string loginUserId = this.myCommand.UserAuthentication.LoginUserID;
        objArray[index] = (object) loginUserId;
        string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
        string text = this.memoEdit_Criteria.Text;
        Activity.Log(dbSetting, docType, docKey, eventKey, @string, text);
      }
    }

    private void cbEditGroupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.GroupBy = this.cbEditGroupBy.Text;
    }

    private void cbEditSortBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
      {
        if (this.myReportingCriteria.SortBy != this.cbEditSortBy.Text)
          this.myReportingCriteria.SortBy = this.cbEditSortBy.Text;
        if (this.cbEditSortBy.Text != "")
        {
          if (this.cbEditSortBy.Text == "Date")
          {
            this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
            this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          }
          else
          {
            this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
            this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          }
        }
      }
    }

    private void cbEditReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.myReportingCriteria.ReportTypeOption = this.cbEditReportType.SelectedIndex;
      FormStockWorkOrderPrintListing orderPrintListing = this;
      // ISSUE: variable of a boxed type
      StockAssemblyOrderStringId local =  StockAssemblyOrderStringId.Code_PrintStockAssemblyOrder;
      object[] objArray = new object[1];
      int index = 0;
      string text = this.cbEditReportType.Text;
      objArray[index] = (object) text;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      orderPrintListing.Text = @string;
      if (this.cbEditReportType.SelectedIndex == 0)
      {
        this.previewButton1.ReportType = "Stock Work Order Document";
        this.previewButton1.SetDBSetting(this.myDBSetting);
        this.printButton1.ReportType = "Stock Work Order Document";
        this.printButton1.SetDBSetting(this.myDBSetting);
      }
      else
      {
        this.previewButton1.ReportType = "Stock Work Order Listing";
        this.previewButton1.SetDBSetting(this.myDBSetting);
        this.printButton1.ReportType = "Stock Work Order Listing";
        this.printButton1.SetDBSetting(this.myDBSetting);
      }
    }

    private void cbCancelledOption_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.IsPrintCancelled = (CancelledDocumentOption) this.cbCancelledOption.SelectedIndex;
    }

    private void chkEditShowCriteria_CheckedChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
    }

    private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      GridView gridView = (GridView) sender;
      int[] selectedRows = gridView.GetSelectedRows();
      if (selectedRows != null && selectedRows.Length != 1)
      {
        for (int index = 0; index < selectedRows.Length; ++index)
          gridView.GetDataRow(selectedRows[index])["ToBeUpdate"] = (object) true;
      }
    }

    private string GetSelectedDocKeys()
    {
      BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
      if (this.gridView1.FocusedRowHandle >= 0 && this.myDataTable.Select("ToBeUpdate = True").Length != 0)
      {
        this.mySelectedDocKeylist = DataTableHelper.GetSelectedKeyArray("ToBeUpdate", "DocKey", this.myDataTable);
        return this.SelectedDocKeysInString;
      }
      else
        return "";
    }

    private void sbtnToggleOptions_Click(object sender, EventArgs e)
    {
      this.panelCriteria.Visible = !this.panelCriteria.Visible;
      if (this.panelCriteria.Visible)
        this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_HideOptions, new object[0]);
      else
        this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowOptions, new object[0]);
    }

    private void sbtnClose_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void FormStockAssemblyPrintSearch_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == (Keys) 120)
      {
        if (this.sbtnInquiry.Enabled)
          this.sbtnInquiry.PerformClick();
      }
      else if (e.KeyCode == (Keys) 119)
      {
        if (this.previewButton1.Enabled)
          this.previewButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 118)
      {
        if (this.printButton1.Enabled)
          this.printButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 117 && this.sbtnToggleOptions.Enabled)
        this.sbtnToggleOptions.PerformClick();
    }

    private void gridView1_Layout(object sender, EventArgs e)
    {
      if (this.gridControl1.DataSource != null)
        this.InitGroupSummary();
    }

    private void repositoryItemTextEdit_Cancelled_FormatEditValue(object sender, ConvertEditValueEventArgs e)
    {
      if (e.Value != null)
      {
        if (e.Value.ToString() == "T")
          e.Value = (object) BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Cancelled, new object[0]);
        else
          e.Value = (object) "";
      }
    }

    private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      string selectedDocKeys = this.GetSelectedDocKeys();
      if (selectedDocKeys.Length == 0)
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderSelected, new object[0]));
        this.DialogResult = DialogResult.None;
      }
      else if (this.cbEditReportType.SelectedIndex == 0)
      {
        if (this.myCommand.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DOC_REPORT_PREVIEW", (XtraForm) this))
        {
          if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
          {
            foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
            {
              if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
              {
                AccessRight accessRight = this.myCommand.UserAuthentication.AccessRight;
                string cmdID = "RPA_WO_PRINTED_PREVIEW";
                FormStockWorkOrderPrintListing orderPrintListing = this;
                // ISSUE: variable of a boxed type
                StockStringId local =  StockStringId.NoPermissionToPreviewPrintedDocument;
                object[] objArray = new object[1];
                int index = 0;
                string str = dataRow["DocNo"].ToString();
                objArray[index] = (object) str;
                string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
                if (!accessRight.IsAccessible(cmdID, (XtraForm) orderPrintListing, @string))
                  return;
              }
            }
          }
          ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.BatchStockWorkOrder, new object[0]), "RPA_WO_DOC_REPORT_PRINT", "RPA_WO_DOC_REPORT_EXPORT", this.memoEdit_Criteria.Text);
          reportInfo.UpdatePrintCountTableName = "RPA_WO";
          reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
          reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
                    ReportTool.PreviewReport("Stock Work Order Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, false, this.myCommand.ReportOption, reportInfo);

                  //  ReportTool.PreviewReport("Stock Work Order Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, false, this.myCommand.ReportOption, reportInfo);
        }
      }
      else if (this.cbEditReportType.SelectedIndex == 1 && this.myCommand.UserAuthentication.AccessRight.IsAccessible("RPA_WO_LISTING_REPORT_PREVIEW", (XtraForm) this))
        ReportTool.PreviewReport("Stock Work Order Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.StockWorkOrderReportingCriteria), this.myDBSetting, e.DefaultReport, false, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.StockWorkOrderListing, new object[0]), "RPA_WO_LISTING_REPORT_PRINT", "RPA_WO_LISTING_REPORT_EXPORT", this.memoEdit_Criteria.Text)
        {
          UpdatePrintCountTableName = "RPA_WO"
        });
    }

    private bool CheckBeforePrint(ReportInfo reportInfo)
    {
      if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
      {
        foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
        {
          if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
          {
            AccessRight accessRight = this.myCommand.UserAuthentication.AccessRight;
            string cmdID = "RPA_WO_PRINTED_PRINT";
            FormStockWorkOrderPrintListing orderPrintListing = this;
            // ISSUE: variable of a boxed type
            StockStringId local =  StockStringId.NoPermissionToPrintPrintedDocument;
            object[] objArray = new object[1];
            int index = 0;
            string str = dataRow["DocNo"].ToString();
            objArray[index] = (object) str;
            string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
            if (!accessRight.IsAccessible(cmdID, (XtraForm) orderPrintListing, @string))
              return false;
          }
        }
      }
      return true;
    }

    private bool CheckBeforeExport(ReportInfo reportInfo)
    {
      if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
      {
        foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
        {
          if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
          {
            AccessRight accessRight = this.myCommand.UserAuthentication.AccessRight;
            string cmdID = "RPA_WO_PRINTED_EXPORT";
            FormStockWorkOrderPrintListing orderPrintListing = this;
            // ISSUE: variable of a boxed type
            StockStringId local =  StockStringId.NoPermissionToExportPrintedDocument;
            object[] objArray = new object[1];
            int index = 0;
            string str = dataRow["DocNo"].ToString();
            objArray[index] = (object) str;
            string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
            if (!accessRight.IsAccessible(cmdID, (XtraForm) orderPrintListing, @string))
              return false;
          }
        }
      }
      return true;
    }

    private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      string selectedDocKeys = this.GetSelectedDocKeys();
      if (selectedDocKeys.Length == 0)
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderSelected, new object[0]));
        this.DialogResult = DialogResult.None;
      }
      else if (this.cbEditReportType.SelectedIndex == 0)
      {
        if (this.myCommand.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DOC_REPORT_PRINT", (XtraForm) this))
        {
          if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
          {
            foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
            {
              if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
              {
                AccessRight accessRight = this.myCommand.UserAuthentication.AccessRight;
                string cmdID = "RPA_WO_PRINTED_PRINT";
                FormStockWorkOrderPrintListing orderPrintListing = this;
                // ISSUE: variable of a boxed type
                StockStringId local =  StockStringId.NoPermissionToPrintPrintedDocument;
                object[] objArray = new object[1];
                int index = 0;
                string str = dataRow["DocNo"].ToString();
                objArray[index] = (object) str;
                string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
                if (!accessRight.IsAccessible(cmdID, (XtraForm) orderPrintListing, @string))
                  return;
              }
            }
          }
          //ReportTool.PrintReport("Stock Work Order Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.BatchStockWorkOrder, new object[0]), "RPA_WO_DOC_REPORT_PRINT", "RPA_WO_DOC_REPORT_EXPORT", this.memoEdit_Criteria.Text)
                  ReportTool.PrintReport("Stock Work Order Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum)StockWorkOrderString.BatchStockWorkOrder, new object[0]), "RPA_WO_DOC_REPORT_PRINT", "RPA_WO_DOC_REPORT_EXPORT", "")
                  {
                      UpdatePrintCountTableName = "RPA_WO"
          });
        }
      }
      else if (this.cbEditReportType.SelectedIndex == 1 && this.myCommand.UserAuthentication.AccessRight.IsAccessible("RPA_WO_LISTING_REPORT_PRINT", (XtraForm) this))
        ReportTool.PrintReport("Stock Work Order Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.StockWorkOrderReportingCriteria), this.myDBSetting, e.DefaultReport, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.StockWorkOrderListing, new object[0]), "RPA_WO_LISTING_REPORT_PRINT", "RPA_WO_LISTING_REPORT_EXPORT", this.memoEdit_Criteria.Text)
        {
          UpdatePrintCountTableName = "RPA_WO"
        });
    }

    private void barBtnDesignDocumentStyleReport_ItemClick(object sender, ItemClickEventArgs e)
    {
            string selectedDocKeys = this.GetSelectedDocKeys();
           // ReportTool.DesignReport("Stock Work Order Document", this.myDBSetting);
            ReportTool.DesignReport("Stock Work Order Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting);

        }

        private void barBtnDesignListingStyleReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            string selectedDocKeys = this.GetSelectedDocKeys();
            if (selectedDocKeys.Length == 0)
            {
                AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString(StockReceiveStringId.ShowMessage_StockReceiveNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            ReportTool.DesignReport("Stock Work Order Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.StockWorkOrderReportingCriteria), this.myDBSetting);
        }

        public virtual void RefreshDesignReport()
    {
      bool show = this.myCommand.UserAuthentication.AccessRight.IsAccessible("TOOLS_RPT_SHOW");
      this.barBtnDesignDocumentStyleReport.Visibility = XtraBarsUtils.ToBarItemVisibility(show);
      this.barBtnDesignListingStyleReport.Visibility = XtraBarsUtils.ToBarItemVisibility(show);
    }

    private void FormSAOPrintSearch_Load(object sender, EventArgs e)
    {
      this.FormInitialize();
    }

    private void FormInitialize()
    {
      FormStockWorkOrderPrintListing.FormInitializeEventArgs initializeEventArgs1 = new FormStockWorkOrderPrintListing.FormInitializeEventArgs(this);
      ScriptObject scriptObject = this.myScriptObject;
      string name = "OnFormInitialize";
      System.Type[] types = new System.Type[1];
      int index1 = 0;
      System.Type type = initializeEventArgs1.GetType();
      types[index1] = type;
      object[] objArray = new object[1];
      int index2 = 0;
      FormStockWorkOrderPrintListing.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
      objArray[index2] = (object) initializeEventArgs2;
      scriptObject.RunMethod(name, types, objArray);
    }

    private void gridView1_DoubleClick(object sender, EventArgs e)
    {
      if (this.myMouseDownHelper.IsLeftMouseDown && BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView) sender).InRow && UserAuthentication.GetOrCreate(this.myDBSetting).AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
        this.GoToDocument();
    }

    private void GoToDocument()
    {
      ColumnView columnView = (ColumnView) this.gridControl1.FocusedView;
      int focusedRowHandle = columnView.FocusedRowHandle;
      DataRow dataRow = columnView.GetDataRow(focusedRowHandle);
      if (dataRow != null)
        DocumentDispatcher.Open(this.myDBSetting, "AO", BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
    }

    protected override void Dispose(bool disposing)
    {
      this.SaveCriteria();
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            this.panelCriteria = new DevExpress.XtraEditors.PanelControl();
            this.cbEditReportType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox_SearchCriteria = new DevExpress.XtraEditors.GroupControl();
            this.ucStockWorkOrderSelector1 = new Production.WorkOrder.UCWOSelector();
            this.cbCancelledOption = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ucDateSelector1 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.simpleButtonAdvanceSearch = new DevExpress.XtraEditors.SimpleButton();
            this.gbReportOption = new DevExpress.XtraEditors.GroupControl();
            this.chkEditShowCriteria = new DevExpress.XtraEditors.CheckEdit();
            this.cbEditGroupBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbEditSortBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colProductionStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductionEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit_Cancelled = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCreatedTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ucSearchResult1 = new BCE.AutoCount.FilterUI.UCSearchResult();
            this.tabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit_Criteria = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.colProjNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpCompletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel_Center = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.printButton1 = new BCE.AutoCount.Controls.PrintButton();
            this.previewButton1 = new BCE.AutoCount.Controls.PreviewButton();
            this.sbtnToggleOptions = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnDesignDocumentStyleReport = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnDesignListingStyleReport = new DevExpress.XtraBars.BarButtonItem();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).BeginInit();
            this.panelCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditReportType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox_SearchCriteria)).BeginInit();
            this.groupBox_SearchCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbReportOption)).BeginInit();
            this.gbReportOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit_Cancelled)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Center)).BeginInit();
            this.panel_Center.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCriteria
            // 
            this.panelCriteria.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCriteria.Controls.Add(this.cbEditReportType);
            this.panelCriteria.Controls.Add(this.label9);
            this.panelCriteria.Controls.Add(this.groupBox_SearchCriteria);
            this.panelCriteria.Controls.Add(this.gbReportOption);
            this.panelCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCriteria.Location = new System.Drawing.Point(0, 66);
            this.panelCriteria.Name = "panelCriteria";
            this.panelCriteria.Size = new System.Drawing.Size(889, 142);
            this.panelCriteria.TabIndex = 2;
            // 
            // cbEditReportType
            // 
            this.cbEditReportType.EditValue = "Print Work Order Listing";
            this.cbEditReportType.Location = new System.Drawing.Point(93, 3);
            this.cbEditReportType.Name = "cbEditReportType";
            this.cbEditReportType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditReportType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditReportType.Size = new System.Drawing.Size(191, 20);
            this.cbEditReportType.TabIndex = 0;
            this.cbEditReportType.SelectedIndexChanged += new System.EventHandler(this.cbEditReportType_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(12, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "Report Type";
            // 
            // groupBox_SearchCriteria
            // 
            this.groupBox_SearchCriteria.Controls.Add(this.ucStockWorkOrderSelector1);
            this.groupBox_SearchCriteria.Controls.Add(this.cbCancelledOption);
            this.groupBox_SearchCriteria.Controls.Add(this.label4);
            this.groupBox_SearchCriteria.Controls.Add(this.label6);
            this.groupBox_SearchCriteria.Controls.Add(this.label3);
            this.groupBox_SearchCriteria.Controls.Add(this.ucDateSelector1);
            this.groupBox_SearchCriteria.Controls.Add(this.simpleButtonAdvanceSearch);
            this.groupBox_SearchCriteria.Location = new System.Drawing.Point(0, 26);
            this.groupBox_SearchCriteria.Name = "groupBox_SearchCriteria";
            this.groupBox_SearchCriteria.Size = new System.Drawing.Size(494, 110);
            this.groupBox_SearchCriteria.TabIndex = 2;
            this.groupBox_SearchCriteria.Text = "Filter Option";
            // 
            // ucStockWorkOrderSelector1
            // 
            this.ucStockWorkOrderSelector1.Location = new System.Drawing.Point(115, 46);
            this.ucStockWorkOrderSelector1.Name = "ucStockWorkOrderSelector1";
            this.ucStockWorkOrderSelector1.Size = new System.Drawing.Size(362, 24);
            this.ucStockWorkOrderSelector1.TabIndex = 0;
            // 
            // cbCancelledOption
            // 
            this.cbCancelledOption.EditValue = "Show Uncancelled";
            this.cbCancelledOption.Location = new System.Drawing.Point(118, 72);
            this.cbCancelledOption.Name = "cbCancelledOption";
            this.cbCancelledOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCancelledOption.Properties.Items.AddRange(new object[] {
            "Show All",
            "Show Cancelled",
            "Show Uncancelled"});
            this.cbCancelledOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbCancelledOption.Size = new System.Drawing.Size(127, 20);
            this.cbCancelledOption.TabIndex = 1;
            this.cbCancelledOption.SelectedIndexChanged += new System.EventHandler(this.cbCancelledOption_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 19);
            this.label4.TabIndex = 2;
            this.label4.Text = "Document Date";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 16);
            this.label6.TabIndex = 3;
            this.label6.Text = "Document No.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cancelled Status";
            // 
            // ucDateSelector1
            // 
            this.ucDateSelector1.Appearance.Options.UseBackColor = true;
            this.ucDateSelector1.Location = new System.Drawing.Point(118, 26);
            this.ucDateSelector1.Name = "ucDateSelector1";
            this.ucDateSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucDateSelector1.TabIndex = 5;
            // 
            // simpleButtonAdvanceSearch
            // 
            this.simpleButtonAdvanceSearch.Location = new System.Drawing.Point(367, 78);
            this.simpleButtonAdvanceSearch.Name = "simpleButtonAdvanceSearch";
            this.simpleButtonAdvanceSearch.Size = new System.Drawing.Size(113, 23);
            this.simpleButtonAdvanceSearch.TabIndex = 6;
            this.simpleButtonAdvanceSearch.Text = "Advanced Filter...";
            this.simpleButtonAdvanceSearch.Click += new System.EventHandler(this.simpleButtonAdvanceSearch_Click);
            // 
            // gbReportOption
            // 
            this.gbReportOption.Controls.Add(this.chkEditShowCriteria);
            this.gbReportOption.Controls.Add(this.cbEditGroupBy);
            this.gbReportOption.Controls.Add(this.label8);
            this.gbReportOption.Controls.Add(this.label7);
            this.gbReportOption.Controls.Add(this.cbEditSortBy);
            this.gbReportOption.Location = new System.Drawing.Point(500, 26);
            this.gbReportOption.Name = "gbReportOption";
            this.gbReportOption.Size = new System.Drawing.Size(247, 110);
            this.gbReportOption.TabIndex = 3;
            this.gbReportOption.Text = "Report Options";
            // 
            // chkEditShowCriteria
            // 
            this.chkEditShowCriteria.Location = new System.Drawing.Point(80, 82);
            this.chkEditShowCriteria.Name = "chkEditShowCriteria";
            this.chkEditShowCriteria.Properties.Caption = "Show Criteria In Report";
            this.chkEditShowCriteria.Size = new System.Drawing.Size(159, 19);
            this.chkEditShowCriteria.TabIndex = 0;
            this.chkEditShowCriteria.CheckedChanged += new System.EventHandler(this.chkEditShowCriteria_CheckedChanged);
            // 
            // cbEditGroupBy
            // 
            this.cbEditGroupBy.Location = new System.Drawing.Point(82, 30);
            this.cbEditGroupBy.Name = "cbEditGroupBy";
            this.cbEditGroupBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditGroupBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditGroupBy.Size = new System.Drawing.Size(154, 20);
            this.cbEditGroupBy.TabIndex = 1;
            this.cbEditGroupBy.SelectedIndexChanged += new System.EventHandler(this.cbEditGroupBy_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(17, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 18);
            this.label8.TabIndex = 2;
            this.label8.Text = "Order By";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(17, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 18);
            this.label7.TabIndex = 3;
            this.label7.Text = "Group By";
            // 
            // cbEditSortBy
            // 
            this.cbEditSortBy.Location = new System.Drawing.Point(82, 53);
            this.cbEditSortBy.Name = "cbEditSortBy";
            this.cbEditSortBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditSortBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditSortBy.Size = new System.Drawing.Size(154, 20);
            this.cbEditSortBy.TabIndex = 4;
            this.cbEditSortBy.SelectedIndexChanged += new System.EventHandler(this.cbEditSortBy_SelectedIndexChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabPage = this.tabPage1;
            this.tabControl1.Size = new System.Drawing.Size(889, 234);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPage1,
            this.tabPage2});
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gridControl1);
            this.tabPage1.Controls.Add(this.ucSearchResult1);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(883, 206);
            this.tabPage1.Text = "Result";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 46);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(883, 160);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCheck,
            this.colProductionStartDate,
            this.colProductionEndDate,
            this.colLocation,
            this.colMachineCode,
            this.colCancelled,
            this.colCreatedTimeStamp,
            this.colCreatedUserID,
            this.colStatus,
            this.colDescription,
            this.colDocDate,
            this.colDocNo,
            this.colLastModified,
            this.colLastModifiedUserID,
            this.colPrintCount,
            this.colRefDocNo,
            this.colRemark1,
            this.colRemark2,
            this.colRemark3,
            this.colRemark4});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.Layout += new System.EventHandler(this.gridView1_Layout);
            // 
            // colCheck
            // 
            this.colCheck.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCheck.FieldName = "ToBeUpdate";
            this.colCheck.Name = "colCheck";
            this.colCheck.Visible = true;
            this.colCheck.VisibleIndex = 0;
            this.colCheck.Width = 77;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colProductionStartDate
            // 
            this.colProductionStartDate.Caption = "Prod. Start Date";
            this.colProductionStartDate.FieldName = "ProductionStartDate";
            this.colProductionStartDate.Name = "colProductionStartDate";
            this.colProductionStartDate.Visible = true;
            this.colProductionStartDate.VisibleIndex = 3;
            // 
            // colProductionEndDate
            // 
            this.colProductionEndDate.Caption = "Prod. End Date";
            this.colProductionEndDate.FieldName = "ProductionEndDate";
            this.colProductionEndDate.Name = "colProductionEndDate";
            this.colProductionEndDate.Visible = true;
            this.colProductionEndDate.VisibleIndex = 4;
            // 
            // colLocation
            // 
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            // 
            // colMachineCode
            // 
            this.colMachineCode.Caption = "MachineCode";
            this.colMachineCode.FieldName = "MachineCode";
            this.colMachineCode.Name = "colMachineCode";
            this.colMachineCode.OptionsColumn.AllowEdit = false;
            this.colMachineCode.Visible = true;
            this.colMachineCode.VisibleIndex = 6;
            this.colMachineCode.Width = 77;
            // 
            // colCancelled
            // 
            this.colCancelled.ColumnEdit = this.repositoryItemTextEdit_Cancelled;
            this.colCancelled.FieldName = "Cancelled";
            this.colCancelled.Name = "colCancelled";
            this.colCancelled.OptionsColumn.AllowEdit = false;
            this.colCancelled.Visible = true;
            this.colCancelled.VisibleIndex = 7;
            this.colCancelled.Width = 77;
            // 
            // repositoryItemTextEdit_Cancelled
            // 
            this.repositoryItemTextEdit_Cancelled.Name = "repositoryItemTextEdit_Cancelled";
            this.repositoryItemTextEdit_Cancelled.FormatEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repositoryItemTextEdit_Cancelled_FormatEditValue);
            // 
            // colCreatedTimeStamp
            // 
            this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
            this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
            this.colCreatedTimeStamp.OptionsColumn.AllowEdit = false;
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.Name = "colCreatedUserID";
            this.colCreatedUserID.OptionsColumn.AllowEdit = false;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 5;
            this.colStatus.Width = 77;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 8;
            this.colDescription.Width = 77;
            // 
            // colDocDate
            // 
            this.colDocDate.FieldName = "DocDate";
            this.colDocDate.Name = "colDocDate";
            this.colDocDate.OptionsColumn.AllowEdit = false;
            this.colDocDate.Visible = true;
            this.colDocDate.VisibleIndex = 2;
            this.colDocDate.Width = 77;
            // 
            // colDocNo
            // 
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.OptionsColumn.AllowEdit = false;
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 1;
            this.colDocNo.Width = 77;
            // 
            // colLastModified
            // 
            this.colLastModified.FieldName = "LastModified";
            this.colLastModified.Name = "colLastModified";
            this.colLastModified.OptionsColumn.AllowEdit = false;
            this.colLastModified.Visible = true;
            this.colLastModified.VisibleIndex = 10;
            this.colLastModified.Width = 53;
            // 
            // colLastModifiedUserID
            // 
            this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
            this.colLastModifiedUserID.Name = "colLastModifiedUserID";
            this.colLastModifiedUserID.OptionsColumn.AllowEdit = false;
            this.colLastModifiedUserID.Visible = true;
            this.colLastModifiedUserID.VisibleIndex = 9;
            this.colLastModifiedUserID.Width = 79;
            // 
            // colPrintCount
            // 
            this.colPrintCount.FieldName = "PrintCount";
            this.colPrintCount.Name = "colPrintCount";
            this.colPrintCount.OptionsColumn.AllowEdit = false;
            // 
            // colRefDocNo
            // 
            this.colRefDocNo.FieldName = "RefDocNo";
            this.colRefDocNo.Name = "colRefDocNo";
            this.colRefDocNo.OptionsColumn.AllowEdit = false;
            // 
            // colRemark1
            // 
            this.colRemark1.FieldName = "Remark1";
            this.colRemark1.Name = "colRemark1";
            this.colRemark1.OptionsColumn.AllowEdit = false;
            // 
            // colRemark2
            // 
            this.colRemark2.FieldName = "Remark2";
            this.colRemark2.Name = "colRemark2";
            this.colRemark2.OptionsColumn.AllowEdit = false;
            // 
            // colRemark3
            // 
            this.colRemark3.FieldName = "Remark3";
            this.colRemark3.Name = "colRemark3";
            this.colRemark3.OptionsColumn.AllowEdit = false;
            // 
            // colRemark4
            // 
            this.colRemark4.FieldName = "Remark4";
            this.colRemark4.Name = "colRemark4";
            this.colRemark4.OptionsColumn.AllowEdit = false;
            // 
            // ucSearchResult1
            // 
            this.ucSearchResult1.Appearance.Options.UseBackColor = true;
            this.ucSearchResult1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucSearchResult1.Location = new System.Drawing.Point(0, 0);
            this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ucSearchResult1.Name = "ucSearchResult1";
            this.ucSearchResult1.Size = new System.Drawing.Size(883, 46);
            this.ucSearchResult1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.memoEdit_Criteria);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(883, 206);
            this.tabPage2.Text = "Criteria";
            // 
            // memoEdit_Criteria
            // 
            this.memoEdit_Criteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit_Criteria.Location = new System.Drawing.Point(0, 0);
            this.memoEdit_Criteria.Name = "memoEdit_Criteria";
            this.memoEdit_Criteria.Properties.Appearance.Options.UseFont = true;
            this.memoEdit_Criteria.Properties.ReadOnly = true;
            this.memoEdit_Criteria.Size = new System.Drawing.Size(883, 206);
            this.memoEdit_Criteria.TabIndex = 0;
            this.memoEdit_Criteria.UseOptimizedRendering = true;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(883, 237);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // colProjNo
            // 
            this.colProjNo.FieldName = "ProjNo";
            this.colProjNo.Name = "colProjNo";
            // 
            // colExpCompletedDate
            // 
            this.colExpCompletedDate.FieldName = "ExpectedCompletedDate";
            this.colExpCompletedDate.Name = "colExpCompletedDate";
            // 
            // panel_Center
            // 
            this.panel_Center.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel_Center.Controls.Add(this.tabControl1);
            this.panel_Center.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Center.Location = new System.Drawing.Point(0, 248);
            this.panel_Center.Name = "panel_Center";
            this.panel_Center.Size = new System.Drawing.Size(889, 234);
            this.panel_Center.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.printButton1);
            this.panelControl1.Controls.Add(this.previewButton1);
            this.panelControl1.Controls.Add(this.sbtnToggleOptions);
            this.panelControl1.Controls.Add(this.sbtnClose);
            this.panelControl1.Controls.Add(this.sbtnInquiry);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 208);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(889, 40);
            this.panelControl1.TabIndex = 1;
            // 
            // printButton1
            // 
            this.printButton1.Location = new System.Drawing.Point(171, 6);
            this.printButton1.Name = "printButton1";
            this.printButton1.ReportType = "";
            this.printButton1.Size = new System.Drawing.Size(72, 23);
            this.printButton1.TabIndex = 0;
            this.printButton1.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // previewButton1
            // 
            this.previewButton1.Location = new System.Drawing.Point(93, 6);
            this.previewButton1.Name = "previewButton1";
            this.previewButton1.ReportType = "";
            this.previewButton1.Size = new System.Drawing.Size(72, 23);
            this.previewButton1.TabIndex = 1;
            this.previewButton1.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // sbtnToggleOptions
            // 
            this.sbtnToggleOptions.Location = new System.Drawing.Point(249, 6);
            this.sbtnToggleOptions.Name = "sbtnToggleOptions";
            this.sbtnToggleOptions.Size = new System.Drawing.Size(75, 23);
            this.sbtnToggleOptions.TabIndex = 2;
            this.sbtnToggleOptions.Text = "Hide Options";
            this.sbtnToggleOptions.Click += new System.EventHandler(this.sbtnToggleOptions_Click);
            // 
            // sbtnClose
            // 
            this.sbtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnClose.Location = new System.Drawing.Point(330, 6);
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Size = new System.Drawing.Size(75, 23);
            this.sbtnClose.TabIndex = 3;
            this.sbtnClose.Text = "Close";
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // sbtnInquiry
            // 
            this.sbtnInquiry.Location = new System.Drawing.Point(12, 6);
            this.sbtnInquiry.Name = "sbtnInquiry";
            this.sbtnInquiry.Size = new System.Drawing.Size(75, 23);
            this.sbtnInquiry.TabIndex = 4;
            this.sbtnInquiry.Text = "Inquiry";
            this.sbtnInquiry.Click += new System.EventHandler(this.sbtnInquiry_Click);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem2,
            this.barButtonItem1,
            this.barSubItem3,
            this.barButtonItem3,
            this.barButtonItem4});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 9;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem3)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Report";
            this.barSubItem3.Id = 6;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Design Document Report";
            this.barButtonItem3.Id = 7;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDocumentStyleReport_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Design Listing Report";
            this.barButtonItem4.Id = 8;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignListingStyleReport_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(889, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 482);
            this.barDockControlBottom.Size = new System.Drawing.Size(889, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 460);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(889, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 460);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Report";
            this.barSubItem2.Id = 3;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Design Document Report";
            this.barButtonItem1.Id = 4;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDocumentStyleReport_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnDesignDocumentStyleReport
            // 
            this.barBtnDesignDocumentStyleReport.Id = 1;
            this.barBtnDesignDocumentStyleReport.Name = "barBtnDesignDocumentStyleReport";
            this.barBtnDesignDocumentStyleReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDocumentStyleReport_ItemClick);
            // 
            // barBtnDesignListingStyleReport
            // 
            this.barBtnDesignListingStyleReport.Id = 2;
            this.barBtnDesignListingStyleReport.Name = "barBtnDesignListingStyleReport";
            this.barBtnDesignListingStyleReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignListingStyleReport_ItemClick);
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Print Work Order Listing";
            this.panelHeader1.HelpTopicId = "Stock_Work_Order.htm";
            this.panelHeader1.Location = new System.Drawing.Point(0, 22);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(889, 44);
            this.panelHeader1.TabIndex = 3;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = -1;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.Text = "Custom 1";
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 2";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.Text = "Custom 2";
            // 
            // FormStockWorkOrderPrintListing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnClose;
            this.ClientSize = new System.Drawing.Size(889, 482);
            this.Controls.Add(this.panel_Center);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelCriteria);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormStockWorkOrderPrintListing";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Print Work Order Listing";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormStockAssemblyList_Closing);
            this.Load += new System.EventHandler(this.FormSAOPrintSearch_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormStockAssemblyPrintSearch_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).EndInit();
            this.panelCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbEditReportType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox_SearchCriteria)).EndInit();
            this.groupBox_SearchCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbReportOption)).EndInit();
            this.gbReportOption.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit_Cancelled)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Center)).EndInit();
            this.panel_Center.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

    }

    public class FormEventArgs
    {
      private FormStockWorkOrderPrintListing myForm;

      public StockWorkOrderCommand Command
      {
        get
        {
          return this.myForm.myCommand;
        }
      }

      public PanelControl PanelCriteria
      {
        get
        {
          return this.myForm.panelCriteria;
        }
      }

      public PanelControl PanelButtons
      {
        get
        {
          return this.myForm.panelControl1;
        }
      }

      public XtraTabControl TabControl
      {
        get
        {
          return this.myForm.tabControl1;
        }
      }

      public GridControl GridControl
      {
        get
        {
          return this.myForm.gridControl1;
        }
      }

      public FormStockWorkOrderPrintListing Form
      {
        get
        {
          return this.myForm;
        }
      }

      public DBSetting DBSettig
      {
        get
        {
          return this.myForm.myDBSetting;
        }
      }

      public FormEventArgs(FormStockWorkOrderPrintListing form)
      {
        this.myForm = form;
      }
    }

    public class FormInitializeEventArgs : FormStockWorkOrderPrintListing.FormEventArgs
    {
      public FormInitializeEventArgs(FormStockWorkOrderPrintListing form)
        : base(form)
      {
      }
    }

    public class FormInquiryEventArgs : FormStockWorkOrderPrintListing.FormEventArgs
    {
      private DataTable myResultTable;

      public DataTable ResultTable
      {
        get
        {
          return this.myResultTable;
        }
      }

      public FormInquiryEventArgs(FormStockWorkOrderPrintListing form, DataTable resultTable)
        : base(form)
      {
        this.myResultTable = resultTable;
      }
    }
  }
}
