﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderRecord
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Common;
using BCE.AutoCount.Data;
using BCE.Data;
using BCE.Misc;
using System;
using System.Data;

namespace Production.WorkOrder
{
    public class StockWorkOrderRecord : BaseRecord
    {
        private StockWorkOrder myStock;
        protected DataRow[] myDetailRows;

        public int DetailCount
        {
            get
            {
                return this.myDetailRows.Length;
            }
        }

        public long DocKey
        {
            get
            {
                return BCE.Data.Convert.ToInt64(this.myRow["DocKey"]);
            }
        }

        public DBString DocNo
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["DocNo"]);
            }
            set
            {
                this.myRow["DocNo"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBDateTime DocDate
        {
            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myRow["DocDate"]);
            }
            set
            {
                if (value.HasValue)
                    this.myRow["DocDate"] = BCE.Data.Convert.ToDBObject((DBDateTime)value);
                else
                    this.myRow["DocDate"] = (object)DBNull.Value;
            }
        }
        public DBDateTime ProductionDate
        {
            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myRow["ProductionDate"]);
            }
            set
            {
                if (value.HasValue)
                    this.myRow["ProductionDate"] = BCE.Data.Convert.ToDBObject((DBDateTime)value);
                else
                    this.myRow["ProductionDate"] = (object)DBNull.Value;
            }
        }
        public DBDateTime TargetDate
        {
            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myRow["TargetDate"]);
            }
            set
            {
                if (value.HasValue)
                    this.myRow["TargetDate"] = BCE.Data.Convert.ToDBObject((DBDateTime)value);
                else
                    this.myRow["TargetDate"] = (object)DBNull.Value;
            }
        }
        public DBString Description
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Description"]);
            }
            set
            {
                this.myRow["Description"] = BCE.Data.Convert.ToDBObject(value);
            }
        }
        public DBString DocType
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["DocType"]);
            }
            set
            {
                this.myRow["DocType"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Status
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Status"]);
            }
            set
            {
                this.myRow["Status"] = BCE.Data.Convert.ToDBObject(value);
            }
        }
        public DBString BOMCode
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["BOMCode"]);
            }
            set
            {
                this.myRow["BOMCode"] = BCE.Data.Convert.ToDBObject(value);
            }
        }
       
        public DBString MachineCode
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["MachineCode"]);
            }
            set
            {
                this.myRow["MachineCode"] = BCE.Data.Convert.ToDBObject(value);
            }
        }
        public DBString ItemCode
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["ItemCode"]);
            }
            set
            {
                this.myRow["ItemCode"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Location
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Location"]);
            }
            set
            {
                this.myRow["Location"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBDecimal ActQty
        {
            get
            {
                return BCE.Data.Convert.ToDBDecimal(this.myRow["ActQty"]);
            }
            set
            {
                this.myRow["ActQty"] = this.myStock.Command.DecimalSetting.RoundToQuantityDBObject(value);
            }
        }



        public DBDecimal EstQty
        {
            get
            {
                return BCE.Data.Convert.ToDBDecimal(this.myRow["EstQty"]);
            }
            set
            {
                this.myRow["EstQty"] = this.myStock.Command.DecimalSetting.RoundToQuantityDBObject(value);
            }
        }



        public DBDecimal EstHPP
        {
            get
            {
                return BCE.Data.Convert.ToDBDecimal(this.myRow["EstHPP"]);
            }
            set
            {
                this.myRow["EstHPP"] = this.myStock.Command.DecimalSetting.RoundToCostDBObject(value);
            }
        }
        public DBDecimal ActHPP
        {
            get
            {
                return BCE.Data.Convert.ToDBDecimal(this.myRow["ActHPP"]);
            }
            set
            {
                this.myRow["ActHPP"] = this.myStock.Command.DecimalSetting.RoundToCostDBObject(value);
            }
        }


        public DBString Note
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Note"]);
            }
            set
            {
                if (value.HasValue)
                    this.myRow["Note"] = (object)Rtf.ToArialRichText((string)value);
                else
                    this.myRow["Note"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Remark1
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Remark1"]);
            }
            set
            {
                this.myRow["Remark1"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Remark2
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Remark2"]);
            }
            set
            {
                this.myRow["Remark2"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Remark3
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Remark3"]);
            }
            set
            {
                this.myRow["Remark3"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Remark4
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Remark4"]);
            }
            set
            {
                this.myRow["Remark4"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public short PrintCount
        {
            get
            {
                return BCE.Data.Convert.ToInt16(this.myRow["PrintCount"]);
            }
        }

        public bool Cancelled
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myRow["Cancelled"]);
            }
        }

        public DBDateTime LastModified
        {
            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myRow["LastModified"]);
            }
        }

        public DBString LastModifiedUserID
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["LastModifiedUserID"]);
            }
        }

        public DBDateTime CreatedTimeStamp
        {
            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myRow["CreatedTimeStamp"]);
            }
        }

        public DBString CreatedUserID
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["CreatedUserID"]);
            }
        }

        public DBString RefDocNo
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["RefDocNo"]);
            }
            set
            {
                this.myRow["RefDocNo"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public bool CanSync
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myRow["CanSync"]);
            }
            set
            {
                this.myRow["CanSync"] = (object)BCE.Data.Convert.BooleanToText(value);
            }
        }

        public DBString ExternalLinkText
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["ExternalLink"]);
            }
            set
            {
                this.myRow["ExternalLink"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public ExternalLink ExternalLink
        {
            get
            {
                return new ExternalLink(this.myRow, "ExternalLink");
            }
        }

        internal StockWorkOrderRecord(StockWorkOrder doc)
          : base(doc.MasterRow)
        {
            this.myStock = doc;
            this.myDetailRows = doc.GetValidDetailRows();
        }

        public StockWorkOrderDetailRecord GetDetailRecord(int index)
        {
            return new StockWorkOrderDetailRecord(this.myStock.Command.DBSetting, this.myDetailRows[index]);
        }

        public StockWorkOrderDetailRecord NewDetail()
        {
            StockWorkOrderDetail assemblyOrderDetail = this.myStock.AddDetail();
            this.myDetailRows = this.myStock.GetValidDetailRows();
            return new StockWorkOrderDetailRecord(this.myStock.Command.DBSetting, assemblyOrderDetail.Row);
        }

        public bool DeleteDetailRecord(int index)
        {
            if (index < 0 || index >= this.myDetailRows.Length)
                throw new ArgumentException("index not in valid range.");
            else if (this.myStock.DeleteDetail(BCE.Data.Convert.ToInt64(this.myDetailRows[index]["DtlKey"])))
            {
                this.myDetailRows = this.myStock.GetValidDetailRows();
                return true;
            }
            else
                return false;
        }

        public void SetDocNoFormatName(string formatName)
        {
            this.myStock.DocNoFormatName = formatName;
        }
    }
}
