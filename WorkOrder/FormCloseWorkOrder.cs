﻿
using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.CommonAccounting;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Data.SqlClient;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Settings;
namespace Production.WorkOrder
{
  
    public class FormCloseStockWorkOrder : XtraForm
    {
        public static string myColumnName;
        private DBSetting myDBSetting;
        private DataTable myDataTable;
        private StockWorkOrderCommand myCommand;
        private AdvancedStockWorkOrderCriteria myCriteria;
        private bool myInSearch;
        private long[] mySelectedDocKeylist;
        private StockWorkOrderReportingCriteria myReportingCriteria;
        private string[] myDocumentStyleOptions;
        private ScriptObject myScriptObject;
        private MouseDownHelper myMouseDownHelper;
        protected UserAuthentication myUserAuthentication;
        private IContainer components;
        private BarManager barManager1;
        private Bar bar2;
        private BarSubItem barSubItem1;
        private BarButtonItem barBtnDesignDocumentStyleReport;
        private BarButtonItem barBtnDesignListingStyleReport;
        private UCDateSelector ucDateSelector1;
        private Label label6;
        private GroupControl groupBox_SearchCriteria;
        private Label label4;
        private UCWOSelector UCWOSelector1;
        private PanelControl panelCriteria;
        private PanelControl panelControl1;
        private SimpleButton sbtnClose;
        private SimpleButton sbtnProcess;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private PanelHeader panelHeader1;
        private XtraTabPage xtraTabPage1;
        private Bar bar1;
        private BarButtonItem barButtonItem1;
        private SimpleButton sbtnInquiry;
        private UCSearchResult ucSearchResult1;
        private GridControl gridControl1;
        private GridView gvMaster;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelled;
        private DevExpress.XtraGrid.Columns.GridColumn colCheck;
        private DevExpress.XtraGrid.Columns.GridColumn colDeptDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDocDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDocNo;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colNewStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private BarButtonItem barButtonItem2;
        protected DecimalSetting myDecimalSetting;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;

        // private StockWorkOrderGrid stockIssueGrid1;
        internal FiscalYear myFiscalYear;
        public DataTable MasterDataTable
        {
            get
            {
                return this.myDataTable;
            }
        }

        public long[] SelectedDocKeys
        {
            get
            {
                return this.mySelectedDocKeylist;
            }
        }



        public string SelectedDocKeysInString
        {
            get
            {
                return StringHelper.ArrayListToCommaString(new ArrayList((ICollection)this.mySelectedDocKeylist));
            }
        }

        public string ColumnName
        {
            get
            {
                return FormCloseStockWorkOrder.myColumnName;
            }
        }

        public StockWorkOrderReportingCriteria StockWorkOrderReportingCriteria
        {
            get
            {
                return this.myReportingCriteria;
            }
        }

        public FormCloseStockWorkOrder(DBSetting dbSetting)
        {
            //FormCloseStockWorkOrder issuePrintListing = this;
            //string[] strArray = new string[2];
            //int index1 = 0;
            //string string1 = BCE.Localization.Localizer.GetString(StockStringId.BatchPrintStockWorkOrder, new object[0]);
            //strArray[index1] = string1;
            //int index2 = 1;
            //string string2 = BCE.Localization.Localizer.GetString(StockStringId.PrintStockWorkOrderListing, new object[0]);
            //strArray[index2] = string2;
            //issuePrintListing.myDocumentStyleOptions = strArray;
            // ISSUE: explicit constructor call
            //base.\u002Ector();
            this.InitializeComponent();
            this.myDBSetting = dbSetting;
            this.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
            this.myCommand = StockWorkOrderCommand.Create(dbSetting);
            // this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockWorkOrderListing");

            this.myDataTable = new DataTable();
            this.LoadCriteria();
            this.ucSearchResult1.Initialize(this.gvMaster, "ToBeUpdate");
            this.InitUserControls();
            myFiscalYear = FiscalYear.GetOrCreate(dbSetting);
            myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);

            this.myMouseDownHelper = new MouseDownHelper();
            //  this.myMouseDownHelper.Init(this.stockIssueGrid1.GridView);
            //  this.stockIssueGrid1.GridView.DoubleClick += new EventHandler(this.GridView_DoubleClick);
            BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Issue.htm#stk028");
            DBSetting dbSetting1 = dbSetting;
            string docType = "";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            //StockWorkOrderString local = StockWorkOrderString.OpenedPrintStockWorkOrderListingWindow;
            //object[] objArray = new object[1];
            //int index3 = 0;
            //string loginUserId = this.myUserAuthentication.LoginUserID;
            //objArray[index3] = (object)loginUserId;
            //string string3 = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            //string detail = "";
            //Activity.Log(dbSetting1, docType, docKey, eventKey, string3, detail);
        }

        private void SaveCriteria()
        {
            PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting)this.myReportingCriteria, "StockWorkOrderDocumentListingReport2.setting");
        }

        private void LoadCriteria()
        {
            this.myReportingCriteria = (StockWorkOrderReportingCriteria)PersistenceUtil.LoadCriteriaData(this.myDBSetting, "StockWorkOrderDocumentListingReport2.setting");
            if (this.myReportingCriteria == null)
                this.myReportingCriteria = new StockWorkOrderReportingCriteria();

        }

        private void InitUserControls()
        {
            this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
            this.UCWOSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter,true);
        }

        private void AddNewColumn()
        {
            this.myDataTable.Columns.Add(new DataColumn()
            {
                DataType = typeof(bool),
                AllowDBNull = true,
                Caption = "Check",
                ColumnName = "ToBeUpdate",
                DefaultValue = (object)false
            });
        }

        private void BasicSearch(bool isSearchAll)
        {
            if (!this.myInSearch)
            {
                this.myInSearch = true;
                GridViewUtils.UpdateData(this.gvMaster);
                this.gridControl1.DataSource = (object)null;
                try
                {
                    //this.myCommand.DocumentListingBasicSearch((this.myReportingCriteria, CommonFunction.BuildSQLColumns(isSearchAll, this.gvMaster.Columns.View), this.myDataTable, "ToBeUpdate");
                    this.gridControl1.MainView.UpdateCurrentRow();
                    this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;

                    this.myCommand.DocumentListingBasicSearch(this.myReportingCriteria, CommonFunction.BuildSQLColumns(isSearchAll, this.gvMaster.Columns.View), this.myDataTable, "ToBeUpdate",true);
                    if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                        this.AddNewColumn();
                    this.gridControl1.DataSource = (object)this.myDataTable;
                }
                finally
                {
                    this.myInSearch = false;
                }
                // BCE.Application.AppMessage.ShowWarningMessage("Process Completed...");

            }
        }



        private void simpleButtonAdvanceSearch_Click(object sender, EventArgs e)
        {
            // this.AdvancedSearch();
        }

        private void FormStockWorkOrderList_Closing(object sender, CancelEventArgs e)
        {
            this.SaveCriteria();
        }

        private void ReloadAllColumns(object sender, EventArgs e)
        {
            this.BasicSearch(true);
        }

        private void sbtnInquiry_Click(object sender, EventArgs e)
        {
            this.BasicSearch(false);
           // this.ucSearchResult1.CheckAll();
            // this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
            //  this.sbtnToggleOptions.Enabled = this.stockIssueGrid1.DataSource != null;
            this.gridControl1.Focus();
            DBSetting dbSetting = this.myDBSetting;
            string docType = "";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            StockWorkOrderString local = StockWorkOrderString.InquiredPrintStockWorkOrderListing;
            object[] objArray = new object[1];
            int index = 0;
            string loginUserId = this.myUserAuthentication.LoginUserID;
            objArray[index] = (object)loginUserId;
            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            // string readableText = this.myReportingCriteria.ReadableText;
            // Activity.Log(dbSetting, docType, docKey, eventKey, @string, readableText);
        }

        private string GetSelectedDocKeys()
        {
            GridViewUtils.UpdateData(this.gvMaster);
            if (this.gvMaster.FocusedRowHandle >= 0 && this.myDataTable.Select("ToBeUpdate = True").Length != 0)
            {
                this.mySelectedDocKeylist = DataTableHelper.GetSelectedKeyArray("ToBeUpdate", "DocKey", this.myDataTable);
            }
            return this.SelectedDocKeysInString;
        }



        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormStockWorkOrderPrintSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)120)
            {
                if (this.sbtnProcess.Enabled)
                    this.sbtnProcess.PerformClick();
            }

        }



        private void FormSIPrintSearch_Load(object sender, EventArgs e)
        {
            this.FormInitialize();
        }

        private void FormInitialize()
        {
            //FormCloseStockWorkOrder.FormInitializeEventArgs initializeEventArgs1 = new FormCloseStockWorkOrder.FormInitializeEventArgs(this);
            //ScriptObject scriptObject = this.myScriptObject;
            //string name = "OnFormInitialize";
            //System.Type[] types = new System.Type[1];
            //int index1 = 0;
            //System.Type type = initializeEventArgs1.GetType();
            //types[index1] = type;
            //object[] objArray = new object[1];
            //int index2 = 0;
            //FormCloseStockWorkOrder.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
            //objArray[index2] = (object)initializeEventArgs2;
            //scriptObject.RunMethod(name, types, objArray);
        }





        private void stockIssueGrid_DrawGroupPanelEvent(object sender, CustomDrawEventArgs e)
        {
            CommonGridViewHelper.DrawGroupPanelString(sender, e, this.myDBSetting);
        }

        protected override void Dispose(bool disposing)
        {
            this.SaveCriteria();
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelCriteria = new DevExpress.XtraEditors.PanelControl();
            this.groupBox_SearchCriteria = new DevExpress.XtraEditors.GroupControl();
            this.sbtnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.UCWOSelector1 = new Production.WorkOrder.UCWOSelector();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ucDateSelector1 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnProcess = new DevExpress.XtraEditors.SimpleButton();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnDesignDocumentStyleReport = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnDesignListingStyleReport = new DevExpress.XtraBars.BarButtonItem();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.ucSearchResult1 = new BCE.AutoCount.FilterUI.UCSearchResult();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gvMaster = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCancelled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDeptDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNewStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).BeginInit();
            this.panelCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox_SearchCriteria)).BeginInit();
            this.groupBox_SearchCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCriteria
            // 
            this.panelCriteria.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCriteria.Controls.Add(this.groupBox_SearchCriteria);
            this.panelCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCriteria.Location = new System.Drawing.Point(0, 66);
            this.panelCriteria.Name = "panelCriteria";
            this.panelCriteria.Size = new System.Drawing.Size(888, 106);
            this.panelCriteria.TabIndex = 2;
            // 
            // groupBox_SearchCriteria
            // 
            this.groupBox_SearchCriteria.Controls.Add(this.sbtnInquiry);
            this.groupBox_SearchCriteria.Controls.Add(this.UCWOSelector1);
            this.groupBox_SearchCriteria.Controls.Add(this.label4);
            this.groupBox_SearchCriteria.Controls.Add(this.label6);
            this.groupBox_SearchCriteria.Controls.Add(this.ucDateSelector1);
            this.groupBox_SearchCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox_SearchCriteria.Location = new System.Drawing.Point(0, 0);
            this.groupBox_SearchCriteria.Name = "groupBox_SearchCriteria";
            this.groupBox_SearchCriteria.Size = new System.Drawing.Size(888, 106);
            this.groupBox_SearchCriteria.TabIndex = 2;
            this.groupBox_SearchCriteria.Text = "Filter Options";
            // 
            // sbtnInquiry
            // 
            this.sbtnInquiry.Location = new System.Drawing.Point(120, 77);
            this.sbtnInquiry.Name = "sbtnInquiry";
            this.sbtnInquiry.Size = new System.Drawing.Size(75, 23);
            this.sbtnInquiry.TabIndex = 6;
            this.sbtnInquiry.Text = "Inquiry";
            this.sbtnInquiry.Click += new System.EventHandler(this.sbtnInquiry_Click);
            // 
            // UCWOSelector1
            // 
            this.UCWOSelector1.Location = new System.Drawing.Point(117, 47);
            this.UCWOSelector1.Name = "UCWOSelector1";
            this.UCWOSelector1.Size = new System.Drawing.Size(557, 24);
            this.UCWOSelector1.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Document No. : ";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(5, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 18);
            this.label6.TabIndex = 3;
            this.label6.Text = " Document Date :";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // ucDateSelector1
            // 
            this.ucDateSelector1.Location = new System.Drawing.Point(120, 24);
            this.ucDateSelector1.Name = "ucDateSelector1";
            this.ucDateSelector1.Size = new System.Drawing.Size(467, 20);
            this.ucDateSelector1.TabIndex = 5;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(890, 271);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.sbtnClose);
            this.panelControl1.Controls.Add(this.sbtnProcess);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 471);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(888, 39);
            this.panelControl1.TabIndex = 1;
            // 
            // sbtnClose
            // 
            this.sbtnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnClose.Location = new System.Drawing.Point(801, 6);
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Size = new System.Drawing.Size(75, 23);
            this.sbtnClose.TabIndex = 3;
            this.sbtnClose.Text = "Close";
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // sbtnProcess
            // 
            this.sbtnProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnProcess.Location = new System.Drawing.Point(720, 6);
            this.sbtnProcess.Name = "sbtnProcess";
            this.sbtnProcess.Size = new System.Drawing.Size(75, 23);
            this.sbtnProcess.TabIndex = 4;
            this.sbtnProcess.Text = "Process";
            this.sbtnProcess.Click += new System.EventHandler(this.sbtnProcess_Click);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 6;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(888, 22);
            this.barDockControlTop.Click += new System.EventHandler(this.barDockControlTop_Click);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 510);
            this.barDockControlBottom.Size = new System.Drawing.Size(888, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 488);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(888, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 488);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Design Document Style Report";
            this.barButtonItem1.Id = 4;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Design Listing Style Report";
            this.barButtonItem2.Id = 5;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnDesignDocumentStyleReport
            // 
            this.barBtnDesignDocumentStyleReport.Id = 1;
            this.barBtnDesignDocumentStyleReport.Name = "barBtnDesignDocumentStyleReport";
            // 
            // barBtnDesignListingStyleReport
            // 
            this.barBtnDesignListingStyleReport.Id = 2;
            this.barBtnDesignListingStyleReport.Name = "barBtnDesignListingStyleReport";
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Update Status Work Order";
            this.panelHeader1.HelpNavigator = System.Windows.Forms.HelpNavigator.AssociateIndex;
            this.panelHeader1.HelpTopicId = "Stock_Issue.htm#stk028";
            this.panelHeader1.Location = new System.Drawing.Point(0, 22);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(888, 44);
            this.panelHeader1.TabIndex = 3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.Text = "Custom 1";
            // 
            // ucSearchResult1
            // 
            this.ucSearchResult1.Appearance.Options.UseBackColor = true;
            this.ucSearchResult1.ClearAllUncheckRecords = false;
            this.ucSearchResult1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucSearchResult1.KeepSearchResultVisible = false;
            this.ucSearchResult1.Location = new System.Drawing.Point(0, 172);
            this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ucSearchResult1.Name = "ucSearchResult1";
            this.ucSearchResult1.Size = new System.Drawing.Size(888, 46);
            this.ucSearchResult1.TabIndex = 8;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 218);
            this.gridControl1.MainView = this.gvMaster;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemDateEdit1,
            this.repositoryItemComboBox1});
            this.gridControl1.Size = new System.Drawing.Size(888, 253);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMaster});
            // 
            // gvMaster
            // 
            this.gvMaster.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCancelled,
            this.colCheck,
            this.colDeptDesc,
            this.colDescription,
            this.colDocDate,
            this.colDocNo,
            this.colStatus,
            this.colNewStatus,
            this.gridColumn1});
            this.gvMaster.GridControl = this.gridControl1;
            this.gvMaster.Name = "gvMaster";
            this.gvMaster.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gvMaster.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvMaster_CustomRowCellEdit);
            this.gvMaster.CustomRowCellEditForEditing += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvMaster_CustomRowCellEditForEditing);
            // 
            // colCancelled
            // 
            this.colCancelled.Caption = "Cancelled";
            this.colCancelled.FieldName = "Cancelled";
            this.colCancelled.Name = "colCancelled";
            this.colCancelled.OptionsColumn.AllowEdit = false;
            this.colCancelled.Visible = true;
            this.colCancelled.VisibleIndex = 7;
            this.colCancelled.Width = 109;
            // 
            // colCheck
            // 
            this.colCheck.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCheck.FieldName = "ToBeUpdate";
            this.colCheck.Name = "colCheck";
            this.colCheck.Visible = true;
            this.colCheck.VisibleIndex = 0;
            this.colCheck.Width = 115;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colDeptDesc
            // 
            this.colDeptDesc.Caption = "DeptDesc";
            this.colDeptDesc.FieldName = "DeptDesc";
            this.colDeptDesc.Name = "colDeptDesc";
            this.colDeptDesc.OptionsColumn.AllowEdit = false;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 6;
            this.colDescription.Width = 181;
            // 
            // colDocDate
            // 
            this.colDocDate.Caption = "DocDate";
            this.colDocDate.FieldName = "DocDate";
            this.colDocDate.Name = "colDocDate";
            this.colDocDate.OptionsColumn.AllowEdit = false;
            this.colDocDate.Visible = true;
            this.colDocDate.VisibleIndex = 5;
            this.colDocDate.Width = 181;
            // 
            // colDocNo
            // 
            this.colDocNo.Caption = "DocNo";
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.OptionsColumn.AllowEdit = false;
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 4;
            this.colDocNo.Width = 181;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 3;
            this.colStatus.Width = 115;
            // 
            // colNewStatus
            // 
            this.colNewStatus.Caption = "New Status";
            this.colNewStatus.ColumnEdit = this.repositoryItemComboBox1;
            this.colNewStatus.FieldName = "NewStatus";
            this.colNewStatus.Name = "colNewStatus";
            this.colNewStatus.Visible = true;
            this.colNewStatus.VisibleIndex = 2;
            this.colNewStatus.Width = 132;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Numbering";
            this.gridColumn1.FieldName = "TotalAssemblyRequestQty";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 64;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // FormCloseStockWorkOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnClose;
            this.ClientSize = new System.Drawing.Size(888, 510);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.ucSearchResult1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelCriteria);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormCloseStockWorkOrder";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormStockWorkOrderList_Closing);
            this.Load += new System.EventHandler(this.FormSIPrintSearch_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormStockWorkOrderPrintSearch_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).EndInit();
            this.panelCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBox_SearchCriteria)).EndInit();
            this.groupBox_SearchCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        public class FormEventArgs
        {
            private FormCloseStockWorkOrder myForm;

            public StockWorkOrderCommand Command
            {
                get
                {
                    return this.myForm.myCommand;
                }
            }

            public PanelControl PanelCriteria
            {
                get
                {
                    return this.myForm.panelCriteria;
                }
            }

            public PanelControl PanelButtons
            {
                get
                {
                    return this.myForm.panelControl1;
                }
            }



            //public GridControl GridControl
            //{
            //    get
            //    {
            //        return this.myForm.stockIssueGrid1.GridControl;
            //    }
            //}

            public FormCloseStockWorkOrder Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public DBSetting DBSetting
            {
                get
                {
                    return this.myForm.myDBSetting;
                }
            }

            public FormEventArgs(FormCloseStockWorkOrder form)
            {
                this.myForm = form;
            }
        }

        public class FormInitializeEventArgs : FormCloseStockWorkOrder.FormEventArgs
        {
            public FormInitializeEventArgs(FormCloseStockWorkOrder form)
              : base(form)
            {
            }
        }

        public class FormInquiryEventArgs : FormCloseStockWorkOrder.FormEventArgs
        {
            private DataTable myResultTable;

            public DataTable ResultTable
            {
                get
                {
                    return this.myResultTable;
                }
            }

            public FormInquiryEventArgs(FormCloseStockWorkOrder form, DataTable resultTable)
              : base(form)
            {
                this.myResultTable = resultTable;
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void barDockControlTop_Click(object sender, EventArgs e)
        {

        }
        protected void DeleteFromStockCostingWIP(long docKey, DBSetting newDBSetting)
        {
            Production.StockCard.StockCosting.Create(newDBSetting).Remove("RV", docKey);
        }
        protected void PostToStockCostingWIP(DataRow masterRow, DataRow productRow, DBSetting newDBSetting)
        {

            DataTable subdataTable = new DataTable(); ;// null;// ds.Tables["SubDetail"];
            DateTime docDate = myDBSetting.GetServerTime();
            long docKey = BCE.Data.Convert.ToInt64(productRow["DocKey"]);
            //this.DeleteFromStockCostingWIP(docKey, newDBSetting);
            if (docDate >= this.myFiscalYear.ActualDataStartDate && !BCE.Data.Convert.TextToBoolean(masterRow["Cancelled"]))
            {
                string sTypeBOM = "";
                bool enable = ModuleControl.GetOrCreate(newDBSetting).ModuleController.AdvancedMultiUOM.Enable;
                Production.StockCard.StockTrans trans = new Production.StockCard.StockTrans(newDBSetting, "WO", docKey);
                object obj = myDBSetting.ExecuteScalar("select CostType from RPA_ItemBOM where BOMCode=? ",(object)productRow["BOMCode"]);
                if (obj != null && obj != DBNull.Value)
                {
                    sTypeBOM = obj.ToString();
                }
                if (sTypeBOM != "Standard")
                {
                    subdataTable = myDBSetting.GetDataTable("select WONo,ItemCode,Location,UOM,SUM(Qty) AS Qty,AVG(COALESCE(Cost,0)) as Cost from RPA_TransRawMaterial with(nolock) where WONo=?  group by WONo,ItemCode,UOM,Location having SUM(Qty)<>0", false, (object)masterRow["DocNo"]);
                    //DataRow[] drSubDetailArray = subdataTable.Select("DtlKey=" + dataRow2["DtlKey"] + "", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
                    if (subdataTable.Rows.Count > 0)
                    {
                        int iloop = 0;
                        foreach (DataRow drSubDetail in subdataTable.Rows)
                        {
                            Decimal num = BCE.Data.Convert.ToDecimal(drSubDetail["Qty"]);
                            Decimal dunitcost = BCE.Data.Convert.ToDecimal(drSubDetail["Cost"]);
                            string itemCode = drSubDetail["ItemCode"].ToString();
                            string strUOM = drSubDetail["UOM"].ToString();
                            string strWONo = "";
                            string strBOMCode = "";
                            string strProductCode = "";
                            iloop++;
                            Decimal numSumRV = 0;

                            object oSumQtyRV = myDBSetting.ExecuteScalar("select sum(Qty) from RPA_TransRawMaterial with(nolock) where WONo=? and ItemCode=? and UOM=? and Location=? and DocType='RV'", (object)drSubDetail["WONo"], (object)drSubDetail["ItemCode"], (object)drSubDetail["UOM"], (object)drSubDetail["Location"]);
                            if (oSumQtyRV != null && oSumQtyRV != DBNull.Value)
                                numSumRV = BCE.Data.Convert.ToDecimal(oSumQtyRV);
                            DataTable WIPTable = myDBSetting.GetDataTable("select * from RPA_TransRawMaterial with(nolock) where WONo=? and ItemCode=? and UOM=? and Location=? and DocType='RV' Order By DocDate", false, (object)drSubDetail["WONo"], (object)drSubDetail["ItemCode"], (object)drSubDetail["UOM"], (object)drSubDetail["Location"]);
                            foreach (DataRow drWIPDetail in WIPTable.Rows)
                            {
                                Decimal numQty = BCE.Data.Convert.ToDecimal(drWIPDetail["Qty"]);
                                Decimal numCost = BCE.Data.Convert.ToDecimal(drWIPDetail["Cost"]);
                                //object obj = myDBSetting.ExecuteScalar("select CostType from RPA_ItemBOM where BOMCode=? ",(object));



                                Decimal dQtyResult = 0;
                                Decimal dTotalCost = 0;
                                dQtyResult = numQty + (((num * numQty) * -1) / numSumRV);
                                // dQtyResult =dQtyResult;
                                dTotalCost = dQtyResult * numCost;
                                newDBSetting.ExecuteNonQuery("UPDATE RPA_TransRawMaterial SET Qty=?,TotalCost=? where StockDtlKey=?", (object)dQtyResult, (object)dTotalCost, (object)drWIPDetail["StockDtlKey"]);
                            }
                        }


                        //drSubDetail["DtlKey"] = iloop;
                        //strWONo = masterRow["DocNo"].ToString(); 
                        //if (productRow["BOMCode"] != null && productRow["BOMCode"] != DBNull.Value)
                        //    strBOMCode = productRow["BOMCode"].ToString();
                        ////object obj = myDBSetting.ExecuteScalar("select ItemCode from RPA_ItemBOM where BOMCode=?", (object)strBOMCode);
                        ////if (obj != null && obj != DBNull.Value)
                        ////{
                        ////    strProductCode = obj.ToString();
                        ////}
                        //strProductCode = productRow["ItemCode"].ToString();
                        //if (!enable)
                        //{
                        //    StockHelper stockHelper = StockHelper.Create(myDBSetting);
                        //    try
                        //    {
                        //        Decimal itemUomRate = stockHelper.GetItemUOMRate(itemCode, strUOM);
                        //        if (itemUomRate != Decimal.Zero)
                        //        {
                        //            strUOM = stockHelper.GetBaseUOM(itemCode);
                        //            num = this.myDecimalSetting.RoundQuantity(num * itemUomRate);
                        //        }
                        //    }
                        //    catch { }
                        //}
                        // trans.Add(BCE.Data.Convert.ToInt64(drSubDetail["DtlKey"]), masterRow["DocNo"].ToString(), strWONo, strProductCode, strBOMCode, itemCode, strUOM, drSubDetail["Location"].ToString(), "", null, null, null, docDate, -num, dunitcost, 0L);
                    }




                }
                else
                {
                    subdataTable = myDBSetting.GetDataTable("select * from RPA_TransRawMaterial with(nolock) where WONo=?  and DocType='RV' ", false, (object)masterRow["DocNo"]);
                    //DataRow[] drSubDetailArray = subdataTable.Select("DtlKey=" + dataRow2["DtlKey"] + "", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
                    if (subdataTable.Rows.Count > 0)
                    {
                        int iloop = 0;
                        foreach (DataRow drSubDetail in subdataTable.Rows)
                        {
                            Decimal num = BCE.Data.Convert.ToDecimal(drSubDetail["Qty"]);
                            Int32 iStockDtlKey = 0;
                            iStockDtlKey = BCE.Data.Convert.ToInt32(drSubDetail["StockDtlKey"]);
                            // Decimal dunitcost = BCE.Data.Convert.ToDecimal(drSubDetail["Cost"]);
                            string itemCode = drSubDetail["ItemCode"].ToString();
                            string strUOM = drSubDetail["UOM"].ToString();
                           
                            DataRow drRM = myDBSetting.GetFirstDataRow("select StockDtlKey,Cost from RPA_TransRawMaterial with(nolock) where BOMCode=? and ProductCode=? and WONo=? and ItemCode=? and DocType='RM' ", (object)drSubDetail["BOMCode"], (object)drSubDetail["ProductCode"], (object)drSubDetail["WONo"], (object)drSubDetail["ItemCode"]);
                            if (drRM != null)
                            {
                                                             
                                newDBSetting.ExecuteNonQuery("UPDATE RPA_TransRawMaterial SET Cost=?,TotalCost=? where StockDtlKey=?", (object)drRM["Cost"], (object)(BCE.Data.Convert.ToDecimal(drRM["Cost"]) * BCE.Data.Convert.ToDecimal(num)), (object)iStockDtlKey);
                            }
                        }
                    }
                }
                //Production.StockCard.StockCosting.Create(newDBSetting).Add(trans);
            }
            //else
                //this.DeleteFromStockCostingWIP(docKey, newDBSetting);
        }

        private void sbtnProcess_Click(object sender, EventArgs e)
        {
            char[] chArray = new char[1];
            int index1 = 0;
            int num1 = 44;
            chArray[index1] = (char)num1;
            //string[] strArray1 = str1.Split(chArray);
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            string str6 = string.Empty;
            //this.myDataTable
            DataRow[] drResult = this.myDataTable.Select("ToBeUpdate = True");

            //  if (numArray != null)
            try
            {
                string strresult = "";
                foreach (DataRow MasterRow in drResult)
                {

                    decimal dA = 0;//Qty Finish Goods Main Product
                    decimal dB = 0;//Total Pemakaian BOM
                    decimal dC = 0;//Qty barang Sortiran
                    decimal dD = 0;//Qty barang alternatif
                    decimal dE = 0;//Biaya Overhead
                    decimal dF = 0;//Percentage barang sortiran
                    decimal dG = 0;//Max Harga barang sortiran
                    decimal dH = 0;//Total B+E
                    decimal dHpp1C = 0;//BS: ((B+E)*F)/C
                    decimal dHpp2C = 0;//BS: Jika 1C>G Maka, 2C=G
                                       //    Jika 1C<=G Maka,2c=Hpp1C
                    decimal dHppBS = 0;//Total Hpp2C
                    decimal dHppFG = 0;//H-Hpp2C
                    decimal dQtyFG = 0;//A+D                        
                    decimal dHppTemp = 0;//((A/dQtyFG)*dHppFG)/A
                    decimal dHppD = 0;//((D/dQtyFG)*dHppFG)/D
                                      // DataRow dataRow = resultDataTable.Rows.Find((object)num2);
                                      // if (dataRow != null)
                                      //    dataRow[checkEditColumnName] = (object)true;
                    DBSetting TransDBSetting = myDBSetting.StartTransaction();
                    object obj;
                    try
                    {
                       

                        if (MasterRow["NewStatus"].ToString() == "")
                        {
                            strresult += "Error Result : " + MasterRow["DocNo"].ToString() + " [Proses gagal!!! Kolom New Status harus di pilih]" + Environment.NewLine;
                            continue;
                        }
                       
                        if (MasterRow["NewStatus"].ToString() == WorkOrderStatusOptions.Closed.ToString())
                        {                            
                            StockWorkOrderCommand wocmd = StockWorkOrderCommand.Create(TransDBSetting);
                            StockWorkOrder woentity = wocmd.Edit(BCE.Data.Convert.ToInt64(MasterRow["DocKey"]));
                            woentity.Status = WorkOrderStatusOptions.Closed.ToString();
                            string sprojnoBOM = "";

                            foreach (DataRow drProduct in woentity.GetValidPIDetailRows())
                            {
                                drProduct["Status"]= WorkOrderStatusOptions.Closed.ToString();
                                obj = myDBSetting.ExecuteScalar("select SUM(QTY) from RPA_RCV A with(nolock) inner join RPA_RCVDTL B with(nolock) ON A.DocKey = B.DocKey where FromDocNo =? and FromDocType in('WO','Main Product') and A.Cancelled = 'F' AND ItemCode =? and FromDocDtlKey=?", (object)MasterRow["DocNo"], (object)drProduct["ItemCode"], (object)drProduct["DtlKey"]);
                                if (obj != null && obj != DBNull.Value)
                                {
                                    dA = BCE.Data.Convert.ToDecimal(obj);
                                }

                                obj = myDBSetting.ExecuteScalar("select SUM((B.Qty-coalesce(B.TransferedQty,0))*B.UnitCost) from RPA_RM A with (nolock) inner join RPA_RMDTL B with (nolock) ON A.DocKey=B.DocKey inner join RPA_WODtl C  with (nolock) on C.DtlKey=B.FromDocDtlKey where  Cancelled='F' AND B.FromDocNo=? AND C.FromDocDtlKey=?", (object)MasterRow["DocNo"], (object)drProduct["DtlKey"]);
                                if (obj != null && obj != DBNull.Value)
                                {
                                    dB = BCE.Data.Convert.ToDecimal(obj);
                                }
                                DataRow drBOM = myDBSetting.GetFirstDataRow("select ProjNo from RPA_RM A with (nolock) inner join RPA_RMDTL B with (nolock) ON A.DocKey=B.DocKey where FromDocNo=? and Cancelled='F'", (object)MasterRow["DocNo"]);
                                if (drBOM != null)
                                {
                                    sprojnoBOM = drBOM["ProjNo"] != null ? drBOM["ProjNo"].ToString() : "";
                                }
                                //obj = myDBSetting.ExecuteScalar("select SUM(QTY) from RPA_RCV A with(nolock) inner join RPA_RCVDTL B with(nolock) ON A.DocKey = B.DocKey where FromDocNo =? and A.Cancelled= 'F' AND FromDocType IN ('AP','Alternatif Product')  ", (object)MasterRow["DocNo"]);
                                //if (obj != null && obj != DBNull.Value)
                                //{
                                //    dD = BCE.Data.Convert.ToDecimal(obj);
                                //}

                                obj = myDBSetting.ExecuteScalar("SELECT SUM(COALESCE(Amount,0)) FROM RPA_WOOvd where DocKey=?", (object)MasterRow["DocKey"]);
                                if (obj != null && obj != DBNull.Value)
                                {
                                    dE = BCE.Data.Convert.ToDecimal(obj);
                                }
                                dE = dE / BCE.Data.Convert.ToDecimal(woentity.GetValidPIDetailRows().Length);
                                // dH = dB + dE;
                                dQtyFG = dA + dD;

                                DataTable dtRcv = myDBSetting.GetDataTable("select distinct A.DocKey from RPA_RCV A with(nolock) inner join RPA_RCVDTL B with(nolock) ON A.DocKey = B.DocKey where FromDocNo =? and FromDocDtlKey=? and A.Cancelled = 'F' and FromDocType in('WO','Main Product')", false, (object)MasterRow["DocNo"], (object)drProduct["DtlKey"]);
                                DataTable dtRcvBS = myDBSetting.GetDataTable("select distinct A.DocKey from RPA_RCV A with(nolock) inner join RPA_RCVDTL B with(nolock) ON A.DocKey = B.DocKey inner join RPA_WOBS C with(nolock) ON B.FromDocDtlKey = C.DtlKey where B.FromDocNo=? and C.FromDocDtlKey =?  and A.Cancelled = 'F' and FromDocType in ('Product sortiran','BS') ", false, (object)MasterRow["DocNo"], (object)drProduct["DtlKey"]);

                                decimal dWIPBSTotal = 0;
                                dHppFG = 0;

                                //decimal dWIPTotal = 0;

                                foreach (DataRow drRcvBS in dtRcvBS.Rows)
                                {
                                    Int64 iFGKey = 0;
                                    iFGKey = BCE.Data.Convert.ToInt64(drRcvBS["DocKey"]);
                                    foreach (DataRow BSRow in woentity.GetValidBSDetailRows())
                                    {
                                        decimal dWIPCost = 0;
                                        dWIPCost = 0;
                                        dC = 0;
                                        dF = 0;//Percentage barang sortiran
                                        dG = 0;//Max Harga barang sortiran                               
                                        dHpp2C = 0;//BS: Jika 1C>G Maka, 2C=G
                                        dHpp1C = 0;//BS: ((B+E)*F)/C
                                                   //    Jika 1C<=G Maka,2c=Hpp1C
                                        StockReceive.StockReceiveCommand srcmd = StockReceive.StockReceiveCommand.Create(TransDBSetting);
                                        StockReceive.StockReceive srentity = srcmd.Edit(iFGKey);
                                        DataRow[] drDetail1 = srentity.DataTableDetail.Select("FromDocNo='" + MasterRow["DocNo"] + "' and ItemCode='" + BSRow["ItemCode"] + "' and FromDocType in ('Product sortiran','BS')", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
                                        if (drDetail1.Length == 0)
                                            continue;
                                        foreach (DataRow drDetail2 in drDetail1)
                                        {
                                            // DataRow drDetail2 = drDetail1[0];
                                            dC = BCE.Data.Convert.ToDecimal(drDetail2["Qty"]);
                                            dF = BCE.Data.Convert.ToDecimal(BSRow["CostPercent"]);
                                            dG = BCE.Data.Convert.ToDecimal(BSRow["CostMax"]);
                                            dHpp1C = (dB * (dF / 100)) / dC;
                                            if (dHpp1C > dG)
                                                dHpp2C = dG;
                                            else
                                                dHpp2C = dHpp1C;
                                            BSRow["ActHPP"] = dHpp2C;
                                            dWIPCost = dHpp2C;

                                            StockReceive.StockReceiveDetail srdtl = srentity.EditDetail(BCE.Data.Convert.ToInt64(drDetail2["DtlKey"].ToString()));
                                            //}
                                            srdtl.WIPCost = dWIPCost;
                                            dWIPBSTotal += (dWIPCost * dC);
                                            // dWIPTotal += (dWIPCost * dC);
                                            string scostype = "";
                                            decimal dUnitCoststandard = 0;
                                            DataRow dritemBOM = myDBSetting.GetFirstDataRow("select CostType,Cost from RPA_ItemBOM where BOMCode=? and ItemCode=?",(object)drDetail2["BOMCode"], (object)drDetail2["ItemCode"]);
                                            if(dritemBOM != null)
                                            {
                                                scostype = dritemBOM["CostType"].ToString();
                                                dUnitCoststandard= BCE.Data.Convert.ToDecimal(dritemBOM["Cost"]);
                                            }
                                            if (scostype == "Standard")
                                            {
                                                obj = myDBSetting.ExecuteScalar("select coalesce(Cost,0) from ItemUOM where ItemCode=? and UOM=?", (object)drDetail2["ItemCode"], (object)drDetail2["UOM"]);
                                                if (obj != null && obj!=DBNull.Value)
                                                {
                                                    srdtl.Row["UnitCost"] = obj;
                                                    srdtl.UnitCost = BCE.Data.Convert.ToDecimal(obj);
                                                    dHpp2C= BCE.Data.Convert.ToDecimal(obj);
                                                }
                                            }
                                            else
                                            {
                                                srdtl.Row["UnitCost"] = dHpp2C;
                                                srdtl.UnitCost = dHpp2C;
                                            }
                                          
                                            srdtl.SubTotal = srdtl.UnitCost * srdtl.Qty;
                                            srdtl.EndEdit();
                                            dHppBS += (dHpp2C * dC);
                                        }
                                        srentity.Save(BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID, false,true);

                                    }
                                }

                                foreach (DataRow drRcv in dtRcv.Rows)
                                {
                                    //decimal dWIPTotal = 0;
                                    Int64 iFGKey = 0;
                                    dHppTemp = 0;
                                    //decimal dSumQtyFG = 0;
                                    decimal dWIPSubTotal = 0;
                                    decimal dWIPCost = 0;
                                    iFGKey = BCE.Data.Convert.ToInt64(drRcv["DocKey"]);
                                    if (iFGKey == 0)
                                        continue;
                                    StockReceive.StockReceiveCommand srcmd = StockReceive.StockReceiveCommand.Create(TransDBSetting);
                                    StockReceive.StockReceive srentity = srcmd.Edit(iFGKey);
                                    
                                    dHppTemp = (dB + (dE * dQtyFG)) - dHppBS;
                                    dQtyFG = dA + dD;

                                    if (dHppTemp > 0 && dHppFG == 0)
                                        dHppFG = dHppTemp / dQtyFG;
                                    //   dHppA = ((dA / dQtyFG) * dHppFG) / dA;
                                  
                                    //foreach (DataRow APRow in woentity.GetValidAPDetailRows())
                                    //{
                                    //    dWIPSubTotal = 0;
                                    //    dWIPCost = 0;
                                    //    //dD = 0;
                                    //    dHppD = 0;//((D/dQtyFG)*dHppFG)/D 
                                    //    APRow["ActHPP"] = dHppFG;
                                    //    DataRow[] drDetail1 = srentity.DataTableDetail.Select("FromDocNo='" + MasterRow["DocNo"] + "' and ItemCode='" + APRow["ItemCode"] + "' and FromDocType in ('Alternatif Product','AP') ", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
                                    //    if (drDetail1.Length == 0)
                                    //        continue;
                                    //    foreach (DataRow drDetail2 in drDetail1)
                                    //    {
                                    //        StockReceive.StockReceiveDetail srdtl = srentity.EditDetail(BCE.Data.Convert.ToInt64(drDetail2["DtlKey"].ToString()));
                                    //        srdtl.UnitCost = dHppFG;
                                    //        srdtl.Row["UnitCost"] = dHppFG;
                                    //        dWIPSubTotal = (BCE.Data.Convert.ToDecimal(srdtl.Row["Qty"]) / dQtyFG) * (dB - dWIPBSTotal);
                                    //        dWIPCost = dWIPSubTotal / BCE.Data.Convert.ToDecimal(srdtl.Row["Qty"]);
                                    //        srdtl.Row["WIPCost"] = dWIPCost;
                                    //        // dWIPTotal += (dWIPCost * BCE.Data.Convert.ToDecimal(srdtl.Row["Qty"]));
                                    //        srdtl.SubTotal = srdtl.UnitCost * srdtl.Qty;
                                    //        srdtl.EndEdit();
                                    //    }
                                    //}
                                    DataRow[] drDetailMain = srentity.DataTableDetail.Select("FromDocDtlKey="+drProduct["DtlKey"]+" and FromDocNo='" + MasterRow["DocNo"] + "' and ItemCode='" + drProduct["ItemCode"] + "' and FromDocType in ('WO','Main Product') ", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);

                                    if (drDetailMain.Length > 0)
                                    {
                                        foreach (DataRow drDetailMain2 in drDetailMain)
                                        {
                                            dWIPSubTotal = 0;
                                            dWIPCost = 0;
                                            //DataRow drDetailMain2 = drDetailMain[0];
                                            StockReceive.StockReceiveDetail srdtl2 = srentity.EditDetail(BCE.Data.Convert.ToInt64(drDetailMain2["DtlKey"].ToString()));
                                            srdtl2.UnitCost = dHppFG;
                                            string scostype = "";
                                            decimal dUnitCoststandard = 0;
                                            DataRow dritemBOM = myDBSetting.GetFirstDataRow("select CostType,Cost from RPA_ItemBOM where BOMCode=? and ItemCode=?", (object)drDetailMain2["BOMCode"], (object)drDetailMain2["ItemCode"]);
                                            if (dritemBOM != null)
                                            {
                                                scostype = dritemBOM["CostType"].ToString();
                                                dUnitCoststandard = BCE.Data.Convert.ToDecimal(dritemBOM["Cost"]);
                                            }
                                            if (scostype == "Standard")
                                            {
                                                obj = myDBSetting.ExecuteScalar("select coalesce(Cost,0) from ItemUOM where ItemCode=? and UOM=?", (object)drDetailMain2["ItemCode"], (object)drDetailMain2["UOM"]);
                                                if (obj != null && obj != DBNull.Value)
                                                {
                                                    srdtl2.Row["UnitCost"] = obj;
                                                    srdtl2.UnitCost = BCE.Data.Convert.ToDecimal(obj);
                                                    drProduct["ActHpp"] = obj;
                                                    dHppFG = BCE.Data.Convert.ToDecimal(obj);
                                                }
                                            }
                                            else
                                            {
                                                srdtl2.Row["UnitCost"] = dHppFG;
                                                drProduct["ActHpp"] = dHppFG.ToString();
                                                srdtl2.UnitCost = dHppFG;
                                            }
                                           // srdtl2.Row["UnitCost"] = dHppFG;
                                            dWIPSubTotal = (BCE.Data.Convert.ToDecimal(srdtl2.Row["Qty"]) / dQtyFG) * (dB - dWIPBSTotal);
                                            dWIPCost = dWIPSubTotal / BCE.Data.Convert.ToDecimal(srdtl2.Row["Qty"]);
                                            srdtl2.Row["WIPCost"] = dWIPCost;
                                            //dWIPTotal += (dWIPCost * BCE.Data.Convert.ToDecimal(srdtl2.Row["Qty"]));
                                            srdtl2.SubTotal = srdtl2.UnitCost * srdtl2.Qty;
                                            srdtl2.EndEdit();
                                        }
                                    }
                                    srentity.Save(BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID, false,true);
                                }

                                //dWIPBSTotal = 0;
                                //dWIPTotal = 0;
                                dHppFG = 0;
                                // Loop Receive Finish Goods after Frozen
                                PostToStockCostingWIP(MasterRow, drProduct, TransDBSetting);
                                bool bIsAutoJournal = false;
                                obj = myDBSetting.ExecuteScalar("select IsAutoJournal from RPA_Settings");
                                if(obj!=null && obj!=DBNull.Value)
                                {
                                    bIsAutoJournal = BCE.Data.Convert.TextToBoolean(obj);
                                }
                                if (bIsAutoJournal)
                                {
                                    #region Post to JE
                                    foreach (DataRow drRcv in dtRcv.Rows)
                                    {
                                        Int64 iFGKey = 0;
                                        dHppTemp = 0;
                                        iFGKey = BCE.Data.Convert.ToInt64(drRcv["DocKey"]);
                                        if (iFGKey == 0)
                                            continue;
                                        StockReceive.StockReceiveCommand srcmd = StockReceive.StockReceiveCommand.Create(TransDBSetting);
                                        StockReceive.StockReceive srentity = srcmd.Edit(iFGKey);
                                        srcmd.PostToJE(sprojnoBOM, woentity.OvdDetailTable, srentity, srentity.myDataSet, TransDBSetting, srentity.Action, false);
                                        srentity.Save(BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID, false, true);

                                    }
                                }

                            }
                            #endregion
                            //if (dtRcv.Rows.Count == 0)
                            //continue;
                            wocmd.SaveData(woentity);
                            TransDBSetting.Commit();
                            //  woentity.Save(BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID);
                            strresult += "Complete Result : " + MasterRow["DocNo"].ToString() + " [status berhasil di ubah menjadi Closed]" + Environment.NewLine;
                        }
                        else
                        {
                            StockWorkOrderCommand wocmd = StockWorkOrderCommand.Create(TransDBSetting);
                            if (MasterRow["NewStatus"].ToString() == WorkOrderStatusOptions.Cancel.ToString())
                            {
                                strresult += "Complete Result : " + MasterRow["DocNo"].ToString() + " [berhasil di cancel]" + Environment.NewLine;
                                wocmd.CancelDocument(BCE.Data.Convert.ToInt64(MasterRow["DocKey"]), BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID);
                            }
                            else
                            {
                                StockWorkOrder woentity = wocmd.Edit(BCE.Data.Convert.ToInt64(MasterRow["DocKey"]));
                                woentity.Status = MasterRow["NewStatus"].ToString();
                                foreach(DataRow drDetail in woentity.GetValidAPDetailRows())
                                {
                                    drDetail["Status"] = MasterRow["NewStatus"].ToString();
                                }
                                strresult += "Complete Result : " + MasterRow["DocNo"].ToString() + " [status berhasil di ubah menjadi " + MasterRow["NewStatus"].ToString() + "]" + Environment.NewLine;
                                wocmd.SaveData(woentity);
                            }
                            if (MasterRow["NewStatus"].ToString() != WorkOrderStatusOptions.Pending.ToString() && MasterRow["Status"].ToString() != WorkOrderStatusOptions.Planned.ToString())
                            {
                                DataTable dtIss = myDBSetting.GetDataTable("select distinct DocNo,A.DocKey from RPA_RM A with (nolock) inner join RPA_RMDTL B with (nolock) ON A.DocKey=B.DocKey where FromDocNo=? and Cancelled='F'", false, (object)MasterRow["DocNo"]);
                                if (dtIss.Rows.Count > 0)
                                {
                                    foreach (DataRow drIss in dtIss.Rows)
                                    {
                                        StockIssueRM.StockIssueCommand sicmd = StockIssueRM.StockIssueCommand.Create(TransDBSetting);
                                        StockIssueRM.StockIssue sientity = sicmd.Edit(BCE.Data.Convert.ToInt64(drIss["DocKey"]));
                                        StockReceiveRM.StockReceiveRMCommand srrmcmd = StockReceiveRM.StockReceiveRMCommand.Create(TransDBSetting);
                                        StockReceiveRM.StockReceiveRM srentity = srrmcmd.AddNew();
                                        //srentity.MachineCode = sientity.MachineCode;
                                        decimal dtotalqty = 0;
                                        foreach (DataRow drDetailIss in sientity.GetValidDetailRows())
                                        {
                                            //StockReceiveRM.StockReceiveRMDetail srdtlentity=                                     
                                            decimal dTransferedQty = 0;
                                            decimal dQty = 0;
                                            decimal dQtyRetur = 0;
                                            dQty = drDetailIss["Qty"] != DBNull.Value ? BCE.Data.Convert.ToDecimal(drDetailIss["Qty"]) : 0;
                                            dTransferedQty = drDetailIss["TransferedQty"] != DBNull.Value ? BCE.Data.Convert.ToDecimal(drDetailIss["TransferedQty"]) : 0;
                                            dQtyRetur = dQty - dTransferedQty;
                                            dtotalqty += dQtyRetur;
                                            //drDetailIss["TransferedQty"] = dTransferedQty+dQtyRetur;
                                            if (dQtyRetur > 0)
                                                srentity.AddDetail(drDetailIss, sientity.DocNo.ToString(), dQtyRetur);
                                        }
                                        if (dtotalqty > 0)
                                            srentity.Save(BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID, false);
                                    }
                                }
                                //else
                                //{
                                //    wocmd = StockWorkOrderCommand.Create(TransDBSetting);
                                //    StockWorkOrder woentity = wocmd.Edit(BCE.Data.Convert.ToInt64(MasterRow["DocKey"]));
                                //    woentity.Status = WorkOrderStatusOptions.Closed.ToString();
                                //    foreach (DataRow drIss in dtIss.Rows)
                                //    {
                                //    }
                                //}
                            }
                            TransDBSetting.Commit();

                        }
                        gridControl1.Refresh();

                    }
                    catch (SqlException ex)
                    {

                        BCE.Data.DataError.HandleSqlException(ex);
                        throw;
                    }
                    finally
                    {
                        TransDBSetting.EndTransaction();
                        BCE.Application.AppMessage.ShowInformationMessage(strresult);
                    }
                }
                BasicSearch(false);


            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        private void gvMaster_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
           
        }

        private void gvMaster_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            DataRow drDtl = gvMaster.GetDataRow(e.RowHandle);

            
            if (e.Column.FieldName == "NewStatus")
            {
                DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbedit = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)e.RepositoryItem;

                if (drDtl["Status"].ToString() == WorkOrderStatusOptions.Planned.ToString() || drDtl["Status"].ToString().Contains(WorkOrderStatusOptions.InProcess.ToString()))
                {
                    cbedit.Items.Clear();
                    cbedit.Items.Add(WorkOrderStatusOptions.Pending.ToString());
                    cbedit.Items.Add(WorkOrderStatusOptions.Cancel.ToString());

                }
                else if (drDtl["Status"].ToString().Contains(WorkOrderStatusOptions.Assembly.ToString()))
                {
                    cbedit.Items.Clear();
                    cbedit.Items.Add(WorkOrderStatusOptions.Closed.ToString());
                    //repositoryItemComboBox1.Items.Add(WorkOrderStatusOptions.InProcess.ToString());
                }
                else if (drDtl["Status"].ToString() == WorkOrderStatusOptions.Pending.ToString())
                {
                    bool bInProcess = false;
                    object obj = myDBSetting.ExecuteScalar("select count(*) from RPA_RM A with (nolock) inner join RPA_RMDTL B with (nolock) ON A.DocKey=B.DocKey where FromDocNo=? and Cancelled='F'", (object)drDtl["DocNo"]);
                    if (obj != null && obj != DBNull.Value)
                    {
                        if (BCE.Data.Convert.ToDecimal(obj) > 0)
                            bInProcess = true;
                    }
                    cbedit.Items.Clear();
                    if (!bInProcess)
                        cbedit.Items.Add(WorkOrderStatusOptions.Planned.ToString());
                    else
                        cbedit.Items.Add(WorkOrderStatusOptions.InProcess.ToString());
                    cbedit.Items.Add(WorkOrderStatusOptions.Cancel.ToString());
                }
            }
        }
    }

}
