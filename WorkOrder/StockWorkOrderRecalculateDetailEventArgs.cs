﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderRecalculateDetailEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Data;
using System.Data;

namespace Production.WorkOrder
{
  public class StockWorkOrderRecalculateDetailEventArgs
  {
    private DataRow myDetailRow;
    private DBSetting myDBSetting;

    public StockWorkOrderDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockWorkOrderDetailRecord(this.myDBSetting, this.myDetailRow);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    internal StockWorkOrderRecalculateDetailEventArgs(DBSetting dbSetting, DataRow detailRow)
    {
      this.myDetailRow = detailRow;
      this.myDBSetting = dbSetting;
    }
  }
}
