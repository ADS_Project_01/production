﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderOutstandingReport
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount;
using BCE.AutoCount.Report;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using BCE.Localization;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Production.WorkOrder
{
  public class StockWorkOrderOutstandingReport
  {
    protected object oldDataSource = new object();
    protected string oldDtlKeyList = "";
    private BasicReportOption myReportOption;
    protected DBSetting myDBSetting;
    protected const string MasterTableName = "Master";
    protected const string DetailTableName = "Detail";
    public const string OutstandingReportStyle = "Outstanding Stock Work Order";
    protected int oldGroupBy;
    protected int oldSortBy;
    protected bool oldShowCriteria;

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    internal StockWorkOrderOutstandingReport()
    {
      try
      {
        this.myReportOption = (BasicReportOption) PersistenceUtil.LoadUserSetting("OutstandingStockAssemblyOrderReportOption.setting");
      }
      catch
      {
      }
      if (this.myReportOption == null)
        this.myReportOption = new BasicReportOption();
    }

    public static StockWorkOrderOutstandingReport Create(DBSetting dbSetting)
    {
      StockWorkOrderOutstandingReport outstandingReport = (StockWorkOrderOutstandingReport) null;
      if (dbSetting.ServerType == DBServerType.SQL2000)
        outstandingReport = (StockWorkOrderOutstandingReport) new StockWorkOrderOutstandingSQL();
      else
        dbSetting.ThrowServerTypeNotSupportedException();
      outstandingReport.myDBSetting = dbSetting;
      return outstandingReport;
    }

    protected string GetWhereSQL(StockWorkOrderOutstandingFormCriteria reportingCriteria, SqlCommand cmd)
    {
      SearchCriteria searchCriteria = new SearchCriteria();
      BCE.AutoCount.SearchFilter.Filter documentNoFilter = reportingCriteria.DocumentNoFilter;
      searchCriteria.AddFilter(documentNoFilter);
      BCE.AutoCount.SearchFilter.Filter bomItemCodeFilter = reportingCriteria.BomItemCodeFilter;
      searchCriteria.AddFilter(bomItemCodeFilter);
      BCE.AutoCount.SearchFilter.Filter itemGroupFilter = reportingCriteria.ItemGroupFilter;
      searchCriteria.AddFilter(itemGroupFilter);
      BCE.AutoCount.SearchFilter.Filter itemTypeFilter = reportingCriteria.ItemTypeFilter;
      searchCriteria.AddFilter(itemTypeFilter);
      BCE.AutoCount.SearchFilter.Filter locationFilter = reportingCriteria.LocationFilter;
      searchCriteria.AddFilter(locationFilter);
      BCE.AutoCount.SearchFilter.Filter projectNoFilter = reportingCriteria.ProjectNoFilter;
      searchCriteria.AddFilter(projectNoFilter);
      BCE.AutoCount.SearchFilter.Filter departmentNoFilter = reportingCriteria.DepartmentNoFilter;
      searchCriteria.AddFilter(departmentNoFilter);
      BCE.AutoCount.SearchFilter.Filter codeDetailFilter = reportingCriteria.ItemCodeDetailFilter;
      searchCriteria.AddFilter(codeDetailFilter);
      BCE.AutoCount.SearchFilter.Filter groupDetailFilter = reportingCriteria.ItemGroupDetailFilter;
      searchCriteria.AddFilter(groupDetailFilter);
      BCE.AutoCount.SearchFilter.Filter typeDetailFilter = reportingCriteria.ItemTypeDetailFilter;
      searchCriteria.AddFilter(typeDetailFilter);
      BCE.AutoCount.SearchFilter.Filter locationDetailFilter = reportingCriteria.LocationDetailFilter;
      searchCriteria.AddFilter(locationDetailFilter);
      BCE.AutoCount.SearchFilter.Filter projectNoDetailFilter = reportingCriteria.ProjectNoDetailFilter;
      searchCriteria.AddFilter(projectNoDetailFilter);
      BCE.AutoCount.SearchFilter.Filter departmentNoDetailFilter = reportingCriteria.DepartmentNoDetailFilter;
      searchCriteria.AddFilter(departmentNoDetailFilter);
      int num = 1;
      searchCriteria.MatchAll = num != 0;
      SqlCommand sqlCommand = cmd;
      string str = searchCriteria.BuildSQL((IDbCommand) sqlCommand);
      if (reportingCriteria.FromDate != DateTime.MinValue)
      {
        str = !(str != "") ? str + " RPA_WO.DocDate >= @DocDate1 " : str + " AND RPA_WO.DocDate >= @DocDate1 ";
        cmd.Parameters.AddWithValue("@DocDate1", (object) reportingCriteria.FromDate);
      }
      if (reportingCriteria.EndDate != DateTime.MinValue)
      {
        str = !(str != "") ? str + " RPA_WO.DocDate <= @DocDate2 " : str + " AND RPA_WO.DocDate <= @DocDate2 ";
        cmd.Parameters.AddWithValue("@DocDate2", (object) reportingCriteria.EndDate);
      }
      return str;
    }

    public void PrintOutstandingListingReport(string dtlKeys, StockWorkOrderOutstandingFormCriteria criteria, bool isDefaultReport)
    {
      if (string.Compare(this.oldDtlKeyList, dtlKeys, false) != 0 || this.oldGroupBy != criteria.GroupBy || (this.oldSortBy != criteria.SortBy || this.oldShowCriteria != criteria.ShowCriteria))
      {
        this.oldDataSource = this.GetOutstandingListingReportDataSource(dtlKeys, criteria);
        this.oldDtlKeyList = dtlKeys;
        this.oldGroupBy = criteria.GroupBy;
        this.oldSortBy = criteria.SortBy;
        this.oldShowCriteria = criteria.ShowCriteria;
      }
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockWorkOrderOutstandingReportStringId.OutstandingStockAssemblyOrderListingReportTitle, new object[0]), "RPA_WO_OUTLIST_REPORT_PRINT", "RPA_WO_OUTLIST_REPORT_EXPORT", "");
      ReportTool.PrintReport("Outstanding Stock Work Order", this.oldDataSource, this.myDBSetting, isDefaultReport, this.myReportOption, reportInfo);
    }

    public void PreviewOutstandingListingReport(string dtlKeys, StockWorkOrderOutstandingFormCriteria criteria, bool isDefaultReport)
    {
      if (string.Compare(this.oldDtlKeyList, dtlKeys, false) != 0 || this.oldGroupBy != criteria.GroupBy || (this.oldSortBy != criteria.SortBy || this.oldShowCriteria != criteria.ShowCriteria))
      {
        this.oldDataSource = this.GetOutstandingListingReportDataSource(dtlKeys, criteria);
        this.oldDtlKeyList = dtlKeys;
        this.oldGroupBy = criteria.GroupBy;
        this.oldSortBy = criteria.SortBy;
        this.oldShowCriteria = criteria.ShowCriteria;
      }
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockWorkOrderOutstandingReportStringId.OutstandingStockAssemblyOrderListingReportTitle, new object[0]), "RPA_WO_OUTLIST_REPORT_PRINT", "RPA_WO_OUTLIST_REPORT_EXPORT", "");
      ReportTool.PreviewReport("Outstanding Stock Work Order", this.oldDataSource, this.myDBSetting, isDefaultReport, false, this.myReportOption, reportInfo);
    }

    private DocumentReportDataSet PreparingDetailListingReportDataSet(DataSet dsReportData, StockWorkOrderOutstandingFormCriteria criteria, string dataName)
    {
      DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, dataName, "Stock Work Order Master", "Stock Work Order Detail");
      DataTable table1 = dsReportData.Tables[0];
      dsReportData.Tables.Remove(table1);
      documentReportDataSet.Tables.Add(table1);
      DataTable table2 = dsReportData.Tables[0];
      dsReportData.Tables.Remove(table2);
      documentReportDataSet.Tables.Add(table2);
      DataTable table3 = new DataTable("Report Option");
      DataColumn column1 = new DataColumn("Criteria", typeof (string));
      table3.Columns.Add(column1);
      DataColumn column2 = new DataColumn("ShowCriteria", typeof (string));
      table3.Columns.Add(column2);
      DataColumn column3 = new DataColumn("GroupBy", typeof (string));
      table3.Columns.Add(column3);
      DataColumn column4 = new DataColumn("SortBy", typeof (string));
      table3.Columns.Add(column4);
      if (criteria != null)
      {
        DataRow row = table3.NewRow();
        string str1 = "";
        foreach (string str2 in criteria.ReadableTextArray)
          str1 = str1 + str2 + "\n";
        row["Criteria"] = (object) str1;
        row["ShowCriteria"] = criteria.ShowCriteria ? (object) "Yes" : (object) "No";
        row["GroupBy"] = (object) criteria.GroupBy;
        row["SortBy"] = (object) criteria.SortBy;
        table3.Rows.Add(row);
      }
      else
      {
        DataRow row = table3.NewRow();
        row["Criteria"] = (object) "Default criteria";
        row["ShowCriteria"] = (object) "Yes";
        row["GroupBy"] = (object) "Debtor Code";
        row["SortBy"] = (object) "Date";
        table3.Rows.Add(row);
      }
      DataRelation relation = new DataRelation("MasterDetailRelation", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["Detail"].Columns["DocKey"], false);
      documentReportDataSet.Relations.Add(relation);
      documentReportDataSet.Tables.Add(table3);
      documentReportDataSet.AddDefaultTables();
      return documentReportDataSet;
    }

    public object GetOutstandingListingReportDataSource(string docKeys, StockWorkOrderOutstandingFormCriteria criteria)
    {
      return (object) this.PreparingDetailListingReportDataSet(this.LoadOutstandingListingReportData(docKeys, criteria), criteria, "Outstanding Stock Work Order Listing");
    }

    public object GetOutstandingListingReportDesignerDataSource()
    {
      return (object) this.PreparingDetailListingReportDataSet(this.LoadOutstandingListingReportDesignerData(), (StockWorkOrderOutstandingFormCriteria) null, "Outstanding Stock Work Order Listing");
    }

    protected virtual DataSet LoadOutstandingListingReportData(string docKeys, StockWorkOrderOutstandingFormCriteria criteria)
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadOutstandingListingReportDesignerData()
    {
      return (DataSet) null;
    }

    public virtual void OutstandingListingBasicSearch(StockWorkOrderOutstandingFormCriteria criteria, string columnName, DataSet newDS, string chkEditColumnName)
    {
    }

    public virtual void BasicSearch(StockWorkOrderOutstandingFormCriteria criteria, string columnName, DataSet newDS, string chkEditColumnName)
    {
    }

    public virtual void CompletedBasicSearch(StockWorkOrderOutstandingFormCriteria criteria, string columnName, DataSet newDS, string chkEditColumnName)
    {
    }
  }
}
