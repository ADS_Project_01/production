﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.FormStockWorkOrderCmd
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controller.FunctionVisibility;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.Help;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.RegistryID.CommandFormStartupID;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Controls;
using BCE.Data;
using BCE.Localization;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace Production.WorkOrder
{
    //[BCE.AutoCount.PlugIn.MenuItem("Stock Work Order",BeginNewGroup =false,  MenuOrder =1, OpenAccessRight = "RPA_GEN_ITEMBOM_OPEN",)]
    [BCE.AutoCount.PlugIn.MenuItem("Work Order", 2, false, "RPA_WO_SHOW", "RPA_WO_OPEN")]

    [SingleInstanceThreadForm]

    public class FormStockWorkOrderCmd : XtraForm
  {
    private string myGroupSummaryId = "";
    private StockWorkOrderCommand myStockWorkOrderCmd;
    private DBSetting myDBSetting;
    private DocumentCommandFormControllerWithOutstandingReport myCommandFormController;
    private AsyncDataSetUpdateDelegate myUpdateDataSetDelegate;
    private ScriptObject myScriptObject;
    private IContainer components;
    private PanelHeader panelHeader1;
    private PreviewButton previewButton1;
    private PrintButton printButton1;
    private Bar bar1;
    private BarDockControl barDockControlBottom;
    private BarDockControl barDockControlLeft;
    private BarDockControl barDockControlRight;
    private BarDockControl barDockControlTop;
    private BarManager barManager1;
    private CheckEdit chkEdtShowAtStartup;
    private PanelControl panelCmd;
    private PanelControl panelControl1;
    private PanelControl panelGrid;
    private RepositoryItemCheckEdit repChkedtIsMultilevel;
    private RepositoryItemTextEdit repositoryItemTextEdit1;
    private SimpleButton sbtnDel;
    private SimpleButton sbtnEdit;
    private SimpleButton sbtnRefresh;
    private SimpleButton sbtnView;
    private GridColumn colAssemblyCost;
    private GridColumn colCancelled;
    private GridColumn colCreatedTimeStamp;
    private GridColumn colCreatedUserID;
    private GridColumn colDescription;
    private GridColumn colDocDate;
    private GridColumn colDocNo;
    private GridColumn colLastModified;
    private GridColumn colLastModifiedUserID;
    private GridColumn colLocation;
    private GridColumn colPrintCount;
    private GridColumn colMachineCode;
    private GridColumn colRefDocNo;
    private GridColumn colRemark1;
    private GridColumn colRemark2;
    private GridColumn colRemark3;
    private GridColumn colRemark4;
    private GridControl gridControlAll;
    private GridView gridViewStockMain;
    private Label label1;
    private Label lblShowAll;
    private GridColumn colStatus;
        private GridColumn gridColumn1;
        private GridColumn colTargetDate;
        private BarDockControl barDockControl3;
        private BarDockControl barDockControl4;
        private BarDockControl barDockControl2;
        private BarDockControl barDockControl1;
        private BarManager barManager2;
        private Bar bar3;
        private Bar bar4;
        private BarSubItem barSubItem1;
        private BarButtonItem barButtonItem1;
        private BarButtonItem barButtonItem2;
        private BarButtonItem barButtonItem3;
        private BarButtonItem barButtonItem4;
        private BarButtonItem barButtonItem5;
        private HyperLinkEdit lblFind;
        private HyperLinkEdit lblPrint;
        private HyperLinkEdit lblNew;
        private HyperLinkEdit lblClose;
        private GridColumn colProductionDate;

    public FormStockWorkOrderCmd(DBSetting dbSetting)
    {
      this.InitializeComponent();
      this.myDBSetting = dbSetting;
     // this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockWorkOrderCommandForm");
      this.myStockWorkOrderCmd = StockWorkOrderCommand.Create(this.myDBSetting);
      StockWorkOrderCommand.DataSetUpdate.Add(this.myDBSetting, this.myUpdateDataSetDelegate = new AsyncDataSetUpdateDelegate(this.UpdateDataSet));
      this.previewButton1.ReportType = "Stock Work Order Document";
      this.previewButton1.SetDBSetting(this.myDBSetting);
      this.printButton1.ReportType = "Stock Work Order Document";
      this.printButton1.SetDBSetting(this.myDBSetting);
      this.InitCommandFormController(this.barManager1);
      BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Work_Order.htm");
      DBSetting dbSetting1 = this.myDBSetting;
      string docType = "WO";
      long docKey = 0L;
      long eventKey = 0L;
      // ISSUE: variable of a boxed type
      StockWorkOrderString local = StockWorkOrderString.OpenStockWorkOrder;
      object[] objArray = new object[1];
      int index = 0;
      string loginUserId = this.myStockWorkOrderCmd.UserAuthentication.LoginUserID;
      objArray[index] = (object) loginUserId;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      string detail = "";
      Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
    }

    private void UpdateDataSet(DataSet ds, AsyncDataSetUpdateAction action)
    {
      DataTable dataTable1 = (DataTable) this.gridControlAll.DataSource;
      if (dataTable1.PrimaryKey.Length != 0)
      {
        DataTable dataTable2 = ds.Tables["Master"];
        long num = BCE.Data.Convert.ToInt64(dataTable2.Rows[0]["DocKey"]);
        DataRow row = dataTable1.Rows.Find((object) num);
        if (action == AsyncDataSetUpdateAction.Update)
        {
          if (row == null)
            row = dataTable1.NewRow();
          foreach (DataColumn index1 in (InternalDataCollectionBase) row.Table.Columns)
          {
            int index2 = dataTable2.Columns.IndexOf(index1.ColumnName);
            if (index2 >= 0)
              row[index1] = dataTable2.Rows[0][index2];
          }
          row.EndEdit();
          if (row.RowState == DataRowState.Detached)
            dataTable1.Rows.Add(row);
          for (int rowHandle = 0; rowHandle < this.gridViewStockMain.RowCount; ++rowHandle)
          {
            DataRow dataRow = this.gridViewStockMain.GetDataRow(rowHandle);
            if (dataRow != null && BCE.Data.Convert.ToInt64(dataRow["DocKey"]) == num)
            {
              this.gridViewStockMain.FocusedRowHandle = rowHandle;
              break;
            }
          }
        }
        else
        {
          if (row != null)
            row.Delete();
          dataTable1.AcceptChanges();
        }
      }
    }

    private void InitCommandFormController(BarManager barManager)
    {
      this.myCommandFormController = new DocumentCommandFormControllerWithOutstandingReport(this.myDBSetting, (Form) this, barManager);
      this.myCommandFormController.NewDocumentEvent = new MethodInvoker(this.NewDocument);
      this.myCommandFormController.EditDocumentEvent = new MethodInvoker(this.EditDocument);
      this.myCommandFormController.ViewDocumentEvent = new MethodInvoker(this.ViewDocument);
      this.myCommandFormController.DeleteDocumentEvent = new MethodInvoker(this.DeleteDocument);
      this.myCommandFormController.PreviewWithDefaultReportEvent = new PrintWithDefaultReportMethodInvoker(this.PreviewDocument);
      this.myCommandFormController.PrintWithDefaultReportEvent = new PrintWithDefaultReportMethodInvoker(this.PrintDocument);
      this.myCommandFormController.FindEvent = new MethodInvoker(this.Find);
      this.myCommandFormController.GeneralPrintEvent = new MethodInvoker(this.GeneralPrint);
      this.myCommandFormController.RefreshGridEvent = new MethodInvoker(this.RefreshGrid);
      this.myCommandFormController.ShowAllRecordsEvent = new ShowAllRecordsDelegate(this.ShowAllRecords);
      this.myCommandFormController.DesignDocumentStyleReportEvent = new MethodInvoker(this.DesignDocumentStyleReport);
      this.myCommandFormController.DesignListingStyleReportEvent = new MethodInvoker(this.DesignDocumentListingStyleReport);
      this.myCommandFormController.DesignDetailListingReportEvent = new MethodInvoker(this.DesignDetailListingReport);
      this.myCommandFormController.DesignOutstandingListingReportEvent = new MethodInvoker(this.DesignOutStandingDocumentListingStyleReport);
      this.myCommandFormController.ReportOptionEvent = new MethodInvoker(this.ReportOption);
      this.myCommandFormController.SetNewControl((Control) this.lblNew);
      this.myCommandFormController.SetFindControl((Control) this.lblFind);
      this.myCommandFormController.SetPrintControl((Control) this.lblPrint);
      this.myCommandFormController.SetShowAllControl((Control) this.lblShowAll);
      this.myCommandFormController.SetMainGridView(this.gridViewStockMain);
      this.myCommandFormController.SetEditDocumentControl((Control) this.sbtnEdit);
      this.myCommandFormController.SetViewDocumentControl((Control) this.sbtnView);
      this.myCommandFormController.SetDeleteDocumentControl((Control) this.sbtnDel);
      this.myCommandFormController.SetPreviewWithDefaultReportControl(this.previewButton1);
      this.myCommandFormController.SetPrintWithDefaultReportControl(this.printButton1);
      this.myCommandFormController.SetRefreshGridControl((Control) this.sbtnRefresh);
      Control[] arrControl = new Control[4];
      int index1 = 0;
      HyperLinkEdit hyperLinkEdit1 = this.lblNew;
      arrControl[index1] = (Control) hyperLinkEdit1;
      int index2 = 1;
      HyperLinkEdit hyperLinkEdit2 = this.lblFind;
      arrControl[index2] = (Control) hyperLinkEdit2;
      int index3 = 2;
      HyperLinkEdit hyperLinkEdit3 = this.lblPrint;
      arrControl[index3] = (Control) hyperLinkEdit3;
      int index4 = 3;
      Label label = this.lblShowAll;
      arrControl[index4] = (Control) label;
      Utils.SetHottrackControls(arrControl);
    }

    public static void StartEntryForm(StockWorkOrder stockAssemblyOrder)
    {
      string assemblyQualifiedName = typeof (FormStockWorkOrderEntry).AssemblyQualifiedName;
      System.Type[] types = new System.Type[1];
      int index1 = 0;
      System.Type type = stockAssemblyOrder.GetType();
      types[index1] = type;
      object[] parameters = new object[1];
      int index2 = 0;
      StockWorkOrder stockAssemblyOrder1 = stockAssemblyOrder;
      parameters[index2] = (object) stockAssemblyOrder1;
      new ThreadForm(assemblyQualifiedName, types, parameters).Show();
    }

    public static void ViewDocument(DBSetting dbSetting, long docKey)
    {
      if (UserAuthentication.GetOrCreate(dbSetting).AccessRight.IsAccessible("RPA_WO_VIEW", true))
      {
        try
        {
          StockWorkOrder stockAssemblyOrder = StockWorkOrderCommand.Create(dbSetting).View(docKey);
          if (stockAssemblyOrder == null)
            AppMessage.ShowMessage((IWin32Window) null, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
          else
            FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) null, ex.Message);
        }
      }
    }

    public static void NewDocument(DBSetting dbSetting)
    {
      if (UserAuthentication.GetOrCreate(dbSetting).AccessRight.IsAccessible("RPA_WO_NEW", true))
      {
        try
        {
          StockWorkOrder stockAssemblyOrder = StockWorkOrderCommand.Create(dbSetting).AddNew();
          if (stockAssemblyOrder != null)
            FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) null, ex.Message);
        }
      }
    }

    public static void EditTempDocument(DBSetting dbSetting, long docKey)
    {
      try
      {
        StockWorkOrder stockAssemblyOrder = StockWorkOrderCommand.Create(dbSetting).LoadFromTempDocument(docKey);
        if (stockAssemblyOrder == null)
          AppMessage.ShowMessage((IWin32Window) null, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
        else
          FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
      }
      catch (DataAccessException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) null, ex.Message);
      }
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
      string fieldname1 = "DocDate";
      string fieldtype1 = "Date";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "ActQty";
      string fieldtype2 = "Quantity";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "EstQty";
      string fieldtype3 = "Quantity";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "EstHPP";
      string fieldtype4 = "Currency";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "ActHPP";
      string fieldtype5 = "Currency";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "LastModified";
      string fieldtype6 = "DateTime";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "CreatedTimeStamp";
      string fieldtype7 = "DateTime";
      formControlUtil.AddField(fieldname7, fieldtype7);
            string fieldname8 = "ProductionDate";
            string fieldtype8 = "DateTime";
            formControlUtil.AddField(fieldname8, fieldtype8);
            string fieldname9 = "TargetDate";
            string fieldtype9= "DateTime";
            formControlUtil.AddField(fieldname9, fieldtype9);
            FormStockWorkOrderCmd assemblyOrderCmd = this;
      formControlUtil.InitControls((Control) assemblyOrderCmd);
    }

        private void InitGroupSummary()
        {
            StringBuilder stringBuilder = new StringBuilder(40);
            stringBuilder.Append("DocKey");
            //if (this.colEstQty.Visible)
            //    stringBuilder.Append("EstQty");
            //if (this.colEstHPP.Visible)
            //    stringBuilder.Append("EstHPP");
            //if (this.colActHPP.Visible)
            //    stringBuilder.Append("ActHPP");
            //if (this.colActQty.Visible)
            //    stringBuilder.Append("ActQty");
            string str1 = ((object)stringBuilder).ToString();
            if (str1 != this.myGroupSummaryId)
            {
                this.myGroupSummaryId = str1;
                this.gridViewStockMain.GroupSummary.Clear();
                GridGroupSummaryItemCollection groupSummary1 = this.gridViewStockMain.GroupSummary;
                int num1 = 3;
                string fieldName1 = "DocNo";
                // ISSUE: variable of the null type
                GridColumn local1 = null;
                // ISSUE: variable of a boxed type
                GridGroupSummaryItemStringId local2 = GridGroupSummaryItemStringId.Count;
                object[] objArray1 = new object[1];
                int index1 = 0;
                string str2 = "{0}";
                objArray1[index1] = (object)str2;
                string string1 = BCE.Localization.Localizer.GetString((Enum)local2, objArray1);
                groupSummary1.Add((SummaryItemType)num1, fieldName1, (GridColumn)local1, string1);
                //if (this.colEstQty.Visible)
                //{
                //    GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockMain.GroupSummary;
                //    int num2 = 0;
                //    string fieldName2 = "EstQty";
                //    // ISSUE: variable of the null type
                //    GridColumn local3 = null;
                //    // ISSUE: variable of a boxed type
                //    GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.NetTotal;
                //    object[] objArray2 = new object[1];
                //    int index2 = 0;
                //    string currencyFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCurrencyFormatString(0);
                //    objArray2[index2] = (object)currencyFormatString;
                //    string string2 = BCE.Localization.Localizer.GetString((Enum)local4, objArray2);
                //    groupSummary2.Add((SummaryItemType)num2, fieldName2, (GridColumn)local3, string2);
                //}
                //if (this.colEstHPP.Visible)
                //{
                //    GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockMain.GroupSummary;
                //    int num2 = 0;
                //    string fieldName2 = "EstHPP";
                //    // ISSUE: variable of the null type
                //    GridColumn local3 = null;
                //    // ISSUE: variable of a boxed type
                //    GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.Total;
                //    object[] objArray2 = new object[1];
                //    int index2 = 0;
                //    string currencyFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCurrencyFormatString(0);
                //    objArray2[index2] = (object)currencyFormatString;
                //    string string2 = BCE.Localization.Localizer.GetString((Enum)local4, objArray2);
                //    groupSummary2.Add((SummaryItemType)num2, fieldName2, (GridColumn)local3, string2);
                //}
                //if (this.colActQty.Visible)
                //{
                //    GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockMain.GroupSummary;
                //    int num2 = 0;
                //    string fieldName2 = "ActQty";
                //    // ISSUE: variable of the null type
                //    GridColumn local3 = null;
                //    // ISSUE: variable of a boxed type
                //    GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.AssemblyCost;
                //    object[] objArray2 = new object[1];
                //    int index2 = 0;
                //    string costFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCostFormatString(0);
                //    objArray2[index2] = (object)costFormatString;
                //    string string2 = BCE.Localization.Localizer.GetString((Enum)local4, objArray2);
                //    groupSummary2.Add((SummaryItemType)num2, fieldName2, (GridColumn)local3, string2);

                //}
                //if (this.colActHPP.Visible)
                //{
                //    GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockMain.GroupSummary;
                //    int num2 = 0;
                //    string fieldName2 = "ActHPP";
                //    // ISSUE: variable of the null type
                //    GridColumn local3 = null;
                //    // ISSUE: variable of a boxed type
                //    GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.AssemblyCost;
                //    object[] objArray2 = new object[1];
                //    int index2 = 0;
                //    string costFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCostFormatString(0);
                //    objArray2[index2] = (object)costFormatString;
                //    string string2 = BCE.Localization.Localizer.GetString((Enum)local4, objArray2);
                //    groupSummary2.Add((SummaryItemType)num2, fieldName2, (GridColumn)local3, string2);
                //}
            }
        }

    private void LoadCommandFormStartupState()
    {
      bool boolean = DBRegistry.Create(this.myDBSetting).GetBoolean((IRegistryID) new StockAssemblyOrderCommandFormStartupID());
      this.chkEdtShowAtStartup.Checked = boolean;
      this.chkEdtShowAtStartup.CheckedChanged += new EventHandler(this.chkEdtShowAtStartup_CheckedChanged);
      this.FormInitialize();
      this.myCommandFormController.ShowAll(boolean);
    }

    private void SaveCommandFormStartupState()
    {
      DBRegistry dbRegistry = DBRegistry.Create(this.myDBSetting);
            StockAssemblyOrderCommandFormStartupID commandFormStartupId1 = new StockAssemblyOrderCommandFormStartupID();
      commandFormStartupId1.NewValue = (object)  (this.chkEdtShowAtStartup.Checked ? true : false);
            StockAssemblyOrderCommandFormStartupID commandFormStartupId2 = commandFormStartupId1;
      dbRegistry.SetValue((IRegistryID) commandFormStartupId2);
    }

    private long GetSelectedDocKey(ref string docNo)
    {
      if (this.gridViewStockMain.FocusedRowHandle >= 0)
      {
        DataRow dataRow = this.gridViewStockMain.GetDataRow(this.gridViewStockMain.FocusedRowHandle);
        if (dataRow != null)
        {
          long num = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
          docNo = dataRow.Table.Columns.IndexOf("DocNo") < 0 ? "(Internal DocKey=" + num.ToString() + ")" : dataRow["DocNo"].ToString();
          return num;
        }
        else
          return -1L;
      }
      else
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordNotSelected, new object[0]));
        return -1L;
      }
    }

    private void View(long docKey)
    {
      try
      {
        StockWorkOrder stockAssemblyOrder = this.myStockWorkOrderCmd.View(docKey);
        if (stockAssemblyOrder == null)
          AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
        else
          FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
      }
    }

    private void Edit(long docKey)
    {
      try
      {
        StockWorkOrder stockAssemblyOrder = this.myStockWorkOrderCmd.Edit(docKey);
        if (stockAssemblyOrder == null)
          AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
        else
          FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
      }
    }

    private void SearchAll(bool allColumns)
    {
      ColumnView view = this.gridViewStockMain.Columns.View;
      string prefix = "RPA_WO";
      int num = allColumns ? 1 : 0;
      string[] mustIncludeColNames = new string[1];
      int index1 = 0;
      string str1 = "DocKey";
      mustIncludeColNames[index1] = str1;
      string[] excludeColNames = new string[3];
      int index2 = 0;
      string str2 = "Delete";
      excludeColNames[index2] = str2;
      int index3 = 1;
      //string str3 = "TransferFrom";
      //excludeColNames[index3] = str3;
      //int index4 = 2;
      //string str4 = "TransferTo";
      //excludeColNames[index4] = str4;
      string columnSQL = ColumnViewUtils.BuildSQLColumnListFromColumnView(view, prefix, num != 0, mustIncludeColNames, excludeColNames);
      try
      {
        this.myStockWorkOrderCmd.InquireAllMaster(columnSQL);
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage(ex.Message);
      }
      //FormStockWorkOrderCmd.FormLoadDataEventArgs loadDataEventArgs1 = new FormStockWorkOrderCmd.FormLoadDataEventArgs(this, this.myStockWorkOrderCmd.DataTableAllMaster);
      //ScriptObject scriptObject = this.myScriptObject;
      //string name = "OnFormLoadData";
      //System.Type[] types = new System.Type[1];
      //int index5 = 0;
      //System.Type type = loadDataEventArgs1.GetType();
      //types[index5] = type;
      //object[] objArray = new object[1];
      //int index6 = 0;
      //FormStockWorkOrderCmd.FormLoadDataEventArgs loadDataEventArgs2 = loadDataEventArgs1;
      //objArray[index6] = (object) loadDataEventArgs2;
      //scriptObject.RunMethod(name, types, objArray);
    }

    private void ReloadAllColumns(object sender, EventArgs e)
    {
      this.SearchAll(true);
    }

    private void NewDocument()
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_NEW", (XtraForm) this))
      {
        try
        {
          StockWorkOrder stockAssemblyOrder = this.myStockWorkOrderCmd.AddNew();
          if (stockAssemblyOrder != null)
            FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
        }
      }
    }

    private void Find()
    {
      using (FormStockWorkFind stockAssemblyFind = new FormStockWorkFind(this.myStockWorkOrderCmd))
      {
        int num = (int) stockAssemblyFind.ShowDialog();
      }
    }

    private void EditDocument()
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_EDIT", (XtraForm) this))
      {                
        string docNo = "";
        long selectedDocKey = this.GetSelectedDocKey(ref docNo);
                object obj = myDBSetting.ExecuteScalar("select dbo.F_RPA_WOStatus(?)", (object)selectedDocKey);
                if (obj != null && obj != DBNull.Value)
                {
                    if (obj.ToString()!= WorkOrderStatusOptions.Planned.ToString() && obj.ToString() != WorkOrderStatusOptions.Pending.ToString())// WorkOrderStatusOptions.Closed.ToString() || obj.ToString() == WorkOrderStatusOptions.Cancel.ToString())
                    {
                        AppMessage.ShowErrorMessage((IWin32Window)this, "Status Document WO has been " + obj.ToString() + ", Edit aborted...");
                        return;
                    }
                }                
                        if (selectedDocKey > -1L && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", selectedDocKey) <= 0 || this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_EDIT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedEditPrintedDocument, new object[0]))))
                            this.Edit(selectedDocKey);
                    
      }
    }

    private void ViewDocument()
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_VIEW", (XtraForm) this))
      {
        string docNo = "";
        long selectedDocKey = this.GetSelectedDocKey(ref docNo);
        if (selectedDocKey > -1L)
          this.View(selectedDocKey);
      }
    }

    private void PreviewDocument(bool useDefaultReport)
    {
      string docNo = "";
      long selectedDocKey = this.GetSelectedDocKey(ref docNo);
      if (selectedDocKey > -1L && this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DOC_REPORT_PREVIEW", (XtraForm) this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", selectedDocKey) <= 0 || this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_PREVIEW", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPreviewPrintedDocument, new object[0]))))
      {
        // ISSUE: variable of a boxed type
        StockWorkOrderString local =  StockWorkOrderString.StockWorkOrder;
        object[] objArray1 = new object[1];
        int index1 = 0;
        string str = docNo;
        objArray1[index1] = (object) str;
        ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum) local, objArray1), "RPA_WO_DOC_REPORT_PRINT", "RPA_WO_DOC_REPORT_EXPORT", "");
        reportInfo.DocType = "WO";
        reportInfo.DocKey = selectedDocKey;
        reportInfo.UpdatePrintCountTableName = "RPA_WO";
        reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
        reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
        reportInfo.EmailAndFaxInfo = StockWorkOrderCommand.GetEmailAndFaxInfo(selectedDocKey, this.myDBSetting);
        //BeforePreviewDocumentEventArgs documentEventArgs1 = new BeforePreviewDocumentEventArgs(reportInfo.EmailAndFaxInfo, reportInfo.DocKey, this.myDBSetting);
        //ScriptObject scriptObject = this.myScriptObject;
        //string name = "BeforePreviewDocument";
        //System.Type[] types = new System.Type[1];
        //int index2 = 0;
        //System.Type type = documentEventArgs1.GetType();
        //types[index2] = type;
        //object[] objArray2 = new object[1];
        //int index3 = 0;
        //BeforePreviewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
        //objArray2[index3] = (object) documentEventArgs2;
        //scriptObject.RunMethod(name, types, objArray2);
        //reportInfo.Tag = (object) documentEventArgs1;
        //if (documentEventArgs1.AllowPreview)
          ReportTool.PreviewReport("Stock Work Order Document", this.myStockWorkOrderCmd.GetReportDataSource(selectedDocKey), this.myDBSetting, useDefaultReport, false, this.myStockWorkOrderCmd.ReportOption, reportInfo);
      }
    }

    private bool CheckBeforePrint(ReportInfo reportInfo)
    {
      if (!((BeforePreviewDocumentEventArgs) reportInfo.Tag).AllowPrint || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", reportInfo.DocKey) > 0 && !this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_PRINT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0])))
        return false;
      else
        return true;
    }

    private bool CheckBeforeExport(ReportInfo reportInfo)
    {
      if (!((BeforePreviewDocumentEventArgs) reportInfo.Tag).AllowExport || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", reportInfo.DocKey) > 0 && !this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_EXPORT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedExportPrintedDocument, new object[0])))
        return false;
      else
        return true;
    }

    private void PrintDocument(bool useDefaultReport)
    {
      string docNo = "";
      long selectedDocKey = this.GetSelectedDocKey(ref docNo);
      if (selectedDocKey > -1L && this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DOC_REPORT_PRINT", (XtraForm) this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", selectedDocKey) <= 0 || this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_PRINT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0]))))
      {
        // ISSUE: variable of a boxed type
        StockWorkOrderString local = StockWorkOrderString.StockWorkOrder;
        object[] objArray = new object[1];
        int index = 0;
        string str = docNo;
        objArray[index] = (object) str;
        ReportTool.PrintReport("Stock Work Order Document", this.myStockWorkOrderCmd.GetReportDataSource(selectedDocKey), this.myDBSetting, useDefaultReport, this.myStockWorkOrderCmd.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum) local, objArray), "RPA_WO_DOC_REPORT_PRINT", "RPA_WO_DOC_REPORT_EXPORT", "")
        {
          DocType = "WO",
          DocKey = selectedDocKey,
          UpdatePrintCountTableName = "RPA_WO",
          EmailAndFaxInfo = StockWorkOrderCommand.GetEmailAndFaxInfo(selectedDocKey, this.myDBSetting)
        });
      }
    }

        private void DeleteDocument()
        {
            if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DELETE", (XtraForm)this))
            {
                string docNo = "";
                
                long selectedDocKey = this.GetSelectedDocKey(ref docNo);
                if (selectedDocKey > -1L && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", selectedDocKey) <= 0 || this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_DELETE", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedDeletePrintedDocument, new object[0]))))
                {
                    object obj = myDBSetting.ExecuteScalar("select status from RPA_WO where DocKey=?", (object)selectedDocKey);
                    if (obj != null && obj != DBNull.Value)
                    {
                        if (obj.ToString() == WorkOrderStatusOptions.Closed.ToString() || obj.ToString() == WorkOrderStatusOptions.Cancel.ToString())
                        {
                            AppMessage.ShowErrorMessage((IWin32Window)this, "Status Document WO has been " + obj.ToString() + ", Delete aborted...");
                            return;
                        }
                    }
                    // ISSUE: variable of a boxed type
                    StockAssemblyOrderStringId local = StockAssemblyOrderStringId.ConfirmMessage_DeleteStockAssemblyOrder;
                    object[] objArray = new object[1];
                    int index = 0;
                    string str = docNo;
                    objArray[index] = (object)str;
                    if (AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum)local, objArray)))
                    {
                        try
                        {
                            this.myStockWorkOrderCmd.Delete(selectedDocKey);
                            AppMessage.ShowMessage(BCE.Localization.Localizer.GetString((Enum)StockAssemblyOrderStringId.ShowMessage_DeleteSuccessfully, new object[0]));
                        }
                        catch (DBConcurrencyException ex)
                        {
                            AppMessage.ShowErrorMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockAssemblyOrderStringId.ErrorMessage_UnableFindCauseDeletedByOthers, new object[0]));
                        }
                        catch (AppException ex)
                        {
                            AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                        }
                    }
                }
            }
        }

    private void ShowAllRecords(bool show)
    {
      this.panelGrid.Visible = show;
      if (this.panelGrid.Visible && this.gridControlAll.DataSource == null)
      {
        this.SearchAll(false);
        this.gridControlAll.DataSource = (object) this.myStockWorkOrderCmd.DataTableAllMaster;
      }
    }

    private void RefreshGrid()
    {
      this.SearchAll(false);
    }

    private void GeneralPrint()
    {
      string[] rptTypes = new string[2];
      int index1 = 0;
      string string1 = BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.BatchPrintStockWorkOrder_PrintStockWorkOrderListing, new object[0]);
      rptTypes[index1] = string1;
      int index2 = 1;
      string string2 = BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.PrintStockWorkOrderDetailListing, new object[0]);
      rptTypes[index2] = string2;
      int index3 = 2;
      //string string3 = BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.PrintOutstandingStockWorkOrderListing, new object[0]);
      //rptTypes[index3] = string3;
      switch (FormSelectListingReportType.SelectListingReportType(rptTypes))
      {
                case 0:
                    //if (this.myStockWorkOrderCmd.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_LISTING_REPORT_SHOW", true))
                    {
                        FormStockWorkOrderPrintListing form = new FormStockWorkOrderPrintListing(myDBSetting);
                        form.WindowState = FormWindowState.Maximized;
                        form.Show();
                    }
                    break;
                case 1:
                    //if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_RCV_DTLLIST_REPORT_SHOW", true))
                    {
                        FormStockWorkOrderPrintDetailListing form = new FormStockWorkOrderPrintDetailListing(myDBSetting);
                        form.WindowState = FormWindowState.Maximized;
                        form.Show();
                    }
                    break;
            }
    }

    private void DesignDocumentStyleReport()
    {
      ReportTool.DesignReport("Stock Work Order Document", this.myDBSetting);
    }

    private void DesignDocumentListingStyleReport()
    {
      ReportTool.DesignReport("Stock Work Order Listing", this.myDBSetting);
    }

    private void DesignDetailListingReport()
    {
      ReportTool.DesignReport("Stock Work Order Detail Listing", this.myDBSetting);
    }

    private void DesignOutStandingDocumentListingStyleReport()
    {
      ReportTool.DesignReport("Outstanding Stock Work Order", this.myDBSetting);
    }

    private void ReportOption()
    {
      FormBasicReportOption.ShowReportOption(this.myStockWorkOrderCmd.ReportOption);
      this.myStockWorkOrderCmd.SaveReportOption();
    }

    private void repositoryItemTextEdit1_FormatEditValue(object sender, ConvertEditValueEventArgs e)
    {
      if (e.Value != null)
      {
        if (e.Value.ToString() == "T")
          e.Value = (object) BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Cancelled, new object[0]);
        else
          e.Value = (object) "";
      }
    }

    private void chkEdtShowAtStartup_CheckedChanged(object sender, EventArgs e)
    {
      this.SaveCommandFormStartupState();
    }

    private void FormStockAssemblyCmd_Load(object sender, EventArgs e)
    {
      new UDFUtil(this.myDBSetting).SetupGrid(this.gridViewStockMain, "RPA_WO", false);
      new CustomizeGridLayout(this.myDBSetting, this.Name, this.gridViewStockMain, new EventHandler(this.ReloadAllColumns)).PageHeader = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_StockAssemblyOrderListing, new object[0]);
      foreach (GridColumn gridColumn in (CollectionBase) this.gridViewStockMain.Columns)
        gridColumn.OptionsColumn.AllowEdit = false;
      this.InitFormControls();
      this.LoadCommandFormStartupState();
      //this.sbtnAOProcessing.Visible = new BusinessFlow(this.myDBSetting).IsAssemblyOrderProcessingVisible();
    }

    private void gridViewStockMain_Layout(object sender, EventArgs e)
    {
      if (this.gridControlAll.DataSource != null)
        this.InitGroupSummary();
    }

    private void sbtnAOProcessing_Click(object sender, EventArgs e)
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("BFLOW_AOP_OPEN", (XtraForm) this))
      {
        string docNo = "";
        long selectedDocKey = this.GetSelectedDocKey(ref docNo);
        if (selectedDocKey > -1L)
          this.myStockWorkOrderCmd.StartBusinessFlowForm(selectedDocKey);
      }
    }

    private void FormStockWorkOrderCmd_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
    {
      this.gridControlAll.DataSource = (object) null;
      this.myStockWorkOrderCmd = (StockWorkOrderCommand) null;
      if (this.myUpdateDataSetDelegate != null)
      {
        StockWorkOrderCommand.DataSetUpdate.Remove(this.myDBSetting, this.myUpdateDataSetDelegate);
        this.myUpdateDataSetDelegate = (AsyncDataSetUpdateDelegate) null;
      }
      if (this.myCommandFormController != null)
      {
        this.myCommandFormController.Cleanup();
        this.myCommandFormController = (DocumentCommandFormControllerWithOutstandingReport) null;
      }
      //FormStockWorkOrderCmd.FormClosedEventArgs formClosedEventArgs1 = new FormStockWorkOrderCmd.FormClosedEventArgs(this);
      //ScriptObject scriptObject = this.myScriptObject;
      //string name = "OnFormClosed";
      //System.Type[] types = new System.Type[1];
      //int index1 = 0;
      //System.Type type = formClosedEventArgs1.GetType();
      //types[index1] = type;
      //object[] objArray = new object[1];
      //int index2 = 0;
      //FormStockWorkOrderCmd.FormClosedEventArgs formClosedEventArgs2 = formClosedEventArgs1;
      //objArray[index2] = (object) formClosedEventArgs2;
      //scriptObject.RunMethod(name, types, objArray);
    }

    private void FormInitialize()
    {
      FormStockWorkOrderCmd.FormInitializeEventArgs initializeEventArgs1 = new FormStockWorkOrderCmd.FormInitializeEventArgs(this);
      ScriptObject scriptObject = this.myScriptObject;
      string name = "OnFormInitialize";
      System.Type[] types = new System.Type[1];
      int index1 = 0;
      System.Type type = initializeEventArgs1.GetType();
      types[index1] = type;
      object[] objArray = new object[1];
      int index2 = 0;
      //FormStockWorkOrderCmd.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
      //objArray[index2] = (object) initializeEventArgs2;
      //scriptObject.RunMethod(name, types, objArray);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            this.panelGrid = new DevExpress.XtraEditors.PanelControl();
            this.gridControlAll = new DevExpress.XtraGrid.GridControl();
            this.gridViewStockMain = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCancelled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCreatedTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTargetDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkedtIsMultilevel = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.printButton1 = new BCE.AutoCount.Controls.PrintButton();
            this.previewButton1 = new BCE.AutoCount.Controls.PreviewButton();
            this.chkEdtShowAtStartup = new DevExpress.XtraEditors.CheckEdit();
            this.sbtnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnView = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.colAssemblyCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.panelCmd = new DevExpress.XtraEditors.PanelControl();
            this.lblClose = new DevExpress.XtraEditors.HyperLinkEdit();
            this.lblFind = new DevExpress.XtraEditors.HyperLinkEdit();
            this.lblPrint = new DevExpress.XtraEditors.HyperLinkEdit();
            this.lblNew = new DevExpress.XtraEditors.HyperLinkEdit();
            this.lblShowAll = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).BeginInit();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStockMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkedtIsMultilevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEdtShowAtStartup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCmd)).BeginInit();
            this.panelCmd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblClose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelGrid
            // 
            this.panelGrid.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelGrid.Controls.Add(this.gridControlAll);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 216);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(880, 170);
            this.panelGrid.TabIndex = 0;
            // 
            // gridControlAll
            // 
            this.gridControlAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAll.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlAll.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlAll.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlAll.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlAll.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlAll.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlAll.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlAll.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlAll.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlAll.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlAll.Location = new System.Drawing.Point(0, 0);
            this.gridControlAll.MainView = this.gridViewStockMain;
            this.gridControlAll.Name = "gridControlAll";
            this.gridControlAll.Size = new System.Drawing.Size(880, 170);
            this.gridControlAll.TabIndex = 3;
            this.gridControlAll.UseEmbeddedNavigator = true;
            this.gridControlAll.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStockMain});
            // 
            // gridViewStockMain
            // 
            this.gridViewStockMain.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCancelled,
            this.colCreatedTimeStamp,
            this.colCreatedUserID,
            this.colDescription,
            this.colDocDate,
            this.colDocNo,
            this.colProductionDate,
            this.colTargetDate,
            this.colLastModified,
            this.colLastModifiedUserID,
            this.colLocation,
            this.colPrintCount,
            this.colMachineCode,
            this.colRefDocNo,
            this.colRemark1,
            this.colRemark2,
            this.colRemark3,
            this.colRemark4,
            this.colStatus});
            this.gridViewStockMain.GridControl = this.gridControlAll;
            this.gridViewStockMain.Name = "gridViewStockMain";
            this.gridViewStockMain.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDocDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewStockMain.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewStockMain_CustomUnboundColumnData);
            this.gridViewStockMain.Layout += new System.EventHandler(this.gridViewStockMain_Layout);
            // 
            // colCancelled
            // 
            this.colCancelled.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCancelled.FieldName = "Cancelled";
            this.colCancelled.Name = "colCancelled";
            this.colCancelled.Visible = true;
            this.colCancelled.VisibleIndex = 4;
            this.colCancelled.Width = 90;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.FormatEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repositoryItemTextEdit1_FormatEditValue);
            // 
            // colCreatedTimeStamp
            // 
            this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
            this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.Name = "colCreatedUserID";
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 3;
            this.colDescription.Width = 149;
            // 
            // colDocDate
            // 
            this.colDocDate.FieldName = "DocDate";
            this.colDocDate.Name = "colDocDate";
            this.colDocDate.Visible = true;
            this.colDocDate.VisibleIndex = 1;
            this.colDocDate.Width = 94;
            // 
            // colDocNo
            // 
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 0;
            this.colDocNo.Width = 94;
            // 
            // colProductionDate
            // 
            this.colProductionDate.Caption = "Production Start Date";
            this.colProductionDate.FieldName = "ProductionStartDate";
            this.colProductionDate.Name = "colProductionDate";
            // 
            // colTargetDate
            // 
            this.colTargetDate.Caption = "Production End Date";
            this.colTargetDate.FieldName = "ProductionEndDate";
            this.colTargetDate.Name = "colTargetDate";
            this.colTargetDate.Visible = true;
            this.colTargetDate.VisibleIndex = 5;
            this.colTargetDate.Width = 89;
            // 
            // colLastModified
            // 
            this.colLastModified.FieldName = "LastModified";
            this.colLastModified.Name = "colLastModified";
            // 
            // colLastModifiedUserID
            // 
            this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
            this.colLastModifiedUserID.Name = "colLastModifiedUserID";
            // 
            // colLocation
            // 
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            // 
            // colPrintCount
            // 
            this.colPrintCount.FieldName = "PrintCount";
            this.colPrintCount.Name = "colPrintCount";
            // 
            // colMachineCode
            // 
            this.colMachineCode.Caption = "MachineCode";
            this.colMachineCode.FieldName = "MachineCode";
            this.colMachineCode.Name = "colMachineCode";
            // 
            // colRefDocNo
            // 
            this.colRefDocNo.FieldName = "RefDocNo";
            this.colRefDocNo.Name = "colRefDocNo";
            // 
            // colRemark1
            // 
            this.colRemark1.FieldName = "Remark1";
            this.colRemark1.Name = "colRemark1";
            // 
            // colRemark2
            // 
            this.colRemark2.FieldName = "Remark2";
            this.colRemark2.Name = "colRemark2";
            // 
            // colRemark3
            // 
            this.colRemark3.FieldName = "Remark3";
            this.colRemark3.Name = "colRemark3";
            // 
            // colRemark4
            // 
            this.colRemark4.FieldName = "Remark4";
            this.colRemark4.Name = "colRemark4";
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status2";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 2;
            // 
            // repChkedtIsMultilevel
            // 
            this.repChkedtIsMultilevel.Caption = "Check";
            this.repChkedtIsMultilevel.Name = "repChkedtIsMultilevel";
            this.repChkedtIsMultilevel.ValueChecked = "T";
            this.repChkedtIsMultilevel.ValueUnchecked = "F";
            // 
            // printButton1
            // 
            this.printButton1.Location = new System.Drawing.Point(216, 6);
            this.printButton1.Name = "printButton1";
            this.printButton1.ReportType = "";
            this.printButton1.Size = new System.Drawing.Size(64, 20);
            this.printButton1.TabIndex = 1;
            this.printButton1.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // previewButton1
            // 
            this.previewButton1.Location = new System.Drawing.Point(146, 6);
            this.previewButton1.Name = "previewButton1";
            this.previewButton1.ReportType = "";
            this.previewButton1.Size = new System.Drawing.Size(64, 20);
            this.previewButton1.TabIndex = 2;
            this.previewButton1.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // chkEdtShowAtStartup
            // 
            this.chkEdtShowAtStartup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkEdtShowAtStartup.Location = new System.Drawing.Point(734, 4);
            this.chkEdtShowAtStartup.Name = "chkEdtShowAtStartup";
            this.chkEdtShowAtStartup.Properties.Caption = "Show this grid at startup";
            this.chkEdtShowAtStartup.Size = new System.Drawing.Size(143, 19);
            this.chkEdtShowAtStartup.TabIndex = 4;
            this.chkEdtShowAtStartup.CheckedChanged += new System.EventHandler(this.chkEdtShowAtStartup_CheckedChanged);
            // 
            // sbtnRefresh
            // 
            this.sbtnRefresh.Location = new System.Drawing.Point(356, 6);
            this.sbtnRefresh.Name = "sbtnRefresh";
            this.sbtnRefresh.Size = new System.Drawing.Size(64, 20);
            this.sbtnRefresh.TabIndex = 5;
            this.sbtnRefresh.Text = "Refresh";
            // 
            // sbtnView
            // 
            this.sbtnView.Location = new System.Drawing.Point(76, 6);
            this.sbtnView.Name = "sbtnView";
            this.sbtnView.Size = new System.Drawing.Size(64, 20);
            this.sbtnView.TabIndex = 6;
            this.sbtnView.Text = "View";
            // 
            // sbtnDel
            // 
            this.sbtnDel.Appearance.ForeColor = System.Drawing.Color.Red;
            this.sbtnDel.Appearance.Options.UseForeColor = true;
            this.sbtnDel.Location = new System.Drawing.Point(286, 6);
            this.sbtnDel.Name = "sbtnDel";
            this.sbtnDel.Size = new System.Drawing.Size(64, 20);
            this.sbtnDel.TabIndex = 7;
            this.sbtnDel.Text = "Delete";
            this.sbtnDel.Click += new System.EventHandler(this.sbtnDel_Click);
            // 
            // sbtnEdit
            // 
            this.sbtnEdit.Location = new System.Drawing.Point(6, 6);
            this.sbtnEdit.Name = "sbtnEdit";
            this.sbtnEdit.Size = new System.Drawing.Size(64, 20);
            this.sbtnEdit.TabIndex = 8;
            this.sbtnEdit.Text = "Edit";
            this.sbtnEdit.Click += new System.EventHandler(this.sbtnEdit_Click);
            // 
            // colAssemblyCost
            // 
            this.colAssemblyCost.FieldName = "AssemblyCost";
            this.colAssemblyCost.Name = "colAssemblyCost";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 22);
            this.barDockControlTop.Size = new System.Drawing.Size(880, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 386);
            this.barDockControlBottom.Size = new System.Drawing.Size(880, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 364);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(880, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 364);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            // 
            // panelCmd
            // 
            this.panelCmd.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCmd.Controls.Add(this.lblClose);
            this.panelCmd.Controls.Add(this.lblFind);
            this.panelCmd.Controls.Add(this.lblPrint);
            this.panelCmd.Controls.Add(this.lblNew);
            this.panelCmd.Controls.Add(this.lblShowAll);
            this.panelCmd.Controls.Add(this.label1);
            this.panelCmd.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCmd.Location = new System.Drawing.Point(0, 102);
            this.panelCmd.Name = "panelCmd";
            this.panelCmd.Size = new System.Drawing.Size(880, 78);
            this.panelCmd.TabIndex = 1;
            // 
            // lblClose
            // 
            this.lblClose.EditValue = "Update Status Work Order";
            this.lblClose.Location = new System.Drawing.Point(448, 27);
            this.lblClose.Name = "lblClose";
            this.lblClose.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblClose.Properties.Appearance.Options.UseBackColor = true;
            this.lblClose.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblClose.Properties.LinkColor = System.Drawing.Color.Black;
            this.lblClose.Size = new System.Drawing.Size(163, 18);
            this.lblClose.TabIndex = 8;
            this.lblClose.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.lblClose_OpenLink);
            // 
            // lblFind
            // 
            this.lblFind.EditValue = "Find Work Order";
            this.lblFind.Location = new System.Drawing.Point(713, 3);
            this.lblFind.Name = "lblFind";
            this.lblFind.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblFind.Properties.Appearance.Options.UseBackColor = true;
            this.lblFind.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblFind.Properties.LinkColor = System.Drawing.Color.Black;
            this.lblFind.Size = new System.Drawing.Size(107, 18);
            this.lblFind.TabIndex = 5;
            this.lblFind.Visible = false;
            // 
            // lblPrint
            // 
            this.lblPrint.EditValue = "Print Work Order Listing";
            this.lblPrint.Location = new System.Drawing.Point(259, 27);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblPrint.Properties.Appearance.Options.UseBackColor = true;
            this.lblPrint.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblPrint.Properties.LinkColor = System.Drawing.Color.Black;
            this.lblPrint.Size = new System.Drawing.Size(136, 18);
            this.lblPrint.TabIndex = 6;
            this.lblPrint.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.lblPrint_OpenLink_1);
            // 
            // lblNew
            // 
            this.lblNew.EditValue = "Create New Work Order";
            this.lblNew.Location = new System.Drawing.Point(89, 27);
            this.lblNew.Name = "lblNew";
            this.lblNew.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblNew.Properties.Appearance.Options.UseBackColor = true;
            this.lblNew.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblNew.Properties.LinkColor = System.Drawing.Color.DarkGoldenrod;
            this.lblNew.Size = new System.Drawing.Size(153, 18);
            this.lblNew.TabIndex = 7;
            // 
            // lblShowAll
            // 
            this.lblShowAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblShowAll.ForeColor = System.Drawing.Color.Black;
            this.lblShowAll.Location = new System.Drawing.Point(12, 58);
            this.lblShowAll.Name = "lblShowAll";
            this.lblShowAll.Size = new System.Drawing.Size(511, 15);
            this.lblShowAll.TabIndex = 3;
            this.lblShowAll.Text = "or you can show a list of Stock Work Order, then find yout Stock Work Order and a" +
    "pply the action on it.";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(115)))), ((int)(((byte)(250)))));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "You can :";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.sbtnEdit);
            this.panelControl1.Controls.Add(this.sbtnDel);
            this.panelControl1.Controls.Add(this.sbtnView);
            this.panelControl1.Controls.Add(this.sbtnRefresh);
            this.panelControl1.Controls.Add(this.chkEdtShowAtStartup);
            this.panelControl1.Controls.Add(this.printButton1);
            this.panelControl1.Controls.Add(this.previewButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 180);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(880, 36);
            this.panelControl1.TabIndex = 0;
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Work Order";
            this.panelHeader1.HelpTopicId = "Stock_Work_Order.htm";
            this.panelHeader1.Hint = "In this Work Order Window, you can  create, modify, or delete Work Order";
            this.panelHeader1.Location = new System.Drawing.Point(0, 22);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(880, 80);
            this.panelHeader1.TabIndex = 2;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3,
            this.bar4});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5});
            this.barManager2.MainMenu = this.bar3;
            this.barManager2.MaxItemId = 6;
            this.barManager2.StatusBar = this.bar4;
            // 
            // bar3
            // 
            this.bar3.BarName = "Main menu";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1)});
            this.bar3.OptionsBar.MultiLine = true;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Main menu";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Report";
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5, true)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Design Document Style Report";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Design Listing Style Report";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Design Detail Listing Style Report";
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Report Option";
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick_1);
            // 
            // bar4
            // 
            this.bar4.BarName = "Status bar";
            this.bar4.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar4.OptionsBar.AllowQuickCustomization = false;
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.Text = "Status bar";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(880, 22);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 386);
            this.barDockControl2.Size = new System.Drawing.Size(880, 23);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 22);
            this.barDockControl3.Size = new System.Drawing.Size(0, 364);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(880, 22);
            this.barDockControl4.Size = new System.Drawing.Size(0, 364);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Design Detail Listing Style Report";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // FormStockWorkOrderCmd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(880, 409);
            this.Controls.Add(this.panelGrid);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelCmd);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.KeyPreview = true;
            this.Name = "FormStockWorkOrderCmd";
            this.Text = "Work Order";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormStockWorkOrderCmd_FormClosed);
            this.Load += new System.EventHandler(this.FormStockAssemblyCmd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).EndInit();
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStockMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkedtIsMultilevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEdtShowAtStartup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCmd)).EndInit();
            this.panelCmd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblClose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            this.ResumeLayout(false);

    }

    public class FormEventArgs
    {
      private FormStockWorkOrderCmd myForm;

      public StockWorkOrderCommand Command
      {
        get
        {
          return this.myForm.myStockWorkOrderCmd;
        }
      }

      public PanelControl PanelAction
      {
        get
        {
          return this.myForm.panelCmd;
        }
      }

      public PanelControl PanelGrid
      {
        get
        {
          return this.myForm.panelGrid;
        }
      }

      public GridControl GridControl
      {
        get
        {
          return this.myForm.gridControlAll;
        }
      }

      public FormStockWorkOrderCmd Form
      {
        get
        {
          return this.myForm;
        }
      }

      public DBSetting DBSetting
      {
        get
        {
          return this.myForm.myDBSetting;
        }
      }

      public FormEventArgs(FormStockWorkOrderCmd form)
      {
        this.myForm = form;
      }
    }

    public class FormInitializeEventArgs : FormStockWorkOrderCmd.FormEventArgs
    {
      public FormInitializeEventArgs(FormStockWorkOrderCmd form)
        : base(form)
      {
      }
    }

    public class FormClosedEventArgs : FormStockWorkOrderCmd.FormEventArgs
    {
      public FormClosedEventArgs(FormStockWorkOrderCmd form)
        : base(form)
      {
      }
    }

    public class FormLoadDataEventArgs : FormStockWorkOrderCmd.FormEventArgs
    {
      private DataTable myGridDataTable;

      public DataTable GridDataTable
      {
        get
        {
          return this.myGridDataTable;
        }
      }

      public FormLoadDataEventArgs(FormStockWorkOrderCmd form, DataTable gridDataTable)
        : base(form)
      {
        this.myGridDataTable = gridDataTable;
      }
    }

        private void lblPrint_OpenLink(object sender, OpenLinkEventArgs e)
        {

        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportTool.DesignReport("Stock Work Order Listing", this.myDBSetting);

        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            long selectedDocKey = 0;
            ReportTool.DesignReport("Stock Work Order Document", this.myStockWorkOrderCmd.GetReportDataSource(selectedDocKey), this.myDBSetting);

           // ReportTool.DesignReport("Stock Work Order Document", this.myDBSetting);

        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportTool.DesignReport("Stock Work Order Detail Listing", this.myDBSetting);
        }

        private void lblNew_OpenLink(object sender, OpenLinkEventArgs e)
        {

        }

        private void sbtnDel_Click(object sender, EventArgs e)
        {

        }

        private void lblPrint_OpenLink_1(object sender, OpenLinkEventArgs e)
        {

        }

        private void lblClose_OpenLink(object sender, OpenLinkEventArgs e)
        {
            if (UserAuthentication.GetOrCreate(myDBSetting).AccessRight.IsAccessible("RPA_WO_CLOSE", true))
            {
                FormCloseStockWorkOrder form = new FormCloseStockWorkOrder(myDBSetting);
                form.Show();
            }
        }

        private void sbtnEdit_Click(object sender, EventArgs e)
        {

        }

        private void barButtonItem5_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            FormBasicReportOption.ShowReportOption(this.myStockWorkOrderCmd.ReportOption);
            this.myStockWorkOrderCmd.SaveReportOption();
        }

        private void printButton1_Print(object sender, PrintEventArgs e)
        {

        }

        private void gridViewStockMain_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            //GridView view = sender as GridView;
            //DataRow dr = view.GetDataRow(e.ListSourceRowIndex);
            //if(e.Column.FieldName=="Status2" && e.IsGetData)
            //{
            //    object obj = myDBSetting.ExecuteScalar("select dbo.F_RPA_WOStatus (?)", (object)dr["DocKey"]);
            //    if (obj != null && obj != DBNull.Value)
            //        e.Value = obj.ToString();
            //    else
            //    {
            //        e.Value = WorkOrderStatusOptions.Planned;
            //    }

            //}
        }

        private void previewButton1_Preview(object sender, PrintEventArgs e)
        {

        }
    }
}
