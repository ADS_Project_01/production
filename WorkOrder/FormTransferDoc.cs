﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormTransferDoc
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.XtraUtils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Production.WorkOrder
{
  public class FormTransferDoc : XtraForm
  {
    private StockWorkOrder myStockAssemblyOrder;
    private DataTable myTable;
    private IContainer components;
    private PanelControl panelControl1;
    private SimpleButton sbtnCancel;
    private SimpleButton sbtnOK;
    private GridColumn colDate;
    private GridColumn colDebtorCode;
    private GridColumn colDeptNo;
    private GridColumn colDescription;
    private GridColumn colDiscount;
    private GridColumn colDocNo;
    private GridColumn colFOCQty;
    private GridColumn colItemCode;
    private GridColumn colLocation;
    private GridColumn colNewQty;
    private GridColumn colNumbering;
    private GridColumn colProjNo;
    private GridColumn colRemainingQty;
    private GridColumn colSmallestQty;
    private GridColumn colSubTotal;
    private GridColumn colTotalQty;
    private GridColumn colUnitPrice;
    private GridControl gridctrTransDoc;
    private GridView gridView1;
        private GridColumn gridColumn1;
        private GridColumn colDebtorName;
        private GridColumn colBOMCode;
        private GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private GridView gvTransDoc;

    public FormTransferDoc(StockWorkOrder aStockAssemblyOrder,object sdebtorcode)
    {
      this.InitializeComponent();
      this.myStockAssemblyOrder = aStockAssemblyOrder;
      this.myTable = this.myStockAssemblyOrder.Command.LoadPartialTransferItemFromSO(sdebtorcode);
      this.Icon = BCE.AutoCount.Application.Icon;
      this.AddColumn();
      this.InitFormControls();
            this.myTable.Columns.Add(new DataColumn("Check", System.Type.GetType("System.Boolean")));
            foreach (DataRow dataRow in (InternalDataCollectionBase)this.myTable.Rows)
            {                
                    dataRow["Check"] = (object)false;
            }
            foreach (DataRow dataRow in aStockAssemblyOrder.GetValidPIDetailRows())
            {
                DataRow[] drTransfer = this.myTable.Select("DocNo='" + dataRow["FromDocNo"] + "' and DtlKey=" + dataRow["FromDocDtlKey"],"",DataViewRowState.CurrentRows);
                foreach (DataRow drTrans in drTransfer)
                {
                    drTrans["Check"] = (object)true;
                    drTrans["NewQty"] = (object)dataRow["Qty"];
                }
            }
            this.gridctrTransDoc.DataSource = (object) this.myTable;
    }

    private void AddColumn()
    {
      this.myTable.Columns.Add("NewQty", typeof (Decimal));
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myStockAssemblyOrder.Command.DBSetting, false);
      string fieldname1 = "Qty";
      string fieldtype1 = "Quantity";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "SmallestQty";
      string fieldtype2 = "Quantity";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "RemainingQty";
      string fieldtype3 = "Quantity";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "TotalQty";
      string fieldtype4 = "Quantity";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "FOCQty";
      string fieldtype5 = "Quantity";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "NewQty";
      string fieldtype6 = "Quantity";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "DocDate";
      string fieldtype7 = "Date";
      formControlUtil.AddField(fieldname7, fieldtype7);
      string fieldname8 = "SubTotal";
      string fieldtype8 = "Currency";
      formControlUtil.AddField(fieldname8, fieldtype8);
      string fieldname9 = "Rate";
      string fieldtype9 = "Quantity";
      formControlUtil.AddField(fieldname9, fieldtype9);
      FormTransferDoc formTransferDoc = this;
      formControlUtil.InitControls((Control) formTransferDoc);
    }

    private void sbtnCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
    }

    private void sbtnOK_Click(object sender, EventArgs e)
    {
      if (this.gvTransDoc.FocusedRowHandle >= 0)
      {
                this.myStockAssemblyOrder.ClearPIDetails();
                DataRow[] dataRowArray = this.myTable.Select("Check = true");
                for (int i = 0; i < dataRowArray.Length; i++)
                {
                    DataRow dataRow = dataRowArray[i];
                    if (BCE.Data.Convert.ToDecimal(dataRow["NewQty"]) == 0)
                        continue;
                    Decimal qty = dataRow["NewQty"] != DBNull.Value ? BCE.Data.Convert.ToDecimal(dataRow["NewQty"]) : BCE.Data.Convert.ToDecimal(dataRow["RemainingQty"]);

                    if (BCE.Data.Convert.ToDecimal(dataRow["RemainingQty"]) < Decimal.Zero)
                    {
                        AppMessage.ShowErrorMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockAssemblyOrderStringId.ErrorMessage_CannotTransferCauseRemainingQty, new object[0]));
                        this.gridctrTransDoc.Focus();
                        this.gvTransDoc.FocusedColumn = this.colNewQty;
                        this.DialogResult = DialogResult.None;
                    }
                    else
                    {
                        this.myStockAssemblyOrder.TransferFromSO(dataRow);
                        this.DialogResult = DialogResult.OK;
                    }
                }
      }
      else
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_SelectRowAndSpecifyQty, new object[0]));
        this.gridctrTransDoc.Focus();
        this.DialogResult = DialogResult.None;
      }
                
    }

    private void gvTransDoc_DoubleClick(object sender, EventArgs e)
    {
      if (BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView) sender).InRow)
        this.sbtnOK.PerformClick();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridctrTransDoc = new DevExpress.XtraGrid.GridControl();
            this.gvTransDoc = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDebtorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDebtorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeptNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFOCQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNewQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumbering = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemainingQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSmallestQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOMCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridctrTransDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTransDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.sbtnOK);
            this.panelControl1.Controls.Add(this.sbtnCancel);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 422);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(765, 51);
            this.panelControl1.TabIndex = 1;
            // 
            // sbtnOK
            // 
            this.sbtnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnOK.Location = new System.Drawing.Point(597, 13);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(75, 23);
            this.sbtnOK.TabIndex = 0;
            this.sbtnOK.Text = "OK";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnCancel.Location = new System.Drawing.Point(678, 13);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.sbtnCancel.TabIndex = 1;
            this.sbtnCancel.Text = "Cancel";
            this.sbtnCancel.Click += new System.EventHandler(this.sbtnCancel_Click);
            // 
            // gridctrTransDoc
            // 
            this.gridctrTransDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridctrTransDoc.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridctrTransDoc.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridctrTransDoc.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridctrTransDoc.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridctrTransDoc.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridctrTransDoc.Location = new System.Drawing.Point(0, 0);
            this.gridctrTransDoc.MainView = this.gvTransDoc;
            this.gridctrTransDoc.Name = "gridctrTransDoc";
            this.gridctrTransDoc.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridctrTransDoc.Size = new System.Drawing.Size(765, 422);
            this.gridctrTransDoc.TabIndex = 0;
            this.gridctrTransDoc.UseEmbeddedNavigator = true;
            this.gridctrTransDoc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTransDoc,
            this.gridView1});
            // 
            // gvTransDoc
            // 
            this.gvTransDoc.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.colDate,
            this.colDebtorCode,
            this.colDebtorName,
            this.colDeptNo,
            this.colDescription,
            this.colDiscount,
            this.colDocNo,
            this.colFOCQty,
            this.colItemCode,
            this.colLocation,
            this.colNewQty,
            this.colNumbering,
            this.colProjNo,
            this.colRemainingQty,
            this.colSmallestQty,
            this.colSubTotal,
            this.colTotalQty,
            this.colUnitPrice,
            this.colBOMCode});
            this.gvTransDoc.GridControl = this.gridctrTransDoc;
            this.gvTransDoc.Name = "gvTransDoc";
            this.gvTransDoc.OptionsCustomization.AllowFilter = false;
            this.gvTransDoc.OptionsView.ShowGroupPanel = false;
            this.gvTransDoc.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gvTransDoc_SelectionChanged);
            this.gvTransDoc.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvTransDoc_CellValueChanged);
            this.gvTransDoc.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvTransDoc_CellValueChanging);
            this.gvTransDoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvTransDoc_KeyDown);
            this.gvTransDoc.DoubleClick += new System.EventHandler(this.gvTransDoc_DoubleClick);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Check";
            this.gridColumn2.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn2.FieldName = "Check";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 42;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colDate
            // 
            this.colDate.Caption = "DocDate";
            this.colDate.FieldName = "DocDate";
            this.colDate.Name = "colDate";
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 10;
            this.colDate.Width = 82;
            // 
            // colDebtorCode
            // 
            this.colDebtorCode.Caption = "Debtor Code";
            this.colDebtorCode.FieldName = "DebtorCode";
            this.colDebtorCode.Name = "colDebtorCode";
            this.colDebtorCode.Visible = true;
            this.colDebtorCode.VisibleIndex = 11;
            this.colDebtorCode.Width = 67;
            // 
            // colDebtorName
            // 
            this.colDebtorName.Caption = "DebtorName";
            this.colDebtorName.FieldName = "DebtorName";
            this.colDebtorName.Name = "colDebtorName";
            this.colDebtorName.Visible = true;
            this.colDebtorName.VisibleIndex = 12;
            this.colDebtorName.Width = 160;
            // 
            // colDeptNo
            // 
            this.colDeptNo.Caption = "Dept No";
            this.colDeptNo.FieldName = "DeptNo";
            this.colDeptNo.Name = "colDeptNo";
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 8;
            this.colDescription.Width = 198;
            // 
            // colDiscount
            // 
            this.colDiscount.Caption = "Discount";
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.Name = "colDiscount";
            // 
            // colDocNo
            // 
            this.colDocNo.Caption = "DocNo";
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 9;
            this.colDocNo.Width = 82;
            // 
            // colFOCQty
            // 
            this.colFOCQty.Caption = "FOC Qty";
            this.colFOCQty.FieldName = "FOCQty";
            this.colFOCQty.Name = "colFOCQty";
            this.colFOCQty.Visible = true;
            this.colFOCQty.VisibleIndex = 5;
            this.colFOCQty.Width = 43;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item Code";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 7;
            this.colItemCode.Width = 78;
            // 
            // colLocation
            // 
            this.colLocation.Caption = "Location";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            // 
            // colNewQty
            // 
            this.colNewQty.Caption = "New Qty";
            this.colNewQty.FieldName = "NewQty";
            this.colNewQty.Name = "colNewQty";
            this.colNewQty.Visible = true;
            this.colNewQty.VisibleIndex = 1;
            this.colNewQty.Width = 49;
            // 
            // colNumbering
            // 
            this.colNumbering.Caption = "Numbering";
            this.colNumbering.FieldName = "Numbering";
            this.colNumbering.Name = "colNumbering";
            // 
            // colProjNo
            // 
            this.colProjNo.Caption = "ProjNo";
            this.colProjNo.FieldName = "ProjNo";
            this.colProjNo.Name = "colProjNo";
            // 
            // colRemainingQty
            // 
            this.colRemainingQty.Caption = "Remaining Qty";
            this.colRemainingQty.FieldName = "RemainingQty";
            this.colRemainingQty.Name = "colRemainingQty";
            this.colRemainingQty.Visible = true;
            this.colRemainingQty.VisibleIndex = 2;
            this.colRemainingQty.Width = 66;
            // 
            // colSmallestQty
            // 
            this.colSmallestQty.Caption = "Smallest Qty";
            this.colSmallestQty.FieldName = "SmallestQty";
            this.colSmallestQty.Name = "colSmallestQty";
            this.colSmallestQty.Visible = true;
            this.colSmallestQty.VisibleIndex = 4;
            this.colSmallestQty.Width = 56;
            // 
            // colSubTotal
            // 
            this.colSubTotal.Caption = "Subtotal";
            this.colSubTotal.FieldName = "SubTotal";
            this.colSubTotal.Name = "colSubTotal";
            // 
            // colTotalQty
            // 
            this.colTotalQty.Caption = "TotalQty";
            this.colTotalQty.FieldName = "TotalQty";
            this.colTotalQty.Name = "colTotalQty";
            this.colTotalQty.Visible = true;
            this.colTotalQty.VisibleIndex = 3;
            this.colTotalQty.Width = 45;
            // 
            // colUnitPrice
            // 
            this.colUnitPrice.Caption = "UnitPrice";
            this.colUnitPrice.FieldName = "UnitPrice";
            this.colUnitPrice.Name = "colUnitPrice";
            // 
            // colBOMCode
            // 
            this.colBOMCode.Caption = "BOMCode";
            this.colBOMCode.FieldName = "BOMCode";
            this.colBOMCode.Name = "colBOMCode";
            this.colBOMCode.Visible = true;
            this.colBOMCode.VisibleIndex = 6;
            this.colBOMCode.Width = 65;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridctrTransDoc;
            this.gridView1.Name = "gridView1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // FormTransferDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(765, 473);
            this.Controls.Add(this.gridctrTransDoc);
            this.Controls.Add(this.panelControl1);
            this.Name = "FormTransferDoc";
            this.Text = "Transfer Document from Sales Order";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridctrTransDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTransDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

    }

        private void gvTransDoc_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            int[] selectedRows = this.gridView1.GetSelectedRows();
            if (selectedRows != null && selectedRows.Length > 1)
            {
                for (int index = 0; index < selectedRows.Length; ++index)
                    this.gridView1.GetDataRow(selectedRows[index])["Check"] = (object)true;
                //this.UpdateSelectedCount();
            }
        }

        private void gvTransDoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
            {
                e.Handled = true;
                DataRow dataRow = this.gridView1.GetDataRow(this.gridView1.FocusedRowHandle);
                if (dataRow != null)
                {
                    dataRow["Check"] = (object)(!BCE.Data.Convert.ToBoolean(dataRow["Check"]) ? 1 : 0);
                    dataRow.EndEdit();
                  //  this.UpdateSelectedCount();
                }
            }
        }

        private void gvTransDoc_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
        
        }

        private void gvTransDoc_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            DataRow dr = gvTransDoc.GetDataRow(e.RowHandle);
            if (e.Column.FieldName.ToString() == "Check")
            {
                if (BCE.Data.Convert.ToBoolean(e.Value))
                {
                    if (BCE.Data.Convert.ToDecimal(dr["NewQty"]) == 0)
                        dr["NewQty"] = dr["RemainingQty"];

                }
                else
                {
                    dr["NewQty"] =0;
                }
            }
        }
    }
}
