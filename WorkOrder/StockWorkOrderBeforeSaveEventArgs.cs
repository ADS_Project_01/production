﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderBeforeSaveEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Data;

namespace Production.WorkOrder
{
  public class StockWorkOrderBeforeSaveEventArgs : StockWorkOrderEventArgs
  {
    private string myErrorMessage = "";
    private DBSetting myDBSetting;
    internal bool myAbort;

    public new DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return this.myErrorMessage;
      }
      set
      {
        this.myErrorMessage = value;
      }
    }

    internal StockWorkOrderBeforeSaveEventArgs(StockWorkOrder doc, DBSetting dbSetting)
      : base(doc)
    {
      this.myDBSetting = dbSetting;
    }

    public void AbortSave()
    {
      this.myAbort = true;
    }
  }
}
