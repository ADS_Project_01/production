﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormInitializeEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using DevExpress.XtraTreeList;
using System;

namespace Production.WorkOrder
{
  [Obsolete("Use FormStockAssemblyOrderEntry.FormInitializeEventArgs instead")]
  public class FormInitializeEventArgs : FormEventArgs
  {
    public FormInitializeEventArgs(StockWorkOrder doc, PanelControl headerPanel, TreeList treeList, XtraTabControl tabControl)
      : base(doc, headerPanel, treeList, tabControl)
    {
    }
  }
}
