﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Data;

namespace Production.WorkOrder
{
  public class StockWorkOrderEventArgs
  {
    private StockWorkOrder myStock;

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.DBSetting;
      }
    }

    public StockWorkOrderRecord MasterRecord
    {
      get
      {
        return new StockWorkOrderRecord(this.myStock);
      }
    }

    internal StockWorkOrderEventArgs(StockWorkOrder doc)
    {
      this.myStock = doc;
    }
  }
}
