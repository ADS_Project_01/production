﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderOutstandingSQL
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Settings;
using BCE.Data;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Production.WorkOrder
{
  public class StockWorkOrderOutstandingSQL : StockWorkOrderOutstandingReport
  {
    public override void BasicSearch(StockWorkOrderOutstandingFormCriteria criteria, string columnNames, DataSet newDS, string chkEditColumnName)
    {
      this.oldDtlKeyList = "";
      if (criteria.OutstandingPrintingOption == StockWorkOrderOutstandingFormCriteria.PrintOption.Outstanding)
        this.OutstandingListingBasicSearch(criteria, columnNames, newDS, chkEditColumnName);
      else
        this.CompletedBasicSearch(criteria, columnNames, newDS, chkEditColumnName);
    }

    public override void OutstandingListingBasicSearch(StockWorkOrderOutstandingFormCriteria criteria, string columnNames, DataSet newDS, string chkEditColumnName)
    {
      DataTable dataTable1 = newDS.Tables["Master"];
      DataTable dataTable2 = newDS.Tables["Detail"];
      string str1 = columnNames;
      char[] chArray = new char[1];
      int index1 = 0;
      int num1 = 44;
      chArray[index1] = (char) num1;
      string[] strArray = str1.Split(chArray);
      columnNames = "";
      foreach (string str2 in strArray)
      {
        string oldValue = str2.Trim();
        columnNames = columnNames + oldValue.Replace(oldValue, "RPA_WO." + oldValue + ",");
      }
      if (columnNames.EndsWith(","))
        columnNames = columnNames.Remove(columnNames.Length - 1, 1);
      columnNames = columnNames.Trim();
      string format1 = "SELECT {0}, RPA_WO.NetTotal, RPA_WO.DocKey, (RPA_WO.Qty - ISNULL(RPA_WO.TransferedQty, 0)) as OutStandingFGQty FROM RPA_WO WHERE RPA_WO.DocKey IN (SELECT DISTINCT RPA_WO.DocKey FROM RPA_WO INNER JOIN RPA_WODTL ON RPA_WO.DocKey = RPA_WODTL.DocKey LEFT OUTER JOIN Item C ON (RPA_WO.ItemCode = C.ItemCode) LEFT OUTER JOIN Item D ON (RPA_WODTL.ItemCode = D.ItemCode) WHERE (RPA_WO.Cancelled = 'F') AND (COALESCE(RPA_WODTL.Qty,0) - COALESCE(RPA_WODTL.TransferedQty,0)) > 0 AND {1})";
      string format2 = "SELECT DISTINCT RPA_WO.DocKey FROM RPA_WO INNER JOIN RPA_WODTL ON RPA_WO.DocKey = RPA_WODTL.DocKey LEFT OUTER JOIN Item C ON (RPA_WO.ItemCode = C.ItemCode) LEFT OUTER JOIN Item D ON (RPA_WODTL.ItemCode = D.ItemCode) WHERE RPA_WO.Cancelled = 'F' AND (COALESCE(RPA_WODTL.Qty,0) - COALESCE(RPA_WODTL.TransferedQty,0)) > 0 AND {0}";
      string format3 = "SELECT *, (COALESCE(RPA_WODTL.Qty,0) - COALESCE(RPA_WODTL.TransferedQty,0)) AS RemainingQty FROM RPA_WODTL WHERE ((COALESCE(RPA_WODTL.Qty,0) - COALESCE(RPA_WODTL.TransferedQty,0)) > 0) AND DocKey IN ({0})";
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        SqlCommand sqlCommand1 = new SqlCommand();
        SqlCommand sqlCommand2 = new SqlCommand();
        sqlCommand1.Connection = sqlConnection;
        sqlCommand2.Connection = sqlConnection;
        string whereSql1 = this.GetWhereSQL(criteria, sqlCommand1);
        string str2;
        string str3;
        if (whereSql1 == string.Empty)
        {
          str2 = string.Format(format1, (object) columnNames, (object) "(1=1)");
          str3 = string.Format(format2, (object) "(1=1)");
        }
        else
        {
          str2 = string.Format(format1, (object) columnNames, (object) whereSql1);
          string whereSql2 = this.GetWhereSQL(criteria, sqlCommand2);
          str3 = string.Format(format2, (object) whereSql2);
        }
        string str4 = string.Format(format3, (object) str3);
        sqlCommand1.CommandText = str2;
        sqlCommand2.CommandText = str4;
        sqlCommand1.CommandTimeout = GeneralSetting.GetOrCreate(this.myDBSetting).SearchCommandTimeout;
        sqlCommand2.CommandTimeout = GeneralSetting.GetOrCreate(this.myDBSetting).SearchCommandTimeout;
        SqlDataAdapter sqlDataAdapter1 = new SqlDataAdapter(sqlCommand1);
        SqlDataAdapter sqlDataAdapter2 = new SqlDataAdapter(sqlCommand2);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
        {
          foreach (DataTable dataTable3 in (InternalDataCollectionBase) newDS.Tables)
            dataTable3.Clear();
        }
        else if (chkEditColumnName.Length > 0 && dataTable1.Rows.Count > 0)
        {
          DataRow[] dataRowArray = dataTable1.Select(string.Format("{0} = true", (object) chkEditColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index2 = 0; index2 < dataRowArray.Length; ++index2)
            numArray[index2] = BCE.Data.Convert.ToInt64(dataRowArray[index2]["DocKey"]);
        }
        sqlConnection.Open();
        sqlDataAdapter1.Fill(dataTable1);
        sqlDataAdapter2.Fill(dataTable2);
        if (dataTable1.Rows.Count > 0)
          this.GetPartialTransferredTo_Table(newDS);
        if (dataTable1.PrimaryKey.Length == 0)
        {
          DataTable dataTable3 = dataTable1;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index2 = 0;
          DataColumn dataColumn = dataTable1.Columns["DocKey"];
          dataColumnArray[index2] = dataColumn;
          dataTable3.PrimaryKey = dataColumnArray;
        }
        if (criteria.SortBy != -1)
        {
          if (criteria.SortBy == 0)
            dataTable1.DefaultView.Sort = "DocNo";
          else if (criteria.SortBy == 1)
            dataTable1.DefaultView.Sort = "DocDate";
        }
        if (numArray != null)
        {
          foreach (long num2 in numArray)
          {
            DataRow dataRow = dataTable1.Rows.Find((object) num2);
            if (dataRow != null)
              dataRow[chkEditColumnName] = (object) true;
          }
        }
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }

    public override void CompletedBasicSearch(StockWorkOrderOutstandingFormCriteria criteria, string columnNames, DataSet newDS, string chkEditColumnName)
    {
      DataTable dataTable1 = newDS.Tables["Master"];
      DataTable dataTable2 = newDS.Tables["Detail"];
      string str1 = columnNames;
      char[] chArray = new char[1];
      int index1 = 0;
      int num1 = 44;
      chArray[index1] = (char) num1;
      string[] strArray = str1.Split(chArray);
      columnNames = "";
      foreach (string str2 in strArray)
      {
        string oldValue = str2.Trim();
        columnNames = columnNames + oldValue.Replace(oldValue, "RPA_WO." + oldValue + ",");
      }
      if (columnNames.EndsWith(","))
        columnNames = columnNames.Remove(columnNames.Length - 1, 1);
      columnNames = columnNames.Trim();
      string format1 = "SELECT {0}, RPA_WO.NetTotal, RPA_WO.DocKey, (RPA_WO.Qty - ISNULL(RPA_WO.TransferedQty, 0)) as OutStandingFGQty FROM RPA_WO WHERE RPA_WO.DocKey IN (SELECT DISTINCT RPA_WO.DocKey FROM RPA_WO INNER JOIN RPA_WODTL ON RPA_WO.DocKey = RPA_WODTL.DocKey LEFT OUTER JOIN Item C ON (RPA_WO.ItemCode = C.ItemCode) LEFT OUTER JOIN Item D ON (RPA_WODTL.ItemCode = D.ItemCode) WHERE (RPA_WO.Cancelled = 'F') AND RPA_WO.DocKey NOT IN ({1}) AND {2})";
      string str3 = "SELECT DISTINCT DocKey From RPA_WODTL WHERE (Qty - TransferedQty) > 0 OR TransferedQty IS NULL";
      string format2 = "SELECT *, (COALESCE(RPA_WODTL.Qty,0) - COALESCE(RPA_WODTL.TransferedQty,0)) AS RemainingQty FROM RPA_WODTL WHERE DocKey IN ({0})";
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        SqlCommand selectCommand = new SqlCommand();
        sqlCommand.Connection = sqlConnection;
        selectCommand.Connection = sqlConnection;
        string whereSql = this.GetWhereSQL(criteria, sqlCommand);
        string str2 = !(whereSql == string.Empty) ? string.Format(format1, (object) columnNames, (object) str3, (object) whereSql) : string.Format(format1, (object) columnNames, (object) str3, (object) "(1=1)");
        sqlCommand.CommandText = str2;
        sqlCommand.CommandTimeout = GeneralSetting.GetOrCreate(this.myDBSetting).SearchCommandTimeout;
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
        {
          foreach (DataTable dataTable3 in (InternalDataCollectionBase) newDS.Tables)
            dataTable3.Clear();
        }
        else if (chkEditColumnName.Length > 0 && dataTable1.Rows.Count > 0)
        {
          DataRow[] dataRowArray = dataTable1.Select(string.Format("{0} = true", (object) chkEditColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index2 = 0; index2 < dataRowArray.Length; ++index2)
            numArray[index2] = BCE.Data.Convert.ToInt64(dataRowArray[index2]["DocKey"]);
        }
        sqlConnection.Open();
        sqlDataAdapter.Fill(dataTable1);
        string str4 = "";
        foreach (DataRow dataRow in (InternalDataCollectionBase) dataTable1.Rows)
          str4 = str4 + dataRow["DocKey"].ToString() + ",";
        if (str4.EndsWith(","))
          str4 = str4.Remove(str4.Length - 1, 1);
        string str5 = str4.Trim();
        if (str5.Length > 0)
        {
          string str6 = string.Format(format2, (object) str5);
          selectCommand.CommandText = str6;
          selectCommand.CommandTimeout = GeneralSetting.GetOrCreate(this.myDBSetting).SearchCommandTimeout;
          new SqlDataAdapter(selectCommand).Fill(dataTable2);
          this.GetPartialTransferredTo_Table(newDS);
        }
        if (dataTable1.PrimaryKey.Length == 0)
        {
          DataTable dataTable3 = dataTable1;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index2 = 0;
          DataColumn dataColumn = dataTable1.Columns["DocKey"];
          dataColumnArray[index2] = dataColumn;
          dataTable3.PrimaryKey = dataColumnArray;
        }
        if (criteria.SortBy != -1)
        {
          if (criteria.SortBy == 0)
            dataTable1.DefaultView.Sort = "DocNo";
          else if (criteria.SortBy == 1)
            dataTable1.DefaultView.Sort = "DocDate";
        }
        if (numArray != null)
        {
          foreach (long num2 in numArray)
          {
            DataRow dataRow = dataTable1.Rows.Find((object) num2);
            if (dataRow != null)
              dataRow[chkEditColumnName] = (object) true;
          }
        }
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }

    private void GetPartialTransferredTo_Table(DataSet newDS)
    {
      DataTable dataTable1 = newDS.Tables["Detail"];
      DataTable dataTable2 = newDS.Tables["Master"];
      SqlConnection sqlConnection1 = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        string str1 = "";
        foreach (DataRow dataRow in (InternalDataCollectionBase) dataTable1.Rows)
          str1 = str1 + dataRow["DtlKey"].ToString() + ",";
        string str2 = str1.Trim();
        if (str2.EndsWith(","))
          str2 = str2.Remove(str2.Length - 1, 1);
        DataTable dataTable3 = new DataTable("PartialTransferToTable");
        if (str2.Length > 0)
        {
          string str3 = "SELECT 'AS' AS DocType, B.FromRPA_WODtlKey AS FromDocDtlKey, A.DocNo, A.DocDate, B.Rate, B.Qty FROM ASM A, ASMDTL B WHERE A.DocKey = B.DocKey AND B.FromRPA_WODtlKey IS NOT NULL AND B.FromRPA_WODtlKey IN ( " + str2 + ")";
          string str4 = "SELECT 'PO' AS DocType, B.FromAODtlKey AS FromDocDtlKey, A.DocNo, A.DocDate, B.Rate, B.Qty FROM PO A, PODTL B WHERE A.DocKey = B.DocKey AND B.FromAODtlKey IS NOT NULL AND B.FromAODtlKey IN ( " + str2 + ")";
          SqlCommand selectCommand1 = new SqlCommand();
          SqlCommand selectCommand2 = new SqlCommand();
          SqlConnection sqlConnection2 = sqlConnection1;
          selectCommand1.Connection = sqlConnection2;
          selectCommand2.Connection = sqlConnection1;
          string str5 = str3;
          selectCommand1.CommandText = str5;
          selectCommand2.CommandText = str4;
          SqlDataAdapter sqlDataAdapter1 = new SqlDataAdapter(selectCommand1);
          SqlDataAdapter sqlDataAdapter2 = new SqlDataAdapter(selectCommand2);
          sqlDataAdapter1.Fill(dataTable3);
          DataTable dataTable4 = dataTable3;
          sqlDataAdapter2.Fill(dataTable4);
          if (!dataTable3.Columns.Contains("Qty2"))
          {
            DataColumn column = new DataColumn("Qty2", typeof (Decimal));
            dataTable3.Columns.Add(column);
          }
          if (dataTable3.Rows.Count > 0)
          {
            foreach (DataRow dataRow in (InternalDataCollectionBase) dataTable3.Rows)
            {
              Decimal num1 = BCE.Data.Convert.ToDecimal(dataRow["FromDocDtlKey"]);
              Decimal num2 = BCE.Data.Convert.ToDecimal(dataTable1.Select("DtlKey = " + num1.ToString(), "", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent)[0]["Rate"]);
              Decimal num3 = BCE.Data.Convert.ToDecimal(dataRow["Rate"]);
              Decimal num4 = BCE.Data.Convert.ToDecimal(dataRow["Qty"]);
              if (num2 != Decimal.Zero)
                dataRow["Qty2"] = (object) DecimalSetting.GetOrCreate(this.myDBSetting).RoundQuantity(num4 * num3 / num2);
            }
          }
          if (!newDS.Tables.Contains("PartialTransferToTable"))
          {
            newDS.Tables.Add(dataTable3);
            DataRelation relation = new DataRelation("Detail_PartialTransferToTable", newDS.Tables["Detail"].Columns["DtlKey"], dataTable3.Columns["FromDocDtlKey"], false);
            newDS.Relations.Add(relation);
          }
          else
          {
            newDS.Tables["PartialTransferToTable"].Clear();
            foreach (DataRow row in (InternalDataCollectionBase) dataTable3.Rows)
              newDS.Tables["PartialTransferToTable"].ImportRow(row);
          }
        }
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        sqlConnection1.Close();
        sqlConnection1.Dispose();
      }
    }

    protected override DataSet LoadOutstandingListingReportData(string docKeys, StockWorkOrderOutstandingFormCriteria criteria)
    {
      string format = "SELECT A.* {0} {1} From vRPA_WorkOrder A WHERE A.DocKey IN (SELECT * FROM LIST(@DocKeyList))";
      string str1 = "SELECT *, (COALESCE(vRPA_WorkOrderDetail.Qty,0) - COALESCE(vRPA_WorkOrderDetail.TransferedQty,0)) AS RemainingQty FROM vRPA_WorkOrderDetail WHERE ((vRPA_WorkOrderDetail.Qty - vRPA_WorkOrderDetail.TransferedQty) > 0 OR vRPA_WorkOrderDetail.TransferedQty IS NULL) AND DocKey IN (SELECT * FROM LIST(@DocKeyList)) AND UOM = ItemBaseUOM ORDER BY Seq";
      string str2 = "";
      string str3 = "";
      if (criteria.SortBy == 0)
        str2 = ",A.DocNo AS SortID";
      else if (criteria.SortBy == 1)
        str2 = ",convert(nvarchar(10),A.DocDate,112) AS SortID ";
      if (criteria.GroupBy == 0)
        str3 = ",'' AS GroupID, '' AS GroupIDName, '' AS GroupIDDisplay, '' AS GroupIDDescription ";
      else if (criteria.GroupBy == 1)
        str3 = ",A.ItemCode AS GroupID, 'Item Code' AS GroupIDName, A.ItemCode AS GroupIDDisplay, A.ItemDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == 2)
        str3 = ",A.ItemGroup AS GroupID, 'Item Group' AS GroupIDName, A.ItemGroup AS GroupIDDisplay, A.ItemGroupDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == 3)
        str3 = ",A.ItemType AS GroupID, 'Item Type' AS GroupIDName, A.ItemType AS GroupIDDisplay, A.ItemTypeDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == 4)
        str3 = ",A.Location AS GroupID, 'Location' AS GroupIDName, A.Location AS GroupIDDisplay, A.LocationDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == 5)
        str3 = ",A.ProjNo AS GroupID, 'Project No' AS GroupIDName, A.ProjNo AS GroupIDDisplay, A.ProjectDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == 6)
        str3 = ",A.DeptNo AS GroupID, 'Department No' AS GroupIDName, A.DeptNo AS GroupIDDisplay, A.DeptDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == 7)
        str3 = ",replace(convert(nvarchar(10),A.DocDate,23),'-','/') AS GroupID, 'Date' AS GroupIDName, replace(convert(nvarchar(10),A.DocDate,103) , '/', '') AS GroupIDDisplay, 'Group By ' + convert(nvarchar(10),A.DocDate,103) As GroupIDDescription ";
      else if (criteria.GroupBy == 8)
        str3 = ",convert(nvarchar(6),A.DocDate,112) AS GroupID, 'Month' AS GroupIDName, DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4)) AS GroupIDDisplay, 'Group By ' + DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4)) AS GroupIDDescription ";
      else if (criteria.GroupBy == 9)
        str3 = ",convert(nvarchar(4),A.DocDate,112) AS GroupID, 'Year' AS GroupIDName, convert(nvarchar(4),A.DocDate,112) AS GroupIDDisplay, 'Group By ' + convert(nvarchar(4),A.DocDate,112) AS GroupIDDescription ";
      string str4 = string.Format(format, (object) str2, (object) str3);
      DataSet dataSet = new DataSet();
      DBSetting dbSetting1 = this.myDBSetting;
      DataSet ds1 = dataSet;
      string tableName1 = "Master";
      string cmdText1 = str4;
      int num1 = 0;
      object[] objArray1 = new object[1];
      int index1 = 0;
      SqlParameter sqlParameter1 = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray1[index1] = (object) sqlParameter1;
      dbSetting1.LoadDataSet(ds1, tableName1, cmdText1, num1 != 0, objArray1);
      DBSetting dbSetting2 = this.myDBSetting;
      DataSet ds2 = dataSet;
      string tableName2 = "Detail";
      string cmdText2 = str1;
      int num2 = 0;
      object[] objArray2 = new object[1];
      int index2 = 0;
      SqlParameter sqlParameter2 = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray2[index2] = (object) sqlParameter2;
      dbSetting2.LoadDataSet(ds2, tableName2, cmdText2, num2 != 0, objArray2);
      return dataSet;
    }

    protected override DataSet LoadOutstandingListingReportDesignerData()
    {
      string format = "SELECT TOP 8 A.* {0} {1} FROM vRPA_WorkOrder A ";
      string cmdText1 = "SELECT *, (COALESCE(vRPA_WorkOrderDetail.Qty,0) - COALESCE(vRPA_WorkOrderDetail.TransferedQty,0)) AS RemainingQty FROM vRPA_WorkOrderDetail WHERE DocKey IN (SELECT TOP 8 DocKey FROM vRPA_WorkOrder) ORDER BY DocKey, Seq";
      string str1 = ",convert(nvarchar(10),A.DocDate,112) AS SortID ";
      string str2 = ",A.ItemCode AS GroupID, 'Item Code' AS GroupIDName, A.ItemCode AS GroupIDDisplay, A.ItemDescription AS GroupIDDescription ";
      string cmdText2 = string.Format(format, (object) str1, (object) str2);
      DataSet ds = new DataSet();
      this.myDBSetting.LoadDataSet(ds, "Master", cmdText2, false, new object[0]);
      this.myDBSetting.LoadDataSet(ds, "Detail", cmdText1, false, new object[0]);
      return ds;
    }
  }
}
