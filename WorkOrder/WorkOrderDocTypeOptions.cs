﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BCE.Localization;
namespace Production.WorkOrder
{
    [LocalizableString]
    public enum WorkOrderDocTypeOptions
    {
        [DefaultString("Internal")]
        Internal,
        [DefaultString("External")]
        External,
    }
}
