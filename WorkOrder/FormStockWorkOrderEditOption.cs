﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormStockAssemblyOrderEditOption
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

namespace Production.WorkOrder
{
  public class FormStockWorkOrderEditOption
  {
    public bool ShowProceedNewStockAssemblyOrder { get; set; }
  }
}
