﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.FormMultiSelectItemBOM
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using BCE.Localization;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using BCE.AutoCount.XtraUtils;
namespace Production.WorkOrder
{
  public class FormMultiSelectWORM: XtraForm
  {
    private DBSetting myDBSetting;
    private BCE.AutoCount.SearchFilter.Filter myFilter;
    private WORMDetailLookupEditBuilder myLookupEditBuilder;
    private DataTable myDataTable;
        private DataTable mySummaryTable;
        private DataTable myBatchNoTable;
        private IContainer components;
    private ComboBoxEdit cbEdtFilter;
    private RepositoryItemCheckEdit repositoryItemCheckEdit_Check;
    private SimpleButton sbtnCancel;
    private SimpleButton sbtnOK;
    private SimpleButton sbtnSelectAll;
    private SimpleButton sbtnUnselectAll;
    private GridColumn colBOMCode;
    private GridColumn colDescription;
    private GridColumn gridColCheck;
    private GridControl gridControl1;
    private GridView gridView1;
    private Label label1;
    private Label lblSelectedCount;
        private GridColumn colCheck;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        //private GridColumn colBOMCode;
        private GridColumn colItemCode;
        private GridColumn colDescription2;
        private GridColumn colDocDate;
        private GridColumn colDocNo;
        private DevExpress.XtraTreeList.TreeList gridCtlItem;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCheckList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDocKey;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDtlKey;
        private DevExpress.XtraTreeList.Columns.TreeListColumn gridColumn2;
        private RepositoryItemLookUpEdit repItemLuedtItem;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDesc;
        private DevExpress.XtraTreeList.Columns.TreeListColumn gridColumn3;
        private RepositoryItemLookUpEdit repositoryItemluUOM;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colRate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colQty;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colUnitCost;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTotalCost;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colItemSeq;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDtlFromDocDtlKey;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colFromBOMCode;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colParentBOMCode;
        private RepositoryItemLookUpEdit repitemAPDebtorCode;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private Panel panel2;
        private Label label2;
        private SimpleButton btnInquiry;
        private LookUpEdit luWONo;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private GroupControl groupControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTreeList.TreeList gridCtlSummary;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn4;
        private RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn5;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn8;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn9;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn10;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn11;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn12;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn13;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn14;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn15;
        private RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private Label label3;
        private ComboBoxEdit cbTab;
        private GroupControl groupControl2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colBatchNo;
        private GridControl gridCtlBatchNo;
        private GridView gvBatchNo;
        private GridColumn gridColumn8;
        private RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private GridColumn gridColumn1;
        private GridColumn gridColumn4;
        private GridColumn gridColumn5;
        private GridColumn gridColumn6;
        private GridColumn gridColumn7;
        private RepositoryItemLookUpEdit repleOvdOverheadCode;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn6;
        private GridColumn gridColumn9;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn7;
        private Panel panel1;
        private GridColumn gridColumn10;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn16;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn17;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn19;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn18;
        private Int32 imyDtlKey;
        public DataTable WOFilterTable
        {
            get
            {
                return this.myDataTable;
            }
        }
        public DataTable BatcNoFilterTable
        {
            get
            {
                return this.myBatchNoTable;
            }
        }


        private void InitFormControls()
        {
            FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
            string fieldname1 = "DocDate";
            string fieldtype1 = "Date";
            formControlUtil.AddField(fieldname1, fieldtype1);
            //string fieldname2 = "Total";
            //string fieldtype2 = "Currency";
            //formControlUtil.AddField(fieldname2, fieldtype2);
            string fieldname3 = "UnitCost";
            string fieldtype3 = "Cost";
            formControlUtil.AddField(fieldname3, fieldtype3);
            string fieldname4 = "Qty";
            string fieldtype4 = "Quantity";
            formControlUtil.AddField(fieldname4, fieldtype4);
            string fieldname5 = "NewQty";
            string fieldtype5 = "Quantity";
            formControlUtil.AddField(fieldname5, fieldtype5);

            FormMultiSelectWORM formEntry = this;
            formControlUtil.InitControls((Control)formEntry);
       
        }

        public FormMultiSelectWORM(DBSetting dbSetting, BCE.AutoCount.SearchFilter.Filter filter,DataTable wofiltertable, DataTable batchnotable)
        {
            this.InitializeComponent();
            this.Icon = BCE.AutoCount.Application.Icon;
            this.myFilter = filter;
            this.myDBSetting = dbSetting;
            WORMLookupEditBuilder filterlookup = new WORMLookupEditBuilder();
            filterlookup.BuildLookupEdit(luWONo.Properties, myDBSetting);
            xtraTabPage1.PageVisible = false;
            xtraTabPage2.PageVisible = false;
            mySummaryTable = new DataTable();
            myDataTable = wofiltertable;
            myDataTable = new DataTable();
            myBatchNoTable = new DataTable();
            myBatchNoTable = batchnotable;
            //DataTable dtWONoFilter = new DataTable();
            // dtWONoFilter= filterlookup.BuildDataTable(myDBSetting);// dtWONoFilter);
           // mySummaryTable.Columns.Add("ID", typeof(string));
            mySummaryTable.Columns.Add("DtlKey", typeof(Int32));
            mySummaryTable.Columns.Add("ItemCode", typeof(string));
            mySummaryTable.Columns.Add("Description", typeof(string));
            mySummaryTable.Columns.Add("UOM", typeof(string));
            mySummaryTable.Columns.Add("Qty", typeof(decimal));
            mySummaryTable.Columns.Add("ItemType", typeof(string));
            mySummaryTable.Columns.Add("UnitCost", typeof(decimal));
            mySummaryTable.Columns.Add("TotalCost", typeof(decimal));
            this.mySummaryTable.Columns.Add(new DataColumn("Check", System.Type.GetType("System.Boolean")));
            mySummaryTable.Constraints.Add("DtlKey", mySummaryTable.Columns["DtlKey"], true);
            btnInquiry.Enabled = false;
            imyDtlKey = -1;
        }

        private void FormParameterCustom_Load(object sender, EventArgs e)
    {
            new CustomizeGridLayout(this.myDBSetting, this.Name+"Batch", this.gvBatchNo);
            new CustomizeGridLayout(this.myDBSetting, this.Name, this.gridView1);
            new CustomizeTreeListLayout(this.myDBSetting, this.Name+"Sum", this.gridCtlSummary);
            new CustomizeTreeListLayout(this.myDBSetting, this.Name+"Detail", this.gridCtlItem);

        }

        private void gridView1_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
    {
      int[] selectedRows = this.gridView1.GetSelectedRows();
      if (selectedRows != null && selectedRows.Length > 1)
      {
        for (int index = 0; index < selectedRows.Length; ++index)
          this.gridView1.GetDataRow(selectedRows[index])["Check"] = (object) true;
        this.UpdateSelectedCount();
      }
    }

    private void sbtnOK_Click(object sender, EventArgs e)
    {

    BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
            DataRow[] dataRowSummaryArray = this.mySummaryTable.Select("Check = true");
            DataRow[] dataRowArray = this.myDataTable.Select("Check = true");

            DataRow drGrid = gridView1.GetDataRow(0);
            if (dataRowArray.Length == 0 && dataRowSummaryArray.Length==0)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)Production.GeneralMaintenance.ItemBOM.ItemBOMMaintStringId.ErrorMessage_RecordNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            else
            {
                this.myFilter.Clear();
                if (dataRowArray.Length > 0)
                {
                    foreach (DataRow dataRow in dataRowArray)
                        this.myFilter.Add((object)dataRow["DtlKey"].ToString());
                }
               
                if (dataRowSummaryArray.Length > 0)
                {
                    foreach (DataRow dataRow in dataRowSummaryArray)
                    {
                        dataRowArray = this.myDataTable.Select("ItemCode='"+ dataRow["ItemCode"] + "' and UOM='"+ dataRow["UOM"] + "' and ItemType='" + dataRow["ItemType"] + "'");
                        
                        foreach (DataRow dataRowDetail in dataRowArray)
                        {
                            this.myFilter.Add((object)dataRowDetail["DtlKey"].ToString());
                        }

                    }
                }

            }
    }

        private void sbtnUnselectAll_Click(object sender, EventArgs e)
        {
            this.gridView1.BeginUpdate();
            
            if (cbTab.SelectedIndex == 0)
            {

                foreach (DataRow dataRow in this.mySummaryTable.Select(this.mySummaryTable.DefaultView.RowFilter))
                    dataRow["Check"] = (object)false;
            }
            else
            {
                foreach (DataRow dataRow in this.myDataTable.Select(this.myDataTable.DefaultView.RowFilter))
                    dataRow["Check"] = (object)false;
            }
            this.gridView1.EndUpdate();
            this.UpdateSelectedCount();
        }

    private void sbtnSelectAll_Click(object sender, EventArgs e)
    {
           // if(cbTab.)
      this.gridView1.BeginUpdate();
            if (cbTab.SelectedIndex == 0)
            {
                foreach (DataRow dataRow in this.mySummaryTable.Select(this.mySummaryTable.DefaultView.RowFilter))
                    dataRow["Check"] = (object)true;
            }
            else
            {

                foreach (DataRow dataRow in this.myDataTable.Select(this.myDataTable.DefaultView.RowFilter))
                    dataRow["Check"] = (object)true;
            }
      this.gridView1.EndUpdate();
      this.UpdateSelectedCount();
    }

    private void gridView1_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
      {
        e.Handled = true;
        DataRow dataRow = this.gridView1.GetDataRow(this.gridView1.FocusedRowHandle);


        if (dataRow != null)
        {
          dataRow["Check"] = (object) (!BCE.Data.Convert.ToBoolean(dataRow["Check"]) ? 1 : 0);
          dataRow.EndEdit();
          this.UpdateSelectedCount();
        }
      }
    }

    private void cbEdtFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
      if (this.cbEdtFilter.SelectedIndex == 1)
        this.myDataTable.DefaultView.RowFilter = "Check = True";
      else if (this.cbEdtFilter.SelectedIndex == 2)
        this.myDataTable.DefaultView.RowFilter = "Check IS NULL OR Check = False";
      else
        this.myDataTable.DefaultView.RowFilter = "";
    }

    private void UpdateSelectedCount()
    {
     // GridViewUtils.UpdateData(this.gridView1);
      int num = 0;
            if (cbTab.SelectedIndex == 0)
            {
                DataRow[] dataRowArray = this.mySummaryTable.Select("Check = true");
                for (int rowHandle = 0; rowHandle < dataRowArray.Length; ++rowHandle)
                {
                    ++num;
                }
            }
            else
            {
                DataRow[] dataRowArray = this.myDataTable.Select("Check = true");
                for (int rowHandle = 0; rowHandle < dataRowArray.Length; ++rowHandle)
                {
                    ++num;
                }
            }
            Label label = this.lblSelectedCount;
      Production.GeneralMaintenance.ItemBOM.ItemBOMMaintStringId local = Production.GeneralMaintenance.ItemBOM.ItemBOMMaintStringId._Selected;
      object[] objArray = new object[1];
      int index = 0;
      string str = num.ToString();
      objArray[index] = (object) str;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      label.Text = @string;
    }

    private void repositoryItemCheckEdit_Check_CheckedChanged(object sender, EventArgs e)
    {
      this.UpdateSelectedCount();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit_Check = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOMCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSelectedCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbEdtFilter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.sbtnUnselectAll = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSelectAll = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.colCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridCtlItem = new DevExpress.XtraTreeList.TreeList();
            this.colCheckList = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colQty = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDocKey = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDtlKey = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDesc = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colRate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colUnitCost = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTotalCost = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colItemSeq = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDtlFromDocDtlKey = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colFromBOMCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colParentBOMCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn7 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn16 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn19 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemluUOM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repItemLuedtItem = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repitemAPDebtorCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridCtlBatchNo = new DevExpress.XtraGrid.GridControl();
            this.gvBatchNo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleOvdOverheadCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.label3 = new System.Windows.Forms.Label();
            this.btnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.cbTab = new DevExpress.XtraEditors.ComboBoxEdit();
            this.luWONo = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlSummary = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn4 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.treeListColumn5 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn8 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn9 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn10 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn11 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn12 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn13 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn14 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn15 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colBatchNo = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn6 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn17 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn18 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit_Check)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEdtFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemluUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLuedtItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repitemAPDebtorCode)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlBatchNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBatchNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleOvdOverheadCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTab.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luWONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(399, 299);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit_Check});
            this.gridControl1.Size = new System.Drawing.Size(277, 10);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.Visible = false;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColCheck,
            this.colDocDate,
            this.colDocNo,
            this.colBOMCode,
            this.colItemCode,
            this.colDescription2});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged_1);
            this.gridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView1_KeyDown);
            // 
            // gridColCheck
            // 
            this.gridColCheck.ColumnEdit = this.repositoryItemCheckEdit_Check;
            this.gridColCheck.FieldName = "Check";
            this.gridColCheck.Name = "gridColCheck";
            this.gridColCheck.Visible = true;
            this.gridColCheck.VisibleIndex = 0;
            this.gridColCheck.Width = 84;
            // 
            // repositoryItemCheckEdit_Check
            // 
            this.repositoryItemCheckEdit_Check.Caption = "Check";
            this.repositoryItemCheckEdit_Check.Name = "repositoryItemCheckEdit_Check";
            this.repositoryItemCheckEdit_Check.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit_Check.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEdit_Check_CheckedChanged);
            // 
            // colDocDate
            // 
            this.colDocDate.Caption = "DocDate";
            this.colDocDate.FieldName = "DocDate";
            this.colDocDate.Name = "colDocDate";
            this.colDocDate.Visible = true;
            this.colDocDate.VisibleIndex = 2;
            this.colDocDate.Width = 141;
            // 
            // colDocNo
            // 
            this.colDocNo.Caption = "DocNo";
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 1;
            this.colDocNo.Width = 177;
            // 
            // colBOMCode
            // 
            this.colBOMCode.Caption = "BOMCode";
            this.colBOMCode.FieldName = "BOMCode";
            this.colBOMCode.Name = "colBOMCode";
            this.colBOMCode.OptionsColumn.AllowEdit = false;
            this.colBOMCode.Visible = true;
            this.colBOMCode.VisibleIndex = 3;
            this.colBOMCode.Width = 112;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item Code";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 4;
            this.colItemCode.Width = 123;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Description";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 5;
            this.colDescription2.Width = 404;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 'T';
            this.repositoryItemCheckEdit1.ValueUnchecked = 'F';
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblSelectedCount);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbEdtFilter);
            this.panel1.Controls.Add(this.sbtnUnselectAll);
            this.panel1.Controls.Add(this.sbtnSelectAll);
            this.panel1.Controls.Add(this.sbtnCancel);
            this.panel1.Controls.Add(this.sbtnOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 426);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(871, 45);
            this.panel1.TabIndex = 1;
            // 
            // lblSelectedCount
            // 
            this.lblSelectedCount.ForeColor = System.Drawing.Color.Navy;
            this.lblSelectedCount.Location = new System.Drawing.Point(398, 14);
            this.lblSelectedCount.Name = "lblSelectedCount";
            this.lblSelectedCount.Size = new System.Drawing.Size(106, 23);
            this.lblSelectedCount.TabIndex = 0;
            this.lblSelectedCount.Text = "Count";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(190, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Filter :";
            // 
            // cbEdtFilter
            // 
            this.cbEdtFilter.EditValue = "Show All Items";
            this.cbEdtFilter.Location = new System.Drawing.Point(241, 11);
            this.cbEdtFilter.Name = "cbEdtFilter";
            this.cbEdtFilter.Properties.Items.AddRange(new object[] {
            "Show All Items",
            "Show Selected Items Only",
            "Show Non-selected Items Only"});
            this.cbEdtFilter.Size = new System.Drawing.Size(149, 20);
            this.cbEdtFilter.TabIndex = 2;
            this.cbEdtFilter.SelectedIndexChanged += new System.EventHandler(this.cbEdtFilter_SelectedIndexChanged);
            // 
            // sbtnUnselectAll
            // 
            this.sbtnUnselectAll.Location = new System.Drawing.Point(93, 10);
            this.sbtnUnselectAll.Name = "sbtnUnselectAll";
            this.sbtnUnselectAll.Size = new System.Drawing.Size(75, 23);
            this.sbtnUnselectAll.TabIndex = 3;
            this.sbtnUnselectAll.Text = "Unselect All";
            this.sbtnUnselectAll.Click += new System.EventHandler(this.sbtnUnselectAll_Click);
            // 
            // sbtnSelectAll
            // 
            this.sbtnSelectAll.Location = new System.Drawing.Point(12, 10);
            this.sbtnSelectAll.Name = "sbtnSelectAll";
            this.sbtnSelectAll.Size = new System.Drawing.Size(75, 23);
            this.sbtnSelectAll.TabIndex = 4;
            this.sbtnSelectAll.Text = "Select All";
            this.sbtnSelectAll.Click += new System.EventHandler(this.sbtnSelectAll_Click);
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnCancel.Location = new System.Drawing.Point(774, 10);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.sbtnCancel.TabIndex = 5;
            this.sbtnCancel.Text = "Cancel";
            // 
            // sbtnOK
            // 
            this.sbtnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnOK.Location = new System.Drawing.Point(693, 10);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(75, 23);
            this.sbtnOK.TabIndex = 6;
            this.sbtnOK.Text = "OK";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // colCheck
            // 
            this.colCheck.Caption = "Check";
            this.colCheck.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCheck.FieldName = "Check";
            this.colCheck.Name = "colCheck";
            this.colCheck.Visible = true;
            this.colCheck.VisibleIndex = 0;
            this.colCheck.Width = 78;
            // 
            // gridCtlItem
            // 
            this.gridCtlItem.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colCheckList,
            this.colQty,
            this.treeListColumn1,
            this.colDocKey,
            this.colDtlKey,
            this.gridColumn2,
            this.colDesc,
            this.gridColumn3,
            this.colRate,
            this.colUnitCost,
            this.colTotalCost,
            this.colItemSeq,
            this.colDtlFromDocDtlKey,
            this.colFromBOMCode,
            this.colParentBOMCode,
            this.treeListColumn2,
            this.treeListColumn3,
            this.treeListColumn7,
            this.treeListColumn16,
            this.treeListColumn19});
            this.gridCtlItem.CustomizationFormBounds = new System.Drawing.Rectangle(936, 430, 234, 209);
            this.gridCtlItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlItem.KeyFieldName = "DtlKey";
            this.gridCtlItem.Location = new System.Drawing.Point(0, 0);
            this.gridCtlItem.Name = "gridCtlItem";
            this.gridCtlItem.OptionsBehavior.PopulateServiceColumns = true;
            this.gridCtlItem.ParentFieldName = "";
            this.gridCtlItem.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemluUOM,
            this.repItemLuedtItem,
            this.repitemAPDebtorCode,
            this.repositoryItemCheckEdit2});
            this.gridCtlItem.Size = new System.Drawing.Size(865, 226);
            this.gridCtlItem.TabIndex = 5;
            this.gridCtlItem.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.gridCtlItem_FocusedNodeChanged);
            this.gridCtlItem.CellValueChanging += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.gridCtlItem_CellValueChanging);
            this.gridCtlItem.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.gridCtlItem_CellValueChanged);
            // 
            // colCheckList
            // 
            this.colCheckList.Caption = "Check";
            this.colCheckList.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colCheckList.FieldName = "Check";
            this.colCheckList.Name = "colCheckList";
            this.colCheckList.Visible = true;
            this.colCheckList.VisibleIndex = 0;
            this.colCheckList.Width = 76;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // colQty
            // 
            this.colQty.FieldName = "Qty";
            this.colQty.Name = "colQty";
            this.colQty.OptionsColumn.AllowEdit = false;
            this.colQty.Visible = true;
            this.colQty.VisibleIndex = 2;
            this.colQty.Width = 83;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "WO No.";
            this.treeListColumn1.FieldName = "DocNo";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 3;
            this.treeListColumn1.Width = 114;
            // 
            // colDocKey
            // 
            this.colDocKey.Caption = "DocKey";
            this.colDocKey.FieldName = "DocKey";
            this.colDocKey.Name = "colDocKey";
            this.colDocKey.OptionsColumn.AllowEdit = false;
            this.colDocKey.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDtlKey
            // 
            this.colDtlKey.Caption = "DtlKey";
            this.colDtlKey.FieldName = "DtlKey";
            this.colDtlKey.Name = "colDtlKey";
            this.colDtlKey.OptionsColumn.AllowEdit = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Item Code";
            this.gridColumn2.FieldName = "ItemCode";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 6;
            this.gridColumn2.Width = 113;
            // 
            // colDesc
            // 
            this.colDesc.Caption = "Description";
            this.colDesc.FieldName = "Description";
            this.colDesc.Name = "colDesc";
            this.colDesc.OptionsColumn.AllowEdit = false;
            this.colDesc.Visible = true;
            this.colDesc.VisibleIndex = 7;
            this.colDesc.Width = 163;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "UOM";
            this.gridColumn3.FieldName = "UOM";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 8;
            this.gridColumn3.Width = 71;
            // 
            // colRate
            // 
            this.colRate.Caption = "Rate";
            this.colRate.FieldName = "Rate";
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowEdit = false;
            this.colRate.Width = 36;
            // 
            // colUnitCost
            // 
            this.colUnitCost.Caption = "Unit Cost";
            this.colUnitCost.FieldName = "UnitCost";
            this.colUnitCost.Name = "colUnitCost";
            this.colUnitCost.OptionsColumn.AllowEdit = false;
            this.colUnitCost.Width = 88;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.Width = 71;
            // 
            // colItemSeq
            // 
            this.colItemSeq.Caption = "Seq";
            this.colItemSeq.FieldName = "Seq";
            this.colItemSeq.Name = "colItemSeq";
            this.colItemSeq.OptionsColumn.AllowEdit = false;
            this.colItemSeq.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDtlFromDocDtlKey
            // 
            this.colDtlFromDocDtlKey.Caption = "FromDocDtlKey";
            this.colDtlFromDocDtlKey.FieldName = "FromDocDtlKey";
            this.colDtlFromDocDtlKey.Name = "colDtlFromDocDtlKey";
            this.colDtlFromDocDtlKey.OptionsColumn.AllowEdit = false;
            this.colDtlFromDocDtlKey.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colFromBOMCode
            // 
            this.colFromBOMCode.Caption = "BOM Code";
            this.colFromBOMCode.FieldName = "BOMCode";
            this.colFromBOMCode.Name = "colFromBOMCode";
            this.colFromBOMCode.OptionsColumn.AllowEdit = false;
            this.colFromBOMCode.Visible = true;
            this.colFromBOMCode.VisibleIndex = 5;
            this.colFromBOMCode.Width = 117;
            // 
            // colParentBOMCode
            // 
            this.colParentBOMCode.Caption = "ParentBOMCode";
            this.colParentBOMCode.FieldName = "ParentBOMCode";
            this.colParentBOMCode.Name = "colParentBOMCode";
            this.colParentBOMCode.OptionsColumn.AllowEdit = false;
            this.colParentBOMCode.Visible = true;
            this.colParentBOMCode.VisibleIndex = 9;
            this.colParentBOMCode.Width = 103;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Product Item Code";
            this.treeListColumn2.FieldName = "ProductItemCode";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.OptionsColumn.AllowEdit = false;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 4;
            this.treeListColumn2.Width = 138;
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Product Item Desc.";
            this.treeListColumn3.FieldName = "ProductItemDesc";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.OptionsColumn.AllowEdit = false;
            // 
            // treeListColumn7
            // 
            this.treeListColumn7.Caption = "New Qty";
            this.treeListColumn7.FieldName = "NewQty";
            this.treeListColumn7.Name = "treeListColumn7";
            this.treeListColumn7.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn7.Visible = true;
            this.treeListColumn7.VisibleIndex = 1;
            this.treeListColumn7.Width = 89;
            // 
            // treeListColumn16
            // 
            this.treeListColumn16.Caption = "ItemType";
            this.treeListColumn16.FieldName = "ItemType";
            this.treeListColumn16.Name = "treeListColumn16";
            this.treeListColumn16.OptionsColumn.AllowEdit = false;
            this.treeListColumn16.Visible = true;
            this.treeListColumn16.VisibleIndex = 10;
            // 
            // treeListColumn19
            // 
            this.treeListColumn19.Caption = "ID";
            this.treeListColumn19.FieldName = "ID";
            this.treeListColumn19.Name = "treeListColumn19";
            this.treeListColumn19.OptionsColumn.AllowEdit = false;
            this.treeListColumn19.Visible = true;
            this.treeListColumn19.VisibleIndex = 11;
            // 
            // repositoryItemluUOM
            // 
            this.repositoryItemluUOM.AutoHeight = false;
            this.repositoryItemluUOM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemluUOM.Name = "repositoryItemluUOM";
            // 
            // repItemLuedtItem
            // 
            this.repItemLuedtItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLuedtItem.Name = "repItemLuedtItem";
            // 
            // repitemAPDebtorCode
            // 
            this.repitemAPDebtorCode.AutoHeight = false;
            this.repitemAPDebtorCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repitemAPDebtorCode.Name = "repitemAPDebtorCode";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupControl2);
            this.panel2.Controls.Add(this.groupControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(871, 172);
            this.panel2.TabIndex = 6;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.gridCtlBatchNo);
            this.groupControl2.Location = new System.Drawing.Point(241, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(627, 169);
            this.groupControl2.TabIndex = 22;
            this.groupControl2.Text = "Batch No Options";
            // 
            // gridCtlBatchNo
            // 
            this.gridCtlBatchNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlBatchNo.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlBatchNo.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlBatchNo.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlBatchNo.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlBatchNo.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlBatchNo.Location = new System.Drawing.Point(2, 21);
            this.gridCtlBatchNo.MainView = this.gvBatchNo;
            this.gridCtlBatchNo.Name = "gridCtlBatchNo";
            this.gridCtlBatchNo.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repleOvdOverheadCode,
            this.repositoryItemCheckEdit4});
            this.gridCtlBatchNo.Size = new System.Drawing.Size(623, 146);
            this.gridCtlBatchNo.TabIndex = 5;
            this.gridCtlBatchNo.UseEmbeddedNavigator = true;
            this.gridCtlBatchNo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBatchNo});
            // 
            // gvBatchNo
            // 
            this.gvBatchNo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn1,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn10});
            this.gvBatchNo.GridControl = this.gridCtlBatchNo;
            this.gvBatchNo.Name = "gvBatchNo";
            this.gvBatchNo.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvBatchNo_CellValueChanging);
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Check";
            this.gridColumn8.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn8.FieldName = "Check";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 57;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "ItemCode";
            this.gridColumn9.FieldName = "ItemCode";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 86;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "BatchNo";
            this.gridColumn1.FieldName = "BatchNo";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 110;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Description";
            this.gridColumn4.FieldName = "Description";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 301;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Manufactured Date";
            this.gridColumn5.FieldName = "ManufacturedDate";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 150;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Expiry Date";
            this.gridColumn6.FieldName = "ExpiryDate";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 139;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "BalQty";
            this.gridColumn7.FieldName = "BalQty";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            this.gridColumn7.Width = 73;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Cost";
            this.gridColumn10.FieldName = "Cost";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 7;
            this.gridColumn10.Width = 114;
            // 
            // repleOvdOverheadCode
            // 
            this.repleOvdOverheadCode.AutoHeight = false;
            this.repleOvdOverheadCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleOvdOverheadCode.Name = "repleOvdOverheadCode";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.btnInquiry);
            this.groupControl1.Controls.Add(this.cbTab);
            this.groupControl1.Controls.Add(this.luWONo);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(232, 131);
            this.groupControl1.TabIndex = 21;
            this.groupControl1.Text = "Filter Options";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 23);
            this.label3.TabIndex = 23;
            this.label3.Text = "Tab";
            // 
            // btnInquiry
            // 
            this.btnInquiry.Location = new System.Drawing.Point(133, 103);
            this.btnInquiry.Name = "btnInquiry";
            this.btnInquiry.Size = new System.Drawing.Size(75, 23);
            this.btnInquiry.TabIndex = 4;
            this.btnInquiry.Text = "Inquiry";
            this.btnInquiry.Click += new System.EventHandler(this.btnInquiry_Click);
            // 
            // cbTab
            // 
            this.cbTab.EditValue = "Summay";
            this.cbTab.Location = new System.Drawing.Point(90, 61);
            this.cbTab.Name = "cbTab";
            this.cbTab.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTab.Properties.Items.AddRange(new object[] {
            "Summay",
            "Detail"});
            this.cbTab.Size = new System.Drawing.Size(118, 20);
            this.cbTab.TabIndex = 22;
            // 
            // luWONo
            // 
            this.luWONo.Location = new System.Drawing.Point(90, 35);
            this.luWONo.Name = "luWONo";
            this.luWONo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luWONo.Size = new System.Drawing.Size(118, 20);
            this.luWONo.TabIndex = 20;
            this.luWONo.EditValueChanged += new System.EventHandler(this.luWONo_EditValueChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "Filter  WO No";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 172);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(871, 254);
            this.xtraTabControl1.TabIndex = 8;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridCtlSummary);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(865, 226);
            this.xtraTabPage1.Text = "Summary";
            // 
            // gridCtlSummary
            // 
            this.gridCtlSummary.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn4,
            this.treeListColumn5,
            this.treeListColumn8,
            this.treeListColumn9,
            this.treeListColumn10,
            this.treeListColumn11,
            this.treeListColumn12,
            this.treeListColumn13,
            this.treeListColumn14,
            this.treeListColumn15,
            this.colBatchNo,
            this.treeListColumn6,
            this.treeListColumn17,
            this.treeListColumn18});
            this.gridCtlSummary.CustomizationFormBounds = new System.Drawing.Rectangle(936, 430, 234, 209);
            this.gridCtlSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlSummary.KeyFieldName = "DtlKey";
            this.gridCtlSummary.Location = new System.Drawing.Point(0, 0);
            this.gridCtlSummary.Name = "gridCtlSummary";
            this.gridCtlSummary.OptionsBehavior.PopulateServiceColumns = true;
            this.gridCtlSummary.ParentFieldName = "DtlKey";
            this.gridCtlSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemLookUpEdit2,
            this.repositoryItemLookUpEdit3,
            this.repositoryItemCheckEdit3});
            this.gridCtlSummary.Size = new System.Drawing.Size(865, 226);
            this.gridCtlSummary.TabIndex = 6;
            this.gridCtlSummary.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.gridCtlSummary_FocusedNodeChanged);
            this.gridCtlSummary.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.gridCtlSummary_CellValueChanged);
            // 
            // treeListColumn4
            // 
            this.treeListColumn4.Caption = "Check";
            this.treeListColumn4.ColumnEdit = this.repositoryItemCheckEdit3;
            this.treeListColumn4.FieldName = "Check";
            this.treeListColumn4.Name = "treeListColumn4";
            this.treeListColumn4.Visible = true;
            this.treeListColumn4.VisibleIndex = 0;
            this.treeListColumn4.Width = 88;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // treeListColumn5
            // 
            this.treeListColumn5.Caption = "WO No.";
            this.treeListColumn5.FieldName = "DocNo";
            this.treeListColumn5.Name = "treeListColumn5";
            this.treeListColumn5.OptionsColumn.AllowEdit = false;
            this.treeListColumn5.Width = 95;
            // 
            // treeListColumn8
            // 
            this.treeListColumn8.Caption = "Item Code";
            this.treeListColumn8.FieldName = "ItemCode";
            this.treeListColumn8.Name = "treeListColumn8";
            this.treeListColumn8.OptionsColumn.AllowEdit = false;
            this.treeListColumn8.Visible = true;
            this.treeListColumn8.VisibleIndex = 1;
            this.treeListColumn8.Width = 116;
            // 
            // treeListColumn9
            // 
            this.treeListColumn9.Caption = "Description";
            this.treeListColumn9.FieldName = "Description";
            this.treeListColumn9.Name = "treeListColumn9";
            this.treeListColumn9.OptionsColumn.AllowEdit = false;
            this.treeListColumn9.Visible = true;
            this.treeListColumn9.VisibleIndex = 2;
            this.treeListColumn9.Width = 192;
            // 
            // treeListColumn10
            // 
            this.treeListColumn10.Caption = "UOM";
            this.treeListColumn10.FieldName = "UOM";
            this.treeListColumn10.Name = "treeListColumn10";
            this.treeListColumn10.OptionsColumn.AllowEdit = false;
            this.treeListColumn10.Visible = true;
            this.treeListColumn10.VisibleIndex = 3;
            this.treeListColumn10.Width = 100;
            // 
            // treeListColumn11
            // 
            this.treeListColumn11.Caption = "Rate";
            this.treeListColumn11.FieldName = "Rate";
            this.treeListColumn11.Name = "treeListColumn11";
            this.treeListColumn11.OptionsColumn.AllowEdit = false;
            this.treeListColumn11.Width = 36;
            // 
            // treeListColumn12
            // 
            this.treeListColumn12.FieldName = "Qty";
            this.treeListColumn12.Name = "treeListColumn12";
            this.treeListColumn12.Visible = true;
            this.treeListColumn12.VisibleIndex = 4;
            this.treeListColumn12.Width = 94;
            // 
            // treeListColumn13
            // 
            this.treeListColumn13.Caption = "Unit Cost";
            this.treeListColumn13.FieldName = "UnitCost";
            this.treeListColumn13.Name = "treeListColumn13";
            this.treeListColumn13.OptionsColumn.AllowEdit = false;
            this.treeListColumn13.Width = 88;
            // 
            // treeListColumn14
            // 
            this.treeListColumn14.Caption = "Total Cost";
            this.treeListColumn14.FieldName = "TotalCost";
            this.treeListColumn14.Name = "treeListColumn14";
            this.treeListColumn14.OptionsColumn.AllowEdit = false;
            this.treeListColumn14.Width = 71;
            // 
            // treeListColumn15
            // 
            this.treeListColumn15.Caption = "Seq";
            this.treeListColumn15.FieldName = "Seq";
            this.treeListColumn15.Name = "treeListColumn15";
            this.treeListColumn15.OptionsColumn.AllowEdit = false;
            this.treeListColumn15.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colBatchNo
            // 
            this.colBatchNo.Caption = "BatchNo";
            this.colBatchNo.FieldName = "BatchNo";
            this.colBatchNo.Name = "colBatchNo";
            this.colBatchNo.OptionsColumn.AllowEdit = false;
            // 
            // treeListColumn6
            // 
            this.treeListColumn6.Caption = "DtlKey";
            this.treeListColumn6.FieldName = "DtlKey";
            this.treeListColumn6.Name = "treeListColumn6";
            this.treeListColumn6.OptionsColumn.AllowEdit = false;
            this.treeListColumn6.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // treeListColumn17
            // 
            this.treeListColumn17.Caption = "ItemType";
            this.treeListColumn17.FieldName = "ItemType";
            this.treeListColumn17.Name = "treeListColumn17";
            this.treeListColumn17.Visible = true;
            this.treeListColumn17.VisibleIndex = 5;
            // 
            // treeListColumn18
            // 
            this.treeListColumn18.Caption = "ID";
            this.treeListColumn18.FieldName = "ID";
            this.treeListColumn18.Name = "treeListColumn18";
            this.treeListColumn18.OptionsColumn.AllowEdit = false;
            this.treeListColumn18.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridCtlItem);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(865, 226);
            this.xtraTabPage2.Text = "Detail";
            // 
            // FormMultiSelectWORM
            // 
            this.AcceptButton = this.sbtnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnCancel;
            this.ClientSize = new System.Drawing.Size(871, 471);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMultiSelectWORM";
            this.ShowInTaskbar = false;
            this.Text = "Multi-select Item BOM";
            this.Load += new System.EventHandler(this.FormParameterCustom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit_Check)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbEdtFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemluUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLuedtItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repitemAPDebtorCode)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlBatchNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBatchNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleOvdOverheadCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbTab.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luWONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

    }

        private void btnInquiry_Click(object sender, EventArgs e)
        {
            this.myDataTable.Clear();
            this.mySummaryTable.Clear();
            this.myBatchNoTable.Clear();


            if (cbTab.SelectedIndex == 0)
            {
                xtraTabPage1.PageVisible = true;
                xtraTabPage2.PageVisible = false;
            }
            else
            {
                xtraTabPage1.PageVisible = false;
                xtraTabPage2.PageVisible = true;
            }
            this.gridCtlItem.DataSource = null;
            this.gridCtlSummary.DataSource = null;
            string swhere = "";
            if (luWONo.Text != "")
                swhere = " and DocNo='"+luWONo.Text+"'";
            myBatchNoTable = myDBSetting.GetDataTable("select distinct ItemCode,ItemDescription,UOM,BatchNo,Description,ManufacturedDate,ExpiryDate,BalQty,(select Max(Cost) from StockDTL a where  LastModified = (select max(LastModified) FROM StockDTL b where  a.ItemCode = b.ItemCode and a.BatchNo = b.BatchNo and a.UOM = b.UOM) and ItemCode=vRPA_BatchNoWO.ItemCode and BatchNo=vRPA_BatchNoWO.BatchNo and UOM=vRPA_BatchNoWO.UOM) as Cost from vRPA_BatchNoWO where ISNULL(BatchNo,'')<>'' and BalQty>0 " + swhere, false, "");
            try { this.myBatchNoTable.Columns.Add(new DataColumn("Check", System.Type.GetType("System.Boolean"))); } catch { }

            this.myLookupEditBuilder = new WORMDetailLookupEditBuilder(swhere);
            this.myDataTable = this.myLookupEditBuilder.BuildDataTable(myDBSetting);
            this.gridCtlItem.DataSource = (object)this.myDataTable;
            myBatchNoTable.DefaultView.RowFilter = "ItemCode='-1'";
            this.gridCtlBatchNo.DataSource = myBatchNoTable.DefaultView;
            try { this.myDataTable.Columns.Add(new DataColumn("Check", System.Type.GetType("System.Boolean"))); } catch { }
            foreach (DataRow dataRow in (InternalDataCollectionBase)this.myBatchNoTable.Rows)
            {
                dataRow["Check"] = (object)false;
            }
            int iDtlKey = 0;
            foreach (DataRow dr in myDataTable.Rows)
            {
               
                //bool bEdit = false;
                DataRow[] drSummary = mySummaryTable.Select("ItemCode='" + dr["ItemCode"] + "' and UOM='" + dr["UOM"] + "' AND ItemType='"+ dr["ItemType"] + "'");
                if (drSummary.Length > 0)
                {
                    drSummary[0]["Qty"] = BCE.Data.Convert.ToDecimal(drSummary[0]["Qty"]) + BCE.Data.Convert.ToDecimal(dr["Qty"]);
                    drSummary[0]["UnitCost"] = dr["UnitCost"];
                    drSummary[0]["TotalCost"] = BCE.Data.Convert.ToDecimal(dr["UnitCost"]) * BCE.Data.Convert.ToDecimal(drSummary[0]["Qty"]);
                }
                else
                {
                    DataRow drNewSummary = mySummaryTable.NewRow();
                    //drNewSummary["ID"] = iDtlKey;
                    drNewSummary["DtlKey"] = iDtlKey;
                    drNewSummary["ItemCode"] = dr["ItemCode"];
                    drNewSummary["Description"] = dr["Description"];
                    drNewSummary["UOM"] = dr["UOM"];
                    drNewSummary["Qty"] = dr["Qty"];
                    drNewSummary["ItemType"] = dr["ItemType"];
                    drNewSummary["UnitCost"] = dr["UnitCost"];
                    drNewSummary["TotalCost"] = dr["TotalCost"];
                    // drNewSummary["Guid"] = (object)Guid.NewGuid();
                    mySummaryTable.Rows.Add(drNewSummary);
                }
                iDtlKey++;
            }
            if (this.myFilter != null)
            {
                if (this.myFilter.Count > 0 && this.myFilter.ToString()!="")
                {
                    for (int index = 0; index < this.myFilter.Count; ++index)
                    {
                        string str = (string)this.myFilter[index];
                        if (cbTab.SelectedIndex == 0)
                        {
                            foreach (DataRow dataRow in (InternalDataCollectionBase)this.mySummaryTable.Rows)
                            {
                                if (dataRow["DtlKey"].ToString() == str)
                                    dataRow["Check"] = (object)true;
                                else
                                    dataRow["Check"] = (object)false;
                            }
                        }
                        else
                        {
                            foreach (DataRow dataRow in (InternalDataCollectionBase)this.myDataTable.Rows)
                            {
                                if (dataRow["DtlKey"].ToString() == str)
                                    dataRow["Check"] = (object)true;
                                else
                                    dataRow["Check"] = (object)false;
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataRow dataRow in (InternalDataCollectionBase)this.myDataTable.Rows)
                    {
                        dataRow["Check"] = (object)false;
                    }
                    foreach (DataRow dataRow in (InternalDataCollectionBase)this.mySummaryTable.Rows)
                    {
                        dataRow["Check"] = (object)false;
                    }
                }
                this.UpdateSelectedCount();
            }
            else
            {
                foreach (DataRow dataRow in (InternalDataCollectionBase)this.mySummaryTable.Rows)
                {
                    dataRow["Check"] = (object)false;
                }
                foreach (DataRow dataRow in (InternalDataCollectionBase)this.myDataTable.Rows)
                {
                    //if (dataRow["DtlKey"].ToString() == str)
                    //    dataRow["Check"] = (object)true;
                    //else
                        dataRow["Check"] = (object)false;
                }
            }
            this.gridCtlSummary.DataSource = mySummaryTable;


        }
        private bool OneOfChildsIsChecked(DevExpress.XtraTreeList.Nodes.TreeListNode node)
        {
            bool result = false;
            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode item in node.Nodes)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    result = true;
                }
            }
            return result;
        }
        private void gridCtlItem_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
        {
            DevExpress.XtraTreeList.Nodes.TreeListNode node = e.Node;

            if (e.Column.FieldName == "Check")
            {
                if (BCE.Data.Convert.ToBoolean(e.Value))
                    gridCtlItem.NodesIterator.DoLocalOperation(n => { n.SetValue("Check", BCE.Data.Convert.ToBoolean(e.Value)); }, gridCtlItem.FocusedNode.Nodes);
                UpdateSelectedCount();
            }

        }

        private void gridCtlItem_CellValueChanging(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
        {
        
        }

        private void gridCtlSummary_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
        {

            DevExpress.XtraTreeList.Nodes.TreeListNode node = e.Node;

            if (e.Column.FieldName == "Check")
            {

                UpdateSelectedCount();

            }
            if (e.Column.FieldName == "Qty")
            {
                object obj = e.Node.GetValue("DtlKey");
                if(obj!=null && obj!=DBNull.Value)
                {
                    //DataRow[] drArray = myDataTable.Select("DtlKey=" + obj + "", "", DataViewRowState.CurrentRows);//.Rows.Find(obj);

                    DataRow drArray=myDataTable.Rows[BCE.Data.Convert.ToInt16(gridCtlSummary.GetVisibleIndexByNode(e.Node))];
                    if (drArray!=null)
                    {
                        drArray.BeginEdit();
                        drArray["Qty"] = e.Value;
                        drArray.EndEdit();
                    }



                }
            }
        }

        private void luWONo_EditValueChanged(object sender, EventArgs e)
        {
            btnInquiry.Enabled = true;
        }

        private void gridCtlSummary_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            imyDtlKey = -1;
            DevExpress.XtraTreeList.Nodes.TreeListNode node = e.Node;
            if (node == null)
                return;
            myBatchNoTable.DefaultView.RowFilter = "ItemCode='"+node.GetValue("ItemCode")+"' and UOM='"+ node.GetValue("UOM") + "'";
            imyDtlKey = BCE.Data.Convert.ToInt32(node.GetValue("DtlKey"));
        }

        private void gridCtlItem_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            DevExpress.XtraTreeList.Nodes.TreeListNode node = e.Node;
            imyDtlKey = -1;
            if (node == null)
                return;
            myBatchNoTable.DefaultView.RowFilter = "ItemCode='" + node.GetValue("ItemCode") + "' and UOM='" + node.GetValue("UOM") + "'";
            imyDtlKey = BCE.Data.Convert.ToInt32(node.GetValue("DtlKey"));
        }

        private void gvBatchNo_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            
            if (e.Column.FieldName=="Check")
            {
                DataRow[] drDetail = myDataTable.Select("DtlKey='"+imyDtlKey+"'","",DataViewRowState.CurrentRows);
                if(drDetail.Length>0)
                {
                    drDetail[0]["Check"] = BCE.Data.Convert.ToBoolean(e.Value);
                }
                DataRow[] drSummary =mySummaryTable.Select("DtlKey='" + imyDtlKey + "'", "", DataViewRowState.CurrentRows);
                if (drSummary.Length > 0)
                {
                    drSummary[0]["Check"] = BCE.Data.Convert.ToBoolean(e.Value);
                }

                //if(BCE.Data.Convert.ToBoolean(e.Value))
                //{
                //gridCtlItem.FocusedNode.SetValue("Check", BCE.Data.Convert.ToBoolean(e.Value));
                //gridCtlItem.FocusedNode.Checked = true;
                //}
            }
            //myBatchNoTable.DefaultView.RowFilter = "ItemCode='" + node.GetValue("ItemCode") + "' and UOM='" + node.GetValue("UOM") + "'";
        }
    }
}
