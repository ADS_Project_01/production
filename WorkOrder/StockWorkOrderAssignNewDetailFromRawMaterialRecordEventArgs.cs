﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderAssignNewDetailFromRawMaterialRecordEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Manufacturing;
using BCE.Data;
using System.Data;

namespace Production.WorkOrder
{
  public class StockWorkOrderAssignNewDetailFromRawMaterialRecordEventArgs
  {
    private StockWorkOrder myStock;
    private DataRow myDetailRow;
    private DataRow myBOMRow;

    public StockWorkOrderRecord MasterRecord
    {
      get
      {
        return new StockWorkOrderRecord(this.myStock);
      }
    }

    public StockWorkOrderDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockWorkOrderDetailRecord(this.myStock.Command.DBSetting, this.myDetailRow);
      }
    }

    //public RawMaterialRecord RawMaterialRecord
    //{
    //  get
    //  {
    //    return new RawMaterialRecord(this.myBOMRow);
    //  }
    //}

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.DBSetting;
      }
    }

    internal StockWorkOrderAssignNewDetailFromRawMaterialRecordEventArgs(StockWorkOrder doc, DataRow detailRow, DataRow bomRow)
    {
      this.myStock = doc;
      this.myDetailRow = detailRow;
      this.myBOMRow = bomRow;
    }
  }
}
