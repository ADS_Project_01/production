﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BCE.Localization;
namespace Production.WorkOrder
{
    [LocalizableString]
    public enum WorkOrderStatusOptions
    {
        [DefaultString("Pending")]
        Pending,
        [DefaultString("Planned")]
        Planned,
        [DefaultString("InProcess")]
        InProcess,
        [DefaultString("Assembly")]
        Assembly,
        [DefaultString("Closed")]
        Closed,
        [DefaultString("Cancel")]
        Cancel,
    }
}
