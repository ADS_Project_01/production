﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.StockWorkOrderCommand
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.Document;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.RegistryID.LastSavedDescriptionID;
using BCE.AutoCount.RegistryID.Misc;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Data;
using BCE.AutoCount.UDF;
namespace Production.WorkOrder
{
    public class StockWorkOrderCommand
    {
        public const string DocumentStyleReportType = "Stock Work Order Document";
        public const string ListingStyleReportType = "Stock Work Order Listing";
        protected const string ReportOptionSettingFileName = "StockWorkOrderReportOption.setting";
        internal const string MasterTableName = "Master";
        internal const string DetailTableName = "Detail";
        internal const string ASMBOMOptionalTableName = "ASMBOMOptional";
        protected DataTable myDataTableAllMaster;
        protected internal StockHelper myHelper;
        protected internal DBSetting myDBSetting;
        protected internal DBRegistry myDBReg;
        internal FiscalYear myFiscalYear;
        internal GeneralSetting myGeneralSetting;
        internal DecimalSetting myDecimalSetting;
        internal UserAuthentication myUserAuthentication;
        private BasicReportOption myReportOption;
        protected const string LoadMasterDataSQL = "SELECT * FROM RPA_WO WHERE DocKey=@DocKey";
        protected const string LoadDetailDataSQL = "SELECT * FROM RPA_WODTL WHERE DocKey=@DocKey ORDER BY Seq";
        protected const string LoadMasterDataByDocNoSQL = "SELECT * FROM RPA_WO WHERE DocNo=@DocNo";
        protected const string LoadDetailDataByDocNoSQL = "SELECT A.* FROM RPA_WODTL A, RPA_WO B WHERE A.DocKey=B.DocKey AND B.DocNo=@DocNo ORDER BY A.Seq";
        protected const string SaveMasterDataSQL = "SELECT * FROM RPA_WO";
        protected const string SaveDetailDataSQL = "SELECT * FROM RPA_WODTL";
        protected const string SaveASMBOMOptionalSQL = "SELECT * FROM ASMBOMOptional";
        protected const string DeleteMasterDataSQL = "DELETE FROM RPA_WO WHERE DocKey=@DocKey";
        protected const string DeleteDetailDataSQL = "DELETE FROM RPA_WODTL WHERE DocKey=@DocKey";
        protected const string LoadItemUOMDataSQL = "SELECT a.Rate, a.UOM, a.Cost FROM ITEMUOM a, ITEM b WHERE a.ItemCode=b.ItemCode AND a.UOM=b.BaseUOM AND a.ItemCode=@ItemCode";
        protected const string LoadItemBOMDataSQL = "SELECT * FROM ItemBOM WHERE ItemCode=@ItemCode ORDER BY ItemCode, Seq";
        protected const string LoadAllMasterDataSQL = "SELECT {0} FROM RPA_WO";
        protected const string LoadSearchMasterDataSQL = "SELECT DISTINCT {0} FROM RPA_WO A, RPA_WODTL B WHERE (A.DocKey=B.DocKey)";
        protected const string LoadSearchMasterDataOnlySQL = "SELECT {0} FROM RPA_WO A";
        protected const string LoadDesignReportMasterDataSQL = "SELECT TOP 5 * FROM vStockWorkOrder ORDER BY DocKey";
        protected const string LoadDesignReportDetailDataSQL = "SELECT * FROM vStockWorkOrderDetail WHERE DocKey IN (SELECT TOP 5 DocKey FROM vStockWorkOrder ORDER BY DocKey) AND PrintOut='T' ORDER BY DocKey, Seq";
        protected const string LoadReportMasterDataSQL = "SELECT * FROM vStockWorkOrder WHERE DocKey=@DocKey";
        protected const string LoadReportDetailDataSQL = "SELECT * FROM vStockWorkOrderDetail WHERE DocKey=@DocKey AND UOM = ItemBaseUOM AND PrintOut='T' ORDER BY Seq";
        protected const string BasicSearchSQL = " SELECT {0} FROM RPA_WO WHERE DocKey In (SELECT DISTINCT RPA_WO.DocKey FROM RPA_WO INNER JOIN RPA_WODTL ON RPA_WO.DocKey = RPA_WODTL.DocKey WHERE {1})";
        protected const string LoadDocumentListingReportMasterDataSQL = "SELECT * FROM vStockWorkOrder WHERE DocKey IN ({0}) ";
        protected const string LoadDocumentListingReportDetailDataSQL = "SELECT * FROM vStockWorkOrderDetail WHERE DocKey IN ({0}) AND UOM = ItemBaseUOM ORDER BY Seq";
        protected const string LoadDesignDocumentListingReportMasterDataSQL = "SELECT TOP 100 * FROM vStockWorkOrder ORDER BY DocKey";
        protected const string LoadDesignDocumentListingReportDetailDataSQL = "SELECT * FROM vStockWorkOrderDetail WHERE DocKey IN (SELECT TOP 100 DocKey FROM vStockWorkOrder ORDER BY DocKey) ORDER BY DocKey, Seq";
        protected const string DocumentStyleReportingBasicSearchSQL = " SELECT DISTINCT {0} FROM RPA_WO LEFT OUTER JOIN RPA_WODTL ON (RPA_WO.DocKey=RPA_WODTL.DocKey) WHERE {1}";
        private static AsyncDataSetUpdate myDataSetUpdate;
        private static StartBusinessFlowDelegate myStartBusinessFlowDelegate;

        public static AsyncDataSetUpdate DataSetUpdate
        {
            get
            {
                if (StockWorkOrderCommand.myDataSetUpdate == null)
                    StockWorkOrderCommand.myDataSetUpdate = new AsyncDataSetUpdate();
                return StockWorkOrderCommand.myDataSetUpdate;
            }
        }

        public BasicReportOption ReportOption
        {
            get
            {
                return this.myReportOption;
            }
        }

        public DataTable DataTableAllMaster
        {
            get
            {
                return this.myDataTableAllMaster;
            }
        }

        public DBSetting DBSetting
        {
            get
            {
                return this.myDBSetting;
            }
        }

        public DBRegistry DBReg
        {
            get
            {
                return this.myDBReg;
            }
        }

        public static StartBusinessFlowDelegate StartBusinessFlow
        {
            get
            {
                return StockWorkOrderCommand.myStartBusinessFlowDelegate;
            }
            set
            {
                StockWorkOrderCommand.myStartBusinessFlowDelegate = value;
            }
        }

        public GeneralSetting GeneralSetting
        {
            get
            {
                return this.myGeneralSetting;
            }
        }

        public DecimalSetting DecimalSetting
        {
            get
            {
                return this.myDecimalSetting;
            }
        }

        public UserAuthentication UserAuthentication
        {
            get
            {
                return this.myUserAuthentication;
            }
        }

        static StockWorkOrderCommand()
        {
        }

        internal StockWorkOrderCommand()
        {
            this.myDataTableAllMaster = new DataTable();
            try
            {
                this.myReportOption = (BasicReportOption)PersistenceUtil.LoadUserSetting("StockWorkOrderReportOption.setting");
            }
            catch
            {
            }
            if (this.myReportOption == null)
                this.myReportOption = new BasicReportOption();
        }

        public static StockWorkOrderCommand Create(DBSetting dbSetting)
        {
            UserAuthentication.GetOrCreate(dbSetting).CheckHasLogined();
            StockWorkOrderCommand assemblyOrderCommand = (StockWorkOrderCommand)null;
            if (dbSetting.ServerType == DBServerType.SQL2000)
                assemblyOrderCommand = (StockWorkOrderCommand)new StockWorkOrderCommandSQL();
            else
                dbSetting.ThrowServerTypeNotSupportedException();
            assemblyOrderCommand.myDBSetting = dbSetting;
            assemblyOrderCommand.myDBReg = DBRegistry.Create(dbSetting);
            assemblyOrderCommand.myHelper = StockHelper.Create(dbSetting);
            assemblyOrderCommand.myFiscalYear = FiscalYear.GetOrCreate(dbSetting);
            assemblyOrderCommand.myGeneralSetting = GeneralSetting.GetOrCreate(dbSetting);
            assemblyOrderCommand.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
            assemblyOrderCommand.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
            return assemblyOrderCommand;
        }

        protected virtual DataSet LoadData(long docKey)
        {
            return (DataSet)null;
        }

        protected virtual DataSet LoadData(string docNo)
        {
            return (DataSet)null;
        }

        protected virtual long LoadFirst()
        {
            return -1L;
        }

        protected virtual long LoadLast()
        {
            return -1L;
        }

        protected virtual long LoadNext(string docNo)
        {
            return -1L;
        }

        protected virtual long LoadPrev(string docNo)
        {
            return -1L;
        }

        protected internal virtual void SaveData(StockWorkOrder stockAsmOrder)
        {
        }

        protected virtual void DeleteData(long docKey)
        {
        }

        public virtual DataTable LoadAllBOMItems()
        {
            return (DataTable)null;
        }

        internal virtual DataTable LoadItemBOMData(string bomcode)
        {
            return (DataTable)null;
        }
        internal virtual DataTable LoadItemBSData(string bomcode)
        {
            return (DataTable)null;
        }
        internal virtual DataTable LoadItemPIData(string bomcode)
        {
            return (DataTable)null;
        }
        internal virtual DataTable LoadItemAPData(string bomcode)
        {
            return (DataTable)null;
        }
        internal virtual DataTable LoadItemOvdData(string bomcode)
        {
            return (DataTable)null;
        }
        internal virtual DataRow LoadItemUOMDataRow(string itemCode)
        {
            return (DataRow)null;
        }

        internal virtual DataTable LoadPartialTransferItemFromSO(object debtorcode)
        {
            return (DataTable)null;
        }

        internal virtual DataSet LoadPartialTransferStatus(long docKey)
        {
            return (DataSet)null;
        }

        public virtual void DocumentListingBasicSearch(StockWorkOrderReportingCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName,bool bclose)
        {
        }

        public virtual void AdvanceSearch(AdvancedStockWorkOrderCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
        {
        }

        public virtual void BasicSearch(StockWorkOrderCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
        {
        }
        public virtual void ProcessClose(StockWorkOrderReportingCriteria criteria, DataTable resultDataTable)
        {
        }

        public StockWorkOrder LoadFromTempDocument(long docKey)
        {
            DataSet aDataSet = TempDocument.Create(this.myDBSetting).Load("WO", docKey);
            if (aDataSet == null)
            {
                return (StockWorkOrder)null;
            }
            else
            {
                StockWorkOrderAction action = aDataSet.Tables["Master"].Rows[0].RowState != DataRowState.Added ? StockWorkOrderAction.Edit : StockWorkOrderAction.New;
                return new StockWorkOrder(this, aDataSet, action);
            }
        }

        public void SaveToTempDocument(StockWorkOrder document, string saveReason)
        {
            TempDocument.Create(this.myDBSetting).Save(document.DocKey, "WO", (string)document.DocNo, saveReason, (string)document.Description, document.myDataSet);
        }

        public StockWorkOrder AddNew()
        {
            DataSet aDataSet = this.LoadData(-1L);
            DataRow row = aDataSet.Tables["Master"].NewRow();
            this.InitNewMasterRow(row);
            aDataSet.Tables["Master"].Rows.Add(row);
            StockWorkOrder doc = new StockWorkOrder(this, aDataSet, StockWorkOrderAction.New);
            //StockWorkOrderEventArgs assemblyOrderEventArgs1 = new StockWorkOrderEventArgs(doc);
            //ScriptObject scriptObject = doc.ScriptObject;
            //string name = "OnNewDocument";
            //Type[] types = new Type[1];
            //int index1 = 0;
            //Type type = assemblyOrderEventArgs1.GetType();
            //types[index1] = type;
            //object[] objArray = new object[1];
            //int index2 = 0;
            //StockWorkOrderEventArgs assemblyOrderEventArgs2 = assemblyOrderEventArgs1;
            //objArray[index2] = (object)assemblyOrderEventArgs2;
            //scriptObject.RunMethod(name, types, objArray);
            return doc;
        }

        private void InitNewMasterRow(DataRow row)
        {
            DBRegistry dbRegistry = DBRegistry.Create(this.myDBSetting);
            if (row.Table.Columns.Contains("Guid"))
                row["Guid"] = (object)Guid.NewGuid();
            row["DocKey"] = (object)dbRegistry.IncOne((IRegistryID)new GlobalUniqueKey());
            row["PrintCount"] = (object)0;
            row["Cancelled"] = (object)BCE.Data.Convert.BooleanToText(false);
            row["DocNo"] = (object)"<<New>>";
            row["DebtorCode"] = (object)"";
            row["DebtorName"] = (object)"";
            row["MachineCode"] = (object)"";
            row["CanSync"] = (object)BCE.Data.Convert.BooleanToText(true);
            row["DocDate"] = (object)DateTime.Today;
            row["ProductionStartDate"] = (object)DateTime.Today;
            row["ProductionEndDate"] = (object)DateTime.Today.AddDays(30); ;
          //  row["DocType"] = (object)WorkOrderDocTypeOptions.Internal;
            row["Status"] = (object)WorkOrderStatusOptions.Planned;
            row["LastUpdate"] = (object)-1;
            row["LastModifiedUserID"] = (object)"";
            row["LastModified"] = (object)DateTime.MinValue;
            row["CreatedUserID"] = (object)"";
            row["CreatedTimeStamp"] = (object)DateTime.MinValue;
            //row["EstQty"] = (object)1;
            //row["ActQty"] = (object)1;
            //row["EstHPP"] = (object)0;
            //row["ActHPP"] = (object)0;


            row["Location"] = (object)this.myUserAuthentication.MainLocation;
            //string str = dbRegistry.GetString((IRegistryID) new StockWorkOrderDescriptionID());
            //int maxLength = row.Table.Columns["Description"].MaxLength;
            //if (str.Length > maxLength)
            //  str = str.Substring(0, maxLength);
            row["Description"] = (object)"";
            //row["IsMultilevel"] = !ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.MultiLevelAssembly.Enable ? (object) "F" : (dbRegistry.GetBoolean((IRegistryID) new EnableMultiLevelStockAssembly()) ? (object) "T" : (object) "F");
            row.EndEdit();
        }

        public StockWorkOrder View(long docKey)
        {
            return this.InternalView(this.LoadData(docKey));
        }

        public StockWorkOrder View(string docNo)
        {
            return this.InternalView(this.LoadData(docNo));
        }

        public StockWorkOrder ViewFirst()
        {
            long docKey = this.LoadFirst();
            if (docKey < 0L)
                return (StockWorkOrder)null;
            else
                return this.View(docKey);
        }

        public StockWorkOrder ViewLast()
        {
            long docKey = this.LoadLast();
            if (docKey < 0L)
                return (StockWorkOrder)null;
            else
                return this.View(docKey);
        }

        public StockWorkOrder ViewNext(string docNo)
        {
            long docKey = this.LoadNext(docNo);
            if (docKey < 0L)
                return (StockWorkOrder)null;
            else
                return this.View(docKey);
        }

        public StockWorkOrder ViewPrev(string docNo)
        {
            long docKey = this.LoadPrev(docNo);
            if (docKey < 0L)
                return (StockWorkOrder)null;
            else
                return this.View(docKey);
        }

        private StockWorkOrder InternalView(DataSet newDataSet)
        {
            if (newDataSet.Tables["Master"].Rows.Count == 0)
            {
                return (StockWorkOrder)null;
            }
            else
            {
                BCE.Data.Convert.ToInt64(newDataSet.Tables["Master"].Rows[0]["DocKey"]);
                return new StockWorkOrder(this, newDataSet, StockWorkOrderAction.View);
            }
        }

        public StockWorkOrder Edit(long docKey)
        {
            return this.InternalEdit(this.LoadData(docKey));
        }

        public StockWorkOrder Edit(string docNo)
        {
            return this.InternalEdit(this.LoadData(docNo));
        }

        private StockWorkOrder InternalEdit(DataSet newDataSet)
        {
            if (newDataSet.Tables["Master"].Rows.Count == 0)
            {
                return (StockWorkOrder)null;
            }
            else
            {
                this.myFiscalYear.CheckTransactionDate(BCE.Data.Convert.ToDateTime(newDataSet.Tables["Master"].Rows[0]["DocDate"]), "StockWorkOrder", this.myDBSetting);
                BCE.Data.Convert.ToInt64(newDataSet.Tables["Master"].Rows[0]["DocKey"]);
                return new StockWorkOrder(this, newDataSet, StockWorkOrderAction.Edit);
            }
        }

        public void Delete(long docKey)
        {
            this.DeleteData(docKey);
        }

        public bool CancelDocument(long docKey, string userID)
        {
            StockWorkOrder stockWorkOrder = this.View(docKey);
            if (stockWorkOrder != null)
            {
                stockWorkOrder.CancelDocument(userID);
                return true;
            }
            else
                return false;
        }

        public bool CancelDocument(string docNo, string userID)
        {
            StockWorkOrder stockWorkOrder = this.View(docNo);
            if (stockWorkOrder != null)
            {
                stockWorkOrder.CancelDocument(userID);
                return true;
            }
            else
                return false;
        }

        public bool UncancelDocument(long docKey, string userID)
        {
            StockWorkOrder stockWorkOrder = this.View(docKey);
            if (stockWorkOrder != null)
            {
                stockWorkOrder.UncancelDocument(userID);
                return true;
            }
            else
                return false;
        }

        public bool UncancelDocument(string docNo, string userID)
        {
            StockWorkOrder stockWorkOrder = this.View(docNo);
            if (stockWorkOrder != null)
            {
                stockWorkOrder.UncancelDocument(userID);
                return true;
            }
            else
                return false;
        }

        public void StartBusinessFlowForm(long docKey)
        {
            if (StockWorkOrderCommand.myStartBusinessFlowDelegate != null)
                StockWorkOrderCommand.myStartBusinessFlowDelegate(this.myDBSetting, docKey);
        }

        public virtual int InquireAllMaster(string columnSQL)
        {
            return 0;
        }

        public virtual int SearchMaster(SearchCriteria criteria, string columnSQL, DataTable resultTable, string MultiSelectColumnName)
        {
            return 0;
        }

        protected void f(DataSet ds, DBSetting dbSetting, AuditTrail.EventType eventType)
        {
            DataRow dataRow = ds.Tables["Master"].Rows[0];
            AuditColumnMap auditColumnMap1 = new AuditColumnMap(Localizer.GetString((Enum)StockWorkOrderStringId.StockWorkOrder, new object[0]), ds.Tables["Master"]);
            auditColumnMap1.AddColumn("DocNo", Localizer.GetString((Enum)StockWorkOrderStringId.DocumentNo, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap1.AddColumn("DocDate", Localizer.GetString((Enum)StockWorkOrderStringId.Date, new object[0]), DocumentAuditTrail.ColumnType.Date);
            auditColumnMap1.AddColumn("Description", Localizer.GetString((Enum)StockWorkOrderStringId.Description, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap1.AddColumn("RefDocNo", Localizer.GetString((Enum)StockWorkOrderStringId.RefDocNo, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap1.AddColumn("ExpectedCompletedDate", Localizer.GetString((Enum)StockWorkOrderStringId.ExpectedCompletionDate, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap1.AddColumn("ItemCode", Localizer.GetString((Enum)StockWorkOrderStringId.ItemCode, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap1.AddColumn("Location", Localizer.GetString((Enum)StockWorkOrderStringId.Location, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap1.AddColumn("BatchNo", Localizer.GetString((Enum)StockWorkOrderStringId.BatchNo, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap1.AddColumn("ProjNo", Localizer.GetString((Enum)StockWorkOrderStringId.ProjectNo_, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap1.AddColumn("DeptNo", Localizer.GetString((Enum)StockWorkOrderStringId.DepartmentNo_, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap1.AddColumn("Qty", Localizer.GetString((Enum)StockWorkOrderStringId.Quantity, new object[0]), DocumentAuditTrail.ColumnType.Quantity);
            auditColumnMap1.AddColumn("AssemblyCost", Localizer.GetString((Enum)StockWorkOrderStringId.AssemblyCost, new object[0]), DocumentAuditTrail.ColumnType.Cost);
            RemarkNameEntity remarkName = RemarkName.Create(dbSetting).GetRemarkName("WO");
            if (remarkName == null)
            {
                auditColumnMap1.AddColumn("Remark1", Localizer.GetString((Enum)StockWorkOrderStringId.Remark1, new object[0]), DocumentAuditTrail.ColumnType.String);
                auditColumnMap1.AddColumn("Remark2", Localizer.GetString((Enum)StockWorkOrderStringId.Remark2, new object[0]), DocumentAuditTrail.ColumnType.String);
                auditColumnMap1.AddColumn("Remark3", Localizer.GetString((Enum)StockWorkOrderStringId.Remark3, new object[0]), DocumentAuditTrail.ColumnType.String);
                auditColumnMap1.AddColumn("Remark4", Localizer.GetString((Enum)StockWorkOrderStringId.Remark4, new object[0]), DocumentAuditTrail.ColumnType.String);
            }
            else
            {
                auditColumnMap1.AddColumn("Remark1", remarkName.GetRemarkName(1), DocumentAuditTrail.ColumnType.String);
                auditColumnMap1.AddColumn("Remark2", remarkName.GetRemarkName(2), DocumentAuditTrail.ColumnType.String);
                auditColumnMap1.AddColumn("Remark3", remarkName.GetRemarkName(3), DocumentAuditTrail.ColumnType.String);
                auditColumnMap1.AddColumn("Remark4", remarkName.GetRemarkName(4), DocumentAuditTrail.ColumnType.String);
            }
            AuditColumnMap auditColumnMap2 = new AuditColumnMap(Localizer.GetString((Enum)StockWorkOrderStringId.Detail, new object[0]), ds.Tables["Detail"]);
            auditColumnMap2.AddColumn("ItemCode", Localizer.GetString((Enum)StockWorkOrderStringId.ItemCode, new object[0]), DocumentAuditTrail.ColumnType.String, true);
            auditColumnMap2.AddColumn("Description", Localizer.GetString((Enum)StockWorkOrderStringId.Description, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap2.AddColumn("Location", Localizer.GetString((Enum)StockWorkOrderStringId.Location, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap2.AddColumn("BatchNo", Localizer.GetString((Enum)StockWorkOrderStringId.BatchNo, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap2.AddColumn("ProjNo", Localizer.GetString((Enum)StockWorkOrderStringId.ProjectNo_, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap2.AddColumn("DeptNo", Localizer.GetString((Enum)StockWorkOrderStringId.DepartmentNo_, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap2.AddColumn("Qty", Localizer.GetString((Enum)StockWorkOrderStringId.Quantity, new object[0]), DocumentAuditTrail.ColumnType.Quantity);
            auditColumnMap2.AddColumn("ItemCost", Localizer.GetString((Enum)StockWorkOrderStringId.ItemCost, new object[0]), DocumentAuditTrail.ColumnType.Cost);
            auditColumnMap2.AddColumn("OverheadCost", Localizer.GetString((Enum)StockWorkOrderStringId.OverheadCost, new object[0]), DocumentAuditTrail.ColumnType.Cost);
            auditColumnMap2.AddColumn("Remark", Localizer.GetString((Enum)StockWorkOrderStringId.Remark, new object[0]), DocumentAuditTrail.ColumnType.String);
            auditColumnMap2.AddColumn("PrintOut", Localizer.GetString((Enum)StockWorkOrderStringId.Print, new object[0]), DocumentAuditTrail.ColumnType.Boolean);
            auditColumnMap2.AddColumn("Numbering", Localizer.GetString((Enum)StockWorkOrderStringId.Numbering, new object[0]), DocumentAuditTrail.ColumnType.String);
            DocumentAuditTrail documentAuditTrail = new DocumentAuditTrail(dbSetting, "WO");
            AuditColumnMap masterColMap1 = auditColumnMap1;
            AuditColumnMap detailColMap1 = auditColumnMap2;
            DBSetting newDBSetting = dbSetting;
            int num1 = (int)eventType;
            documentAuditTrail.PostAuditLog(masterColMap1, detailColMap1, newDBSetting, (AuditTrail.EventType)num1);
            string detail = "";
            AuditColumnMap masterColMap2 = auditColumnMap1;
            AuditColumnMap detailColMap2 = auditColumnMap2;
            int num2 = (int)eventType;
            // ISSUE: explicit reference operation
            // ISSUE: variable of a reference type
            string message = @detail;
            documentAuditTrail.BuildAuditLog(masterColMap2, detailColMap2, (AuditTrail.EventType)num2, ref message);
            long docKey = dataRow.RowState != DataRowState.Deleted ? BCE.Data.Convert.ToInt64(dataRow["DocKey"]) : BCE.Data.Convert.ToInt64(dataRow["DocKey", DataRowVersion.Original]);
            if (eventType == AuditTrail.EventType.New)
            {
                // ISSUE: variable of a boxed type
                StockWorkOrderString local = StockWorkOrderString.CreatedNewStockWorkOrder;
                object[] objArray = new object[2];
                int index1 = 0;
                string loginUserId = this.myUserAuthentication.LoginUserID;
                objArray[index1] = (object)loginUserId;
                int index2 = 1;
                string str = dataRow["DocNo"].ToString();
                objArray[index2] = (object)str;
                string @string = Localizer.GetString((Enum)local, objArray);
                Activity.Log(this.myDBSetting, "WO", docKey, 0L, @string, detail);
            }
            else if (eventType == AuditTrail.EventType.Edit)
            {
                // ISSUE: variable of a boxed type
                StockWorkOrderString local = StockWorkOrderString.UpdatedStockWorkOrder;
                object[] objArray = new object[2];
                int index1 = 0;
                string loginUserId = this.myUserAuthentication.LoginUserID;
                objArray[index1] = (object)loginUserId;
                int index2 = 1;
                string str = dataRow["DocNo", DataRowVersion.Original].ToString();
                objArray[index2] = (object)str;
                string @string = Localizer.GetString((Enum)local, objArray);
                Activity.Log(this.myDBSetting, "WO", docKey, 0L, @string, detail);
            }
            else if (eventType == AuditTrail.EventType.Delete)
            {
                // ISSUE: variable of a boxed type
                StockWorkOrderString local = StockWorkOrderString.DeletedStockWorkOrder;
                object[] objArray = new object[2];
                int index1 = 0;
                string loginUserId = this.myUserAuthentication.LoginUserID;
                objArray[index1] = (object)loginUserId;
                int index2 = 1;
                string str = dataRow["DocNo", DataRowVersion.Original].ToString();
                objArray[index2] = (object)str;
                string @string = Localizer.GetString((Enum)local, objArray);
                Activity.Log(this.myDBSetting, "WO", docKey, 0L, @string, detail);
            }
        }

        protected void UpdateStockUOMConv(StockWorkOrder doc)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT FromDocNo FROM UOMConv WHERE FromDocKey = ?";
            object[] objArray = new object[1];
            int index = 0;
            // ISSUE: variable of a boxed type
            long local = doc.DocKey;
            objArray[index] = (object)local;
            object obj = dbSetting.ExecuteScalar(cmdText, objArray);
            if (obj != null && obj.ToString() != (string)doc.DocNo)
                this.myDBSetting.ExecuteNonQuery(string.Format("UPDATE UOMConv SET FromDocNo = '{0}' WHERE FromDocKey = {1}", (object)doc.DocNo, (object)doc.DocKey), new object[0]);
        }

        public void SaveReportOption()
        {
            PersistenceUtil.SaveUserSetting((object)this.myReportOption, "StockWorkOrderReportOption.setting");
        }

        protected virtual DataSet LoadDesignReportData()
        {
            return (DataSet)null;
        }

        protected virtual DataSet LoadReportData(long docKey)
        {
            return (DataSet)null;
        }

        private DocumentReportDataSet PreparingReportDataSet(DataSet dsReportData)
        {
            DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, "Stock Work Order", "Stock Work Order Master", "Stock Work Order Detail");
            documentReportDataSet.Tables.Add(dsReportData.Tables[0].Copy());
            documentReportDataSet.Tables.Add(dsReportData.Tables[1].Copy());
            documentReportDataSet.Tables.Add(dsReportData.Tables[2].Copy());
            documentReportDataSet.Tables.Add(dsReportData.Tables[3].Copy());
            documentReportDataSet.Tables.Add(dsReportData.Tables[4].Copy());
            documentReportDataSet.Tables.Add(dsReportData.Tables[5].Copy());
            documentReportDataSet.Tables.Add(LoadRawMaterialSummary(documentReportDataSet.Tables["Detail"]));

            DataRelation relation = new DataRelation("MasterDetailRelation", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["Detail"].Columns["DocKey"], false);
            DataRelation relation2 = new DataRelation("drProduct", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["PIDetail"].Columns["DocKey"], false);
            DataRelation relation3 = new DataRelation("drBS", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["BSDetail"].Columns["DocKey"], false);
            DataRelation relation4 = new DataRelation("drOvd", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["OvdDetail"].Columns["DocKey"], false);
            DataRelation relation5 = new DataRelation("drAP", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["APDetail"].Columns["DocKey"], false);
            DataRelation relation6 = new DataRelation("drRMSummary", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["RMSummary"].Columns["DocKey"], false);

            documentReportDataSet.Relations.Add(relation);
            documentReportDataSet.Relations.Add(relation2);
            documentReportDataSet.Relations.Add(relation3);
            documentReportDataSet.Relations.Add(relation4);
            documentReportDataSet.Relations.Add(relation5);
            documentReportDataSet.Relations.Add(relation6);
            documentReportDataSet.AddDefaultTables();
            return documentReportDataSet;
        }

        public DataTable LoadRawMaterialSummary(DataTable dtDetail)
        {
            DataTable myDetailSummaryTable = new DataTable();
            myDetailSummaryTable.Columns.Add("DtlKey", typeof(Int32));
            myDetailSummaryTable.Columns.Add("DocKey", typeof(Int64));
            myDetailSummaryTable.Columns.Add("ItemCode", typeof(string));
            myDetailSummaryTable.Columns.Add("Description", typeof(string));
            myDetailSummaryTable.Columns.Add("Level", typeof(string));
            myDetailSummaryTable.Columns.Add("UOM", typeof(string));
            myDetailSummaryTable.Columns.Add("Qty", typeof(decimal));
            myDetailSummaryTable.Columns.Add("UnitCost", typeof(decimal));
            myDetailSummaryTable.Columns.Add("TotalCost", typeof(decimal));
            myDetailSummaryTable.Constraints.Add("DtlKey", myDetailSummaryTable.Columns["DtlKey"], true);
            myDetailSummaryTable.TableName = "RMSummary";
            //bool bLevel = false;

            //if(this.MasterRow["CanSync"]!=null && this.MasterRow["CanSync"] != null)
            //{
            //    bLevel = BCE.Data.Convert.TextToBoolean(this.MasterRow["CanSync"]);
            //}
            string sFieldItem = "";
            foreach (UDFColumn udfColumn in new UDFUtil(this.myDBSetting).GetUDF("Item"))
            {
                sFieldItem += udfColumn.FieldName+ "," ;
                switch (udfColumn.Type)
                {
                    case UDFType.Text:
                        myDetailSummaryTable.Columns.Add(udfColumn.FieldName, typeof(string));
                        break;
                    case UDFType.Boolean:
                        myDetailSummaryTable.Columns.Add(udfColumn.FieldName, typeof(char));
                        break;
                    case UDFType.Date:
                        myDetailSummaryTable.Columns.Add(udfColumn.FieldName, typeof(DateTime));
                        break;
                    case UDFType.Decimal:
                        myDetailSummaryTable.Columns.Add(udfColumn.FieldName, typeof(Decimal));
                        break;
                    case UDFType.Integer:
                        myDetailSummaryTable.Columns.Add(udfColumn.FieldName, typeof(Int32));
                        break;
                    case UDFType.Memo:
                        myDetailSummaryTable.Columns.Add(udfColumn.FieldName, typeof(string));
                        break;
                    case UDFType.RichText:
                        myDetailSummaryTable.Columns.Add(udfColumn.FieldName, typeof(string));
                        break;
                    case UDFType.System:
                        myDetailSummaryTable.Columns.Add(udfColumn.FieldName, typeof(string));
                        break;
                    case UDFType.ImageLink:
                        myDetailSummaryTable.Columns.Add(udfColumn.FieldName, typeof(Images));
                        break;
                }
                
               

            }
                myDetailSummaryTable.Clear();
            //int icount = 0;
            DataRow[] drDetail = dtDetail.Select("", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
            foreach (DataRow dr in drDetail)
            {
                //bool bEdit = false;
                //if (bLevel)
                {
                    DataRow[] drSummary = myDetailSummaryTable.Select("ItemCode='" + dr["ItemCode"] + "' and UOM='" + dr["UOM"] + "' and Level='" + dr["Level"] + "'");
                    if (drSummary.Length > 0)
                    {
                        drSummary[0]["Qty"] = BCE.Data.Convert.ToDecimal(drSummary[0]["Qty"]) + BCE.Data.Convert.ToDecimal(dr["Qty"]);
                        drSummary[0]["UnitCost"] = dr["UnitCost"];
                        drSummary[0]["TotalCost"] = BCE.Data.Convert.ToDecimal(dr["UnitCost"]) * BCE.Data.Convert.ToDecimal(drSummary[0]["Qty"]);
                    }
                    else
                    {
                        DataRow drNewSummary = myDetailSummaryTable.NewRow();
                        drNewSummary["DtlKey"] = dr["DtlKey"];
                        drNewSummary["DocKey"] = dr["DocKey"];
                        drNewSummary["Level"] = dr["Level"];
                        //drNewSummary["Seq"] = dr["Seq"];
                        drNewSummary["ItemCode"] = dr["ItemCode"];
                        drNewSummary["Description"] = dr["Description"];
                        drNewSummary["UOM"] = dr["UOM"];
                        drNewSummary["Qty"] = dr["Qty"];
                        drNewSummary["UnitCost"] = dr["UnitCost"];
                        drNewSummary["TotalCost"] = dr["TotalCost"];

                        DataRow drItem = myDBSetting.GetFirstDataRow("select "+ sFieldItem + "ItemCode from Item where ItemCode=?",(object)dr["ItemCode"]);

                        foreach (UDFColumn udfColumn in new UDFUtil(this.myDBSetting).GetUDF("Item"))
                        {
                            drNewSummary[udfColumn.FieldName]= drItem[udfColumn.FieldName];
                        }
                            // drNewSummary["Guid"] = (object)Guid.NewGuid();
                            myDetailSummaryTable.Rows.Add(drNewSummary);
                    }
                }
                //else
                //{
                //    DataRow[] drSummary = myDetailSummaryTable.Select("ItemCode='" + dr["ItemCode"] + "' and UOM='" + dr["UOM"] + "'");
                //    if (drSummary.Length > 0)
                //    {
                //        drSummary[0]["Qty"] = BCE.Data.Convert.ToDecimal(drSummary[0]["Qty"]) + BCE.Data.Convert.ToDecimal(dr["Qty"]);
                //        drSummary[0]["UnitCost"] = dr["UnitCost"];
                //        drSummary[0]["TotalCost"] = BCE.Data.Convert.ToDecimal(dr["UnitCost"]) * BCE.Data.Convert.ToDecimal(drSummary[0]["Qty"]);
                //    }
                //    else
                //    {
                //        DataRow drNewSummary = myDetailSummaryTable.NewRow();
                //        drNewSummary["DtlKey"] = dr["DtlKey"];
                //        drNewSummary["Level"] = 0;
                //        //drNewSummary["DocKey"] = dr["DocKey"];
                //        //drNewSummary["Seq"] = dr["Seq"];
                //        drNewSummary["ItemCode"] = dr["ItemCode"];
                //        drNewSummary["Description"] = dr["Description"];
                //        drNewSummary["UOM"] = dr["UOM"];
                //        drNewSummary["Qty"] = dr["Qty"];
                //        drNewSummary["UnitCost"] = dr["UnitCost"];
                //        drNewSummary["TotalCost"] = dr["TotalCost"];
                //        // drNewSummary["Guid"] = (object)Guid.NewGuid();
                //        myDetailSummaryTable.Rows.Add(drNewSummary);
                //    }
                //}
            }
            return myDetailSummaryTable;
            // gridCtlItemSummary.DataSource = (object)this.RMSummaryTable;
        }

        public object GetReportDesignerDataSource()
        {
            return (object)this.PreparingReportDataSet(this.LoadDesignReportData());
        }

        public object GetReportDataSource(long docKey)
        {
            return (object)this.PreparingReportDataSet(this.LoadReportData(docKey));
        }

        public object GetReportDataSource(string dockeys)
        {
            return (object)this.PreparingReportDataSet(this.LoadDocumentListingReportData(dockeys));
        }

        public void PrintReport(SearchCriteria criteria)
        {
        }

        private DocumentReportDataSet PreparingDocumentListingReportDataSet(DataSet dsReportData, string docKeys, StockWorkOrderReportingCriteria criteria, string dataName)
        {
            DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, dataName, "Stock Work Order Master", "Stock Work Order Detail");
            documentReportDataSet.Tables.Add(dsReportData.Tables[0].Copy());
            documentReportDataSet.Tables.Add(dsReportData.Tables[1].Copy());
            documentReportDataSet.Relations.Add("MasterDetailRelation", documentReportDataSet.Tables[0].Columns["DocKey"], documentReportDataSet.Tables[1].Columns["DocKey"], false);
            StockReportHelper.CreateReportOptionTable((StockReportingCriteria)criteria, (DataSet)documentReportDataSet);
            StockReportHelper.CreateGroupIDAndSortIDColumn((StockReportingCriteria)criteria, (DataSet)documentReportDataSet);
            string masterTableName = "RPA_WO";
            //documentReportDataSet.Tables.Add(ManufacturingReportHelper.GetItemAnalysisSummaryTable(this.myDBSetting, docKeys, masterTableName + "DTL"));
            //documentReportDataSet.Tables.Add(ManufacturingReportHelper.GetDateAnalysisSummaryTable(this.myDBSetting, docKeys, masterTableName, masterTableName + "DTL"));
            //documentReportDataSet.Tables.Add(ManufacturingReportHelper.GetMonthAnalysisSummaryTable(this.myDBSetting, docKeys, masterTableName, masterTableName + "DTL"));
            //documentReportDataSet.Tables.Add(ManufacturingReportHelper.GetYearAnalysisSummaryTable(this.myDBSetting, docKeys, masterTableName, masterTableName + "DTL"));
            documentReportDataSet.AddDefaultTables();
            return documentReportDataSet;
        }

        public object GetDocumentListingReportDataSource(string docKeys, StockWorkOrderReportingCriteria criteria)
        {
            return (object)this.PreparingDocumentListingReportDataSet(this.LoadDocumentListingReportData(docKeys), docKeys, criteria, "Stock Work Order Listing");
        }

        public object GetDocumentListingReportDesignerDataSource(StockWorkOrderReportingCriteria criteria)
        {
            DataSet dsReportData = this.LoadDocumentListingReportDesignerData();
            string docKeys = "";
            foreach (DataRow dataRow in (InternalDataCollectionBase)dsReportData.Tables["Master"].Rows)
                docKeys = docKeys + dataRow["Dockey"] + ", ";
            return (object)this.PreparingDocumentListingReportDataSet(dsReportData, docKeys, criteria, "Stock Work Order Listing");
        }

        protected virtual DataSet LoadDocumentListingReportData(string docKeys)
        {
            return (DataSet)null;
        }

        protected virtual DataSet LoadDocumentListingReportDesignerData()
        {
            return (DataSet)null;
        }
      
        public static EmailAndFaxInfo GetEmailAndFaxInfo(long docKey, DBSetting dbSetting)
        {
            string str1 = "Select A.Description, A.DocNo, A.DocDate From RPA_WO A WHERE A.DocKey = ?";
            DBSetting dbSetting1 = dbSetting;
            string cmdText = str1;
            object[] objArray = new object[1];
            int index = 0;
            // ISSUE: variable of a boxed type
            long local = docKey;
            objArray[index] = (object)local;
            DataRow firstDataRow = dbSetting1.GetFirstDataRow(cmdText, objArray);
            if (firstDataRow == null)
            {
                return new EmailAndFaxInfo();
            }
            else
            {
                EmailAndFaxInfo emailAndFaxInfo = new EmailAndFaxInfo();
                string str2 = string.Format("{0} {1}, dated {2}", (object)DocumentType.ToLocalizedString("WO"), (object)firstDataRow["DocNo"].ToString(), (object)GeneralSetting.GetOrCreate(dbSetting).FormatDate(BCE.Data.Convert.ToDateTime(firstDataRow["DocDate"])));
                emailAndFaxInfo.Subject = str2;
                string str3 = StringHelper.ConvertToValidFilename(string.Format("{0} {1}", (object)DocumentType.ToLocalizedString("WO"), (object)firstDataRow["DocNo"].ToString()));
                emailAndFaxInfo.AttachmentPdfFilename = str3;
                return emailAndFaxInfo;
            }
        }
    }
}
