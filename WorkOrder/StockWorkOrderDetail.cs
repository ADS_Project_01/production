﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderDetail
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Misc;
using System.Data;

namespace Production.WorkOrder
{
    public class StockWorkOrderDetail
    {
        private DataRow myRow;
        private DBSetting myDBSetting;
        private DecimalSetting myDecimalSetting;

        public DataRow Row
        {
            get
            {
                return this.myRow;
            }
        }

        public int Seq
        {
            get
            {
                return Convert.ToInt32(this.myRow["Seq"]);
            }
        }

        public DBString Numbering
        {
            get
            {
                return Convert.ToDBString(this.myRow["Numbering"]);
            }
            set
            {
                this.myRow["Numbering"] = Convert.ToDBObject(value);
            }
        }

        public DBString ItemCode
        {
            get
            {
                return Convert.ToDBString(this.myRow["ItemCode"]);
            }
            set
            {
                this.myRow["ItemCode"] = Convert.ToDBObject(value);
            }
        }


        public DBString UOM
        {
            get
            {
                return Convert.ToDBString(this.myRow["UOM"]);
            }
            set
            {
                this.myRow["UOM"] = Convert.ToDBObject(value);
            }
        }

        public DBString Description
        {
            get
            {
                return Convert.ToDBString(this.myRow["Description"]);
            }
            set
            {
                this.myRow["Description"] = Convert.ToDBObject(value);
            }
        }
        public DBString ItemType
        {
            get
            {
                return Convert.ToDBString(this.myRow["ItemType"]);
            }
            set
            {
                this.myRow["ItemType"] = Convert.ToDBObject(value);
            }
        }

        public DBString FromItemCode
        {
            get
            {
                return Convert.ToDBString(this.myRow["FromItemCode"]);
            }
            set
            {
                this.myRow["FromItemCode"] = Convert.ToDBObject(value);
            }
        }


        public DBDecimal Qty
        {
            get
            {
                return Convert.ToDBDecimal(this.myRow["Qty"]);
            }
            set
            {
                this.myRow["Qty"] = this.myDecimalSetting.RoundToQuantityDBObject(value);
            }
        }

        public DBDecimal Rate
        {
            get
            {
                return Convert.ToDBDecimal(this.myRow["Rate"]);
            }
            set
            {
                this.myRow["Rate"] = this.myDecimalSetting.RoundToQuantityDBObject(value);
            }
        }
        public DBDecimal RateUOM
        {
            get
            {
                return Convert.ToDBDecimal(this.myRow["RateUOM"]);
            }
            set
            {
                this.myRow["RateUOM"] = this.myDecimalSetting.RoundToQuantityDBObject(value);
            }
        }

        public DBDecimal ProductRatio
        {
            get
            {
                return Convert.ToDBDecimal(this.myRow["ProductRatio"]);
            }
            set
            {
                this.myRow["ProductRatio"] = this.myDecimalSetting.RoundToQuantityDBObject(value);
            }
        }


        public long DtlKey
        {
            get
            {
                return (long)Convert.ToDBInt64(this.myRow["DtlKey"]);
            }
        }



        public DBDecimal UnitCost
        {
            get
            {
                return Convert.ToDBDecimal(this.myRow["UnitCost"]);
            }
            set
            {
                this.myRow["UnitCost"] = this.myDecimalSetting.RoundToCostDBObject(value);
            }
        }

        public DBDecimal TotalCost
        {
            get
            {
                return Convert.ToDBDecimal(this.myRow["TotalCost"]);
            }
            set
            {
                this.myRow["TotalCost"] = this.myDecimalSetting.RoundToCostDBObject(value);
            }
        }

        public bool PrintOut
        {
            get
            {
                return Convert.TextToBoolean(this.myRow["PrintOut"]);
            }
            set
            {
                this.myRow["PrintOut"] = (object)Convert.BooleanToText(value);
            }
        }

        internal StockWorkOrderDetail(DBSetting dbSetting, DataRow row)
        {
            this.myRow = row;
            this.myDBSetting = dbSetting;
            this.myDecimalSetting = DecimalSetting.GetOrCreate(this.myDBSetting);
        }
    }
}
