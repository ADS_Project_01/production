﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyKeepAfterSave
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using System;

namespace Production.WorkOrder
{
  [Serializable]
  public class WorkOrderKeepAfterSave
  {
    public bool CheckKeepAfterSave;
    public int InstantInfoHeight;
    public bool MaximizeWindow;
  }
}
