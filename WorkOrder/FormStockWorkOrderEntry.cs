﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.FormStockWorkOrderEntry
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.ContextException;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.Document;
using BCE.AutoCount.DragDrop;
using BCE.AutoCount.Help;
using BCE.AutoCount.Inquiry;
using BCE.AutoCount.Inquiry.UserControls;
using BCE.AutoCount.Invoicing;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.Manufacturing.BOMOptional;
using BCE.AutoCount.RegistryID.Misc;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.AutoCount.Stock.Item;
using BCE.AutoCount.Stock.StockUOMConversion;
using BCE.AutoCount.UDF;
using BCE.AutoCount.WinForms;
using BCE.AutoCount.XtraUtils;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
using BCE.Controls;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Production.Tools.LookupEditBuilders;
namespace Production.WorkOrder
{
    public class FormStockWorkOrderEntry : XtraForm
    {
        private DBRegistry myDBReg;
        private bool myUseLookupEditToInputItemCode;
        private bool mySkipExecuteFormActivated;
        private bool myLastValidationSuccess;
        private CustomizeGridLayout myGridLayout;
        private CustomizeGridLayout myBSGridLayout;
        private CustomizeGridLayout myAPGridLayout;
        private CustomizeGridLayout myOvdGridLayout;
        private CustomizeGridLayout myPIGridLayout;
        private int myInProcessTempDocument;
        private StockHelper myStockHelper;
        private bool myHasDeactivated;
        private bool myItemCodeFromPopup;
        private bool myCanDrag;
        private RemarkName myRemarkName;
        private RemarkNameEntity myRemarkNameEntity;
        private MRUHelper myMRUHelper;
        private MRUHelper myMRUHelperRemark1;
        private MRUHelper myMRUHelperRemark2;
        private MRUHelper myMRUHelperRemark3;
        private MRUHelper myMRUHelperRemark4;
        private DataSet myLastDragDataSet;
        private StockWorkOrder myStockWorkOrder;
        private DBSetting myDBSetting;
        private UndoManager myMasterRecordUndo;
        private bool mySaveInKIV;
        private bool myIsLoadForm;
        private ScriptObject myScriptObject;
        private ItemBatchForReceiveLookupEditBuilder myMasterItemBatchLookupEditBuilder;
        private int myColLocationVisibleIndex;
        private int myColLocationVisibleIndex1;
        private int myColBatchNoVisibleIndex;
        private int myColBatchNoVisibleIndex1;
        private int myColProjNoVisibleIndex;
        private int myColDeptNoVisibleIndex;
        private UndoManager myDetailRecordUndo;
        private UndoManager myBSDetailRecordUndo;
        private UndoManager myAPDetailRecordUndo;
        private UndoManager myPIDetailRecordUndo;
        private UndoManager myOvdDetailRecordUndo;
        private DocumentCarrier myDropDocCarrier;
        private int myDisableAutoSaveCounter;
        private bool myFilterByLocation;
        private bool myIsInstantInfoShown;
        private string myLastInquiryItemCode;
        private string myLastInquiryUOM;
        private UCInquiryStock myUCInquiry;
        private FormItemSearch myFormItemSearch;
        private FormStockWorkOrderEditOption myEditOption;
        private IContainer components;
        private bool myHasUnlinkLookupEditEventHandlers;
        private PreviewButton btnPreview;
        private PrintButton btnPrint;
        private UCInquiryStock ucInquiryStock1;
        private ExternalLinkBox externalLinkBox1;
        private BCE.Controls.MemoEdit memoEdtNote;
        private Navigator navigator;
        private Bar bar1;
        private BarButtonItem barbtnCheckTransferredToStatus;
        private BarButtonItem barBtnSaveInKIVFolder;
        private BarButtonItem barbtnTransferFromSO;
        private BarButtonItem barButtonItem1;
        private BarButtonItem barItemCopyFrom;
        private BarButtonItem barItemCopySelectedDetails;
        private BarButtonItem barItemCopyTo;
        private BarButtonItem barItemCopyWholeDocument;
        private BarButtonItem barItemPasteItemDetailOnly;
        private BarButtonItem barItemPasteWholeDocument;
        private BarButtonItem iEditDescriptionMRU;
        private BarButtonItem iUndoMaster;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private BarDockControl barDockControlTop;
        private BarManager barManager1;
        private BarSubItem barSubItem1;
        private BarSubItem barSubItem2;
        private CheckEdit chkedtNextRecord;
        private DateEdit dateEdtDate;
        private LookUpEdit luEdtDocNoFormat;
        private MRUEdit mruEdtDescription;
        private PanelControl panBottom;
        private PanelControl panelView;
        private PanelControl panelControl1;
        private PanelControl panelHeader;
        private PanelControl panTop;
        private RepositoryItemButtonEdit repBtnedtFurtherDesc;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private RepositoryItemLookUpEdit repLuedtBatchNo;
        private RepositoryItemLookUpEdit repLuedtDeptNo;
        private RepositoryItemLookUpEdit repLuedtItem;
        private RepositoryItemLookUpEdit repLuedtLocation;
        private RepositoryItemLookUpEdit repLuedtProjNo;
        private RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private SimpleButton sBtnAdd;
        private SimpleButton sbtnCancel;
        private SimpleButton sbtnCancelDoc;
        private SimpleButton sbtnDelete;
        private SimpleButton sbtnEdit;
        private SimpleButton sBtnInsert;
        private SimpleButton sBtnMoveDown;
        private SimpleButton sBtnMoveUp;
        private SimpleButton sBtnRangeSetting;
        private SimpleButton sBtnRemove;
        private SimpleButton sbtnSave;
        private SimpleButton sbtnSavePreview;
        private SimpleButton sbtnSavePrint;
        private SimpleButton sBtnSearch;
        private SimpleButton sBtnSelectAllMain;
        private SimpleButton sBtnShowInstant;
        private SimpleButton sBtnUndo;
        private TextEdit textEdtRefDocNo;
        private TextEdit textEdtTotal;
        private TextEdit txtedtEstHPP;
        private TextEdit txtEdtStockAssemblyNo;
        private GridColumn colAvailableBal;
        private GridColumn colAvailableQty;
        private GridColumn colBatchNo;
        private GridColumn colDescription;
        private GridColumn colItemCode;
        private GridColumn colLocation;
        private GridColumn colOnHandBal;
        private GridColumn colOnHandQty;
        private GridColumn colReqQty;
        private GridColumn colUOM;
        private XtraTabControl tabControl1;
        private XtraTabPage tabPageExternalLink;
        private XtraTabPage tabPageDetail;
        private XtraTabPage tabPageNote;
        private XtraTabPage tabPageBS;
        private TreeListColumn tlBatchNo;
        private TreeListColumn tlDeptNo;
        private TreeListColumn tlDesc;
        private TreeListColumn tlFurtherDescription;
        private TreeListColumn tlIsBOMItem;
        private TreeListColumn tlItemCode;
        private TreeListColumn tlItemCost;
        private TreeListColumn tlLocation;
        private TreeListColumn tlNumbering;
        private TreeListColumn tlOverheadCost;
        private TreeListColumn tlPrintOut;
        private TreeListColumn tlProjNo;
        private TreeListColumn tlQty;
        private TreeListColumn tlRate;
        private TreeListColumn tlRemark;
        private TreeListColumn tlSeq;
        private TreeListColumn tlSubTotal;
        private ImageList imageList1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label9;
        private Label lblCancelled;
        private Label lblDate;
        private Label lblDescription;
        private Label lblRefDocNo;
        private Label lblStockAdjNo;
        private System.Windows.Forms.Timer timer1;
        private BarButtonItem barItemCopyAsTabDelimitedText;
        private XtraTabPage tabPageMoreHeader;
        private BarSubItem barSubItem4;
        private BarButtonItem iEditRemark1MRU;
        private BarButtonItem iEditRemark2MRU;
        private BarButtonItem iEditRemark3MRU;
        private BarButtonItem iEditRemark4MRU;
        private Label labelRemark4;
        private Label labelRemark3;
        private Label labelRemark2;
        private Label labelRemark1;
        private MRUEdit mruEdtRemark1;
        private MRUEdit mruEdtRemark2;
        private MRUEdit mruEdtRemark3;
        private MRUEdit mruEdtRemark4;
        private BarButtonItem barBtnConvertLevel;
        private System.Windows.Forms.Timer timer2;
        private XtraTabPage xtraTabPage1;
        private XtraTabPage tabPagePI;
        private XtraTabPage tabPageOvd;
        private GridControl gridCtlIBOMPI;
        private GridView gvBOMPI;
        private GridColumn colAPItemCode;
        private RepositoryItemLookUpEdit repleAPItemCode;
        private GridColumn gridColumn1;
        private GridColumn colAPCostPercent;
        private PanelControl panelControl2;
        private SimpleButton sbtnUndoPIDtl;
        private SimpleButton sbtnAddPIDtl;
        private SimpleButton sbtnDeletePIDtl;
        private GridControl gridCtlIBOMOvd;
        private GridView gvBOMOvd;
        private GridColumn colOvdOverheadCode;
        private RepositoryItemLookUpEdit repleOvdOverheadCode;
        private GridColumn colOvdDescription;
        private GridColumn colOvdAmount;
        private PanelControl panelControl3;
        private SimpleButton sbtnUndoOvdDtl;
        private SimpleButton sbtnAddOvdDtl;
        private SimpleButton sbtnDeleteOvdDtl;
        private GridControl gridCtlIBOMBS;
        private GridView gvBOMBS;
        private GridColumn colBSItemCode;
        private RepositoryItemLookUpEdit repBOMItemLookUpEdit;
        private GridColumn colBOMDesc;
        private GridColumn colBSCostPercent;
        private GridColumn ColBSCostMax;
        private RepositoryItemLookUpEdit repItemBSItemCode;
        private PanelControl panelControl4;
        private SimpleButton sbtnUndoBSDtl;
        private SimpleButton sbtnAddBSDTL;
        private SimpleButton sbtnDeleteBSDTL;
        private LookUpEdit luLocation;
        private Label lblLocation;
        private TreeList gridCtlItem;
        //private DataTable RMSummaryTable;
        //private GridView gvItem;
        private TreeListColumn colDocKey;
        private TreeListColumn colDtlKey;
        private TreeListColumn gridColumn2;
        private RepositoryItemLookUpEdit repItemLuedtItem;
        private TreeListColumn colDesc;
        private TreeListColumn gridColumn3;
        private RepositoryItemLookUpEdit repositoryItemluUOM;
        private TreeListColumn colRate;
        private TreeListColumn colQty;
        private TreeListColumn colUnitCost;
        private TreeListColumn colTotalCost;
        private DateEdit deTarget;
        private Label label3;
        private DateEdit deProduction;
        private Label label1;
        private TextEdit txtStatus;
        private LookUpEdit luMachineCode;
        private Label label5;
        private TextEdit txtMachineName;
        private SimpleButton sbtnSelectAllBS;
        private SimpleButton sbtnDownBS;
        private SimpleButton sbtnUpBS;
        private SimpleButton sBtnInsertBS;
        private SimpleButton sbtnSelectAllPI;
        private SimpleButton sbtnDownPI;
        private SimpleButton sbtnUpPI;
        private SimpleButton sbtnInsertAllPI;
        private SimpleButton sbtnSelectAllOvd;
        private SimpleButton sbtnDownOvd;
        private SimpleButton sbtnUpOvd;
        private SimpleButton sbtnInsertOvd;
        private TreeListColumn colItemSeq;
        private GridColumn colBSSeq;
        private GridColumn colAPSeq;
        private GridColumn colOvdSeq;
        private TextEdit txtProjNo;
        private LookUpEdit luProjNo;
        private Label label6;
        private Label label4;
        private TextEdit txtDebtorName;
        private LookUpEdit luDebtorCode;
        private Label label2;
        private BarButtonItem barButtonItem2;
        private GridColumn colAPUOM;
        private GridColumn colAPRate;
        private GridColumn colAPQty;
        private GridColumn colAPBOMCode;
        private GridColumn colAPStatus;
        private GridColumn colAPCOGM;
        private GridColumn COLAPFromDocType;
        private GridColumn COLAPFromDocNo;
        private GridColumn COLAPFromDocDtlKey;
        private GridColumn COLAPDebtorCode;
        private GridColumn COLAPDebtorName;
        private GridColumn COLAPStyle;
        private GridColumn COLAPLength;
        private GridColumn COLAPUOMLength;
        private GridColumn COLAPWidth;
        private GridColumn COLAPUOMWidth;
        private GridColumn COLAPSize;
        private GridColumn COLAPTickness;
        private RepositoryItemLookUpEdit repleAPUOM;
        private RepositoryItemLookUpEdit repleItemBOM;
        private SplitterControl splitterControl1;
        private TreeListColumn colDtlFromDocDtlKey;
        private TreeListColumn colFromBOMCode;
        private TreeListColumn colParentBOMCode;
        private GridColumn colVolumeUOM;
        private GridColumn colVolume;
        private GridColumn colWeightUOM;
        private GridColumn colWeight;
        private RepositoryItemLookUpEdit repitemAPDebtorCode;
        private RepositoryItemLookUpEdit repItemProductDebtorCode;
        private XtraTabControl xtraTabControlRM;
        private XtraTabPage xtraTabPage2;
        private XtraTabPage xtraTabPage8;
        private TreeList gridCtlItemSummary;
        private TreeListColumn treeListColumn4;
        private TreeListColumn treeListColumn8;
        private TreeListColumn treeListColumn9;
        private TreeListColumn treeListColumn10;
        private TreeListColumn treeListColumn1;
        private TreeListColumn treeListColumn2;
        private TreeListColumn colQty2;
        private TreeListColumn colBOMCode;
        private TreeListColumn treeListColumn5;
        private SimpleButton btnCancelDetail;
        private PanelControl panelControl5;
        private CheckEdit ceLevel;
        private TreeListColumn colLevelSum;
        private GridColumn gridColumn5;
        private GridColumn gridColumn4;
        private TreeListColumn colProductRatio;
        private TreeListColumn colRateUOM;
        private GridColumn colNumbering;
        private XtraTabPage tabPageAP;
        private GridControl gridCtlIBOMAP;
        private GridView gvBOMAP;
        private GridColumn gridColumn6;
        private GridColumn gridColumn7;
        private GridColumn gridColumn8;
        private GridColumn gridColumn9;
        private PanelControl panelControl6;
        private SimpleButton sbtnSelectAllAP;
        private SimpleButton sbtnDownAP;
        private SimpleButton sbtnUPAP;
        private SimpleButton sbtnInsertAllAP;
        private SimpleButton sbtnUndoAPDtl;
        private SimpleButton sbtnAddAPDtl;
        private SimpleButton sbtnDeleteAPDtl;
        private RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private GridColumn colFromDocDtlKey;
        private GridColumn gridColumn11;
        private GridColumn gridColumn12;
        private GridColumn gridColumn10;
        private TreeListColumn treeListColumn3;
        private TreeListColumn treeListColumn6;
        private Production.Tools.LookupEditBuilders.ItemBOMLookupEditBuilder itembomlookup;
        public FormStockWorkOrderEntry(StockWorkOrder stockWorkOrder)
          : this(stockWorkOrder, new FormStockWorkOrderEditOption())
        {
        }

        public FormStockWorkOrderEntry(StockWorkOrder stockWorkOrder, FormStockWorkOrderEditOption editOption)
        {
            if (stockWorkOrder == null)
                throw new ArgumentNullException("stockWorkOrder");
            else if (editOption == null)
            {
                throw new ArgumentNullException("editOption");
            }
            else
            {
                this.myEditOption = editOption;
                this.InitializeComponent();
                this.myDBSetting = stockWorkOrder.Command.DBSetting;
                this.myDBReg = stockWorkOrder.Command.DBReg;
                this.myScriptObject = ScriptManager.CreateObject(this.myDBSetting, "StockWorkOrderEditForm");
                this.SetUseLookupEditToInputItemCode();
                this.myStockHelper = StockHelper.Create(this.myDBSetting);
                this.btnPreview.ReportType = "Stock Work Order Document";
                this.btnPreview.SetDBSetting(this.myDBSetting);
                this.btnPrint.ReportType = "Stock Work Order Document";
                this.btnPrint.SetDBSetting(this.myDBSetting);
                this.InitRemark();
                this.InitMRUHelper();
                this.SetupLookupEdit();
                this.memoEdtNote.SetDBSetting(this.myDBSetting);
                WorkOrderKeepAfterSave assemblyKeepAfterSave = (WorkOrderKeepAfterSave)null;
                try
                {
                    assemblyKeepAfterSave = (WorkOrderKeepAfterSave)PersistenceUtil.LoadUserSetting("StockWorkOrderCondition.setting");
                }
                catch
                {
                    
                }
                if (assemblyKeepAfterSave != null)
                {
                    this.chkedtNextRecord.Checked = assemblyKeepAfterSave.CheckKeepAfterSave;
                    if (this.sBtnShowInstant.Text == BCE.Localization.Localizer.GetString(StockWorkOrderStringId.Code_ShowFooter, new object[0]))
                        this.panBottom.Height = assemblyKeepAfterSave.InstantInfoHeight;
                    if (assemblyKeepAfterSave.MaximizeWindow)
                        this.WindowState = FormWindowState.Maximized;
                }
                //this.myGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gvItem);
                new CustomizeTreeListLayout(this.myDBSetting, this.Name+ "RMDtl", this.gridCtlItem).PrintTreeList = false;


                this.myBSGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name + "BS1", this.gvBOMBS);

                this.myPIGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name + "PI1", this.gvBOMPI);

                this.myAPGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name + "AP1", this.gvBOMAP);

                this.myOvdGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name + "Ovd1", this.gvBOMOvd);

                BCE.AutoCount.Help.HelpProvider.SetHelpTopic(new System.Windows.Forms.HelpProvider(), (Control)this, "Stock_Work_Order.htm");
                FormStockWorkOrderEntry.FormInitializeEventArgs initializeEventArgs1 = new FormStockWorkOrderEntry.FormInitializeEventArgs(this, stockWorkOrder);
                ScriptObject scriptObject1 = this.myScriptObject;
                string name1 = "OnFormInitialize";
                System.Type[] types1 = new System.Type[1];
                int index1 = 0;
                System.Type type1 = initializeEventArgs1.GetType();
                types1[index1] = type1;
                object[] objArray1 = new object[1];
                int index2 = 0;
                //FormStockWorkOrderEntry.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
                //objArray1[index2] = (object)initializeEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray1);
                //ScriptObject scriptObject2 = stockWorkOrder.ScriptObject;
                //string name2 = "OnFormInitialize";
                //System.Type[] types2 = new System.Type[1];
                //int index3 = 0;
                //System.Type type2 = initializeEventArgs1.GetType();
                //types2[index3] = type2;
                //object[] objArray2 = new object[1];
                //int index4 = 0;
                //FormStockWorkOrderEntry.FormInitializeEventArgs initializeEventArgs3 = initializeEventArgs1;
                //objArray2[index4] = (object)initializeEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray2);
                this.SetStockWorkOrder(stockWorkOrder);
                this.LogActivity(this.myDBSetting, stockWorkOrder);
            }
        }

        [StartThreadForm]
        public void StartForm(StockWorkOrder stockWorkOrder)
        {
            this.StartForm(stockWorkOrder, new FormStockWorkOrderEditOption());
        }

        [StartThreadForm]
        public void StartForm(StockWorkOrder stockWorkOrder, FormStockWorkOrderEditOption editOption)
        {
            this.myEditOption = editOption;
            this.myDBSetting = stockWorkOrder.Command.DBSetting;
            this.myDBReg = stockWorkOrder.Command.DBReg;
            this.SetUseLookupEditToInputItemCode();
            this.SetStockWorkOrder(stockWorkOrder);
            this.LogActivity(this.myDBSetting, stockWorkOrder);
        }

        private void SetUseLookupEditToInputItemCode()
        {
            this.myUseLookupEditToInputItemCode = this.myDBReg.GetBoolean((IRegistryID)new UseLookupEditToInputItemCode());
            if (this.myUseLookupEditToInputItemCode)
            {
                if (this.repLuedtItem.Columns.Count == 0)
                    DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repLuedtItem, this.myDBSetting);
                this.tlItemCode.ColumnEdit = (RepositoryItem)this.repLuedtItem;
            }
            else
                this.tlItemCode.ColumnEdit = (RepositoryItem)null;
        }

        private void LogActivity(DBSetting dbSetting, StockWorkOrder stockWorkOrder)
        {
            if (stockWorkOrder.Action == StockWorkOrderAction.Edit)
            {
                DBSetting dbSetting1 = dbSetting;
                string docType = "WO";
                long docKey = stockWorkOrder.DocKey;
                long eventKey = 0L;
                // ISSUE: variable of a boxed type
                StockWorkOrderString local = StockWorkOrderString.ViewStockWorkOrderInEditMode;
                object[] objArray = new object[2];
                int index1 = 0;
                string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
                objArray[index1] = (object)loginUserId;
                int index2 = 1;
                string str = stockWorkOrder.DocNo.ToString();
                objArray[index2] = (object)str;
                string @string = BCE.Localization.Localizer.GetString(local, objArray);
                string detail = "";
                Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
            }
            else if (stockWorkOrder.Action == StockWorkOrderAction.View)
            {
                DBSetting dbSetting1 = dbSetting;
                string docType = "WO";
                long docKey = stockWorkOrder.DocKey;
                long eventKey = 0L;
                // ISSUE: variable of a boxed type
                StockWorkOrderString local = StockWorkOrderString.ViewStockWorkOrder;
                object[] objArray = new object[2];
                int index1 = 0;
                string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
                objArray[index1] = (object)loginUserId;
                int index2 = 1;
                string str = stockWorkOrder.DocNo.ToString();
                objArray[index2] = (object)str;
                string @string = BCE.Localization.Localizer.GetString(local, objArray);
                string detail = "";
                Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
            }
        }
       
        private void SetStockWorkOrder(StockWorkOrder newStockWorkOrder)
        {
            if (this.myStockWorkOrder != newStockWorkOrder)
            {
                this.myStockWorkOrder = newStockWorkOrder;
                this.myStockWorkOrder.EnableAutoLoadItemDetail = true;
                this.BindingMasterData();
                this.gridCtlItem.DataSource = (object)this.myStockWorkOrder.DataTableDetail;
                this.gridCtlItemSummary.DataSource = (object)this.myStockWorkOrder.DataTableDetailSummary;
                //RMSummaryTable = this.myStockWorkOrder.DataTableDetail.Copy();
               // ProcessRawMaterialSummary();
                this.gridCtlIBOMBS.DataSource = (object)this.myStockWorkOrder.BSDetailTable;
                this.gridCtlIBOMPI.DataSource = (object)this.myStockWorkOrder.PIDetailTable;
                this.gridCtlIBOMOvd.DataSource = (object)this.myStockWorkOrder.OvdDetailTable;
                this.gridCtlIBOMAP.DataSource = (object)this.myStockWorkOrder.APDetailTable;
                // this.gvItem.Columns["Seq"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                this.gvBOMBS.Columns["Seq"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                this.gvBOMPI.Columns["Seq"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                this.gvBOMOvd.Columns["Seq"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                this.gvBOMAP.Columns["Seq"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                this.chkedtNextRecord.Visible = this.myStockWorkOrder.Action == StockWorkOrderAction.New && this.myEditOption.ShowProceedNewStockAssemblyOrder;
                this.SetControlState();
                this.FooterHeightAdjustment();
                this.RefreshLookupEdit();
                this.InitUndoManager();
                this.InitDelegate();
                if (this.myStockWorkOrder.DetailCount > 0)
                    this.myStockHelper.GetBaseUOM(this.myStockWorkOrder.DataTableDetail.Rows[0]["ItemCode"].ToString());
                //UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
                //TreeList treeList = this.gvBOMBS;
                string tableName1 = "RPA_WODTL";
                // udfUtil.SetupTreeListControl(treeList, tableName1);
                XtraTabControl tabControl = this.tabControl1;
                string tableName2 = "RPA_WO";
                DataTable dataTableMaster = this.myStockWorkOrder.DataTableMaster;
                //udfUtil.SetupTabControl(tabControl, tableName2, (object)dataTableMaster);
                this.splitterControl1.Visible = this.sBtnShowInstant.Text == BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_ShowFooter, new object[0]);
                this.mySaveInKIV = false;
                this.timer1.Interval = TempDocumentSetting.Default.AutoSaveSeconds * 1000;
                this.timer1.Enabled = true;
                this.Tag = (object)new FormStockWorkOrderEntry.MyFilterOption()
                {
                    Document = this.myStockWorkOrder
                };
                this.FormInitialize();

            }
        }

        private void InitDelegate()
        {
            this.myStockWorkOrder.PauseUndoEvent += new PauseUndoDelegate(this.PauseUndo);
            this.myStockWorkOrder.ResumeUndoEvent += new ResumeUndoDelegate(this.ResumeUndo);
        }

        private void UnlinkDelegate()
        {
            this.myStockWorkOrder.PauseUndoEvent -= new PauseUndoDelegate(this.PauseUndo);
            this.myStockWorkOrder.ResumeUndoEvent -= new ResumeUndoDelegate(this.ResumeUndo);
        }

        private void FormInitialize()
        {
            if (this.myStockWorkOrder != null)
            {
                //BCE.AutoCount.Manufacturing.StockWorkOrder.FormInitializeEventArgs initializeEventArgs1 = new BCE.AutoCount.Manufacturing.StockWorkOrder.FormInitializeEventArgs(this.myStockWorkOrder, this.panelHeader, this.gvBOMBS, this.tabControl1);
                //ScriptObject scriptObject = this.myStockWorkOrder.ScriptObject;
                //string name = "OnFormInitialize";
                //System.Type[] types = new System.Type[2];
                //int index1 = 0;
                //System.Type type1 = typeof (object);
                //types[index1] = type1;
                //int index2 = 1;
                //type2 = initializeEventArgs1.GetType();
                //types[index2] = type2;
                //object[] objArray = new object[2];
                //int index3 = 0;
                //FormStockWorkOrderEntry assemblyOrderEntry = this;
                //objArray[index3] = (object) assemblyOrderEntry;
                //int index4 = 1;
                //BCE.AutoCount.Manufacturing.StockWorkOrder.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
                //objArray[index4] = (object) initializeEventArgs2;
                //scriptObject.RunMethod(name, types, objArray);
            }
        }

        private void CreateUCInquiry()
        {
            this.myUCInquiry = new UCInquiryStock();
            this.myUCInquiry.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this.myUCInquiry.Location = new Point(0, 0);
            this.myUCInquiry.Name = "myUCInquiry";
            this.myUCInquiry.Size = new Size(this.label11.Left - 2, this.panBottom.Height);
            this.myUCInquiry.TabIndex = 6;
            this.myUCInquiry.TabStop = false;
            this.myUCInquiry.Visible = false;
            this.panBottom.Controls.Add((Control)this.myUCInquiry);
            this.myUCInquiry.Initialize(this.myDBSetting, "", "", "", "", InquiryType.Stock);
           // this.ReloadInquiryUserControl();
        }

        private void ReloadInquiryUserControl()
        {
            if (this.myUCInquiry != null && this.myIsInstantInfoShown)
                this.myUCInquiry.Reload(this.myLastInquiryItemCode, this.myLastInquiryUOM, "", "");
        }
        //private bool EndGridEdit()
        //{
        //    if (this.myStockWorkOrder == null)
        //        return false;
        //    else if (this.myStockWorkOrder.Action == StockWorkOrderAction.View || this.gridCtlItem.FocusedView == null)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        BaseView focusedView = this.gridCtlItem.FocusedView;
        //        try
        //        {
        //            if (focusedView.IsEditing)
        //            {
        //                if (!focusedView.ValidateEditor())
        //                    return false;
        //                else
        //                    focusedView.CloseEditor();
        //            }
        //            return focusedView.UpdateCurrentRow();
        //        }
        //        catch
        //        {
        //            return false;
        //        }
        //    }
        //}
        //private void ReloadInquiryUserControl()
        //{
        //    if (this.myUCInquiry != null && this.myIsInstantInfoShown && this.EndGridEdit())
        //    {
        //        //GridView gridView = this.gvDetail.GetDetailView(this.gvDetail.FocusedRowHandle, 0) as GridView;
        //        DataRow dataRow = this.gvItem.GetDataRow( this.gridCtlItem.FocusedRowHandle) ;
        //        if (dataRow != null)
        //            this.myUCInquiry.Reload(dataRow["ItemCode"].ToString(), dataRow["UOM"].ToString(),"","");
        //    }
        //}
        private void BindingMasterData()
        {
            ControlsHelper.ClearDataBinding((IEnumerable)this.Controls);
            this.txtEdtStockAssemblyNo.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "DocNo"));
            this.textEdtRefDocNo.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "RefDocNo"));
            //this.textEdtExpectedCompletedDate.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "ExpectedCompletedDate"));
            this.mruEdtDescription.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "Description"));
            this.memoEdtNote.DataBindings.Add(new Binding("rtf", (object)this.myStockWorkOrder.DataTableMaster, "Note"));

            //this.cbDocType.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "DocType"));
          //  this.txtStatus.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "Status"));

            object obj = myDBSetting.ExecuteScalar("select dbo.F_RPA_WOStatus (?)", (object)this.myStockWorkOrder.DocKey);
            if (obj != null && obj != DBNull.Value)
                this.txtStatus.Text = obj.ToString();
            else
            {
                this.txtStatus.Text = WorkOrderStatusOptions.Planned.ToString();
            }
            //this.txtedtAssemblyCost.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "AssemblyCost"));
            this.luDebtorCode.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "DebtorCode"));
           // this.txtedtEstHPP.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "EstHPP"));
            this.txtDebtorName.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "DebtorName"));
            this.dateEdtDate.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "DocDate"));
            //this.luProject.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "ProjNo"));
            this.deProduction.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "ProductionStartDate"));
            this.deTarget.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "ProductionEndDate"));
            this.luMachineCode.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "MachineCode"));
            this.luProjNo.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "ProjNo"));
            //this.luBOM.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "BOMCode"));
            // this.luDeptNo.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "DeptNo"));
            // this.luBatchNo.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "BatchNo"));
            this.luLocation.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "Location"));
            this.externalLinkBox1.DataBindings.Add(new Binding("Links", (object)this.myStockWorkOrder.DataTableMaster, "ExternalLink"));
            this.mruEdtRemark1.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "Remark1"));
            this.mruEdtRemark2.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "Remark2"));
            this.mruEdtRemark3.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "Remark3"));
            this.mruEdtRemark4.DataBindings.Add(new Binding("EditValue", (object)this.myStockWorkOrder.DataTableMaster, "Remark4"));
            //this.txtBOMName.EditValue = "";
            BCE.Controls.Utils.Bind((Control)this.ceLevel, (object)this.myStockWorkOrder.DataTableMaster, "CanSync");

            this.txtProjNo.EditValue = "";
            this.txtMachineName.EditValue = "";
            this.UpdateBOMItemDescription();
            this.UpdateProductItemDescription();
            this.UpdateMachineDescription();
            this.UpdateDebtorDescription();
            this.UpdateProjectDescription();
        }

        private void InitRemark()
        {
            this.myRemarkName = RemarkName.Create(this.myDBSetting);
            this.myRemarkNameEntity = this.myRemarkName.GetRemarkName("AS");
        }

        private void SetupRemark()
        {
            this.labelRemark1.Text = this.myRemarkNameEntity.GetRemarkName(1);
            this.labelRemark2.Text = this.myRemarkNameEntity.GetRemarkName(2);
            this.labelRemark3.Text = this.myRemarkNameEntity.GetRemarkName(3);
            this.labelRemark4.Text = this.myRemarkNameEntity.GetRemarkName(4);
            if (!this.myRemarkNameEntity.IsRemarkSupportMRU(1))
                this.iEditRemark1MRU.Enabled = false;
            if (!this.myRemarkNameEntity.IsRemarkSupportMRU(2))
                this.iEditRemark2MRU.Enabled = false;
            if (!this.myRemarkNameEntity.IsRemarkSupportMRU(3))
                this.iEditRemark3MRU.Enabled = false;
            if (!this.myRemarkNameEntity.IsRemarkSupportMRU(4))
                this.iEditRemark4MRU.Enabled = false;
        }

        private void InitMRUHelper()
        {
            this.myMRUHelper = MRUHelper.Create(this.myDBSetting, 13, this.mruEdtDescription.Properties);
            this.myMRUHelperRemark1 = MRUHelper.Create(this.myDBSetting, 46, this.mruEdtRemark1.Properties);
            this.myMRUHelperRemark2 = MRUHelper.Create(this.myDBSetting, 47, this.mruEdtRemark2.Properties);
            this.myMRUHelperRemark3 = MRUHelper.Create(this.myDBSetting, 48, this.mruEdtRemark3.Properties);
            this.myMRUHelperRemark4 = MRUHelper.Create(this.myDBSetting, 49, this.mruEdtRemark4.Properties);
        }

        private void InitUndoManager()
        {
            DataTable dataTableMaster = this.myStockWorkOrder.DataTableMaster;
            DataTable dataTableDetail = this.myStockWorkOrder.DataTableDetail;
            DataTable dataTablePI = this.myStockWorkOrder.PIDetailTable;
            DataTable dataTableBS = this.myStockWorkOrder.BSDetailTable;
            DataTable dataTableAP = this.myStockWorkOrder.APDetailTable;
            DataTable dataTableOvd = this.myStockWorkOrder.OvdDetailTable;

            this.myMasterRecordUndo = new UndoManager(dataTableMaster, true);
            this.myMasterRecordUndo.ExcludeFields.Add((object)"Total");
            this.myMasterRecordUndo.ExcludeFields.Add((object)"NetTotal");
            this.myMasterRecordUndo.CanUndoChanged += new EventHandler(this.myMasterRecordUndo_CanUndoChanged);
            this.iUndoMaster.Enabled = this.myMasterRecordUndo.CanUndo;

            this.myPIDetailRecordUndo = new UndoManager(dataTablePI, false);
            this.myPIDetailRecordUndo.CanUndoChanged += new EventHandler(this.myPIDetailRecordUndo_CanUndoChanged);

            this.myDetailRecordUndo = new UndoManager(dataTableDetail, false, new FillRowValueDelegate(this.CalcSubTotal));
            //this.myDetailRecordUndo.ExcludeFields.Add((object)"TotalCost");
            this.myDetailRecordUndo.CanUndoChanged += new EventHandler(this.myDetailRecordUndo_CanUndoChanged);
            this.sBtnUndo.Enabled = this.myDetailRecordUndo.CanUndo;
            this.myBSDetailRecordUndo = new UndoManager(dataTableBS, false);
            //this.myBSDetailRecordUndo.ExcludeFields.Add((object)"SubTotal");
            this.myBSDetailRecordUndo.CanUndoChanged += new EventHandler(this.myBSDetailRecordUndo_CanUndoChanged);
            this.sbtnUndoBSDtl.Enabled = this.myBSDetailRecordUndo.CanUndo;
            this.myAPDetailRecordUndo = new UndoManager(dataTableAP, false);
            //this.myAPDetailRecordUndo.ExcludeFields.Add((object)"SubTotal");
            this.myAPDetailRecordUndo.CanUndoChanged += new EventHandler(this.myAPDetailRecordUndo_CanUndoChanged);
            this.sbtnUndoPIDtl.Enabled = this.myAPDetailRecordUndo.CanUndo;
            this.myOvdDetailRecordUndo = new UndoManager(dataTableOvd, false);
            //this.myOvdDetailRecordUndo.ExcludeFields.Add((object)"SubTotal");
            this.myOvdDetailRecordUndo.CanUndoChanged += new EventHandler(this.myOvdDetailRecordUndo_CanUndoChanged);
            this.sbtnUndoOvdDtl.Enabled = this.myOvdDetailRecordUndo.CanUndo;
            this.sBtnUndo.Enabled = this.myDetailRecordUndo.CanUndo;
            this.myMasterRecordUndo.SaveState();
            this.myMasterRecordUndo.KeepCurrentChanges();
            this.myDetailRecordUndo.SaveState();
            this.myDetailRecordUndo.KeepCurrentChanges();

            this.myBSDetailRecordUndo.SaveState();
            this.myBSDetailRecordUndo.KeepCurrentChanges();

            this.myOvdDetailRecordUndo.SaveState();
            this.myOvdDetailRecordUndo.KeepCurrentChanges();

            this.myPIDetailRecordUndo.SaveState();
            this.myPIDetailRecordUndo.KeepCurrentChanges();

            this.myAPDetailRecordUndo.SaveState();
            this.myAPDetailRecordUndo.KeepCurrentChanges();
        }

        private void myMasterRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.iUndoMaster.Enabled = this.myMasterRecordUndo.CanUndo;
        }

        private void myDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sBtnUndo.Enabled = this.myDetailRecordUndo.CanUndo;
        }
        private void myBSDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndoBSDtl.Enabled = this.myBSDetailRecordUndo.CanUndo;
        }

        private void myPIDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndoPIDtl.Enabled = this.myPIDetailRecordUndo.CanUndo;
        }

        private void myAPDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndoAPDtl.Enabled = this.myAPDetailRecordUndo.CanUndo;
        }

        private void myOvdDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndoOvdDtl.Enabled = this.myOvdDetailRecordUndo.CanUndo;
        }
        private void PauseUndo()
        {
            if (this.myMasterRecordUndo != null)
                this.myMasterRecordUndo.PauseCapture();
            if (this.myDetailRecordUndo != null)
                this.myDetailRecordUndo.PauseCapture();

            if (this.myBSDetailRecordUndo != null)
                this.myBSDetailRecordUndo.PauseCapture();

            if (this.myPIDetailRecordUndo != null)
                this.myPIDetailRecordUndo.PauseCapture();

            if (this.myAPDetailRecordUndo != null)
                this.myAPDetailRecordUndo.PauseCapture();

            if (this.myOvdDetailRecordUndo != null)
                this.myOvdDetailRecordUndo.PauseCapture();
            this.DisableAutoSave();
            //  this.gvBOMBS.BeginUpdate();
        }

        private void ResumeUndo(bool captureChanges)
        {
            if (this.myMasterRecordUndo != null)
            {
                this.myMasterRecordUndo.ResumeCapture();
                if (!this.myMasterRecordUndo.IsPauseCapture())
                    this.myMasterRecordUndo.SaveState();
                if (!captureChanges)
                    this.myMasterRecordUndo.KeepCurrentChanges();
            }
            if (this.myDetailRecordUndo != null)
            {
                this.myDetailRecordUndo.ResumeCapture();
                if (!this.myDetailRecordUndo.IsPauseCapture())
                    this.myDetailRecordUndo.SaveState();
                if (!captureChanges)
                    this.myDetailRecordUndo.KeepCurrentChanges();
            }

            if (this.myBSDetailRecordUndo != null)
            {
                this.myBSDetailRecordUndo.ResumeCapture();
                if (!this.myBSDetailRecordUndo.IsPauseCapture())
                    this.myBSDetailRecordUndo.SaveState();
                if (!captureChanges)
                    this.myBSDetailRecordUndo.KeepCurrentChanges();
            }

            if (this.myPIDetailRecordUndo != null)
            {
                this.myPIDetailRecordUndo.ResumeCapture();
                if (!this.myPIDetailRecordUndo.IsPauseCapture())
                    this.myPIDetailRecordUndo.SaveState();
                if (!captureChanges)
                    this.myPIDetailRecordUndo.KeepCurrentChanges();
            }

            if (this.myOvdDetailRecordUndo != null)
            {
                this.myOvdDetailRecordUndo.ResumeCapture();
                if (!this.myOvdDetailRecordUndo.IsPauseCapture())
                    this.myOvdDetailRecordUndo.SaveState();
                if (!captureChanges)
                    this.myOvdDetailRecordUndo.KeepCurrentChanges();
            }

            if (this.myAPDetailRecordUndo != null)
            {
                this.myAPDetailRecordUndo.ResumeCapture();
                if (!this.myAPDetailRecordUndo.IsPauseCapture())
                    this.myAPDetailRecordUndo.SaveState();
                if (!captureChanges)
                    this.myAPDetailRecordUndo.KeepCurrentChanges();
            }

            this.EnableAutoSave();
            //this.gvBOMBS.EndUpdate();
        }

        private void DisableAutoSave()
        {
            this.myDisableAutoSaveCounter = this.myDisableAutoSaveCounter + 1;
        }

        private void EnableAutoSave()
        {
            if (this.myDisableAutoSaveCounter > 0)
                this.myDisableAutoSaveCounter = this.myDisableAutoSaveCounter - 1;
        }

        private bool IsAutoSaveDisabled()
        {
            return this.myDisableAutoSaveCounter > 0;
        }

        private void SetDetailButtonState()
        {
            bool flag1 = this.myStockWorkOrder.Action == StockWorkOrderAction.View;
            bool flag2 = this.gridCtlItem.Nodes.Count > 0;
            this.sBtnRemove.Enabled = !flag1 & flag2;
            this.sBtnMoveUp.Enabled = !flag1 & flag2;
            this.sBtnMoveDown.Enabled = !flag1 & flag2;
            this.sBtnInsert.Enabled = !flag1 & flag2;
            this.sBtnSelectAllMain.Enabled = !flag1 & flag2;
        }

        private void SetBSDetailButtonState()
        {
            bool flag1 = this.myStockWorkOrder.Action == StockWorkOrderAction.View;
            bool flag2 = this.gvBOMBS.RowCount > 0;
            this.sbtnDeleteBSDTL.Enabled = !flag1 & flag2;
            this.sbtnUpBS.Enabled = !flag1 & flag2;
            this.sbtnDownBS.Enabled = !flag1 & flag2;
            this.sBtnInsertBS.Enabled = !flag1 & flag2;
            this.sbtnSelectAllBS.Enabled = !flag1 & flag2;
        }

        private void SetPIDetailButtonState()
        {
            bool flag1 = this.myStockWorkOrder.Action == StockWorkOrderAction.View;
            bool flag2 = this.gvBOMPI.RowCount > 0;
            this.sbtnDeletePIDtl.Enabled = !flag1 & flag2;
            this.sbtnUpPI.Enabled = !flag1 & flag2;
            this.sbtnDownPI.Enabled = !flag1 & flag2;
            this.sbtnInsertAllPI.Enabled = !flag1 & flag2;
            this.sbtnSelectAllPI.Enabled = !flag1 & flag2;
        }

        private void SetAPDetailButtonState()
        {
            bool flag1 = this.myStockWorkOrder.Action == StockWorkOrderAction.View;
            bool flag2 = this.gvBOMAP.RowCount > 0;
            this.sbtnDeleteAPDtl.Enabled = !flag1 & flag2;
            this.sbtnUPAP.Enabled = !flag1 & flag2;
            this.sbtnDownAP.Enabled = !flag1 & flag2;
            this.sbtnInsertAllAP.Enabled = !flag1 & flag2;
            this.sbtnSelectAllAP.Enabled = !flag1 & flag2;
        }

        private void SetOvdDetailButtonState()
        {
            bool flag1 = this.myStockWorkOrder.Action == StockWorkOrderAction.View;
            bool flag2 = this.gvBOMOvd.RowCount > 0;
            this.sbtnDeleteOvdDtl.Enabled = !flag1 & flag2;
            this.sbtnUpOvd.Enabled = !flag1 & flag2;
            this.sbtnDownOvd.Enabled = !flag1 & flag2;
            this.sbtnInsertOvd.Enabled = !flag1 & flag2;
            this.sbtnSelectAllOvd.Enabled = !flag1 & flag2;
        }
        private void AdjustBottomButtons()
        {
            Control[] controlArray = new Control[9];
            int index1 = 0;
            SimpleButton simpleButton1 = this.sbtnCancel;
            controlArray[index1] = (Control)simpleButton1;
            int index2 = 1;
            SimpleButton simpleButton2 = this.sbtnSavePrint;
            controlArray[index2] = (Control)simpleButton2;
            int index3 = 2;
            SimpleButton simpleButton3 = this.sbtnSavePreview;
            controlArray[index3] = (Control)simpleButton3;
            int index4 = 3;
            SimpleButton simpleButton4 = this.sbtnSave;
            controlArray[index4] = (Control)simpleButton4;
            int index5 = 4;
            SimpleButton simpleButton5 = this.sbtnDelete;
            controlArray[index5] = (Control)simpleButton5;
            int index6 = 5;
            SimpleButton simpleButton6 = this.sbtnCancelDoc;
            controlArray[index6] = (Control)simpleButton6;
            int index7 = 6;
            SimpleButton simpleButton7 = this.sbtnEdit;
            controlArray[index7] = (Control)simpleButton7;
            int index8 = 7;
            PrintButton printButton = this.btnPrint;
            controlArray[index8] = (Control)printButton;
            int index9 = 8;
            PreviewButton previewButton = this.btnPreview;
            controlArray[index9] = (Control)previewButton;
            int num = this.panelView.Width;
            foreach (Control control in controlArray)
            {
                if (control.Visible)
                {
                    num = num - control.Width - 4;
                    control.Left = num;
                }
            }
        }
        private void SetControlState()
        {
            bool bReadOnly2 = false;
            bool bReadOnly = this.myStockWorkOrder.Action == StockWorkOrderAction.View;
            bReadOnly2 = bReadOnly;
            if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
            {
               // bReadOnly = (WorkOrderDocTypeOptions)cbDocType.SelectedIndex == WorkOrderDocTypeOptions.Internal && txtStatus.Text == WorkOrderStatusOptions.Closed.ToString();
                bReadOnly2 = !bReadOnly;
            }
            BCE.Controls.Utils.SetReadOnly((IEnumerable)this.Controls, bReadOnly);
            this.navigator.Visible = bReadOnly;
            this.sbtnSavePreview.Enabled = this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DOC_REPORT_PREVIEW");
            this.sbtnSavePrint.Enabled = this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DOC_REPORT_PRINT");
            this.sBtnAdd.Enabled = !bReadOnly;
            this.sbtnAddBSDTL.Enabled = !bReadOnly;
            this.sbtnAddBSDTL.Enabled = !bReadOnly;
            this.sbtnAddOvdDtl.Enabled = !bReadOnly;
           // btnCancelDetail.Enabled = !bReadOnly;
            sbtnAddPIDtl.Enabled = !bReadOnly;
            //sbtnDeleteAPDtl.Enabled = !bReadOnly;
            //sbtnInsertAllAP.Enabled = !bReadOnly;
            this.txtMachineName.Properties.ReadOnly = true;
            this.txtDebtorName.Properties.ReadOnly = true;
            this.txtProjNo.Properties.ReadOnly = true;
            this.txtStatus.Properties.ReadOnly = true;
            //this.txtActHPP.Properties.ReadOnly = true;
            // this.sBtnSearch.Enabled = !bReadOnly;
            //this.sBtnRangeSetting.Enabled = !bReadOnly;//> 0&& this.gvBOMBS.Selection.Count > 0
            this.barbtnTransferFromSO.Enabled = false;// !bReadOnly;
            this.gvBOMBS.OptionsBehavior.Editable = !bReadOnly;
            //this.barBtnConvertLevel.Enabled = !bReadOnly;
            this.SetDetailButtonState();
            this.SetBSDetailButtonState();
            this.SetPIDetailButtonState();
            this.SetAPDetailButtonState();
            this.SetOvdDetailButtonState();
            this.btnPreview.Visible = bReadOnly;
            this.btnPrint.Visible = bReadOnly;
            this.sbtnEdit.Visible = bReadOnly;
            if (this.myStockWorkOrder.Action == StockWorkOrderAction.View)
            {
                this.sbtnEdit.Visible = bReadOnly2;
            }
            if (ceLevel.Checked)
            {
                // gridCtlItemSummary.
                colLevelSum.Visible = true;
                colLevelSum.SortIndex = 0;
                colLevelSum.VisibleIndex = 0;
            }
            else
            {
                colLevelSum.Visible = false;
                colLevelSum.SortIndex = -1;
            }

            this.sbtnCancelDoc.Visible = bReadOnly;
            this.btnCancelDetail.Visible = bReadOnly;
            DataRow drProduct = gvBOMPI.GetDataRow(gvBOMPI.FocusedRowHandle);
            try
            {
                if (drProduct["Status"].ToString() != WorkOrderStatusOptions.Planned.ToString() && drProduct["Status"].ToString() != WorkOrderStatusOptions.Pending.ToString())
                {
                    btnCancelDetail.Enabled = false;
                    sbtnAddPIDtl.Enabled = false;
                    sbtnDeletePIDtl.Enabled = false;
                    sbtnInsertAllPI.Enabled = false;
                }
                else
                {
                    if (myStockWorkOrder.Action != StockWorkOrderAction.View)
                    {
                        btnCancelDetail.Enabled = false;
                        sbtnAddPIDtl.Enabled = true;
                        sbtnDeletePIDtl.Enabled = true;
                        sbtnInsertAllPI.Enabled = true;
                    }
                    else
                    {

                        btnCancelDetail.Enabled = true;
                        sbtnAddPIDtl.Enabled = false;
                        sbtnDeletePIDtl.Enabled = false;
                        sbtnInsertAllPI.Enabled = false;
                    }
                }
            }
            catch {
                btnCancelDetail.Enabled = false;
                sbtnAddPIDtl.Enabled = true;
                sbtnDeletePIDtl.Enabled = true;
                sbtnInsertAllPI.Enabled = true;

            }


            this.sbtnDelete.Visible = bReadOnly;
            this.sbtnSave.Visible = !bReadOnly;
            this.sbtnSavePreview.Visible = !bReadOnly;
            this.sbtnSavePrint.Visible = !bReadOnly;
            if (bReadOnly)
                this.sbtnCancel.Text = BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_Close, new object[0]);
            else
                this.sbtnCancel.Text = BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_Cancel, new object[0]);
            this.lblCancelled.Visible = this.myStockWorkOrder.Cancelled;
            if (this.myStockWorkOrder.Cancelled)
                this.sbtnCancelDoc.Text = BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_UncancelDocument, new object[0]);
            else
                this.sbtnCancelDoc.Text = BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_CancelDocument, new object[0]);

            //this.lblCancelled.Visible = true;
            //this.panBOMOptional.Enabled = this.myStockWorkOrder.IsMultilevel;
            this.SetWindowCaption();

        }

        private void InvokeAccessRightChanged()
        {
            this.BeginInvoke((Delegate)new MethodInvoker(this.ShowDocNoFormatLookupEdit));
            this.SetModuleFeature();
        }

        private void ShowDocNoFormatLookupEdit()
        {
            if (this.myStockWorkOrder != null)
            {
                try
                {
                    this.luEdtDocNoFormat.Visible = (string)this.myStockWorkOrder.DocNo == "<<New>>" && this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_CHANGE_DOCNO_FORMAT");
                    this.txtEdtStockAssemblyNo.Enabled = this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_EDIT_DOCNO");
                    this.myFilterByLocation = SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnableFilterByCurrentUserLocationInDataEntry;
                    this.luLocation.Enabled = !this.myFilterByLocation;
                    this.tlLocation.OptionsColumn.ReadOnly = this.myFilterByLocation;
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                }
            }
        }

        private void SetWindowCaption()
        {
            if (this.myStockWorkOrder != null)
            {
                this.ShowDocNoFormatLookupEdit();
                string str1;
                if ((string)this.myStockWorkOrder.DocNo == "<<New>>")
                {
                    Document document = Document.CreateDocument(this.myDBSetting);
                    // ISSUE: variable of a boxed type
                    StockWorkOrderStringId local = StockWorkOrderStringId.Code_NextPossibleNo;
                    object[] objArray = new object[1];
                    int index = 0;
                    string documentNo = document.GetDocumentNo("SW", this.luEdtDocNoFormat.Text, (DateTime)this.myStockWorkOrder.DocDate);
                    objArray[index] = (object)documentNo;
                    str1 = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                }
                else
                    str1 = string.Format(" - [{0}]", (object)this.myStockWorkOrder.DocNo);
                string str2 = this.myStockWorkOrder.Action != StockWorkOrderAction.View ? (this.myStockWorkOrder.Action != StockWorkOrderAction.Edit ? BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_NewStockWorkOrder, new object[0]) + str1 : BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_EditStockWorkOrder, new object[0]) + str1) : BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_ViewStockWorkOrder, new object[0]) + str1;
                string format = "{0} - {1} - {2} (Ver: {3})";
                object[] objArray1 = new object[4];
                int index1 = 0;
                string str3 = str2;
                objArray1[index1] = (object)str3;
                int index2 = 1;
                // ISSUE: variable of a boxed type
                DBString local1 = this.myStockWorkOrder.Command.GeneralSetting.CompanyProfile.CompanyNameWithRemark;
                objArray1[index2] = (object)local1;
                int index3 = 2;
                string productName = OEM.GetCurrentOEM().ProductName;
                objArray1[index3] = (object)productName;
                int index4 = 3;
                //  string minorProductVersion = ProductVersion.GetMajorMinorProductVersion();
                //objArray1[index4] = (object) minorProductVersion;
                this.Text = BCE.AutoCount.WinForms.FormHelper.SetUniqueWindowCaption(string.Format(format, objArray1), (Form)this);
            }
        }

        private void InvokeModuleFeature(ModuleController controller)
        {
            this.BeginInvoke((Delegate)new MethodInvoker(this.SetModuleFeature));
        }

        private void SetModuleFeature()
        {
            ModuleController moduleController = ModuleControl.GetOrCreate(this.myDBSetting).ModuleController;
            bool flag = this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("SYS_BHV_VIEW_COST") && ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.BOM.Enable;
            this.barBtnConvertLevel.Visibility = moduleController.MultiLevelAssembly.Enable ? BarItemVisibility.Always : BarItemVisibility.Never;
          //  int num1 = this.txtedtQuantityEst.Left + this.txtedtQuantityEst.Width + 44;
           // int num2 = num1;
            Point location;
            Size size;
            //if (!moduleController.MultiLocationStock.Enable)
            //{
            //  this.myColLocationVisibleIndex = this.tlLocation.VisibleIndex;
            //  this.tlLocation.VisibleIndex = -1;
            //}
            //else
            //{
            //  this.tlLocation.VisibleIndex = this.myColLocationVisibleIndex;
            // // PanelControl panelControl = this.panLocation;
            // // int x = num1;
            //  //location = this.panLocation.Location;
            //  //int y = location.Y;
            //  //Point point = new Point(x, y);
            //  //panelControl.Location = point;
            // // int num3 = num1;
            // // size = this.panLocation.Size;
            // // int num4 = size.Width + 6;
            // // num1 = num3 + num4;
            //}
            //this.tlLocation.OptionsColumn.ShowInCustomizationForm = moduleController.MultiLocationStock.Enable;
            //this.panLocation.Visible = moduleController.MultiLocationStock.Enable;
            //if (!moduleController.BatchNo.Enable)
            //{
            //  this.myColBatchNoVisibleIndex = this.tlBatchNo.VisibleIndex;
            //  this.tlBatchNo.VisibleIndex = -1;
            //}
            //else
            //{
            //  this.tlBatchNo.VisibleIndex = this.myColBatchNoVisibleIndex;
            //  //PanelControl panelControl = this.panBatch;
            //  //int x = num1;
            //  //location = this.panBatch.Location;
            //  //int y = location.Y;
            //  //Point point = new Point(x, y);
            //  //panelControl.Location = point;
            //}
            //this.tlBatchNo.OptionsColumn.ShowInCustomizationForm = moduleController.BatchNo.Enable;
            // this.panBatch.Visible = moduleController.BatchNo.Enable;
            //if (!moduleController.Project.Enable)
            //{
            //  this.myColProjNoVisibleIndex = this.tlProjNo.VisibleIndex;
            //  this.tlProjNo.VisibleIndex = -1;
            //}
            //else
            //{
            //  this.tlProjNo.VisibleIndex = this.myColProjNoVisibleIndex;
            //  PanelControl panelControl = this.panProject;
            //  int x = num2;
            //  location = this.panProject.Location;
            //  int y = location.Y;
            //  Point point = new Point(x, y);
            //  panelControl.Location = point;
            //  int num3 = num2;
            //  size = this.panProject.Size;
            //  int num4 = size.Width + 6;
            //  num2 = num3 + num4;
            //}
            // this.tlProjNo.OptionsColumn.ShowInCustomizationForm = moduleController.Project.Enable;
            //this.panProject.Visible = moduleController.Project.Enable;
            //if (!moduleController.Department.Enable)
            //{
            //  this.myColDeptNoVisibleIndex = this.tlDeptNo.VisibleIndex;
            //  this.tlDeptNo.VisibleIndex = -1;
            //}
            //else
            //{
            //  this.tlDeptNo.VisibleIndex = this.myColDeptNoVisibleIndex;
            //  PanelControl panelControl = this.panDept;
            //  int x = num2;
            //  location = this.panDept.Location;
            //  int y = location.Y;
            //  Point point = new Point(x, y);
            //  panelControl.Location = point;
            //}
            //this.tlDeptNo.OptionsColumn.ShowInCustomizationForm = moduleController.Department.Enable;
            //this.panDept.Visible = moduleController.Department.Enable;
            //this.tlItemCost.Visible = flag;
            //this.tlItemCost.OptionsColumn.ShowInCustomizationForm = flag;
            //this.tlOverheadCost.Visible = flag;
            //this.tlOverheadCost.OptionsColumn.ShowInCustomizationForm = flag;
            //this.tlSubTotal.Visible = flag;
            //this.tlSubTotal.OptionsColumn.ShowInCustomizationForm = flag;
            //this.label12.Visible = flag;
            //this.label11.Visible = flag;
            //this.label10.Visible = flag;
            //this.textEdtTotal.Visible = flag;
            // this.txtedtAssemblyCost.Visible = flag;
            // this.txtedtEstHPP.Visible = flag;
            //this.FooterHeightAdjustment();
            if (this.myGridLayout != null)
                this.myGridLayout.SaveDefaultLayout();

            if (this.myBSGridLayout != null)
                this.myBSGridLayout.SaveDefaultLayout();

            if (this.myAPGridLayout != null)
                this.myAPGridLayout.SaveDefaultLayout();

            if (this.myOvdGridLayout != null)
                this.myOvdGridLayout.SaveDefaultLayout();
        }

        private void FooterHeightAdjustment()
        {
            if (this.myStockWorkOrder != null && !this.myIsInstantInfoShown)
            {
                if ((!this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("SYS_BHV_VIEW_COST") ? 0 : (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.BOM.Enable ? 1 : 0)) == 0)
                    this.panBottom.Height = 0;
                else
                    this.panBottom.Height = BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 76.0));
            }
        }

        private void FormStockAssemblyEntry_Load(object sender, EventArgs e)
        {
            this.myIsLoadForm = true;
            this.myColLocationVisibleIndex = this.tlLocation.VisibleIndex;
            this.myColBatchNoVisibleIndex = this.tlBatchNo.VisibleIndex;
            this.myColProjNoVisibleIndex = this.tlProjNo.VisibleIndex;
            this.myColDeptNoVisibleIndex = this.tlDeptNo.VisibleIndex;
            this.SetModuleFeature();
           // new CustomizeTreeListLayout(this.myDBSetting, this.Name+"RMDtl", this.gridCtlItem).PrintTreeList = false;
            new CustomizeTreeListLayout(this.myDBSetting, this.Name + "RMSummary", this.gridCtlItemSummary).PrintTreeList = false;
            //new CustomizeGridLayout(this.myDBSetting, this.Name, this.gvItem);
            new CustomizeGridLayout(this.myDBSetting, this.Name + "BS1", this.gvBOMBS);
            new CustomizeGridLayout(this.myDBSetting, this.Name + "AP1", this.gvBOMPI);
            new CustomizeGridLayout(this.myDBSetting, this.Name+"Ovd1", this.gvBOMOvd);

            this.tlLocation.OptionsColumn.ReadOnly = this.myFilterByLocation;
            ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.AddListener(new ModuleControllerListenerDelegate(this.InvokeModuleFeature), (Component)this);
            this.myStockWorkOrder.Command.UserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.InvokeAccessRightChanged), (Component)this);
            this.InitFormControls();
            // this.ucInquiryTreeReload();
            this.SetControlState();
            this.SetupRemark();
           // this.luBOM.EditValueChanged += new EventHandler(this.luAssemblyItem_EditValueChanged);
        }

        private void EndCurrentEdit()
        {
            try
            {
                if (this.myStockWorkOrder != null)
                {
                    CurrencyManager currencyManager = (CurrencyManager)this.BindingContext[(object)this.myStockWorkOrder.DataTableMaster];
                    if (currencyManager != null)
                    {
                        currencyManager.EndCurrentEdit();
                        currencyManager.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                AppMessage.ShowInformationMessage(ex.Message);
            }
        }

        //private void luAssemblyItem_EditValueChanged(object sender, EventArgs e)
        //{
        //    //if (this.luBOM.EditValue != null && this.luBOM.EditValue != DBNull.Value && !(this.luBOM.EditValue.ToString() == ""))
        //    {
        //       // this.myMasterItemBatchLookupEditBuilder.FilterItemCode(this.luBOM.EditValue.ToString(), (DateTime)this.myStockWorkOrder.DocDate);
        //        //this.panBOMOptional.Enabled = this.myStockWorkOrder.IsMultilevel;
        //        this.UpdateBOMItemDescription();
        //        this.gvItem.BeginUpdate();
        //        try
        //        {
        //            this.BeginInvoke((Delegate)new MethodInvoker(this.EndCurrentEdit));
        //        }
        //        finally
        //        {
        //            this.gvItem.EndUpdate();
        //        }
        //        //this.ucInquiryReloadAssemblyItem();
        //    }
        //}

        private void SetupLookupEdit()
        {
            this.myMasterItemBatchLookupEditBuilder = new ItemBatchForReceiveLookupEditBuilder();
            //this.cbDocType.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(WorkOrderDocTypeOptions)));

            //this.myMasterItemBatchLookupEditBuilder.BuildLookupEdit(this.luBatchNo.Properties, this.myDBSetting);
            // DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.BuildLookupEdit(this.luProject.Properties, this.myDBSetting);
            // DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.BuildLookupEdit(this.luDeptNo.Properties, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.BuildLookupEdit(this.luLocation.Properties, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.BuildLookupEdit(this.luProjNo.Properties, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).StockAssemblyOrderDocNoFormatLookupEditBuilder.BuildLookupEdit(this.luEdtDocNoFormat.Properties, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchLookupEditBuilder.BuildLookupEdit(this.repLuedtBatchNo, this.myDBSetting);
            if (this.myUseLookupEditToInputItemCode && this.repLuedtItem.Columns.Count == 0)
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repLuedtItem, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.BuildLookupEdit(this.repLuedtLocation, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.BuildLookupEdit(this.repLuedtProjNo, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.BuildLookupEdit(this.repLuedtDeptNo, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.BuildLookupEdit(this.repositoryItemluUOM, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.BuildLookupEdit(this.repleAPUOM, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DebtorLookupEditBuilder.BuildLookupEdit(this.repItemProductDebtorCode, this.myDBSetting);

            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DebtorLookupEditBuilder.BuildLookupEdit(this.luDebtorCode.Properties, this.myDBSetting);
            OverheadCodeLookupEditBuilder overheadbuilder = OverheadCodeLookupEditBuilder.Create();
            overheadbuilder.BuildLookupEdit(repleOvdOverheadCode, myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repItemLuedtItem, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repBOMItemLookUpEdit, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repositoryItemLookUpEdit3, this.myDBSetting);


            Tools.LookupEditBuilders.ItemWithBOMOnlyLookupEditBuilder itembuilder = new Tools.LookupEditBuilders.ItemWithBOMOnlyLookupEditBuilder();
            // if (this.myUseLookupEditToInputItemCode && this.repItemLkEdt_ItemCode.Columns.Count == 0)
            itembuilder.BuildLookupEdit(this.repleAPItemCode, this.myDBSetting);
            //DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repleAPItemCode, this.myDBSetting);
            OverheadCodeLookupEditBuilder ovdlookup = OverheadCodeLookupEditBuilder.Create();
            ovdlookup.BuildLookupEdit(this.repleOvdOverheadCode, this.myDBSetting);
            string sitemcode = "";
            // if (luProductCode.EditValue != null && luProductCode.EditValue != DBNull.Value)
            //    sitemcode = luProductCode.EditValue.ToString();
            itembomlookup = new Production.Tools.LookupEditBuilders.ItemBOMLookupEditBuilder();
            itembomlookup.BuildLookupEdit(this.repleItemBOM, this.myDBSetting);
            itembomlookup.FilterItemCode(sitemcode);

            MachineLookupEditBuilder machinebuilder = MachineLookupEditBuilder.Create();
            machinebuilder.BuildLookupEdit(luMachineCode.Properties, myDBSetting);

        }

        private void UnlinkLookupEditEventHandlers()
        {
            if (this.luProjNo != null)
              DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.UnlinkEventHandlers(this.luProjNo.Properties);
            if (this.luDebtorCode != null)
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.UnlinkEventHandlers(this.luDebtorCode.Properties);

            if (this.repItemProductDebtorCode != null)
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.UnlinkEventHandlers(this.repItemProductDebtorCode);

            //if (this.luDeptNo != null)
            //  DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.UnlinkEventHandlers(this.luDeptNo.Properties);
            if (this.luLocation != null)
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.UnlinkEventHandlers(this.luLocation.Properties);
           // if (this.luBOM != null)
            //    DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).BOMItemLookupEditBuilder.UnlinkEventHandlers(this.luBOM.Properties);
            if (this.luEdtDocNoFormat != null)
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).StockAssemblyOrderDocNoFormatLookupEditBuilder.UnlinkEventHandlers(this.luEdtDocNoFormat.Properties);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchLookupEditBuilder.UnlinkEventHandlers(this.repLuedtBatchNo);
            if (this.repLuedtItem.Columns.Count > 0)
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.UnlinkEventHandlers(this.repLuedtItem);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.UnlinkEventHandlers(this.repLuedtLocation);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.UnlinkEventHandlers(this.repLuedtProjNo);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.UnlinkEventHandlers(this.repLuedtDeptNo);
        }

        private void RefreshLookupEdit()
        {
            try
            {
                //this.myMasterItemBatchLookupEditBuilder.Refresh(this.luBatchNo.Properties);
                ProjectLookupEditBuilder lookupEditBuilder1 = DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder;
                RepositoryItemLookUpEdit[] lookupEdits1 = new RepositoryItemLookUpEdit[2];
                int index1 = 0;
                RepositoryItemLookUpEdit properties1 = this.luProjNo.Properties;
                lookupEdits1[index1] = properties1;
                int index2 = 1;
                //RepositoryItemLookUpEdit repositoryItemLookUpEdit1 = this.repLuedtProjNo;
                //lookupEdits1[index2] = repositoryItemLookUpEdit1;
                //lookupEditBuilder1.Refresh(lookupEdits1);
                DepartmentLookupEditBuilder lookupEditBuilder2 = DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder;
                RepositoryItemLookUpEdit[] lookupEdits2 = new RepositoryItemLookUpEdit[2];
                int index3 = 0;
                //RepositoryItemLookUpEdit properties2 = this.luDeptNo.Properties;
                //lookupEdits2[index3] = properties2;
                //int index4 = 1;
                //RepositoryItemLookUpEdit repositoryItemLookUpEdit2 = this.repLuedtDeptNo;
                //lookupEdits2[index4] = repositoryItemLookUpEdit2;
                //lookupEditBuilder2.Refresh(lookupEdits2);
                LocationLookupEditBuilder lookupEditBuilder3 = DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder;
                RepositoryItemLookUpEdit[] lookupEdits3 = new RepositoryItemLookUpEdit[2];
                int index5 = 0;
                RepositoryItemLookUpEdit properties3 = this.luLocation.Properties;
                lookupEdits3[index5] = properties3;
                int index6 = 1;
                RepositoryItemLookUpEdit repositoryItemLookUpEdit3 = this.repLuedtLocation;
                lookupEdits3[index6] = repositoryItemLookUpEdit3;
                lookupEditBuilder3.Refresh(lookupEdits3);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DebtorLookupEditBuilder.Refresh(this.luDebtorCode.Properties);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DebtorLookupEditBuilder.Refresh(this.repItemProductDebtorCode);

                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).StockAssemblyOrderDocNoFormatLookupEditBuilder.Refresh(this.luEdtDocNoFormat.Properties);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchLookupEditBuilder.Refresh(this.repLuedtBatchNo);
                if (this.myUseLookupEditToInputItemCode && this.repLuedtItem.Columns.Count > 0)
                    DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.Refresh(this.repLuedtItem);
            }
            catch (DataAccessException ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
            }
        }

        private void InitFormControls()
        {
            FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
            
            //string fieldname1 = "Qty";
            //string fieldtype1 = "Quantity";
            //formControlUtil.AddField(fieldname1, fieldtype1);
            //txtedtQuantityEst.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            //txtedtQuantityEst.Properties.DisplayFormat.FormatString = BCE.AutoCount.Settings.DecimalSetting.GetOrCreate(myDBSetting).FormatQuantity(0);
            //txtedtQuantityEst.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            //txtedtQuantityEst.Properties.EditFormat.FormatString = BCE.AutoCount.Settings.DecimalSetting.GetOrCreate(myDBSetting).FormatQuantity(0);
            string fieldname2 = "EstHPP";
            string fieldtype2 = "Currency";
            formControlUtil.AddField(fieldname2, fieldtype2);
            string fieldname3 = "UnitCost";
            string fieldtype3 = "Cost";
            formControlUtil.AddField(fieldname3, fieldtype3);
            string fieldname4 = "TotalCost";
            string fieldtype4 = "Currency";
            formControlUtil.AddField(fieldname4, fieldtype4);
            string fieldname5 = "CostPercent";
            string fieldtype5 = "Quantity";
            formControlUtil.AddField(fieldname5, fieldtype5);
            string fieldname6 = "CostMax";
            string fieldtype6 = "Quantity";
            formControlUtil.AddField(fieldname6, fieldtype6);
            string fieldname7 = "Amount";
            string fieldtype7 = "Cost";
            formControlUtil.AddField(fieldname7, fieldtype7);
            string fieldname8 = "ActHPP";
            string fieldtype8 = "Currency";
            formControlUtil.AddField(fieldname8, fieldtype8);
            string fieldname12 = "OnHandQty";
            string fieldtype12 = "Quantity";
            formControlUtil.AddField(fieldname12, fieldtype12);
            string fieldname13 = "OnHandBal";
            string fieldtype13 = "Quantity";
            formControlUtil.AddField(fieldname13, fieldtype13);
            string fieldname14 = "AvailableQty";
            string fieldtype14 = "Quantity";
            formControlUtil.AddField(fieldname14, fieldtype14);
            string fieldname15 = "AvailableBal";
            string fieldtype15 = "Quantity";
            formControlUtil.AddField(fieldname15, fieldtype15);
            FormStockWorkOrderEntry assemblyOrderEntry = this;
            formControlUtil.InitControls((Control)assemblyOrderEntry);
            this.sBtnAdd.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipAddDetail, new object[0]);
            this.sBtnInsert.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipInsertBefore, new object[0]);
            this.sBtnRemove.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipDeleteDetail, new object[0]);
            this.sBtnMoveUp.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipMoveUp, new object[0]);
            this.sBtnMoveDown.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipMoveDown, new object[0]);
            this.sBtnUndo.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipUndoDetail, new object[0]);
            this.sBtnSelectAllMain.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipSelectAllDetail, new object[0]);
            this.sBtnRangeSetting.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipRangeSet, new object[0]);
            this.sBtnSearch.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipItemSearch, new object[0]);
            this.sBtnShowInstant.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipToggleInstantInfo, new object[0]);
            this.sbtnSave.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipSave, new object[0]);
            this.sbtnSavePreview.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipSaveAndPreview, new object[0]);
            this.sbtnSavePrint.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipSaveAndPrint, new object[0]);
            this.sbtnCancel.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipCancelOrClose, new object[0]);
            object obj = myDBSetting.ExecuteScalar("select QtyDecimal from RPA_Settings");
            if (obj != null && obj != DBNull.Value)
            {
                colQty.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
                colQty.Format.FormatString = "N" + obj.ToString();
                colQty2.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
                colQty2.Format.FormatString = "N" + obj.ToString();

                colAPQty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                colAPQty.DisplayFormat.FormatString = "N" + obj.ToString();

                // colQty.DisplayFormat.FormatString = "d" + obj.ToString();
            }
            obj = myDBSetting.ExecuteScalar("select RateDecimal from RPA_Settings");
            if (obj != null && obj != DBNull.Value)
            {
                colProductRatio.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
                colProductRatio.Format.FormatString = "N" + obj.ToString();
                // colBSRate.DisplayFormat.FormatString = "N" + obj.ToString();
                // colBSRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;

                colRate.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
                colRate.Format.FormatString = "N" + obj.ToString();

                colAPRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                colAPRate.DisplayFormat.FormatString = "N" + obj.ToString();

                colRateUOM.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
                colRateUOM.Format.FormatString = "N" + obj.ToString();
            }
        }

        private void FocusFirstControl()
        {
            this.mruEdtDescription.Focus();
        }

        private bool Save(SaveDocumentAction saveAction)
        {
            #region Register Program
            string sMacAddr = "";
            string sCompanyName = "";
            AssemblyInfoEntity info = new AssemblyInfoEntity();
            object obj2 = myDBSetting.ExecuteScalar("USE MASTER select srvname from sys.sysservers");
            if (obj2 != null && obj2 != DBNull.Value)
            {
                sMacAddr = obj2.ToString();
            }
            obj2 = myDBSetting.ExecuteScalar("select CompanyName from Profile");
            if (obj2 != null && obj2 != DBNull.Value)
            {

                sCompanyName = obj2.ToString();
            }
            if (!Production.GLib.G.IsRegistered("RPA_Settings", sMacAddr + sCompanyName + info.Product, sMacAddr))
            {
                DateTime dtLicenseDate = GLib.G.GetLicenseDate("RPA_Settings", "Production");
                if (DateTime.Now > dtLicenseDate)
                {
                    System.Windows.Forms.MessageBox.Show("Cannot save... Please activate your software.", "Transaction block after " + dtLicenseDate.ToLongDateString(), MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
            #endregion
            if (this.myStockWorkOrder == null || this.IsAutoSaveDisabled())
            {
                return false;
            }
            else
            {
                this.AcceptEdit();
                
                System.Windows.Forms.Application.DoEvents();
                bool flag = false;
                this.DisableAutoSave();
                //FormStockWorkOrderEntry.FormBeforeSaveEventArgs beforeSaveEventArgs1 = new FormStockWorkOrderEntry.FormBeforeSaveEventArgs(this, this.myStockWorkOrder);
                //ScriptObject scriptObject1 = this.myScriptObject;
                //string name1 = "BeforeSave";
                //System.Type[] types1 = new System.Type[1];
                //int index1 = 0;
                //System.Type type1 = beforeSaveEventArgs1.GetType();
                //types1[index1] = type1;
                //object[] objArray1 = new object[1];
                //int index2 = 0;
                //FormStockWorkOrderEntry.FormBeforeSaveEventArgs beforeSaveEventArgs2 = beforeSaveEventArgs1;
                //objArray1[index2] = (object)beforeSaveEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray1);
                //ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
                //string name2 = "BeforeSave";
                //System.Type[] types2 = new System.Type[1];
                //int index3 = 0;
                //System.Type type2 = beforeSaveEventArgs1.GetType();
                //types2[index3] = type2;
                //object[] objArray2 = new object[1];
                //int index4 = 0;
                //FormStockWorkOrderEntry.FormBeforeSaveEventArgs beforeSaveEventArgs3 = beforeSaveEventArgs1;
                //objArray2[index4] = (object)beforeSaveEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray2);
                try
                {
                    if (this.myMasterRecordUndo.CanUndo || this.myPIDetailRecordUndo.CanUndo || this.myStockWorkOrder.PIDetailTable.GetChanges() != null || this.myDetailRecordUndo.CanUndo || this.myStockWorkOrder.DataTableDetail.GetChanges() != null || this.myBSDetailRecordUndo.CanUndo || this.myStockWorkOrder.BSDetailTable.GetChanges() != null || this.myAPDetailRecordUndo.CanUndo || this.myStockWorkOrder.APDetailTable.GetChanges() != null || this.myOvdDetailRecordUndo.CanUndo || this.myStockWorkOrder.OvdDetailTable.GetChanges() != null)
                    {
                        //this.CheckToDeleteLastEmptyItem();
                        //foreach (DataRow dataRow in this.myStockWorkOrder.DataTableDetail.Select("IsBOMItem = 'T'"))
                        //{
                        //    if (this.myStockWorkOrder.DataTableDetail.Select("ParentDtlKey=" + (object)BCE.Data.Convert.ToInt64(dataRow["DtlKey"])).Length == 0)
                        //        dataRow["IsBOMItem"] = (object)"F";
                        //}
                      //  if (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.AdvancedMultiUOM.Enable)
                        //    UOMConversionHelper.Create(this.myDBSetting, "SW", this.luEdtDocNoFormat.Text, (DateTime)this.myStockWorkOrder.DocDate).ApplyUOMConversion(this.GetLeafRawMaterialTable(), this.myStockWorkOrder.DocKey, (string)this.myStockWorkOrder.DocNo);
                        this.myStockWorkOrder.DocNoFormatName = this.luEdtDocNoFormat.Text;
                        if (myStockWorkOrder.Status != WorkOrderStatusOptions.InProcess.ToString() && myStockWorkOrder.Status != WorkOrderStatusOptions.Closed.ToString())
                        {
                           // if ((WorkOrderDocTypeOptions)cbDocType.SelectedIndex == WorkOrderDocTypeOptions.Internal)
                                myStockWorkOrder.Status = WorkOrderStatusOptions.Planned.ToString();
                         //   else
                           //     myStockWorkOrder.Status = WorkOrderStatusOptions.Closed.ToString();
                        }
                        this.myStockWorkOrder.Save(this.myStockWorkOrder.Command.UserAuthentication.LoginUserID);
                        this.myMRUHelper.SaveMRUItems();
                        if (this.myRemarkNameEntity.IsRemarkSupportMRU(1) && this.myMRUHelperRemark1 != null)
                            this.myMRUHelperRemark1.SaveMRUItems();
                        if (this.myRemarkNameEntity.IsRemarkSupportMRU(2) && this.myMRUHelperRemark2 != null)
                            this.myMRUHelperRemark2.SaveMRUItems();
                        if (this.myRemarkNameEntity.IsRemarkSupportMRU(3) && this.myMRUHelperRemark3 != null)
                            this.myMRUHelperRemark3.SaveMRUItems();
                        if (this.myRemarkNameEntity.IsRemarkSupportMRU(4) && this.myMRUHelperRemark4 != null)
                            this.myMRUHelperRemark4.SaveMRUItems();
                    }
                    if (saveAction != SaveDocumentAction.Save)
                        this.PrintPreviewAfterSave(saveAction);
                    flag = true;
                }
                catch (DBConcurrencyException ex)
                {
                    DocumentEditHelper.HandleDBConcurrencyException(this.myDBSetting, "RPA_WO", this.myStockWorkOrder.DocKey);
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                 //   if (ex.GetType() == typeof(TransferedQtyIsExceedLimit))
                       // this.txtedtQuantityEst.Focus();
                }
                finally
                {
                    this.EnableAutoSave();
                }
                return flag;
            }
        }

        private void CheckToDeleteLastEmptyItem()
        {
            //if (this.gvBOMBS.RowCount > 0)
            //{
            //  int count = this.RowCount;
            //  if (this.gvBOMBS.Nodes[count - 1][(object) "ItemCode"] == DBNull.Value && this.gvBOMBS.Nodes[count - 1][(object) "Description"].ToString().Length == 0 && BCE.Data.Convert.ToDecimal(this.gvBOMBS.Nodes[count - 1][(object) "SubTotal"]) == Decimal.Zero)
            //  {
            //    DataRow dataRow = this.myStockWorkOrder.DataTableDetail.Rows.Find(this.gvBOMBS.Nodes[count - 1][(object) "DtlKey"]);
            //    if (dataRow != null)
            //      dataRow.Delete();
            //  }
            //}
        }

        private void PrintPreviewAfterSave(SaveDocumentAction saveAction)
        {
            // ISSUE: variable of a boxed type
            StockWorkOrderString local = StockWorkOrderString.StockWorkOrder;
            object[] objArray1 = new object[1];
            int index1 = 0;
            string str = this.myStockWorkOrder.DocNo.ToString();
            objArray1[index1] = (object)str;
            ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum)local, objArray1), "RPA_WO_DOC_REPORT_PRINT", "RPA_WO_DOC_REPORT_EXPORT", "");
            reportInfo.DocType = "WO";
            reportInfo.DocKey = this.myStockWorkOrder.DocKey;
            reportInfo.UpdatePrintCountTableName = "RPA_WO";
            if (saveAction == SaveDocumentAction.SaveAndPreview)
            {
                reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
                reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
                BCE.AutoCount.Manufacturing.StockAssemblyOrder.BeforePreviewDocumentEventArgs documentEventArgs1 = new BCE.AutoCount.Manufacturing.StockAssemblyOrder.BeforePreviewDocumentEventArgs(reportInfo.EmailAndFaxInfo, reportInfo.DocKey, this.myDBSetting);
                ScriptObject scriptObject1 = this.myScriptObject;
                string name1 = "BeforePreviewDocument";
                System.Type[] types1 = new System.Type[1];
                int index2 = 0;
                System.Type type1 = documentEventArgs1.GetType();
                types1[index2] = type1;
                object[] objArray2 = new object[1];
                int index3 = 0;
                //BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
                // objArray2[index3] = (object) documentEventArgs2;
                // scriptObject1.RunMethod(name1, types1, objArray2);
                // ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
                // string name2 = "BeforePreviewDocument";
                // System.Type[] types2 = new System.Type[1];
                // int index4 = 0;
                // System.Type type2 = documentEventArgs1.GetType();
                // types2[index4] = type2;
                // object[] objArray3 = new object[1];
                // int index5 = 0;
                //// BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs3 = documentEventArgs1;
                // objArray3[index5] = (object) documentEventArgs3;
                // scriptObject2.RunMethod(name2, types2, objArray3);
                // reportInfo.Tag = (object) documentEventArgs1;
                if (documentEventArgs1.AllowPreview)
                    ReportTool.PreviewReport("Stock Work Order Document", this.myStockWorkOrder.Command.GetReportDataSource(this.myStockWorkOrder.DocKey), this.myDBSetting, true, true, this.myStockWorkOrder.Command.ReportOption, reportInfo);
            }
            else if (saveAction == SaveDocumentAction.SaveAndPrint)
                ReportTool.PrintReport("Stock Work Order Document", this.myStockWorkOrder.Command.GetReportDataSource(this.myStockWorkOrder.DocKey), this.myDBSetting, true, this.myStockWorkOrder.Command.ReportOption, reportInfo);
        }

        //private DataTable GetLeafRawMaterialTable()
        //{
        //    DataTable dataTable = this.myStockWorkOrder.DataTableDetail.Copy();
        //    for (int index = dataTable.Rows.Count - 1; index >= 0; --index)
        //    {
        //        DataRow row = dataTable.Rows[index];
        //        if (row.RowState == DataRowState.Deleted)
        //        {
        //            if (BCE.Data.Convert.TextToBoolean(row["IsBOMItem", DataRowVersion.Original]))
        //                dataTable.Rows.Remove(row);
        //        }
        //        else if (BCE.Data.Convert.TextToBoolean(row["IsBOMItem"]))
        //            dataTable.Rows.Remove(row);
        //    }
        //    return dataTable;
        //}

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.AcceptEdit();
            this.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

            object obj = myDBSetting.ExecuteScalar("select status from RPA_WO where DocKey=?", (object)this.myStockWorkOrder.DocKey);
            if (obj != null && obj != DBNull.Value)
            {
                if (obj.ToString() != WorkOrderStatusOptions.Planned.ToString() && obj.ToString() != WorkOrderStatusOptions.Pending.ToString())// WorkOrderStatusOptions.Closed.ToString() || obj.ToString() == WorkOrderStatusOptions.Cancel.ToString())
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, "Status Document WO has been " + obj.ToString() + ", Edit aborted...");
                    return;
                }
            }

            if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_EDIT", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_EDIT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedEditPrintedDocument, new object[0]))))
            {
                try
                {
                    this.myStockWorkOrder.Edit();
                }
                catch (StandardApplicationException ex)
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                    return;
                }
                this.SetControlState();
                this.SwitchToEditMode();
                DBSetting dbSetting = this.myDBSetting;
                string docType = "WO";
                long docKey = this.myStockWorkOrder.DocKey;
                long eventKey = 0L;
                // ISSUE: variable of a boxed type
                StockWorkOrderString local = StockWorkOrderString.EditedStockWorkOrder;
                object[] objArray = new object[2];
                int index1 = 0;
                string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
                objArray[index1] = (object)loginUserId;
                int index2 = 1;
                string str = this.myStockWorkOrder.DocNo.ToString();
                objArray[index2] = (object)str;
                string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                string detail = "";
                Activity.Log(dbSetting, docType, docKey, eventKey, @string, detail);
            }
        }

        private void SwitchToEditMode()
        {
            if (this.myStockWorkOrder != null)
            {
                FormStockWorkOrderEntry.FormEventArgs formEventArgs1 = new FormStockWorkOrderEntry.FormEventArgs(this, this.myStockWorkOrder);
                ScriptObject scriptObject1 = this.myScriptObject;
                string name1 = "OnSwitchToEditMode";
                System.Type[] types1 = new System.Type[1];
                int index1 = 0;
                System.Type type1 = formEventArgs1.GetType();
                types1[index1] = type1;
                object[] objArray1 = new object[1];
                int index2 = 0;
                FormStockWorkOrderEntry.FormEventArgs formEventArgs2 = formEventArgs1;
                objArray1[index2] = (object)formEventArgs2;
                scriptObject1.RunMethod(name1, types1, objArray1);
                ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
                string name2 = "OnSwitchToEditMode";
                System.Type[] types2 = new System.Type[1];
                int index3 = 0;
                System.Type type2 = formEventArgs1.GetType();
                types2[index3] = type2;
                object[] objArray2 = new object[1];
                int index4 = 0;
                FormStockWorkOrderEntry.FormEventArgs formEventArgs3 = formEventArgs1;
                objArray2[index4] = (object)formEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray2);
                //FormEventArgs formEventArgs4 = new FormEventArgs(this.myStockWorkOrder, this.panelHeader, this.gvBOMBS, this.tabControl1);
                //ScriptObject scriptObject3 = this.myStockWorkOrder.ScriptObject;
                //string name3 = "OnSwitchToEditMode";
                //System.Type[] types3 = new System.Type[2];
                //int index5 = 0;
                //System.Type type3 = typeof (object);
                //types3[index5] = type3;
                //int index6 = 1;
                //System.Type type4 = formEventArgs4.GetType();
                //types3[index6] = type4;
                //object[] objArray3 = new object[2];
                //int index7 = 0;
                //FormStockWorkOrderEntry assemblyOrderEntry = this;
                //objArray3[index7] = (object) assemblyOrderEntry;
                //int index8 = 1;
                //BCE.AutoCount.Manufacturing.StockWorkOrder.FormEventArgs formEventArgs5 = formEventArgs4;
                //objArray3[index8] = (object) formEventArgs5;
                //scriptObject3.RunMethod(name3, types3, objArray3);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            object obj = myDBSetting.ExecuteScalar("select status from RPA_WO where DocKey=?", (object)this.myStockWorkOrder.DocKey);
            if (obj != null && obj != DBNull.Value)
            {
                if (obj.ToString() != WorkOrderStatusOptions.Planned.ToString() && obj.ToString() != WorkOrderStatusOptions.Pending.ToString())// WorkOrderStatusOptions.Closed.ToString() || obj.ToString() == WorkOrderStatusOptions.Cancel.ToString())
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, "Status Document WO has been " + obj.ToString() + ", Delete aborted...");
                    return;
                }
            }
            if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DELETE", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_DELETE", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedDeletePrintedDocument, new object[0]))))
            {
                // ISSUE: variable of a boxed type
                StockWorkOrderStringId local1 = StockWorkOrderStringId.ConfirmMessage_DeleteStockWorkOrder;
                object[] objArray = new object[1];
                int index = 0;
                // ISSUE: variable of a boxed type
                DBString local2 = this.myStockWorkOrder.DocNo;
                objArray[index] = (object)local2;
                if (AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum)local1, objArray)))
                {
                    this.DisableAutoSave();
                    try
                    {
                        BCE.Data.Convert.ToInt64(this.myStockWorkOrder.DataTableMaster.Rows[0]["DocKey"]);
                        this.myStockWorkOrder.Delete();
                        AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.ShowMessage_DeleteSuccessfully, new object[0]));
                        this.Close();
                    }
                    catch (DBConcurrencyException ex)
                    {
                        DocumentEditHelper.HandleDBConcurrencyExceptionWhenDeleteDocument();
                    }
                    catch (AppException ex)
                    {
                        AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                    }
                    finally
                    {
                        this.EnableAutoSave();
                    }
                }
            }
        }

        private void FormStockAssemblyEntry_Closing(object sender, CancelEventArgs e)
        {
            if (this.myStockWorkOrder != null)
            {
                if (this.myStockWorkOrder.Action != StockWorkOrderAction.View && this.Visible && !this.mySaveInKIV)
                {
                    this.AcceptEdit();
                    if (this.myMasterRecordUndo.CanUndo || this.myDetailRecordUndo.CanUndo || this.myStockWorkOrder.DataTableDetail.GetChanges() != null)
                    {
                        this.Activate();
                        DialogResult dialogResult = AppMessage.ShowConfirmSaveChangesMessage();
                        e.Cancel = dialogResult != DialogResult.Yes ? dialogResult != DialogResult.No : !this.Save(SaveDocumentAction.Save);
                    }
                }
                if (!e.Cancel)
                {
                    //FormStockWorkOrderEntry.FormClosedEventArgs formClosedEventArgs1 = new FormStockWorkOrderEntry.FormClosedEventArgs(this, this.myStockWorkOrder);
                    //ScriptObject scriptObject1 = this.myScriptObject;
                    //string name1 = "OnFormClosed";
                    //System.Type[] types1 = new System.Type[1];
                    //int index1 = 0;
                    //System.Type type1 = formClosedEventArgs1.GetType();
                    //types1[index1] = type1;
                    //object[] objArray1 = new object[1];
                    //int index2 = 0;
                    ////FormStockWorkOrderEntry.FormClosedEventArgs formClosedEventArgs2 = formClosedEventArgs1;
                    ////objArray1[index2] = (object)formClosedEventArgs2;
                    ////scriptObject1.RunMethod(name1, types1, objArray1);
                    ////ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
                    //string name2 = "OnFormClosed";
                    //System.Type[] types2 = new System.Type[1];
                    //int index3 = 0;
                    //System.Type type2 = formClosedEventArgs1.GetType();
                    //types2[index3] = type2;
                    //object[] objArray2 = new object[1];
                    //int index4 = 0;
                    //FormStockWorkOrderEntry.FormClosedEventArgs formClosedEventArgs3 = formClosedEventArgs1;
                    //objArray2[index4] = (object)formClosedEventArgs3;
                    //scriptObject2.RunMethod(name2, types2, objArray2);
                    this.Cleanup();
                }
            }
        }

        private void Cleanup()
        {
            this.timer1.Enabled = false;
            this.timer2.Enabled = false;
            if (this.Visible && !this.mySaveInKIV)
            {
                if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
                {
                    try
                    {
                        this.DeleteTempDocument();
                    }
                    catch (Exception ex)
                    {
                        StandardExceptionHandler.WriteExceptionToErrorLog(ex);
                    }
                }
            }
            this.gridCtlItem.DataSource = (object)null;
            this.gridCtlIBOMBS.DataSource = (object)null;
            this.gridCtlIBOMPI.DataSource = (object)null;
            this.gridCtlIBOMOvd.DataSource = (object)null;
            ControlsHelper.ClearAllDataBinding((IEnumerable)this.Controls);
            this.UnlinkDelegate();
            if (this.myStockWorkOrder != null)
            {
                this.myStockWorkOrder.PrepareForDispose();
                this.myStockWorkOrder = (StockWorkOrder)null;
            }
            if (this.myMasterRecordUndo != null)
            {
                this.myMasterRecordUndo.CanUndoChanged -= new EventHandler(this.myMasterRecordUndo_CanUndoChanged);
                this.myMasterRecordUndo.Dispose();
                this.myMasterRecordUndo = (UndoManager)null;
            }
            if (this.myDetailRecordUndo != null)
            {
                this.myDetailRecordUndo.CanUndoChanged -= new EventHandler(this.myDetailRecordUndo_CanUndoChanged);
                this.myDetailRecordUndo.Dispose();
                this.myDetailRecordUndo = (UndoManager)null;
            }
            if (this.myBSDetailRecordUndo != null)
            {
                this.myBSDetailRecordUndo.CanUndoChanged -= new EventHandler(this.myBSDetailRecordUndo_CanUndoChanged);
                this.myBSDetailRecordUndo.Dispose();
                this.myBSDetailRecordUndo = (UndoManager)null;
            }
            if (this.myAPDetailRecordUndo != null)
            {
                this.myAPDetailRecordUndo.CanUndoChanged -= new EventHandler(this.myAPDetailRecordUndo_CanUndoChanged);
                this.myAPDetailRecordUndo.Dispose();
                this.myAPDetailRecordUndo = (UndoManager)null;
            }
            if (this.myOvdDetailRecordUndo != null)
            {
                this.myOvdDetailRecordUndo.CanUndoChanged -= new EventHandler(this.myOvdDetailRecordUndo_CanUndoChanged);
                this.myOvdDetailRecordUndo.Dispose();
                this.myOvdDetailRecordUndo = (UndoManager)null;
            }
            if (this.timer1 != null)
                this.timer1.Dispose();
            if (this.timer2 != null)
                this.timer2.Dispose();
            if (this.myFormItemSearch != null)
            {
                this.myFormItemSearch.Dispose();
                this.myFormItemSearch = (FormItemSearch)null;
            }
        }

        private void chkedtNextRecord_CheckedChanged(object sender, EventArgs e)
        {
            this.SaveLocalSetting();
        }

        private void FormStockAssemblyEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.myStockWorkOrder != null)
            {
                if (e.Control && e.KeyCode == (Keys.LButton | Keys.ShiftKey | Keys.Space) && (this.sBtnShowInstant.Visible && this.sBtnShowInstant.Enabled))
                {
                    this.sBtnShowInstant.PerformClick();
                    e.Handled = true;
                }
                else if (e.Shift && e.Control)
                {
                    if (e.KeyCode == (Keys)77)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPageDetail)
                            this.tabControl1.SelectedTabPage = this.tabPageDetail;
                        this.gridCtlItem.Focus();
                        e.Handled = true;
                    }
                    else if (e.KeyCode == (Keys)69)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPageExternalLink)
                            this.tabControl1.SelectedTabPage = this.tabPageExternalLink;
                        this.externalLinkBox1.Focus();
                        e.Handled = true;
                    }
                    else if (e.KeyCode == (Keys)75)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPageBS)
                            this.tabControl1.SelectedTabPage = this.tabPageBS;
                        this.gvBOMBS.Focus();
                        e.Handled = true;
                    }
                    else if (e.KeyCode == (Keys)68)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPagePI)
                            this.tabControl1.SelectedTabPage = this.tabPagePI;
                        this.gvBOMPI.Focus();
                        e.Handled = true;
                    }
                    else if (e.KeyCode == (Keys)72)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPageOvd)
                            this.tabControl1.SelectedTabPage = this.tabPageOvd;
                        this.gvBOMOvd.Focus();
                        e.Handled = true;
                    }
                    else if (e.KeyCode == (Keys)78)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPageNote)
                            this.tabControl1.SelectedTabPage = this.tabPageNote;
                        this.memoEdtNote.Focus();
                        e.Handled = true;
                    }
                }
                else if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
                {
                    if (!e.Shift && !e.Control && !e.Alt)
                    {
                        if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            if (this.tabControl1.SelectedTabPageIndex == 0)
                            {
                                this.AddNewDetail();
                                this.gridCtlItem.Focus();
                                e.Handled = true;
                            }
                            if (this.tabControl1.SelectedTabPageIndex == 1)
                            {
                                this.AddNewBSDetail();
                                this.gvBOMBS.Focus();
                                e.Handled = true;
                            }
                            if (this.tabControl1.SelectedTabPageIndex == 2)
                            {
                                this.AddNewAPDetail();
                                this.gvBOMPI.Focus();
                                e.Handled = true;
                            }
                            if (this.tabControl1.SelectedTabPageIndex == 3)
                            {
                                this.AddNewOvdDetail();
                                this.gvBOMOvd.Focus();
                                e.Handled = true;
                            }
                        }
                        else if (e.KeyCode == (Keys)114)
                        {
                            this.sbtnSave.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)119)
                        {
                            this.sbtnSavePreview.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)118)
                        {
                            this.sbtnSavePrint.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)120 && this.sBtnSearch.Enabled)
                        {
                            this.sBtnSearch.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)123 && this.sBtnRangeSetting.Enabled)
                        {
                            this.sBtnRangeSetting.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)117)
                        {
                            Control control = this.ActiveControl;
                            while (control != null && !(control is TabPage))
                                control = control.Parent;
                            if (control == null)
                            {
                                if (this.tabControl1.SelectedTabPage == this.tabPageDetail)
                                {
                                    if (this.gridCtlItem.CanFocus)
                                        this.gridCtlItem.Focus();
                                }
                                else if (this.tabControl1.SelectedTabPage == this.tabPageBS)
                                {
                                    if (this.gridCtlIBOMBS.CanFocus)
                                        this.gridCtlIBOMBS.Focus();
                                }
                                else if (this.tabControl1.SelectedTabPage == this.tabPagePI)
                                {
                                    if (this.gridCtlIBOMPI.CanFocus)
                                        this.gridCtlIBOMPI.Focus();
                                }
                                else if (this.tabControl1.SelectedTabPage == this.tabPageOvd)
                                {
                                    if (this.gridCtlIBOMOvd.CanFocus)
                                        this.gridCtlIBOMOvd.Focus();
                                }
                                else if (this.tabControl1.SelectedTabPage == this.tabPageExternalLink)
                                {
                                    if (this.externalLinkBox1.CanFocus)
                                        this.externalLinkBox1.Focus();
                                }
                                else if (this.memoEdtNote.CanFocus)
                                    this.memoEdtNote.Focus();
                            }
                            else if (this.mruEdtDescription.CanFocus)
                                this.mruEdtDescription.Focus();
                        }
                    }
                    else if (e.Control && e.Alt)
                    {
                        if (e.KeyCode == (Keys)65)
                        {
                            if (this.tabControl1.SelectedTabPage == this.tabPageDetail)
                            {
                                this.sBtnSelectAllMain.PerformClick();
                                this.gridCtlItem.Focus();
                                e.Handled = true;
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageBS)
                            {
                                this.sbtnSelectAllBS.PerformClick();
                                this.gvBOMBS.Focus();
                                e.Handled = true;
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPagePI)
                            {
                                this.sbtnSelectAllPI.PerformClick();
                                this.gvBOMPI.Focus();
                                e.Handled = true;
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageOvd)
                            {
                                this.sbtnSelectAllOvd.PerformClick();
                                this.gvBOMOvd.Focus();
                                e.Handled = true;
                            }
                        }
                    }
                    else if (e.Alt && e.Shift)
                    {
                        if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Space))
                        {


                            if (this.tabControl1.SelectedTabPage == this.tabPageDetail)
                            {
                                if (this.sBtnMoveUp.Enabled)
                                {
                                    this.sBtnMoveUp.PerformClick();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageBS)
                            {
                                if (this.sbtnUpBS.Enabled)
                                {
                                    this.sbtnUpBS.PerformClick();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPagePI)
                            {
                                if (this.sbtnUpPI.Enabled)
                                {
                                    this.sbtnUpPI.PerformClick();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageOvd)
                            {
                                if (this.sbtnUpOvd.Enabled)
                                {
                                    this.sbtnUpOvd.PerformClick();
                                    e.Handled = true;
                                }
                            }
                        }
                        else if (e.KeyCode == (Keys.Back | Keys.Space) && (this.sBtnMoveDown.Enabled || this.sbtnDownBS.Enabled || this.sbtnDownPI.Enabled || this.sbtnDownOvd.Enabled))
                        {

                            if (this.tabControl1.SelectedTabPage == this.tabPageDetail)
                            {
                                if (this.sBtnMoveDown.Enabled)
                                {
                                    this.sBtnMoveDown.PerformClick();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageBS)
                            {
                                if (this.sbtnDownBS.Enabled)
                                {
                                    this.sbtnDownBS.PerformClick();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPagePI)
                            {
                                if (this.sbtnDownPI.Enabled)
                                {
                                    this.sbtnDownPI.PerformClick();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageOvd)
                            {
                                if (this.sbtnDownOvd.Enabled)
                                {
                                    this.sbtnDownOvd.PerformClick();
                                    e.Handled = true;
                                }
                            }
                        }
                    }
                    else if (e.Control)
                    {
                        if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space) && (this.sBtnRemove.Enabled || this.sbtnDeleteBSDTL.Enabled || this.sbtnDeletePIDtl.Enabled || this.sbtnDeleteOvdDtl.Enabled))
                        {
                           
                            if (this.tabControl1.SelectedTabPage == this.tabPageDetail)
                            {
                                if (this.sBtnRemove.Enabled)
                                {
                                    this.sBtnRemove.PerformClick();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageBS)
                            {
                                if (this.sbtnDeleteBSDTL.Enabled)
                                {
                                    this.sbtnDeleteBSDTL.PerformClick();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPagePI)
                            {
                                if (this.sbtnDeletePIDtl.Enabled)
                                {
                                    this.sbtnDeletePIDtl.PerformClick();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageOvd)
                            {
                                if (this.sbtnDeleteOvdDtl.Enabled)
                                {
                                    this.sbtnDeleteOvdDtl.PerformClick();
                                    e.Handled = true;
                                }
                            }
                        }
                        else if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space) )
                        {
                           

                            if (this.tabControl1.SelectedTabPage == this.tabPageDetail)
                            {
                                if (this.sBtnInsert.Enabled)
                                {
                                    this.InsertDetailBefore();
                                    this.gridCtlItem.Focus();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageBS)
                            {
                                if (this.sBtnInsertBS.Enabled)
                                {
                                    this.InsertBSDetailBefore();
                                    this.gvBOMBS.Focus();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPagePI)
                            {
                                if (this.sbtnInsertAllPI.Enabled)
                                {
                                    this.InsertBSDetailBefore();
                                    this.gvBOMPI.Focus();
                                    e.Handled = true;
                                }
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageOvd)
                            {
                                if (this.sbtnInsertOvd.Enabled)
                                {
                                    this.InsertOvdDetailBefore();
                                    this.gvBOMOvd.Focus();
                                    e.Handled = true;
                                }
                            }
                        }
                        else if (e.KeyCode == (Keys)90  && (this.sBtnUndo.Enabled || this.sbtnUndoBSDtl.Enabled || this.sbtnUndoPIDtl.Enabled || this.sbtnUndoOvdDtl.Enabled))
                        {
                          

                            if (this.tabControl1.SelectedTabPage == this.tabPageDetail)
                            {
                                this.sBtnUndo.PerformClick();
                                e.Handled = true;
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageBS)
                            {
                                this.sbtnUndoBSDtl.PerformClick();
                                e.Handled = true;
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPagePI)
                            {
                                this.sbtnUndoPIDtl.PerformClick();
                                e.Handled = true;
                            }
                            else if (this.tabControl1.SelectedTabPage == this.tabPageOvd)
                            {
                                this.sbtnUndoOvdDtl.PerformClick();
                                e.Handled = true;
                            }
                        }
                        else if (e.KeyCode == (Keys)70)
                        {
                            e.Handled = true;
                        }
                    }
                }
                else if (e.KeyCode == (Keys)113 && this.sbtnEdit.Enabled)
                {
                    this.sbtnEdit.PerformClick();
                    e.Handled = true;
                }
                else if (e.KeyCode == (Keys)119 && this.btnPreview.Enabled)
                {
                    this.btnPreview.PerformClick();
                    e.Handled = true;
                }
                else if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space) && this.sbtnDelete.Enabled)
                {
                    this.sbtnDelete.PerformClick();
                    e.Handled = true;
                }
            }
        }

        private void FormStockAssemblyEntry_Activated(object sender, EventArgs e)
        {
            if (!this.mySkipExecuteFormActivated)
            {
                this.SetUseLookupEditToInputItemCode();
                if (this.myHasDeactivated)
                    this.RefreshLookupEdit();
                else
                    this.FocusFirstControl();
            }
        }
        private void btnCancelDoc_Click(object sender, EventArgs e)
        {
            object obj = myDBSetting.ExecuteScalar("select status from RPA_WO where DocKey=?", (object)this.myStockWorkOrder.DocKey);
            if (obj != null && obj != DBNull.Value)
            {
                if (obj.ToString() != WorkOrderStatusOptions.Planned.ToString() && obj.ToString() != WorkOrderStatusOptions.Pending.ToString())// WorkOrderStatusOptions.Closed.ToString() || obj.ToString() == WorkOrderStatusOptions.Cancel.ToString())
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, "Status Document WO has been " + obj.ToString() + ", Cancel aborted...");
                    return;
                }
            }
            if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_CANCEL", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_CANCEL", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedCancelOrUncancelPrintedDocument, new object[0]))))
            {
                string string1;
                if (this.myStockWorkOrder.Cancelled)
                {
                    // ISSUE: variable of a boxed type
                    StockWorkOrderStringId local1 = StockWorkOrderStringId.ConfirmMessage_UncancelStockWorkOrderAction;
                    object[] objArray = new object[1];
                    int index = 0;
                    // ISSUE: variable of a boxed type
                    DBString local2 = this.myStockWorkOrder.DocNo;
                    objArray[index] = (object)local2;
                    string1 = BCE.Localization.Localizer.GetString((Enum)local1, objArray);
                }
                else
                {
                    // ISSUE: variable of a boxed type
                    StockWorkOrderStringId local1 = StockWorkOrderStringId.ConfirmMessage_CancelStockWorkOrderAction;
                    object[] objArray = new object[1];
                    int index = 0;
                    // ISSUE: variable of a boxed type
                    DBString local2 = this.myStockWorkOrder.DocNo;
                    objArray[index] = (object)local2;
                    string1 = BCE.Localization.Localizer.GetString((Enum)local1, objArray);
                }
                if (AppMessage.ShowConfirmMessage(string1))
                {
                    this.DisableAutoSave();
                    try
                    {
                        if (this.myStockWorkOrder.Cancelled)
                        {
                            this.myStockWorkOrder.UncancelDocument(this.myStockWorkOrder.Command.UserAuthentication.LoginUserID);
                            DBSetting dbSetting = this.myDBSetting;
                            string docType = "WO";
                            long docKey = this.myStockWorkOrder.DocKey;
                            long eventKey = 0L;
                            // ISSUE: variable of a boxed type
                            StockWorkOrderString local = StockWorkOrderString.UncancelStockWorkOrder;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
                            objArray[index1] = (object)loginUserId;
                            int index2 = 1;
                            string str = this.myStockWorkOrder.DocNo.ToString();
                            objArray[index2] = (object)str;
                            string string2 = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                            string detail = "";
                            Activity.Log(dbSetting, docType, docKey, eventKey, string2, detail);
                        }
                        else
                        {
                            this.myStockWorkOrder.CancelDocument(this.myStockWorkOrder.Command.UserAuthentication.LoginUserID);
                            DBSetting dbSetting = this.myDBSetting;
                            string docType = "WO";
                            long docKey = this.myStockWorkOrder.DocKey;
                            long eventKey = 0L;
                            // ISSUE: variable of a boxed type
                            StockWorkOrderString local = StockWorkOrderString.CancelStockWorkOrder;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
                            objArray[index1] = (object)loginUserId;
                            int index2 = 1;
                            string str = this.myStockWorkOrder.DocNo.ToString();
                            objArray[index2] = (object)str;
                            string string2 = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                            string detail = "";
                            Activity.Log(dbSetting, docType, docKey, eventKey, string2, detail);
                        }
                        this.SetControlState();
                    }
                    catch (DBConcurrencyException ex)
                    {
                        DocumentEditHelper.HandleDBConcurrencyException(this.myDBSetting, "RPA_WO", this.myStockWorkOrder.DocKey);
                    }
                    catch (AppException ex)
                    {
                        AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                    }
                    finally
                    {
                        this.EnableAutoSave();
                    }
                }
            }
        }

        private void txtEdtStockAssemblyNo_EditValueChanged(object sender, EventArgs e)
        {
            this.SetWindowCaption();
        }

        //private void ucInquiryTreeReload()
        //{
        //  if (this.gvBOMBS.Focus != null && this.gvBOMBS.Count != 0)
        //  {
        //    this.myLastInquiryItemCode = this.gvBOMBS.FocusedNode.GetDisplayText((object) "ItemCode");
        //    this.myLastInquiryUOM = this.myStockHelper.GetBaseUOM(this.myLastInquiryItemCode);
        //    this.ReloadInquiryUserControl();
        //  }
        //}

        private void ucInquiryReloadAssemblyItem()
        {
           // this.myLastInquiryItemCode = this.luBOM.Text;
            this.myLastInquiryUOM = this.myStockHelper.GetBaseUOM(this.myLastInquiryItemCode);
            this.ReloadInquiryUserControl();
        }

        private void AcceptEdit()
        {
            try
            {
                if (this.mruEdtDescription != null && this.mruEdtDescription.BindingManager != null)
                    this.mruEdtDescription.BindingManager.EndCurrentEdit();

            }
            catch (DataException ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
            }
        }

        private string GetSelectedDetailsXml()
        {
            DataSet selectedDetailsDataSet = null;// this.GetSelectedDetailsDataSet();
            if (selectedDetailsDataSet != null)
            {
                StringWriter stringWriter = new StringWriter();
                selectedDetailsDataSet.WriteXml((TextWriter)stringWriter, XmlWriteMode.WriteSchema);
                return stringWriter.ToString();
            }
            else
                return "";
        }

        //private DataSet GetSelectedDetailsDataSet()
        //{
        //  this.AcceptEdit();
        //  if (this.gvBOMBS.RowCount == 0)
        //  {
        //    return (DataSet) null;
        //  }
        //  else
        //  {
        //    DataTable dataTable1 = this.myStockWorkOrder.DataTableDetail.Copy();
        //    DataSet dataSet = this.myStockWorkOrder.StockWorkOrderDataSet.Clone();
        //    DataTable dataTable2 = dataSet.Tables[1];
        //    try
        //    {
        //      for (int index1 = 0; index1 < this.gvBOMBS.RowCount; ++index1)
        //      {
        //        if (!dataTable2.Rows.Contains(this.gvBOMBS.ro[index1][(object) "DtlKey"]))
        //        {
        //          DataRow row = dataTable1.Rows.Find(this.gvBOMBS.Selection[index1][(object) "DtlKey"]);
        //          dataTable2.ImportRow(row);
        //        }
        //        for (int index2 = 0; index2 < this.gvBOMBS.Selection[index1].Nodes.Count; ++index2)
        //        {
        //          if (!dataTable2.Rows.Contains(this.gvBOMBS.Selection[index1].Nodes[index2][(object) "DtlKey"]))
        //          {
        //            DataRow row = dataTable1.Rows.Find(this.gvBOMBS.Selection[index1].Nodes[index2][(object) "DtlKey"]);
        //            dataTable2.ImportRow(row);
        //          }
        //        }
        //      }
        //    }
        //    catch (Exception ex)
        //    {
        //      AppMessage.ShowErrorMessage(ex.Message);
        //    }
        //    return dataSet;
        //  }
        //}

        private void panelHeader_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.myStockWorkOrder != null)
            {
                this.AcceptEdit();
                this.myLastDragDataSet = this.myStockWorkOrder.StockWorkOrderDataSet;
                int num = (int)this.DoDragDrop((object)new DocumentCarrier(this.myLastDragDataSet, true), DragDropEffects.Copy);
            }
        }

        private void FormStockAssemblyEntry_DragOver(object sender, DragEventArgs e)
        {
            if (this.myStockWorkOrder.Action == StockWorkOrderAction.View)
                e.Effect = DragDropEffects.None;
            else if (e.Data.GetDataPresent(typeof(DocumentCarrier)))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void Drop()
        {
            this.gridCtlItem.BeginUpdate();
            try
            {
                this.myStockWorkOrder.ImportFromDataSet(this.myDropDocCarrier.CarriedDataSet, this.myDropDocCarrier.AcceptWholeDocument);
            }
            finally
            {
                this.gridCtlItem.EndUpdate();
            }
        }

        private void FormStockAssemblyEntry_DragDrop(object sender, DragEventArgs e)
        {
            bool flag = false;
            if (e.Data.GetDataPresent(typeof(DocumentCarrier)))
            {
                this.myDropDocCarrier = (DocumentCarrier)e.Data.GetData(typeof(DocumentCarrier));
                if (this.myLastDragDataSet != this.myDropDocCarrier.CarriedDataSet)
                {
                    this.AcceptEdit();
                    if (!this.myDropDocCarrier.AcceptWholeDocument && this.myDropDocCarrier.CarriedDataSet.Tables.Count > 1)
                    {
                        if (this.myDropDocCarrier.CarriedDataSet.Tables[1].Select("IsBOMItem='T'").Length != 0)
                            flag = true;
                        //if (flag && !this.myStockWorkOrder.IsMultilevel)
                        //{
                        //  AppMessage.ShowInformationMessage(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderStringId.ShowMessage_MultilevelStatusNotMatched, new object[0]));
                        //  return;
                        //}
                    }
                    this.ExecuteWithPauseUndo(new MethodInvoker(this.Drop));
                }
            }
        }

        private void barSubItem1_Popup(object sender, EventArgs e)
        {
            //this.barItemCopySelectedDetails.Enabled = this.gridCtlItem.SelectedRowsCount > 0;
            this.barItemPasteItemDetailOnly.Enabled = this.myStockWorkOrder.Action != StockWorkOrderAction.View;
            this.barItemPasteWholeDocument.Enabled = this.myStockWorkOrder.Action != StockWorkOrderAction.View;
            this.barBtnSaveInKIVFolder.Enabled = this.myStockWorkOrder.Action != StockWorkOrderAction.View;
        }

        private void barItemCopyWholeDocument_ItemClick(object sender, ItemClickEventArgs e)
        {
            ClipboardHelper.SetDataObject((object)this.myStockWorkOrder.ExportAsXml());
        }

        private void barItemCopySelectedDetails_ItemClick(object sender, ItemClickEventArgs e)
        {
            ClipboardHelper.SetDataObject((object)this.GetSelectedDetailsXml());
        }

        private void PasteFromClipboard(bool wholeDocument)
        {
            IDataObject dataObject;
            try
            {
                dataObject = Clipboard.GetDataObject();
            }
            catch (Exception ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message + BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.ErrorMessage_PleaseTryAgain, new object[0]));
                return;
            }
            if (dataObject.GetDataPresent(DataFormats.UnicodeText))
            {
                string str = (string)dataObject.GetData(DataFormats.UnicodeText);
                this.gridCtlItem.BeginUpdate();
                try
                {
                    this.myStockWorkOrder.ImportFromXml(str, wholeDocument);
                }
                catch (XmlException ex1)
                {
                    try
                    {
                        if (!this.myStockWorkOrder.ImportFromTabDelimitedText(str, wholeDocument))
                            AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.ShowMessage_InvalidTextFormat, new object[0]));
                    }
                    catch (DataAccessException ex2)
                    {
                        AppMessage.ShowErrorMessage((IWin32Window)this, ex2.Message);
                    }
                }
                finally
                {
                    this.gridCtlItem.EndUpdate();
                }
            }
        }

        private void PasteWholeDocument()
        {
            this.PasteFromClipboard(true);
        }

        private void barItemPasteWholeDocument_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.PasteWholeDocument));
        }

        private void PasteItemDetailOnly()
        {
            this.PasteFromClipboard(false);
        }

        private void barItemPasteItemDetailOnly_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.PasteItemDetailOnly));
        }

        private void barSubItem2_Popup(object sender, EventArgs e)
        {
            this.barItemCopyFrom.Enabled = this.myStockWorkOrder.Action != StockWorkOrderAction.View;
        }

        private void CopyFromStock()
        {
            using (FormStockWorkSearch stockAssemblySearch = new FormStockWorkSearch(this.myStockWorkOrder.Command, this.myDBSetting, BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.CopyFromOtherStockWorkOrder, new object[0]), false))
            {
                if (stockAssemblySearch.ShowDialog((IWin32Window)this) == DialogResult.OK)
                {
                    StockWorkOrder fromDoc = this.myStockWorkOrder.Command.Edit(stockAssemblySearch.SelectedDocKeys[0]);
                    this.myStockWorkOrder.ImportFromDataSet(fromDoc.StockWorkOrderDataSet, true);
                    FormStockWorkOrderEntry.AfterCopyFromOtherDocumentEventArgs documentEventArgs1 = new FormStockWorkOrderEntry.AfterCopyFromOtherDocumentEventArgs(this.myStockWorkOrder, fromDoc);
                    ScriptObject scriptObject1 = this.myScriptObject;
                    string name1 = "AfterCopyFromOtherDocument";
                    System.Type[] types1 = new System.Type[1];
                    int index1 = 0;
                    System.Type type1 = documentEventArgs1.GetType();
                    types1[index1] = type1;
                    object[] objArray1 = new object[1];
                    int index2 = 0;
                    FormStockWorkOrderEntry.AfterCopyFromOtherDocumentEventArgs documentEventArgs2 = documentEventArgs1;
                    objArray1[index2] = (object)documentEventArgs2;
                    scriptObject1.RunMethod(name1, types1, objArray1);
                    ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
                    string name2 = "AfterCopyFromOtherDocument";
                    System.Type[] types2 = new System.Type[1];
                    int index3 = 0;
                    System.Type type2 = documentEventArgs1.GetType();
                    types2[index3] = type2;
                    object[] objArray2 = new object[1];
                    int index4 = 0;
                    FormStockWorkOrderEntry.AfterCopyFromOtherDocumentEventArgs documentEventArgs3 = documentEventArgs1;
                    objArray2[index4] = (object)documentEventArgs3;
                    scriptObject2.RunMethod(name2, types2, objArray2);
                }
            }
        }

        private void barItemCopyFrom_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.CopyFromStock));
        }

        private void CopyToStock()
        {
            StockWorkOrder stockWorkOrder;
            try
            {
                if (!this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_NEW", true))
                    return;
                else
                    stockWorkOrder = this.myStockWorkOrder.Command.AddNew();
            }
            catch (DataAccessException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                stockWorkOrder = (StockWorkOrder)null;
            }
            if (stockWorkOrder != null)
            {
                stockWorkOrder.ImportFromDataSet(this.myStockWorkOrder.StockWorkOrderDataSet, true);
                FormStockWorkOrderEntry.AfterCopyToNewDocumentEventArgs documentEventArgs1 = new FormStockWorkOrderEntry.AfterCopyToNewDocumentEventArgs(stockWorkOrder, this.myStockWorkOrder);
                ScriptObject scriptObject1 = this.myScriptObject;
                string name1 = "AfterCopyToNewDocument";
                System.Type[] types1 = new System.Type[1];
                int index1 = 0;
                System.Type type1 = documentEventArgs1.GetType();
                types1[index1] = type1;
                object[] objArray1 = new object[1];
                int index2 = 0;
                FormStockWorkOrderEntry.AfterCopyToNewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
                objArray1[index2] = (object)documentEventArgs2;
                scriptObject1.RunMethod(name1, types1, objArray1);
                ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
                string name2 = "AfterCopyToNewDocument";
                System.Type[] types2 = new System.Type[1];
                int index3 = 0;
                System.Type type2 = documentEventArgs1.GetType();
                types2[index3] = type2;
                object[] objArray2 = new object[1];
                int index4 = 0;
                FormStockWorkOrderEntry.AfterCopyToNewDocumentEventArgs documentEventArgs3 = documentEventArgs1;
                objArray2[index4] = (object)documentEventArgs3;
                scriptObject2.RunMethod(name2, types2, objArray2);
                FormStockWorkOrderCmd.StartEntryForm(stockWorkOrder);
            }
        }

        private void barItemCopyTo_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.CopyToStock));
        }
        private void UpdateProductItemDescription()
        {
            //if (luProductCode.EditValue != null && luProductCode.EditValue != DBNull.Value)
            //{
            //    Object obj = myDBSetting.ExecuteScalar("select Description from Item where ItemCode=?", (object)luProductCode.EditValue);
            //    if (obj != null && obj != DBNull.Value)
            //        txtProductName.EditValue = obj;
            //}
            //else
            //    txtProductName.EditValue = "";
        }
        private void UpdateMachineDescription()
        {
            if (luMachineCode.EditValue != null && luMachineCode.EditValue != DBNull.Value)
            {
                Object obj = myDBSetting.ExecuteScalar("select Description from RPA_Machine where MachineCode=?", (object)luMachineCode.EditValue);
                if (obj != null && obj != DBNull.Value)
                    txtMachineName.EditValue = obj;
            }
            else
                txtMachineName.EditValue = "";
        }
        private void UpdateDebtorDescription()
        {
            if (luDebtorCode.EditValue != null && luDebtorCode.EditValue != DBNull.Value)
            {
                Object obj = myDBSetting.ExecuteScalar("select CompanyName from Debtor where AccNo=?", (object)luDebtorCode.EditValue);
                if (obj != null && obj != DBNull.Value)
                {
                    txtDebtorName.EditValue = obj;
                    txtDebtorName.Text = obj.ToString();
                }
            }
            else
                txtDebtorName.EditValue = "";
        }
        private void UpdateProjectDescription()
        {
            if (luProjNo.EditValue != null && luProjNo.EditValue != DBNull.Value)
            {
                Object obj = myDBSetting.ExecuteScalar("select Description from Project where ProjNo=?", (object)luProjNo.EditValue);
                if (obj != null && obj != DBNull.Value)
                    txtProjNo.EditValue = obj;
            }
            else
                txtProjNo.EditValue = "";
        }
        private void UpdateBOMItemDescription()
        {
            //if (luBOM.EditValue != null && luBOM.EditValue != DBNull.Value)
            //{
            //    Object obj = myDBSetting.ExecuteScalar("select Description from RPA_ItemBOM where BOMCode=?", (object)luBOM.EditValue);
            //    if (obj != null && obj != DBNull.Value)
            //        txtBOMName.EditValue = obj;
            //    else
            //        txtBOMName.EditValue = "";
            //}
        }

        private void iUndoMaster_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.myMasterRecordUndo.Undo();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void CalcSubTotal(DataRow row, string[] columnChanged)
        {
            if (columnChanged.Length != 0)
                this.myStockWorkOrder.CalcSubTotal(row);
        }

        private void iEditMRUItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelper.EditMRUItems();
        }
     
        private void ExecuteWithPauseUndo(MethodInvoker sDelegate)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            if (this.myMasterRecordUndo != null)
                this.myMasterRecordUndo.PauseCapture();
            if (this.myDetailRecordUndo != null)
                this.myDetailRecordUndo.PauseCapture();
            if (this.myBSDetailRecordUndo != null)
                this.myBSDetailRecordUndo.PauseCapture();
            if (this.myAPDetailRecordUndo != null)
                this.myAPDetailRecordUndo.PauseCapture();
            if (this.myOvdDetailRecordUndo != null)
                this.myOvdDetailRecordUndo.PauseCapture();
            try
            {
                sDelegate();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
            finally
            {
                if (this.myMasterRecordUndo != null)
                {
                    this.myMasterRecordUndo.ResumeCapture();
                    if (!this.myMasterRecordUndo.IsPauseCapture())
                        this.myMasterRecordUndo.SaveState();
                }
                if (this.myDetailRecordUndo != null)
                {
                    this.myDetailRecordUndo.ResumeCapture();
                    if (!this.myDetailRecordUndo.IsPauseCapture())
                        this.myDetailRecordUndo.SaveState();
                }
                if (this.myBSDetailRecordUndo != null)
                {
                    this.myBSDetailRecordUndo.ResumeCapture();
                    if (!this.myBSDetailRecordUndo.IsPauseCapture())
                        this.myBSDetailRecordUndo.SaveState();
                }
                if (this.myAPDetailRecordUndo != null)
                {
                    this.myAPDetailRecordUndo.ResumeCapture();
                    if (!this.myAPDetailRecordUndo.IsPauseCapture())
                        this.myAPDetailRecordUndo.SaveState();
                }
                if (this.myOvdDetailRecordUndo != null)
                {
                    this.myOvdDetailRecordUndo.ResumeCapture();
                    if (!this.myOvdDetailRecordUndo.IsPauseCapture())
                        this.myOvdDetailRecordUndo.SaveState();
                }
                Cursor.Current = current;
            }
        }

        private void luEdtDocNoFormat_EditValueChanged(object sender, EventArgs e)
        {
            this.SetWindowCaption();
        }

        private void repositoryItemLookUpEditItem_CloseUp(object sender, CloseUpEventArgs e)
        {
            if (e.AcceptValue)
                this.myItemCodeFromPopup = true;
        }

        private void FormStockAssemblyEntry_Deactivate(object sender, EventArgs e)
        {
            this.myHasDeactivated = true;
        }

        private void luAssemblyItem_Enter(object sender, EventArgs e)
        {
            this.ucInquiryReloadAssemblyItem();
            //if (this.myStockWorkOrder.FromDocDtlKey > 0L)
            //  this.luAssemblyItem.Properties.ReadOnly = true;
            //else
            //  this.luAssemblyItem.Properties.ReadOnly = false;
        }

        private void sbtnSave_Click(object sender, EventArgs e)
        {
            if (this.Save(SaveDocumentAction.Save))
                this.CheckContinueToNewDocument();
            else
                this.DialogResult = DialogResult.None;
        }

        private void CheckContinueToNewDocument()
        {
            if (this.chkedtNextRecord.Visible && this.chkedtNextRecord.Checked)
            {
                StockWorkOrder newStockWorkOrder = this.myStockWorkOrder.Command.AddNew();
                if (newStockWorkOrder != null)
                {
                    this.SetStockWorkOrder(newStockWorkOrder);
                    this.FocusFirstControl();
                    return;
                }
            }
            this.Close();
        }

        private void sbtnSavePreview_Click(object sender, EventArgs e)
        {
            if (this.Save(SaveDocumentAction.SaveAndPreview))
                this.CheckContinueToNewDocument();
            else
                this.DialogResult = DialogResult.None;
        }

        private void sbtnSavePrint_Click(object sender, EventArgs e)
        {
            if (this.Save(SaveDocumentAction.SaveAndPrint))
                this.CheckContinueToNewDocument();
            else
                this.DialogResult = DialogResult.None;
        }

        private bool SaveTempDocument(string saveReason)
        {
            if (this.myInProcessTempDocument > 0)
            {
                return false;
            }
            else
            {
                FormStockWorkOrderEntry assemblyOrderEntry = this;
                bool lockTaken = false;
                try
                {
                    Monitor.Enter((object)assemblyOrderEntry, ref lockTaken);
                    this.myInProcessTempDocument = this.myInProcessTempDocument + 1;
                    try
                    {
                        this.myStockWorkOrder.SaveToTempDocument(saveReason);
                    }
                    finally
                    {
                        if (this.myInProcessTempDocument > 0)
                            this.myInProcessTempDocument = this.myInProcessTempDocument - 1;
                    }
                }
                finally
                {
                    if (lockTaken)
                        Monitor.Exit((object)assemblyOrderEntry);
                }
                return true;
            }
        }

        private void DeleteTempDocument()
        {
            FormStockWorkOrderEntry assemblyOrderEntry = this;
            bool lockTaken = false;
            try
            {
                Monitor.Enter((object)assemblyOrderEntry, ref lockTaken);
                this.myInProcessTempDocument = this.myInProcessTempDocument + 1;
                try
                {
                    TempDocument.Delete(this.myDBSetting, this.myStockWorkOrder.DocKey);
                }
                finally
                {
                    if (this.myInProcessTempDocument > 0)
                        this.myInProcessTempDocument = this.myInProcessTempDocument - 1;
                }
            }
            finally
            {
                if (lockTaken)
                    Monitor.Exit((object)assemblyOrderEntry);
            }
        }

        private void barBtnSaveInKIVFolder_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.AcceptEdit();
            try
            {
                if (this.SaveTempDocument("K.I.V."))
                {
                    this.mySaveInKIV = true;
                    this.Close();
                }
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.Visible && this.myStockWorkOrder.Action != StockWorkOrderAction.View && TempDocumentSetting.Default.EnableAutoSave)
            {
                if (!this.IsAutoSaveDisabled())
                {
                    try
                    {
                        this.SaveTempDocument("Auto-save");
                    }
                    catch (Exception ex)
                    {
                        StandardExceptionHandler.WriteExceptionToErrorLog(ex);
                    }
                }
            }
        }

        private void navigator_ButtonClick(object sender, BCE.Controls.NavigatorButtonClickEventArgs e)
        {
            StockWorkOrder newStockWorkOrder = (StockWorkOrder)null;
            try
            {
                if (e.ButtonType == BCE.Controls.NavigatorButtonType.First)
                    newStockWorkOrder = this.myStockWorkOrder.Command.ViewFirst();
                else if (e.ButtonType == BCE.Controls.NavigatorButtonType.Prev)
                    newStockWorkOrder = this.myStockWorkOrder.Command.ViewPrev((string)this.myStockWorkOrder.DocNo);
                else if (e.ButtonType == BCE.Controls.NavigatorButtonType.Next)
                    newStockWorkOrder = this.myStockWorkOrder.Command.ViewNext((string)this.myStockWorkOrder.DocNo);
                else if (e.ButtonType == BCE.Controls.NavigatorButtonType.Last)
                    newStockWorkOrder = this.myStockWorkOrder.Command.ViewLast();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
                return;
            }
            if (newStockWorkOrder != null)
                this.SetStockWorkOrder(newStockWorkOrder);
        }

        private void barbtnTransferFromSO_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            this.gvBOMPI.BeginUpdate();
            try
            {
                using (FormTransferDoc formTransferDoc = new FormTransferDoc(this.myStockWorkOrder,luDebtorCode.EditValue))
                {
                    int num = (int)formTransferDoc.ShowDialog();
                }
            }
            finally
            {
                this.gvBOMPI.EndUpdate();
                Cursor.Current = current;
            }
        }

        private void barbtnCheckTransferredToStatus_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                using (FormTransferredToStatus transferredToStatus = new FormTransferredToStatus(this.myStockWorkOrder))
                {
                    int num = (int)transferredToStatus.ShowDialog();
                }
            }
            finally
            {
                Cursor.Current = current;
            }
        }

        private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DOC_REPORT_PREVIEW", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_PREVIEW", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPreviewPrintedDocument, new object[0]))))
            {
                // ISSUE: variable of a boxed type
                StockWorkOrderString local = StockWorkOrderString.StockWorkOrder;
                object[] objArray1 = new object[1];
                int index1 = 0;
                string str = this.myStockWorkOrder.DocNo.ToString();
                objArray1[index1] = (object)str;
                ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum)local, objArray1), "RPA_WO_DOC_REPORT_PRINT", "RPA_WO_DOC_REPORT_EXPORT", "");
                reportInfo.DocType = "WO";
                reportInfo.DocKey = this.myStockWorkOrder.DocKey;
                reportInfo.UpdatePrintCountTableName = "RPA_WO";
                reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
                reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
                reportInfo.EmailAndFaxInfo = StockWorkOrderCommand.GetEmailAndFaxInfo(this.myStockWorkOrder.DocKey, this.myDBSetting);
                //BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs1 = new BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs(reportInfo.EmailAndFaxInfo, reportInfo.DocKey, this.myDBSetting);
                //ScriptObject scriptObject1 = this.myScriptObject;
                //string name1 = "BeforePreviewDocument";
                //System.Type[] types1 = new System.Type[1];
                //int index2 = 0;
                //System.Type type1 = documentEventArgs1.GetType();
                //types1[index2] = type1;
                //object[] objArray2 = new object[1];
                //int index3 = 0;
                //BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
                //objArray2[index3] = (object) documentEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray2);
                //ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
                //string name2 = "BeforePreviewDocument";
                //System.Type[] types2 = new System.Type[1];
                //int index4 = 0;
                //System.Type type2 = documentEventArgs1.GetType();
                //types2[index4] = type2;
                //object[] objArray3 = new object[1];
                //int index5 = 0;
                //BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs3 = documentEventArgs1;
                //objArray3[index5] = (object) documentEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray3);
                //reportInfo.Tag = (object) documentEventArgs1;
                ReportTool.PreviewReport("Stock Work Order Document", this.myStockWorkOrder.Command.GetReportDataSource(this.myStockWorkOrder.DocKey), this.myDBSetting, e.DefaultReport, false, this.myStockWorkOrder.Command.ReportOption, reportInfo);
            }
        }

        private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DOC_REPORT_PRINT", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_PRINT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0]))))
            {
                // ISSUE: variable of a boxed type
                StockWorkOrderString local = StockWorkOrderString.StockWorkOrder;
                object[] objArray = new object[1];
                int index = 0;
                string str = this.myStockWorkOrder.DocNo.ToString();
                objArray[index] = (object)str;
                ReportTool.PrintReport("Stock Work Order Document", this.myStockWorkOrder.Command.GetReportDataSource(this.myStockWorkOrder.DocKey), this.myDBSetting, e.DefaultReport, this.myStockWorkOrder.Command.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum)local, objArray), "RPA_WO_DOC_REPORT_PRINT", "RPA_WO_DOC_REPORT_EXPORT", "")
                {
                    DocType = "WO",
                    DocKey = this.myStockWorkOrder.DocKey,
                    UpdatePrintCountTableName = "RPA_WO",
                    EmailAndFaxInfo = StockWorkOrderCommand.GetEmailAndFaxInfo(this.myStockWorkOrder.DocKey, this.myDBSetting)
                });
            }
        }

        private void sBtnAdd_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.AddNewDetail));
        }

        private void repLuedtItem_EditValueChanged(object sender, EventArgs e)
        {
            if (this.myItemCodeFromPopup)
            {
                this.myItemCodeFromPopup = false;
            }
            //this.ucInquiryTreeReload();
        }

        private void AddNewDetail()
        {
            if (this.tabControl1.SelectedTabPage != this.tabPageDetail)
                this.tabControl1.SelectedTabPage = this.tabPageDetail;
            this.UpdateTreeList();
            this.myStockWorkOrder.AddDetail();
            // BCE.AutoCount.XtraUtils.GridViewUtils.SelectRowBySeq(this.gvItem, this.myStockWorkOrder.AddDetail().Seq);
            this.gridCtlItem.Focus();
        }
        private void AddNewBSDetail()
        {
            if (this.tabControl1.SelectedTabPage != this.tabPageBS)
                this.tabControl1.SelectedTabPage = this.tabPageBS;
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMBS);
            BCE.AutoCount.XtraUtils.GridViewUtils.SelectRowBySeq(this.gvBOMBS, this.myStockWorkOrder.AddBSDetail().Seq);
            this.gvBOMBS.Focus();
        }
        private void AddNewPIDetail()
        {
            if (this.tabControl1.SelectedTabPage != this.tabPagePI)
                this.tabControl1.SelectedTabPage = this.tabPagePI;
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMPI);
            BCE.AutoCount.XtraUtils.GridViewUtils.SelectRowBySeq(this.gvBOMPI, this.myStockWorkOrder.AddPIDetail().Seq);
            this.gvBOMPI.Focus();
        }


        private void AddNewAPDetail()
        {
            if (this.tabControl1.SelectedTabPage != this.tabPageAP)
                this.tabControl1.SelectedTabPage = this.tabPageAP;
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMAP);
            BCE.AutoCount.XtraUtils.GridViewUtils.SelectRowBySeq(this.gvBOMAP, this.myStockWorkOrder.AddAPDetail().Seq);
            this.gvBOMAP.Focus();
        }


        private void AddNewOvdDetail()
        {
            if (this.tabControl1.SelectedTabPage != this.tabPageOvd)
                this.tabControl1.SelectedTabPage = this.tabPageOvd;
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMOvd);
            BCE.AutoCount.XtraUtils.GridViewUtils.SelectRowBySeq(this.gvBOMOvd, this.myStockWorkOrder.AddOvdDetail().Seq);
            this.gvBOMOvd.Focus();
        }
        private void sBtnInsert_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.InsertDetailBefore));
        }

        private void repLuedtItem_CloseUp(object sender, CloseUpEventArgs e)
        {
            if (e.AcceptValue)
                this.myItemCodeFromPopup = true;
        }

        private void InsertDetailBefore()
        {

            this.UpdateTreeList();
            TreeListNode focusedNode = this.gridCtlItem.FocusedNode;
            if (focusedNode != null)
            {
                this.myStockWorkOrder.InsertDetailBefore(focusedNode);
                this.SortTreeList();
                this.gridCtlItem.Focus();
            }
        }
        private void UpdateTreeList()
        {
            this.gridCtlItem.CloseEditor();
            this.gridCtlItem.EndCurrentEdit();
        }
        private void SortTreeList()
        {
            this.gridCtlItem.BeginSort();
            this.gridCtlItem.EndSort();
        }
        private void InsertBSDetailBefore()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMBS);
            int focusedRowHandle = this.gvBOMBS.FocusedRowHandle;
            if (focusedRowHandle >= 0)
            {
                this.myStockWorkOrder.InsertBSDetailBefore(focusedRowHandle);
                this.gvBOMBS.FocusedRowHandle = focusedRowHandle;
                this.gvBOMBS.ClearSelection();
                this.gvBOMBS.SelectRow(focusedRowHandle);
                if (this.gvBOMBS.VisibleColumns.Count > 0)
                    this.gvBOMBS.FocusedColumn = this.gvBOMBS.VisibleColumns[0];
            }
        }

        private void InsertAPDetailBefore()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMAP);
            int focusedRowHandle = this.gvBOMAP.FocusedRowHandle;
            if (focusedRowHandle >= 0)
            {
                this.myStockWorkOrder.InsertBAPDetailBefore(focusedRowHandle);
                this.gvBOMAP.FocusedRowHandle = focusedRowHandle;
                this.gvBOMAP.ClearSelection();
                this.gvBOMAP.SelectRow(focusedRowHandle);
                if (this.gvBOMAP.VisibleColumns.Count > 0)
                    this.gvBOMAP.FocusedColumn = this.gvBOMAP.VisibleColumns[0];
            }
        }

        private void InsertPIDetailBefore()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMPI);
            int focusedRowHandle = this.gvBOMPI.FocusedRowHandle;
            if (focusedRowHandle >= 0)
            {
                this.myStockWorkOrder.InsertBPIDetailBefore(focusedRowHandle);
                this.gvBOMPI.FocusedRowHandle = focusedRowHandle;
                this.gvBOMPI.ClearSelection();
                this.gvBOMPI.SelectRow(focusedRowHandle);
                if (this.gvBOMPI.VisibleColumns.Count > 0)
                    this.gvBOMPI.FocusedColumn = this.gvBOMPI.VisibleColumns[0];
            }
        }

        private void InsertOvdDetailBefore()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMBS);
            int focusedRowHandle = this.gvBOMBS.FocusedRowHandle;
            if (focusedRowHandle >= 0)
            {
                this.myStockWorkOrder.InsertBOvdDetailBefore(focusedRowHandle);
                this.gvBOMBS.FocusedRowHandle = focusedRowHandle;
                this.gvBOMBS.ClearSelection();
                this.gvBOMBS.SelectRow(focusedRowHandle);
                if (this.gvBOMBS.VisibleColumns.Count > 0)
                    this.gvBOMBS.FocusedColumn = this.gvBOMBS.VisibleColumns[0];
            }
        }
        //private void UpdateTreeList()
        //{
        //  this.gvBOMBS.CloseEditor();
        //  this.gvBOMBS.EndCurrentEdit();
        //}

        //private void SortTreeList()
        //{
        //    this.gvBOMBS.BeginSort();
        //    this.gvBOMBS.EndSort();
        //}

        private void sBtnRemove_Click(object sender, EventArgs e)
        {
           this.ExecuteWithPauseUndo(new MethodInvoker(this.DeleteDetailselectedRows));
        }

        //private void DeleteTreeSelectedRows()
        //{
        //  //this.UpdateTreeList();
        //  IEnumerator enumerator = this.gvBOMBS.Selection.GetEnumerator();
        //  ArrayList arrayList = new ArrayList();
        //  while (enumerator.MoveNext())
        //  {
        //    TreeListNode treeListNode = (TreeListNode) enumerator.Current;
        //    arrayList.Add(treeListNode[(object) "DtlKey"]);
        //    if (BCE.Data.Convert.ToDecimal(treeListNode[(object) "TransferedQty"]) > Decimal.Zero)
        //    {
        //      FormStockWorkOrderEntry assemblyOrderEntry = this;
        //      // ISSUE: variable of a boxed type
        //      StockWorkOrderStringId  local = StockWorkOrderStringId.ErrorMessage_NotAllowDeleteCauseAlreadyTransfer;
        //      object[] objArray = new object[1];
        //      int index = 0;
        //      string str = treeListNode[(object) "ItemCode"].ToString();
        //      objArray[index] = (object) str;
        //      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
        //      AppMessage.ShowErrorMessage((IWin32Window) assemblyOrderEntry, @string);
        //      return;
        //    }
        //  }
        //  if (arrayList.Count != 0 && AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderStringId.ConfirmMessage_DeleteSelectedRecord, new object[0])))
        //  {
        //    for (int index = 0; index < arrayList.Count; ++index)
        //    {
        //      DataRow dataRow = this.myStockWorkOrder.DataTableDetail.Rows.Find((object) System.Convert.ToInt64(arrayList[index]));
        //      if (dataRow != null)
        //        dataRow.Delete();
        //    }
        //    this.SetControlState();
        //  }
        //}
        private void MoveUpDetail()
        {
            //BCE.XtraUtils.GridViewUtils.MoveUp(this.gridCtlItem);

        }
        private void MoveDownDetail()
        {
            //BCE.XtraUtils.GridViewUtils.MoveDown(this.gridCtlItem);
        }

        private void MoveUpBSDetail()
        {
            BCE.XtraUtils.GridViewUtils.MoveUp(this.gvBOMBS);
        }
        private void MoveDownBSDetail()
        {
            BCE.XtraUtils.GridViewUtils.MoveDown(this.gvBOMBS);
        }

        private void MoveUpAPDetail()
        {
            BCE.XtraUtils.GridViewUtils.MoveUp(this.gvBOMAP);
        }
        private void MoveDownAPDetail()
        {
            BCE.XtraUtils.GridViewUtils.MoveDown(this.gvBOMAP);
        }


        private void MoveUpPIDetail()
        {
            BCE.XtraUtils.GridViewUtils.MoveUp(this.gvBOMPI);
        }
        private void MoveDownPIDetail()
        {
            BCE.XtraUtils.GridViewUtils.MoveDown(this.gvBOMPI);
        }

        private void MoveUpOvdDetail()
        {
            BCE.XtraUtils.GridViewUtils.MoveUp(this.gvBOMOvd);
        }
        private void MoveDownOvdDetail()
        {
            BCE.XtraUtils.GridViewUtils.MoveDown(this.gvBOMOvd);
        }

        private void sBtnMoveUp_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveUpDetail));
        }
        
        private void sBtnMoveDown_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveDownDetail));
        }
        private void DeleteDetailselectedRows()
        {
            //BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvItem);
            //DevExpress.XtraGrid.Views.Grid.GridView view = this.gvItem as DevExpress.XtraGrid.Views.Grid.GridView;           
        }
        private void DeleteBSDetailselectedRows()
        {
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvBOMBS);
            DevExpress.XtraGrid.Views.Grid.GridView view = this.gvBOMBS as DevExpress.XtraGrid.Views.Grid.GridView;
        }
        private void DeleteAPDetailselectedRows()
        {
            DataRow drProduct = gvBOMAP.GetDataRow(gvBOMAP.FocusedRowHandle);
            this.myStockWorkOrder.DataTableDetailSummary.Clear();
            if (drProduct != null)
            {
                //DataRow[] dataRowRM =this.myStockWorkOrder.DataTableDetail.Select("FromDocDtlKey = " + drProduct["DtlKey"], "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent); 
                //for (int j = dataRowRM.Length - 1; j >= 0; j--)
                //{
                //    dataRowRM[j].Delete();
                //}
            }

            //this.myStockWorkOrder.ProcessRawMaterialSummary(ceLevel.Checked);
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvBOMAP);
            DevExpress.XtraGrid.Views.Grid.GridView view = this.gvBOMAP as DevExpress.XtraGrid.Views.Grid.GridView;
        }

        private void DeletePIDetailselectedRows()
        {
            DataRow drProduct = gvBOMPI.GetDataRow(gvBOMPI.FocusedRowHandle);
            this.myStockWorkOrder.DataTableDetailSummary.Clear();
            if (drProduct != null)
            {
                DataRow[] dataRowRM = this.myStockWorkOrder.DataTableDetail.Select("FromDocDtlKey = " + drProduct["DtlKey"], "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
                for (int j = dataRowRM.Length - 1; j >= 0; j--)
                {
                    dataRowRM[j].Delete();
                }
            }

            this.myStockWorkOrder.ProcessRawMaterialSummary(ceLevel.Checked);
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvBOMPI);
            DevExpress.XtraGrid.Views.Grid.GridView view = this.gvBOMPI as DevExpress.XtraGrid.Views.Grid.GridView;
        }

        private void DeleteOvdDetailselectedRows()
        {
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvBOMOvd);
            DevExpress.XtraGrid.Views.Grid.GridView view = this.gvBOMOvd as DevExpress.XtraGrid.Views.Grid.GridView;
        }

        private void sBtnSearch_Click(object sender, EventArgs e)
        {
            //this.ExecuteWithPauseUndo(new MethodInvoker(this.ItemSearchTree));
        }
            
        private void sBtnUndoTree_Click(object sender, EventArgs e)
        {
            //if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
            //{
            //    Cursor current = Cursor.Current;
            //    Cursor.Current = Cursors.WaitCursor;
            //    try
            //    {
            //        this.gvItem.BeginUpdate();
            //        this.myStockWorkOrder.BeginLoadDetailData();
            //        this.myDetailRecordUndo.Undo();
            //        this.myStockWorkOrder.EndLoadDetailData();
            //        this.gvItem.EndUpdate();
            //    }
            //    finally
            //    {
            //        Cursor.Current = current;
            //    }
            //}
        }
        private void ToggleMultiLevel()
        {
            if (this.myIsLoadForm)
                this.BeginInvoke((Delegate)new MethodInvoker(this.EndCurrentEdit));
        }

        private void sBtnShowInstant_Click(object sender, EventArgs e)
        {
            this.myIsInstantInfoShown = !this.myIsInstantInfoShown;
            if (this.myUCInquiry == null)
                this.CreateUCInquiry();
            this.myUCInquiry.Visible = !this.myUCInquiry.Visible;
            this.splitterControl1.Visible = this.myUCInquiry.Visible;
            if (this.myUCInquiry.Visible)
            {
                this.panBottom.Height = BCE.Data.Convert.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height / 96.0 * 156.0));
                this.sBtnShowInstant.Text = BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_ShowFooter, new object[0]);
            }
            else
            {
                this.FooterHeightAdjustment();
                this.sBtnShowInstant.Text = BCE.Localization.Localizer.GetString((Enum)StockWorkOrderStringId.Code_ShowInstantInfo, new object[0]);
            }
        }


        private void InitializeFormControl()
        {
            FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting, false);
            string fieldname1 = "Qty";
            string fieldtype1 = "Quantity";
            formControlUtil.AddField(fieldname1, fieldtype1);
            //txtedtQuantityEst.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            //txtedtQuantityEst.Properties.DisplayFormat.FormatString = BCE.AutoCount.Settings.DecimalSetting.GetOrCreate(myDBSetting).FormatQuantity(0);
            //txtedtQuantityEst.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            //txtedtQuantityEst.Properties.EditFormat.FormatString = BCE.AutoCount.Settings.DecimalSetting.GetOrCreate(myDBSetting).FormatQuantity(0);
            string fieldname2 = "EstHPP";
            string fieldtype2 = "Currency";
            formControlUtil.AddField(fieldname2, fieldtype2);
            string fieldname3 = "UnitCost";
            string fieldtype3 = "Cost";
            formControlUtil.AddField(fieldname3, fieldtype3);
            string fieldname4 = "TotalCost";
            string fieldtype4 = "Currency";
            formControlUtil.AddField(fieldname4, fieldtype4);

            string fieldname5 = "CostPercent";
            string fieldtype5 = "Quantity";
            formControlUtil.AddField(fieldname5, fieldtype5);
            string fieldname6 = "CostMax";
            string fieldtype6 = "Quantity";
            formControlUtil.AddField(fieldname6, fieldtype6);
            string fieldname7 = "Amount";
            string fieldtype7 = "Cost";
            formControlUtil.AddField(fieldname7, fieldtype7);
            string fieldname8 = "ActHPP";
            string fieldtype8 = "Currency";
            formControlUtil.AddField(fieldname8, fieldtype8);
            FormStockWorkOrderEntry bomOptionalEntry = this;
            formControlUtil.InitControls((Control)bomOptionalEntry);

        }
            private void sBtnApplyBOMOptional_Click(object sender, EventArgs e)
        {
            this.ApplyBOMOptional();
        }

        private void ApplyBOMOptional()
        {
            //FormBOMOptional formBomOptional = (FormBOMOptional) null;
            //TreeListNode treeListNode = (TreeListNode) null;
            //TreeListNodes childNodes = (TreeListNodes) null;
            //object docKey = (object) null;
            //DataTable tableAsmbomOptional = this.myStockWorkOrder.DataTableASMBOMOptional;
            //Decimal defaultQty = new Decimal();
            //if (this.chkedtChildItem.Checked && this.gvBOMBS.FocusedNode != null)
            //{
            //  if (this.gvBOMBS.FocusedNode[(object) "IsBOMItem"].ToString() == "T")
            //  {
            //    docKey = this.gvBOMBS.FocusedNode[(object) "DtlKey"];
            //    formBomOptional = new FormBOMOptional(this.gvBOMBS.FocusedNode[(object) "ItemCode"].ToString(), this.myDBSetting, (Decimal) this.myStockWorkOrder.Qty, tableAsmbomOptional, docKey);
            //    treeListNode = this.gvBOMBS.FocusedNode;
            //    childNodes = this.gvBOMBS.FocusedNode.Nodes;
            //    defaultQty = BCE.Data.Convert.ToDecimal(childNodes.ParentNode[(object) "Qty"]);
            //  }
            //}
            //else if (this.luAssemblyItem.EditValue != null)
            //{
            //  docKey = this.myStockWorkOrder.DataTableMaster.Rows[0]["DocKey"];
            //  formBomOptional = new FormBOMOptional(this.luAssemblyItem.EditValue.ToString(), this.myDBSetting, (Decimal) this.myStockWorkOrder.Qty, tableAsmbomOptional, docKey);
            //  childNodes = this.gvBOMBS.Nodes;
            //  defaultQty = BCE.Data.Convert.ToDecimal(this.txtedtQuantity.EditValue);
            //}
            //if (formBomOptional != null && formBomOptional.HvBOMOptional && formBomOptional.ShowDialog() == DialogResult.OK)
            //{
            //  DataTable dtblBomOptional = formBomOptional.DtblBOMOptional;
            //  formBomOptional.Close();
            //  DataRow[] oldBOMOptional = tableAsmbomOptional.Select("DocKey=" + (object) BCE.Data.Convert.ToInt64(docKey));
            //  this.ApplyBOMOptional(dtblBomOptional, oldBOMOptional, childNodes, defaultQty, docKey);
            //  this.UpdateASMBOMOptional(dtblBomOptional, docKey);
            //  TreeListNodes treeListNodes = treeListNode != null ? treeListNode.Nodes : this.gvBOMBS.Nodes;
            //  this.gvBOMBS.BeginUpdate();
            //  try
            //  {
            //    for (int index = treeListNodes.Count - 1; index >= 0; --index)
            //    {
            //      TreeListNode node = treeListNodes[index];
            //      if (BCE.Data.Convert.ToDecimal(node[(object) "Qty"]) == Decimal.Zero)
            //        ((DataRowView) this.gvBOMBS.GetDataRecordByNode(node)).Delete();
            //    }
            //  }
            //  finally
            //  {
            //    this.gvBOMBS.EndUpdate();
            //  }
            //}
        }



        private void SetModuleFeaturesGrid()
        {
            this.myColLocationVisibleIndex1 = this.colLocation.VisibleIndex;
            this.myColBatchNoVisibleIndex1 = this.colBatchNo.VisibleIndex;
            ModuleController moduleController = ModuleControl.GetOrCreate(this.myDBSetting).ModuleController;
            if (!moduleController.MultiLocationStock.Enable)
            {
                this.myColLocationVisibleIndex1 = this.colLocation.VisibleIndex;
                this.colLocation.Visible = false;
                this.colLocation.VisibleIndex = -1;
            }
            else
            {
                this.colLocation.Visible = true;
                this.colLocation.VisibleIndex = this.myColLocationVisibleIndex1;
            }
            this.colLocation.OptionsColumn.ShowInCustomizationForm = moduleController.MultiLocationStock.Enable;
            if (!moduleController.BatchNo.Enable)
            {
                this.myColBatchNoVisibleIndex1 = this.colBatchNo.VisibleIndex;
                this.colBatchNo.Visible = false;
                this.colBatchNo.VisibleIndex = -1;
            }
            else
            {
                this.colBatchNo.Visible = true;
                this.colBatchNo.VisibleIndex = this.myColBatchNoVisibleIndex1;
            }
            this.colBatchNo.OptionsColumn.ShowInCustomizationForm = moduleController.BatchNo.Enable;
        }

        //private void tabControl1_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        //{
        //    if (this.tabControl1.SelectedTabPage == this.tabPageBS)
        //    {
        //        Cursor current = Cursor.Current;
        //        Cursor.Current = Cursors.WaitCursor;
        //        try
        //        {
        //            this.SetModuleFeaturesGrid();
        //            foreach (DataRow dataRow in this.myStockWorkOrder.DataTableDetail.Select("IsBOMItem = 'T'"))
        //            {
        //                if (this.myStockWorkOrder.DataTableDetail.Select("ParentDtlKey=" + (object)BCE.Data.Convert.ToInt64(dataRow["DtlKey"])).Length == 0)
        //                    dataRow["IsBOMItem"] = (object)"F";
        //            }
        //            DataRow[] dataRowArray = this.myStockWorkOrder.DataTableDetail.Select("IsBOMItem='F'");
        //            if (dataRowArray.Length != 0)
        //            {
        //                DataTable dtblNonBOMItem = this.myStockWorkOrder.DataTableDetail.Clone();
        //                foreach (DataRow row in dataRowArray)
        //                    dtblNonBOMItem.ImportRow(row);
        //                foreach (DataRow dataRow in (InternalDataCollectionBase)dtblNonBOMItem.Rows)
        //                    dataRow["BatchNo"] = (object)dataRow["BatchNo"].ToString();
        //                //this.gridCtlIBOMAP.DataSource = (object) StockStatus.GetStockStatus(this.myDBSetting, dtblNonBOMItem, (object) this.myStockWorkOrder.DocKey);
        //            }
        //        }
        //        finally
        //        {
        //            Cursor.Current = current;
        //        }
        //    }
        //}

        private void treeListDetail_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            //string[] strArray = new string[6];
            //int index1 = 0;
            //string str1 = "ItemCode";
            //strArray[index1] = str1;
            //int index2 = 1;
            //string str2 = "UOM";
            //strArray[index2] = str2;
            //int index3 = 2;
            //string str3 = "BOMCode";
            //strArray[index3] = str3;
          
            //foreach (string str7 in strArray)
            //{
            //    if (e.Column.FieldName == str7)
            //    {
            //        object obj = e.Node[(object)e.Column];
            //        if (obj == null)
            //        {
            //            break;
            //        }
            //        else
            //        {
            //            e.CellText = obj.ToString();
            //            break;
            //        }
            //    }
            //}
        }

        private void txtEdtStockAssemblyNo_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Control && e.KeyCode == (Keys.Back | Keys.Space) || e.KeyCode == (Keys)115) && this.luEdtDocNoFormat.Visible)
            {
                this.luEdtDocNoFormat.ShowPopup();
                this.luEdtDocNoFormat.Focus();
                e.Handled = true;
            }
        }

        private void FormStockWorkOrderEntry_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.AdjustBottomButtons();
                //FormStockWorkOrderEntry.FormShowEventArgs formShowEventArgs1 = new FormStockWorkOrderEntry.FormShowEventArgs(this, this.myStockWorkOrder);
                //ScriptObject scriptObject1 = this.myScriptObject;
                //string name1 = "OnFormShow";
                //System.Type[] types1 = new System.Type[1];
                //int index1 = 0;
                //System.Type type1 = formShowEventArgs1.GetType();
                //types1[index1] = type1;
                //object[] objArray1 = new object[1];
                //int index2 = 0;
                //FormStockWorkOrderEntry.FormShowEventArgs formShowEventArgs2 = formShowEventArgs1;
                //objArray1[index2] = (object)formShowEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray1);
                //ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
                //string name2 = "OnFormShow";
                //System.Type[] types2 = new System.Type[1];
                //int index3 = 0;
                //System.Type type2 = formShowEventArgs1.GetType();
                //types2[index3] = type2;
                //object[] objArray2 = new object[1];
                //int index4 = 0;
                //FormStockWorkOrderEntry.FormShowEventArgs formShowEventArgs3 = formShowEventArgs1;
                //objArray2[index4] = (object)formShowEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray2);
            }
        }

        private bool CheckBeforePrint(ReportInfo reportInfo)
        {
            //if (!((StockWorkOrder.BeforePreviewDocumentEventArgs) reportInfo.Tag).AllowPrint || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", reportInfo.DocKey) > 0 && !this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_PRINT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0])))
            //  return false;
            //  else
            return true;
        }

        private bool CheckBeforeExport(ReportInfo reportInfo)
        {
            //if (!((StockWorkOrder.BeforePreviewDocumentEventArgs) reportInfo.Tag).AllowExport || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", reportInfo.DocKey) > 0 && !this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_EXPORT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedExportPrintedDocument, new object[0])))
            // return false;
            //else
            return true;
        }

        private void treeListDetail_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (sender != null && e.Value != null && e.Value != DBNull.Value)
            {
                TreeList treeList = sender as TreeList;
                this.mySkipExecuteFormActivated = true;
                try
                {
                    if (treeList.FocusedColumn != null && treeList.FocusedColumn.FieldName == "ItemCode" && (!this.myUseLookupEditToInputItemCode && e.Value.ToString().Length > 0) && !FormPartialMatchItemCode.IsValidAndActiveItemCode(this.myDBSetting, e.Value.ToString()))
                    {
                        DataTable partialMatchItemCode1 = FormPartialMatchItemCode.GetPartialMatchItemCode(this.myDBSetting, e.Value.ToString());
                        if (partialMatchItemCode1 == null || partialMatchItemCode1.Rows.Count == 0)
                        {
                            e.ErrorText = BCE.Localization.Localizer.GetString(FormPartialMatchItemCodeStringId.ErrorMessage_InvalidOrInactiveItemCode, new object[0]);
                            AppMessage.ShowErrorMessage(e.ErrorText);
                            e.Valid = false;
                        }
                        else if (partialMatchItemCode1.Rows.Count == 1)
                        {
                            e.Value = (object)partialMatchItemCode1.Rows[0]["ItemCode"].ToString();
                        }
                        else
                        {
                            using (FormPartialMatchItemCode partialMatchItemCode2 = new FormPartialMatchItemCode(this.myDBSetting, partialMatchItemCode1, e.Value.ToString(), true))
                            {
                                if (partialMatchItemCode2.ShowDialog() == DialogResult.OK)
                                {
                                    e.Value = (object)partialMatchItemCode2.SelectedItemCode;
                                }
                                else
                                {
                                    e.ErrorText = BCE.Localization.Localizer.GetString(FormPartialMatchItemCodeStringId.ErrorMessage_InvalidOrInactiveItemCode, new object[0]);
                                    e.Valid = false;
                                }
                            }
                        }
                    }
                }
                finally
                {
                    this.mySkipExecuteFormActivated = false;
                }
            }
        }

        private void dateEdtDate_Validating(object sender, CancelEventArgs e)
        {
            if (this.myStockWorkOrder.Command != null)
            {
                DateEdit dateEdit1 = sender as DateEdit;
                FiscalYear orCreate = FiscalYear.GetOrCreate(this.myDBSetting);
                if (!orCreate.IsValidTransactionDate(dateEdit1.DateTime))
                {
                    DateTime firstDate;
                    DateTime lastDate;
                    if (orCreate.GetValidTransactionDateRange(out firstDate, out lastDate))
                    {
                        DateEdit dateEdit2 = dateEdit1;
                        // ISSUE: variable of a boxed type
                        InvoicingStringId local = InvoicingStringId.ErrorMessage_ValidDocumentDate;
                        object[] objArray = new object[2];
                        int index1 = 0;
                        string str1 = this.myStockWorkOrder.Command.GeneralSetting.FormatDate(firstDate);
                        objArray[index1] = (object)str1;
                        int index2 = 1;
                        string str2 = this.myStockWorkOrder.Command.GeneralSetting.FormatDate(lastDate);
                        objArray[index2] = (object)str2;
                        string @string = BCE.Localization.Localizer.GetString(local, objArray);
                        dateEdit2.ErrorText = @string;
                    }
                    else
                        dateEdit1.ErrorText = BCE.Localization.Localizer.GetString(InvoicingStringId.ErrorMessage_InvalidDocumentDate, new object[0]);
                    e.Cancel = true;
                    this.myLastValidationSuccess = false;
                }
                else
                    this.myLastValidationSuccess = true;
            }
        }

        private void barItemCopyAsTabDelimitedText_ItemClick(object sender, ItemClickEventArgs e)
        {
            //ClipboardHelper.SetDataObject((object) this.myStockWorkOrder.ExportAsTabDelimiterText());
            //AppMessage.ShowInformationMessage(BCE.Localization.Localizer.GetString( StockWorkOrderStringId.InfoMessage_SwitchToExcelToPaste, new object[0]));
        }

        private void iEditRemark1MRU_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelperRemark1.EditMRUItems();
        }

        private void iEditRemark2MRU_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelperRemark2.EditMRUItems();
        }

        private void iEditRemark3MRU_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelperRemark3.EditMRUItems();
        }

        private void iEditRemark4MRU_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelperRemark4.EditMRUItems();
        }

        private void barBtnConvertLevel_ItemClick(object sender, ItemClickEventArgs e)
        {
            //// string str1 = !this.myStockWorkOrder.IsMultilevel ? "Multi" : "Single";
            // int num1 = 7;
            // FormStockWorkOrderEntry assemblyOrderEntry = this;
            // // ISSUE: variable of a boxed type
            // StockAssemblyStringId  local1 =  StockAssemblyStringId.ConfirmMessage_ChangeLevel;
            // object[] objArray1 = new object[1];
            // int index1 = 0;
            // string str2 = str1;
            // objArray1[index1] = (object) str2;
            // string string1 = BCE.Localization.Localizer.GetString( local1, objArray1);
            // string caption = "Change Level";
            // int num2 = 4;
            // int num3 = 48;
            // int num4 = (int) XtraMessageBox.Show((IWin32Window) assemblyOrderEntry, string1, caption, (MessageBoxButtons) num2, (MessageBoxIcon) num3);
            // if (num1 != num4)
            // {
            //   this.myStockWorkOrder.IsMultilevel = !this.myStockWorkOrder.IsMultilevel;
            //   string str3 = !this.myStockWorkOrder.IsMultilevel ? "Multi" : "Single";
            //   BarButtonItem barButtonItem = this.barBtnConvertLevel;
            //   // ISSUE: variable of a boxed type
            //   StockAssemblyStringId  local2 =  StockAssemblyStringId.ConvertToLevel;
            //   object[] objArray2 = new object[1];
            //   int index2 = 0;
            //   string str4 = str3;
            //   objArray2[index2] = (object) str4;
            //   string string2 = BCE.Localization.Localizer.GetString( local2, objArray2);
            //   barButtonItem.Caption = string2;
            //   BaseRegistryID baseRegistryId = (BaseRegistryID) new EnableMultiLevelStockAssembly();
            //   baseRegistryId.NewValue = (object)  (this.myStockWorkOrder.IsMultilevel ? true : false);
            //   this.myDBReg.SetValue((IRegistryID) baseRegistryId);
            //   this.ToggleMultiLevel();
            // }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.timer2.Stop();
            if (this.myFormItemSearch != null)
            {
                this.myFormItemSearch.Dispose();
                this.myFormItemSearch = (FormItemSearch)null;
            }
        }

        private void splitterControl1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            this.SaveLocalSetting();
        }

        private void SaveLocalSetting()
        {
            WorkOrderKeepAfterSave assemblyKeepAfterSave = new WorkOrderKeepAfterSave();
            int num1 = this.chkedtNextRecord.Checked ? 1 : 0;
            assemblyKeepAfterSave.CheckKeepAfterSave = num1 != 0;
            int height = this.panBottom.Height;
            assemblyKeepAfterSave.InstantInfoHeight = height;
            int num2 = this.WindowState == FormWindowState.Maximized ? 1 : 0;
            assemblyKeepAfterSave.MaximizeWindow = num2 != 0;
            string fileName = "StockWorkOrderCondition.setting";
            PersistenceUtil.SaveUserSetting((object)assemblyKeepAfterSave, fileName);
        }



        protected override void Dispose(bool disposing)
        {
            if (!this.myHasUnlinkLookupEditEventHandlers)
            {
                this.UnlinkLookupEditEventHandlers();
                this.myHasUnlinkLookupEditEventHandlers = true;
            }
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStockWorkOrderEntry));
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelHeader = new DevExpress.XtraEditors.PanelControl();
            this.txtProjNo = new DevExpress.XtraEditors.TextEdit();
            this.luProjNo = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDebtorName = new DevExpress.XtraEditors.TextEdit();
            this.luDebtorCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.luLocation = new DevExpress.XtraEditors.LookUpEdit();
            this.mruEdtDescription = new DevExpress.XtraEditors.MRUEdit();
            this.lblLocation = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtMachineName = new DevExpress.XtraEditors.TextEdit();
            this.luMachineCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.deTarget = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.deProduction = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.lblCancelled = new System.Windows.Forms.Label();
            this.textEdtRefDocNo = new DevExpress.XtraEditors.TextEdit();
            this.dateEdtDate = new DevExpress.XtraEditors.DateEdit();
            this.txtEdtStockAssemblyNo = new DevExpress.XtraEditors.TextEdit();
            this.lblRefDocNo = new System.Windows.Forms.Label();
            this.lblStockAdjNo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.luEdtDocNoFormat = new DevExpress.XtraEditors.LookUpEdit();
            this.ceLevel = new DevExpress.XtraEditors.CheckEdit();
            this.xtraTabControlRM = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlItem = new DevExpress.XtraTreeList.TreeList();
            this.colDocKey = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDtlKey = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repItemLuedtItem = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDesc = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemluUOM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colRate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colRateUOM = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colQty = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colUnitCost = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTotalCost = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colItemSeq = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDtlFromDocDtlKey = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colFromBOMCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colParentBOMCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colBOMCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn5 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colProductRatio = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn6 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repitemAPDebtorCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.panBottom = new DevExpress.XtraEditors.PanelControl();
            this.ucInquiryStock1 = new BCE.AutoCount.Inquiry.UserControls.UCInquiryStock();
            this.txtedtEstHPP = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textEdtTotal = new DevExpress.XtraEditors.TextEdit();
            this.panTop = new DevExpress.XtraEditors.PanelControl();
            this.sBtnShowInstant = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnRangeSetting = new DevExpress.XtraEditors.SimpleButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.sBtnSelectAllMain = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnUndo = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnMoveDown = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnMoveUp = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnInsert = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage8 = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlItemSummary = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn4 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn8 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn9 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn10 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colQty2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colLevelSum = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barItemCopyWholeDocument = new DevExpress.XtraBars.BarButtonItem();
            this.barItemCopySelectedDetails = new DevExpress.XtraBars.BarButtonItem();
            this.barItemPasteWholeDocument = new DevExpress.XtraBars.BarButtonItem();
            this.barItemPasteItemDetailOnly = new DevExpress.XtraBars.BarButtonItem();
            this.iUndoMaster = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barItemCopyFrom = new DevExpress.XtraBars.BarButtonItem();
            this.barItemCopyTo = new DevExpress.XtraBars.BarButtonItem();
            this.iEditRemark1MRU = new DevExpress.XtraBars.BarButtonItem();
            this.iEditRemark2MRU = new DevExpress.XtraBars.BarButtonItem();
            this.iEditRemark3MRU = new DevExpress.XtraBars.BarButtonItem();
            this.iEditRemark4MRU = new DevExpress.XtraBars.BarButtonItem();
            this.panelView = new DevExpress.XtraEditors.PanelControl();
            this.sbtnSave = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSavePreview = new DevExpress.XtraEditors.SimpleButton();
            this.chkedtNextRecord = new DevExpress.XtraEditors.CheckEdit();
            this.sbtnSavePrint = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrint = new BCE.AutoCount.Controls.PrintButton();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnCancelDoc = new DevExpress.XtraEditors.SimpleButton();
            this.btnPreview = new BCE.AutoCount.Controls.PreviewButton();
            this.navigator = new BCE.Controls.Navigator();
            this.sbtnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.tabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabPagePI = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlIBOMPI = new DevExpress.XtraGrid.GridControl();
            this.gvBOMPI = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleAPItemCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleAPUOM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colAPRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPBOMCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleItemBOM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colAPStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPCOGM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPCostPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPSeq = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPFromDocType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPFromDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPFromDocDtlKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPDebtorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemProductDebtorCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.COLAPDebtorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPStyle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPUOMLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPUOMWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COLAPTickness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVolumeUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeightUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumbering = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancelDetail = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSelectAllPI = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDownPI = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUpPI = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInsertAllPI = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUndoPIDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddPIDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDeletePIDtl = new DevExpress.XtraEditors.SimpleButton();
            this.tabPageDetail = new DevExpress.XtraTab.XtraTabPage();
            this.tabPageBS = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlIBOMBS = new DevExpress.XtraGrid.GridControl();
            this.gvBOMBS = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBSItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repBOMItemLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colBOMDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBSCostPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColBSCostMax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBSSeq = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemBSItemCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnSelectAllBS = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDownBS = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUpBS = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnInsertBS = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUndoBSDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddBSDTL = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDeleteBSDTL = new DevExpress.XtraEditors.SimpleButton();
            this.tabPageAP = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlIBOMAP = new DevExpress.XtraGrid.GridControl();
            this.gvBOMAP = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDocDtlKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnSelectAllAP = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDownAP = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUPAP = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInsertAllAP = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUndoAPDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddAPDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDeleteAPDtl = new DevExpress.XtraEditors.SimpleButton();
            this.tabPageOvd = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlIBOMOvd = new DevExpress.XtraGrid.GridControl();
            this.gvBOMOvd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOvdOverheadCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleOvdOverheadCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOvdDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOvdAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOvdSeq = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnSelectAllOvd = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDownOvd = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUpOvd = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInsertOvd = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUndoOvdDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddOvdDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDeleteOvdDtl = new DevExpress.XtraEditors.SimpleButton();
            this.tabPageMoreHeader = new DevExpress.XtraTab.XtraTabPage();
            this.labelRemark4 = new System.Windows.Forms.Label();
            this.labelRemark3 = new System.Windows.Forms.Label();
            this.labelRemark2 = new System.Windows.Forms.Label();
            this.labelRemark1 = new System.Windows.Forms.Label();
            this.mruEdtRemark1 = new DevExpress.XtraEditors.MRUEdit();
            this.mruEdtRemark2 = new DevExpress.XtraEditors.MRUEdit();
            this.mruEdtRemark3 = new DevExpress.XtraEditors.MRUEdit();
            this.mruEdtRemark4 = new DevExpress.XtraEditors.MRUEdit();
            this.tabPageExternalLink = new DevExpress.XtraTab.XtraTabPage();
            this.externalLinkBox1 = new BCE.Controls.ExternalLinkBox();
            this.tabPageNote = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdtNote = new BCE.Controls.MemoEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.tlItemCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repLuedtItem = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tlDesc = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlFurtherDescription = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repBtnedtFurtherDesc = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.tlLocation = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repLuedtLocation = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tlBatchNo = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repLuedtBatchNo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tlProjNo = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repLuedtProjNo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tlDeptNo = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repLuedtDeptNo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tlRate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlQty = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlItemCost = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlOverheadCost = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlSubTotal = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlRemark = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlPrintOut = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.tlSeq = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlNumbering = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlIsBOMItem = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBatchNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReqQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHandQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHandBal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvailableQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvailableBal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.barItemCopyAsTabDelimitedText = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnSaveInKIVFolder = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.iEditDescriptionMRU = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnConvertLevel = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnTransferFromSO = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnCheckTransferredToStatus = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelHeader)).BeginInit();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luProjNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebtorName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luDebtorCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMachineName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luMachineCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTarget.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTarget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deProduction.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deProduction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdtRefDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdtDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdtDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEdtStockAssemblyNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luEdtDocNoFormat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlRM)).BeginInit();
            this.xtraTabControlRM.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLuedtItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemluUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repitemAPDebtorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panBottom)).BeginInit();
            this.panBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtedtEstHPP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panTop)).BeginInit();
            this.panTop.SuspendLayout();
            this.xtraTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlItemSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelView)).BeginInit();
            this.panelView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtNextRecord.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPagePI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleAPItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleAPUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleItemBOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemProductDebtorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.tabPageDetail.SuspendLayout();
            this.tabPageBS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBOMItemLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemBSItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.tabPageAP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            this.tabPageOvd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMOvd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMOvd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleOvdOverheadCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.tabPageMoreHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark4.Properties)).BeginInit();
            this.tabPageExternalLink.SuspendLayout();
            this.tabPageNote.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBtnedtFurtherDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtBatchNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtProjNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtDeptNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            // 
            // panelHeader
            // 
            this.panelHeader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelHeader.Controls.Add(this.txtProjNo);
            this.panelHeader.Controls.Add(this.luProjNo);
            this.panelHeader.Controls.Add(this.label6);
            this.panelHeader.Controls.Add(this.label4);
            this.panelHeader.Controls.Add(this.txtDebtorName);
            this.panelHeader.Controls.Add(this.luDebtorCode);
            this.panelHeader.Controls.Add(this.label2);
            this.panelHeader.Controls.Add(this.luLocation);
            this.panelHeader.Controls.Add(this.mruEdtDescription);
            this.panelHeader.Controls.Add(this.lblLocation);
            this.panelHeader.Controls.Add(this.lblDescription);
            this.panelHeader.Controls.Add(this.txtMachineName);
            this.panelHeader.Controls.Add(this.luMachineCode);
            this.panelHeader.Controls.Add(this.label5);
            this.panelHeader.Controls.Add(this.deTarget);
            this.panelHeader.Controls.Add(this.label3);
            this.panelHeader.Controls.Add(this.deProduction);
            this.panelHeader.Controls.Add(this.label1);
            this.panelHeader.Controls.Add(this.txtStatus);
            this.panelHeader.Controls.Add(this.label9);
            this.panelHeader.Controls.Add(this.lblCancelled);
            this.panelHeader.Controls.Add(this.textEdtRefDocNo);
            this.panelHeader.Controls.Add(this.dateEdtDate);
            this.panelHeader.Controls.Add(this.txtEdtStockAssemblyNo);
            this.panelHeader.Controls.Add(this.lblRefDocNo);
            this.panelHeader.Controls.Add(this.lblStockAdjNo);
            this.panelHeader.Controls.Add(this.lblDate);
            this.panelHeader.Controls.Add(this.luEdtDocNoFormat);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 22);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(984, 156);
            this.panelHeader.TabIndex = 1;
            this.panelHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelHeader_MouseDown);
            // 
            // txtProjNo
            // 
            this.txtProjNo.Location = new System.Drawing.Point(232, 96);
            this.txtProjNo.Name = "txtProjNo";
            this.txtProjNo.Properties.ReadOnly = true;
            this.txtProjNo.Size = new System.Drawing.Size(217, 20);
            this.txtProjNo.TabIndex = 30;
            // 
            // luProjNo
            // 
            this.luProjNo.Location = new System.Drawing.Point(105, 96);
            this.luProjNo.Name = "luProjNo";
            this.luProjNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luProjNo.Size = new System.Drawing.Size(118, 20);
            this.luProjNo.TabIndex = 28;
            this.luProjNo.EditValueChanged += new System.EventHandler(this.luProjNo_EditValueChanged);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(17, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 17);
            this.label6.TabIndex = 29;
            this.label6.Text = "ProjNo";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(16, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 17);
            this.label4.TabIndex = 27;
            this.label4.Text = "Debtor Name";
            // 
            // txtDebtorName
            // 
            this.txtDebtorName.Location = new System.Drawing.Point(105, 52);
            this.txtDebtorName.Name = "txtDebtorName";
            this.txtDebtorName.Size = new System.Drawing.Size(344, 20);
            this.txtDebtorName.TabIndex = 26;
            // 
            // luDebtorCode
            // 
            this.luDebtorCode.Location = new System.Drawing.Point(105, 30);
            this.luDebtorCode.Name = "luDebtorCode";
            this.luDebtorCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luDebtorCode.Size = new System.Drawing.Size(118, 20);
            this.luDebtorCode.TabIndex = 24;
            this.luDebtorCode.EditValueChanged += new System.EventHandler(this.luDebtorCode_EditValueChanged);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(16, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 17);
            this.label2.TabIndex = 25;
            this.label2.Text = "Debtor Code";
            // 
            // luLocation
            // 
            this.luLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.luLocation.Location = new System.Drawing.Point(841, 76);
            this.luLocation.Name = "luLocation";
            this.luLocation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luLocation.Size = new System.Drawing.Size(118, 20);
            this.luLocation.TabIndex = 0;
            // 
            // mruEdtDescription
            // 
            this.mruEdtDescription.Location = new System.Drawing.Point(106, 119);
            this.mruEdtDescription.Name = "mruEdtDescription";
            this.mruEdtDescription.Size = new System.Drawing.Size(580, 20);
            this.mruEdtDescription.TabIndex = 11;
            // 
            // lblLocation
            // 
            this.lblLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocation.BackColor = System.Drawing.Color.Transparent;
            this.lblLocation.Location = new System.Drawing.Point(725, 79);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(61, 13);
            this.lblLocation.TabIndex = 1;
            this.lblLocation.Text = "Location";
            // 
            // lblDescription
            // 
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Location = new System.Drawing.Point(17, 120);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(76, 17);
            this.lblDescription.TabIndex = 10;
            this.lblDescription.Text = "Description";
            // 
            // txtMachineName
            // 
            this.txtMachineName.Location = new System.Drawing.Point(232, 75);
            this.txtMachineName.Name = "txtMachineName";
            this.txtMachineName.Properties.ReadOnly = true;
            this.txtMachineName.Size = new System.Drawing.Size(217, 20);
            this.txtMachineName.TabIndex = 23;
            // 
            // luMachineCode
            // 
            this.luMachineCode.Location = new System.Drawing.Point(105, 74);
            this.luMachineCode.Name = "luMachineCode";
            this.luMachineCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luMachineCode.Size = new System.Drawing.Size(118, 20);
            this.luMachineCode.TabIndex = 19;
            this.luMachineCode.EditValueChanged += new System.EventHandler(this.luMachineCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(16, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 20;
            this.label5.Text = "Machine No";
            // 
            // deTarget
            // 
            this.deTarget.EditValue = new System.DateTime(2019, 2, 14, 0, 0, 0, 0);
            this.deTarget.Location = new System.Drawing.Point(331, 8);
            this.deTarget.Name = "deTarget";
            this.deTarget.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.deTarget.Properties.NullDate = "";
            this.deTarget.Size = new System.Drawing.Size(118, 20);
            this.deTarget.TabIndex = 18;
            this.deTarget.EditValueChanged += new System.EventHandler(this.deTarget_EditValueChanged);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(226, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 18);
            this.label3.TabIndex = 17;
            this.label3.Text = "Production End Date";
            // 
            // deProduction
            // 
            this.deProduction.EditValue = new System.DateTime(2019, 2, 14, 0, 0, 0, 0);
            this.deProduction.Location = new System.Drawing.Point(106, 8);
            this.deProduction.Name = "deProduction";
            this.deProduction.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.deProduction.Properties.NullDate = "";
            this.deProduction.Size = new System.Drawing.Size(118, 20);
            this.deProduction.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(725, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Status";
            // 
            // txtStatus
            // 
            this.txtStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatus.Location = new System.Drawing.Point(841, 99);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtStatus.Properties.Appearance.Options.UseForeColor = true;
            this.txtStatus.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtStatus.Properties.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(118, 18);
            this.txtStatus.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(16, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 14);
            this.label9.TabIndex = 2;
            this.label9.Text = "Production Date";
            // 
            // lblCancelled
            // 
            this.lblCancelled.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCancelled.BackColor = System.Drawing.Color.Transparent;
            this.lblCancelled.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.lblCancelled.ForeColor = System.Drawing.Color.Red;
            this.lblCancelled.Location = new System.Drawing.Point(503, 58);
            this.lblCancelled.Name = "lblCancelled";
            this.lblCancelled.Size = new System.Drawing.Size(216, 43);
            this.lblCancelled.TabIndex = 3;
            this.lblCancelled.Text = "CANCELLED";
            // 
            // textEdtRefDocNo
            // 
            this.textEdtRefDocNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdtRefDocNo.Location = new System.Drawing.Point(841, 53);
            this.textEdtRefDocNo.Name = "textEdtRefDocNo";
            this.textEdtRefDocNo.Size = new System.Drawing.Size(118, 20);
            this.textEdtRefDocNo.TabIndex = 4;
            // 
            // dateEdtDate
            // 
            this.dateEdtDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEdtDate.EditValue = new System.DateTime(2019, 2, 14, 0, 0, 0, 0);
            this.dateEdtDate.Location = new System.Drawing.Point(841, 30);
            this.dateEdtDate.Name = "dateEdtDate";
            this.dateEdtDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.dateEdtDate.Properties.NullDate = "";
            this.dateEdtDate.Size = new System.Drawing.Size(118, 20);
            this.dateEdtDate.TabIndex = 5;
            this.dateEdtDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdtDate_Validating);
            this.dateEdtDate.Validated += new System.EventHandler(this.txtEdtStockAssemblyNo_EditValueChanged);
            // 
            // txtEdtStockAssemblyNo
            // 
            this.txtEdtStockAssemblyNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEdtStockAssemblyNo.Location = new System.Drawing.Point(841, 8);
            this.txtEdtStockAssemblyNo.Name = "txtEdtStockAssemblyNo";
            this.txtEdtStockAssemblyNo.Size = new System.Drawing.Size(118, 20);
            this.txtEdtStockAssemblyNo.TabIndex = 6;
            this.txtEdtStockAssemblyNo.EditValueChanged += new System.EventHandler(this.txtEdtStockAssemblyNo_EditValueChanged);
            this.txtEdtStockAssemblyNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEdtStockAssemblyNo_KeyDown);
            // 
            // lblRefDocNo
            // 
            this.lblRefDocNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRefDocNo.BackColor = System.Drawing.Color.Transparent;
            this.lblRefDocNo.Location = new System.Drawing.Point(725, 56);
            this.lblRefDocNo.Name = "lblRefDocNo";
            this.lblRefDocNo.Size = new System.Drawing.Size(101, 23);
            this.lblRefDocNo.TabIndex = 7;
            this.lblRefDocNo.Text = "RefDocNo";
            // 
            // lblStockAdjNo
            // 
            this.lblStockAdjNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStockAdjNo.BackColor = System.Drawing.Color.Transparent;
            this.lblStockAdjNo.Location = new System.Drawing.Point(725, 11);
            this.lblStockAdjNo.Name = "lblStockAdjNo";
            this.lblStockAdjNo.Size = new System.Drawing.Size(114, 23);
            this.lblStockAdjNo.TabIndex = 8;
            this.lblStockAdjNo.Text = "Work Order No.";
            // 
            // lblDate
            // 
            this.lblDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(725, 31);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(101, 23);
            this.lblDate.TabIndex = 9;
            this.lblDate.Text = "Work Order Date";
            // 
            // luEdtDocNoFormat
            // 
            this.luEdtDocNoFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.luEdtDocNoFormat.Location = new System.Drawing.Point(841, 8);
            this.luEdtDocNoFormat.Name = "luEdtDocNoFormat";
            this.luEdtDocNoFormat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luEdtDocNoFormat.Size = new System.Drawing.Size(118, 20);
            this.luEdtDocNoFormat.TabIndex = 12;
            this.luEdtDocNoFormat.TabStop = false;
            this.luEdtDocNoFormat.EditValueChanged += new System.EventHandler(this.luEdtDocNoFormat_EditValueChanged);
            // 
            // ceLevel
            // 
            this.ceLevel.Location = new System.Drawing.Point(16, 8);
            this.ceLevel.Name = "ceLevel";
            this.ceLevel.Properties.Caption = "Summary per Level";
            this.ceLevel.Properties.ValueChecked = "T";
            this.ceLevel.Properties.ValueUnchecked = "F";
            this.ceLevel.Size = new System.Drawing.Size(126, 19);
            this.ceLevel.TabIndex = 10;
            this.ceLevel.CheckedChanged += new System.EventHandler(this.ceLevel_CheckedChanged);
            // 
            // xtraTabControlRM
            // 
            this.xtraTabControlRM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlRM.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControlRM.Name = "xtraTabControlRM";
            this.xtraTabControlRM.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControlRM.Size = new System.Drawing.Size(978, 408);
            this.xtraTabControlRM.TabIndex = 31;
            this.xtraTabControlRM.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage8});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridCtlItem);
            this.xtraTabPage2.Controls.Add(this.splitterControl1);
            this.xtraTabPage2.Controls.Add(this.panBottom);
            this.xtraTabPage2.Controls.Add(this.panTop);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(972, 380);
            this.xtraTabPage2.Text = "Detail";
            // 
            // gridCtlItem
            // 
            this.gridCtlItem.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colDocKey,
            this.colDtlKey,
            this.gridColumn2,
            this.colDesc,
            this.gridColumn3,
            this.colRate,
            this.colRateUOM,
            this.colQty,
            this.colUnitCost,
            this.colTotalCost,
            this.colItemSeq,
            this.colDtlFromDocDtlKey,
            this.colFromBOMCode,
            this.colParentBOMCode,
            this.colBOMCode,
            this.treeListColumn5,
            this.colProductRatio,
            this.treeListColumn3,
            this.treeListColumn6});
            this.gridCtlItem.CustomizationFormBounds = new System.Drawing.Rectangle(1127, 460, 234, 209);
            this.gridCtlItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlItem.KeyFieldName = "FromBOMCode";
            this.gridCtlItem.Location = new System.Drawing.Point(5, 34);
            this.gridCtlItem.Name = "gridCtlItem";
            this.gridCtlItem.OptionsBehavior.PopulateServiceColumns = true;
            this.gridCtlItem.ParentFieldName = "ParentBOMCode";
            this.gridCtlItem.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemluUOM,
            this.repItemLuedtItem,
            this.repitemAPDebtorCode});
            this.gridCtlItem.Size = new System.Drawing.Size(967, 212);
            this.gridCtlItem.TabIndex = 4;
            this.gridCtlItem.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.treeListDetail_CustomDrawNodeCell);
            this.gridCtlItem.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.gridCtlItem_CellValueChanged);
            // 
            // colDocKey
            // 
            this.colDocKey.Caption = "DocKey";
            this.colDocKey.FieldName = "DocKey";
            this.colDocKey.Name = "colDocKey";
            this.colDocKey.OptionsColumn.AllowEdit = false;
            this.colDocKey.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDtlKey
            // 
            this.colDtlKey.Caption = "DtlKey";
            this.colDtlKey.FieldName = "DtlKey";
            this.colDtlKey.Name = "colDtlKey";
            this.colDtlKey.OptionsColumn.AllowEdit = false;
            this.colDtlKey.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Item Code";
            this.gridColumn2.ColumnEdit = this.repItemLuedtItem;
            this.gridColumn2.FieldName = "ItemCode";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 82;
            // 
            // repItemLuedtItem
            // 
            this.repItemLuedtItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLuedtItem.Name = "repItemLuedtItem";
            // 
            // colDesc
            // 
            this.colDesc.Caption = "Description";
            this.colDesc.FieldName = "Description";
            this.colDesc.Name = "colDesc";
            this.colDesc.OptionsColumn.AllowEdit = false;
            this.colDesc.Visible = true;
            this.colDesc.VisibleIndex = 2;
            this.colDesc.Width = 166;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "UOM";
            this.gridColumn3.ColumnEdit = this.repositoryItemluUOM;
            this.gridColumn3.FieldName = "UOM";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 51;
            // 
            // repositoryItemluUOM
            // 
            this.repositoryItemluUOM.AutoHeight = false;
            this.repositoryItemluUOM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemluUOM.Name = "repositoryItemluUOM";
            // 
            // colRate
            // 
            this.colRate.Caption = "Rate BOM";
            this.colRate.FieldName = "Rate";
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowEdit = false;
            this.colRate.Visible = true;
            this.colRate.VisibleIndex = 6;
            this.colRate.Width = 50;
            // 
            // colRateUOM
            // 
            this.colRateUOM.Caption = "RateUOM";
            this.colRateUOM.FieldName = "RateUOM";
            this.colRateUOM.Name = "colRateUOM";
            this.colRateUOM.OptionsColumn.AllowEdit = false;
            this.colRateUOM.Visible = true;
            this.colRateUOM.VisibleIndex = 5;
            this.colRateUOM.Width = 67;
            // 
            // colQty
            // 
            this.colQty.FieldName = "Qty";
            this.colQty.Name = "colQty";
            this.colQty.OptionsColumn.AllowEdit = false;
            this.colQty.Visible = true;
            this.colQty.VisibleIndex = 8;
            this.colQty.Width = 47;
            // 
            // colUnitCost
            // 
            this.colUnitCost.Caption = "Unit Cost";
            this.colUnitCost.FieldName = "UnitCost";
            this.colUnitCost.Name = "colUnitCost";
            this.colUnitCost.OptionsColumn.AllowEdit = false;
            this.colUnitCost.Visible = true;
            this.colUnitCost.VisibleIndex = 9;
            this.colUnitCost.Width = 59;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 10;
            this.colTotalCost.Width = 71;
            // 
            // colItemSeq
            // 
            this.colItemSeq.Caption = "Seq";
            this.colItemSeq.FieldName = "Seq";
            this.colItemSeq.Name = "colItemSeq";
            this.colItemSeq.OptionsColumn.AllowEdit = false;
            this.colItemSeq.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDtlFromDocDtlKey
            // 
            this.colDtlFromDocDtlKey.Caption = "FromDocDtlKey";
            this.colDtlFromDocDtlKey.FieldName = "FromDocDtlKey";
            this.colDtlFromDocDtlKey.Name = "colDtlFromDocDtlKey";
            this.colDtlFromDocDtlKey.OptionsColumn.AllowEdit = false;
            this.colDtlFromDocDtlKey.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colFromBOMCode
            // 
            this.colFromBOMCode.Caption = "From BOMCode";
            this.colFromBOMCode.FieldName = "FromBOMCode";
            this.colFromBOMCode.Name = "colFromBOMCode";
            this.colFromBOMCode.OptionsColumn.AllowEdit = false;
            this.colFromBOMCode.Width = 142;
            // 
            // colParentBOMCode
            // 
            this.colParentBOMCode.Caption = "ParentBOMCode";
            this.colParentBOMCode.FieldName = "ParentBOMCode";
            this.colParentBOMCode.Name = "colParentBOMCode";
            this.colParentBOMCode.OptionsColumn.AllowEdit = false;
            this.colParentBOMCode.Visible = true;
            this.colParentBOMCode.VisibleIndex = 12;
            this.colParentBOMCode.Width = 73;
            // 
            // colBOMCode
            // 
            this.colBOMCode.Caption = "BOM Code";
            this.colBOMCode.FieldName = "BOMCode";
            this.colBOMCode.Name = "colBOMCode";
            this.colBOMCode.OptionsColumn.AllowEdit = false;
            this.colBOMCode.OptionsColumn.ShowInCustomizationForm = false;
            this.colBOMCode.Visible = true;
            this.colBOMCode.VisibleIndex = 0;
            this.colBOMCode.Width = 77;
            // 
            // treeListColumn5
            // 
            this.treeListColumn5.Caption = "Level";
            this.treeListColumn5.FieldName = "Level";
            this.treeListColumn5.Name = "treeListColumn5";
            this.treeListColumn5.OptionsColumn.AllowEdit = false;
            this.treeListColumn5.Visible = true;
            this.treeListColumn5.VisibleIndex = 11;
            this.treeListColumn5.Width = 57;
            // 
            // colProductRatio
            // 
            this.colProductRatio.Caption = "Product Ratio %";
            this.colProductRatio.FieldName = "ProductRatio";
            this.colProductRatio.Name = "colProductRatio";
            this.colProductRatio.OptionsColumn.AllowEdit = false;
            this.colProductRatio.Visible = true;
            this.colProductRatio.VisibleIndex = 7;
            this.colProductRatio.Width = 87;
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "From Item Code";
            this.treeListColumn3.FieldName = "FromItemCode";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.OptionsColumn.AllowEdit = false;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 13;
            this.treeListColumn3.Width = 76;
            // 
            // treeListColumn6
            // 
            this.treeListColumn6.Caption = "Item Type";
            this.treeListColumn6.FieldName = "ItemType";
            this.treeListColumn6.Name = "treeListColumn6";
            this.treeListColumn6.OptionsColumn.AllowEdit = false;
            this.treeListColumn6.Visible = true;
            this.treeListColumn6.VisibleIndex = 3;
            this.treeListColumn6.Width = 70;
            // 
            // repitemAPDebtorCode
            // 
            this.repitemAPDebtorCode.AutoHeight = false;
            this.repitemAPDebtorCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repitemAPDebtorCode.Name = "repitemAPDebtorCode";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(0, 34);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 212);
            this.splitterControl1.TabIndex = 0;
            this.splitterControl1.TabStop = false;
            this.splitterControl1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitterControl1_SplitterMoved);
            // 
            // panBottom
            // 
            this.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panBottom.Controls.Add(this.ucInquiryStock1);
            this.panBottom.Controls.Add(this.txtedtEstHPP);
            this.panBottom.Controls.Add(this.label10);
            this.panBottom.Controls.Add(this.label11);
            this.panBottom.Controls.Add(this.label12);
            this.panBottom.Controls.Add(this.textEdtTotal);
            this.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panBottom.Location = new System.Drawing.Point(0, 246);
            this.panBottom.Name = "panBottom";
            this.panBottom.Size = new System.Drawing.Size(972, 134);
            this.panBottom.TabIndex = 2;
            // 
            // ucInquiryStock1
            // 
            this.ucInquiryStock1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucInquiryStock1.Location = new System.Drawing.Point(0, 0);
            this.ucInquiryStock1.Name = "ucInquiryStock1";
            this.ucInquiryStock1.Size = new System.Drawing.Size(972, 134);
            this.ucInquiryStock1.TabIndex = 0;
            this.ucInquiryStock1.TabStop = false;
            this.ucInquiryStock1.Visible = false;
            // 
            // txtedtEstHPP
            // 
            this.txtedtEstHPP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtedtEstHPP.Location = new System.Drawing.Point(834, 10);
            this.txtedtEstHPP.Name = "txtedtEstHPP";
            this.txtedtEstHPP.Properties.ReadOnly = true;
            this.txtedtEstHPP.Size = new System.Drawing.Size(118, 20);
            this.txtedtEstHPP.TabIndex = 1;
            this.txtedtEstHPP.Visible = false;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(827, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 23);
            this.label10.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(827, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 23);
            this.label11.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(827, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 23);
            this.label12.TabIndex = 5;
            // 
            // textEdtTotal
            // 
            this.textEdtTotal.Location = new System.Drawing.Point(827, 6);
            this.textEdtTotal.Name = "textEdtTotal";
            this.textEdtTotal.Size = new System.Drawing.Size(100, 20);
            this.textEdtTotal.TabIndex = 6;
            // 
            // panTop
            // 
            this.panTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panTop.Controls.Add(this.sBtnShowInstant);
            this.panTop.Controls.Add(this.sBtnRangeSetting);
            this.panTop.Controls.Add(this.sBtnSelectAllMain);
            this.panTop.Controls.Add(this.sBtnUndo);
            this.panTop.Controls.Add(this.sBtnSearch);
            this.panTop.Controls.Add(this.sBtnAdd);
            this.panTop.Controls.Add(this.sBtnRemove);
            this.panTop.Controls.Add(this.sBtnMoveDown);
            this.panTop.Controls.Add(this.sBtnMoveUp);
            this.panTop.Controls.Add(this.sBtnInsert);
            this.panTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panTop.Location = new System.Drawing.Point(0, 0);
            this.panTop.Name = "panTop";
            this.panTop.Size = new System.Drawing.Size(972, 34);
            this.panTop.TabIndex = 3;
            this.panTop.Visible = false;
            // 
            // sBtnShowInstant
            // 
            this.sBtnShowInstant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sBtnShowInstant.Location = new System.Drawing.Point(859, 5);
            this.sBtnShowInstant.Name = "sBtnShowInstant";
            this.sBtnShowInstant.Size = new System.Drawing.Size(106, 23);
            this.sBtnShowInstant.TabIndex = 1;
            this.sBtnShowInstant.TabStop = false;
            this.sBtnShowInstant.Text = "Show Instant Info";
            this.sBtnShowInstant.Click += new System.EventHandler(this.sBtnShowInstant_Click);
            // 
            // sBtnRangeSetting
            // 
            this.sBtnRangeSetting.ImageIndex = 5;
            this.sBtnRangeSetting.ImageList = this.imageList1;
            this.sBtnRangeSetting.Location = new System.Drawing.Point(717, 5);
            this.sBtnRangeSetting.Name = "sBtnRangeSetting";
            this.sBtnRangeSetting.Size = new System.Drawing.Size(54, 23);
            this.sBtnRangeSetting.TabIndex = 2;
            this.sBtnRangeSetting.TabStop = false;
            this.sBtnRangeSetting.Text = "Range";
            this.sBtnRangeSetting.Visible = false;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            // 
            // sBtnSelectAllMain
            // 
            this.sBtnSelectAllMain.Image = ((System.Drawing.Image)(resources.GetObject("sBtnSelectAllMain.Image")));
            this.sBtnSelectAllMain.ImageIndex = 4;
            this.sBtnSelectAllMain.ImageList = this.imageList1;
            this.sBtnSelectAllMain.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBtnSelectAllMain.Location = new System.Drawing.Point(240, 5);
            this.sBtnSelectAllMain.Name = "sBtnSelectAllMain";
            this.sBtnSelectAllMain.Size = new System.Drawing.Size(31, 23);
            this.sBtnSelectAllMain.TabIndex = 3;
            this.sBtnSelectAllMain.TabStop = false;
            this.sBtnSelectAllMain.Click += new System.EventHandler(this.sBtnSelectAllMain_Click);
            // 
            // sBtnUndo
            // 
            this.sBtnUndo.Image = ((System.Drawing.Image)(resources.GetObject("sBtnUndo.Image")));
            this.sBtnUndo.ImageIndex = 3;
            this.sBtnUndo.ImageList = this.imageList1;
            this.sBtnUndo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBtnUndo.Location = new System.Drawing.Point(127, 5);
            this.sBtnUndo.Name = "sBtnUndo";
            this.sBtnUndo.Size = new System.Drawing.Size(33, 23);
            this.sBtnUndo.TabIndex = 4;
            this.sBtnUndo.TabStop = false;
            this.sBtnUndo.Click += new System.EventHandler(this.sBtnUndoTree_Click);
            // 
            // sBtnSearch
            // 
            this.sBtnSearch.ImageIndex = 6;
            this.sBtnSearch.ImageList = this.imageList1;
            this.sBtnSearch.Location = new System.Drawing.Point(712, 5);
            this.sBtnSearch.Name = "sBtnSearch";
            this.sBtnSearch.Size = new System.Drawing.Size(75, 23);
            this.sBtnSearch.TabIndex = 5;
            this.sBtnSearch.TabStop = false;
            this.sBtnSearch.Text = "Search";
            this.sBtnSearch.Visible = false;
            this.sBtnSearch.Click += new System.EventHandler(this.sBtnSearch_Click);
            // 
            // sBtnAdd
            // 
            this.sBtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("sBtnAdd.Image")));
            this.sBtnAdd.ImageIndex = 0;
            this.sBtnAdd.ImageList = this.imageList1;
            this.sBtnAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBtnAdd.Location = new System.Drawing.Point(22, 5);
            this.sBtnAdd.Name = "sBtnAdd";
            this.sBtnAdd.Size = new System.Drawing.Size(26, 23);
            this.sBtnAdd.TabIndex = 6;
            this.sBtnAdd.TabStop = false;
            this.sBtnAdd.Click += new System.EventHandler(this.sBtnAdd_Click);
            // 
            // sBtnRemove
            // 
            this.sBtnRemove.Image = ((System.Drawing.Image)(resources.GetObject("sBtnRemove.Image")));
            this.sBtnRemove.ImageIndex = 1;
            this.sBtnRemove.ImageList = this.imageList1;
            this.sBtnRemove.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBtnRemove.Location = new System.Drawing.Point(92, 5);
            this.sBtnRemove.Name = "sBtnRemove";
            this.sBtnRemove.Size = new System.Drawing.Size(29, 23);
            this.sBtnRemove.TabIndex = 7;
            this.sBtnRemove.TabStop = false;
            this.sBtnRemove.Click += new System.EventHandler(this.sBtnRemove_Click);
            // 
            // sBtnMoveDown
            // 
            this.sBtnMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("sBtnMoveDown.Image")));
            this.sBtnMoveDown.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBtnMoveDown.Location = new System.Drawing.Point(201, 5);
            this.sBtnMoveDown.Name = "sBtnMoveDown";
            this.sBtnMoveDown.Size = new System.Drawing.Size(32, 23);
            this.sBtnMoveDown.TabIndex = 8;
            this.sBtnMoveDown.TabStop = false;
            this.sBtnMoveDown.Click += new System.EventHandler(this.sBtnMoveDown_Click);
            // 
            // sBtnMoveUp
            // 
            this.sBtnMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("sBtnMoveUp.Image")));
            this.sBtnMoveUp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBtnMoveUp.Location = new System.Drawing.Point(165, 5);
            this.sBtnMoveUp.Name = "sBtnMoveUp";
            this.sBtnMoveUp.Size = new System.Drawing.Size(30, 23);
            this.sBtnMoveUp.TabIndex = 9;
            this.sBtnMoveUp.TabStop = false;
            this.sBtnMoveUp.Text = "Up";
            this.sBtnMoveUp.Click += new System.EventHandler(this.sBtnMoveUp_Click);
            // 
            // sBtnInsert
            // 
            this.sBtnInsert.Image = ((System.Drawing.Image)(resources.GetObject("sBtnInsert.Image")));
            this.sBtnInsert.ImageIndex = 2;
            this.sBtnInsert.ImageList = this.imageList1;
            this.sBtnInsert.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBtnInsert.Location = new System.Drawing.Point(54, 5);
            this.sBtnInsert.Name = "sBtnInsert";
            this.sBtnInsert.Size = new System.Drawing.Size(32, 23);
            this.sBtnInsert.TabIndex = 10;
            this.sBtnInsert.TabStop = false;
            this.sBtnInsert.Click += new System.EventHandler(this.sBtnInsert_Click);
            // 
            // xtraTabPage8
            // 
            this.xtraTabPage8.Controls.Add(this.gridCtlItemSummary);
            this.xtraTabPage8.Controls.Add(this.panelControl5);
            this.xtraTabPage8.Name = "xtraTabPage8";
            this.xtraTabPage8.Size = new System.Drawing.Size(972, 380);
            this.xtraTabPage8.Text = "Summary";
            // 
            // gridCtlItemSummary
            // 
            this.gridCtlItemSummary.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn4,
            this.treeListColumn8,
            this.treeListColumn9,
            this.treeListColumn10,
            this.treeListColumn1,
            this.treeListColumn2,
            this.colQty2,
            this.colLevelSum});
            this.gridCtlItemSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlItemSummary.KeyFieldName = "DtlKey";
            this.gridCtlItemSummary.Location = new System.Drawing.Point(0, 33);
            this.gridCtlItemSummary.Name = "gridCtlItemSummary";
            this.gridCtlItemSummary.OptionsBehavior.PopulateServiceColumns = true;
            this.gridCtlItemSummary.ParentFieldName = "";
            this.gridCtlItemSummary.Size = new System.Drawing.Size(972, 347);
            this.gridCtlItemSummary.TabIndex = 5;
            // 
            // treeListColumn4
            // 
            this.treeListColumn4.Caption = "Description";
            this.treeListColumn4.FieldName = "Description";
            this.treeListColumn4.Name = "treeListColumn4";
            this.treeListColumn4.OptionsColumn.AllowEdit = false;
            this.treeListColumn4.Visible = true;
            this.treeListColumn4.VisibleIndex = 2;
            this.treeListColumn4.Width = 293;
            // 
            // treeListColumn8
            // 
            this.treeListColumn8.Caption = "Unit Cost";
            this.treeListColumn8.FieldName = "UnitCost";
            this.treeListColumn8.Name = "treeListColumn8";
            this.treeListColumn8.OptionsColumn.AllowEdit = false;
            this.treeListColumn8.Visible = true;
            this.treeListColumn8.VisibleIndex = 5;
            this.treeListColumn8.Width = 114;
            // 
            // treeListColumn9
            // 
            this.treeListColumn9.Caption = "Total Cost";
            this.treeListColumn9.FieldName = "TotalCost";
            this.treeListColumn9.Name = "treeListColumn9";
            this.treeListColumn9.OptionsColumn.AllowEdit = false;
            this.treeListColumn9.Visible = true;
            this.treeListColumn9.VisibleIndex = 6;
            this.treeListColumn9.Width = 169;
            // 
            // treeListColumn10
            // 
            this.treeListColumn10.Caption = "DtlKey";
            this.treeListColumn10.FieldName = "DtlKey";
            this.treeListColumn10.Name = "treeListColumn10";
            this.treeListColumn10.OptionsColumn.AllowEdit = false;
            this.treeListColumn10.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Item Code";
            this.treeListColumn1.FieldName = "ItemCode";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 1;
            this.treeListColumn1.Width = 114;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "UOM";
            this.treeListColumn2.FieldName = "UOM";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.OptionsColumn.AllowEdit = false;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 3;
            this.treeListColumn2.Width = 90;
            // 
            // colQty2
            // 
            this.colQty2.Caption = "Qty";
            this.colQty2.FieldName = "Qty";
            this.colQty2.Name = "colQty2";
            this.colQty2.OptionsColumn.AllowEdit = false;
            this.colQty2.Visible = true;
            this.colQty2.VisibleIndex = 4;
            this.colQty2.Width = 100;
            // 
            // colLevelSum
            // 
            this.colLevelSum.Caption = "Level";
            this.colLevelSum.FieldName = "Level";
            this.colLevelSum.Name = "colLevelSum";
            this.colLevelSum.OptionsColumn.AllowEdit = false;
            this.colLevelSum.Visible = true;
            this.colLevelSum.VisibleIndex = 0;
            this.colLevelSum.Width = 36;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.ceLevel);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(972, 33);
            this.panelControl5.TabIndex = 6;
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barItemCopyWholeDocument,
            this.barItemPasteWholeDocument,
            this.barItemPasteItemDetailOnly,
            this.barSubItem2,
            this.barItemCopyFrom,
            this.barItemCopyTo,
            this.barItemCopySelectedDetails,
            this.iUndoMaster,
            this.iEditRemark1MRU,
            this.iEditRemark2MRU,
            this.iEditRemark3MRU,
            this.iEditRemark4MRU,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 24;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(116, 222);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 1";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Edit";
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barItemCopyWholeDocument),
            new DevExpress.XtraBars.LinkPersistInfo(this.barItemCopySelectedDetails),
            new DevExpress.XtraBars.LinkPersistInfo(this.barItemPasteWholeDocument, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barItemPasteItemDetailOnly),
            new DevExpress.XtraBars.LinkPersistInfo(this.iUndoMaster, true)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.Popup += new System.EventHandler(this.barSubItem1_Popup);
            // 
            // barItemCopyWholeDocument
            // 
            this.barItemCopyWholeDocument.Caption = "Copy Whole Document";
            this.barItemCopyWholeDocument.Id = 1;
            this.barItemCopyWholeDocument.Name = "barItemCopyWholeDocument";
            this.barItemCopyWholeDocument.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopyWholeDocument_ItemClick);
            // 
            // barItemCopySelectedDetails
            // 
            this.barItemCopySelectedDetails.Caption = "Copy Selected Details";
            this.barItemCopySelectedDetails.Id = 7;
            this.barItemCopySelectedDetails.Name = "barItemCopySelectedDetails";
            this.barItemCopySelectedDetails.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopySelectedDetails_ItemClick);
            // 
            // barItemPasteWholeDocument
            // 
            this.barItemPasteWholeDocument.Caption = "Paste Whole Document";
            this.barItemPasteWholeDocument.Id = 2;
            this.barItemPasteWholeDocument.Name = "barItemPasteWholeDocument";
            this.barItemPasteWholeDocument.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemPasteWholeDocument_ItemClick);
            // 
            // barItemPasteItemDetailOnly
            // 
            this.barItemPasteItemDetailOnly.Caption = "Paste Item Detail Only";
            this.barItemPasteItemDetailOnly.Id = 3;
            this.barItemPasteItemDetailOnly.Name = "barItemPasteItemDetailOnly";
            this.barItemPasteItemDetailOnly.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemPasteItemDetailOnly_ItemClick);
            // 
            // iUndoMaster
            // 
            this.iUndoMaster.Caption = "Undo Master";
            this.iUndoMaster.Id = 8;
            this.iUndoMaster.Name = "iUndoMaster";
            this.iUndoMaster.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iUndoMaster_ItemClick);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Id = 4;
            this.barSubItem2.Name = "barSubItem2";
            this.barSubItem2.Popup += new System.EventHandler(this.barSubItem2_Popup);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Transfer From Sales Order";
            this.barButtonItem2.Id = 23;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnTransferFromSO_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(984, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 661);
            this.barDockControlBottom.Size = new System.Drawing.Size(984, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 639);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(984, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 639);
            // 
            // barItemCopyFrom
            // 
            this.barItemCopyFrom.Id = 5;
            this.barItemCopyFrom.Name = "barItemCopyFrom";
            this.barItemCopyFrom.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopyFrom_ItemClick);
            // 
            // barItemCopyTo
            // 
            this.barItemCopyTo.Id = 6;
            this.barItemCopyTo.Name = "barItemCopyTo";
            this.barItemCopyTo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopyTo_ItemClick);
            // 
            // iEditRemark1MRU
            // 
            this.iEditRemark1MRU.Id = 18;
            this.iEditRemark1MRU.Name = "iEditRemark1MRU";
            this.iEditRemark1MRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditRemark1MRU_ItemClick);
            // 
            // iEditRemark2MRU
            // 
            this.iEditRemark2MRU.Id = 19;
            this.iEditRemark2MRU.Name = "iEditRemark2MRU";
            this.iEditRemark2MRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditRemark2MRU_ItemClick);
            // 
            // iEditRemark3MRU
            // 
            this.iEditRemark3MRU.Id = 20;
            this.iEditRemark3MRU.Name = "iEditRemark3MRU";
            this.iEditRemark3MRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditRemark3MRU_ItemClick);
            // 
            // iEditRemark4MRU
            // 
            this.iEditRemark4MRU.Id = 21;
            this.iEditRemark4MRU.Name = "iEditRemark4MRU";
            this.iEditRemark4MRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditRemark4MRU_ItemClick);
            // 
            // panelView
            // 
            this.panelView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelView.Controls.Add(this.sbtnSave);
            this.panelView.Controls.Add(this.sbtnSavePreview);
            this.panelView.Controls.Add(this.chkedtNextRecord);
            this.panelView.Controls.Add(this.sbtnSavePrint);
            this.panelView.Controls.Add(this.btnPrint);
            this.panelView.Controls.Add(this.sbtnCancel);
            this.panelView.Controls.Add(this.sbtnEdit);
            this.panelView.Controls.Add(this.sbtnCancelDoc);
            this.panelView.Controls.Add(this.btnPreview);
            this.panelView.Controls.Add(this.navigator);
            this.panelView.Controls.Add(this.sbtnDelete);
            this.panelView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelView.Location = new System.Drawing.Point(0, 614);
            this.panelView.Name = "panelView";
            this.panelView.Size = new System.Drawing.Size(984, 47);
            this.panelView.TabIndex = 2;
            // 
            // sbtnSave
            // 
            this.sbtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnSave.Location = new System.Drawing.Point(637, 12);
            this.sbtnSave.Name = "sbtnSave";
            this.sbtnSave.Size = new System.Drawing.Size(75, 23);
            this.sbtnSave.TabIndex = 6;
            this.sbtnSave.Text = "&Save";
            this.sbtnSave.Click += new System.EventHandler(this.sbtnSave_Click);
            // 
            // sbtnSavePreview
            // 
            this.sbtnSavePreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnSavePreview.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnSavePreview.Location = new System.Drawing.Point(721, 12);
            this.sbtnSavePreview.Name = "sbtnSavePreview";
            this.sbtnSavePreview.Size = new System.Drawing.Size(86, 23);
            this.sbtnSavePreview.TabIndex = 2;
            this.sbtnSavePreview.Text = "Save && &Preview";
            this.sbtnSavePreview.Click += new System.EventHandler(this.sbtnSavePreview_Click);
            // 
            // chkedtNextRecord
            // 
            this.chkedtNextRecord.Location = new System.Drawing.Point(17, 16);
            this.chkedtNextRecord.Name = "chkedtNextRecord";
            this.chkedtNextRecord.Properties.Caption = "After &save, proceed with new Stock Work Order";
            this.chkedtNextRecord.Size = new System.Drawing.Size(259, 19);
            this.chkedtNextRecord.TabIndex = 9;
            this.chkedtNextRecord.CheckedChanged += new System.EventHandler(this.chkedtNextRecord_CheckedChanged);
            // 
            // sbtnSavePrint
            // 
            this.sbtnSavePrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnSavePrint.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnSavePrint.Location = new System.Drawing.Point(816, 12);
            this.sbtnSavePrint.Name = "sbtnSavePrint";
            this.sbtnSavePrint.Size = new System.Drawing.Size(75, 23);
            this.sbtnSavePrint.TabIndex = 8;
            this.sbtnSavePrint.Text = "&Save && Print";
            this.sbtnSavePrint.Click += new System.EventHandler(this.sbtnSavePrint_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(560, 12);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.ReportType = "";
            this.btnPrint.Size = new System.Drawing.Size(72, 23);
            this.btnPrint.TabIndex = 0;
            this.btnPrint.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnCancel.Location = new System.Drawing.Point(897, 12);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.sbtnCancel.TabIndex = 1;
            this.sbtnCancel.Text = "Cancel";
            this.sbtnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sbtnEdit
            // 
            this.sbtnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnEdit.Location = new System.Drawing.Point(633, 12);
            this.sbtnEdit.Name = "sbtnEdit";
            this.sbtnEdit.Size = new System.Drawing.Size(69, 23);
            this.sbtnEdit.TabIndex = 10;
            this.sbtnEdit.Text = "Edit";
            this.sbtnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // sbtnCancelDoc
            // 
            this.sbtnCancelDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancelDoc.Location = new System.Drawing.Point(714, 12);
            this.sbtnCancelDoc.Name = "sbtnCancelDoc";
            this.sbtnCancelDoc.Size = new System.Drawing.Size(93, 23);
            this.sbtnCancelDoc.TabIndex = 4;
            this.sbtnCancelDoc.Text = "Cancel Document";
            this.sbtnCancelDoc.Click += new System.EventHandler(this.btnCancelDoc_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreview.Location = new System.Drawing.Point(482, 12);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.ReportType = "";
            this.btnPreview.Size = new System.Drawing.Size(72, 23);
            this.btnPreview.TabIndex = 3;
            this.btnPreview.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // navigator
            // 
            this.navigator.Location = new System.Drawing.Point(12, 12);
            this.navigator.Name = "navigator";
            this.navigator.Size = new System.Drawing.Size(92, 23);
            this.navigator.TabIndex = 5;
            this.navigator.ButtonClick += new BCE.Controls.NavigatorButtonClickEventHandler(this.navigator_ButtonClick);
            // 
            // sbtnDelete
            // 
            this.sbtnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnDelete.Appearance.ForeColor = System.Drawing.Color.Red;
            this.sbtnDelete.Appearance.Options.UseForeColor = true;
            this.sbtnDelete.Location = new System.Drawing.Point(816, 12);
            this.sbtnDelete.Name = "sbtnDelete";
            this.sbtnDelete.Size = new System.Drawing.Size(75, 23);
            this.sbtnDelete.TabIndex = 7;
            this.sbtnDelete.Text = "Delete";
            this.sbtnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabPage = this.tabPagePI;
            this.tabControl1.Size = new System.Drawing.Size(984, 436);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPagePI,
            this.tabPageDetail,
            this.tabPageBS,
            this.tabPageAP,
            this.tabPageOvd,
            this.tabPageMoreHeader,
            this.tabPageExternalLink,
            this.tabPageNote});
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tabPagePI
            // 
            this.tabPagePI.Controls.Add(this.gridCtlIBOMPI);
            this.tabPagePI.Controls.Add(this.panelControl2);
            this.tabPagePI.Name = "tabPagePI";
            this.tabPagePI.Size = new System.Drawing.Size(978, 408);
            this.tabPagePI.Text = "Product Item";
            // 
            // gridCtlIBOMPI
            // 
            this.gridCtlIBOMPI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlIBOMPI.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlIBOMPI.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlIBOMPI.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlIBOMPI.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlIBOMPI.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlIBOMPI.Location = new System.Drawing.Point(0, 33);
            this.gridCtlIBOMPI.MainView = this.gvBOMPI;
            this.gridCtlIBOMPI.Name = "gridCtlIBOMPI";
            this.gridCtlIBOMPI.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repleAPItemCode,
            this.repleAPUOM,
            this.repleItemBOM,
            this.repItemProductDebtorCode});
            this.gridCtlIBOMPI.Size = new System.Drawing.Size(978, 375);
            this.gridCtlIBOMPI.TabIndex = 4;
            this.gridCtlIBOMPI.UseEmbeddedNavigator = true;
            this.gridCtlIBOMPI.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBOMPI});
            // 
            // gvBOMPI
            // 
            this.gvBOMPI.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPItemCode,
            this.gridColumn1,
            this.colAPUOM,
            this.colAPRate,
            this.colAPQty,
            this.colAPBOMCode,
            this.colAPStatus,
            this.colAPCOGM,
            this.colAPCostPercent,
            this.colAPSeq,
            this.COLAPFromDocType,
            this.COLAPFromDocNo,
            this.COLAPFromDocDtlKey,
            this.COLAPDebtorCode,
            this.COLAPDebtorName,
            this.COLAPStyle,
            this.COLAPLength,
            this.COLAPUOMLength,
            this.COLAPWidth,
            this.COLAPUOMWidth,
            this.COLAPSize,
            this.COLAPTickness,
            this.colVolumeUOM,
            this.colVolume,
            this.colWeightUOM,
            this.colWeight,
            this.colNumbering});
            this.gvBOMPI.GridControl = this.gridCtlIBOMPI;
            this.gvBOMPI.Name = "gvBOMPI";
            this.gvBOMPI.OptionsView.ColumnAutoWidth = false;
            this.gvBOMPI.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvItem_CustomDrawCell);
            this.gvBOMPI.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvBOMPI_FocusedRowChanged);
            this.gvBOMPI.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gvBOMPI_FocusedColumnChanged);
            // 
            // colAPItemCode
            // 
            this.colAPItemCode.Caption = "ItemCode";
            this.colAPItemCode.ColumnEdit = this.repleAPItemCode;
            this.colAPItemCode.FieldName = "ItemCode";
            this.colAPItemCode.Name = "colAPItemCode";
            this.colAPItemCode.Visible = true;
            this.colAPItemCode.VisibleIndex = 0;
            this.colAPItemCode.Width = 98;
            // 
            // repleAPItemCode
            // 
            this.repleAPItemCode.AutoHeight = false;
            this.repleAPItemCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleAPItemCode.Name = "repleAPItemCode";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Description";
            this.gridColumn1.FieldName = "Description";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 132;
            // 
            // colAPUOM
            // 
            this.colAPUOM.Caption = "UOM";
            this.colAPUOM.ColumnEdit = this.repleAPUOM;
            this.colAPUOM.FieldName = "UOM";
            this.colAPUOM.Name = "colAPUOM";
            this.colAPUOM.Visible = true;
            this.colAPUOM.VisibleIndex = 2;
            this.colAPUOM.Width = 53;
            // 
            // repleAPUOM
            // 
            this.repleAPUOM.AutoHeight = false;
            this.repleAPUOM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleAPUOM.Name = "repleAPUOM";
            // 
            // colAPRate
            // 
            this.colAPRate.Caption = "Rate";
            this.colAPRate.FieldName = "Rate";
            this.colAPRate.Name = "colAPRate";
            this.colAPRate.OptionsColumn.AllowEdit = false;
            this.colAPRate.Visible = true;
            this.colAPRate.VisibleIndex = 3;
            this.colAPRate.Width = 53;
            // 
            // colAPQty
            // 
            this.colAPQty.Caption = "Qty";
            this.colAPQty.FieldName = "Qty";
            this.colAPQty.Name = "colAPQty";
            this.colAPQty.Visible = true;
            this.colAPQty.VisibleIndex = 4;
            this.colAPQty.Width = 53;
            // 
            // colAPBOMCode
            // 
            this.colAPBOMCode.Caption = "BOMCode";
            this.colAPBOMCode.ColumnEdit = this.repleItemBOM;
            this.colAPBOMCode.FieldName = "BOMCode";
            this.colAPBOMCode.Name = "colAPBOMCode";
            this.colAPBOMCode.Visible = true;
            this.colAPBOMCode.VisibleIndex = 5;
            this.colAPBOMCode.Width = 79;
            // 
            // repleItemBOM
            // 
            this.repleItemBOM.AutoHeight = false;
            this.repleItemBOM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleItemBOM.Name = "repleItemBOM";
            // 
            // colAPStatus
            // 
            this.colAPStatus.Caption = "Status";
            this.colAPStatus.FieldName = "Status";
            this.colAPStatus.Name = "colAPStatus";
            this.colAPStatus.OptionsColumn.AllowEdit = false;
            this.colAPStatus.Visible = true;
            this.colAPStatus.VisibleIndex = 6;
            this.colAPStatus.Width = 50;
            // 
            // colAPCOGM
            // 
            this.colAPCOGM.Caption = "COGM";
            this.colAPCOGM.FieldName = "COGM";
            this.colAPCOGM.Name = "colAPCOGM";
            this.colAPCOGM.OptionsColumn.AllowEdit = false;
            this.colAPCOGM.Width = 43;
            // 
            // colAPCostPercent
            // 
            this.colAPCostPercent.Caption = "Cost (%)";
            this.colAPCostPercent.FieldName = "CostPercent";
            this.colAPCostPercent.Name = "colAPCostPercent";
            this.colAPCostPercent.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colAPSeq
            // 
            this.colAPSeq.Caption = "Seq";
            this.colAPSeq.FieldName = "Seq";
            this.colAPSeq.Name = "colAPSeq";
            this.colAPSeq.OptionsColumn.AllowEdit = false;
            this.colAPSeq.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // COLAPFromDocType
            // 
            this.COLAPFromDocType.Caption = "FromDocType";
            this.COLAPFromDocType.FieldName = "FromDocType";
            this.COLAPFromDocType.Name = "COLAPFromDocType";
            this.COLAPFromDocType.OptionsColumn.AllowEdit = false;
            this.COLAPFromDocType.Width = 43;
            // 
            // COLAPFromDocNo
            // 
            this.COLAPFromDocNo.Caption = "FromDocNo";
            this.COLAPFromDocNo.FieldName = "FromDocNo";
            this.COLAPFromDocNo.Name = "COLAPFromDocNo";
            this.COLAPFromDocNo.OptionsColumn.AllowEdit = false;
            this.COLAPFromDocNo.Width = 43;
            // 
            // COLAPFromDocDtlKey
            // 
            this.COLAPFromDocDtlKey.Caption = "FromDocDtlKey";
            this.COLAPFromDocDtlKey.FieldName = "FromDocDtlKey";
            this.COLAPFromDocDtlKey.Name = "COLAPFromDocDtlKey";
            this.COLAPFromDocDtlKey.OptionsColumn.AllowEdit = false;
            this.COLAPFromDocDtlKey.Width = 43;
            // 
            // COLAPDebtorCode
            // 
            this.COLAPDebtorCode.Caption = "DebtorCode";
            this.COLAPDebtorCode.ColumnEdit = this.repItemProductDebtorCode;
            this.COLAPDebtorCode.FieldName = "DebtorCode";
            this.COLAPDebtorCode.Name = "COLAPDebtorCode";
            this.COLAPDebtorCode.Visible = true;
            this.COLAPDebtorCode.VisibleIndex = 7;
            this.COLAPDebtorCode.Width = 81;
            // 
            // repItemProductDebtorCode
            // 
            this.repItemProductDebtorCode.AutoHeight = false;
            this.repItemProductDebtorCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemProductDebtorCode.Name = "repItemProductDebtorCode";
            // 
            // COLAPDebtorName
            // 
            this.COLAPDebtorName.Caption = "DebtorName";
            this.COLAPDebtorName.FieldName = "DebtorName";
            this.COLAPDebtorName.Name = "COLAPDebtorName";
            this.COLAPDebtorName.OptionsColumn.AllowEdit = false;
            this.COLAPDebtorName.Visible = true;
            this.COLAPDebtorName.VisibleIndex = 8;
            this.COLAPDebtorName.Width = 140;
            // 
            // COLAPStyle
            // 
            this.COLAPStyle.Caption = "Style";
            this.COLAPStyle.FieldName = "Style";
            this.COLAPStyle.Name = "COLAPStyle";
            this.COLAPStyle.OptionsColumn.AllowEdit = false;
            this.COLAPStyle.Visible = true;
            this.COLAPStyle.VisibleIndex = 9;
            this.COLAPStyle.Width = 39;
            // 
            // COLAPLength
            // 
            this.COLAPLength.Caption = "Length";
            this.COLAPLength.FieldName = "Length";
            this.COLAPLength.Name = "COLAPLength";
            this.COLAPLength.OptionsColumn.AllowEdit = false;
            this.COLAPLength.Visible = true;
            this.COLAPLength.VisibleIndex = 10;
            this.COLAPLength.Width = 64;
            // 
            // COLAPUOMLength
            // 
            this.COLAPUOMLength.Caption = "UOMLength";
            this.COLAPUOMLength.FieldName = "UOMLength";
            this.COLAPUOMLength.Name = "COLAPUOMLength";
            this.COLAPUOMLength.OptionsColumn.AllowEdit = false;
            this.COLAPUOMLength.Visible = true;
            this.COLAPUOMLength.VisibleIndex = 11;
            this.COLAPUOMLength.Width = 71;
            // 
            // COLAPWidth
            // 
            this.COLAPWidth.Caption = "Width";
            this.COLAPWidth.FieldName = "Width";
            this.COLAPWidth.Name = "COLAPWidth";
            this.COLAPWidth.OptionsColumn.AllowEdit = false;
            this.COLAPWidth.Visible = true;
            this.COLAPWidth.VisibleIndex = 12;
            this.COLAPWidth.Width = 50;
            // 
            // COLAPUOMWidth
            // 
            this.COLAPUOMWidth.Caption = "UOMWidth";
            this.COLAPUOMWidth.FieldName = "UOMWidth";
            this.COLAPUOMWidth.Name = "COLAPUOMWidth";
            this.COLAPUOMWidth.OptionsColumn.AllowEdit = false;
            this.COLAPUOMWidth.Visible = true;
            this.COLAPUOMWidth.VisibleIndex = 13;
            this.COLAPUOMWidth.Width = 82;
            // 
            // COLAPSize
            // 
            this.COLAPSize.Caption = "Size";
            this.COLAPSize.FieldName = "Size";
            this.COLAPSize.Name = "COLAPSize";
            this.COLAPSize.OptionsColumn.AllowEdit = false;
            this.COLAPSize.Visible = true;
            this.COLAPSize.VisibleIndex = 14;
            this.COLAPSize.Width = 37;
            // 
            // COLAPTickness
            // 
            this.COLAPTickness.Caption = "Tickness";
            this.COLAPTickness.FieldName = "Tickness";
            this.COLAPTickness.Name = "COLAPTickness";
            this.COLAPTickness.OptionsColumn.AllowEdit = false;
            this.COLAPTickness.Visible = true;
            this.COLAPTickness.VisibleIndex = 15;
            this.COLAPTickness.Width = 69;
            // 
            // colVolumeUOM
            // 
            this.colVolumeUOM.Caption = "VolumeUOM";
            this.colVolumeUOM.FieldName = "VolumeUOM";
            this.colVolumeUOM.Name = "colVolumeUOM";
            this.colVolumeUOM.OptionsColumn.AllowEdit = false;
            this.colVolumeUOM.Visible = true;
            this.colVolumeUOM.VisibleIndex = 16;
            // 
            // colVolume
            // 
            this.colVolume.Caption = "Volume";
            this.colVolume.FieldName = "Volume";
            this.colVolume.Name = "colVolume";
            this.colVolume.OptionsColumn.AllowEdit = false;
            this.colVolume.Visible = true;
            this.colVolume.VisibleIndex = 17;
            // 
            // colWeightUOM
            // 
            this.colWeightUOM.Caption = "WeightUOM";
            this.colWeightUOM.FieldName = "WeightUOM";
            this.colWeightUOM.Name = "colWeightUOM";
            this.colWeightUOM.OptionsColumn.AllowEdit = false;
            this.colWeightUOM.Visible = true;
            this.colWeightUOM.VisibleIndex = 18;
            // 
            // colWeight
            // 
            this.colWeight.Caption = "Weight";
            this.colWeight.FieldName = "Weight";
            this.colWeight.Name = "colWeight";
            this.colWeight.OptionsColumn.AllowEdit = false;
            this.colWeight.Visible = true;
            this.colWeight.VisibleIndex = 19;
            // 
            // colNumbering
            // 
            this.colNumbering.Caption = "Level";
            this.colNumbering.FieldName = "Numbering";
            this.colNumbering.Name = "colNumbering";
            this.colNumbering.Visible = true;
            this.colNumbering.VisibleIndex = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.btnCancelDetail);
            this.panelControl2.Controls.Add(this.sbtnSelectAllPI);
            this.panelControl2.Controls.Add(this.sbtnDownPI);
            this.panelControl2.Controls.Add(this.sbtnUpPI);
            this.panelControl2.Controls.Add(this.sbtnInsertAllPI);
            this.panelControl2.Controls.Add(this.sbtnUndoPIDtl);
            this.panelControl2.Controls.Add(this.sbtnAddPIDtl);
            this.panelControl2.Controls.Add(this.sbtnDeletePIDtl);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(978, 33);
            this.panelControl2.TabIndex = 5;
            // 
            // btnCancelDetail
            // 
            this.btnCancelDetail.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnCancelDetail.Appearance.Options.UseForeColor = true;
            this.btnCancelDetail.ImageIndex = 4;
            this.btnCancelDetail.ImageList = this.imageList1;
            this.btnCancelDetail.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCancelDetail.Location = new System.Drawing.Point(265, 4);
            this.btnCancelDetail.Name = "btnCancelDetail";
            this.btnCancelDetail.Size = new System.Drawing.Size(83, 23);
            this.btnCancelDetail.TabIndex = 20;
            this.btnCancelDetail.TabStop = false;
            this.btnCancelDetail.Text = "Cancel Detail";
            this.btnCancelDetail.Click += new System.EventHandler(this.btnCancelDetail_Click);
            // 
            // sbtnSelectAllPI
            // 
            this.sbtnSelectAllPI.Image = ((System.Drawing.Image)(resources.GetObject("sbtnSelectAllPI.Image")));
            this.sbtnSelectAllPI.ImageIndex = 4;
            this.sbtnSelectAllPI.ImageList = this.imageList1;
            this.sbtnSelectAllPI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnSelectAllPI.Location = new System.Drawing.Point(228, 4);
            this.sbtnSelectAllPI.Name = "sbtnSelectAllPI";
            this.sbtnSelectAllPI.Size = new System.Drawing.Size(31, 23);
            this.sbtnSelectAllPI.TabIndex = 16;
            this.sbtnSelectAllPI.TabStop = false;
            this.sbtnSelectAllPI.Click += new System.EventHandler(this.sbtnSelectAllPI_Click);
            // 
            // sbtnDownPI
            // 
            this.sbtnDownPI.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDownPI.Image")));
            this.sbtnDownPI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDownPI.Location = new System.Drawing.Point(190, 4);
            this.sbtnDownPI.Name = "sbtnDownPI";
            this.sbtnDownPI.Size = new System.Drawing.Size(32, 23);
            this.sbtnDownPI.TabIndex = 17;
            this.sbtnDownPI.TabStop = false;
            this.sbtnDownPI.Click += new System.EventHandler(this.sbtnDownPI_Click);
            // 
            // sbtnUpPI
            // 
            this.sbtnUpPI.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUpPI.Image")));
            this.sbtnUpPI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnUpPI.Location = new System.Drawing.Point(154, 4);
            this.sbtnUpPI.Name = "sbtnUpPI";
            this.sbtnUpPI.Size = new System.Drawing.Size(30, 23);
            this.sbtnUpPI.TabIndex = 18;
            this.sbtnUpPI.TabStop = false;
            this.sbtnUpPI.Text = "Up";
            this.sbtnUpPI.Click += new System.EventHandler(this.sbtnUpPI_Click);
            // 
            // sbtnInsertAllPI
            // 
            this.sbtnInsertAllPI.Image = ((System.Drawing.Image)(resources.GetObject("sbtnInsertAllPI.Image")));
            this.sbtnInsertAllPI.ImageIndex = 2;
            this.sbtnInsertAllPI.ImageList = this.imageList1;
            this.sbtnInsertAllPI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnInsertAllPI.Location = new System.Drawing.Point(52, 4);
            this.sbtnInsertAllPI.Name = "sbtnInsertAllPI";
            this.sbtnInsertAllPI.Size = new System.Drawing.Size(32, 23);
            this.sbtnInsertAllPI.TabIndex = 19;
            this.sbtnInsertAllPI.TabStop = false;
            this.sbtnInsertAllPI.Click += new System.EventHandler(this.sbtnInsertAllPI_Click);
            // 
            // sbtnUndoPIDtl
            // 
            this.sbtnUndoPIDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndoPIDtl.Image")));
            this.sbtnUndoPIDtl.ImageIndex = 3;
            this.sbtnUndoPIDtl.Location = new System.Drawing.Point(122, 4);
            this.sbtnUndoPIDtl.Name = "sbtnUndoPIDtl";
            this.sbtnUndoPIDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnUndoPIDtl.TabIndex = 15;
            this.sbtnUndoPIDtl.TabStop = false;
            this.sbtnUndoPIDtl.Text = "Undo";
            this.sbtnUndoPIDtl.Click += new System.EventHandler(this.sbtnUndoPIDtl_Click);
            // 
            // sbtnAddPIDtl
            // 
            this.sbtnAddPIDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddPIDtl.Image")));
            this.sbtnAddPIDtl.ImageIndex = 0;
            this.sbtnAddPIDtl.ImageList = this.imageList1;
            this.sbtnAddPIDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddPIDtl.Location = new System.Drawing.Point(20, 4);
            this.sbtnAddPIDtl.Name = "sbtnAddPIDtl";
            this.sbtnAddPIDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddPIDtl.TabIndex = 13;
            this.sbtnAddPIDtl.TabStop = false;
            this.sbtnAddPIDtl.Click += new System.EventHandler(this.sbtnAddPIDtl_Click);
            // 
            // sbtnDeletePIDtl
            // 
            this.sbtnDeletePIDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeletePIDtl.Image")));
            this.sbtnDeletePIDtl.ImageIndex = 1;
            this.sbtnDeletePIDtl.ImageList = this.imageList1;
            this.sbtnDeletePIDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeletePIDtl.Location = new System.Drawing.Point(90, 4);
            this.sbtnDeletePIDtl.Name = "sbtnDeletePIDtl";
            this.sbtnDeletePIDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnDeletePIDtl.TabIndex = 14;
            this.sbtnDeletePIDtl.TabStop = false;
            this.sbtnDeletePIDtl.Click += new System.EventHandler(this.sbtnDeletePIDtl_Click);
            // 
            // tabPageDetail
            // 
            this.tabPageDetail.Controls.Add(this.xtraTabControlRM);
            this.tabPageDetail.Name = "tabPageDetail";
            this.tabPageDetail.Size = new System.Drawing.Size(978, 408);
            this.tabPageDetail.Text = "Raw Materials";
            // 
            // tabPageBS
            // 
            this.tabPageBS.Controls.Add(this.gridCtlIBOMBS);
            this.tabPageBS.Controls.Add(this.panelControl4);
            this.tabPageBS.Name = "tabPageBS";
            this.tabPageBS.Size = new System.Drawing.Size(978, 408);
            this.tabPageBS.Text = "Item Waste/ Not Goods";
            // 
            // gridCtlIBOMBS
            // 
            this.gridCtlIBOMBS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlIBOMBS.Location = new System.Drawing.Point(0, 33);
            this.gridCtlIBOMBS.MainView = this.gvBOMBS;
            this.gridCtlIBOMBS.Name = "gridCtlIBOMBS";
            this.gridCtlIBOMBS.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repItemBSItemCode,
            this.repBOMItemLookUpEdit});
            this.gridCtlIBOMBS.Size = new System.Drawing.Size(978, 375);
            this.gridCtlIBOMBS.TabIndex = 2;
            this.gridCtlIBOMBS.UseEmbeddedNavigator = true;
            this.gridCtlIBOMBS.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBOMBS});
            // 
            // gvBOMBS
            // 
            this.gvBOMBS.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBSItemCode,
            this.colBOMDesc,
            this.colBSCostPercent,
            this.ColBSCostMax,
            this.colBSSeq,
            this.gridColumn5});
            this.gvBOMBS.GridControl = this.gridCtlIBOMBS;
            this.gvBOMBS.Name = "gvBOMBS";
            this.gvBOMBS.OptionsBehavior.Editable = false;
            this.gvBOMBS.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvBOMBS_FocusedRowChanged);
            // 
            // colBSItemCode
            // 
            this.colBSItemCode.Caption = "Item Code";
            this.colBSItemCode.ColumnEdit = this.repBOMItemLookUpEdit;
            this.colBSItemCode.FieldName = "ItemCode";
            this.colBSItemCode.Name = "colBSItemCode";
            this.colBSItemCode.OptionsColumn.AllowEdit = false;
            this.colBSItemCode.Visible = true;
            this.colBSItemCode.VisibleIndex = 0;
            this.colBSItemCode.Width = 169;
            // 
            // repBOMItemLookUpEdit
            // 
            this.repBOMItemLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repBOMItemLookUpEdit.Name = "repBOMItemLookUpEdit";
            // 
            // colBOMDesc
            // 
            this.colBOMDesc.Caption = "Description";
            this.colBOMDesc.FieldName = "Description";
            this.colBOMDesc.Name = "colBOMDesc";
            this.colBOMDesc.OptionsColumn.AllowEdit = false;
            this.colBOMDesc.Visible = true;
            this.colBOMDesc.VisibleIndex = 1;
            this.colBOMDesc.Width = 459;
            // 
            // colBSCostPercent
            // 
            this.colBSCostPercent.Caption = "Cost (%)";
            this.colBSCostPercent.FieldName = "CostPercent";
            this.colBSCostPercent.Name = "colBSCostPercent";
            this.colBSCostPercent.OptionsColumn.AllowEdit = false;
            this.colBSCostPercent.Visible = true;
            this.colBSCostPercent.VisibleIndex = 2;
            this.colBSCostPercent.Width = 135;
            // 
            // ColBSCostMax
            // 
            this.ColBSCostMax.Caption = "CostMax";
            this.ColBSCostMax.FieldName = "CostMax";
            this.ColBSCostMax.Name = "ColBSCostMax";
            this.ColBSCostMax.OptionsColumn.AllowEdit = false;
            this.ColBSCostMax.Visible = true;
            this.ColBSCostMax.VisibleIndex = 3;
            this.ColBSCostMax.Width = 176;
            // 
            // colBSSeq
            // 
            this.colBSSeq.Caption = "Seq";
            this.colBSSeq.FieldName = "Seq";
            this.colBSSeq.Name = "colBSSeq";
            this.colBSSeq.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "BOM Code";
            this.gridColumn5.FieldName = "FromBOMCode";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 94;
            // 
            // repItemBSItemCode
            // 
            this.repItemBSItemCode.AutoHeight = false;
            this.repItemBSItemCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemBSItemCode.Name = "repItemBSItemCode";
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.sbtnSelectAllBS);
            this.panelControl4.Controls.Add(this.sbtnDownBS);
            this.panelControl4.Controls.Add(this.sbtnUpBS);
            this.panelControl4.Controls.Add(this.sBtnInsertBS);
            this.panelControl4.Controls.Add(this.sbtnUndoBSDtl);
            this.panelControl4.Controls.Add(this.sbtnAddBSDTL);
            this.panelControl4.Controls.Add(this.sbtnDeleteBSDTL);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(978, 33);
            this.panelControl4.TabIndex = 3;
            this.panelControl4.Visible = false;
            // 
            // sbtnSelectAllBS
            // 
            this.sbtnSelectAllBS.Image = ((System.Drawing.Image)(resources.GetObject("sbtnSelectAllBS.Image")));
            this.sbtnSelectAllBS.ImageIndex = 4;
            this.sbtnSelectAllBS.ImageList = this.imageList1;
            this.sbtnSelectAllBS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnSelectAllBS.Location = new System.Drawing.Point(228, 4);
            this.sbtnSelectAllBS.Name = "sbtnSelectAllBS";
            this.sbtnSelectAllBS.Size = new System.Drawing.Size(31, 23);
            this.sbtnSelectAllBS.TabIndex = 13;
            this.sbtnSelectAllBS.TabStop = false;
            this.sbtnSelectAllBS.Click += new System.EventHandler(this.sbtnSelectAllBS_Click);
            // 
            // sbtnDownBS
            // 
            this.sbtnDownBS.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDownBS.Image")));
            this.sbtnDownBS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDownBS.Location = new System.Drawing.Point(190, 4);
            this.sbtnDownBS.Name = "sbtnDownBS";
            this.sbtnDownBS.Size = new System.Drawing.Size(32, 23);
            this.sbtnDownBS.TabIndex = 14;
            this.sbtnDownBS.TabStop = false;
            this.sbtnDownBS.Click += new System.EventHandler(this.sbtnDownBS_Click);
            // 
            // sbtnUpBS
            // 
            this.sbtnUpBS.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUpBS.Image")));
            this.sbtnUpBS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnUpBS.Location = new System.Drawing.Point(154, 4);
            this.sbtnUpBS.Name = "sbtnUpBS";
            this.sbtnUpBS.Size = new System.Drawing.Size(30, 23);
            this.sbtnUpBS.TabIndex = 15;
            this.sbtnUpBS.TabStop = false;
            this.sbtnUpBS.Text = "Up";
            this.sbtnUpBS.Click += new System.EventHandler(this.sbtnUpBS_Click);
            // 
            // sBtnInsertBS
            // 
            this.sBtnInsertBS.Image = ((System.Drawing.Image)(resources.GetObject("sBtnInsertBS.Image")));
            this.sBtnInsertBS.ImageIndex = 2;
            this.sBtnInsertBS.ImageList = this.imageList1;
            this.sBtnInsertBS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBtnInsertBS.Location = new System.Drawing.Point(52, 4);
            this.sBtnInsertBS.Name = "sBtnInsertBS";
            this.sBtnInsertBS.Size = new System.Drawing.Size(32, 23);
            this.sBtnInsertBS.TabIndex = 16;
            this.sBtnInsertBS.TabStop = false;
            this.sBtnInsertBS.Click += new System.EventHandler(this.sBtnInsertBS_Click);
            // 
            // sbtnUndoBSDtl
            // 
            this.sbtnUndoBSDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndoBSDtl.Image")));
            this.sbtnUndoBSDtl.ImageIndex = 3;
            this.sbtnUndoBSDtl.Location = new System.Drawing.Point(122, 4);
            this.sbtnUndoBSDtl.Name = "sbtnUndoBSDtl";
            this.sbtnUndoBSDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnUndoBSDtl.TabIndex = 12;
            this.sbtnUndoBSDtl.TabStop = false;
            this.sbtnUndoBSDtl.Text = "Undo";
            this.sbtnUndoBSDtl.Click += new System.EventHandler(this.sbtnUndoBSDtl_Click);
            // 
            // sbtnAddBSDTL
            // 
            this.sbtnAddBSDTL.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddBSDTL.Image")));
            this.sbtnAddBSDTL.ImageIndex = 0;
            this.sbtnAddBSDTL.ImageList = this.imageList1;
            this.sbtnAddBSDTL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddBSDTL.Location = new System.Drawing.Point(20, 4);
            this.sbtnAddBSDTL.Name = "sbtnAddBSDTL";
            this.sbtnAddBSDTL.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddBSDTL.TabIndex = 10;
            this.sbtnAddBSDTL.TabStop = false;
            this.sbtnAddBSDTL.Click += new System.EventHandler(this.sbtnAddBSDTL_Click);
            // 
            // sbtnDeleteBSDTL
            // 
            this.sbtnDeleteBSDTL.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeleteBSDTL.Image")));
            this.sbtnDeleteBSDTL.ImageIndex = 1;
            this.sbtnDeleteBSDTL.ImageList = this.imageList1;
            this.sbtnDeleteBSDTL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeleteBSDTL.Location = new System.Drawing.Point(90, 4);
            this.sbtnDeleteBSDTL.Name = "sbtnDeleteBSDTL";
            this.sbtnDeleteBSDTL.Size = new System.Drawing.Size(26, 23);
            this.sbtnDeleteBSDTL.TabIndex = 11;
            this.sbtnDeleteBSDTL.TabStop = false;
            this.sbtnDeleteBSDTL.Click += new System.EventHandler(this.sbtnDeleteBSDTL_Click);
            // 
            // tabPageAP
            // 
            this.tabPageAP.Controls.Add(this.gridCtlIBOMAP);
            this.tabPageAP.Controls.Add(this.panelControl6);
            this.tabPageAP.Name = "tabPageAP";
            this.tabPageAP.Size = new System.Drawing.Size(978, 408);
            this.tabPageAP.Text = "Alternatif Product";
            // 
            // gridCtlIBOMAP
            // 
            this.gridCtlIBOMAP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlIBOMAP.Location = new System.Drawing.Point(0, 33);
            this.gridCtlIBOMAP.MainView = this.gvBOMAP;
            this.gridCtlIBOMAP.Name = "gridCtlIBOMAP";
            this.gridCtlIBOMAP.Size = new System.Drawing.Size(978, 375);
            this.gridCtlIBOMAP.TabIndex = 7;
            this.gridCtlIBOMAP.UseEmbeddedNavigator = true;
            this.gridCtlIBOMAP.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBOMAP});
            // 
            // gvBOMAP
            // 
            this.gvBOMAP.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.colFromDocDtlKey,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn10});
            this.gvBOMAP.GridControl = this.gridCtlIBOMAP;
            this.gvBOMAP.Name = "gvBOMAP";
            this.gvBOMAP.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvBOMAP_FocusedRowChanged);
            this.gvBOMAP.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gvBOMAP_FocusedColumnChanged);
            this.gvBOMAP.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gvBOMAP_CustomUnboundColumnData);
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "ItemCode";
            this.gridColumn6.ColumnEdit = this.repositoryItemLookUpEdit3;
            this.gridColumn6.FieldName = "ItemCode";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 213;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Description";
            this.gridColumn7.FieldName = "Description";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            this.gridColumn7.Width = 616;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Cost (%)";
            this.gridColumn8.FieldName = "CostPercent";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Seq";
            this.gridColumn9.FieldName = "Seq";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colFromDocDtlKey
            // 
            this.colFromDocDtlKey.Caption = "FromDocDtlKey";
            this.colFromDocDtlKey.FieldName = "FromDocDtlKey";
            this.colFromDocDtlKey.Name = "colFromDocDtlKey";
            this.colFromDocDtlKey.OptionsColumn.AllowEdit = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "BOM Code";
            this.gridColumn11.FieldName = "FromBOMCode";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 2;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "ParentBOMCode";
            this.gridColumn12.FieldName = "ParentBOMCode";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Product Code";
            this.gridColumn10.FieldName = "ProductCode";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.sbtnSelectAllAP);
            this.panelControl6.Controls.Add(this.sbtnDownAP);
            this.panelControl6.Controls.Add(this.sbtnUPAP);
            this.panelControl6.Controls.Add(this.sbtnInsertAllAP);
            this.panelControl6.Controls.Add(this.sbtnUndoAPDtl);
            this.panelControl6.Controls.Add(this.sbtnAddAPDtl);
            this.panelControl6.Controls.Add(this.sbtnDeleteAPDtl);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(0, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(978, 33);
            this.panelControl6.TabIndex = 6;
            this.panelControl6.Visible = false;
            // 
            // sbtnSelectAllAP
            // 
            this.sbtnSelectAllAP.Image = ((System.Drawing.Image)(resources.GetObject("sbtnSelectAllAP.Image")));
            this.sbtnSelectAllAP.ImageIndex = 4;
            this.sbtnSelectAllAP.ImageList = this.imageList1;
            this.sbtnSelectAllAP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnSelectAllAP.Location = new System.Drawing.Point(228, 4);
            this.sbtnSelectAllAP.Name = "sbtnSelectAllAP";
            this.sbtnSelectAllAP.Size = new System.Drawing.Size(31, 23);
            this.sbtnSelectAllAP.TabIndex = 16;
            this.sbtnSelectAllAP.TabStop = false;
            this.sbtnSelectAllAP.Click += new System.EventHandler(this.sbtnSelectAllAP_Click_1);
            // 
            // sbtnDownAP
            // 
            this.sbtnDownAP.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDownAP.Image")));
            this.sbtnDownAP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDownAP.Location = new System.Drawing.Point(190, 4);
            this.sbtnDownAP.Name = "sbtnDownAP";
            this.sbtnDownAP.Size = new System.Drawing.Size(32, 23);
            this.sbtnDownAP.TabIndex = 17;
            this.sbtnDownAP.TabStop = false;
            this.sbtnDownAP.Click += new System.EventHandler(this.sbtnDownAP_Click_1);
            // 
            // sbtnUPAP
            // 
            this.sbtnUPAP.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUPAP.Image")));
            this.sbtnUPAP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnUPAP.Location = new System.Drawing.Point(154, 4);
            this.sbtnUPAP.Name = "sbtnUPAP";
            this.sbtnUPAP.Size = new System.Drawing.Size(30, 23);
            this.sbtnUPAP.TabIndex = 18;
            this.sbtnUPAP.TabStop = false;
            this.sbtnUPAP.Text = "Up";
            this.sbtnUPAP.Click += new System.EventHandler(this.sbtnUPAP_Click_1);
            // 
            // sbtnInsertAllAP
            // 
            this.sbtnInsertAllAP.Image = ((System.Drawing.Image)(resources.GetObject("sbtnInsertAllAP.Image")));
            this.sbtnInsertAllAP.ImageIndex = 2;
            this.sbtnInsertAllAP.ImageList = this.imageList1;
            this.sbtnInsertAllAP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnInsertAllAP.Location = new System.Drawing.Point(52, 4);
            this.sbtnInsertAllAP.Name = "sbtnInsertAllAP";
            this.sbtnInsertAllAP.Size = new System.Drawing.Size(32, 23);
            this.sbtnInsertAllAP.TabIndex = 19;
            this.sbtnInsertAllAP.TabStop = false;
            this.sbtnInsertAllAP.Click += new System.EventHandler(this.sbtnInsertAllAP_Click);
            // 
            // sbtnUndoAPDtl
            // 
            this.sbtnUndoAPDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndoAPDtl.Image")));
            this.sbtnUndoAPDtl.ImageIndex = 3;
            this.sbtnUndoAPDtl.Location = new System.Drawing.Point(122, 4);
            this.sbtnUndoAPDtl.Name = "sbtnUndoAPDtl";
            this.sbtnUndoAPDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnUndoAPDtl.TabIndex = 15;
            this.sbtnUndoAPDtl.TabStop = false;
            this.sbtnUndoAPDtl.Text = "Undo";
            this.sbtnUndoAPDtl.Click += new System.EventHandler(this.sbtnUndoAPDtl_Click);
            // 
            // sbtnAddAPDtl
            // 
            this.sbtnAddAPDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddAPDtl.Image")));
            this.sbtnAddAPDtl.ImageIndex = 0;
            this.sbtnAddAPDtl.ImageList = this.imageList1;
            this.sbtnAddAPDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddAPDtl.Location = new System.Drawing.Point(20, 4);
            this.sbtnAddAPDtl.Name = "sbtnAddAPDtl";
            this.sbtnAddAPDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddAPDtl.TabIndex = 13;
            this.sbtnAddAPDtl.TabStop = false;
            this.sbtnAddAPDtl.Click += new System.EventHandler(this.sbtnAddAPDtl_Click);
            // 
            // sbtnDeleteAPDtl
            // 
            this.sbtnDeleteAPDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeleteAPDtl.Image")));
            this.sbtnDeleteAPDtl.ImageIndex = 1;
            this.sbtnDeleteAPDtl.ImageList = this.imageList1;
            this.sbtnDeleteAPDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeleteAPDtl.Location = new System.Drawing.Point(90, 4);
            this.sbtnDeleteAPDtl.Name = "sbtnDeleteAPDtl";
            this.sbtnDeleteAPDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnDeleteAPDtl.TabIndex = 14;
            this.sbtnDeleteAPDtl.TabStop = false;
            this.sbtnDeleteAPDtl.Click += new System.EventHandler(this.sbtnDeleteAPDtl_Click);
            // 
            // tabPageOvd
            // 
            this.tabPageOvd.Controls.Add(this.gridCtlIBOMOvd);
            this.tabPageOvd.Controls.Add(this.panelControl3);
            this.tabPageOvd.Name = "tabPageOvd";
            this.tabPageOvd.Size = new System.Drawing.Size(978, 408);
            this.tabPageOvd.Text = "Factory Overhead Cost";
            // 
            // gridCtlIBOMOvd
            // 
            this.gridCtlIBOMOvd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlIBOMOvd.Location = new System.Drawing.Point(0, 33);
            this.gridCtlIBOMOvd.MainView = this.gvBOMOvd;
            this.gridCtlIBOMOvd.Name = "gridCtlIBOMOvd";
            this.gridCtlIBOMOvd.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repleOvdOverheadCode});
            this.gridCtlIBOMOvd.Size = new System.Drawing.Size(978, 375);
            this.gridCtlIBOMOvd.TabIndex = 4;
            this.gridCtlIBOMOvd.UseEmbeddedNavigator = true;
            this.gridCtlIBOMOvd.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBOMOvd});
            // 
            // gvBOMOvd
            // 
            this.gvBOMOvd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOvdOverheadCode,
            this.colOvdDescription,
            this.colOvdAmount,
            this.colOvdSeq,
            this.gridColumn4});
            this.gvBOMOvd.GridControl = this.gridCtlIBOMOvd;
            this.gvBOMOvd.Name = "gvBOMOvd";
            this.gvBOMOvd.OptionsBehavior.Editable = false;
            this.gvBOMOvd.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvBOMOvd_FocusedRowChanged);
            // 
            // colOvdOverheadCode
            // 
            this.colOvdOverheadCode.Caption = "OverheadCode";
            this.colOvdOverheadCode.ColumnEdit = this.repleOvdOverheadCode;
            this.colOvdOverheadCode.FieldName = "OverheadCode";
            this.colOvdOverheadCode.Name = "colOvdOverheadCode";
            this.colOvdOverheadCode.Visible = true;
            this.colOvdOverheadCode.VisibleIndex = 0;
            this.colOvdOverheadCode.Width = 235;
            // 
            // repleOvdOverheadCode
            // 
            this.repleOvdOverheadCode.AutoHeight = false;
            this.repleOvdOverheadCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleOvdOverheadCode.Name = "repleOvdOverheadCode";
            // 
            // colOvdDescription
            // 
            this.colOvdDescription.Caption = "Description";
            this.colOvdDescription.FieldName = "Description";
            this.colOvdDescription.Name = "colOvdDescription";
            this.colOvdDescription.OptionsColumn.AllowEdit = false;
            this.colOvdDescription.Visible = true;
            this.colOvdDescription.VisibleIndex = 1;
            this.colOvdDescription.Width = 537;
            // 
            // colOvdAmount
            // 
            this.colOvdAmount.Caption = "Jumlah (Rp)";
            this.colOvdAmount.FieldName = "Amount";
            this.colOvdAmount.Name = "colOvdAmount";
            this.colOvdAmount.Visible = true;
            this.colOvdAmount.VisibleIndex = 2;
            this.colOvdAmount.Width = 306;
            // 
            // colOvdSeq
            // 
            this.colOvdSeq.Caption = "Seq";
            this.colOvdSeq.FieldName = "Seq";
            this.colOvdSeq.Name = "colOvdSeq";
            this.colOvdSeq.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "BOMCode";
            this.gridColumn4.FieldName = "FromBOMCode";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.sbtnSelectAllOvd);
            this.panelControl3.Controls.Add(this.sbtnDownOvd);
            this.panelControl3.Controls.Add(this.sbtnUpOvd);
            this.panelControl3.Controls.Add(this.sbtnInsertOvd);
            this.panelControl3.Controls.Add(this.sbtnUndoOvdDtl);
            this.panelControl3.Controls.Add(this.sbtnAddOvdDtl);
            this.panelControl3.Controls.Add(this.sbtnDeleteOvdDtl);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(978, 33);
            this.panelControl3.TabIndex = 5;
            this.panelControl3.Visible = false;
            // 
            // sbtnSelectAllOvd
            // 
            this.sbtnSelectAllOvd.Image = ((System.Drawing.Image)(resources.GetObject("sbtnSelectAllOvd.Image")));
            this.sbtnSelectAllOvd.ImageIndex = 4;
            this.sbtnSelectAllOvd.ImageList = this.imageList1;
            this.sbtnSelectAllOvd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnSelectAllOvd.Location = new System.Drawing.Point(222, 4);
            this.sbtnSelectAllOvd.Name = "sbtnSelectAllOvd";
            this.sbtnSelectAllOvd.Size = new System.Drawing.Size(31, 23);
            this.sbtnSelectAllOvd.TabIndex = 19;
            this.sbtnSelectAllOvd.TabStop = false;
            this.sbtnSelectAllOvd.Click += new System.EventHandler(this.sbtnSelectAllOvd_Click);
            // 
            // sbtnDownOvd
            // 
            this.sbtnDownOvd.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDownOvd.Image")));
            this.sbtnDownOvd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDownOvd.Location = new System.Drawing.Point(185, 4);
            this.sbtnDownOvd.Name = "sbtnDownOvd";
            this.sbtnDownOvd.Size = new System.Drawing.Size(32, 23);
            this.sbtnDownOvd.TabIndex = 20;
            this.sbtnDownOvd.TabStop = false;
            this.sbtnDownOvd.Click += new System.EventHandler(this.sbtnDownOvd_Click);
            // 
            // sbtnUpOvd
            // 
            this.sbtnUpOvd.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUpOvd.Image")));
            this.sbtnUpOvd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnUpOvd.Location = new System.Drawing.Point(150, 4);
            this.sbtnUpOvd.Name = "sbtnUpOvd";
            this.sbtnUpOvd.Size = new System.Drawing.Size(30, 23);
            this.sbtnUpOvd.TabIndex = 21;
            this.sbtnUpOvd.TabStop = false;
            this.sbtnUpOvd.Text = "Up";
            this.sbtnUpOvd.Click += new System.EventHandler(this.sbtnUpOvd_Click);
            // 
            // sbtnInsertOvd
            // 
            this.sbtnInsertOvd.Image = ((System.Drawing.Image)(resources.GetObject("sbtnInsertOvd.Image")));
            this.sbtnInsertOvd.ImageIndex = 2;
            this.sbtnInsertOvd.ImageList = this.imageList1;
            this.sbtnInsertOvd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnInsertOvd.Location = new System.Drawing.Point(51, 4);
            this.sbtnInsertOvd.Name = "sbtnInsertOvd";
            this.sbtnInsertOvd.Size = new System.Drawing.Size(32, 23);
            this.sbtnInsertOvd.TabIndex = 22;
            this.sbtnInsertOvd.TabStop = false;
            this.sbtnInsertOvd.Click += new System.EventHandler(this.sbtnInsertOvd_Click);
            // 
            // sbtnUndoOvdDtl
            // 
            this.sbtnUndoOvdDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndoOvdDtl.Image")));
            this.sbtnUndoOvdDtl.ImageIndex = 3;
            this.sbtnUndoOvdDtl.Location = new System.Drawing.Point(119, 4);
            this.sbtnUndoOvdDtl.Name = "sbtnUndoOvdDtl";
            this.sbtnUndoOvdDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnUndoOvdDtl.TabIndex = 18;
            this.sbtnUndoOvdDtl.TabStop = false;
            this.sbtnUndoOvdDtl.Text = "Undo";
            this.sbtnUndoOvdDtl.Click += new System.EventHandler(this.sbtnUndoOvdDtl_Click);
            // 
            // sbtnAddOvdDtl
            // 
            this.sbtnAddOvdDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddOvdDtl.Image")));
            this.sbtnAddOvdDtl.ImageIndex = 0;
            this.sbtnAddOvdDtl.ImageList = this.imageList1;
            this.sbtnAddOvdDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddOvdDtl.Location = new System.Drawing.Point(20, 4);
            this.sbtnAddOvdDtl.Name = "sbtnAddOvdDtl";
            this.sbtnAddOvdDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddOvdDtl.TabIndex = 16;
            this.sbtnAddOvdDtl.TabStop = false;
            this.sbtnAddOvdDtl.Click += new System.EventHandler(this.sbtnAddOvdDtl_Click);
            // 
            // sbtnDeleteOvdDtl
            // 
            this.sbtnDeleteOvdDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeleteOvdDtl.Image")));
            this.sbtnDeleteOvdDtl.ImageIndex = 1;
            this.sbtnDeleteOvdDtl.ImageList = this.imageList1;
            this.sbtnDeleteOvdDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeleteOvdDtl.Location = new System.Drawing.Point(88, 4);
            this.sbtnDeleteOvdDtl.Name = "sbtnDeleteOvdDtl";
            this.sbtnDeleteOvdDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnDeleteOvdDtl.TabIndex = 17;
            this.sbtnDeleteOvdDtl.TabStop = false;
            this.sbtnDeleteOvdDtl.Click += new System.EventHandler(this.sbtnDeleteOvdDtl_Click);
            // 
            // tabPageMoreHeader
            // 
            this.tabPageMoreHeader.Controls.Add(this.labelRemark4);
            this.tabPageMoreHeader.Controls.Add(this.labelRemark3);
            this.tabPageMoreHeader.Controls.Add(this.labelRemark2);
            this.tabPageMoreHeader.Controls.Add(this.labelRemark1);
            this.tabPageMoreHeader.Controls.Add(this.mruEdtRemark1);
            this.tabPageMoreHeader.Controls.Add(this.mruEdtRemark2);
            this.tabPageMoreHeader.Controls.Add(this.mruEdtRemark3);
            this.tabPageMoreHeader.Controls.Add(this.mruEdtRemark4);
            this.tabPageMoreHeader.Name = "tabPageMoreHeader";
            this.tabPageMoreHeader.Size = new System.Drawing.Size(978, 408);
            this.tabPageMoreHeader.Text = "More Header";
            // 
            // labelRemark4
            // 
            this.labelRemark4.BackColor = System.Drawing.Color.Transparent;
            this.labelRemark4.Location = new System.Drawing.Point(17, 85);
            this.labelRemark4.Name = "labelRemark4";
            this.labelRemark4.Size = new System.Drawing.Size(58, 15);
            this.labelRemark4.TabIndex = 0;
            this.labelRemark4.Text = "Remark 4";
            // 
            // labelRemark3
            // 
            this.labelRemark3.BackColor = System.Drawing.Color.Transparent;
            this.labelRemark3.Location = new System.Drawing.Point(17, 59);
            this.labelRemark3.Name = "labelRemark3";
            this.labelRemark3.Size = new System.Drawing.Size(58, 15);
            this.labelRemark3.TabIndex = 1;
            this.labelRemark3.Text = "Remark 3";
            // 
            // labelRemark2
            // 
            this.labelRemark2.BackColor = System.Drawing.Color.Transparent;
            this.labelRemark2.Location = new System.Drawing.Point(17, 35);
            this.labelRemark2.Name = "labelRemark2";
            this.labelRemark2.Size = new System.Drawing.Size(58, 15);
            this.labelRemark2.TabIndex = 2;
            this.labelRemark2.Text = "Remark 2";
            // 
            // labelRemark1
            // 
            this.labelRemark1.BackColor = System.Drawing.Color.Transparent;
            this.labelRemark1.Location = new System.Drawing.Point(17, 11);
            this.labelRemark1.Name = "labelRemark1";
            this.labelRemark1.Size = new System.Drawing.Size(58, 15);
            this.labelRemark1.TabIndex = 3;
            this.labelRemark1.Text = "Remark 1";
            // 
            // mruEdtRemark1
            // 
            this.mruEdtRemark1.Location = new System.Drawing.Point(104, 8);
            this.mruEdtRemark1.Name = "mruEdtRemark1";
            this.mruEdtRemark1.Size = new System.Drawing.Size(416, 20);
            this.mruEdtRemark1.TabIndex = 4;
            // 
            // mruEdtRemark2
            // 
            this.mruEdtRemark2.Location = new System.Drawing.Point(104, 32);
            this.mruEdtRemark2.Name = "mruEdtRemark2";
            this.mruEdtRemark2.Size = new System.Drawing.Size(416, 20);
            this.mruEdtRemark2.TabIndex = 5;
            // 
            // mruEdtRemark3
            // 
            this.mruEdtRemark3.Location = new System.Drawing.Point(104, 56);
            this.mruEdtRemark3.Name = "mruEdtRemark3";
            this.mruEdtRemark3.Size = new System.Drawing.Size(416, 20);
            this.mruEdtRemark3.TabIndex = 6;
            // 
            // mruEdtRemark4
            // 
            this.mruEdtRemark4.Location = new System.Drawing.Point(104, 82);
            this.mruEdtRemark4.Name = "mruEdtRemark4";
            this.mruEdtRemark4.Size = new System.Drawing.Size(416, 20);
            this.mruEdtRemark4.TabIndex = 7;
            // 
            // tabPageExternalLink
            // 
            this.tabPageExternalLink.Controls.Add(this.externalLinkBox1);
            this.tabPageExternalLink.Name = "tabPageExternalLink";
            this.tabPageExternalLink.Size = new System.Drawing.Size(978, 408);
            this.tabPageExternalLink.Text = "External Link";
            // 
            // externalLinkBox1
            // 
            this.externalLinkBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.externalLinkBox1.Location = new System.Drawing.Point(0, 0);
            this.externalLinkBox1.Name = "externalLinkBox1";
            this.externalLinkBox1.Size = new System.Drawing.Size(978, 408);
            this.externalLinkBox1.TabIndex = 0;
            // 
            // tabPageNote
            // 
            this.tabPageNote.Controls.Add(this.memoEdtNote);
            this.tabPageNote.Name = "tabPageNote";
            this.tabPageNote.Size = new System.Drawing.Size(978, 408);
            this.tabPageNote.Text = "Note";
            // 
            // memoEdtNote
            // 
            this.memoEdtNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdtNote.Location = new System.Drawing.Point(0, 0);
            this.memoEdtNote.Name = "memoEdtNote";
            this.memoEdtNote.Size = new System.Drawing.Size(978, 408);
            this.memoEdtNote.TabIndex = 0;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(294, 273);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // tlItemCode
            // 
            this.tlItemCode.ColumnEdit = this.repLuedtItem;
            this.tlItemCode.FieldName = "ItemCode";
            this.tlItemCode.Name = "tlItemCode";
            // 
            // repLuedtItem
            // 
            this.repLuedtItem.Name = "repLuedtItem";
            this.repLuedtItem.ValidateOnEnterKey = true;
            this.repLuedtItem.CloseUp += new DevExpress.XtraEditors.Controls.CloseUpEventHandler(this.repLuedtItem_CloseUp);
            this.repLuedtItem.EditValueChanged += new System.EventHandler(this.repLuedtItem_EditValueChanged);
            // 
            // tlDesc
            // 
            this.tlDesc.FieldName = "Description";
            this.tlDesc.Name = "tlDesc";
            // 
            // tlFurtherDescription
            // 
            this.tlFurtherDescription.ColumnEdit = this.repBtnedtFurtherDesc;
            this.tlFurtherDescription.FieldName = "FurtherDescription";
            this.tlFurtherDescription.Name = "tlFurtherDescription";
            // 
            // repBtnedtFurtherDesc
            // 
            this.repBtnedtFurtherDesc.Name = "repBtnedtFurtherDesc";
            this.repBtnedtFurtherDesc.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // tlLocation
            // 
            this.tlLocation.ColumnEdit = this.repLuedtLocation;
            this.tlLocation.FieldName = "Location";
            this.tlLocation.Name = "tlLocation";
            // 
            // repLuedtLocation
            // 
            this.repLuedtLocation.Name = "repLuedtLocation";
            // 
            // tlBatchNo
            // 
            this.tlBatchNo.ColumnEdit = this.repLuedtBatchNo;
            this.tlBatchNo.FieldName = "BatchNo";
            this.tlBatchNo.Name = "tlBatchNo";
            // 
            // repLuedtBatchNo
            // 
            this.repLuedtBatchNo.Name = "repLuedtBatchNo";
            // 
            // tlProjNo
            // 
            this.tlProjNo.ColumnEdit = this.repLuedtProjNo;
            this.tlProjNo.FieldName = "ProjNo";
            this.tlProjNo.Name = "tlProjNo";
            // 
            // repLuedtProjNo
            // 
            this.repLuedtProjNo.Name = "repLuedtProjNo";
            // 
            // tlDeptNo
            // 
            this.tlDeptNo.ColumnEdit = this.repLuedtDeptNo;
            this.tlDeptNo.FieldName = "DeptNo";
            this.tlDeptNo.Name = "tlDeptNo";
            // 
            // repLuedtDeptNo
            // 
            this.repLuedtDeptNo.Name = "repLuedtDeptNo";
            // 
            // tlRate
            // 
            this.tlRate.FieldName = "Rate";
            this.tlRate.Name = "tlRate";
            // 
            // tlQty
            // 
            this.tlQty.FieldName = "Qty";
            this.tlQty.Name = "tlQty";
            // 
            // tlItemCost
            // 
            this.tlItemCost.FieldName = "ItemCost";
            this.tlItemCost.Name = "tlItemCost";
            // 
            // tlOverheadCost
            // 
            this.tlOverheadCost.FieldName = "OverHeadCost";
            this.tlOverheadCost.Name = "tlOverheadCost";
            // 
            // tlSubTotal
            // 
            this.tlSubTotal.FieldName = "SubTotalCost";
            this.tlSubTotal.Name = "tlSubTotal";
            // 
            // tlRemark
            // 
            this.tlRemark.FieldName = "Remark";
            this.tlRemark.Name = "tlRemark";
            // 
            // tlPrintOut
            // 
            this.tlPrintOut.ColumnEdit = this.repositoryItemCheckEdit1;
            this.tlPrintOut.FieldName = "PrintOut";
            this.tlPrintOut.Name = "tlPrintOut";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = "T";
            this.repositoryItemCheckEdit1.ValueUnchecked = "F";
            // 
            // tlSeq
            // 
            this.tlSeq.FieldName = "Seq";
            this.tlSeq.Name = "tlSeq";
            // 
            // tlNumbering
            // 
            this.tlNumbering.FieldName = "Numbering";
            this.tlNumbering.Name = "tlNumbering";
            // 
            // tlIsBOMItem
            // 
            this.tlIsBOMItem.ColumnEdit = this.repositoryItemCheckEdit1;
            this.tlIsBOMItem.FieldName = "IsBOMItem";
            this.tlIsBOMItem.Name = "tlIsBOMItem";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            // 
            // colItemCode
            // 
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            // 
            // colLocation
            // 
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            // 
            // colUOM
            // 
            this.colUOM.FieldName = "UOM";
            this.colUOM.Name = "colUOM";
            // 
            // colBatchNo
            // 
            this.colBatchNo.FieldName = "BatchNo";
            this.colBatchNo.Name = "colBatchNo";
            // 
            // colReqQty
            // 
            this.colReqQty.FieldName = "ReqQty";
            this.colReqQty.Name = "colReqQty";
            // 
            // colOnHandQty
            // 
            this.colOnHandQty.FieldName = "OnHandQty";
            this.colOnHandQty.Name = "colOnHandQty";
            // 
            // colOnHandBal
            // 
            this.colOnHandBal.FieldName = "OnHandBal";
            this.colOnHandBal.Name = "colOnHandBal";
            // 
            // colAvailableQty
            // 
            this.colAvailableQty.FieldName = "AvailableQty";
            this.colAvailableQty.Name = "colAvailableQty";
            // 
            // colAvailableBal
            // 
            this.colAvailableBal.FieldName = "AvailableBal";
            this.colAvailableBal.Name = "colAvailableBal";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.tabControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 178);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(984, 436);
            this.panelControl1.TabIndex = 0;
            // 
            // barItemCopyAsTabDelimitedText
            // 
            this.barItemCopyAsTabDelimitedText.Id = 16;
            this.barItemCopyAsTabDelimitedText.Name = "barItemCopyAsTabDelimitedText";
            this.barItemCopyAsTabDelimitedText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopyAsTabDelimitedText_ItemClick);
            // 
            // barBtnSaveInKIVFolder
            // 
            this.barBtnSaveInKIVFolder.Id = 11;
            this.barBtnSaveInKIVFolder.Name = "barBtnSaveInKIVFolder";
            this.barBtnSaveInKIVFolder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnSaveInKIVFolder_ItemClick);
            // 
            // barSubItem4
            // 
            this.barSubItem4.Id = 17;
            this.barSubItem4.Name = "barSubItem4";
            // 
            // iEditDescriptionMRU
            // 
            this.iEditDescriptionMRU.Id = 10;
            this.iEditDescriptionMRU.Name = "iEditDescriptionMRU";
            this.iEditDescriptionMRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditMRUItem_ItemClick);
            // 
            // barBtnConvertLevel
            // 
            this.barBtnConvertLevel.Id = 22;
            this.barBtnConvertLevel.Name = "barBtnConvertLevel";
            this.barBtnConvertLevel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnConvertLevel_ItemClick);
            // 
            // barbtnTransferFromSO
            // 
            this.barbtnTransferFromSO.Id = 14;
            this.barbtnTransferFromSO.Name = "barbtnTransferFromSO";
            this.barbtnTransferFromSO.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnTransferFromSO_ItemClick);
            // 
            // barbtnCheckTransferredToStatus
            // 
            this.barbtnCheckTransferredToStatus.Id = 15;
            this.barbtnCheckTransferredToStatus.Name = "barbtnCheckTransferredToStatus";
            this.barbtnCheckTransferredToStatus.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnCheckTransferredToStatus_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 12;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 300000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // FormStockWorkOrderEntry
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnCancel;
            this.ClientSize = new System.Drawing.Size(984, 661);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panelView);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormStockWorkOrderEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Work Order Entry";
            this.Activated += new System.EventHandler(this.FormStockAssemblyEntry_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormStockAssemblyEntry_Closing);
            this.Deactivate += new System.EventHandler(this.FormStockAssemblyEntry_Deactivate);
            this.Load += new System.EventHandler(this.FormStockAssemblyEntry_Load);
            this.VisibleChanged += new System.EventHandler(this.FormStockWorkOrderEntry_VisibleChanged);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FormStockAssemblyEntry_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.FormStockAssemblyEntry_DragOver);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormStockAssemblyEntry_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelHeader)).EndInit();
            this.panelHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtProjNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luProjNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebtorName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luDebtorCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMachineName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luMachineCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTarget.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTarget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deProduction.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deProduction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdtRefDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdtDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdtDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEdtStockAssemblyNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luEdtDocNoFormat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlRM)).EndInit();
            this.xtraTabControlRM.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLuedtItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemluUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repitemAPDebtorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panBottom)).EndInit();
            this.panBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtedtEstHPP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panTop)).EndInit();
            this.panTop.ResumeLayout(false);
            this.xtraTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlItemSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelView)).EndInit();
            this.panelView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkedtNextRecord.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPagePI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleAPItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleAPUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleItemBOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemProductDebtorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.tabPageDetail.ResumeLayout(false);
            this.tabPageBS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBOMItemLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemBSItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.tabPageAP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.tabPageOvd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMOvd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMOvd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleOvdOverheadCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.tabPageMoreHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark4.Properties)).EndInit();
            this.tabPageExternalLink.ResumeLayout(false);
            this.tabPageNote.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBtnedtFurtherDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtBatchNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtProjNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLuedtDeptNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void Navigator_ButtonClick(object sender, BCE.Controls.NavigatorButtonClickEventArgs e)
        {
            throw new NotImplementedException();
        }

        private class MyFilterOption : EnterKeyMessageFilter.FilterOption
        {
            private StockWorkOrder myDocument;

            public StockWorkOrder Document
            {
                get
                {
                    return this.myDocument;
                }
                set
                {
                    this.myDocument = value;
                }
            }

            public MyFilterOption()
            {
                this.AddGridControl("myGridControl");
                this.SetNoFilterTabPage("tabPageExternalLink");
                this.SetNoFilterTabPage("tabPageNote");
            }

            protected override bool CanDelete(DataRow row)
            {
                if (this.myDocument != null && this.myDocument.Action == StockWorkOrderAction.View || (row["ItemCode"] != DBNull.Value || row["Description"].ToString().Length != 0))
                    return false;
                else
                    return BCE.Data.Convert.ToDecimal(row["SubTotal"]) == Decimal.Zero;
            }
        }

        public class FormEventArgs
        {
            private FormStockWorkOrderEntry myForm;
            private StockWorkOrder myStockWorkOrder;

            public StockWorkOrder StockWorkOrder
            {
                get
                {
                    return this.myStockWorkOrder;
                }
            }

            public FormStockWorkOrderEntry Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public PanelControl HeaderPanel
            {
                get
                {
                    return this.myForm.panelHeader;
                }
            }


            public XtraTabControl TabControl
            {
                get
                {
                    return this.myForm.tabControl1;
                }
            }

            public DataTable MasterTable
            {
                get
                {
                    return this.myStockWorkOrder.DataTableMaster;
                }
            }

            public DataTable DetailTable
            {
                get
                {
                    return this.myStockWorkOrder.DataTableDetail;
                }
            }

            public EditWindowMode EditWindowMode
            {
                get
                {
                    if (this.myStockWorkOrder.Action == StockWorkOrderAction.New)
                        return EditWindowMode.New;
                    else if (this.myStockWorkOrder.Action == StockWorkOrderAction.Edit)
                        return EditWindowMode.Edit;
                    else
                        return EditWindowMode.View;
                }
            }

            public DBSetting DBSetting
            {
                get
                {
                    return this.myForm.myDBSetting;
                }
            }

            public FormEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
            {
                this.myForm = form;
                this.myStockWorkOrder = doc;
            }
        }

        public class FormInitializeEventArgs : FormStockWorkOrderEntry.FormEventArgs
        {
            public FormInitializeEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
              : base(form, doc)
            {
            }
        }

        public class FormDataBindingEventArgs : FormStockWorkOrderEntry.FormEventArgs
        {
            public FormDataBindingEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
              : base(form, doc)
            {
            }
        }

        public class FormShowEventArgs : FormStockWorkOrderEntry.FormEventArgs
        {
            public FormShowEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
              : base(form, doc)
            {
            }
        }

        public class FormClosedEventArgs : FormStockWorkOrderEntry.FormEventArgs
        {
            public FormClosedEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
              : base(form, doc)
            {
            }
        }

        public class FormBeforeSaveEventArgs : FormStockWorkOrderEntry.FormEventArgs
        {
            public FormBeforeSaveEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
              : base(form, doc)
            {
            }
        }

        public class AfterCopyToNewDocumentEventArgs : StockWorkOrderEventArgs
        {
            private StockWorkOrder myFromStock;

            public StockWorkOrderRecord FromMasterRecord
            {
                get
                {
                    return new StockWorkOrderRecord(this.myFromStock);
                }
            }

            internal AfterCopyToNewDocumentEventArgs(StockWorkOrder doc, StockWorkOrder fromDoc)
              : base(doc)
            {
                this.myFromStock = fromDoc;
            }
        }

        public class AfterCopyFromOtherDocumentEventArgs : StockWorkOrderEventArgs
        {
            private StockWorkOrder myFromStock;

            public StockWorkOrderRecord FromMasterRecord
            {
                get
                {
                    return new StockWorkOrderRecord(this.myFromStock);
                }
            }

            internal AfterCopyFromOtherDocumentEventArgs(StockWorkOrder doc, StockWorkOrder fromDoc)
              : base(doc)
            {
                this.myFromStock = fromDoc;
            }
        }

        private void sBtnSelectAllMain_Click(object sender, EventArgs e)
        {
           // this.gvItem.SelectAll();
            //this.gridCtlItem.Focus();
        }

        private void sBtnInsertBS_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.InsertBSDetailBefore));
        }

        private void sbtnDeleteBSDTL_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.DeleteBSDetailselectedRows));
        }

        private void sbtnAddBSDTL_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.AddNewBSDetail));
        }

        private void sbtnUndoBSDtl_Click(object sender, EventArgs e)
        {
            if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gvBOMBS.BeginUpdate();
                    this.myStockWorkOrder.BeginLoadDetailData();
                    this.myBSDetailRecordUndo.Undo();
                    this.myStockWorkOrder.EndLoadDetailData();
                    this.gvBOMBS.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void sbtnUpBS_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveUpBSDetail));
        }

        private void sbtnDownBS_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveDownBSDetail));

        }

        private void sbtnSelectAllBS_Click(object sender, EventArgs e)
        {
            this.gvBOMBS.SelectAll();
            this.gridCtlIBOMBS.Focus();
        }

        private void sbtnAddAPDtl_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.AddNewAPDetail));
        }

        private void sbtnAddPIDtl_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.AddNewPIDetail));
        }

        private void sbtnInsertAllAP_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.InsertAPDetailBefore));
        }
        private void sbtnDeleteAPDtl_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.DeleteAPDetailselectedRows));
        }

        private void sbtnInsertAllPI_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.InsertPIDetailBefore));
        }
        private void sbtnDeletePIDtl_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.DeletePIDetailselectedRows));
        }


        private void sbtnUpPI_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveUpPIDetail));
        }
        private void sbtnUpAP_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveUpAPDetail));
        }

        private void sbtnDownPI_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveDownPIDetail));

        }
        private void sbtnSelectAllPI_Click(object sender, EventArgs e)
        {
            this.gvBOMPI.SelectAll();
            this.gridCtlIBOMPI.Focus();
        }

        private void sbtnDownAP_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveDownAPDetail));

        }

        private void sbtnSelectAllAP_Click(object sender, EventArgs e)
        {
            this.gvBOMAP.SelectAll();
            this.gridCtlIBOMAP.Focus();
        }

        private void sbtnAddOvdDtl_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.AddNewOvdDetail));
        }

        private void sbtnInsertOvd_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.InsertOvdDetailBefore));
        }

        private void sbtnDeleteOvdDtl_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.DeleteOvdDetailselectedRows));

        }

        private void sbtnUndoOvdDtl_Click(object sender, EventArgs e)
        {
            if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gvBOMOvd.BeginUpdate();
                    this.myStockWorkOrder.BeginLoadDetailData();
                    this.myOvdDetailRecordUndo.Undo();
                    this.myStockWorkOrder.EndLoadDetailData();
                    this.gvBOMOvd.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void sbtnUpOvd_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveUpOvdDetail));

        }

        private void sbtnDownOvd_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveDownOvdDetail));

        }

        private void sbtnSelectAllOvd_Click(object sender, EventArgs e)
        {
            this.gvBOMOvd.SelectAll();
            this.gridCtlIBOMOvd.Focus();
        }
       
        private void luMachineCode_EditValueChanged(object sender, EventArgs e)
        {
            UpdateMachineDescription();
        }
        private void FilterItemUOM(GridView view)
        {
            if (view.FocusedColumn != null && view.FocusedColumn.FieldName == "UOM")
            {
                DataRow dataRow = view.GetDataRow(view.FocusedRowHandle);
                if (dataRow != null)
                    DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.FilterItemCode(dataRow["ItemCode"].ToString());
            }
        }
        private void FilterItemUOM(TreeList view)
        {
            if (view.FocusedColumn != null && view.FocusedColumn.FieldName == "UOM")
            {
                TreeListNode dataRow = view.GetNodeByVisibleIndex(view.GetVisibleIndexByNode(view.FocusedNode));
                if (dataRow != null)
                    DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.FilterItemCode(dataRow["ItemCode"].ToString());
            }
        }
        private void FilterItemBOM(GridView view)
        {
            if (view.FocusedColumn != null && view.FocusedColumn.FieldName == "BOMCode")
            {
                DataRow dataRow = view.GetDataRow(view.FocusedRowHandle);
                if (dataRow != null)
                {
                    //ItemBOMLookupEditBuilder itembomlookup = new ItemBOMLookupEditBuilder();
                    //tembomlookup.BuildLookupEdit(this.repleItemBOM, this.myDBSetting);
                    itembomlookup.FilterItemCode(dataRow["ItemCode"].ToString());
                }
            }
        }

        private void luProductCode_EditValueChanged(object sender, EventArgs e)
        {
            //UpdateProductItemDescription();
            //string sitemcode = "";
            //if (luProductCode.EditValue != null && luProductCode.EditValue != DBNull.Value)
            //    sitemcode = luProductCode.EditValue.ToString();
            //ItemBOMLookupEditBuilder itembomlookup = new ItemBOMLookupEditBuilder();
            //itembomlookup.BuildLookupEdit(this.luBOM.Properties, this.myDBSetting);
            //itembomlookup.FilterItemCode(sitemcode);
        }

        private void gvItem_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (this.myStockWorkOrder != null)
            {
                this.FilterItemUOM(this.gridCtlItem);
                this.ReloadInquiryUserControl();
                SetDetailButtonState();
            }
        }

        private void gvItem_FocusedColumnChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs e)
        {
            if (this.myStockWorkOrder != null)
            {
                this.FilterItemUOM(this.gridCtlItem);
            }
        }

        private void gvItem_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gvBOMBS_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            this.SetBSDetailButtonState();
        }

        private void gvBOMAP_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            DataRow dataRow = gvBOMAP.GetDataRow(gvBOMAP.FocusedRowHandle);
            if (dataRow == null)
                return;
            if (this.myStockWorkOrder != null)
            {
                //this.FilterItemUOM(this.gridCtlItem);
               // this.FilterItemBOM(this.gvBOMAP);
                this.ReloadInquiryUserControl();
                this.SetAPDetailButtonState();



                //if (dataRow["FromDocDtlKey"] != null && dataRow["FromDocDtlKey"] != DBNull.Value)
                //{
                //    if (dataRow["FromDocDtlKey"].ToString() != "")
                //    {
                //        repItemProductDebtorCode.ReadOnly = true;
                //    }
                //    else
                //    {
                //        repItemProductDebtorCode.ReadOnly = false;
                //    }
                //}
                //else
                //{
                //    repItemProductDebtorCode.ReadOnly = false;
                //}

                
                //if (e.FocusedColumn.FieldName == "DebtorCode")
                //{
                //repitemAPDebtorCode.ReadOnly = false;
                //if (luDebtorCode.EditValue != null && luDebtorCode.EditValue != DBNull.Value)
                //{
                //    if (luDebtorCode.EditValue.ToString() != "")
                //    {
                //        repitemAPDebtorCode.ReadOnly = true;
                //    }
                //}

                //}

            }
        }

        private void gvBOMOvd_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            this.SetOvdDetailButtonState();
        }

        private void cbDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void cbDocType_EditValueChanged(object sender, EventArgs e)
        {
           
        }

        private void deTarget_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void luDebtorCode_EditValueChanged(object sender, EventArgs e)
        {
            UpdateDebtorDescription();
        }

        private void luProjNo_EditValueChanged(object sender, EventArgs e)
        {
            UpdateProjectDescription();
        }

        private void gvBOMAP_FocusedColumnChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs e)
        {
            DataRow drDetail = gvBOMAP.GetDataRow(gvBOMAP.FocusedRowHandle);
            if (this.myStockWorkOrder != null)
            {
                //if (e.FocusedColumn.FieldName == "UOM")
                //    this.FilterItemUOM(this.gvBOMAP);
                //if (e.FocusedColumn.FieldName == "BOMCode")
                //    this.FilterItemBOM(this.gvBOMAP);


                //if (e.FocusedColumn.FieldName == "DebtorCode")
                //{
                //    repItemProductDebtorCode.ReadOnly = false;
                //    if (drDetail["FromDocDtlKey"] != null && drDetail["FromDocDtlKey"] != DBNull.Value)
                //    {
                //        if (drDetail["FromDocDtlKey"].ToString() != "")
                //        {
                //            repItemProductDebtorCode.ReadOnly = true;
                //        }
                //    }

                //}
            }
        }

        private void gvBOMAP_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
           // this.FilterItemBOM(this.gvBOMAP);

            //if (e.Column.FieldName == "UOM")
            //{
            //    if (this.myStockWorkOrder != null)
            //    {
            //        this.FilterItemUOM(this.gvBOMAP);
            //    }
            //}

            //if (e.Column.FieldName == "BOMCode")
            //{
            //    if (this.myStockWorkOrder != null)
            //    {
            //        this.FilterItemBOM(this.gvBOMAP);
            //    }
            //}
        }

        private void gridCtlItem_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
        {

        }

        private void btnCancelDetail_Click(object sender, EventArgs e)
        {
            if (gvBOMPI.FocusedRowHandle == -1)
                return;
            DataRow drProduct = gvBOMPI.GetDataRow(gvBOMPI.FocusedRowHandle);
            object obj = myDBSetting.ExecuteScalar("select status from RPA_WO where DocKey=?", (object)this.myStockWorkOrder.DocKey);
            if (obj != null && obj != DBNull.Value)
            {
                if (drProduct["Status"].ToString() != WorkOrderStatusOptions.Planned.ToString() && drProduct["Status"].ToString() != WorkOrderStatusOptions.Pending.ToString())// WorkOrderStatusOptions.Closed.ToString() || obj.ToString() == WorkOrderStatusOptions.Cancel.ToString())
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, "Status Item Product has been " + drProduct["Status"].ToString() + ", Cancel aborted...");
                    return;
                }
            }
            if (drProduct["Status"].ToString() == WorkOrderStatusOptions.Planned.ToString() || drProduct["Status"].ToString() == WorkOrderStatusOptions.Pending.ToString())
            {
                drProduct["Status"] = WorkOrderStatusOptions.Cancel.ToString();
                drProduct["Cancelled"] = BCE.Data.Convert.BooleanToText(true);
                drProduct["CancelledUserID"] = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
                drProduct["CancelledDate"] = BCE.AutoCount.Application.DBSetting.GetServerTime();

            }
            this.Save(SaveDocumentAction.Save);
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {

        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
       
        }

        private void ceLevel_CheckedChanged(object sender, EventArgs e)
        {
            if (ceLevel.Checked)
            {
               // gridCtlItemSummary.
                colLevelSum.Visible = true;
                colLevelSum.SortIndex = 0;
                colLevelSum.VisibleIndex = 0;
            }
            else
            {
                colLevelSum.Visible = false;
                colLevelSum.SortIndex = -1;
            }
            this.myStockWorkOrder.ProcessRawMaterialSummary(ceLevel.Checked);
        }

        private void sbtnSelectAllAP_Click_1(object sender, EventArgs e)
        {
            this.gvBOMAP.SelectAll();
            this.gridCtlIBOMAP.Focus();
        }

        private void sbtnUndoAPDtl_Click(object sender, EventArgs e)
        {
            if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gvBOMAP.BeginUpdate();
                    this.myStockWorkOrder.BeginLoadDetailData();
                    this.myAPDetailRecordUndo.Undo();
                    this.myStockWorkOrder.EndLoadDetailData();
                    this.gvBOMAP.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void sbtnUPAP_Click_1(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveUpAPDetail));
        }

        private void sbtnDownAP_Click_1(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveDownAPDetail));
        }

        private void sbtnUndoPIDtl_Click(object sender, EventArgs e)
        {
            if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gvBOMPI.BeginUpdate();
                    this.myStockWorkOrder.BeginLoadDetailData();
                    this.myPIDetailRecordUndo.Undo();
                    this.myStockWorkOrder.EndLoadDetailData();
                    this.gvBOMPI.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void gvBOMPI_FocusedColumnChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs e)
        {
            DataRow drDetail = gvBOMPI.GetDataRow(gvBOMPI.FocusedRowHandle);
            if (this.myStockWorkOrder != null)
            {
                if (e.FocusedColumn.FieldName == "UOM")
                    this.FilterItemUOM(this.gvBOMPI);
                if (e.FocusedColumn.FieldName == "BOMCode")
                    this.FilterItemBOM(this.gvBOMPI);


                if (e.FocusedColumn.FieldName == "DebtorCode")
                {
                    repItemProductDebtorCode.ReadOnly = false;
                    if (drDetail["FromDocDtlKey"] != null && drDetail["FromDocDtlKey"] != DBNull.Value)
                    {
                        if (drDetail["FromDocDtlKey"].ToString() != "")
                        {
                            repItemProductDebtorCode.ReadOnly = true;
                        }
                    }

                }
            }
        }

        private void gvBOMPI_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            DataRow dataRow = gvBOMPI.GetDataRow(gvBOMPI.FocusedRowHandle);
            if (dataRow == null)
                return;
            if (this.myStockWorkOrder != null)
            {
                this.FilterItemUOM(this.gridCtlItem);
                this.FilterItemBOM(this.gvBOMPI);
                this.ReloadInquiryUserControl();
                this.SetPIDetailButtonState();



                if (dataRow["FromDocDtlKey"] != null && dataRow["FromDocDtlKey"] != DBNull.Value)
                {
                    if (dataRow["FromDocDtlKey"].ToString() != "")
                    {
                        repItemProductDebtorCode.ReadOnly = true;
                    }
                    else
                    {
                        repItemProductDebtorCode.ReadOnly = false;
                    }
                }
                else
                {
                    repItemProductDebtorCode.ReadOnly = false;
                }


                //if (e.FocusedColumn.FieldName == "DebtorCode")
                //{
                //    repitemPIDebtorCode.ReadOnly = false;
                //    if (luDebtorCode.EditValue != null && luDebtorCode.EditValue != DBNull.Value)
                //    {
                //        if (luDebtorCode.EditValue.ToString() != "")
                //        {
                //            repitemPIDebtorCode.ReadOnly = true;
                //        }
                //    }

                //}

            }
        }

        private void gvBOMAP_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            GridView view = sender as GridView;
            DataRow APRow = view.GetDataRow(e.ListSourceRowIndex);
            if(e.Column.FieldName=="ProductCode")
            {
                DataRow[] drPI =myStockWorkOrder.PIDetailTable.Select("DtlKey="+ APRow["FromDocDtlKey"] + "", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
                if(drPI!=null)
                    e.Value = drPI[0]["ItemCode"].ToString();
            }
        }
    }
}
