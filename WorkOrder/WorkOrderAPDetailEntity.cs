﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.LinkDetailEntity
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using System.Data;

namespace Production.WorkOrder
{
    public class WorkOrderAPDetailEntity
    {
        private DataRow myRow;
        public int Seq
        {
            get
            {
                return BCE.Data.Convert.ToInt32(this.myRow["Seq"]);
            }
        }
        public DataRow Row
        {
            get
            {
                return this.myRow;
            }
        }

        internal WorkOrderAPDetailEntity(DataRow aRow)
        {
            this.myRow = aRow;
        }
    }
}
