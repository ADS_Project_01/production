﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.FormStockAssemblyFind
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Controller.FunctionVisibility;
using BCE.AutoCount.Data;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Production.WorkOrder
{
  [SingleInstanceThreadForm]
  public class FormStockWorkFind : XtraForm
  {
    private StockWorkOrderCommand myStockWorkOrderCmd;
    private bool isInAOP;
    private DataTable mySearchDataTable;
    private long[] mySelectedDocKeys;
    private string mySelectedDocNos;
    private bool myInSearch;
    private AdvancedStockWorkOrderCriteria myCriteria;
    private DBSetting myDBSetting;
    private IContainer components;
    private UCSearchResult ucSearchResult1;
    private CheckEdit checkEditDescription;
    private CheckEdit checkEditDetailUDF;
    private CheckEdit checkEditDocDate;
    private CheckEdit checkEditRefDocNo;
    private CheckEdit checkEditSANo;
    private CheckEdit checkEditUDF;
    private CheckEdit chkedtShowOnlyOutstanding;
    private GroupControl groupBox1;
    private PanelControl panel1;
    private PanelControl panel3;
    private PanelControl pnAOProcessing;
    private PanelControl pnClose;
    private PanelControl pnEdit;
    private RepositoryItemCheckEdit repositoryItemCheckEditDelete;
    private RepositoryItemTextEdit repositoryItemTextEdit1;
    private SimpleButton btnAdvSearch;
    private SimpleButton sbtnAOProcessing;
    private SimpleButton sbtnClear;
    private SimpleButton sbtnClose;
    private SimpleButton sbtnDelete;
    private SimpleButton sbtnEdit;
    private SimpleButton sbtnSearch;
    private SimpleButton sbtnView;
    private TextEdit textEditKeyWord;
    private GridColumn colAssemblyCost;
    private GridColumn colBatchNo;
    private GridColumn colCancelled;
    private GridColumn colCreatedTimeStamp;
    private GridColumn colCreatedUserID;
    private GridColumn colDeptNo;
    private GridColumn colDescription;
    private GridColumn colDocDate;
    private GridColumn colDocNo;
    private GridColumn colItemCode;
    private GridColumn colLastModified;
    private GridColumn colLastModifiedUserID;
    private GridColumn colLocation;
    private GridColumn colNetTotal;
    private GridColumn colPrintCount;
    private GridColumn colProjNo;
    private GridColumn colQty;
    private GridColumn colRefDocNo;
    private GridColumn colRemark1;
    private GridColumn colRemark2;
    private GridColumn colRemark3;
    private GridColumn colRemark4;
    private GridColumn colTick;
    private GridColumn colTotal;
    private GridControl myStockAssemblyGridControl;
    private GridView gridViewStockAssembly;
    private Label labelKeyWord;

    public DataTable SearchDataTable
    {
      get
      {
        return this.mySearchDataTable;
      }
    }

    public long[] SelectedDocKeys
    {
      get
      {
        return this.mySelectedDocKeys;
      }
    }

    public string SelectedDocNos
    {
      get
      {
        return this.mySelectedDocNos;
      }
    }

    public FormStockWorkFind(StockWorkOrderCommand stockWorkOrderCmd)
      : this(stockWorkOrderCmd, false)
    {
    }

    public FormStockWorkFind(StockWorkOrderCommand stockWorkOrderCmd, bool isCallFromAOP)
    {
      this.InitializeComponent();
      this.Icon = BCE.AutoCount.Application.Icon;
      this.myInSearch = false;
      this.mySearchDataTable = new DataTable();
      this.myStockWorkOrderCmd = stockWorkOrderCmd;
      this.myDBSetting = this.myStockWorkOrderCmd.DBSetting;
      StockWorkOrderFindCriteria orderFindCriteria = (StockWorkOrderFindCriteria) null;
      try
      {
        orderFindCriteria = (StockWorkOrderFindCriteria) PersistenceUtil.LoadUserSetting("StockWorkOrderFind.setting");
      }
      catch
      {
      }
      if (orderFindCriteria != null)
      {
        this.checkEditDescription.Checked = orderFindCriteria.Description;
        this.checkEditSANo.Checked = orderFindCriteria.DocNo;
        this.checkEditDocDate.Checked = orderFindCriteria.DocDate;
        this.checkEditRefDocNo.Checked = orderFindCriteria.RefDocNo;
        this.checkEditUDF.Checked = orderFindCriteria.UDF;
        this.checkEditDetailUDF.Checked = orderFindCriteria.DetailUDF;
        this.chkedtShowOnlyOutstanding.Checked = orderFindCriteria.ShowOnlyOutstaning;
      }
      this.Tag = (object) EnterKeyMessageFilter.NoFilter;
      this.Icon = BCE.AutoCount.Application.Icon;
      this.ucSearchResult1.Initialize(this.gridViewStockAssembly, "Tick");
      this.myStockAssemblyGridControl.DataSource = (object) this.mySearchDataTable;
      this.ucSearchResult1.ShowButtons = true;
      this.InitFormControls();
      new UDFUtil(this.myDBSetting).SetupGrid(this.gridViewStockAssembly, "RPA_WO", false);
      CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gridViewStockAssembly, new EventHandler(this.ReloadAllColumns));
      this.isInAOP = isCallFromAOP;
      if (this.isInAOP)
        this.pnEdit.Visible = false;
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
      string fieldname1 = "DocDate";
      string fieldtype1 = "Date";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "Qty";
      string fieldtype2 = "Quantity";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "Total";
      string fieldtype3 = "Currency";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "AssemblyCost";
      string fieldtype4 = "Currency";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "NetTotal";
      string fieldtype5 = "Currency";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "LastModified";
      string fieldtype6 = "DateTime";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "CreatedTimeStamp";
      string fieldtype7 = "DateTime";
      formControlUtil.AddField(fieldname7, fieldtype7);
      FormStockWorkFind stockAssemblyFind = this;
      formControlUtil.InitControls((Control) stockAssemblyFind);
      this.InitGroupSummary();
    }

    private void InitGroupSummary()
    {
      this.gridViewStockAssembly.GroupSummary.Clear();
      GridGroupSummaryItemCollection groupSummary1 = this.gridViewStockAssembly.GroupSummary;
      int num1 = 3;
      string fieldName1 = "DocNo";
            // ISSUE: variable of the null type
            GridColumn local1 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local2 =  GridGroupSummaryItemStringId.Count;
      object[] objArray1 = new object[1];
      int index1 = 0;
      string str = "{0}";
      objArray1[index1] = (object) str;
      string string1 = BCE.Localization.Localizer.GetString((Enum) local2, objArray1);
      groupSummary1.Add((SummaryItemType) num1, fieldName1, (GridColumn) local1, string1);
      if (this.colNetTotal.Visible)
      {
        GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockAssembly.GroupSummary;
        int num2 = 0;
        string fieldName2 = "NetTotal";
                // ISSUE: variable of the null type
                GridColumn local3 = null;
        // ISSUE: variable of a boxed type
        GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.NetTotal;
        object[] objArray2 = new object[1];
        int index2 = 0;
        string currencyFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCurrencyFormatString(0);
        objArray2[index2] = (object) currencyFormatString;
        string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
        groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      }
      if (this.colTotal.Visible)
      {
        GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockAssembly.GroupSummary;
        int num2 = 0;
        string fieldName2 = "Total";
                // ISSUE: variable of the null type
                GridColumn local3 = null;
        // ISSUE: variable of a boxed type
        GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.Total;
        object[] objArray2 = new object[1];
        int index2 = 0;
        string currencyFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCurrencyFormatString(0);
        objArray2[index2] = (object) currencyFormatString;
        string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
        groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      }
      if (this.colAssemblyCost.Visible)
      {
        GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockAssembly.GroupSummary;
        int num2 = 0;
        string fieldName2 = "AssemblyCost";
                // ISSUE: variable of the null type
                GridColumn local3 = null;
        // ISSUE: variable of a boxed type
        GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.AssemblyCost;
        object[] objArray2 = new object[1];
        int index2 = 0;
        string costFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCostFormatString(0);
        objArray2[index2] = (object) costFormatString;
        string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
        groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      }
    }

    private void SearchMaster(SearchCriteria criteria, string columnSQL)
    {
      if (!this.myInSearch)
      {
        this.myInSearch = true;
        Cursor current = Cursor.Current;
        Cursor.Current = Cursors.WaitCursor;
        try
        {
          BCE.XtraUtils.GridViewUtils.UpdateData(this.gridViewStockAssembly);
          this.myStockAssemblyGridControl.DataSource = (object) null;
          this.myStockWorkOrderCmd.SearchMaster(criteria, columnSQL, this.mySearchDataTable, "Tick");
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
          return;
        }
        finally
        {
          this.myStockAssemblyGridControl.DataSource = (object) this.mySearchDataTable;
          Cursor.Current = current;
          this.myInSearch = false;
        }
        if (this.mySearchDataTable.Columns.IndexOf("Tick") < 0)
        {
          this.mySearchDataTable.Columns.Add(new DataColumn()
          {
            DataType = typeof (bool),
            DefaultValue = (object) false,
            AllowDBNull = true,
            Caption = "Tick",
            ColumnName = "Tick"
          });
          this.mySearchDataTable.BeginLoadData();
          try
          {
            foreach (DataRow dataRow in (InternalDataCollectionBase) this.mySearchDataTable.Rows)
              dataRow["Tick"] = (object) false;
            this.mySearchDataTable.AcceptChanges();
          }
          finally
          {
            this.mySearchDataTable.EndLoadData();
          }
        }
        this.sbtnEdit.Enabled = true;
        this.sbtnView.Enabled = true;
        this.sbtnDelete.Enabled = true;
        this.sbtnAOProcessing.Enabled = true;
      }
    }

    private void Search(bool allColumns)
    {
      if (!this.myInSearch)
      {
        StockWorkOrderCriteria assemblyOrderCriteria = new StockWorkOrderCriteria();
        assemblyOrderCriteria.MatchAll = false;
        assemblyOrderCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
        if (this.checkEditDescription.Checked)
          assemblyOrderCriteria.Description = this.textEditKeyWord.Text;
        if (this.checkEditSANo.Checked)
          assemblyOrderCriteria.DocNo = this.textEditKeyWord.Text;
        if (this.checkEditDocDate.Checked)
          assemblyOrderCriteria.DocDate = this.textEditKeyWord.Text;
        if (this.checkEditRefDocNo.Checked)
          assemblyOrderCriteria.RefDocNo = this.textEditKeyWord.Text;
        if (this.checkEditUDF.Checked)
          assemblyOrderCriteria.UDF = this.textEditKeyWord.Text;
        if (this.checkEditDetailUDF.Checked)
          assemblyOrderCriteria.DetailUDF = this.textEditKeyWord.Text;
        assemblyOrderCriteria.ShowOnlyOutstanding = this.chkedtShowOnlyOutstanding.Checked;
        this.SearchMaster((SearchCriteria) assemblyOrderCriteria, this.BuildSQLColumns(allColumns));
      }
    }

    private void sbtnSearch_Click(object sender, EventArgs e)
    {
      this.Search(false);
      this.myStockAssemblyGridControl.Focus();
    }

    private void btnAdvSearch_Click(object sender, EventArgs e)
    {
      if (this.myCriteria == null)
        this.myCriteria = new AdvancedStockWorkOrderCriteria(this.myDBSetting);
      this.myCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
      using (FormAdvancedSearch formAdvancedSearch = new FormAdvancedSearch((SearchCriteria) this.myCriteria, this.myDBSetting))
      {
        if (formAdvancedSearch.ShowDialog((IWin32Window) this) == DialogResult.OK)
        {
          this.ucSearchResult1.KeepSearchResult = this.myCriteria.KeepSearchResult;
          this.SearchMaster((SearchCriteria) this.myCriteria, this.BuildSQLColumns(false));
        }
      }
    }

    private void sbtnClear_Click(object sender, EventArgs e)
    {
      this.textEditKeyWord.Text = string.Empty;
      this.checkEditDescription.Checked = true;
      this.checkEditSANo.Checked = true;
      this.checkEditDocDate.Checked = true;
      this.checkEditRefDocNo.Checked = false;
    }

    private string BuildSQLColumns(bool allColumns)
    {
      string str = "A.DocKey";
      foreach (GridColumn gridColumn in (CollectionBase) this.gridViewStockAssembly.Columns.View.Columns)
      {
        if ((gridColumn.VisibleIndex > -1 || allColumns && gridColumn.OptionsColumn.ShowInCustomizationForm) && (gridColumn.FieldName != null && gridColumn.FieldName.Length != 0 && gridColumn.FieldName != "Tick"))
          str = str + ", A." + gridColumn.FieldName;
      }
      return str.Trim();
    }

    private void ReloadAllColumns(object sender, EventArgs e)
    {
      this.Search(true);
    }

    private void FormStockAssemblySearch_Activated(object sender, EventArgs e)
    {
    }

    private void repositoryItemTextEdit1_FormatEditValue(object sender, ConvertEditValueEventArgs e)
    {
      if (e.Value != null)
      {
        if (e.Value.ToString() == "T")
          e.Value = (object) BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Cancelled, new object[0]);
        else
          e.Value = (object) "";
      }
    }

    private bool CheckSelection()
    {
      BCE.XtraUtils.GridViewUtils.UpdateData(this.gridViewStockAssembly);
      DataRow[] dataRowArray = this.mySearchDataTable.Select("Tick = True");
      if (dataRowArray.Length == 0)
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum)StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderSelected, new object[0]));
        this.DialogResult = DialogResult.None;
        return false;
      }
      else
      {
        int num = 0;
        this.mySelectedDocKeys = new long[dataRowArray.Length];
        this.mySelectedDocNos = "";
        foreach (DataRow dataRow in dataRowArray)
        {
          this.mySelectedDocKeys[num++] = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
          if (this.mySelectedDocNos.Length != 0)
            this.mySelectedDocNos = this.mySelectedDocNos + ", ";
          this.mySelectedDocNos = this.mySelectedDocNos + "'" + dataRow["DocNo"].ToString() + "'";
        }
        return true;
      }
    }

    private bool Delete(long[] docKeyList)
    {
      bool flag = false;
      try
      {
        foreach (long docKey in docKeyList)
        {
          this.myStockWorkOrderCmd.Delete(docKey);
          DataRow dataRow = this.mySearchDataTable.Rows.Find((object) docKey);
          if (dataRow != null)
            dataRow.Delete();
        }
        flag = true;
        FormStockWorkFind stockAssemblyFind = this;
                // ISSUE: variable of a boxed type
                StockAssemblyOrderStringId local = StockAssemblyOrderStringId.ShowMessage_TotalDeletedSuccessfully;
        object[] objArray = new object[1];
        int index = 0;
        string str = docKeyList.Length.ToString();
        objArray[index] = (object) str;
        string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
        AppMessage.ShowMessage((IWin32Window) stockAssemblyFind, @string);
      }
      catch (DBConcurrencyException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum)StockAssemblyOrderStringId.ErrorMessage_OperationAborted, new object[0]));
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
      }
      return flag;
    }

    private bool Edit(long[] docKeyList)
    {
      bool flag = true;
      foreach (long docKey in docKeyList)
      {
        try
        {
          StockWorkOrder stockWorkOrder = this.myStockWorkOrderCmd.Edit(docKey);
          if (stockWorkOrder == null)
            AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum)StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
          else
            FormStockWorkOrderCmd.StartEntryForm(stockWorkOrder);
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
          flag = false;
        }
      }
      return flag;
    }

    private bool View(long[] docKeyList)
    {
      bool flag = true;
      foreach (long docKey in docKeyList)
      {
        try
        {
          StockWorkOrder stockWorkOrder = this.myStockWorkOrderCmd.View(docKey);
          if (stockWorkOrder == null)
            AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum)StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
          else
            FormStockWorkOrderCmd.StartEntryForm(stockWorkOrder);
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
          flag = false;
        }
      }
      return flag;
    }

    private void sbtnEdit_Click(object sender, EventArgs e)
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_EDIT", (XtraForm) this) && this.CheckSelection())
      {
        if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
        {
          foreach (long docKey in this.mySelectedDocKeys)
          {
            if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", docKey) > 0 && !this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_EDIT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedEditPrintedDocument, new object[0])))
              return;
          }
        }
        if (this.mySelectedDocKeys.Length > 1)
        {
                    // ISSUE: variable of a boxed type
                    StockAssemblyOrderStringId local = StockAssemblyOrderStringId.ConfirmMessage_EditFollowingStockAssemblyOrder;
          object[] objArray = new object[1];
          int index = 0;
          string str = this.mySelectedDocNos;
          objArray[index] = (object) str;
          if (!AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum) local, objArray)))
            return;
        }
        if (this.Edit(this.mySelectedDocKeys))
          this.Close();
      }
    }

    private void sbtnView_Click(object sender, EventArgs e)
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_VIEW", (XtraForm) this) && this.CheckSelection())
      {
        if (this.mySelectedDocKeys.Length > 1)
        {
                    // ISSUE: variable of a boxed type
                    StockAssemblyOrderStringId local = StockAssemblyOrderStringId.ConfirmMessage_ViewFollowingStockAssemblyOrder;
          object[] objArray = new object[1];
          int index = 0;
          string str = this.mySelectedDocNos;
          objArray[index] = (object) str;
          if (!AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum) local, objArray)))
            return;
        }
        if (this.View(this.mySelectedDocKeys))
          this.Close();
      }
    }

    private void sbtnDelete_Click(object sender, EventArgs e)
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_DELETE", (XtraForm) this) && this.CheckSelection())
      {
        if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
        {
          foreach (long docKey in this.mySelectedDocKeys)
          {
            if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", docKey) > 0 && !this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_DELETE", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedDeletePrintedDocument, new object[0])))
              return;
          }
        }
                // ISSUE: variable of a boxed type
                StockAssemblyOrderStringId local = StockAssemblyOrderStringId.ConfirmMessage_DeleteFollowingStockAssemblyOrder;
        object[] objArray = new object[1];
        int index = 0;
        string str = this.mySelectedDocNos;
        objArray[index] = (object) str;
        if (AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum) local, objArray)) && this.Delete(this.mySelectedDocKeys))
          this.Close();
      }
    }

    private void sbtnClose_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void gridViewStockAssembly_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      GridView gridView = (GridView) sender;
      int[] selectedRows = gridView.GetSelectedRows();
      if (selectedRows != null && selectedRows.Length != 1)
      {
        for (int index = 0; index < selectedRows.Length; ++index)
          gridView.GetDataRow(selectedRows[index])["Tick"] = (object) true;
      }
    }

    private void FormStockAssemblyFind_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == (Keys) 113)
      {
        if (this.sbtnEdit.Enabled)
          this.sbtnEdit.PerformClick();
        e.Handled = true;
      }
      else if (e.KeyCode == (Keys) 123)
      {
        if (this.sbtnView.Enabled)
          this.sbtnView.PerformClick();
        e.Handled = true;
      }
      else if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space))
      {
        Control control = this.ActiveControl;
        while (control != null && control != this.textEditKeyWord)
          control = control.Parent;
        if (control == null)
        {
          if (this.sbtnDelete.Enabled)
            this.sbtnDelete.PerformClick();
          e.Handled = true;
        }
      }
      else if (e.KeyCode == (Keys) 114)
      {
        this.textEditKeyWord.SelectAll();
        this.textEditKeyWord.Focus();
        e.Handled = true;
      }
    }

    private void gridViewStockAssembly_Layout(object sender, EventArgs e)
    {
      if (this.myStockAssemblyGridControl.DataSource != null)
        this.InitGroupSummary();
    }

    private void FormStockAssemblyFind_Load(object sender, EventArgs e)
    {
      this.sbtnAOProcessing.Visible = new BusinessFlow(this.myDBSetting).IsAssemblyOrderProcessingVisible();
    }

    private void sbtnAOProcessing_Click(object sender, EventArgs e)
    {
      if (StockWorkOrderCommand.StartBusinessFlow != null && this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("BFLOW_AOP_OPEN", (XtraForm) this) && this.CheckSelection())
      {
        if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
        {
          foreach (long docKey in this.mySelectedDocKeys)
          {
            if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_WO", docKey) > 0 && !this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("RPA_WO_PRINTED_EDIT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedEditPrintedDocument, new object[0])))
              return;
          }
        }
        if (this.mySelectedDocKeys.Length > 1)
        {
                    // ISSUE: variable of a boxed type
                    StockAssemblyOrderStringId local = StockAssemblyOrderStringId.ConfirmMessage_EditFollowingStockAssemblyOrder;
          object[] objArray = new object[1];
          int index = 0;
          string str = this.mySelectedDocNos;
          objArray[index] = (object) str;
          if (!AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum) local, objArray)))
            return;
        }
        if (this.AOProcessing(this.mySelectedDocKeys))
          this.Close();
      }
    }

    private bool AOProcessing(long[] docKeyList)
    {
      bool flag = true;
      foreach (long docKey in docKeyList)
        StockWorkOrderCommand.StartBusinessFlow(this.myDBSetting, docKey);
      return flag;
    }

    protected override void Dispose(bool disposing)
    {
      StockWorkOrderFindCriteria orderFindCriteria = new StockWorkOrderFindCriteria();
      int num1 = this.checkEditDescription.Checked ? 1 : 0;
      orderFindCriteria.Description = num1 != 0;
      int num2 = this.checkEditSANo.Checked ? 1 : 0;
      orderFindCriteria.DocNo = num2 != 0;
      int num3 = this.checkEditDocDate.Checked ? 1 : 0;
      orderFindCriteria.DocDate = num3 != 0;
      int num4 = this.checkEditRefDocNo.Checked ? 1 : 0;
      orderFindCriteria.RefDocNo = num4 != 0;
      int num5 = this.checkEditUDF.Checked ? 1 : 0;
      orderFindCriteria.UDF = num5 != 0;
      int num6 = this.checkEditDetailUDF.Checked ? 1 : 0;
      orderFindCriteria.DetailUDF = num6 != 0;
      int num7 = this.chkedtShowOnlyOutstanding.Checked ? 1 : 0;
      orderFindCriteria.ShowOnlyOutstaning = num7 != 0;
      string fileName = "StockWorkOrderFind.setting";
      PersistenceUtil.SaveUserSetting((object) orderFindCriteria, fileName);
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormStockWorkFind));
      this.panel1 = new PanelControl();
      this.btnAdvSearch = new SimpleButton();
      this.groupBox1 = new GroupControl();
      this.chkedtShowOnlyOutstanding = new CheckEdit();
      this.checkEditUDF = new CheckEdit();
      this.checkEditDetailUDF = new CheckEdit();
      this.checkEditSANo = new CheckEdit();
      this.checkEditDescription = new CheckEdit();
      this.checkEditDocDate = new CheckEdit();
      this.checkEditRefDocNo = new CheckEdit();
      this.sbtnClear = new SimpleButton();
      this.sbtnSearch = new SimpleButton();
      this.textEditKeyWord = new TextEdit();
      this.labelKeyWord = new Label();
      this.panel3 = new PanelControl();
      this.pnAOProcessing = new PanelControl();
      this.sbtnAOProcessing = new SimpleButton();
      this.pnEdit = new PanelControl();
      this.sbtnDelete = new SimpleButton();
      this.sbtnView = new SimpleButton();
      this.sbtnEdit = new SimpleButton();
      this.pnClose = new PanelControl();
      this.sbtnClose = new SimpleButton();
      this.ucSearchResult1 = new UCSearchResult();
      this.myStockAssemblyGridControl = new GridControl();
      this.gridViewStockAssembly = new GridView();
      this.colTick = new GridColumn();
      this.repositoryItemCheckEditDelete = new RepositoryItemCheckEdit();
      this.colDocNo = new GridColumn();
      this.colDocDate = new GridColumn();
      this.colDescription = new GridColumn();
      this.colItemCode = new GridColumn();
      this.colLocation = new GridColumn();
      this.colBatchNo = new GridColumn();
      this.colProjNo = new GridColumn();
      this.colDeptNo = new GridColumn();
      this.colQty = new GridColumn();
      this.colTotal = new GridColumn();
      this.colAssemblyCost = new GridColumn();
      this.colNetTotal = new GridColumn();
      this.colRemark1 = new GridColumn();
      this.colRemark2 = new GridColumn();
      this.colRemark3 = new GridColumn();
      this.colRemark4 = new GridColumn();
      this.colPrintCount = new GridColumn();
      this.colCancelled = new GridColumn();
      this.repositoryItemTextEdit1 = new RepositoryItemTextEdit();
      this.colLastModified = new GridColumn();
      this.colLastModifiedUserID = new GridColumn();
      this.colCreatedTimeStamp = new GridColumn();
      this.colCreatedUserID = new GridColumn();
      this.colRefDocNo = new GridColumn();
      this.panel1.BeginInit();
      this.panel1.SuspendLayout();
      this.groupBox1.BeginInit();
      this.groupBox1.SuspendLayout();
      this.chkedtShowOnlyOutstanding.Properties.BeginInit();
      this.checkEditUDF.Properties.BeginInit();
      this.checkEditDetailUDF.Properties.BeginInit();
      this.checkEditSANo.Properties.BeginInit();
      this.checkEditDescription.Properties.BeginInit();
      this.checkEditDocDate.Properties.BeginInit();
      this.checkEditRefDocNo.Properties.BeginInit();
      this.textEditKeyWord.Properties.BeginInit();
      this.panel3.BeginInit();
      this.panel3.SuspendLayout();
      this.pnAOProcessing.BeginInit();
      this.pnAOProcessing.SuspendLayout();
      this.pnEdit.BeginInit();
      this.pnEdit.SuspendLayout();
      this.pnClose.BeginInit();
      this.pnClose.SuspendLayout();
      this.myStockAssemblyGridControl.BeginInit();
      this.gridViewStockAssembly.BeginInit();
      this.repositoryItemCheckEditDelete.BeginInit();
      this.repositoryItemTextEdit1.BeginInit();
      this.SuspendLayout();
      componentResourceManager.ApplyResources((object) this.panel1, "panel1");
      this.panel1.BorderStyle = BorderStyles.NoBorder;
      this.panel1.Controls.Add((Control) this.btnAdvSearch);
      this.panel1.Controls.Add((Control) this.groupBox1);
      this.panel1.Controls.Add((Control) this.sbtnClear);
      this.panel1.Controls.Add((Control) this.sbtnSearch);
      this.panel1.Controls.Add((Control) this.textEditKeyWord);
      this.panel1.Controls.Add((Control) this.labelKeyWord);
      this.panel1.Name = "panel1";
      componentResourceManager.ApplyResources((object) this.btnAdvSearch, "btnAdvSearch");
      this.btnAdvSearch.Name = "btnAdvSearch";
      this.btnAdvSearch.Click += new EventHandler(this.btnAdvSearch_Click);
      componentResourceManager.ApplyResources((object) this.groupBox1, "groupBox1");
      this.groupBox1.Controls.Add((Control) this.chkedtShowOnlyOutstanding);
      this.groupBox1.Controls.Add((Control) this.checkEditUDF);
      this.groupBox1.Controls.Add((Control) this.checkEditDetailUDF);
      this.groupBox1.Controls.Add((Control) this.checkEditSANo);
      this.groupBox1.Controls.Add((Control) this.checkEditDescription);
      this.groupBox1.Controls.Add((Control) this.checkEditDocDate);
      this.groupBox1.Controls.Add((Control) this.checkEditRefDocNo);
      this.groupBox1.Name = "groupBox1";
      componentResourceManager.ApplyResources((object) this.chkedtShowOnlyOutstanding, "chkedtShowOnlyOutstanding");
      this.chkedtShowOnlyOutstanding.Name = "chkedtShowOnlyOutstanding";
      this.chkedtShowOnlyOutstanding.Properties.AccessibleDescription = componentResourceManager.GetString("chkedtShowOnlyOutstanding.Properties.AccessibleDescription");
      this.chkedtShowOnlyOutstanding.Properties.AccessibleName = componentResourceManager.GetString("chkedtShowOnlyOutstanding.Properties.AccessibleName");
      this.chkedtShowOnlyOutstanding.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkedtShowOnlyOutstanding.Properties.AutoHeight");
      this.chkedtShowOnlyOutstanding.Properties.Caption = componentResourceManager.GetString("chkedtShowOnlyOutstanding.Properties.Caption");
      this.chkedtShowOnlyOutstanding.Properties.DisplayValueChecked = componentResourceManager.GetString("chkedtShowOnlyOutstanding.Properties.DisplayValueChecked");
      this.chkedtShowOnlyOutstanding.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkedtShowOnlyOutstanding.Properties.DisplayValueGrayed");
      this.chkedtShowOnlyOutstanding.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkedtShowOnlyOutstanding.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.checkEditUDF, "checkEditUDF");
      this.checkEditUDF.Name = "checkEditUDF";
      this.checkEditUDF.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditUDF.Properties.AccessibleDescription");
      this.checkEditUDF.Properties.AccessibleName = componentResourceManager.GetString("checkEditUDF.Properties.AccessibleName");
      this.checkEditUDF.Properties.AutoHeight = (bool) componentResourceManager.GetObject("checkEditUDF.Properties.AutoHeight");
      this.checkEditUDF.Properties.Caption = componentResourceManager.GetString("checkEditUDF.Properties.Caption");
      this.checkEditUDF.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditUDF.Properties.DisplayValueChecked");
      this.checkEditUDF.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditUDF.Properties.DisplayValueGrayed");
      this.checkEditUDF.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditUDF.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.checkEditDetailUDF, "checkEditDetailUDF");
      this.checkEditDetailUDF.Name = "checkEditDetailUDF";
      this.checkEditDetailUDF.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditDetailUDF.Properties.AccessibleDescription");
      this.checkEditDetailUDF.Properties.AccessibleName = componentResourceManager.GetString("checkEditDetailUDF.Properties.AccessibleName");
      this.checkEditDetailUDF.Properties.AutoHeight = (bool) componentResourceManager.GetObject("checkEditDetailUDF.Properties.AutoHeight");
      this.checkEditDetailUDF.Properties.Caption = componentResourceManager.GetString("checkEditDetailUDF.Properties.Caption");
      this.checkEditDetailUDF.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditDetailUDF.Properties.DisplayValueChecked");
      this.checkEditDetailUDF.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditDetailUDF.Properties.DisplayValueGrayed");
      this.checkEditDetailUDF.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditDetailUDF.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.checkEditSANo, "checkEditSANo");
      this.checkEditSANo.Name = "checkEditSANo";
      this.checkEditSANo.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditSANo.Properties.AccessibleDescription");
      this.checkEditSANo.Properties.AccessibleName = componentResourceManager.GetString("checkEditSANo.Properties.AccessibleName");
      this.checkEditSANo.Properties.AutoHeight = (bool) componentResourceManager.GetObject("checkEditSANo.Properties.AutoHeight");
      this.checkEditSANo.Properties.Caption = componentResourceManager.GetString("checkEditSANo.Properties.Caption");
      this.checkEditSANo.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditSANo.Properties.DisplayValueChecked");
      this.checkEditSANo.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditSANo.Properties.DisplayValueGrayed");
      this.checkEditSANo.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditSANo.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.checkEditDescription, "checkEditDescription");
      this.checkEditDescription.Name = "checkEditDescription";
      this.checkEditDescription.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditDescription.Properties.AccessibleDescription");
      this.checkEditDescription.Properties.AccessibleName = componentResourceManager.GetString("checkEditDescription.Properties.AccessibleName");
      this.checkEditDescription.Properties.AutoHeight = (bool) componentResourceManager.GetObject("checkEditDescription.Properties.AutoHeight");
      this.checkEditDescription.Properties.Caption = componentResourceManager.GetString("checkEditDescription.Properties.Caption");
      this.checkEditDescription.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditDescription.Properties.DisplayValueChecked");
      this.checkEditDescription.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditDescription.Properties.DisplayValueGrayed");
      this.checkEditDescription.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditDescription.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.checkEditDocDate, "checkEditDocDate");
      this.checkEditDocDate.Name = "checkEditDocDate";
      this.checkEditDocDate.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditDocDate.Properties.AccessibleDescription");
      this.checkEditDocDate.Properties.AccessibleName = componentResourceManager.GetString("checkEditDocDate.Properties.AccessibleName");
      this.checkEditDocDate.Properties.AutoHeight = (bool) componentResourceManager.GetObject("checkEditDocDate.Properties.AutoHeight");
      this.checkEditDocDate.Properties.Caption = componentResourceManager.GetString("checkEditDocDate.Properties.Caption");
      this.checkEditDocDate.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditDocDate.Properties.DisplayValueChecked");
      this.checkEditDocDate.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditDocDate.Properties.DisplayValueGrayed");
      this.checkEditDocDate.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditDocDate.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.checkEditRefDocNo, "checkEditRefDocNo");
      this.checkEditRefDocNo.Name = "checkEditRefDocNo";
      this.checkEditRefDocNo.Properties.AccessibleDescription = componentResourceManager.GetString("checkEditRefDocNo.Properties.AccessibleDescription");
      this.checkEditRefDocNo.Properties.AccessibleName = componentResourceManager.GetString("checkEditRefDocNo.Properties.AccessibleName");
      this.checkEditRefDocNo.Properties.AutoHeight = (bool) componentResourceManager.GetObject("checkEditRefDocNo.Properties.AutoHeight");
      this.checkEditRefDocNo.Properties.Caption = componentResourceManager.GetString("checkEditRefDocNo.Properties.Caption");
      this.checkEditRefDocNo.Properties.DisplayValueChecked = componentResourceManager.GetString("checkEditRefDocNo.Properties.DisplayValueChecked");
      this.checkEditRefDocNo.Properties.DisplayValueGrayed = componentResourceManager.GetString("checkEditRefDocNo.Properties.DisplayValueGrayed");
      this.checkEditRefDocNo.Properties.DisplayValueUnchecked = componentResourceManager.GetString("checkEditRefDocNo.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.sbtnClear, "sbtnClear");
      this.sbtnClear.Name = "sbtnClear";
      this.sbtnClear.Click += new EventHandler(this.sbtnClear_Click);
      componentResourceManager.ApplyResources((object) this.sbtnSearch, "sbtnSearch");
      this.sbtnSearch.Name = "sbtnSearch";
      this.sbtnSearch.Click += new EventHandler(this.sbtnSearch_Click);
      componentResourceManager.ApplyResources((object) this.textEditKeyWord, "textEditKeyWord");
      this.textEditKeyWord.Name = "textEditKeyWord";
      this.textEditKeyWord.Properties.AccessibleDescription = componentResourceManager.GetString("textEditKeyWord.Properties.AccessibleDescription");
      this.textEditKeyWord.Properties.AccessibleName = componentResourceManager.GetString("textEditKeyWord.Properties.AccessibleName");
      this.textEditKeyWord.Properties.AutoHeight = (bool) componentResourceManager.GetObject("textEditKeyWord.Properties.AutoHeight");
      this.textEditKeyWord.Properties.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.AutoComplete");
      this.textEditKeyWord.Properties.Mask.BeepOnError = (bool) componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.BeepOnError");
      this.textEditKeyWord.Properties.Mask.EditMask = componentResourceManager.GetString("textEditKeyWord.Properties.Mask.EditMask");
      this.textEditKeyWord.Properties.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.IgnoreMaskBlank");
      this.textEditKeyWord.Properties.Mask.MaskType = (DevExpress.XtraEditors.Mask.MaskType) componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.MaskType");
      this.textEditKeyWord.Properties.Mask.PlaceHolder = (char) componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.PlaceHolder");
      this.textEditKeyWord.Properties.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.SaveLiteral");
      this.textEditKeyWord.Properties.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.ShowPlaceHolders");
      this.textEditKeyWord.Properties.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("textEditKeyWord.Properties.Mask.UseMaskAsDisplayFormat");
      this.textEditKeyWord.Properties.NullValuePrompt = componentResourceManager.GetString("textEditKeyWord.Properties.NullValuePrompt");
      this.textEditKeyWord.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("textEditKeyWord.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.labelKeyWord, "labelKeyWord");
      this.labelKeyWord.Name = "labelKeyWord";
      componentResourceManager.ApplyResources((object) this.panel3, "panel3");
      this.panel3.BorderStyle = BorderStyles.NoBorder;
      this.panel3.Controls.Add((Control) this.pnAOProcessing);
      this.panel3.Controls.Add((Control) this.pnEdit);
      this.panel3.Controls.Add((Control) this.pnClose);
      this.panel3.Name = "panel3";
      componentResourceManager.ApplyResources((object) this.pnAOProcessing, "pnAOProcessing");
      this.pnAOProcessing.BorderStyle = BorderStyles.NoBorder;
      this.pnAOProcessing.Controls.Add((Control) this.sbtnAOProcessing);
      this.pnAOProcessing.Name = "pnAOProcessing";
      componentResourceManager.ApplyResources((object) this.sbtnAOProcessing, "sbtnAOProcessing");
      this.sbtnAOProcessing.Name = "sbtnAOProcessing";
      this.sbtnAOProcessing.Click += new EventHandler(this.sbtnAOProcessing_Click);
      componentResourceManager.ApplyResources((object) this.pnEdit, "pnEdit");
      this.pnEdit.BorderStyle = BorderStyles.NoBorder;
      this.pnEdit.Controls.Add((Control) this.sbtnDelete);
      this.pnEdit.Controls.Add((Control) this.sbtnView);
      this.pnEdit.Controls.Add((Control) this.sbtnEdit);
      this.pnEdit.Name = "pnEdit";
      componentResourceManager.ApplyResources((object) this.sbtnDelete, "sbtnDelete");
      this.sbtnDelete.Appearance.ForeColor = (Color) componentResourceManager.GetObject("sbtnDelete.Appearance.ForeColor");
      this.sbtnDelete.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("sbtnDelete.Appearance.GradientMode");
      this.sbtnDelete.Appearance.Image = (Image) componentResourceManager.GetObject("sbtnDelete.Appearance.Image");
      this.sbtnDelete.Appearance.Options.UseForeColor = true;
      this.sbtnDelete.Name = "sbtnDelete";
      this.sbtnDelete.Click += new EventHandler(this.sbtnDelete_Click);
      componentResourceManager.ApplyResources((object) this.sbtnView, "sbtnView");
      this.sbtnView.Name = "sbtnView";
      this.sbtnView.Click += new EventHandler(this.sbtnView_Click);
      componentResourceManager.ApplyResources((object) this.sbtnEdit, "sbtnEdit");
      this.sbtnEdit.Name = "sbtnEdit";
      this.sbtnEdit.Click += new EventHandler(this.sbtnEdit_Click);
      componentResourceManager.ApplyResources((object) this.pnClose, "pnClose");
      this.pnClose.BorderStyle = BorderStyles.NoBorder;
      this.pnClose.Controls.Add((Control) this.sbtnClose);
      this.pnClose.Name = "pnClose";
      componentResourceManager.ApplyResources((object) this.sbtnClose, "sbtnClose");
      this.sbtnClose.DialogResult = DialogResult.Cancel;
      this.sbtnClose.Name = "sbtnClose";
      this.sbtnClose.Click += new EventHandler(this.sbtnClose_Click);
      componentResourceManager.ApplyResources((object) this.ucSearchResult1, "ucSearchResult1");
      this.ucSearchResult1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor");
      this.ucSearchResult1.Appearance.BackColor2 = (Color) componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor2");
      this.ucSearchResult1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucSearchResult1.Appearance.GradientMode");
      this.ucSearchResult1.Appearance.Image = (Image) componentResourceManager.GetObject("ucSearchResult1.Appearance.Image");
      this.ucSearchResult1.Appearance.Options.UseBackColor = true;
      this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
      this.ucSearchResult1.Name = "ucSearchResult1";
      componentResourceManager.ApplyResources((object) this.myStockAssemblyGridControl, "myStockAssemblyGridControl");
      this.myStockAssemblyGridControl.EmbeddedNavigator.AccessibleDescription = componentResourceManager.GetString("myStockAssemblyGridControl.EmbeddedNavigator.AccessibleDescription");
      this.myStockAssemblyGridControl.EmbeddedNavigator.AccessibleName = componentResourceManager.GetString("myStockAssemblyGridControl.EmbeddedNavigator.AccessibleName");
      this.myStockAssemblyGridControl.EmbeddedNavigator.AllowHtmlTextInToolTip = (DefaultBoolean) componentResourceManager.GetObject("myStockAssemblyGridControl.EmbeddedNavigator.AllowHtmlTextInToolTip");
      this.myStockAssemblyGridControl.EmbeddedNavigator.Anchor = (AnchorStyles) componentResourceManager.GetObject("myStockAssemblyGridControl.EmbeddedNavigator.Anchor");
      this.myStockAssemblyGridControl.EmbeddedNavigator.BackgroundImage = (Image) componentResourceManager.GetObject("myStockAssemblyGridControl.EmbeddedNavigator.BackgroundImage");
      this.myStockAssemblyGridControl.EmbeddedNavigator.BackgroundImageLayout = (ImageLayout) componentResourceManager.GetObject("myStockAssemblyGridControl.EmbeddedNavigator.BackgroundImageLayout");
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Append.Hint = componentResourceManager.GetString("myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Append.Hint");
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.CancelEdit.Hint = componentResourceManager.GetString("myStockAssemblyGridControl.EmbeddedNavigator.Buttons.CancelEdit.Hint");
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Edit.Hint = componentResourceManager.GetString("myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Edit.Hint");
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.EndEdit.Hint = componentResourceManager.GetString("myStockAssemblyGridControl.EmbeddedNavigator.Buttons.EndEdit.Hint");
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Remove.Hint = componentResourceManager.GetString("myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Remove.Hint");
      this.myStockAssemblyGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
      this.myStockAssemblyGridControl.EmbeddedNavigator.ImeMode = (ImeMode) componentResourceManager.GetObject("myStockAssemblyGridControl.EmbeddedNavigator.ImeMode");
      this.myStockAssemblyGridControl.EmbeddedNavigator.MaximumSize = (Size) componentResourceManager.GetObject("myStockAssemblyGridControl.EmbeddedNavigator.MaximumSize");
      this.myStockAssemblyGridControl.EmbeddedNavigator.TextLocation = (NavigatorButtonsTextLocation) componentResourceManager.GetObject("myStockAssemblyGridControl.EmbeddedNavigator.TextLocation");
      this.myStockAssemblyGridControl.EmbeddedNavigator.ToolTip = componentResourceManager.GetString("myStockAssemblyGridControl.EmbeddedNavigator.ToolTip");
      this.myStockAssemblyGridControl.EmbeddedNavigator.ToolTipIconType = (ToolTipIconType) componentResourceManager.GetObject("myStockAssemblyGridControl.EmbeddedNavigator.ToolTipIconType");
      this.myStockAssemblyGridControl.EmbeddedNavigator.ToolTipTitle = componentResourceManager.GetString("myStockAssemblyGridControl.EmbeddedNavigator.ToolTipTitle");
      this.myStockAssemblyGridControl.MainView = (BaseView) this.gridViewStockAssembly;
      this.myStockAssemblyGridControl.Name = "myStockAssemblyGridControl";
      RepositoryItemCollection repositoryItems = this.myStockAssemblyGridControl.RepositoryItems;
      RepositoryItem[] items1 = new RepositoryItem[2];
      int index1 = 0;
      RepositoryItemCheckEdit repositoryItemCheckEdit = this.repositoryItemCheckEditDelete;
      items1[index1] = (RepositoryItem) repositoryItemCheckEdit;
      int index2 = 1;
      RepositoryItemTextEdit repositoryItemTextEdit = this.repositoryItemTextEdit1;
      items1[index2] = (RepositoryItem) repositoryItemTextEdit;
      repositoryItems.AddRange(items1);
      this.myStockAssemblyGridControl.UseEmbeddedNavigator = true;
      ViewRepositoryCollection viewCollection = this.myStockAssemblyGridControl.ViewCollection;
      BaseView[] views = new BaseView[1];
      int index3 = 0;
      GridView gridView = this.gridViewStockAssembly;
      views[index3] = (BaseView) gridView;
      viewCollection.AddRange(views);
      componentResourceManager.ApplyResources((object) this.gridViewStockAssembly, "gridViewStockAssembly");
      GridColumnCollection columns1 = this.gridViewStockAssembly.Columns;
      GridColumn[] columns2 = new GridColumn[24];
      int index4 = 0;
      GridColumn gridColumn1 = this.colTick;
      columns2[index4] = gridColumn1;
      int index5 = 1;
      GridColumn gridColumn2 = this.colDocNo;
      columns2[index5] = gridColumn2;
      int index6 = 2;
      GridColumn gridColumn3 = this.colDocDate;
      columns2[index6] = gridColumn3;
      int index7 = 3;
      GridColumn gridColumn4 = this.colDescription;
      columns2[index7] = gridColumn4;
      int index8 = 4;
      GridColumn gridColumn5 = this.colItemCode;
      columns2[index8] = gridColumn5;
      int index9 = 5;
      GridColumn gridColumn6 = this.colLocation;
      columns2[index9] = gridColumn6;
      int index10 = 6;
      GridColumn gridColumn7 = this.colBatchNo;
      columns2[index10] = gridColumn7;
      int index11 = 7;
      GridColumn gridColumn8 = this.colProjNo;
      columns2[index11] = gridColumn8;
      int index12 = 8;
      GridColumn gridColumn9 = this.colDeptNo;
      columns2[index12] = gridColumn9;
      int index13 = 9;
      GridColumn gridColumn10 = this.colQty;
      columns2[index13] = gridColumn10;
      int index14 = 10;
      GridColumn gridColumn11 = this.colTotal;
      columns2[index14] = gridColumn11;
      int index15 = 11;
      GridColumn gridColumn12 = this.colAssemblyCost;
      columns2[index15] = gridColumn12;
      int index16 = 12;
      GridColumn gridColumn13 = this.colNetTotal;
      columns2[index16] = gridColumn13;
      int index17 = 13;
      GridColumn gridColumn14 = this.colRemark1;
      columns2[index17] = gridColumn14;
      int index18 = 14;
      GridColumn gridColumn15 = this.colRemark2;
      columns2[index18] = gridColumn15;
      int index19 = 15;
      GridColumn gridColumn16 = this.colRemark3;
      columns2[index19] = gridColumn16;
      int index20 = 16;
      GridColumn gridColumn17 = this.colRemark4;
      columns2[index20] = gridColumn17;
      int index21 = 17;
      GridColumn gridColumn18 = this.colPrintCount;
      columns2[index21] = gridColumn18;
      int index22 = 18;
      GridColumn gridColumn19 = this.colCancelled;
      columns2[index22] = gridColumn19;
      int index23 = 19;
      GridColumn gridColumn20 = this.colLastModified;
      columns2[index23] = gridColumn20;
      int index24 = 20;
      GridColumn gridColumn21 = this.colLastModifiedUserID;
      columns2[index24] = gridColumn21;
      int index25 = 21;
      GridColumn gridColumn22 = this.colCreatedTimeStamp;
      columns2[index25] = gridColumn22;
      int index26 = 22;
      GridColumn gridColumn23 = this.colCreatedUserID;
      columns2[index26] = gridColumn23;
      int index27 = 23;
      GridColumn gridColumn24 = this.colRefDocNo;
      columns2[index27] = gridColumn24;
      columns1.AddRange(columns2);
      this.gridViewStockAssembly.GridControl = this.myStockAssemblyGridControl;
      this.gridViewStockAssembly.Name = "gridViewStockAssembly";
      this.gridViewStockAssembly.OptionsBehavior.AllowIncrementalSearch = true;
      this.gridViewStockAssembly.OptionsSelection.MultiSelect = true;
      this.gridViewStockAssembly.OptionsView.ShowFooter = true;
      this.gridViewStockAssembly.SelectionChanged += new SelectionChangedEventHandler(this.gridViewStockAssembly_SelectionChanged);
      this.gridViewStockAssembly.Layout += new EventHandler(this.gridViewStockAssembly_Layout);
      componentResourceManager.ApplyResources((object) this.colTick, "colTick");
      this.colTick.ColumnEdit = (RepositoryItem) this.repositoryItemCheckEditDelete;
      this.colTick.FieldName = "Tick";
      this.colTick.Name = "colTick";
      componentResourceManager.ApplyResources((object) this.repositoryItemCheckEditDelete, "repositoryItemCheckEditDelete");
      this.repositoryItemCheckEditDelete.Name = "repositoryItemCheckEditDelete";
      componentResourceManager.ApplyResources((object) this.colDocNo, "colDocNo");
      this.colDocNo.FieldName = "DocNo";
      this.colDocNo.Name = "colDocNo";
      this.colDocNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDocDate, "colDocDate");
      this.colDocDate.FieldName = "DocDate";
      this.colDocDate.Name = "colDocDate";
      this.colDocDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDescription, "colDescription");
      this.colDescription.FieldName = "Description";
      this.colDescription.Name = "colDescription";
      this.colDescription.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colItemCode, "colItemCode");
      this.colItemCode.FieldName = "ItemCode";
      this.colItemCode.Name = "colItemCode";
      this.colItemCode.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLocation, "colLocation");
      this.colLocation.FieldName = "Location";
      this.colLocation.Name = "colLocation";
      this.colLocation.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colBatchNo, "colBatchNo");
      this.colBatchNo.FieldName = "BatchNo";
      this.colBatchNo.Name = "colBatchNo";
      this.colBatchNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colProjNo, "colProjNo");
      this.colProjNo.FieldName = "ProjNo";
      this.colProjNo.Name = "colProjNo";
      this.colProjNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDeptNo, "colDeptNo");
      this.colDeptNo.FieldName = "DeptNo";
      this.colDeptNo.Name = "colDeptNo";
      this.colDeptNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colQty, "colQty");
      this.colQty.FieldName = "Qty";
      this.colQty.Name = "colQty";
      this.colQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colTotal, "colTotal");
      this.colTotal.FieldName = "Total";
      this.colTotal.Name = "colTotal";
      this.colTotal.OptionsColumn.AllowEdit = false;
      GridColumnSummaryItemCollection summary1 = this.colTotal.Summary;
      GridSummaryItem[] items2 = new GridSummaryItem[1];
      int index28 = 0;
      GridColumnSummaryItem columnSummaryItem1 = new GridColumnSummaryItem((SummaryItemType) componentResourceManager.GetObject("colTotal.Summary"));
      items2[index28] = (GridSummaryItem) columnSummaryItem1;
      summary1.AddRange(items2);
      componentResourceManager.ApplyResources((object) this.colAssemblyCost, "colAssemblyCost");
      this.colAssemblyCost.FieldName = "AssemblyCost";
      this.colAssemblyCost.Name = "colAssemblyCost";
      this.colAssemblyCost.OptionsColumn.AllowEdit = false;
      GridColumnSummaryItemCollection summary2 = this.colAssemblyCost.Summary;
      GridSummaryItem[] items3 = new GridSummaryItem[1];
      int index29 = 0;
      GridColumnSummaryItem columnSummaryItem2 = new GridColumnSummaryItem((SummaryItemType) componentResourceManager.GetObject("colAssemblyCost.Summary"));
      items3[index29] = (GridSummaryItem) columnSummaryItem2;
      summary2.AddRange(items3);
      componentResourceManager.ApplyResources((object) this.colNetTotal, "colNetTotal");
      this.colNetTotal.FieldName = "NetTotal";
      this.colNetTotal.Name = "colNetTotal";
      this.colNetTotal.OptionsColumn.AllowEdit = false;
      GridColumnSummaryItemCollection summary3 = this.colNetTotal.Summary;
      GridSummaryItem[] items4 = new GridSummaryItem[1];
      int index30 = 0;
      GridColumnSummaryItem columnSummaryItem3 = new GridColumnSummaryItem((SummaryItemType) componentResourceManager.GetObject("colNetTotal.Summary"));
      items4[index30] = (GridSummaryItem) columnSummaryItem3;
      summary3.AddRange(items4);
      componentResourceManager.ApplyResources((object) this.colRemark1, "colRemark1");
      this.colRemark1.FieldName = "Remark1";
      this.colRemark1.Name = "colRemark1";
      this.colRemark1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark2, "colRemark2");
      this.colRemark2.FieldName = "Remark2";
      this.colRemark2.Name = "colRemark2";
      this.colRemark2.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark3, "colRemark3");
      this.colRemark3.FieldName = "Remark3";
      this.colRemark3.Name = "colRemark3";
      this.colRemark3.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark4, "colRemark4");
      this.colRemark4.FieldName = "Remark4";
      this.colRemark4.Name = "colRemark4";
      this.colRemark4.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colPrintCount, "colPrintCount");
      this.colPrintCount.FieldName = "PrintCount";
      this.colPrintCount.Name = "colPrintCount";
      this.colPrintCount.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCancelled, "colCancelled");
      this.colCancelled.ColumnEdit = (RepositoryItem) this.repositoryItemTextEdit1;
      this.colCancelled.FieldName = "Cancelled";
      this.colCancelled.Name = "colCancelled";
      this.colCancelled.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.repositoryItemTextEdit1, "repositoryItemTextEdit1");
      this.repositoryItemTextEdit1.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.AutoComplete");
      this.repositoryItemTextEdit1.Mask.BeepOnError = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.BeepOnError");
      this.repositoryItemTextEdit1.Mask.EditMask = componentResourceManager.GetString("repositoryItemTextEdit1.Mask.EditMask");
      this.repositoryItemTextEdit1.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.IgnoreMaskBlank");
      this.repositoryItemTextEdit1.Mask.MaskType = (DevExpress.XtraEditors.Mask.MaskType) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.MaskType");
      this.repositoryItemTextEdit1.Mask.PlaceHolder = (char) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.PlaceHolder");
      this.repositoryItemTextEdit1.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.SaveLiteral");
      this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.ShowPlaceHolders");
      this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat");
      this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
      this.repositoryItemTextEdit1.FormatEditValue += new ConvertEditValueEventHandler(this.repositoryItemTextEdit1_FormatEditValue);
      componentResourceManager.ApplyResources((object) this.colLastModified, "colLastModified");
      this.colLastModified.FieldName = "LastModified";
      this.colLastModified.Name = "colLastModified";
      this.colLastModified.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLastModifiedUserID, "colLastModifiedUserID");
      this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
      this.colLastModifiedUserID.Name = "colLastModifiedUserID";
      this.colLastModifiedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedTimeStamp, "colCreatedTimeStamp");
      this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
      this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
      this.colCreatedTimeStamp.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedUserID, "colCreatedUserID");
      this.colCreatedUserID.FieldName = "CreatedUserID";
      this.colCreatedUserID.Name = "colCreatedUserID";
      this.colCreatedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRefDocNo, "colRefDocNo");
      this.colRefDocNo.FieldName = "RefDocNo";
      this.colRefDocNo.Name = "colRefDocNo";
      this.colRefDocNo.OptionsColumn.AllowEdit = false;
      this.AcceptButton = (IButtonControl) this.sbtnSearch;
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.Controls.Add((Control) this.myStockAssemblyGridControl);
      this.Controls.Add((Control) this.ucSearchResult1);
      this.Controls.Add((Control) this.panel3);
      this.Controls.Add((Control) this.panel1);
      this.KeyPreview = true;
      this.Name = "FormStockAssemblyFind";
      this.ShowInTaskbar = false;
      this.Activated += new EventHandler(this.FormStockAssemblySearch_Activated);
      this.Load += new EventHandler(this.FormStockAssemblyFind_Load);
      this.KeyUp += new KeyEventHandler(this.FormStockAssemblyFind_KeyUp);
      this.panel1.EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.groupBox1.EndInit();
      this.groupBox1.ResumeLayout(false);
      this.chkedtShowOnlyOutstanding.Properties.EndInit();
      this.checkEditUDF.Properties.EndInit();
      this.checkEditDetailUDF.Properties.EndInit();
      this.checkEditSANo.Properties.EndInit();
      this.checkEditDescription.Properties.EndInit();
      this.checkEditDocDate.Properties.EndInit();
      this.checkEditRefDocNo.Properties.EndInit();
      this.textEditKeyWord.Properties.EndInit();
      this.panel3.EndInit();
      this.panel3.ResumeLayout(false);
      this.pnAOProcessing.EndInit();
      this.pnAOProcessing.ResumeLayout(false);
      this.pnEdit.EndInit();
      this.pnEdit.ResumeLayout(false);
      this.pnClose.EndInit();
      this.pnClose.ResumeLayout(false);
      this.myStockAssemblyGridControl.EndInit();
      this.gridViewStockAssembly.EndInit();
      this.repositoryItemCheckEditDelete.EndInit();
      this.repositoryItemTextEdit1.EndInit();
      this.ResumeLayout(false);
    }
  }
}
