﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Document;
using BCE.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using DevExpress.XtraTreeList;
using System;
using System.Data;

namespace Production.WorkOrder
{
  [Obsolete("Use FormStockAssemblyOrderEntry.FormEventArgs instead")]
  public class FormEventArgs
  {
    private StockWorkOrder myStockAssemblyOrder;
    private PanelControl myHeaderPanel;
    private TreeList myTreeList;
    private XtraTabControl myTabControl;

    public PanelControl HeaderPanel
    {
      get
      {
        return this.myHeaderPanel;
      }
    }

    public TreeList TreeList
    {
      get
      {
        return this.myTreeList;
      }
    }

    public XtraTabControl TabControl
    {
      get
      {
        return this.myTabControl;
      }
    }

    public DataTable MasterTable
    {
      get
      {
        return this.myStockAssemblyOrder.DataTableMaster;
      }
    }

    public DataTable DetailTable
    {
      get
      {
        return this.myStockAssemblyOrder.DataTableDetail;
      }
    }

    public EditWindowMode EditWindowMode
    {
      get
      {
        if (this.myStockAssemblyOrder.Action == StockWorkOrderAction.New)
          return EditWindowMode.New;
        else if (this.myStockAssemblyOrder.Action == StockWorkOrderAction.Edit)
          return EditWindowMode.Edit;
        else
          return EditWindowMode.View;
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myStockAssemblyOrder.Command.DBSetting;
      }
    }

    public FormEventArgs(StockWorkOrder doc, PanelControl headerPanel, TreeList treeList, XtraTabControl tabControl)
    {
      this.myStockAssemblyOrder = doc;
      this.myHeaderPanel = headerPanel;
      this.myTreeList = treeList;
      this.myTabControl = tabControl;
    }
  }
}
