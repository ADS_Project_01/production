﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderNewDetailEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using System.Data;

namespace Production.WorkOrder
{
  public class StockWorkOrderNewDetailEventArgs
  {
    private DataRow myDetailRow;
    private StockWorkOrder myStock;

    public StockWorkOrderRecord MasterRecord
    {
      get
      {
        return new StockWorkOrderRecord(this.myStock);
      }
    }

    public StockWorkOrderDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockWorkOrderDetailRecord(this.myStock.Command.DBSetting, this.myDetailRow);
      }
    }

    internal StockWorkOrderNewDetailEventArgs(StockWorkOrder doc, DataRow detailRow)
    {
      this.myStock = doc;
      this.myDetailRow = detailRow;
    }
  }
}
