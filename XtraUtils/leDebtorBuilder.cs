﻿using System;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Popup;
using BCE.Data;
using BCE.AutoCount.Common;
using System.Globalization;
using System.Threading;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.XtraUtils
{
    class leDebtorBuilder : LookupEditBuilderEx
    {
        string sLanguage;

        public leDebtorBuilder()
            : base()
        {
            this.myUseCaching = true;
            this.myUseChangeCount2 = true;
            this.myLastChangeCount2 = 1;

           // this.myCacheFileName = "RPA_leDebtorBuilder.Data";
        

            myCmdString = "SELECT AccNo,CompanyName,CurrencyCode,Address1 FROM Debtor where IsActive='T'";
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "AccNo";
            lookupEdit.ValueMember = "AccNo";
            lookupEdit.DataSource = myDataTable;
            lookupEdit.Columns.Clear();

            lookupEdit.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccNo", "Code", 100));
            lookupEdit.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CompanyName", "Name", 100));
            lookupEdit.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CurrencyCode", "CurrencyCode", 50));
            lookupEdit.PopupWidth = 250;



        }
    }

}
