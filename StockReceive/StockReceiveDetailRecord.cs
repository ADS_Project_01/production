﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveDetailRecord
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Stock;
using BCE.Data;
using System;
using System.Data;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveDetailRecord : StockDetailRecord
  {
    public DBString SerialNoList
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["SerialNoList"]);
      }
    }

    internal StockReceiveDetailRecord(DBSetting dbSetting, DataRow row)
      : base(dbSetting, row)
    {
    }
  }
}
