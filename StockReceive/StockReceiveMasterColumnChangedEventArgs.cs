﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveMasterColumnChangedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveMasterColumnChangedEventArgs
  {
    private string myColumnName;
    private StockReceive myStock;

    public string ChangedColumnName
    {
      get
      {
        return this.myColumnName;
      }
    }

    public StockReceiveRecord MasterRecord
    {
      get
      {
        return new StockReceiveRecord(this.myStock);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.myDBSetting;
      }
    }

    internal StockReceiveMasterColumnChangedEventArgs(string columnName, StockReceive doc)
    {
      this.myColumnName = columnName;
      this.myStock = doc;
    }
  }
}
