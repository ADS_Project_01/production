﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveDetailListingReportTypeHandler
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount;
using BCE.Data;

namespace Production.StockReceive
{
  public class StockReceiveDetailListingReportTypeHandler : StockReceiveReportTypeHandler
  {
    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      BasicReportOption reportOption = (BasicReportOption) null;
      try
      {
        reportOption = (BasicReportOption) PersistenceUtil.LoadUserSetting("StockReceiveReportOption.setting");
      }
      catch
      {
      }
      if (reportOption == null)
        reportOption = new BasicReportOption();
      return StockReceiveReportCommand.Create(dbSetting, reportOption).GetDetailListingReportDesignerDataSource();
    }
  }
}
