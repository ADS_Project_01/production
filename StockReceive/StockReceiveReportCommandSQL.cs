﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveReportCommandSQL
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount;
using BCE.AutoCount.Stock;
//using BCE.AutoCount.UDF;
using BCE.Data;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Production.StockReceive
{
  public class StockReceiveReportCommandSQL : StockReceiveReportCommand
  {
    protected override DataSet LoadDetailListingReportData(string dtlKeys, StockReceiveDetailReportingCriteria criteria)
    {
      string format = "Select A.*, B.* {0} {1} From vRPA_StockReceive A Inner Join vRPA_StockReceiveDetail B On (A.DocKey = B.DocKey) Where B.DtlKey In (SELECT * FROM LIST(@DtlKeyList))";
      string str1 = "";
      string str2 = "";
      if (criteria.SortBy == DetailListingSortByOption.DocumentNo)
        str1 = ",A.DocNo AS SortID";
      else if (criteria.SortBy == DetailListingSortByOption.Date)
        str1 = ", (cast(year(A.DocDate) as varchar(4)) + case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end + case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end )AS SortID ";
      else if (criteria.SortBy == DetailListingSortByOption.ItemCode)
        str1 = ",B.ItemCode AS SortID";
      else if (criteria.SortBy == DetailListingSortByOption.ItemGroup)
        str1 = ",B.ItemGroup AS SortID";
      else if (criteria.SortBy == DetailListingSortByOption.ItemType)
        str1 = ",B.ItemType AS SortID";
      else if (criteria.SortBy == DetailListingSortByOption.Location)
        str1 = ",B.Location AS SortID";
      else if (criteria.SortBy == DetailListingSortByOption.ProjectNo)
        str1 = ",B.ProjNo AS SortID";
      else if (criteria.SortBy == DetailListingSortByOption.DepartmentNo)
        str1 = ",B.DeptNo AS SortID";
      if (criteria.GroupBy == DetailListingGroupByOption.Date)
        str2 = ",(cast(year(A.DocDate) as varchar(4)) +'/'+ case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end +'/'+ case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end ) AS GroupID, 'Date' AS GroupIDName, (case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end  + case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end + cast(year(A.DocDate) as varchar(4))) AS GroupIDDisplay, 'Group By ' + (case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end +'/'+ case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end +'/'+ cast(year(A.DocDate) as varchar(4))) As GroupIDDescription ";
      else if (criteria.GroupBy == DetailListingGroupByOption.Month)
        str2 = ",cast(year(A.DocDate) as varchar(4)) + (case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2))end ) AS GroupID, 'Month' AS GroupIDName, DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4)) AS GroupIDDisplay, 'Group By ' + DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4)) AS GroupIDDescription ";
      else if (criteria.GroupBy == DetailListingGroupByOption.Year)
        str2 = ",cast(year(A.DocDate) as varchar(4)) AS GroupID, 'Year' AS GroupIDName, cast(year(A.DocDate) as varchar(4)) AS GroupIDDisplay, 'Group By ' + cast(year(A.DocDate) AS varchar(4)) AS GroupIDDescription ";
      else if (criteria.GroupBy == DetailListingGroupByOption.ItemCode)
        str2 = ",B.ItemCode AS GroupID, 'Item Code' AS GroupIDName, B.ItemCode AS GroupIDDisplay, B.ItemDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == DetailListingGroupByOption.ItemGroup)
        str2 = ",B.ItemGroup AS GroupID, 'Item Group' AS GroupIDName, B.ItemGroup AS GroupIDDisplay, B.ItemGroupDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == DetailListingGroupByOption.ItemType)
        str2 = ",B.ItemType AS GroupID, 'Item Type' AS GroupIDName, B.ItemType AS GroupIDDisplay, B.ItemTypeDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == DetailListingGroupByOption.Location)
        str2 = ",B.Location AS GroupID, 'Location' AS GroupIDName, B.Location AS GroupIDDisplay, B.LocationDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == DetailListingGroupByOption.ProjectNo)
        str2 = ",B.ProjNo AS GroupID, 'Project No' AS GroupIDName, B.ProjNo AS GroupIDDisplay, B.ProjectDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == DetailListingGroupByOption.DepartmentNo)
        str2 = ",B.DeptNo AS GroupID, 'Department No' AS GroupIDName, B.DeptNo AS GroupIDDisplay, B.DeptDescription AS GroupIDDescription ";
      string str3 = string.Format(format, (object) str1, (object) str2);
      DataSet dataSet = new DataSet();
      DBSetting dbSetting = this.myDBSetting;
      DataSet ds = dataSet;
      string tableName = "Master";
      string cmdText = str3;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DtlKeyList", (object) dtlKeys);
      objArray[index] = (object) sqlParameter;
      dbSetting.LoadDataSet(ds, tableName, cmdText, num != 0, objArray);
      return dataSet;
    }

    protected override DataSet LoadDetailListingReportDesignerData()
    {
      string cmdText = string.Format("Select TOP 100 A.*, B.* {0} {1} from vRPA_StockReceive A inner join vRPA_StockReceiveDetail B on (A.DocKey = B.DocKey)", (object) ", (cast(year(A.DocDate) as varchar(4)) + case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end + case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end )AS SortID ", (object) ",B.ItemCode AS GroupID, 'Item Code' AS GroupIDName, B.ItemCode AS GroupIDDisplay, B.ItemDescription AS GroupIDDescription ");
      DataSet ds = new DataSet();
      this.myDBSetting.LoadDataSet(ds, "Master", cmdText, false, new object[0]);
      return ds;
    }

    public override void DetailListingAdvanceSearch(AdvancedStockReceiveCriteria criteria, DataSet newDS, string checkEditColumnName)
    {
      if (newDS.Relations.Contains("MasterDetailRelation") && newDS.Relations.CanRemove(newDS.Relations["MasterDetailRelation"]))
        newDS.Relations.Clear();
      DataTable dataTable1 = newDS.Tables["Master"];
      DataTable dataTable2 = newDS.Tables["Detail"];
      string str1 = "";
      //UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
      //foreach (UDFColumn udfColumn in udfUtil.GetUDF("RPA_RCV"))
      //{
      //  string[] strArray = new string[6];
      //  int index1 = 0;
      //  string str2 = str1;
      //  strArray[index1] = str2;
      //  int index2 = 1;
      //  string str3 = " , A.";
      //  strArray[index2] = str3;
      //  int index3 = 2;
      //  string fieldName1 = udfColumn.FieldName;
      //  strArray[index3] = fieldName1;
      //  int index4 = 3;
      //  string str4 = " AS RPA_RCV";
      //  strArray[index4] = str4;
      //  int index5 = 4;
      //  string fieldName2 = udfColumn.FieldName;
      //  strArray[index5] = fieldName2;
      //  int index6 = 5;
      //  string str5 = " ";
      //  strArray[index6] = str5;
      //  str1 = string.Concat(strArray);
      //}
      //foreach (UDFColumn udfColumn in udfUtil.GetUDF("RPA_RCVDtl"))
      //{
      //  string[] strArray = new string[6];
      //  int index1 = 0;
      //  string str2 = str1;
      //  strArray[index1] = str2;
      //  int index2 = 1;
      //  string str3 = " , B.";
      //  strArray[index2] = str3;
      //  int index3 = 2;
      //  string fieldName1 = udfColumn.FieldName;
      //  strArray[index3] = fieldName1;
      //  int index4 = 3;
      //  string str4 = " AS RPA_RCVDtl";
      //  strArray[index4] = str4;
      //  int index5 = 4;
      //  string fieldName2 = udfColumn.FieldName;
      //  strArray[index5] = fieldName2;
      //  int index6 = 5;
      //  string str5 = " ";
      //  strArray[index6] = str5;
      //  str1 = string.Concat(strArray);
      //}
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      string format = " Select A.*, B.Description AS DtlDescription, B.*, D.ItemType, D.ItemGroup, Project.Description AS ProjDesc, Dept.Description AS DeptDesc " + str1 + " From RPA_RCV A  Inner Join RPA_RCVDtl B on A.DocKey = B.DocKey  Left Outer Join Item D On (B.ItemCode = D.ItemCode)  LEFT OUTER JOIN Project On (B.ProjNo = Project.ProjNo)  LEFT OUTER JOIN Dept On (B.DeptNo = Dept.DeptNo)  Where {0} ";
      try
      {
        SqlCommand selectCommand = new SqlCommand();
        selectCommand.Connection = sqlConnection;
        string str2 = criteria.BuildSQL((IDbCommand) selectCommand);
        if (str2 == string.Empty)
          str2 = "(1=1)";
        string str3 = string.Format(format, (object) str2);
        selectCommand.CommandText = str3;
        string str4 = SQLHelper.BuildFilteredByUserSQL(this.myDBSetting, "A");
        if (str4.Length > 0)
        {
          string str5 = str3 + " AND " + str4;
        }
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
        {
          newDS.Tables["Detail"].Clear();
          newDS.Tables["Master"].Clear();
        }
        else if (checkEditColumnName.Length > 0 && dataTable1.Rows.Count > 0)
        {
          DataRow[] dataRowArray = dataTable1.Select(string.Format("{0} = true", (object) checkEditColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index = 0; index < dataRowArray.Length; ++index)
            numArray[index] = BCE.Data.Convert.ToInt64(dataRowArray[index]["DtlKey"]);
        }
        sqlDataAdapter.Fill(dataTable1);
        if (dataTable1.PrimaryKey.Length == 0)
        {
          DataTable dataTable3 = dataTable1;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index = 0;
          DataColumn dataColumn = dataTable1.Columns["DtlKey"];
          dataColumnArray[index] = dataColumn;
          dataTable3.PrimaryKey = dataColumnArray;
        }
        if (numArray != null)
        {
          foreach (long num in numArray)
          {
            DataRow dataRow = dataTable1.Rows.Find((object) num);
            if (dataRow != null)
              dataRow[checkEditColumnName] = (object) true;
          }
        }
      }
      catch (Exception ex)
      {
        if (ex is SqlException)
          BCE.Data.DataError.HandleSqlException((SqlException) ex);
        if (!(ex is ConstraintException))
          throw;
      }
      finally
      {
        if (sqlConnection != null)
        {
          sqlConnection.Close();
          sqlConnection.Dispose();
        }
      }
    }

    public override void DetailListingBasicSearch(StockReceiveDetailReportingCriteria criteria, string columnName, DataSet newDS, string chkEditColumnName)
    {
      if (newDS.Relations.Contains("MasterDetailRelation") && newDS.Relations.CanRemove(newDS.Relations["MasterDetailRelation"]))
        newDS.Relations.Clear();
      DataTable dataTable1 = newDS.Tables["Master"];
      DataTable dataTable2 = newDS.Tables["Detail"];
      string format = " SELECT {0}, RPA_RCVDtl.DtlKey, D.ItemGroup, D.ItemType FROM RPA_RCV  INNER JOIN RPA_RCVDtl ON RPA_RCV.DocKey = RPA_RCVDtl.DocKey  LEFT OUTER JOIN Item D ON (RPA_RCVDtl.ItemCode = D.ItemCode)  LEFT OUTER JOIN Project On (RPA_RCVDtl.ProjNo = Project.ProjNo)  LEFT OUTER JOIN Dept On (RPA_RCVDtl.DeptNo = Dept.DeptNo)  WHERE {1}";
      string str1 = columnName;
      char[] chArray = new char[1];
      int index1 = 0;
      int num1 = 44;
      chArray[index1] = (char) num1;
      string[] strArray1 = str1.Split(chArray);
      columnName = string.Empty;
      string[] strArray2 = new string[19];
      int index2 = 0;
      string str2 = "ItemCode";
      strArray2[index2] = str2;
      int index3 = 1;
      string str3 = "Location";
      strArray2[index3] = str3;
      int index4 = 2;
      string str4 = "BatchNo";
      strArray2[index4] = str4;
      int index5 = 3;
      string str5 = "DtlDescription";
      strArray2[index5] = str5;
      int index6 = 4;
      string str6 = "FurtherDescription";
      strArray2[index6] = str6;
      int index7 = 5;
      string str7 = "ProjNo";
      strArray2[index7] = str7;
      int index8 = 6;
      string str8 = "DeptNo";
      strArray2[index8] = str8;
      int index9 = 7;
      string str9 = "Qty";
      strArray2[index9] = str9;
      int index10 = 8;
      string str10 = "UnitCost";
      strArray2[index10] = str10;
      int index11 = 9;
      string str11 = "SubTotal";
      strArray2[index11] = str11;
      int index12 = 10;
      string str12 = "UOM";
      strArray2[index12] = str12;
      int index13 = 11;
      string str13 = "ProjDesc";
      strArray2[index13] = str13;
      int index14 = 12;
      string str14 = "DeptDesc";
      strArray2[index14] = str14;
            int index15 = 13;
            string str15 = "Debit";
            strArray2[index15] = str15;
            int index16 = 14;
            string str16 = "Credit";
            strArray2[index16] = str16;
            int index17 = 15;
            string str17 = "FromDocNo";
            strArray2[index17] = str17;
            int index18 = 16;
            string str18 = "FromDocType";
            strArray2[index18] = str18;

            

           
            string[] strArray3 = strArray2;
      foreach (string str21 in strArray1)
      {
        bool flag = false;
        string oldValue = str21.Trim();
        if (!(oldValue == "ItemGroup") && !(oldValue == "ItemType"))
        {
          foreach (string str22 in strArray3)
          {
            if (str22 == oldValue)
            {
              flag = true;
              columnName = !(str22 == "DtlDescription") ? (!(str22 == "ProjDesc") ? (!(str22 == "DeptDesc") ? columnName + oldValue.Replace(oldValue, "RPA_RCVDtl." + oldValue + ",") : columnName + oldValue.Replace(oldValue, "Dept.Description as " + oldValue + ",")) : columnName + oldValue.Replace(oldValue, "Project.Description as " + oldValue + ",")) : columnName + oldValue.Replace(oldValue, "RPA_RCVDtl.Description as " + oldValue + ",");
              break;
            }
          }
          //UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
          //foreach (UDFColumn udfColumn in udfUtil.GetUDF("RPA_RCV"))
          //{
          //  if ("RPA_RCV" + udfColumn.FieldName == str15)
          //  {
          //    string[] strArray4 = new string[6];
          //    int index15 = 0;
          //    string str16 = columnName;
          //    strArray4[index15] = str16;
          //    int index16 = 1;
          //    string str17 = "RPA_RCV.";
          //    strArray4[index16] = str17;
          //    int index17 = 2;
          //    string fieldName1 = udfColumn.FieldName;
          //    strArray4[index17] = fieldName1;
          //    int index18 = 3;
          //    string str18 = " AS RPA_RCV";
          //    strArray4[index18] = str18;
          //    int index19 = 4;
          //    string fieldName2 = udfColumn.FieldName;
          //    strArray4[index19] = fieldName2;
          //    int index20 = 5;
          //    string str19 = ",";
          //    strArray4[index20] = str19;
          //    columnName = string.Concat(strArray4);
          //    flag = true;
          //    break;
          //  }
          //}
          //foreach (UDFColumn udfColumn in udfUtil.GetUDF("RPA_RCVDtl"))
          //{
          //  if ("RPA_RCVDtl" + udfColumn.FieldName == str15)
          //  {
          //    string[] strArray4 = new string[6];
          //    int index15 = 0;
          //    string str16 = columnName;
          //    strArray4[index15] = str16;
          //    int index16 = 1;
          //    string str17 = "RPA_RCVDtl.";
          //    strArray4[index16] = str17;
          //    int index17 = 2;
          //    string fieldName1 = udfColumn.FieldName;
          //    strArray4[index17] = fieldName1;
          //    int index18 = 3;
          //    string str18 = " AS RPA_RCVDtl";
          //    strArray4[index18] = str18;
          //    int index19 = 4;
          //    string fieldName2 = udfColumn.FieldName;
          //    strArray4[index19] = fieldName2;
          //    int index20 = 5;
          //    string str19 = ",";
          //    strArray4[index20] = str19;
          //    columnName = string.Concat(strArray4);
          //    flag = true;
          //    break;
          //  }
          //}
          if (!flag)
            columnName = columnName + oldValue.Replace(oldValue, "RPA_RCV." + oldValue + ",");
        }
      }
      if (columnName.EndsWith(","))
        columnName = columnName.Remove(columnName.Length - 1, 1);
      columnName = columnName.Trim();
      string str20 = string.Empty;
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = sqlConnection;
        string whereSql = this.GetWhereSQL(criteria, sqlCommand);
        string str22 = !(whereSql == string.Empty) ? string.Format(format, (object) columnName, (object) whereSql) : string.Format(format, (object) columnName, (object) "(1=1)");
        string str23 = SQLHelper.BuildFilteredByUserSQL(this.myDBSetting, "RPA_RCVDtl");
        if (str23.Length > 0)
                    str22 = str22 + " AND " + str23;
        sqlCommand.CommandText = str22;
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
        {
          dataTable2.Clear();
          dataTable1.Clear();
        }
        else if (chkEditColumnName.Length > 0 && dataTable1.Rows.Count > 0)
        {
          DataRow[] dataRowArray = dataTable1.Select(string.Format("{0} = true", (object) chkEditColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index23 = 0; index23 < dataRowArray.Length; ++index23)
            numArray[index23] = BCE.Data.Convert.ToInt64(dataRowArray[index23]["DtlKey"]);
        }
        sqlConnection.Open();
        sqlDataAdapter.Fill(dataTable1);
        if (dataTable1.PrimaryKey.Length == 0)
        {
          DataTable dataTable3 = dataTable1;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index24 = 0;
          DataColumn dataColumn = dataTable1.Columns["DtlKey"];
          dataColumnArray[index24] = dataColumn;
          dataTable3.PrimaryKey = dataColumnArray;
        }
        if (criteria.SortBy == DetailListingSortByOption.DocumentNo)
          dataTable1.DefaultView.Sort = "DocNo";
        else if (criteria.SortBy == DetailListingSortByOption.Date)
          dataTable1.DefaultView.Sort = "DocDate";
        else if (criteria.SortBy == DetailListingSortByOption.ItemCode)
          dataTable1.DefaultView.Sort = "ItemCode";
        else if (criteria.SortBy == DetailListingSortByOption.ItemGroup)
          dataTable1.DefaultView.Sort = "ItemGroup";
        else if (criteria.SortBy == DetailListingSortByOption.ItemType)
          dataTable1.DefaultView.Sort = "ItemType";
        else if (criteria.SortBy == DetailListingSortByOption.Location)
          dataTable1.DefaultView.Sort = "Location";
        else if (criteria.SortBy == DetailListingSortByOption.ProjectNo)
          dataTable1.DefaultView.Sort = "ProjNo";
        else if (criteria.SortBy == DetailListingSortByOption.DepartmentNo)
          dataTable1.DefaultView.Sort = "DeptNo";
        if (numArray != null)
        {
          foreach (long num2 in numArray)
          {
            DataRow dataRow = dataTable1.Rows.Find((object) num2);
            if (dataRow != null)
              dataRow[chkEditColumnName] = (object) true;
          }
        }
      }
      catch (Exception ex)
      {
        if (ex is SqlException)
          BCE.Data.DataError.HandleSqlException((SqlException) ex);
        if (!(ex is ConstraintException))
          throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }
  }
}
