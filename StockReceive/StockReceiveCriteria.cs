﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveCriteria
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.SearchFilter;

namespace Production.StockReceive
{
  public class StockReceiveCriteria : SearchCriteria
  {
    private SimpleSearch myDocNo;
    private SimpleSearch myDocDate;
    private SimpleSearch myDescription;
    private SimpleSearch myRefDocNo;
    private string myUDF;
    private string myDetailUDF;

    public string DocNo
    {
      set
      {
        this.myDocNo.SearchData = value;
      }
    }

    public string DocDate
    {
      set
      {
        this.myDocDate.SearchData = value;
      }
    }

    public string Description
    {
      set
      {
        this.myDescription.SearchData = value;
      }
    }

    public string RefDocNo
    {
      set
      {
        this.myRefDocNo.SearchData = value;
      }
    }

    public string UDF
    {
      get
      {
        return this.myUDF;
      }
      set
      {
        this.myUDF = value;
      }
    }

    public string DetailUDF
    {
      get
      {
        return this.myDetailUDF;
      }
      set
      {
        this.myDetailUDF = value;
      }
    }

    public StockReceiveCriteria()
    {
      this.AddSearch(this.myDocNo = new SimpleSearch("A", "DocNo"));
      this.AddSearch(this.myDocDate = new SimpleSearch("A", "DocDate"));
      this.AddSearch(this.myDescription = new SimpleSearch("A", "Description"));
      this.AddSearch(this.myRefDocNo = new SimpleSearch("A", "RefDocNo"));
      this.myUDF = "";
      this.myDetailUDF = "";
    }
  }
}
