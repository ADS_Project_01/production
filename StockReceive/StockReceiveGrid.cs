﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveGrid
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
//using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Production.StockReceive
{
  public class StockReceiveGrid : UserControl
  {
    private string myGroupSummaryId = "";
        private CustomDrawEventHandler DrawGroupPanelEvent;
        protected DecimalSetting myDecimalSetting;
    protected UserAuthentication myUserAuthentication;
    private IContainer components;
    private GridControl myStockReceiveGridControl;
    private GridView gridViewStockReceive;
    private GridColumn colTick;
    private RepositoryItemCheckEdit repositoryItemCheckEditTick;
    private GridColumn colDocNo;
    private GridColumn colDocDate;
    private GridColumn colDescription;
    private GridColumn colTotal;
    private GridColumn colRemark1;
    private GridColumn colRemark2;
    private GridColumn colRemark3;
    private GridColumn colRemark4;
    private GridColumn colPrintCount;
    private GridColumn colCancelled;
    private RepositoryItemTextEdit repositoryItemTextEdit1;
    private GridColumn colLastModified;
    private GridColumn colLastModifiedUserID;
    private GridColumn colCreatedTimeStamp;
    private GridColumn colCreatedUserID;
    private GridColumn colRefDocNo;
        private GridColumn colJEKey;
        private GridColumn colJENo;
        private GridColumn colAccNo;
        private GridColumn colMachineCode;
        private EventHandler ReloadAllColumnsEvent;

        [Description("The DataSource of the GridControl")]
    [Category("User Control")]
    [DefaultValue(null)]
    public object DataSource
    {
      get
      {
        return this.myStockReceiveGridControl.DataSource;
      }
      set
      {
        this.myStockReceiveGridControl.DataSource = value;
      }
    }

    [Description("The Main GridControl")]
    [Category("User Control")]
    public GridControl GridControl
    {
      get
      {
        return this.myStockReceiveGridControl;
      }
    }

    [Description("The Main GridView")]
    [Category("User Control")]
    public GridView GridView
    {
      get
      {
        return this.gridViewStockReceive;
      }
    }

    [Description("Specify the FieldName of the Tick Column, this property will be used if the ShowTickColumn is True.")]
    [Category("User Control")]
    [DefaultValue("Tick")]
    public string TickColumnFieldName
    {
      get
      {
        return this.colTick.FieldName;
      }
      set
      {
        this.colTick.FieldName = value;
        this.colTick.Caption = " ";
      }
    }

    [Description("Specify whether the Tick Column will be show or hide, default is hide.")]
    [Category("User Control")]
    [DefaultValue(false)]
    public bool ShowTickColumn
    {
      get
      {
        return this.colTick.Visible;
      }
      set
      {
        this.colTick.Visible = value;
        this.gridViewStockReceive.OptionsSelection.MultiSelect = value;
        if (value)
          this.colTick.VisibleIndex = 0;
      }
    }

    [Description("Occurs whenever there is a new column appears on the grid.")]
    [Category("User Control")]
    public event EventHandler ReloadAllColumnsEvent2
    {
      add
      {
        EventHandler eventHandler = this.ReloadAllColumnsEvent;
        EventHandler comparand;
        do
        {
          comparand = eventHandler;
          eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.ReloadAllColumnsEvent, comparand + value, comparand);
        }
        while (eventHandler != comparand);
      }
      remove
      {
        EventHandler eventHandler = this.ReloadAllColumnsEvent;
        EventHandler comparand;
        do
        {
          comparand = eventHandler;
          eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.ReloadAllColumnsEvent, comparand - value, comparand);
        }
        while (eventHandler != comparand);
      }
    }

    [Description("Occurs whenever there is a manually custom draw string on the grid group panel.")]
    [Category("User Control")]
    public event CustomDrawEventHandler DrawGroupPanelEvent2
    {
      add
      {
        CustomDrawEventHandler drawEventHandler = this.DrawGroupPanelEvent;
        CustomDrawEventHandler comparand;
        do
        {
          comparand = drawEventHandler;
          drawEventHandler = Interlocked.CompareExchange<CustomDrawEventHandler>(ref this.DrawGroupPanelEvent, comparand + value, comparand);
        }
        while (drawEventHandler != comparand);
      }
      remove
      {
        CustomDrawEventHandler drawEventHandler = this.DrawGroupPanelEvent;
        CustomDrawEventHandler comparand;
        do
        {
          comparand = drawEventHandler;
          drawEventHandler = Interlocked.CompareExchange<CustomDrawEventHandler>(ref this.DrawGroupPanelEvent, comparand - value, comparand);
        }
        while (drawEventHandler != comparand);
      }
    }

    public StockReceiveGrid()
    {
      this.InitializeComponent();
    }

    public void Initialize(DBSetting dbSetting)
    {
      this.myStockReceiveGridControl.ForceInitialize();
      this.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
      this.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
      FormControlUtil formControlUtil = new FormControlUtil(dbSetting);
      string fieldname1 = "DocDate";
      string fieldtype1 = "Date";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "Total";
      string fieldtype2 = "Currency";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "LastModified";
      string fieldtype3 = "DateTime";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "CreatedTimeStamp";
      string fieldtype4 = "DateTime";
      formControlUtil.AddField(fieldname4, fieldtype4);
      StockReceiveGrid stockReceiveGrid = this;
      formControlUtil.InitControls((Control) stockReceiveGrid);
      this.InitGroupSummary();
     // new UDFUtil(dbSetting).SetupGrid(this.gridViewStockReceive, "RPA_RCV", false);
      new CustomizeGridLayout(dbSetting, this.Name, this.gridViewStockReceive, new EventHandler(this.ReloadAllColumns)).PageHeader = BCE.Localization.Localizer.GetString((Enum) StockReceiveStringId.Code_StockReceiveListing, new object[0]);
      if (this.ParentForm != null)
        this.myUserAuthentication.AccessRight.AddListener((AccessRightListenerDelegate) (() => this.InvokeAccessRightChanged(dbSetting)), (Component) this.ParentForm);
      this.ControlShowTotalCost();
    }

    public string BuildSQLColumns(bool allColumns)
    {
      ColumnView view = this.gridViewStockReceive.Columns.View;
      string prefix = "";
      int num = allColumns ? 1 : 0;
      string[] mustIncludeColNames = new string[1];
      int index1 = 0;
      string str1 = "DocKey";
      mustIncludeColNames[index1] = str1;
      string[] excludeColNames = new string[3];
      int index2 = 0;
      string tickColumnFieldName = this.TickColumnFieldName;
      excludeColNames[index2] = tickColumnFieldName;
      int index3 = 1;
      string str2 = "Month";
      excludeColNames[index3] = str2;
      int index4 = 2;
      string str3 = "Year";
      excludeColNames[index4] = str3;
      return ColumnViewUtils.BuildSQLColumnListFromColumnView(view, prefix, num != 0, mustIncludeColNames, excludeColNames);
    }

    private void InitGroupSummary()
    {
      StringBuilder stringBuilder = new StringBuilder(40);
      stringBuilder.Append("DocKey");
      if (this.colTotal.Visible)
        stringBuilder.Append("Total");
      string str1 = ((object) stringBuilder).ToString();
      if (str1 != this.myGroupSummaryId)
      {
        this.myGroupSummaryId = str1;
        this.gridViewStockReceive.GroupSummary.Clear();
        GridGroupSummaryItemCollection groupSummary1 = this.gridViewStockReceive.GroupSummary;
        int num1 = 3;
        string fieldName1 = "DocNo";
                // ISSUE: variable of the null type
                GridColumn local1 = null;
        // ISSUE: variable of a boxed type
        GridGroupSummaryItemStringId local2 =  GridGroupSummaryItemStringId.Count;
        object[] objArray1 = new object[1];
        int index1 = 0;
        string str2 = "{0}";
        objArray1[index1] = (object) str2;
        string string1 = BCE.Localization.Localizer.GetString((Enum) local2, objArray1);
        groupSummary1.Add((SummaryItemType) num1, fieldName1, (GridColumn) local1, string1);
        GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockReceive.GroupSummary;
        int num2 = 0;
        string fieldName2 = "Total";
                // ISSUE: variable of the null type
                GridColumn local3 = null;
        // ISSUE: variable of a boxed type
        GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.Total;
        object[] objArray2 = new object[1];
        int index2 = 0;
        string currencyFormatString = this.myDecimalSetting.GetCurrencyFormatString(0);
        objArray2[index2] = (object) currencyFormatString;
        string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
        groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      }
    }

    private void ControlShowTotalCost()
    {
      if (!ModuleControl.GetOrCreate(BCE.AutoCount.Application.DBSetting).ModuleController.ExpressEdition.Has)
      {
        bool flag = this.myUserAuthentication.AccessRight.IsAccessible("STK_ISS_SHOW_TOTAL");
        this.colTotal.Visible = flag;
        this.colTotal.OptionsColumn.ShowInCustomizationForm = flag;
        this.gridViewStockReceive.OptionsView.ShowFooter = flag;
      }
    }

    private void gridViewStockMain_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.colTick.Visible)
      {
        GridView gridView = (GridView) sender;
        int[] selectedRows = gridView.GetSelectedRows();
        if (selectedRows != null && selectedRows.Length != 0 && selectedRows.Length != 1)
        {
          for (int index = 0; index < selectedRows.Length; ++index)
          {
            DataRow dataRow = gridView.GetDataRow(selectedRows[index]);
            if (dataRow != null)
              dataRow[this.TickColumnFieldName] = (object) true;
          }
        }
      }
    }

    private void gridViewStockMain_Layout(object sender, EventArgs e)
    {
      if (this.DataSource != null)
        this.InitGroupSummary();
    }

    private void ReloadAllColumns(object sender, EventArgs e)
    {
      // ISSUE: reference to a compiler-generated field
      if (this.ReloadAllColumnsEvent != null)
      {
        // ISSUE: reference to a compiler-generated field
        this.ReloadAllColumnsEvent(sender, e);
      }
    }

    private void InvokeAccessRightChanged(DBSetting dbSetting)
    {
            this.BeginInvoke((Delegate)new MethodInvoker(this.ControlShowTotalCost));
        }

        private void gridViewStockReceive_CustomDrawGroupPanel(object sender, CustomDrawEventArgs e)
    {
      // ISSUE: reference to a compiler-generated field
      if (this.DrawGroupPanelEvent != null)
      {
        // ISSUE: reference to a compiler-generated field
        this.DrawGroupPanelEvent(sender, e);
      }
    }

    private void repositoryItemTextEdit1_FormatEditValue(object sender, ConvertEditValueEventArgs e)
    {
      if (e.Value != null)
      {
        if (e.Value.ToString() == "T")
          e.Value = (object)BCE.Localization.Localizer.GetString((Enum) StockStringId.Cancelled, new object[0]);
        else
          e.Value = (object) "";
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

        private void InitializeComponent()
        {
            this.myStockReceiveGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridViewStockReceive = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTick = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditTick = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJEKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJENo = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.myStockReceiveGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStockReceive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditTick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // myStockReceiveGridControl
            // 
            this.myStockReceiveGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.myStockReceiveGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.myStockReceiveGridControl.Location = new System.Drawing.Point(0, 0);
            this.myStockReceiveGridControl.MainView = this.gridViewStockReceive;
            this.myStockReceiveGridControl.Name = "myStockReceiveGridControl";
            this.myStockReceiveGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditTick,
            this.repositoryItemTextEdit1});
            this.myStockReceiveGridControl.Size = new System.Drawing.Size(752, 240);
            this.myStockReceiveGridControl.TabIndex = 3;
            this.myStockReceiveGridControl.UseEmbeddedNavigator = true;
            this.myStockReceiveGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStockReceive});
            // 
            // gridViewStockReceive
            // 
            this.gridViewStockReceive.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTick,
            this.colDocNo,
            this.colDocDate,
            this.colDescription,
            this.colMachineCode,
            this.colTotal,
            this.colRemark1,
            this.colRemark2,
            this.colRemark3,
            this.colRemark4,
            this.colPrintCount,
            this.colCancelled,
            this.colLastModified,
            this.colLastModifiedUserID,
            this.colCreatedTimeStamp,
            this.colCreatedUserID,
            this.colAccNo,
            this.colRefDocNo,
            this.colJEKey,
            this.colJENo});
            this.gridViewStockReceive.GridControl = this.myStockReceiveGridControl;
            this.gridViewStockReceive.GroupFormat = "{0}: {1} ({2})";
            this.gridViewStockReceive.Name = "gridViewStockReceive";
            this.gridViewStockReceive.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridViewStockReceive.OptionsSelection.MultiSelect = true;
            this.gridViewStockReceive.OptionsView.ShowFooter = true;
            this.gridViewStockReceive.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDocDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewStockReceive.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewStockMain_SelectionChanged);
            this.gridViewStockReceive.Layout += new System.EventHandler(this.gridViewStockMain_Layout);
            // 
            // colTick
            // 
            this.colTick.Caption = "Tick";
            this.colTick.ColumnEdit = this.repositoryItemCheckEditTick;
            this.colTick.FieldName = "Tick";
            this.colTick.Name = "colTick";
            this.colTick.OptionsColumn.ShowInCustomizationForm = false;
            this.colTick.Width = 32;
            // 
            // repositoryItemCheckEditTick
            // 
            this.repositoryItemCheckEditTick.AutoHeight = false;
            this.repositoryItemCheckEditTick.Caption = "Check";
            this.repositoryItemCheckEditTick.Name = "repositoryItemCheckEditTick";
            // 
            // colDocNo
            // 
            this.colDocNo.Caption = "Doc. No.";
            this.colDocNo.FieldName = "DocNo";
            this.colDocNo.Name = "colDocNo";
            this.colDocNo.OptionsColumn.AllowEdit = false;
            this.colDocNo.Visible = true;
            this.colDocNo.VisibleIndex = 0;
            this.colDocNo.Width = 68;
            // 
            // colDocDate
            // 
            this.colDocDate.Caption = "Date";
            this.colDocDate.FieldName = "DocDate";
            this.colDocDate.Name = "colDocDate";
            this.colDocDate.OptionsColumn.AllowEdit = false;
            this.colDocDate.Visible = true;
            this.colDocDate.VisibleIndex = 1;
            this.colDocDate.Width = 62;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 2;
            this.colDescription.Width = 181;
            // 
            // colMachineCode
            // 
            this.colMachineCode.Caption = "MachineCode";
            this.colMachineCode.FieldName = "MachineCode";
            this.colMachineCode.Name = "colMachineCode";
            this.colMachineCode.Visible = true;
            this.colMachineCode.VisibleIndex = 3;
            // 
            // colTotal
            // 
            this.colTotal.Caption = "Total";
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.OptionsColumn.AllowEdit = false;
            this.colTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTotal.Visible = true;
            this.colTotal.VisibleIndex = 4;
            this.colTotal.Width = 61;
            // 
            // colRemark1
            // 
            this.colRemark1.Caption = "Remark 1";
            this.colRemark1.FieldName = "Remark1";
            this.colRemark1.Name = "colRemark1";
            this.colRemark1.OptionsColumn.AllowEdit = false;
            // 
            // colRemark2
            // 
            this.colRemark2.Caption = "Remark 2";
            this.colRemark2.FieldName = "Remark2";
            this.colRemark2.Name = "colRemark2";
            this.colRemark2.OptionsColumn.AllowEdit = false;
            // 
            // colRemark3
            // 
            this.colRemark3.Caption = "Remark 3";
            this.colRemark3.FieldName = "Remark3";
            this.colRemark3.Name = "colRemark3";
            this.colRemark3.OptionsColumn.AllowEdit = false;
            // 
            // colRemark4
            // 
            this.colRemark4.Caption = "Remark 4";
            this.colRemark4.FieldName = "Remark4";
            this.colRemark4.Name = "colRemark4";
            this.colRemark4.OptionsColumn.AllowEdit = false;
            // 
            // colPrintCount
            // 
            this.colPrintCount.Caption = "Print Count";
            this.colPrintCount.FieldName = "PrintCount";
            this.colPrintCount.Name = "colPrintCount";
            this.colPrintCount.OptionsColumn.AllowEdit = false;
            this.colPrintCount.Width = 52;
            // 
            // colCancelled
            // 
            this.colCancelled.Caption = "Cancelled";
            this.colCancelled.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCancelled.FieldName = "Cancelled";
            this.colCancelled.Name = "colCancelled";
            this.colCancelled.OptionsColumn.AllowEdit = false;
            this.colCancelled.Visible = true;
            this.colCancelled.VisibleIndex = 6;
            this.colCancelled.Width = 63;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.FormatEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repositoryItemTextEdit1_FormatEditValue);
            // 
            // colLastModified
            // 
            this.colLastModified.Caption = "Last Modified Time";
            this.colLastModified.FieldName = "LastModified";
            this.colLastModified.Name = "colLastModified";
            this.colLastModified.OptionsColumn.AllowEdit = false;
            this.colLastModified.Visible = true;
            this.colLastModified.VisibleIndex = 8;
            this.colLastModified.Width = 48;
            // 
            // colLastModifiedUserID
            // 
            this.colLastModifiedUserID.Caption = "Last Modified User";
            this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
            this.colLastModifiedUserID.Name = "colLastModifiedUserID";
            this.colLastModifiedUserID.OptionsColumn.AllowEdit = false;
            this.colLastModifiedUserID.Visible = true;
            this.colLastModifiedUserID.VisibleIndex = 9;
            this.colLastModifiedUserID.Width = 48;
            // 
            // colCreatedTimeStamp
            // 
            this.colCreatedTimeStamp.Caption = "Created Time";
            this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
            this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
            this.colCreatedTimeStamp.OptionsColumn.AllowEdit = false;
            this.colCreatedTimeStamp.Width = 48;
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.Caption = "Created User";
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.Name = "colCreatedUserID";
            this.colCreatedUserID.OptionsColumn.AllowEdit = false;
            this.colCreatedUserID.Width = 109;
            // 
            // colAccNo
            // 
            this.colAccNo.Caption = "AccNo";
            this.colAccNo.Name = "colAccNo";
            this.colAccNo.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colRefDocNo
            // 
            this.colRefDocNo.Caption = "Ref. Doc. No.";
            this.colRefDocNo.FieldName = "RefDocNo";
            this.colRefDocNo.Name = "colRefDocNo";
            this.colRefDocNo.OptionsColumn.AllowEdit = false;
            this.colRefDocNo.Visible = true;
            this.colRefDocNo.VisibleIndex = 5;
            this.colRefDocNo.Width = 91;
            // 
            // colJEKey
            // 
            this.colJEKey.Caption = "JE Key";
            this.colJEKey.FieldName = "JEKey";
            this.colJEKey.Name = "colJEKey";
            this.colJEKey.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colJENo
            // 
            this.colJENo.Caption = "JE No.";
            this.colJENo.FieldName = "JENo";
            this.colJENo.Name = "colJENo";
            this.colJENo.Visible = true;
            this.colJENo.VisibleIndex = 7;
            // 
            // StockReceiveGrid
            // 
            this.Controls.Add(this.myStockReceiveGridControl);
            this.Name = "StockReceiveGrid";
            this.Size = new System.Drawing.Size(752, 240);
            ((System.ComponentModel.ISupportInitialize)(this.myStockReceiveGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStockReceive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditTick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

    }
}
