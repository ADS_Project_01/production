﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveBeforeDeleteEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveBeforeDeleteEventArgs : StockReceiveEventArgs
  {
    private string myErrorMessage = "";
    private DBSetting myDBSetting;
    internal bool myAbort;

    public new DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return this.myErrorMessage;
      }
      set
      {
        this.myErrorMessage = value;
      }
    }

    internal StockReceiveBeforeDeleteEventArgs(StockReceive doc, DBSetting dbSetting)
      : base(doc)
    {
      this.myDBSetting = dbSetting;
    }

    public void AbortDelete()
    {
      this.myAbort = true;
    }
  }
}
