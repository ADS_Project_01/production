﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveRecalculateDetailEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;
using System.Data;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveRecalculateDetailEventArgs
  {
    private DataRow myDetailRow;
    private DBSetting myDBSetting;

    public StockReceiveDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockReceiveDetailRecord(this.myDBSetting, this.myDetailRow);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    internal StockReceiveRecalculateDetailEventArgs(DBSetting dbSetting, DataRow detailRow)
    {
      this.myDetailRow = detailRow;
      this.myDBSetting = dbSetting;
    }
  }
}
