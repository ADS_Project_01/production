﻿// Type: BCE.AutoCount.Stock.StockReceive.AdvancedStockReceiveCriteria
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.SearchFilter;
//using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Localization;
using System;

namespace Production.StockReceive
{
  [Serializable]
  public class AdvancedStockReceiveCriteria : SearchCriteria
  {
    public AdvancedStockReceiveCriteria(DBSetting dbSetting)
    {
      this.Add((SearchElement) new SearchDecorator("", "", Localizer.GetString((Enum) StockReceiveColumnStringId.MasterSearchFields, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "DocNo", Localizer.GetString((Enum) StockReceiveColumnStringId.StockReceiveNo, new object[0]), FilterControlType.StockReceive));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "DocDate", Localizer.GetString((Enum) StockReceiveColumnStringId.StockReceiveDate, new object[0]), FilterControlType.Date));
      this.Add((SearchElement) new TextSearch("A", "Description", Localizer.GetString((Enum) StockReceiveColumnStringId.Description, new object[0])));
      this.Add((SearchElement) new NumberSearch("A", "Total", Localizer.GetString((Enum) StockReceiveColumnStringId.Total, new object[0])));
      this.Add((SearchElement) new MemoSearch("A", "Note", Localizer.GetString((Enum) StockReceiveColumnStringId.Note, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark1", Localizer.GetString((Enum) StockReceiveColumnStringId.Remark1, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark2", Localizer.GetString((Enum) StockReceiveColumnStringId.Remark2, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark3", Localizer.GetString((Enum) StockReceiveColumnStringId.Remark3, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark4", Localizer.GetString((Enum) StockReceiveColumnStringId.Remark4, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "RefDocNo", Localizer.GetString((Enum) StockReceiveColumnStringId.RefDocNo, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "CreatedUserID", Localizer.GetString((Enum) StockReceiveColumnStringId.CreatedUserID, new object[0]), FilterControlType.User));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "LastModifiedUserID", Localizer.GetString((Enum) StockReceiveColumnStringId.LastModifiedUserID, new object[0]), FilterControlType.User));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "CreatedTimeStamp", Localizer.GetString((Enum) StockReceiveColumnStringId.CreatedTimestamp, new object[0]), FilterControlType.DateTime));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "LastModified", Localizer.GetString((Enum) StockReceiveColumnStringId.LastModified, new object[0]), FilterControlType.DateTime));
      this.Add((SearchElement) new BooleanSearch("A", "Cancelled", Localizer.GetString((Enum) StockReceiveColumnStringId.CancelledDocument, new object[0])));
      //new UDFUtil(dbSetting).AddUDFIntoAdvancedSearch((SearchCriteria) this, "RPA_RCV", "A");
      this.Add((SearchElement) new SearchDecorator("", "", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.DetailSearchFields, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "ItemCode", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.ItemCode, new object[0]), FilterControlType.Item));
      this.Add((SearchElement) new TextSearch("B", "Description", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.ItemDescription, new object[0])));
      this.Add((SearchElement) new MemoSearch("B", "FurtherDescription", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.ItemFurtherDescription, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "Location", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Location, new object[0]), FilterControlType.Location));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "ProjNo", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Project, new object[0]), FilterControlType.Project));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "DeptNo", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Department, new object[0]), FilterControlType.Department));
      this.Add((SearchElement) new TextSearch("B", "BatchNo", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.BatchNo, new object[0])));
      this.Add((SearchElement) new TextSearch("B", "UOM", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.UOM, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "Qty", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Quantity, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "UnitCost", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.UnitCost, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "SubTotal", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.SubTotal, new object[0])));
      this.Add((SearchElement) new TextSearch("B", "Numbering", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Numbering, new object[0])));
     // new UDFUtil(dbSetting).AddUDFIntoAdvancedSearch((SearchCriteria) this, "RPA_RCVDtl", "B");
    }
  }
}
