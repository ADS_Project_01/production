﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveOnDeleteEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveOnDeleteEventArgs : StockReceiveEventArgs
  {
    private DBSetting myDBSetting;

    public new DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    internal StockReceiveOnDeleteEventArgs(StockReceive doc, DBSetting dbSetting)
      : base(doc)
    {
      this.myDBSetting = dbSetting;
    }
  }
}
