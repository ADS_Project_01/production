﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveReportTypeHandler
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Stock;
using BCE.Data;

namespace Production.StockReceive
{
  public class StockReceiveReportTypeHandler : StockDocumentReportTypeHandler
  {
    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      return StockReceiveCommand.Create(dbSetting).GetReportDesignerDataSource();
    }
  }
}
