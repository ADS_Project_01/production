﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveCommandSQL
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Common;
using BCE.AutoCount.ContextException;
using BCE.AutoCount.Data;
using BCE.AutoCount.Document;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.RegistryID.LastSavedDescriptionID;
//using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.SerialNumber2;
using BCE.AutoCount.Stock;
//using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Production.StockReceive
{
  public class StockReceiveCommandSQL : StockReceiveCommand
  {
    protected override DataSet LoadData(long docKey)
    {
      DataSet dataSet = DocumentHelper.LoadMasterDetailData(this.myDBSetting, docKey, "SELECT * FROM RPA_RCV WHERE DocKey=@DocKey", "SELECT * FROM RPA_RCVDtl WHERE DocKey=@DocKey ORDER BY Seq");
      dataSet.Tables.Add(SerialNumberHelper.GetSNTransByDocKey(this.myDBSetting, docKey));
     // UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
      string tableName1 = "RPA_RCV";
      DataTable table1 = dataSet.Tables["Master"];
      //udfUtil.SetDefaultValue(tableName1, table1);
      string tableName2 = "RPA_RCVDtl";
      DataTable table2 = dataSet.Tables["Detail"];
     // udfUtil.SetDefaultValue(tableName2, table2);
      return dataSet;
    }

    protected override DataSet LoadData(string docNo)
    {
      DataSet dataSet = DocumentHelper.LoadMasterDetailData(this.myDBSetting, docNo, "SELECT * FROM RPA_RCV WHERE DocNo=@DocNo", "SELECT A.* FROM RPA_RCVDtl A, RPA_RCV B WHERE A.DocKey=B.DocKey AND B.DocNo=@DocNo ORDER BY A.Seq");
      dataSet.Tables.Add(SerialNumberHelper.GetSNTransByDocKey(this.myDBSetting, BCE.Data.Convert.ToInt64(dataSet.Tables["Master"].Rows[0]["DocKey"])));
      //UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
      string tableName1 = "RPA_RCV";
      DataTable table1 = dataSet.Tables["Master"];
      //udfUtil.SetDefaultValue(tableName1, table1);
      string tableName2 = "RPA_RCVDtl";
      DataTable table2 = dataSet.Tables["Detail"];
      //udfUtil.SetDefaultValue(tableName2, table2);
      return dataSet;
    }

    protected override long LoadFirst()
    {
      return DocumentHelper.GetFirstDocumentKey(this.myDBSetting, "RPA_RCV");
    }

    protected override long LoadLast()
    {
      return DocumentHelper.GetLastDocumentKey(this.myDBSetting, "RPA_RCV");
    }

    protected override long LoadNext(string docNo)
    {
      return DocumentHelper.GetNextDocumentKey(this.myDBSetting, "RPA_RCV", docNo);
    }

    protected override long LoadPrev(string docNo)
    {
      return DocumentHelper.GetPrevDocumentKey(this.myDBSetting, "RPA_RCV", docNo);
    }

        protected internal override void SaveData(StockReceive stockReceive, bool canceldoc,bool PostTrans)
        {
            string str = "";
            DataSet dataSet = (DataSet)null;
            bool flag = false;
            TransactionControl.DisableTransactionCounter();
            DBSetting dbSetting = this.myDBSetting.StartTransaction();
            try
            {
                //StockReceiveBeforeSaveEventArgs beforeSaveEventArgs1 = new StockReceiveBeforeSaveEventArgs(stockReceive, dbSetting);
                //ScriptObject scriptObject1 = stockReceive.ScriptObject;
                //string name1 = "BeforeSave";
                //Type[] types1 = new Type[1];
                //int index1 = 0;
                //Type type1 = beforeSaveEventArgs1.GetType();
                //types1[index1] = type1;
                //object[] objArray1 = new object[1];
                //int index2 = 0;
                //StockReceiveBeforeSaveEventArgs beforeSaveEventArgs2 = beforeSaveEventArgs1;
                //objArray1[index2] = (object) beforeSaveEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray1);
                //if (beforeSaveEventArgs1.ErrorMessage != null && beforeSaveEventArgs1.ErrorMessage.Length > 0)
                //  throw new StandardApplicationException(beforeSaveEventArgs1.ErrorMessage);
                //else if (beforeSaveEventArgs1.myAbort)
                //{
                //  throw new StandardApplicationException("");
                //}
                //else
                {
                    dataSet = stockReceive.StockReceiveDataSet.Copy();
                    StockReceive doc = new StockReceive(StockReceiveCommand.Create(dbSetting), dataSet, stockReceive.Action);
                    StockReceiveOnSaveEventArgs issueOnSaveEventArgs1 = new StockReceiveOnSaveEventArgs(doc, dbSetting);
                    //ScriptObject scriptObject2 = stockReceive.ScriptObject;
                    //string name2 = "OnSave";
                    //Type[] types2 = new Type[1];
                    //int index3 = 0;
                    //Type type2 = issueOnSaveEventArgs1.GetType();
                    //types2[index3] = type2;
                    //object[] objArray2 = new object[1];
                    //int index4 = 0;
                    //StockReceiveOnSaveEventArgs issueOnSaveEventArgs2 = issueOnSaveEventArgs1;
                    //objArray2[index4] = (object) issueOnSaveEventArgs2;
                    //scriptObject2.RunMethod(name2, types2, objArray2);
                    DataRow masterRow = dataSet.Tables["Master"].Rows[0];
                    flag = masterRow["DocNo"].ToString() == "<<New>>";
                    if (flag)
                    {
                        Document document = Document.CreateDocument(dbSetting);
                        masterRow["DocNo"] = (object)document.IncreaseNextNumber("RV", stockReceive.DocNoFormatName, BCE.Data.Convert.ToDateTime(masterRow["DocDate"]));
                        StockReceiveEventArgs stockReceiveEventArgs1 = new StockReceiveEventArgs(doc);
                        //ScriptObject scriptObject3 = stockReceive.ScriptObject;
                        //string name3 = "OnGetNewDocumentNo";
                        //Type[] types3 = new Type[1];
                        //int index5 = 0;
                        //Type type3 = stockReceiveEventArgs1.GetType();
                        //types3[index5] = type3;
                        //object[] objArray3 = new object[1];
                        //int index6 = 0;
                        //StockReceiveEventArgs stockReceiveEventArgs2 = stockReceiveEventArgs1;
                        //objArray3[index6] = (object) stockReceiveEventArgs2;
                        //scriptObject3.RunMethod(name3, types3, objArray3);
                        str = masterRow["DocNo"].ToString();
                    }
                    DBRegistry dbRegistry = DBRegistry.Create(dbSetting);
                    BaseRegistryID baseRegistryId1 = (BaseRegistryID)new StockReceiveDescriptionID();
                    baseRegistryId1.NewValue = (object)masterRow["Description"].ToString();
                    BaseRegistryID baseRegistryId2 = baseRegistryId1;
                    dbRegistry.SetValue((IRegistryID)baseRegistryId2);
                    //this.PostAuditLog(dataSet, dbSetting, DocumentHelper.DetermineEventType(masterRow));
                    //this.SaveSerialNo(dataSet, dbSetting);
                    //if (masterRow.RowState == DataRowState.Modified)
                    //{
                    //    int num1 = BCE.Data.Convert.TextToBoolean(masterRow["Cancelled", DataRowVersion.Original]) ? 1 : 0;
                    //    bool isCancelled = BCE.Data.Convert.TextToBoolean(masterRow["Cancelled", DataRowVersion.Current]);
                    //    int num2 = isCancelled ? 1 : 0;
                    //    if (num1 != num2)
                    //        this.CancelUncancelSerialNo(dataSet, dbSetting, isCancelled);
                    //}
                    //this.PostReallocatePurchaseByProject(dataSet, dbSetting);
                    // if (this.PostToJE(stockReceive, dataSet, dbSetting, stockReceive.Action, canceldoc))
                    string strwhereDtl = "('-1'";
                    foreach (DataRow drDetail in stockReceive.GetValidDetailRows())
                    {

                        if (drDetail["FromDocDtlKey"] != null && drDetail["FromDocDtlKey"] != DBNull.Value)
                        {
                            if (drDetail["FromDocType"].ToString() == "Main Product")
                                strwhereDtl += ",'" + drDetail["FromDocDtlKey"].ToString() + "'";
                        }
                    }
                     strwhereDtl += ")";
                    if (!PostTrans)
                    {
                        if (masterRow.RowState == DataRowState.Modified)
                        {
                            int num1 = BCE.Data.Convert.TextToBoolean(masterRow["Cancelled", DataRowVersion.Original]) ? 1 : 0;
                            bool isCancelled = BCE.Data.Convert.TextToBoolean(masterRow["Cancelled", DataRowVersion.Current]);
                            int num2 = isCancelled ? 1 : 0;
                            if (num1 != num2)
                            {
                                if (isCancelled)
                                {
                                    string squeryupdateWO = "update RPA_WOProduct SET Status='InProcess' from RPA_WO a with(nolock) inner join RPA_WOProduct b on a.DocKey=b.DocKey where b.Status!='Closed' and DtlKey in " + strwhereDtl + " and DocNo=? and not exists(SELECT 1 FROM RPA_RM  a1 inner join RPA_RMDTL b1 ON a1.DocKey=b1.DocKey where Cancelled='F' and FromDocType='WO' and FromDocDtlKey=b.DtlKey  AND FromDocNo=?)";
                                    dbSetting.ExecuteNonQuery(squeryupdateWO, (object)masterRow["RefDocNo"], (object)masterRow["RefDocNo"]);

                                    // string squeryupdateWOMaster = "update RPA_WO SET Status='InProcess' from RPA_WO a with(nolock) inner join RPA_WOProduct b on a.DocKey=b.DocKey where a.Status!='Closed' and DocNo=? and not exists(SELECT 1 FROM RPA_RM  a1 inner join RPA_RMDTL b1 ON a1.DocKey=b1.DocKey where Cancelled='F' and FromDocType='WO'  AND FromDocNo=?)";
                                    //dbSetting.ExecuteNonQuery(squeryupdateWOMaster, (object)masterRow["RefDocNo"], (object)masterRow["RefDocNo"]);
                                }
                                else
                                {
                                    string squeryupdateWO = "update RPA_WOProduct SET Status='Assembly' from RPA_WO a with(nolock) inner join RPA_WOProduct b on a.DocKey=b.DocKey where b.Status!='Closed' and DocNo=? and DtlKey in " + strwhereDtl;
                                    dbSetting.ExecuteNonQuery(squeryupdateWO, (object)masterRow["RefDocNo"]);
                                    // string squeryupdateWOMaster = "update RPA_WO SET Status='Assembly' from RPA_WO a with(nolock) inner join RPA_WOProduct b on a.DocKey=b.DocKey where b.Status!='Closed' and DocNo=?";
                                    // dbSetting.ExecuteNonQuery(squeryupdateWOMaster, (object)masterRow["RefDocNo"]);
                                }
                            }
                            else
                            {
                                string squeryupdateWO = "update RPA_WOProduct SET Status='Assembly' from RPA_WO a with(nolock) inner join RPA_WOProduct b on a.DocKey=b.DocKey where b.Status!='Closed' and DocNo=? and DtlKey in " + strwhereDtl;
                                dbSetting.ExecuteNonQuery(squeryupdateWO, (object)masterRow["RefDocNo"]);
                                //string squeryupdateWOMaster = "update RPA_WO SET Status='Assembly' from RPA_WO a with(nolock) inner join RPA_WOProduct b on a.DocKey=b.DocKey where b.Status!='Closed' and DocNo=?";
                                //dbSetting.ExecuteNonQuery(squeryupdateWOMaster, (object)masterRow["RefDocNo"]);
                            }
                        }
                        else
                        {
                            string squeryupdateWO = "update RPA_WOProduct SET Status='Assembly' from RPA_WO a with(nolock) inner join RPA_WOProduct b on a.DocKey=b.DocKey  where b.Status!='Closed' and DocNo=? and DtlKey in " + strwhereDtl;
                            dbSetting.ExecuteNonQuery(squeryupdateWO, (object)masterRow["RefDocNo"]);
                            //string squeryupdateWOMaster = "update RPA_WO SET Status='Assembly' from RPA_WO a with(nolock) inner join RPA_WOProduct b on a.DocKey=b.DocKey where b.Status!='Closed' and DocNo=?";
                            //dbSetting.ExecuteNonQuery(squeryupdateWOMaster, (object)masterRow["RefDocNo"]);
                        }
                    }
                    //{
                    if(!PostTrans)
                        this.PostToStockCostingWIP(dataSet, dbSetting);
                    this.PostToSR(stockReceive, dataSet, dbSetting, stockReceive.Action, canceldoc);

                    dbSetting.SimpleSaveDataTable(dataSet.Tables["Master"], "SELECT * FROM RPA_RCV");
                    dbSetting.SimpleSaveDataTable(dataSet.Tables["Detail"], "SELECT * FROM RPA_RCVDtl");
                    //}
                    TempDocument.Delete(dbSetting, BCE.Data.Convert.ToInt64(masterRow["DocKey"]));
                    dbSetting.Commit();
                    if (!PostTrans)
                    {
                        string squeryupdateWOMaster = "update RPA_WO SET Status=dbo.F_RPA_WOStatus(a.DocKey) from RPA_WO a with(nolock) where DocNo=?";
                        myDBSetting.ExecuteNonQuery(squeryupdateWOMaster, (object)masterRow["RefDocNo"]);
                    }
                    //this.PostToStockCosting(dataSet, dbSetting);
                    

                }
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                dbSetting.EndTransaction();
                TransactionControl.EnableTransactionCounter();
            }
            if (flag)
            {
                stockReceive.DisableColumnChanged();
                stockReceive.DocNo = (DBString)str;
                stockReceive.EnableColumnChanged();
            }
            stockReceive.StockReceiveDataSet.AcceptChanges();
            this.UpdateStockUOMConv(stockReceive);
            TransactionControl.IncTransactionCount(this.myDBSetting);
            StockReceiveCommand.DataSetUpdate.Update(this.myDBSetting, dataSet, AsyncDataSetUpdateAction.Update);
            StockReceiveEventArgs stockReceiveEventArgs3 = new StockReceiveEventArgs(stockReceive);
            //ScriptObject scriptObject = stockReceive.ScriptObject;
            //string name = "AfterSave";
            //Type[] types = new Type[1];
            //int index7 = 0;
            //Type type = stockReceiveEventArgs3.GetType();
            //types[index7] = type;
            //object[] objArray = new object[1];
            //int index8 = 0;
            //StockReceiveEventArgs stockReceiveEventArgs4 = stockReceiveEventArgs3;
            //objArray[index8] = (object) stockReceiveEventArgs4;
            //scriptObject.RunMethod(name, types, objArray);
        }

    protected override void DeleteData(long docKey)
    {
      StockReceive doc = this.View(docKey);
      if (doc == null)
      {
        throw new DBConcurrencyException();
      }
      else
      {
        this.myFiscalYear.CheckTransactionDate((DateTime) doc.DocDate, "StockReceive", this.myDBSetting);
                DBSetting dbSetting2 = this.myDBSetting.StartTransaction();
                try
                {
                    string strwhereDtl = "('-1'";
                    foreach (DataRow drDetail in doc.GetValidDetailRows())
                    {

                        if (drDetail["FromDocDtlKey"] != null && drDetail["FromDocDtlKey"] != DBNull.Value)
                        {
                            if (drDetail["FromDocType"].ToString() == "Main Product")
                                strwhereDtl += ",'" + drDetail["FromDocDtlKey"].ToString() + "'";
                        }
                    }
                    strwhereDtl += ")";
                    string sRefDocNo = "";

                    if (doc.RefDocNo != null && doc.RefDocNo != DBNull.Value)
                        sRefDocNo = doc.RefDocNo.ToString();
                    string squeryupdateWO = "update RPA_WOProduct SET Status='InProcess' from RPA_WO a with(nolock) inner join RPA_WOProduct b on a.DocKey=b.DocKey where b.Status!='Closed' and DtlKey in " + strwhereDtl + " and DocNo=? and not exists(SELECT 1 FROM RPA_RM  a1 inner join RPA_RMDTL b1 ON a1.DocKey=b1.DocKey where Cancelled='F' and FromDocType='WO' and FromDocDtlKey=b.DtlKey  AND FromDocNo=?)";
                    dbSetting2.ExecuteNonQuery(squeryupdateWO, (object)doc.RefDocNo.ToString(), (object)sRefDocNo);

                    dbSetting2.ExecuteNonQuery("update JEDTL set RefNo2=null from JEDTL inner join JE on JE.DocKey=JEDTL.DocKey where DocNo=?", (object)doc.DocNo.ToString());
                    dbSetting2.ExecuteNonQuery("update RCV set UDF_JNSTRANS=null from RCVDTL inner join RCV on RCV.DocKey=RCVDTL.DocKey where DocNo=?", (object)doc.DocNo.ToString());

                    dbSetting2.Commit();
                }
                catch (Exception ex)
                {

                    BCE.Application.AppMessage.ShowErrorMessage(ex.Message);
                }
                dbSetting2.EndTransaction();
                DBSetting dbSetting = this.myDBSetting.StartTransaction();
        try
        {
          //StockReceiveBeforeDeleteEventArgs beforeDeleteEventArgs1 = new StockReceiveBeforeDeleteEventArgs(doc, dbSetting);
          //ScriptObject scriptObject1 = doc.ScriptObject;
          //string name1 = "BeforeDelete";
          //Type[] types1 = new Type[1];
          //int index1 = 0;
          //Type type1 = beforeDeleteEventArgs1.GetType();
          //types1[index1] = type1;
          //object[] objArray1 = new object[1];
          //int index2 = 0;
          //StockReceiveBeforeDeleteEventArgs beforeDeleteEventArgs2 = beforeDeleteEventArgs1;
          //objArray1[index2] = (object) beforeDeleteEventArgs2;
          //scriptObject1.RunMethod(name1, types1, objArray1);
          //if (beforeDeleteEventArgs1.ErrorMessage != null && beforeDeleteEventArgs1.ErrorMessage.Length > 0)
          //  throw new StandardApplicationException(beforeDeleteEventArgs1.ErrorMessage);
          //else if (beforeDeleteEventArgs1.myAbort)
          //{
          //  throw new StandardApplicationException("");
          //}
          //else
          {
                        Int64 iDocKey = 0;
                        object obj = myDBSetting.ExecuteScalar("select DocKey from RCV where DocNo=?", (object)doc.DocNo.ToString());

                        if (obj != null && obj != DBNull.Value)
                        {
                            iDocKey = BCE.Data.Convert.ToInt64(obj);
                        }


                        DataSet stockReceiveDataSet = doc.StockReceiveDataSet;
                        this.PostAuditLog(stockReceiveDataSet, dbSetting, AuditTrail.EventType.Delete);
                        BCE.AutoCount.Stock.StockReceive.StockReceiveCommand srCommand = BCE.AutoCount.Stock.StockReceive.StockReceiveCommand.Create(myDBSetting);
                        srCommand.Delete(iDocKey);
                        this.DeleteSerialNo(stockReceiveDataSet, dbSetting);
                       // this.DeleteFromStockCosting(docKey, dbSetting);
                        this.DeleteFromStockCostingWIP(docKey, dbSetting);
                        //this.DeleteReallocatePurchaseByProject(BCE.Data.Convert.ToInt64(stockReceiveDataSet.Tables["Master"].Rows[0]["ReallocatePurchaseByProjectJEDocKey"]), dbSetting);
                        SqlCommand command1 = dbSetting.CreateCommand("DELETE FROM RPA_RCV WHERE DocKey=@DocKey", new object[0]);
                        command1.Parameters.AddWithValue("@DocKey", (object)docKey);
                        command1.ExecuteNonQuery();
                        SqlCommand command2 = dbSetting.CreateCommand("DELETE FROM RPA_RCVDtl WHERE DocKey=@DocKey", new object[0]);
                        command2.Parameters.AddWithValue("@DocKey", (object)docKey);
                        command2.ExecuteNonQuery();
                      
                       
                        //StockReceiveOnDeleteEventArgs onDeleteEventArgs1 = new StockReceiveOnDeleteEventArgs(doc, dbSetting);
                        //ScriptObject scriptObject2 = doc.ScriptObject;
                        //string name2 = "OnDelete";
                        //Type[] types2 = new Type[1];
                        //int index3 = 0;
                        //Type type2 = onDeleteEventArgs1.GetType();
                        //types2[index3] = type2;
                        //object[] objArray2 = new object[1];
                        //int index4 = 0;
                        //StockReceiveOnDeleteEventArgs onDeleteEventArgs2 = onDeleteEventArgs1;
                        //objArray2[index4] = (object) onDeleteEventArgs2;
                        //scriptObject2.RunMethod(name2, types2, objArray2);
                        dbSetting.Commit();
                       
                    }
        }
        catch (SqlException ex)
        {
          BCE.Data.DataError.HandleSqlException(ex);
        }
        finally
        {
          dbSetting.EndTransaction();
        }
        StockReceiveCommand.DataSetUpdate.Update(this.myDBSetting, doc.StockReceiveDataSet, AsyncDataSetUpdateAction.Delete);
        //StockReceiveEventArgs stockReceiveEventArgs1 = new StockReceiveEventArgs(doc);
        //ScriptObject scriptObject = doc.ScriptObject;
        //string name = "AfterDelete";
        //Type[] types = new Type[1];
        //int index5 = 0;
        //Type type = stockReceiveEventArgs1.GetType();
        //types[index5] = type;
        //object[] objArray = new object[1];
        //int index6 = 0;
        //StockReceiveEventArgs stockReceiveEventArgs2 = stockReceiveEventArgs1;
        //objArray[index6] = (object) stockReceiveEventArgs2;
        //scriptObject.RunMethod(name, types, objArray);
      }
    }

    public override void SaveDocumentInfoTable(DataTable table)
    {
      try
      {
        string documentInfoColumns = this.GetDocumentInfoColumns();
        this.myDBSetting.SimpleSaveDataTable(table, string.Format("SELECT {0} FROM RPA_RCV", (object) documentInfoColumns));
      }
      catch (DBConcurrencyException ex)
      {
        throw new StandardApplicationException(Localizer.GetString((Enum) StockReceiveStringId.InfoMessage_SaveAborted, new object[0]));
      }
    }

    public override int InquireAllMaster(string columnSQL, bool hasYearMonth)
    {
      SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        connection.Open();
        string str1 = SQLHelper.BuildFilteredByUserSQL(this.myDBSetting, "RPA_RCV", "WHERE");
        SqlCommand selectCommand = new SqlCommand(string.Format("SELECT {0} FROM RPA_RCV {1} order by DocDate Desc", (object) columnSQL, (object) str1), connection);
        int commandTimeOut = this.myDBSetting.CommandTimeOut;
        selectCommand.CommandTimeout = commandTimeOut;
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
        this.myDataTableAllMaster.Clear();
        DataTable dataTable1 = this.myDataTableAllMaster;
        sqlDataAdapter.Fill(dataTable1);
        if (this.myDataTableAllMaster.PrimaryKey.Length == 0)
        {
          DataTable dataTable2 = this.myDataTableAllMaster;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index = 0;
          DataColumn dataColumn = this.myDataTableAllMaster.Columns["DocKey"];
          dataColumnArray[index] = dataColumn;
          dataTable2.PrimaryKey = dataColumnArray;
        }
        if (hasYearMonth)
        {
          if (this.myDataTableAllMaster.Columns.IndexOf("YearMonth") < 0)
            this.myDataTableAllMaster.Columns.Add("YearMonth", typeof (string));
          if (this.myDataTableAllMaster.Columns.IndexOf("Year") < 0)
            this.myDataTableAllMaster.Columns.Add("Year", typeof (string));
          foreach (DataRow dataRow1 in (InternalDataCollectionBase) this.myDataTableAllMaster.Rows)
          {
            dataRow1.BeginEdit();
            DataRow dataRow2 = dataRow1;
            string index1 = "YearMonth";
            DateTime dateTime = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            string str2 = dateTime.ToString("yyyy.MM");
            dataRow2[index1] = (object) str2;
            DataRow dataRow3 = dataRow1;
            string index2 = "Year";
            dateTime = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            string str3 = dateTime.ToString("yyyy");
            dataRow3[index2] = (object) str3;
            dataRow1.EndEdit();
          }
          this.myDataTableAllMaster.AcceptChanges();
        }
        return this.myDataTableAllMaster.Rows.Count;
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        connection.Close();
        connection.Dispose();
      }
    }

    public override int SearchMaster(SearchCriteria criteria, string columnSQL, DataTable resultTable, string multiSelectColumnName)
    {
      string str1 = columnSQL;
      char[] chArray = new char[1];
      int index1 = 0;
      int num1 = 44;
      chArray[index1] = (char) num1;
      string[] strArray = str1.Split(chArray);
      StringBuilder stringBuilder = new StringBuilder();
      foreach (string str2 in strArray)
      {
        stringBuilder.Append("A.");
        stringBuilder.Append(str2);
        stringBuilder.Append(",");
      }
      string str3 = ((object) stringBuilder).ToString();
      if (str3.EndsWith(","))
        str3 = str3.Remove(str3.Length - 1, 1);
      columnSQL = str3.Trim();
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        sqlConnection.Open();
        SqlCommand sqlCommand1 = new SqlCommand();
        sqlCommand1.Connection = sqlConnection;
        sqlCommand1.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
        string str2 = criteria.BuildSQL((IDbCommand) sqlCommand1);
        if (criteria is StockReceiveCriteria)
        {
          StockReceiveCriteria stockReceiveCriteria = criteria as StockReceiveCriteria;
          //if (stockReceiveCriteria.UDF.Length > 0)
          //{
          //  string str4 = new UDFUtil(this.myDBSetting).GenerateUDFSearchString(sqlCommand1, "RPA_RCV", "A", stockReceiveCriteria.UDF);
          //  if (str4.Trim().Length > 0)
          //    str2 = str2.Length != 0 ? str2 + " OR " + str4 : str4;
          //}
          //if (stockReceiveCriteria.DetailUDF.Length > 0)
          //{
          //  string str4 = new UDFUtil(this.myDBSetting).GenerateUDFSearchString(sqlCommand1, "RPA_RCVDtl", "B", stockReceiveCriteria.DetailUDF);
          //  if (str4.Trim().Length > 0)
          //    str2 = str2.Length != 0 ? str2 + " OR " + str4 : str4;
          //}
        }
        if (str2.IndexOf("B.") >= 0)
        {
          sqlCommand1.CommandText = string.Format("SELECT DISTINCT {0} FROM RPA_RCV A, RPA_RCVDtl B WHERE (A.DocKey=B.DocKey)", (object) columnSQL);
          if (str2.Length > 0)
            sqlCommand1.CommandText = sqlCommand1.CommandText + " AND (" + str2 + ")";
        }
        else
        {
          sqlCommand1.CommandText = string.Format("SELECT {0} FROM RPA_RCV A", (object) columnSQL);
          if (str2.Length > 0)
            sqlCommand1.CommandText = sqlCommand1.CommandText + " WHERE (" + str2 + ")";
        }
        string str5 = SQLHelper.BuildFilteredByUserSQL(this.myDBSetting, "A");
        if (str5.Length > 0)
        {
          if (str2.IndexOf("B.") >= 0)
          {
            SqlCommand sqlCommand2 = sqlCommand1;
            string str4 = sqlCommand2.CommandText + " AND " + str5;
            sqlCommand2.CommandText = str4;
          }
          else if (str2.Length > 0)
          {
            SqlCommand sqlCommand2 = sqlCommand1;
            string str4 = sqlCommand2.CommandText + " AND " + str5;
            sqlCommand2.CommandText = str4;
          }
          else
          {
            SqlCommand sqlCommand2 = sqlCommand1;
            string str4 = sqlCommand2.CommandText + " WHERE " + str5;
            sqlCommand2.CommandText = str4;
          }
        }
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand1);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
          resultTable.Clear();
        else if (multiSelectColumnName.Length > 0 && resultTable.Rows.Count > 0 && resultTable.Columns[multiSelectColumnName] != null)
        {
          DataRow[] dataRowArray = resultTable.Select(string.Format("{0} = true", (object) multiSelectColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index2 = 0; index2 < dataRowArray.Length; ++index2)
            numArray[index2] = BCE.Data.Convert.ToInt64(dataRowArray[index2]["DocKey"]);
        }
        sqlDataAdapter.Fill(resultTable);
        if (resultTable.PrimaryKey.Length == 0)
        {
          DataTable dataTable = resultTable;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index2 = 0;
          DataColumn dataColumn = resultTable.Columns["DocKey"];
          dataColumnArray[index2] = dataColumn;
          dataTable.PrimaryKey = dataColumnArray;
        }
        if (numArray != null)
        {
          foreach (long num2 in numArray)
          {
            DataRow dataRow = resultTable.Rows.Find((object) num2);
            if (dataRow != null)
              dataRow[multiSelectColumnName] = (object) true;
          }
        }
        return resultTable.Rows.Count;
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }

    protected override DataSet LoadDesignReportData()
    {
      SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        DataSet dataSet = new DataSet();
        connection.Open();
        new SqlDataAdapter(new SqlCommand("SELECT TOP 5 * FROM vRPA_StockReceive ORDER BY DocKey", connection)).Fill(dataSet, "Master");
        new SqlDataAdapter(new SqlCommand("SELECT * FROM vRPA_StockReceiveDetail WHERE DocKey IN (SELECT TOP 5 DocKey FROM vRPA_StockReceive ORDER BY DocKey) AND PrintOut='T' ORDER BY DocKey, Seq", connection)).Fill(dataSet, "Detail");
        return dataSet;
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        connection.Close();
        connection.Dispose();
      }
    }

    protected override DataSet LoadReportData(long docKey)
    {
      SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        DataSet dataSet = new DataSet();
        connection.Open();
        SqlCommand selectCommand1 = new SqlCommand("SELECT * FROM vRPA_StockReceive WHERE DocKey=@DocKey", connection);
        selectCommand1.Parameters.AddWithValue("@DocKey", (object) docKey);
        new SqlDataAdapter(selectCommand1).Fill(dataSet, "Master");
        SqlCommand selectCommand2 = new SqlCommand("SELECT * FROM vRPA_StockReceiveDetail WHERE DocKey=@DocKey AND PrintOut='T' ORDER BY Seq", connection);
        selectCommand2.Parameters.AddWithValue("@DocKey", (object) docKey);
        new SqlDataAdapter(selectCommand2).Fill(dataSet, "Detail");
        return dataSet;
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        connection.Close();
        connection.Dispose();
      }
    }

    public override void DocumentListingBasicSearch(StockReceiveReportingCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
    {
      string str1 = columnName;
      char[] chArray = new char[1];
      int index1 = 0;
      int num1 = 44;
      chArray[index1] = (char) num1;
      string[] strArray1 = str1.Split(chArray);
      columnName = string.Empty;
      foreach (string oldValue in strArray1)
      {
        bool flag = false;
        //foreach (UDFColumn udfColumn in new UDFUtil(this.myDBSetting).GetUDF("RPA_RCV"))
        //{
        //  if ("RPA_RCV" + udfColumn.FieldName == oldValue)
        //  {
        //    string[] strArray2 = new string[6];
        //    int index2 = 0;
        //    string str2 = columnName;
        //    strArray2[index2] = str2;
        //    int index3 = 1;
        //    string str3 = "RPA_RCV.";
        //    strArray2[index3] = str3;
        //    int index4 = 2;
        //    string fieldName1 = udfColumn.FieldName;
        //    strArray2[index4] = fieldName1;
        //    int index5 = 3;
        //    string str4 = " AS RPA_RCV";
        //    strArray2[index5] = str4;
        //    int index6 = 4;
        //    string fieldName2 = udfColumn.FieldName;
        //    strArray2[index6] = fieldName2;
        //    int index7 = 5;
        //    string str5 = ",";
        //    strArray2[index7] = str5;
        //    columnName = string.Concat(strArray2);
        //    flag = true;
        //    break;
        //  }
        //}
        if (!flag)
          columnName = columnName + oldValue.Replace(oldValue, "RPA_RCV." + oldValue + ",");
      }
      if (columnName.EndsWith(","))
        columnName = columnName.Remove(columnName.Length - 1, 1);
      columnName = columnName.Trim();
      string str6 = string.Empty;
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = sqlConnection;
        string whereSql = this.GetWhereSQL(criteria, sqlCommand);
        string str2 = !(whereSql == string.Empty) ? string.Format(" SELECT DISTINCT {0} FROM RPA_RCV LEFT OUTER JOIN RPA_RCVDtl ON (RPA_RCV.DocKey=RPA_RCVDtl.DocKey) WHERE {1}", (object) columnName, (object) whereSql) : string.Format("Select DISTINCT {0}, DocKey From RPA_RCV Where (1=1)", (object) columnName);
        string str3 = SQLHelper.BuildFilteredByUserSQL(this.myDBSetting, "RPA_RCV");
        if (str3.Length > 0)
          str2 = str2 + " AND " + str3;
        sqlCommand.CommandText = str2;
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
          resultDataTable.Clear();
        else if (checkEditColumnName.Length > 0 && resultDataTable.Rows.Count > 0)
        {
          DataRow[] dataRowArray = resultDataTable.Select(string.Format("{0} = true", (object) checkEditColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index2 = 0; index2 < dataRowArray.Length; ++index2)
            numArray[index2] = BCE.Data.Convert.ToInt64(dataRowArray[index2]["DocKey"]);
        }
        sqlConnection.Open();
        sqlDataAdapter.Fill(resultDataTable);
        if (resultDataTable.PrimaryKey.Length == 0)
        {
          DataTable dataTable = resultDataTable;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index2 = 0;
          DataColumn dataColumn = resultDataTable.Columns["DocKey"];
          dataColumnArray[index2] = dataColumn;
          dataTable.PrimaryKey = dataColumnArray;
        }
        resultDataTable.DefaultView.Sort = criteria.SortBy != ListingSortByOption.DocumentNo ? "DocDate" : "DocNo";
        if (numArray != null)
        {
          foreach (long num2 in numArray)
          {
            DataRow dataRow = resultDataTable.Rows.Find((object) num2);
            if (dataRow != null)
              dataRow[checkEditColumnName] = (object) true;
          }
        }
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }

    protected string GetWhereSQL(StockReceiveReportingCriteria reportingCriteria, SqlCommand cmd)
    {
      string str1 = "";
      SearchCriteria searchCriteria = new SearchCriteria();
      BCE.AutoCount.SearchFilter.Filter dateFilter = reportingCriteria.DateFilter;
      searchCriteria.AddFilter(dateFilter);
      BCE.AutoCount.SearchFilter.Filter documentFilter = reportingCriteria.DocumentFilter;
      searchCriteria.AddFilter(documentFilter);
      int num = 1;
      searchCriteria.MatchAll = num != 0;
      SqlCommand sqlCommand = cmd;
      string str2 = searchCriteria.BuildSQL((IDbCommand) sqlCommand);
      if (str2.Length > 0)
        str1 = str2;
      if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.UnCancelled)
        str1 = str1.Length <= 0 ? " RPA_RCV.Cancelled = 'F' " : str1 + " And RPA_RCV.Cancelled = 'F' ";
      else if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
        str1 = str1.Length <= 0 ? " RPA_RCV.Cancelled = 'T' " : str1 + " And RPA_RCV.Cancelled = 'T' ";
      return str1;
    }

    public override void BasicSearch(StockReceiveCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
    {
      string str1 = columnName;
      char[] chArray = new char[1];
      int index1 = 0;
      int num1 = 44;
      chArray[index1] = (char) num1;
      string[] strArray = str1.Split(chArray);
      columnName = string.Empty;
      foreach (string oldValue in strArray)
        columnName = columnName + oldValue.Replace(oldValue, "RPA_RCV." + oldValue + ",");
      if (columnName.EndsWith(","))
        columnName = columnName.Remove(columnName.Length - 1, 1);
      columnName = columnName.Trim();
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = sqlConnection;
        criteria.MatchAll = false;
        string str2 = criteria.BuildSQL((IDbCommand) sqlCommand);
        //if (criteria.UDF.Length > 0)
        //{
        //  string str3 = new UDFUtil(this.myDBSetting).GenerateUDFSearchString(sqlCommand, "RPA_RCV", "RPA_RCV", criteria.UDF);
        //  if (str3.Trim().Length > 0)
        //    str2 = str2.Length != 0 ? str2 + " OR " + str3 : str3;
        //}
        //if (criteria.DetailUDF.Length > 0)
        //{
        //  string str3 = new UDFUtil(this.myDBSetting).GenerateUDFSearchString(sqlCommand, "RPA_RCVDtl", "RPA_RCVDtl", criteria.DetailUDF);
        //  if (str3.Trim().Length > 0)
        //    str2 = str2.Length != 0 ? str2 + " OR " + str3 : str3;
        //}
        string str4 = string.Empty;
        string str5 = !(str2 == string.Empty) ? string.Format(" SELECT {0} FROM RPA_RCV WHERE DocKey In (SELECT DISTINCT RPA_RCV.DocKey FROM RPA_RCV INNER JOIN RPA_RCVDtl ON RPA_RCV.DocKey = RPA_RCVDtl.DocKey WHERE {1})", (object) columnName, (object) str2) : string.Format("Select DISTINCT {0}, DocKey From RPA_RCV Where (1=1)", (object) columnName);
        string str6 = SQLHelper.BuildFilteredByUserSQL(this.myDBSetting, "RPA_RCV");
        if (str6.Length > 0)
          str5 = str5 + " AND " + str6;
        sqlCommand.CommandText = str5;
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
          resultDataTable.Clear();
        else if (checkEditColumnName.Length > 0 && resultDataTable.Rows.Count > 0)
        {
          DataRow[] dataRowArray = resultDataTable.Select(string.Format("{0} = true", (object) checkEditColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index2 = 0; index2 < dataRowArray.Length; ++index2)
            numArray[index2] = BCE.Data.Convert.ToInt64(dataRowArray[index2]["DocKey"]);
        }
        sqlConnection.Open();
        sqlDataAdapter.Fill(resultDataTable);
        if (resultDataTable.PrimaryKey.Length == 0)
        {
          DataTable dataTable = resultDataTable;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index2 = 0;
          DataColumn dataColumn = resultDataTable.Columns["DocKey"];
          dataColumnArray[index2] = dataColumn;
          dataTable.PrimaryKey = dataColumnArray;
        }
        if (numArray != null)
        {
          foreach (long num2 in numArray)
          {
            DataRow dataRow = resultDataTable.Rows.Find((object) num2);
            if (dataRow != null)
              dataRow[checkEditColumnName] = (object) true;
          }
        }
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }

    public override void AdvanceSearch(AdvancedStockReceiveCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
    {
      string str1 = columnName;
      char[] chArray = new char[1];
      int index1 = 0;
      int num1 = 44;
      chArray[index1] = (char) num1;
      string[] strArray1 = str1.Split(chArray);
      columnName = string.Empty;
      foreach (string oldValue in strArray1)
      {
        bool flag = false;
        //foreach (UDFColumn udfColumn in new UDFUtil(this.myDBSetting).GetUDF("RPA_RCV"))
        //{
        //  if ("RPA_RCV" + udfColumn.FieldName == oldValue)
        //  {
        //    string[] strArray2 = new string[6];
        //    int index2 = 0;
        //    string str2 = columnName;
        //    strArray2[index2] = str2;
        //    int index3 = 1;
        //    string str3 = "A.";
        //    strArray2[index3] = str3;
        //    int index4 = 2;
        //    string fieldName1 = udfColumn.FieldName;
        //    strArray2[index4] = fieldName1;
        //    int index5 = 3;
        //    string str4 = " AS RPA_RCV";
        //    strArray2[index5] = str4;
        //    int index6 = 4;
        //    string fieldName2 = udfColumn.FieldName;
        //    strArray2[index6] = fieldName2;
        //    int index7 = 5;
        //    string str5 = ",";
        //    strArray2[index7] = str5;
        //    columnName = string.Concat(strArray2);
        //    flag = true;
        //    break;
        //  }
        //}
        if (!flag)
          columnName = columnName + oldValue.Replace(oldValue, "A." + oldValue + ",");
      }
      if (columnName.EndsWith(","))
        columnName = columnName.Remove(columnName.Length - 1, 1);
      columnName = columnName.Trim();
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        SqlCommand selectCommand = new SqlCommand();
        selectCommand.Connection = sqlConnection;
        string str2 = criteria.BuildSQL((IDbCommand) selectCommand);
        string str3 = string.Empty;
        string str4 = !(str2 != string.Empty) ? string.Format("Select DISTINCT {0} From RPA_RCV A, RPA_RCVDtl B where A.DocKey = B.DocKey ", (object) columnName) : string.Format("Select DISTINCT {0} From RPA_RCV A, RPA_RCVDtl B where A.DocKey = B.DocKey and {1}", (object) columnName, (object) str2);
        string str5 = SQLHelper.BuildFilteredByUserSQL(this.myDBSetting, "A");
        if (str5.Length > 0)
          str4 = str4 + " AND " + str5;
        selectCommand.CommandText = str4;
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
          resultDataTable.Clear();
        else if (checkEditColumnName.Length > 0 && resultDataTable.Rows.Count > 0)
        {
          DataRow[] dataRowArray = resultDataTable.Select(string.Format("{0} = true", (object) checkEditColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index2 = 0; index2 < dataRowArray.Length; ++index2)
            numArray[index2] = BCE.Data.Convert.ToInt64(dataRowArray[index2]["DocKey"]);
        }
        sqlConnection.Open();
        sqlDataAdapter.Fill(resultDataTable);
        if (resultDataTable.PrimaryKey.Length == 0)
        {
          DataTable dataTable = resultDataTable;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index2 = 0;
          DataColumn dataColumn = resultDataTable.Columns["DocKey"];
          dataColumnArray[index2] = dataColumn;
          dataTable.PrimaryKey = dataColumnArray;
        }
        if (numArray != null)
        {
          foreach (long num2 in numArray)
          {
            DataRow dataRow = resultDataTable.Rows.Find((object) num2);
            if (dataRow != null)
              dataRow[checkEditColumnName] = (object) true;
          }
        }
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }

    protected override DataSet LoadDocumentListingReportDesignerData()
    {
      SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        DataSet dataSet = new DataSet();
        connection.Open();
        new SqlDataAdapter(new SqlCommand("SELECT TOP 100 * FROM vRPA_StockReceive ORDER BY DocKey", connection)).Fill(dataSet, "Master");
        new SqlDataAdapter(new SqlCommand("SELECT * FROM vRPA_StockReceiveDetail WHERE DocKey IN (SELECT TOP 100 DocKey FROM vRPA_StockReceive ORDER BY DocKey) ORDER BY DocKey, Seq", connection)).Fill(dataSet, "Detail");
        return dataSet;
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        connection.Close();
        connection.Dispose();
      }
    }

    protected override DataSet LoadDocumentListingReportData(string docKeys)
    {
      SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
      string cmdText1 = string.Format("SELECT * FROM vRPA_StockReceive WHERE DocKey IN ({0}) ", (object) docKeys);
      string cmdText2 = string.Format("SELECT * FROM vRPA_StockReceiveDetail WHERE DocKey IN ({0}) ORDER BY Seq", (object) docKeys);
      try
      {
        DataSet dataSet = new DataSet();
        connection.Open();
        new SqlDataAdapter(new SqlCommand(cmdText1, connection)).Fill(dataSet, "Master");
        new SqlDataAdapter(new SqlCommand(cmdText2, connection)).Fill(dataSet, "Detail");
        return dataSet;
      }
      catch (SqlException ex)
      {
        BCE.Data.DataError.HandleSqlException(ex);
        throw;
      }
      finally
      {
        connection.Close();
        connection.Dispose();
      }
    }

    private string GetDocumentInfoColumns()
    {
      string str = "DocKey, DocNo, DocDate, Description, Total, RefDocNo, Note, Remark1, Remark2, Remark3, Remark4";
      //foreach (UDFColumn udfColumn in new UDFUtil(this.myDBSetting).GetUDF("RPA_RCV"))
      //  str = str + ", " + udfColumn.FieldName;
      return str;
    }

    public override DataTable GetDocumentInfoTable(long[] docKeys)
    {
      string documentInfoColumns = this.GetDocumentInfoColumns();
      if (docKeys == null)
      {
        return this.myDBSetting.GetDataTable(string.Format("SELECT {0} FROM RPA_RCV", (object) documentInfoColumns), true, new object[0]);
      }
      else
      {
        string str1 = StringHelper.ArrayListToCommaString(new ArrayList((ICollection) docKeys));
        string str2 = string.Format("SELECT {0} FROM RPA_RCV WHERE DocKey IN (SELECT * FROM List(?))", (object) documentInfoColumns);
        DBSetting dbSetting = this.myDBSetting;
        string cmdText = str2;
        int num = 1;
        object[] objArray = new object[1];
        int index = 0;
        string str3 = str1;
        objArray[index] = (object) str3;
        return dbSetting.GetDataTable(cmdText, num != 0, objArray);
      }
    }
  }
}
