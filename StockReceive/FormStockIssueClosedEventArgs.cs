﻿// Type: BCE.AutoCount.Stock.StockReceive.FormClosedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.Stock.dll

using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;

namespace RPASystem.StockReceive
{
    [Obsolete("Use FormStockReceiveEntry.FormStockReceiveClosedEventArgs instead")]
    public class FormStockReceiveClosedEventArgs : FormStockReceiveEventArgs
    {
        public FormStockReceiveClosedEventArgs(StockReceive doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
            : base(doc, headerPanel, gridControl, tabControl)
        {
        }
    }
}
