﻿
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;

namespace Production.StockReceive
{
    [Obsolete("Use FormStockReceiveEntry.FormClosedEventArgs instead")]
    public class FormClosedEventArgs : FormStockReceiveEventArgs
    {
        public FormClosedEventArgs(StockReceive doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
          : base(doc, headerPanel, gridControl, tabControl)
        {
        }
    }
}
