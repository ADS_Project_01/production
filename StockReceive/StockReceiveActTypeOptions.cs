﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BCE.Localization;
namespace Production.StockReceive
{
    [LocalizableString]
    public enum StockReceiveActTypeOptions
    {
        [DefaultString("FRESH")]
        FRESH,
        [DefaultString("FROZEN")]
        FROZEN,
    }
}
