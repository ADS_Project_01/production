﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveString
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Localization;

namespace Production.StockReceive
{
  [LocalizableString]
  public enum StockReceiveString
  {
    [DefaultString("Post from Stock Receive FG: {0}")] PostFromStockReceive,
    [DefaultString("{0} opened Stock Receive FG window.")] OpenedStockReceiveWindow,
    [DefaultString("Stock Receive FG {0}")] StockReceive,
    [DefaultString("{0} viewed Stock Receive FG {1} in edit mode.")] ViewStockReceiveInEditMode,
    [DefaultString("{0} viewed Stock Receive FG {1}.")] ViewStockReceive,
    [DefaultString("{0} cancels Stock Receive FG {1}.")] CancelStockReceive,
    [DefaultString("{0} un-cancels Stock Receive FG {1}.")] UncancelStockReceive,
    [DefaultString("{0} edited Stock Receive FG {1}.")] EditedStockReceive,
    [DefaultString("{0} opened Print Stock Receive FG Detail Listing window.")] OpenedPrintStockReceiveDetailListingWindow,
    [DefaultString("{0} inquired on Print Stock Receive FG Detail Listing.")] InquiredPrintStockReceiveDetailListing,
    [DefaultString("RPA Stock Receive FG Detail FG Listing")] StockReceiveDetailListing,
    [DefaultString("{0} opened Print Stock Receive FG Listing window.")] OpenedPrintStockReceiveListingWindow,
    [DefaultString("{0} inquired on Print Stock Receive FG Listing.")] InquiredPrintStockReceiveListing,
    [DefaultString("Batch Stock Receive FG")] BatchPrintStockReceive,
    [DefaultString("RPA Stock Receive FG FG Listing")] PrintStockReceiveListing,
    [DefaultString("{0} created new Stock Receive FG {1}.")] CreateNewStockReceive,
    [DefaultString("{0} updated Stock Receive FG {1}.")] UpdatedStockReceive,
    [DefaultString("{0} deleted Stock Receive FG {1}.")] DeletedStockReceive,
    [DefaultString("Batch Print Stock Receive FG / Print Stock Receive FG Listing")] BatchPrintStockReceiveOrStockReceiveListing,
    [DefaultString("Print Stock Receive FG Detail Listing")] PrintStockReceiveDetailListing,
  }
}
