﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveDetailDeletedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveDetailDeletedEventArgs : StockReceiveEventArgs
  {
    internal StockReceiveDetailDeletedEventArgs(StockReceive doc)
      : base(doc)
    {
    }
  }
}
