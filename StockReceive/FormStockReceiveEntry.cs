﻿// Type: BCE.AutoCount.Stock.StockReceive.FormStockReceiveEntry
// Assembly: BCE.AutoCount.Stock, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.Document;
using BCE.AutoCount.DragDrop;
using BCE.AutoCount.Help;
using BCE.AutoCount.Inquiry;
using BCE.AutoCount.Inquiry.UserControls;
using BCE.AutoCount.Invoicing;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.RegistryID.BarCode;
using BCE.AutoCount.RegistryID.Misc;
using BCE.AutoCount.Report;
//using BCE.AutoCount.Scripting;
using BCE.AutoCount.SerialNumber2;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.AutoCount.Stock.Item;
using BCE.AutoCount.Stock.StockUOMConversion;
using BCE.AutoCount.SearchFilter;
//using BCE.AutoCount.UDF;
using BCE.AutoCount.WinForms;
using BCE.AutoCount.XtraUtils;
using BCE.Controls;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using BCE.AutoCount.Stock.StockReceive;
using System.Threading;
using System.Data.SqlClient;
using Production.Tools.LookupEditBuilders;
using BCE.AutoCount.Common;
namespace Production.StockReceive
{
    public class FormStockReceiveEntry : XtraForm
    {
        private bool myIsInstantInfoShown;
        protected internal GeneralSetting myGeneralSetting;
        private DBRegistry myDBReg;
        private bool myUseLookupEditToInputItemCode;
        private bool mySkipExecuteFormActivated;
        private bool myLastValidationSuccess;
        private int myInstantInfoHeight;
        private int myFooterHeight;
        private StockReceive myStockReceive;
        private DBSetting myDBSetting;
        private int myColLocationVisibleIndex;
        private int myColBatchNoVisibleIndex;
        private int myColProjNoVisibleIndex;
        private int myColDeptNoVisibleIndex;
        private DataSet myLastDragDataSet;
        private UndoManager myMasterRecordUndo;
        private UndoManager myDetailRecordUndo;
        private DocumentCarrier myDropDocCarrier;
        private bool myItemCodeFromPopup;
        private bool myCanDrag;
        private bool myHasDeactivated;
        private CustomizeGridLayout myGridLayout;
        private int myInProcessTempDocument;
        private bool mySaveInKIV;
        private BCE.AutoCount.XtraUtils.MRUHelper myMRUHelper;
        private BCE.AutoCount.XtraUtils.MRUHelper myMRUHelperForRemark1;
        private BCE.AutoCount.XtraUtils.MRUHelper myMRUHelperForRemark2;
        private BCE.AutoCount.XtraUtils.MRUHelper myMRUHelperForRemark3;
        private BCE.AutoCount.XtraUtils.MRUHelper myMRUHelperForRemark4;
        private RemarkName myRemarkName;
        private RemarkNameEntity myRemarkNameEntity;
        private bool myFilterByLocation;
       // private ScriptObject myScriptObject;
        private bool myIsBeforeStartDate;
        private int myDisableAutoSaveCounter;
        private bool myIsAutomaticMergeSameItemCode;
        private FormItemSearch myFormItemSearch;
        protected DecimalSetting myDecimalSetting;
        protected UserAuthentication myUserAuthentication;
        private FormStockReceiveEditOption myEditOption;
        private IContainer components;
        private bool myHasUnlinkLookupEditEventHandlers;
        private DateEdit dateEdtDate;
        private Label lblRefDocNo;
        private Label lblStockAdjNo;
        private Label lblDate;
        private Label lblDescription;
        private PanelControl panel2;
        private SimpleButton sbtnCancel;
        private XtraTabControl tabControl1;
        private XtraTabPage tabPageMain;
        private GridControl myGridControl;
        private XtraTabPage tabPageNote;
        private XtraTabPage tabPageExternalLink;
        private CheckEdit chkedtNextRecord;
        private PanelControl panel5;
        private BCE.Controls.MemoEdit memoEdtNote;
        private ExternalLinkBox externalLinkBox1;
        private Label lblCancelled;
        private Label lblTotal;
        private TextEdit textEdtTotal;
        private PanelControl panelHeader;
        private SimpleButton sbtnItemSearch;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private ImageList imageList1;
        private SimpleButton sbtnRangeSet;
        private MRUEdit mruEdtDescription;
        private LookUpEdit luEdtDocNoFormat;
        private ScanBarcodeControl scanBarcodeControl1;
        private PanelControl panelControl1;
        private TextEdit txtEdtStockReceiveNo;
        private UCInquiryStock ucInquiryStock;
        private SimpleButton sbtnShowInstantInfo;
        private PanelControl panelBottom;
        private SimpleButton sbtnCancelDoc;
        private SimpleButton sbtnDelete;
        private SimpleButton sbtnEdit;
        private SimpleButton sbtnSavePreview;
        private SimpleButton sbtnSave;
        private SimpleButton sbtnSavePrint;
        private System.Windows.Forms.Timer timer1;
        private Navigator navigator;
        private SimpleButton sbtnSerialNo;
        private PreviewButton btnPreview;
        private PrintButton btnPrint;
        private HyperLinkEdit hylinkEditUpdateRealCost;
        private HyperLinkEdit hylinkEditUpdateReferenceCost;
        private SplitterControl splitterControl1;
        private Label labelRemark4;
        private Label labelRemark3;
        private Label labelRemark2;
        private Label labelRemark1;
        private MRUEdit mruEdtRemark1;
        private MRUEdit mruEdtRemark2;
        private MRUEdit mruEdtRemark3;
        private MRUEdit mruEdtRemark4;
        private XtraTabPage tabPageMoreHeader;
        private GridView gridViewStockDetail;
        private GridColumn colSeq;
        private GridColumn colNumbering;
        private GridColumn colItemCode;
        private GridColumn colLocation;
        private GridColumn colDescription;
        private GridColumn colProjNo;
        private GridColumn colDeptNo;
        private GridColumn colQty;
        private GridColumn colUOM;
        private GridColumn colUnitCost;
        private GridColumn colSubTotal;
        private GridColumn colPrintOut;
        private RepositoryItemLookUpEdit repItemLkEdt_ItemCode;
        private RepositoryItemLookUpEdit repItemLkEdt_Location;
        private RepositoryItemLookUpEdit repItemLkEdt_ProjNo;
        private RepositoryItemLookUpEdit repItemLkEdt_Dept;
        private RepositoryItemLookUpEdit repItemLkEdt_ItemUOM;
        private GridColumn colBatchNo;
        private RepositoryItemLookUpEdit repItemLkEdt_ItemBatch;
        private BarManager barManager1;
        private Bar bar1;
        private BarSubItem barSubItem1;
        private BarButtonItem barItemCopyWholeDocument;
        private BarButtonItem barItemPasteWholeDocument;
        private BarButtonItem barItemPasteItemDetailOnly;
        private BarSubItem barSubItem2;
        private BarButtonItem barItemCopyFrom;
        private BarButtonItem barItemCopyTo;
        private BarButtonItem barItemCopySelectedDetails;
        private BarButtonItem iUndoMaster;
        private BarButtonItem iEditMRU;
        private GridColumn colFurtherDescription;
        private RepositoryItemButtonEdit repItemBtnEdt_FurtherDescription;
        private RepositoryItemCheckEdit repItemCkEdt_General;
        private BarCheckItem barCheckReallocatePurchaseByProject;
        private GridColumn colSerialNoList;
        private BarSubItem barSubItem3;
        private BarButtonItem iEditRemark1MRU;
        private BarButtonItem iEditRemark2MRU;
        private BarButtonItem iEditRemark3MRU;
        private BarButtonItem iEditRemark4MRU;
        private BarButtonItem barBtnSaveInKIV;
        private BarButtonItem barItemCopyAsTabDelimitedText;
        private XtraTabPage xtraTabPage1;
        private XtraTabPage xtraTabPage2;
        private SimpleButton sbtnSelectAll;
        private SimpleButton sbtnUndo;
        private SimpleButton sbtnAddDTL;
        private SimpleButton sbtnDeleteDTL;
        private SimpleButton sbtnDown;
        private SimpleButton sbtnUp;
        private SimpleButton sbtnInsertBefore;
        private GridColumn gridColumn1;
        private RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private GridColumn colDebit;
        private GridColumn colCredit;
        private GridColumn colFromDocNo;
        private GridColumn colFromDocType;
        private GridColumn colFromDocDtlKey;
        private RepositoryItemLookUpEdit repItemLkEdt_AccNo;
        private System.Windows.Forms.Timer timer2;
        private LookUpEdit luMachineCode;
        private Label label5;
        private string sItemCode;
        private LookUpEdit luWONo;
        protected BCE.AutoCount.SearchFilter.Filter myWOFilter = new BCE.AutoCount.SearchFilter.Filter("a", "Code");
        private GridColumn colWIPCost;
        private GridColumn coBOMCode;
        private GridColumn colBatchRM;
        private RepositoryItemLookUpEdit repleBatchNo;
        private Tools.LookupEditBuilders.ItemFGLookupEditBuilder itembuilder;
        private BatchNoRMLookupEditBuilder batchnolp;
        //private Tools.LookupEditBuilders.ItemBatchForReceiveFGLookupEditBuilder itemBatchForReceiveFG;
        public FormStockReceiveEntry(StockReceive stockReceive)
          : this(stockReceive, new FormStockReceiveEditOption())
        {
        }

        public FormStockReceiveEntry(StockReceive stockReceive, FormStockReceiveEditOption editOption)
        {
            sItemCode = "";
            if (stockReceive == null)
                throw new ArgumentNullException("stockReceive");
            else if (editOption == null)
            {
                throw new ArgumentNullException("editOption");
            }
            else
            {
                this.myEditOption = editOption;
                this.InitializeComponent();
                this.myDBSetting = stockReceive.Command.DBSetting;
                this.myDBReg = stockReceive.Command.DBReg;
                //this.myScriptObject = ScriptManager.CreateObject(this.myDBSetting, "StockReceiveEditForm");
                this.myDecimalSetting = DecimalSetting.GetOrCreate(this.myDBSetting);
                this.myUserAuthentication = UserAuthentication.GetOrCreate(this.myDBSetting);
                this.SetUseLookupEditToInputItemCode();
                this.btnPreview.ReportType = "Stock Receive Document";
                this.btnPreview.SetDBSetting(this.myDBSetting);
                this.btnPrint.ReportType = "Stock Receive Document";
                this.btnPrint.SetDBSetting(this.myDBSetting);
                this.memoEdtNote.SetDBSetting(this.myDBSetting);
                this.InitRemark();
                this.InitMRUHelper();
                this.SetupLookupEdit();
               
                this.ucInquiryStock.Initialize(this.myDBSetting, "", "", "", "", InquiryType.Stock);
                BCE.AutoCount.Help.HelpProvider.SetHelpTopic(new System.Windows.Forms.HelpProvider(), (Control)this, "Stock_Issue.htm#stk027");
                //FormStockReceiveEntry.FormInitializeEventArgs initializeEventArgs1 = new FormStockReceiveEntry.FormInitializeEventArgs(this, stockReceive);
                //ScriptObject scriptObject1 = this.myScriptObject;
                //string name1 = "OnFormInitialize";
                //System.Type[] types1 = new System.Type[1];
                //int index1 = 0;
                //System.Type type1 = initializeEventArgs1.GetType();
                //types1[index1] = type1;
                //object[] objArray1 = new object[1];
                //int index2 = 0;
                //FormStockReceiveEntry.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
                //objArray1[index2] = (object)initializeEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray1);
                //ScriptObject scriptObject2 = stockReceive.ScriptObject;
                //string name2 = "OnFormInitialize";
                //System.Type[] types2 = new System.Type[1];
                //int index3 = 0;
                //System.Type type2 = initializeEventArgs1.GetType();
                //types2[index3] = type2;
                //object[] objArray2 = new object[1];
                //int index4 = 0;
                //FormStockReceiveEntry.FormInitializeEventArgs initializeEventArgs3 = initializeEventArgs1;
                //objArray2[index4] = (object)initializeEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray2);
                this.SetStockReceive(stockReceive);
                this.LogActivity(this.myDBSetting, stockReceive);
                tabControl1.TabIndex = 0;
                this.ucInquiryStock.Visible = !this.ucInquiryStock.Visible;
                this.splitterControl1.Visible = this.ucInquiryStock.Visible;
                //this.ucwofgSelector1.Initialize(myDBSetting, myWOFilter);
                InitializeBatchNoMaster();

                //.Height = assemblyKeepAfterSave.InstantInfoHeight;

            }
        }

        [StartThreadForm]
        public void StartForm(StockReceive stockReceive)
        {
            this.StartForm(stockReceive, new FormStockReceiveEditOption());
        }

        [StartThreadForm]
        public void StartForm(StockReceive stockReceive, FormStockReceiveEditOption editOption)
        {
            this.myEditOption = editOption;
            this.myDBSetting = stockReceive.Command.DBSetting;
            this.myDBReg = stockReceive.Command.DBReg;
            this.SetUseLookupEditToInputItemCode();
            this.LoadLocalSetting();
            this.SetStockReceive(stockReceive);
            this.LogActivity(this.myDBSetting, stockReceive);
            InitializeBatchNoMaster();
        }

        private void SetUseLookupEditToInputItemCode()
        {
            this.myUseLookupEditToInputItemCode = this.myDBReg.GetBoolean((IRegistryID)new UseLookupEditToInputItemCode());
            if (this.myUseLookupEditToInputItemCode)
            {
                if (this.repItemLkEdt_ItemCode.Columns.Count == 0)
                    DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repItemLkEdt_ItemCode, this.myDBSetting);
                this.colItemCode.ColumnEdit = (RepositoryItem)this.repItemLkEdt_ItemCode;
            }
            else
                this.colItemCode.ColumnEdit = (RepositoryItem)null;
        }

        private void LogActivity(DBSetting dbSetting, StockReceive stockReceive)
        {
            if (stockReceive.Action == StockReceiveAction.Edit)
            {
                DBSetting dbSetting1 = dbSetting;
                string docType = "RV";
                long docKey = stockReceive.DocKey;
                long eventKey = 0L;
                // ISSUE: variable of a boxed type
                StockReceiveString  local=StockReceiveString.ViewStockReceiveInEditMode;
                object[] objArray = new object[2];
                int index1 = 0;
                string loginUserId = this.myUserAuthentication.LoginUserID;
                objArray[index1] = (object)loginUserId;
                int index2 = 1;
                string str = stockReceive.DocNo.ToString();
                objArray[index2] = (object)str;
                string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                string detail = "";
                Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
            }
            else if (stockReceive.Action == StockReceiveAction.View)
            {
                DBSetting dbSetting1 = dbSetting;
                string docType = "RV";
                long docKey = stockReceive.DocKey;
                long eventKey = 0L;
                // ISSUE: variable of a boxed type
                StockReceiveString  local=StockReceiveString.ViewStockReceive;
                object[] objArray = new object[2];
                int index1 = 0;
                string loginUserId = this.myUserAuthentication.LoginUserID;
                objArray[index1] = (object)loginUserId;
                int index2 = 1;
                string str = stockReceive.DocNo.ToString();
                objArray[index2] = (object)str;
                string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                string detail = "";
                Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
            }
        }

        private void SaveLocalSetting()
        {
            BCE.AutoCount.Stock.StockReceive.StockReceiveKeepAfterSave issueKeepAfterSave = new BCE.AutoCount.Stock.StockReceive.StockReceiveKeepAfterSave();
            issueKeepAfterSave.CheckKeepAfterSave = this.chkedtNextRecord.Checked;
            issueKeepAfterSave.InstantInfoHeight = this.myInstantInfoHeight;
            issueKeepAfterSave.MaximizeWindow = this.WindowState == FormWindowState.Maximized;
            try
            {
                PersistenceUtil.SaveUserSetting((object)issueKeepAfterSave, "StockReceiveCondition2.setting");
            }
            catch (Exception ex)
            {
                StandardExceptionHandler.WriteExceptionToErrorLog(ex);
            }
        }

        private void LoadLocalSetting()
        {
           BCE.AutoCount.Stock.StockReceive.StockReceiveKeepAfterSave issueKeepAfterSave =(BCE.AutoCount.Stock.StockReceive.StockReceiveKeepAfterSave)PersistenceUtil.LoadUserSetting("StockReceiveCondition2.setting");
            if (issueKeepAfterSave != null)
            {
                this.chkedtNextRecord.Checked = issueKeepAfterSave.CheckKeepAfterSave;
                this.myInstantInfoHeight = issueKeepAfterSave.InstantInfoHeight;
                if (issueKeepAfterSave.MaximizeWindow)
                    this.WindowState = FormWindowState.Maximized;
            }
        }

        private void SetStockReceive(StockReceive newStockReceive)
        {
            if (this.myStockReceive != newStockReceive)
            {
                this.myIsAutomaticMergeSameItemCode = this.myDBReg.GetBoolean((IRegistryID)new AutomaticMergeSameItemCode());
                this.luEdtDocNoFormat.EditValue = (object)null;
                this.myStockReceive = newStockReceive;
                this.myStockReceive.EnableAutoLoadItemDetail = true;
                this.BindingMasterData();
                this.myGridControl.DataSource = (object)this.myStockReceive.DataTableDetail;
                this.gridViewStockDetail.Columns["Seq"].SortOrder = ColumnSortOrder.Ascending;
                this.chkedtNextRecord.Visible = this.myStockReceive.Action == StockReceiveAction.New && this.myEditOption.ShowProceedNewStockReceive;
                this.SetControlState();
                this.RefreshLookupEdit();
                this.InitUndoManager();
                this.InitDelegate();
                string itemCode;
                string UOM;
                if (this.myStockReceive.DetailCount > 0)
                {
                    DataRow dataRow = this.myStockReceive.DataTableDetail.Rows[0];
                    string index1 = "ItemCode";
                    itemCode = dataRow[index1].ToString();
                    string index2 = "UOM";
                    UOM = dataRow[index2].ToString();
                }
                else
                {
                    itemCode = "";
                    UOM = "";
                }
                if (this.gridViewStockDetail.DataRowCount == 1)
                    this.gridViewStockDetail_FocusedRowChanged((object)this.gridViewStockDetail, new FocusedRowChangedEventArgs(0, 0));
                else
                    this.gridViewStockDetail_FocusedRowChanged((object)this.gridViewStockDetail, new FocusedRowChangedEventArgs(this.gridViewStockDetail.FocusedRowHandle, this.gridViewStockDetail.FocusedRowHandle));
                this.ucInquiryStock.Reload(itemCode, UOM, "", "");
               // UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
                GridView gridView = this.gridViewStockDetail;
                string tableName1 = "RPA_RCVDtl";
                //udfUtil.SetupGrid(gridView, tableName1);
                XtraTabControl tabControl = this.tabControl1;
                string tableName2 = "RPA_RCV";
                DataTable dataTableMaster = this.myStockReceive.DataTableMaster;
                //udfUtil.SetupTabControl(tabControl, tableName2, (object)dataTableMaster);
                this.mySaveInKIV = false;
                this.timer1.Interval = TempDocumentSetting.Default.AutoSaveSeconds * 1000;
                this.timer1.Enabled = true;
                this.Tag = (object)new FormStockReceiveEntry.MyFilterOption()
                {
                    Document = this.myStockReceive
                };
                this.sbtnSavePreview.Enabled = this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_DOC_REPORT_PREVIEW");
                this.sbtnSavePrint.Enabled = this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_DOC_REPORT_PRINT");
                this.FormInitialize();
                //if (this.myStockReceive.Action == StockReceiveAction.New && this.myStockReceive.ReallocatePurchaseByProject && this.IsHandleCreated)
                //    this.SelectReallocatePurchaseByProjectNo();
                //FormStockReceiveEntry.FormDataBindingEventArgs bindingEventArgs1 = new FormStockReceiveEntry.FormDataBindingEventArgs(this, this.myStockReceive);
                //ScriptObject scriptObject1 = this.myScriptObject;
                //string name1 = "OnDataBinding";
                //System.Type[] types1 = new System.Type[1];
                //int index3 = 0;
                //System.Type type1 = bindingEventArgs1.GetType();
                //types1[index3] = type1;
                //object[] objArray1 = new object[1];
                //int index4 = 0;
                //FormStockReceiveEntry.FormDataBindingEventArgs bindingEventArgs2 = bindingEventArgs1;
                //objArray1[index4] = (object)bindingEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray1);
                //ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
                //string name2 = "OnDataBinding";
                //System.Type[] types2 = new System.Type[1];
                //int index5 = 0;
                //System.Type type2 = bindingEventArgs1.GetType();
                //types2[index5] = type2;
                //object[] objArray2 = new object[1];
                //int index6 = 0;
                //FormStockReceiveEntry.FormDataBindingEventArgs bindingEventArgs3 = bindingEventArgs1;
                //objArray2[index6] = (object)bindingEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray2);
            }
        }

        private void SelectReallocatePurchaseByProjectNo()
        {
            using (FormSelectReallocatePurchaseByProjectNo purchaseByProjectNo = new FormSelectReallocatePurchaseByProjectNo(this.myDBSetting, this.myStockReceive.DataTableMaster, "ReallocatePurchaseByProjectNo"))
            {
                int num = (int)purchaseByProjectNo.ShowDialog((IWin32Window)this);
            }
        }

        private void FormInitialize()
        {
            if (this.myStockReceive != null)
            {
                //Production.StockReceive.FormInitializeEventArgs initializeEventArgs1 = new Production.StockReceive.FormInitializeEventArgs(this.myStockReceive, this.panelHeader, this.myGridControl, this.tabControl1);
                //ScriptObject scriptObject = this.myStockReceive.ScriptObject;
                //string name = "OnFormInitialize";
                //System.Type[] types = new System.Type[2];
                //int index1 = 0;
                //System.Type type1 = typeof(object);
                //types[index1] = type1;
                //int index2 = 1;
                //System.Type type2 = initializeEventArgs1.GetType();
                //types[index2] = type2;
                //object[] objArray = new object[2];
                //int index3 = 0;
                //FormStockReceiveEntry formStockReceiveEntry = this;
                //objArray[index3] = (object)formStockReceiveEntry;
                //int index4 = 1;
                //Production.StockReceive.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
                //objArray[index4] = (object)initializeEventArgs2;
                //scriptObject.RunMethod(name, types, objArray);
            }
        }
        private void BindingMasterData()
        {

            ControlsHelper.ClearDataBinding((IEnumerable)this.Controls);
            this.txtEdtStockReceiveNo.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "DocNo"));
            this.luWONo.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "RefDocNo"));
            this.luMachineCode.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "MachineCode"));
            //this.luBatchNo.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "BatchNo"));
            this.mruEdtDescription.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "Description"));
            this.memoEdtNote.DataBindings.Add(new Binding("rtf", (object)this.myStockReceive.DataTableMaster, "Note"));
            this.textEdtTotal.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "Total"));
            this.dateEdtDate.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "DocDate"));
            this.externalLinkBox1.DataBindings.Add(new Binding("Links", (object)this.myStockReceive.DataTableMaster, "ExternalLink"));
            this.mruEdtRemark1.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "Remark1"));
            this.mruEdtRemark2.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "Remark2"));
            this.mruEdtRemark3.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "Remark3"));
            this.mruEdtRemark4.DataBindings.Add(new Binding("EditValue", (object)this.myStockReceive.DataTableMaster, "Remark4"));
            WorkOrder.WOFGHeaderLookupEditBuilder wobuilder = new WorkOrder.WOFGHeaderLookupEditBuilder();
            wobuilder.BuildLookupEdit(luWONo.Properties, myDBSetting);
            //wobuilder.FilterLookup("");
        }

        private void InitMRUHelper()
        {
            //this.myMRUHelper = BCE.AutoCount.XtraUtils.MRUHelper.Create(this.myDBSetting, 11, this.mruEdtDescription.Properties);
            //this.myMRUHelperForRemark1 = BCE.AutoCount.XtraUtils.MRUHelper.Create(this.myDBSetting, 30, this.mruEdtRemark1.Properties);
            //if (!this.myRemarkNameEntity.IsRemarkSupportMRU(1))
            //    this.mruEdtRemark1.Properties.Items.Clear();
            //this.myMRUHelperForRemark2 = BCE.AutoCount.XtraUtils.MRUHelper.Create(this.myDBSetting, 31, this.mruEdtRemark2.Properties);
            //if (!this.myRemarkNameEntity.IsRemarkSupportMRU(2))
            //    this.mruEdtRemark2.Properties.Items.Clear();
            //this.myMRUHelperForRemark3 = BCE.AutoCount.XtraUtils.MRUHelper.Create(this.myDBSetting, 32, this.mruEdtRemark3.Properties);
            //if (!this.myRemarkNameEntity.IsRemarkSupportMRU(3))
            //    this.mruEdtRemark3.Properties.Items.Clear();
            //this.myMRUHelperForRemark4 = BCE.AutoCount.XtraUtils.MRUHelper.Create(this.myDBSetting, 33, this.mruEdtRemark4.Properties);
            //if (!this.myRemarkNameEntity.IsRemarkSupportMRU(4))
            //    this.mruEdtRemark4.Properties.Items.Clear();
        }

        private void SaveMRUHelper()
        {
            //this.myMRUHelper.SaveMRUItems();
            //if (this.myRemarkNameEntity.IsRemarkSupportMRU(1) && this.myMRUHelperForRemark1 != null)
            //    this.myMRUHelperForRemark1.SaveMRUItems();
            //if (this.myRemarkNameEntity.IsRemarkSupportMRU(2) && this.myMRUHelperForRemark2 != null)
            //    this.myMRUHelperForRemark2.SaveMRUItems();
            //if (this.myRemarkNameEntity.IsRemarkSupportMRU(3) && this.myMRUHelperForRemark3 != null)
            //    this.myMRUHelperForRemark3.SaveMRUItems();
            //if (this.myRemarkNameEntity.IsRemarkSupportMRU(4) && this.myMRUHelperForRemark4 != null)
            //    this.myMRUHelperForRemark4.SaveMRUItems();
        }

        private void InitRemark()
        {
            this.myRemarkName = RemarkName.Create(this.myDBSetting);
            this.myRemarkNameEntity = this.myRemarkName.GetRemarkName("RV");
        }

        private void SetupRemark()
        {
            //this.labelRemark1.Text = this.myRemarkNameEntity.GetRemarkName(1);
            //this.labelRemark2.Text = this.myRemarkNameEntity.GetRemarkName(2);
            //this.labelRemark3.Text = this.myRemarkNameEntity.GetRemarkName(3);
            //this.labelRemark4.Text = this.myRemarkNameEntity.GetRemarkName(4);
            //if (!this.myRemarkNameEntity.IsRemarkSupportMRU(1))
            //{
            //    this.iEditRemark1MRU.Visibility = BarItemVisibility.Never;
            //}
            //else
            //{
            //    BarButtonItem barButtonItem = this.iEditRemark1MRU;
            //    // ISSUE: variable of a boxed type
            //    StockStringId  local=StockStringId.EditRemarkMRUItems;
            //    object[] objArray = new object[1];
            //    int index = 0;
            //    string text = this.labelRemark1.Text;
            //    objArray[index] = (object)text;
            //    string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            //    barButtonItem.Caption = @string;
            //}
            //if (!this.myRemarkNameEntity.IsRemarkSupportMRU(2))
            //{
            //    this.iEditRemark2MRU.Visibility = BarItemVisibility.Never;
            //}
            //else
            //{
            //    BarButtonItem barButtonItem = this.iEditRemark2MRU;
            //    // ISSUE: variable of a boxed type
            //    StockStringId  local=StockStringId.EditRemarkMRUItems;
            //    object[] objArray = new object[1];
            //    int index = 0;
            //    string text = this.labelRemark2.Text;
            //    objArray[index] = (object)text;
            //    string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            //    barButtonItem.Caption = @string;
            //}
            //if (!this.myRemarkNameEntity.IsRemarkSupportMRU(3))
            //{
            //    this.iEditRemark3MRU.Visibility = BarItemVisibility.Never;
            //}
            //else
            //{
            //    BarButtonItem barButtonItem = this.iEditRemark3MRU;
            //    // ISSUE: variable of a boxed type
            //    StockStringId  local=StockStringId.EditRemarkMRUItems;
            //    object[] objArray = new object[1];
            //    int index = 0;
            //    string text = this.labelRemark3.Text;
            //    objArray[index] = (object)text;
            //    string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            //    barButtonItem.Caption = @string;
            //}
            //if (!this.myRemarkNameEntity.IsRemarkSupportMRU(4))
            //{
            //    this.iEditRemark4MRU.Visibility = BarItemVisibility.Never;
            //}
            //else
            //{
            //    BarButtonItem barButtonItem = this.iEditRemark4MRU;
            //    // ISSUE: variable of a boxed type
            //    StockStringId  local=StockStringId.EditRemarkMRUItems;
            //    object[] objArray = new object[1];
            //    int index = 0;
            //    string text = this.labelRemark4.Text;
            //    objArray[index] = (object)text;
            //    string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            //    barButtonItem.Caption = @string;
            //}
        }

        private void InitUndoManager()
        {
            DataTable dataTableMaster = this.myStockReceive.DataTableMaster;
            DataTable dataTableDetail = this.myStockReceive.DataTableDetail;
            this.myMasterRecordUndo = new UndoManager(dataTableMaster, true);
            this.myMasterRecordUndo.ExcludeFields.Add((object)"Total");
            this.myMasterRecordUndo.CanUndoChanged += new EventHandler(this.myMasterRecordUndo_CanUndoChanged);
            this.iUndoMaster.Enabled = this.myMasterRecordUndo.CanUndo;
            this.myDetailRecordUndo = new UndoManager(dataTableDetail, false, new FillRowValueDelegate(this.CalcSubTotal));
            this.myDetailRecordUndo.ExcludeFields.Add((object)"SubTotal");
            this.myDetailRecordUndo.CanUndoChanged += new EventHandler(this.myDetailRecordUndo_CanUndoChanged);
            this.sbtnUndo.Enabled = this.myDetailRecordUndo.CanUndo;
            this.myMasterRecordUndo.SaveState();
            this.myMasterRecordUndo.KeepCurrentChanges();
            this.myDetailRecordUndo.SaveState();
            this.myDetailRecordUndo.KeepCurrentChanges();
        }

        private void InitDelegate()
        {
            this.myStockReceive.ConfirmChangingItemCodeEvent2 += new ConfirmChangingItemCodeEventHandler(this.ItemCodeChanged);
            this.myStockReceive.ConfirmSerialNumberQuantityNotMatchEvent2 += new ConfirmSerialNumberQuantityNotMatchEventHandler(BCE.AutoCount.SerialNumber2. FormSerialNumberNotMatch.ConfirmSerialNumberQuantityNotMatchEvent);
            this.myStockReceive.ShowSerialNumberEntryFormEvent2 += new ShowSerialNumberEntryFormEventHandler(this.myStockReceive_ShowSerialNumberEntryFormEvent);
            this.myStockReceive.SetDocNoFormatEvent2 += new SetDocNoFormatEventHandler(this.SetDocNoFormatName);
        }

        private void SetDocNoFormatName(string docNoFormatName)
        {
            DataTable dataTable = (DataTable)this.luEdtDocNoFormat.Properties.DataSource;
            if (dataTable != null)
            {
                if (dataTable.PrimaryKey.Length != 0)
                {
                    if (dataTable.Rows.Find((object)docNoFormatName) != null && this.luEdtDocNoFormat.Text != docNoFormatName)
                        this.luEdtDocNoFormat.EditValue = (object)docNoFormatName;
                }
                else if (dataTable.Select("Name = '" + StringHelper.ToSingleQuoteString(docNoFormatName) + "'").Length != 0 && this.luEdtDocNoFormat.Text != docNoFormatName)
                    this.luEdtDocNoFormat.EditValue = (object)docNoFormatName;
            }
        }

        private void myStockReceive_ShowSerialNumberEntryFormEvent(long dtlKey)
        {
            FormStockReceiveEntry formStockReceiveEntry = this;
            ShowSerialNumberEntryFormEventHandler formEventHandler = new ShowSerialNumberEntryFormEventHandler(this.ShowSerialNumberEntryFormEvent);
            object[] objArray = new object[1];
            int index = 0;
            // ISSUE: variable of a boxed type
            long local = dtlKey;
            objArray[index] = (object)local;
            // ISSUE: explicit non-virtual call
            formStockReceiveEntry.BeginInvoke((Delegate)formEventHandler, objArray);
        }

        private void ShowSerialNumberEntryFormEvent(long dtlKey)
        {
            DataRow[] dataRowArray = this.myStockReceive.DataTableDetail.Select("DtlKey = " + dtlKey.ToString());
            if (dataRowArray.Length != 0)
            {
                DataRow dr = dataRowArray[0];
                if (dr != null && InvoicingHelper.HasSerialNo(this.myDBSetting, dr["ItemCode"].ToString()))
                {
                    //FormStockReceiveEntry.BeforePromptSerialNumberEntryEventArgs numberEntryEventArgs1 = new FormStockReceiveEntry.BeforePromptSerialNumberEntryEventArgs(this, this.myStockReceive, dtlKey);
                    //ScriptObject scriptObject1 = this.myScriptObject;
                    //string name1 = "BeforePromptSerialNumberEntry";
                    //System.Type[] types1 = new System.Type[1];
                    //int index1 = 0;
                    //System.Type type1 = numberEntryEventArgs1.GetType();
                    //types1[index1] = type1;
                    //object[] objArray1 = new object[1];
                    //int index2 = 0;
                    //FormStockReceiveEntry.BeforePromptSerialNumberEntryEventArgs numberEntryEventArgs2 = numberEntryEventArgs1;
                    //objArray1[index2] = (object)numberEntryEventArgs2;
                    //scriptObject1.RunMethod(name1, types1, objArray1);
                    //ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
                    //string name2 = "BeforePromptSerialNumberEntry";
                    //System.Type[] types2 = new System.Type[1];
                    //int index3 = 0;
                    //System.Type type2 = numberEntryEventArgs1.GetType();
                    //types2[index3] = type2;
                    //object[] objArray2 = new object[1];
                    //int index4 = 0;
                    //FormStockReceiveEntry.BeforePromptSerialNumberEntryEventArgs numberEntryEventArgs3 = numberEntryEventArgs1;
                    //objArray2[index4] = (object)numberEntryEventArgs3;
                    //scriptObject2.RunMethod(name2, types2, objArray2);
                    //if (!numberEntryEventArgs1.Handled && !this.myIsBeforeStartDate)
                        this.ShowSerialNumberEntryForm(dr);
                }
            }
        }

        private void UnlinkDelegate()
        {
            this.myStockReceive.ConfirmChangingItemCodeEvent2 -= new ConfirmChangingItemCodeEventHandler(this.ItemCodeChanged);
            this.myStockReceive.ConfirmSerialNumberQuantityNotMatchEvent2 -= new ConfirmSerialNumberQuantityNotMatchEventHandler(BCE.AutoCount.SerialNumber2. FormSerialNumberNotMatch.ConfirmSerialNumberQuantityNotMatchEvent);
            this.myStockReceive.ShowSerialNumberEntryFormEvent2 -= new ShowSerialNumberEntryFormEventHandler(this.myStockReceive_ShowSerialNumberEntryFormEvent);
            this.myStockReceive.SetDocNoFormatEvent2 -= new SetDocNoFormatEventHandler(this.SetDocNoFormatName);
        }

        private bool ItemCodeChanged(long dtlKey, string itemCode)
        {
            if (this.myStockReceive.DataTableSerialNo.Select("DtlKey =" + dtlKey.ToString()).Length != 0 && !AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString(StockReceiveStringId.ConfirmMessage_ItemCodeChanged, new object[0])))
                return false;
            else
                return true;
        }

        private void ToggleSerialNumberButtonVisibility(string itemCode)
        {
            this.sbtnSerialNo.Visible = InvoicingHelper.HasSerialNo(this.myDBSetting, itemCode) && !this.myIsBeforeStartDate;
        }

        private void myMasterRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.iUndoMaster.Enabled = this.myMasterRecordUndo.CanUndo;
        }

        private void myDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndo.Enabled = this.myDetailRecordUndo.CanUndo;
        }

        private void SetDetailButtonState()
        {
            bool flag1 = this.myStockReceive.Action == StockReceiveAction.View;
            bool flag2 = this.gridViewStockDetail.RowCount > 0;
            this.sbtnDeleteDTL.Enabled = !flag1 & flag2;
            this.sbtnUp.Enabled = !flag1 & flag2;
            this.sbtnDown.Enabled = !flag1 & flag2;
            this.sbtnInsertBefore.Enabled = !flag1 & flag2;
            this.sbtnSelectAll.Enabled = !flag1 & flag2;
        }

        private void AdjustBottomButtons()
        {
            Control[] controlArray = new Control[9];
            int index1 = 0;
            SimpleButton simpleButton1 = this.sbtnCancel;
            controlArray[index1] = (Control)simpleButton1;
            int index2 = 1;
            SimpleButton simpleButton2 = this.sbtnSavePrint;
            controlArray[index2] = (Control)simpleButton2;
            int index3 = 2;
            SimpleButton simpleButton3 = this.sbtnSavePreview;
            controlArray[index3] = (Control)simpleButton3;
            int index4 = 3;
            SimpleButton simpleButton4 = this.sbtnSave;
            controlArray[index4] = (Control)simpleButton4;
            int index5 = 4;
            SimpleButton simpleButton5 = this.sbtnDelete;
            controlArray[index5] = (Control)simpleButton5;
            int index6 = 5;
            SimpleButton simpleButton6 = this.sbtnCancelDoc;
            controlArray[index6] = (Control)simpleButton6;
            int index7 = 6;
            SimpleButton simpleButton7 = this.sbtnEdit;
            controlArray[index7] = (Control)simpleButton7;
            int index8 = 7;
            PrintButton printButton = this.btnPrint;
            controlArray[index8] = (Control)printButton;
            int index9 = 8;
            PreviewButton previewButton = this.btnPreview;
            controlArray[index9] = (Control)previewButton;
            int num = this.panel2.Width;
            foreach (Control control in controlArray)
            {
                if (control.Visible)
                {
                    num = num - control.Width - 4;
                    control.Left = num;
                }
            }
        }

        private void SetControlState()
        {
            bool bReadOnly = this.myStockReceive.Action == StockReceiveAction.View;
            BCE.Controls.Utils.SetReadOnly((IEnumerable)this.Controls, bReadOnly);
            this.navigator.Visible = bReadOnly;
            this.sbtnSavePreview.Enabled = this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_DOC_REPORT_PREVIEW");
            this.sbtnSavePrint.Enabled = this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_DOC_REPORT_PRINT");
            this.sbtnAddDTL.Enabled = !bReadOnly;
            this.sbtnItemSearch.Enabled = !bReadOnly;
            this.sbtnRangeSet.Enabled = !bReadOnly && this.gridViewStockDetail.SelectedRowsCount > 0;
            this.hylinkEditUpdateRealCost.Enabled = !bReadOnly;
            this.hylinkEditUpdateReferenceCost.Enabled = !bReadOnly;
            this.SetDetailButtonState();
            this.btnPreview.Visible = bReadOnly;
            this.btnPrint.Visible = bReadOnly;
            this.sbtnEdit.Visible = bReadOnly;
            this.sbtnCancelDoc.Visible = bReadOnly;
            this.sbtnDelete.Visible = bReadOnly;
            this.sbtnSave.Visible = !bReadOnly;
            this.sbtnSavePreview.Visible = !bReadOnly;
            this.sbtnSavePrint.Visible = !bReadOnly;
          //  this.btnApplyFilter.Enabled = !bReadOnly;
            if (bReadOnly)
                this.sbtnCancel.Text = BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_Close, new object[0]);
            else
                this.sbtnCancel.Text = BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_Cancel, new object[0]);
            this.lblCancelled.Visible = this.myStockReceive.Cancelled;
            if (this.myStockReceive.Cancelled)
                this.sbtnCancelDoc.Text = BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_UncancelDocument, new object[0]);
            else
                this.sbtnCancelDoc.Text = BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_CancelDocument, new object[0]);
            this.SetWindowCaption();
        }

        private void InvokeAccessRightChanged()
        {
            this.BeginInvoke((Delegate)new MethodInvoker(this.ShowDocNoFormatLookupEdit));
            this.BeginInvoke((Delegate)new MethodInvoker(this.ControlShowTotalCost));
        }

        private void ShowDocNoFormatLookupEdit()
        {
            if (this.myStockReceive != null)
            {
                try
                {
                    this.luEdtDocNoFormat.Visible = (string)this.myStockReceive.DocNo == "<<New>>" && this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_CHANGE_DOCNO_FORMAT");
                    this.txtEdtStockReceiveNo.Enabled = this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_EDIT_DOCNO");
                    this.myFilterByLocation = SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnableFilterByCurrentUserLocationInDataEntry;
                    this.colLocation.OptionsColumn.ReadOnly = this.myFilterByLocation;
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                }
            }
        }

        private void ControlShowTotalCost()
        {
            if (!ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.ExpressEdition.Has)
            {
               // bool flag = this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_SHOW_TOTAL") && //this.myUserAuthentication.AccessRight.IsAccessible("SYS_BHV_VIEW_COST");
                //if (flag)
                //{
                //    this.myFooterHeight = 31;
                //}
                ////else
                //{
                //    this.colUnitCost.Visible = flag;
                //    this.colUnitCost.OptionsColumn.ShowInCustomizationForm = flag;
                //    this.colSubTotal.Visible = flag;
                //    this.colSubTotal.OptionsColumn.ShowInCustomizationForm = flag;
                //    this.myFooterHeight = 0;
                //}
            }
        }

        private void SetWindowCaption()
        {
            if (this.myStockReceive != null)
            {
                this.ShowDocNoFormatLookupEdit();
                this.ControlShowTotalCost();
                string @string;
                if ((string)this.myStockReceive.DocNo == "<<New>>")
                {
                    Document document = Document.CreateDocument(this.myDBSetting);
                    // ISSUE: variable of a boxed type
                    StockReceiveStringId  local=StockReceiveStringId.Code_NextPossibleNo;
                    object[] objArray = new object[1];
                    int index = 0;
                    string documentNo = document.GetDocumentNo("RV", this.luEdtDocNoFormat.Text, (DateTime)this.myStockReceive.DocDate);
                    objArray[index] = (object)documentNo;
                    @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                }
                else
                {
                    // ISSUE: variable of a boxed type
                    StockReceiveStringId local1 = StockReceiveStringId.Code_StockReceiveDocNo;
                    object[] objArray = new object[1];
                    int index = 0;
                    // ISSUE: variable of a boxed type
                    DBString local2 = this.myStockReceive.DocNo;
                    objArray[index] = (object)local2;
                    @string = BCE.Localization.Localizer.GetString((Enum)local1, objArray);
                }
                string str1 = this.myStockReceive.Action != StockReceiveAction.View ? (this.myStockReceive.Action != StockReceiveAction.Edit ? BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_NewStockReceive, new object[0]) + @string : BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_EditStockReceive, new object[0]) + @string) : BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_ViewStockReceive, new object[0]) + @string;
                string format = "{0} - {1} - {2} (Ver: {3})";
                object[] objArray1 = new object[4];
                int index1 = 0;
                string str2 = str1;
                objArray1[index1] = (object)str2;
                int index2 = 1;
                // ISSUE: variable of a boxed type
                DBString local3 = this.myStockReceive.Command.GeneralSetting.CompanyProfile.CompanyNameWithRemark;
                objArray1[index2] = (object)local3;
                int index3 = 2;
                string productName = OEM.GetCurrentOEM().ProductName;
                objArray1[index3] = (object)productName;
                int index4 = 3;
                //string minorProductVersion = ProductVersion.GetMajorMinorProductVersion();
                //objArray1[index4] = (object)minorProductVersion;
                this.Text = FormHelper.SetUniqueWindowCaption(string.Format(format, objArray1), (Form)this);
            }
        }

        private void InvokeModuleFeature(ModuleController contrller)
        {
            this.BeginInvoke((Delegate)new MethodInvoker(this.SetModuleFeature));
        }

        private void SetModuleFeature()
        {
            ModuleController moduleController = ModuleControl.GetOrCreate(this.myDBSetting).ModuleController;
            if (!moduleController.MultiLocationStock.Enable)
            {
                this.myColLocationVisibleIndex = this.colLocation.VisibleIndex;
                this.colLocation.VisibleIndex = -1;
            }
            else
                this.colLocation.VisibleIndex = this.myColLocationVisibleIndex;
            this.colLocation.OptionsColumn.ShowInCustomizationForm = moduleController.MultiLocationStock.Enable;
            if (!moduleController.BatchNo.Enable)
            {
                this.myColBatchNoVisibleIndex = this.colBatchNo.VisibleIndex;
                this.colBatchNo.VisibleIndex = -1;
            }
            else
                this.colBatchNo.VisibleIndex = this.myColBatchNoVisibleIndex;
            this.colBatchNo.OptionsColumn.ShowInCustomizationForm = moduleController.BatchNo.Enable;
            if (!moduleController.Project.Enable)
            {
                this.myColProjNoVisibleIndex = this.colProjNo.VisibleIndex;
                this.colProjNo.VisibleIndex = -1;
            }
            else
                this.colProjNo.VisibleIndex = this.myColProjNoVisibleIndex;
            this.colProjNo.OptionsColumn.ShowInCustomizationForm = moduleController.Project.Enable;
            if (!moduleController.Department.Enable)
            {
                this.myColDeptNoVisibleIndex = this.colDeptNo.VisibleIndex;
                this.colDeptNo.VisibleIndex = -1;
            }
            else
                this.colDeptNo.VisibleIndex = this.myColDeptNoVisibleIndex;
            this.colDeptNo.OptionsColumn.ShowInCustomizationForm = moduleController.Department.Enable;
            if (this.myGridLayout != null)
                this.myGridLayout.SaveDefaultLayout();
            if (moduleController.Department.Enable || moduleController.Project.Enable)
                this.barCheckReallocatePurchaseByProject.Visibility = BarItemVisibility.Always;
            else
                this.barCheckReallocatePurchaseByProject.Visibility = BarItemVisibility.Never;
        }

        private void FormStockReceiveEntry_Load(object sender, EventArgs e)
        {
            this.LoadLocalSetting();
            this.myColLocationVisibleIndex = this.colLocation.VisibleIndex;
            this.myColBatchNoVisibleIndex = this.colBatchNo.VisibleIndex;
            this.myColProjNoVisibleIndex = this.colProjNo.VisibleIndex;
            this.myColDeptNoVisibleIndex = this.colDeptNo.VisibleIndex;
            this.SetModuleFeature();
            this.myGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gridViewStockDetail);
            this.ReconnectGridColumnEdit();
            this.colLocation.OptionsColumn.ReadOnly = this.myFilterByLocation;
            ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.AddListener(new ModuleControllerListenerDelegate(this.InvokeModuleFeature), (Component)this);
            this.myUserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.InvokeAccessRightChanged), (Component)this);
            this.scanBarcodeControl1.Initialize(this.myDBSetting);
            this.InitFormControls();
            this.SetControlState();
           // if (this.myStockReceive.Action == StockReceiveAction.New && this.myStockReceive.ReallocatePurchaseByProject && this.IsHandleCreated)
            //    this.BeginInvoke((Delegate)new MethodInvoker(this.SelectReallocatePurchaseByProjectNo));
            this.sbtnShowInstantInfo.PerformClick();
            InitializeBatchNoMaster();

        }

        private void ReconnectGridColumnEdit()
        {
            this.colItemCode.ColumnEdit = !this.myUseLookupEditToInputItemCode ? (RepositoryItem)null : (RepositoryItem)this.repItemLkEdt_ItemCode;
            this.colLocation.ColumnEdit = (RepositoryItem)this.repItemLkEdt_Location;
            this.colFurtherDescription.ColumnEdit = (RepositoryItem)this.repItemBtnEdt_FurtherDescription;
            this.colProjNo.ColumnEdit = (RepositoryItem)this.repItemLkEdt_ProjNo;
            this.colPrintOut.ColumnEdit = (RepositoryItem)this.repItemCkEdt_General;
            this.colBatchNo.ColumnEdit = (RepositoryItem)this.repItemLkEdt_ItemBatch;
            this.colDeptNo.ColumnEdit = (RepositoryItem)this.repItemLkEdt_Dept;
            this.colUOM.ColumnEdit = (RepositoryItem)this.repItemLkEdt_ItemUOM;
        }

        private void GetBarCode(string itemCode, string uom, object batchNo, object serialNo, Decimal qty)
        {
            this.sbtnAddDTL.PerformClick();
            this.gridViewStockDetail.SetFocusedRowCellValue(this.colItemCode, (object)itemCode);
            if (uom.Length > 0)
                this.gridViewStockDetail.SetFocusedRowCellValue(this.colUOM, (object)uom);
            if (batchNo != null)
                this.gridViewStockDetail.SetFocusedRowCellValue(this.colBatchNo, batchNo);
            this.gridViewStockDetail.SetFocusedRowCellValue(this.colQty, (object)qty);
        }

        private void SetupLookupEdit()
        {            
            WorkOrder.WOFGHeaderLookupEditBuilder wobuilder = new WorkOrder.WOFGHeaderLookupEditBuilder();
            wobuilder.BuildLookupEdit(luWONo.Properties, myDBSetting);
          //  wobuilder.FilterLookup("");
            itembuilder = new Tools.LookupEditBuilders.ItemFGLookupEditBuilder();
            repItemLkEdt_ItemCode.ListChanged += RepItemLkEdt_ItemCode_ListChanged1;
            repItemLkEdt_ItemCode.PropertiesChanged += RepItemLkEdt_ItemCode_PropertiesChanged;
           // if (this.myUseLookupEditToInputItemCode && this.repItemLkEdt_ItemCode.Columns.Count == 0)
            itembuilder.BuildLookupEdit(this.repItemLkEdt_ItemCode, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).AllAccountLookupEditBuilder.BuildLookupEdit(this.repItemLkEdt_AccNo, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.BuildLookupEdit(this.repItemLkEdt_ItemUOM, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.BuildLookupEdit(this.repItemLkEdt_ItemBatch, this.myDBSetting);

            batchnolp = new BatchNoRMLookupEditBuilder();
            batchnolp.BuildLookupEdit(repleBatchNo, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.BuildLookupEdit(this.repItemLkEdt_ItemBatch, this.myDBSetting);
            //DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.BuildLookupEdit(this.luBatchNo.Properties, this.myDBSetting);
            // itemBatchForReceiveFG = new ItemBatchForReceiveFGLookupEditBuilder();
            //itemBatchForReceiveFG.BuildLookupEdit(this.repItemLkEdt_ItemBatch.Properties, this.myDBSetting);
            MachineLookupEditBuilder machinebuilder = MachineLookupEditBuilder.Create();
            machinebuilder.BuildLookupEdit(luMachineCode.Properties, myDBSetting);
            //if (this.myUseLookupEditToInputItemCode && this.repItemLkEdt_ItemCode.Columns.Count == 0)
               // DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemFGLookupEditBuilder.BuildLookupEdit(this.repItemLkEdt_ItemCode, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.BuildLookupEdit(this.repItemLkEdt_Location, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.BuildLookupEdit(this.repItemLkEdt_ProjNo, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.BuildLookupEdit(this.repItemLkEdt_Dept, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).StockReceiveDocNoFormatLookupEditBuilder.BuildLookupEdit(this.luEdtDocNoFormat.Properties, this.myDBSetting);
        }

        private void RepItemLkEdt_ItemCode_PropertiesChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void RepItemLkEdt_ItemCode_ListChanged1(object sender, ListChangedEventArgs e)
        {
            if (repItemLkEdt_ItemCode.GetDataSourceValue("BOMCode", e.NewIndex) != null)
            {
                string str = repItemLkEdt_ItemCode.GetDataSourceValue("BOMCode", e.NewIndex).ToString();
                DataRow drDetail = gridViewStockDetail.GetDataRow(gridViewStockDetail.FocusedRowHandle);
                drDetail["BOMCode"] = str;
            }
           // throw new NotImplementedException();
        }

        private void UnlinkLookupEditEventHandlers()
        {
            //itemBatchForReceiveFG.UnlinkEventHandlers(this.luBatchNo.Properties);
           // DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.UnlinkEventHandlers(this.luBatchNo.Properties);
            MachineLookupEditBuilder machinebuilder = MachineLookupEditBuilder.Create();
            machinebuilder.UnlinkEventHandlers(luMachineCode.Properties);

            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).AllAccountLookupEditBuilder.UnlinkEventHandlers(this.repItemLkEdt_AccNo);

            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.UnlinkEventHandlers(this.repItemLkEdt_ItemUOM);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.UnlinkEventHandlers(this.repItemLkEdt_ItemBatch);
            if (this.repItemLkEdt_ItemCode.Columns.Count > 0)
                itembuilder.UnlinkEventHandlers(this.repItemLkEdt_ItemCode);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.UnlinkEventHandlers(this.repItemLkEdt_Location);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.UnlinkEventHandlers(this.repItemLkEdt_ProjNo);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.UnlinkEventHandlers(this.repItemLkEdt_Dept);
            if (this.luEdtDocNoFormat != null)
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).StockReceiveDocNoFormatLookupEditBuilder.UnlinkEventHandlers(this.luEdtDocNoFormat.Properties);
        }

        private void RefreshLookupEdit()
        {
            try
            {
                //itemBatchForReceiveFG.Refresh(this.luBatchNo.Properties);
                //DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.Refresh(this.luBatchNo.Properties);
                //MachineLookupEditBuilder machinebuilder = MachineLookupEditBuilder.Create();
                //machinebuilder.Refresh(luMachineCode.Properties);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).AllAccountLookupEditBuilder.Refresh(this.repItemLkEdt_AccNo);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.Refresh(this.repItemLkEdt_ItemUOM);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.Refresh(this.repItemLkEdt_ItemBatch);
               
                //if (this.myUseLookupEditToInputItemCode && this.repItemLkEdt_ItemCode.Columns.Count > 0)
                    //itembuilder.Refresh(this.repItemLkEdt_ItemCode);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.Refresh(this.repItemLkEdt_Location);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.Refresh(this.repItemLkEdt_ProjNo);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.Refresh(this.repItemLkEdt_Dept);
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).StockReceiveDocNoFormatLookupEditBuilder.Refresh(this.luEdtDocNoFormat.Properties);
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
        }

        private void InitFormControls()
        {
            FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
            string fieldname1 = "DocDate";
            string fieldtype1 = "Date";
            formControlUtil.AddField(fieldname1, fieldtype1);
            string fieldname2 = "Total";
            string fieldtype2 = "Currency";
            formControlUtil.AddField(fieldname2, fieldtype2);
            string fieldname3 = "UnitCost";
            string fieldtype3 = "Cost";
            formControlUtil.AddField(fieldname3, fieldtype3);
            string fieldname4 = "Qty";
            string fieldtype4 = "Quantity";
            formControlUtil.AddField(fieldname4, fieldtype4);
            string fieldname5 = "SubTotal";
            string fieldtype5 = "Currency";
            formControlUtil.AddField(fieldname5, fieldtype5);
          
            FormStockReceiveEntry formStockReceiveEntry = this;
            formControlUtil.InitControls((Control)formStockReceiveEntry);
            this.sbtnAddDTL.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipAddDetail, new object[0]);
            this.sbtnInsertBefore.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipInsertBefore, new object[0]);
            this.sbtnDeleteDTL.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipDeleteDetail, new object[0]);
            this.sbtnUp.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipMoveUp, new object[0]);
            this.sbtnDown.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipMoveDown, new object[0]);
            this.sbtnUndo.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipUndoDetail, new object[0]);
            this.sbtnSelectAll.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipSelectAllDetail, new object[0]);
            this.sbtnRangeSet.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipRangeSet, new object[0]);
            this.sbtnItemSearch.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipItemSearch, new object[0]);
            this.sbtnShowInstantInfo.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipToggleInstantInfo, new object[0]);
            this.sbtnSave.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipSave, new object[0]);
            this.sbtnSavePreview.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipSaveAndPreview, new object[0]);
            this.sbtnSavePrint.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipSaveAndPrint, new object[0]);
            this.sbtnCancel.ToolTip = BCE.Localization.Localizer.GetString((Enum)BaseStringId.ToolTipCancelOrClose, new object[0]);
            this.SetupRemark();
        }

        private void FocusFirstControl()
        {
            this.mruEdtDescription.Focus();
        }

        private void AddNewDetail()
        {
            if (this.EndGridEdit())
            {
                if (this.tabControl1.SelectedTabPage != this.tabPageMain)
                    this.tabControl1.SelectedTabPage = this.tabPageMain;
                BCE.AutoCount.XtraUtils.GridViewUtils.SelectRowBySeq(this.gridViewStockDetail, this.myStockReceive.AddDetail(null).Seq);
            }
        }
        private void InitializeBatchNoMaster()
        {
            //if (sItemCode == "")
            {
                //if (myStockReceive.GetValidDetailRows().Length > 0)
                //{
                //foreach (DataRow drDtl in myStockReceive.GetValidDetailRows())
                // {
                //DataRow dr = myDBSetting.GetFirstDataRow("select ItemCode from item where HasBatchNo='T'", "");
                //if (dr != null && this.myStockReceive != null)
                {
                    // 
                    string sWONo = "";
                    sWONo = luWONo.EditValue != null ? luWONo.EditValue.ToString():"";
                        
                    //DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.FilterItemCode(sItemCode, (DateTime)this.myStockReceive.DocDate);

                    DataRow dr = myDBSetting.GetFirstDataRow("select a.ItemCode from item a inner join vRPA_TransWOtoRCV b on a.ItemCode=b.ItemCode where HasBatchNo='T' and DocNo=?", (object)sWONo);
                    if (dr != null && this.myStockReceive != null)
                    {
                        sItemCode = dr["ItemCode"].ToString();
                       // itemBatchForReceiveFG.FilterItemCode(sItemCode, (DateTime)this.myStockReceive.DocDate);

                    }
                    DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.FilterItemCode(sItemCode, (DateTime)this.myStockReceive.DocDate);
                    

                    //batchbuilder.Refresh(luBatchNo.Properties);
                    //break;

                }
                //DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.BuildLookupEdit(luBatchNo.Properties, myDBSetting, false);
                //}
                //}
            }
        }
        private void InitializeBatchNoMaster(string sitemcode)
        {
            if (myStockReceive.GetValidDetailRows().Length > 0)
            {
                object obj = myDBSetting.ExecuteScalar("select HasBatchNo from item where itemcode=?", (object)sitemcode);
                if (obj != null && obj != DBNull.Value)
                {
                    if (BCE.Data.Convert.TextToBoolean(obj))
                    {
                        sItemCode = sitemcode;
                        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.FilterItemCode(sitemcode, (DateTime)this.myStockReceive.DocDate);
                    }
                }
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.AddNewDetail));
            InitializeBatchNoMaster();
        }

        private void InsertDetailBefore()
        {
            if (this.EndGridEdit())
            {
                int focusedRowHandle = this.gridViewStockDetail.FocusedRowHandle;
                if (focusedRowHandle >= 0)
                {
                    this.myStockReceive.InsertDetailBefore(focusedRowHandle);
                    this.gridViewStockDetail.FocusedRowHandle = focusedRowHandle;
                    this.gridViewStockDetail.ClearSelection();
                    this.gridViewStockDetail.SelectRow(focusedRowHandle);
                    if (this.gridViewStockDetail.VisibleColumns.Count > 0)
                        this.gridViewStockDetail.FocusedColumn = this.gridViewStockDetail.VisibleColumns[0];

                    InitializeBatchNoMaster();
                }
            }
        }

        private void sbtnInsertBefore_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.InsertDetailBefore));
        }

        private bool Save(SaveDocumentAction saveAction)
        {

            string sMacAddr = "";
            string sCompanyName = "";
            AssemblyInfoEntity info = new AssemblyInfoEntity();
            object obj2 = myDBSetting.ExecuteScalar("USE MASTER select srvname from sys.sysservers");
            if (obj2 != null && obj2 != DBNull.Value)
            {
                sMacAddr = obj2.ToString();

            }
            obj2 = myDBSetting.ExecuteScalar("select CompanyName from Profile");
            if (obj2 != null && obj2 != DBNull.Value)
            {

                sCompanyName = obj2.ToString();
            }
            if (!Production.GLib.G.IsRegistered("RPA_Settings", sMacAddr + sCompanyName + info.Product, sMacAddr))
            {
                DateTime dtLicenseDate = Production.GLib.G.GetLicenseDate("RPA_Settings", "Production");
                if (DateTime.Now > dtLicenseDate)
                {
                    System.Windows.Forms.MessageBox.Show("Cannot save... Please activate your software.", "Transaction block after " + dtLicenseDate.ToLongDateString(), MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
          

            
            bool bIsMachine = false;
            object obj = myDBSetting.ExecuteScalar("select IsMachineNo from RPA_Settings");
            if(obj!=null && obj!=DBNull.Value)
            {
                bIsMachine = BCE.Data.Convert.TextToBoolean(obj);
            }
            if(bIsMachine)
            {
                if (luMachineCode.EditValue == null || luMachineCode.EditValue == DBNull.Value)
                {
                    BCE.Application.AppMessage.ShowErrorMessage("field Machine No. harus diisi, Simpan di batalkan...");
                    return false;
                }
            }
            int icount = 0;
            foreach (DataRow drDetail in myStockReceive.GetValidDetailRows())
            {
                icount++;
                string sBatchRM = "";
                sBatchRM = drDetail["BatchRM"] != null ? drDetail["BatchRM"].ToString() : "";

                if (drDetail["FromDocType"] != null && drDetail["FromDocType"] != DBNull.Value && sBatchRM.Length==0)
                {
                    if (drDetail["FromDocType"].ToString() == "Main Product")
                    {
                        obj = myDBSetting.ExecuteScalar("SELECT COUNT(*) FROM RPA_WODtl WOD WITH(NOLOCK) inner join (select b.FromDocType, C.BatchNo, C.Description, B.FromDocDtlKey, C.ManufacturedDate, C.ExpiryDate, C.Qty,b.ItemCode from RPA_RM a  WITH(NOLOCK) inner join RPA_RMDTL b  WITH(NOLOCK) on a.DocKey = b.DocKey inner join RPA_RMSubDtl c WITH(NOLOCK) on a.DocKey = b.DocKey and b.DtlKey = c.DtlKey where FromDocType = 'WO') RM ON WOD.DtlKey = RM.FromDocDtlKey inner join Item on item.ItemCode=RM.ItemCode where isnull(UDF_MainRM,'F')='T' and wod.FromDocDtlKey=?", (object)drDetail["FromDocDtlKey"]);
                        if (obj != null && obj != DBNull.Value)
                        {
                            if (BCE.Data.Convert.ToInt32(obj) > 0) 
                            {
                                BCE.Application.AppMessage.ShowErrorMessage("No"+icount.ToString()+" Item Code ..."+ drDetail["ItemCode"].ToString()+" reference Batch No. Item Raw Material tidak boleh kosong, Simpan di batalkan...");
                                return false;
                            }
                        }

                    }
                }
            }

            if (this.myStockReceive == null || this.myStockReceive.Action == StockReceiveAction.View || (this.IsAutoSaveDisabled() || !this.EndDocumentEdit()))
            {
                return false;
            }
            else
            {
                System.Windows.Forms.Application.DoEvents();
                bool flag = false;
                this.DisableAutoSave();
                if (this.myIsBeforeStartDate)
                    this.myStockReceive.ConfirmSerialNumberQuantityNotMatchEvent2 -= new ConfirmSerialNumberQuantityNotMatchEventHandler(BCE.AutoCount.SerialNumber2. FormSerialNumberNotMatch.ConfirmSerialNumberQuantityNotMatchEvent);
                FormStockReceiveEntry.FormBeforeSaveEventArgs beforeSaveEventArgs1 = new FormStockReceiveEntry.FormBeforeSaveEventArgs(this, this.myStockReceive);
                //ScriptObject scriptObject1 = this.myScriptObject;
                //string name1 = "BeforeSave";
                //System.Type[] types1 = new System.Type[1];
                //int index1 = 0;
                //System.Type type1 = beforeSaveEventArgs1.GetType();
                //types1[index1] = type1;
                //object[] objArray1 = new object[1];
                //int index2 = 0;
                //FormStockReceiveEntry.FormBeforeSaveEventArgs beforeSaveEventArgs2 = beforeSaveEventArgs1;
                //objArray1[index2] = (object)beforeSaveEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray1);
                //ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
                //string name2 = "BeforeSave";
                //System.Type[] types2 = new System.Type[1];
                //int index3 = 0;
                //System.Type type2 = beforeSaveEventArgs1.GetType();
                //types2[index3] = type2;
                //object[] objArray2 = new object[1];
                //int index4 = 0;
                //FormStockReceiveEntry.FormBeforeSaveEventArgs beforeSaveEventArgs3 = beforeSaveEventArgs1;
                //objArray2[index4] = (object)beforeSaveEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray2);
                try
                {
                    if (this.myStockReceive.IsModified())
                    {
                        if (this.gridViewStockDetail.RowCount > 0)
                        {
                            DataRow dataRow = this.gridViewStockDetail.GetDataRow(this.gridViewStockDetail.RowCount - 1);
                            if (dataRow != null && dataRow["ItemCode"] == DBNull.Value && (dataRow["Description"].ToString().Length == 0 && BCE.Data.Convert.ToDecimal(dataRow["SubTotal"]) == Decimal.Zero))
                                dataRow.Delete();
                        }
                        if (!FormHelper.ValidateUDFRequiredPropertyForItemCodeBase(this.myDBSetting, this.myStockReceive.DataTableMaster, this.myStockReceive.DataTableDetail, "RCV", "RCVDTL", this.tabControl1, this.myStockReceive.GetValidDetailRows(), this.gridViewStockDetail) || this.gridViewStockDetail.RowCount == 0 && !AppMessage.ShowConfirmMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.Code_Confirm, new object[0]), BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.ConfirmMessage_NoDetailItemWasSpecified, new object[0])) || !ItemBatchHelper.CheckEmptyItemBatch(this.myDBSetting, this.myStockReceive.DataTableDetail))
                        {
                            return false;
                        }
                        else
                        {
                            this.myStockReceive.Save(BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID,false);
                            this.SaveMRUHelper();
                        }
                    }
                    if (saveAction != SaveDocumentAction.Save)
                        this.PrintPreviewAfterSave(saveAction);
                    flag = true;
                }
                catch (DBConcurrencyException ex)
                {
                    DocumentEditHelper.HandleDBConcurrencyException(this.myDBSetting, "RCV", this.myStockReceive.DocKey);
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                }
                finally
                {
                    if (this.myIsBeforeStartDate)
                        this.myStockReceive.ConfirmSerialNumberQuantityNotMatchEvent2 += new ConfirmSerialNumberQuantityNotMatchEventHandler(BCE.AutoCount.SerialNumber2.FormSerialNumberNotMatch.ConfirmSerialNumberQuantityNotMatchEvent);
                    this.EnableAutoSave();
                }
                return flag;
            }
        }

        private void PrintPreviewAfterSave(SaveDocumentAction saveAction)
        {
            // ISSUE: variable of a boxed type
            StockReceiveString local1 = StockReceiveString.StockReceive;
            object[] objArray1 = new object[1];
            int index1 = 0;
            // ISSUE: variable of a boxed type
            DBString local2 = this.myStockReceive.DocNo;
            objArray1[index1] = (object)local2;
            ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum)local1, objArray1), "RPA_RCV_DOC_REPORT_PRINT", "RPA_RCV_DOC_REPORT_EXPORT", "");
            reportInfo.DocType = "RV";
            reportInfo.DocKey = this.myStockReceive.DocKey;
            reportInfo.UpdatePrintCountTableName = "RPA_RCV";
            reportInfo.EmailAndFaxInfo = StockReceiveCommand.GetEmailAndFaxInfo(this.myStockReceive.DocKey, this.myDBSetting);
            if (saveAction == SaveDocumentAction.SaveAndPreview)
            {
                if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_DOC_REPORT_PREVIEW", (XtraForm)this) && this.CheckPrintedDocument(saveAction))
                {
                    reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
                    reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
                    //BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs documentEventArgs1 = new BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs(reportInfo.EmailAndFaxInfo, reportInfo.DocKey, this.myDBSetting);
                    //ScriptObject scriptObject1 = this.myScriptObject;
                    //string name1 = "BeforePreviewDocument";
                    //System.Type[] types1 = new System.Type[1];
                    //int index2 = 0;
                    //System.Type type1 = documentEventArgs1.GetType();
                    //types1[index2] = type1;
                    //object[] objArray2 = new object[1];
                    //int index3 = 0;
                    //BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
                    //objArray2[index3] = (object)documentEventArgs2;
                    //scriptObject1.RunMethod(name1, types1, objArray2);
                    //ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
                    //string name2 = "BeforePreviewDocument";
                    //System.Type[] types2 = new System.Type[1];
                    //int index4 = 0;
                    //System.Type type2 = documentEventArgs1.GetType();
                    //types2[index4] = type2;
                    //object[] objArray3 = new object[1];
                    //int index5 = 0;
                    //BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs documentEventArgs3 = documentEventArgs1;
                    //objArray3[index5] = (object)documentEventArgs3;
                    //scriptObject2.RunMethod(name2, types2, objArray3);
                    //reportInfo.Tag = (object)documentEventArgs1;
                    //if (documentEventArgs1.AllowPreview)
                        ReportTool.PreviewReport("Stock Receive Document", this.myStockReceive.Command.GetReportDataSource(this.myStockReceive.DocKey), this.myDBSetting, true, true, this.myStockReceive.Command.ReportOption, reportInfo);
                }
            }
            else if (saveAction == SaveDocumentAction.SaveAndPrint && this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_DOC_REPORT_PRINT", (XtraForm)this) && this.CheckPrintedDocument(saveAction))
                ReportTool.PrintReport("Stock Receive Document", this.myStockReceive.Command.GetReportDataSource(this.myStockReceive.DocKey), this.myDBSetting, true, this.myStockReceive.Command.ReportOption, reportInfo);
        }

        private bool CheckPrintedDocument(SaveDocumentAction saveAction)
        {
            if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && (int)this.myStockReceive.PrintCount > 0)
            {
                if (saveAction == SaveDocumentAction.SaveAndPreview)
                {
                    if (!this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_PRINTED_PREVIEW", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPreviewPrintedDocument, new object[0])))
                        return false;
                }
                else if (!this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_PRINTED_PRINT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0])))
                    return false;
            }
            return true;
        }

        private void CheckAndPerformUOMConversion()
        {
            if (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.AdvancedMultiUOM.Enable && this.myStockReceive.DataTableDetail.Rows.Count > 0)
            {
                UOMConversionHelper conversionHelper = UOMConversionHelper.Create(this.myDBSetting, "RV", this.luEdtDocNoFormat.Text, (DateTime)this.myStockReceive.DocDate);
                DataTable detailTable = this.myStockReceive.DataTableDetail.Copy();
                detailTable.Columns.Add("Rate", typeof(Decimal));
                StockHelper stockHelper = StockHelper.Create(this.myDBSetting);
                foreach (DataRow dataRow in (InternalDataCollectionBase)detailTable.Rows)
                {
                    if (dataRow.RowState != DataRowState.Deleted)
                        dataRow["Rate"] = (object)stockHelper.GetItemUOMRate(dataRow["ItemCode"].ToString(), dataRow["UOM"].ToString());
                }
                conversionHelper.ApplyUOMConversion(detailTable, this.myStockReceive.DocKey, (string)this.myStockReceive.DocNo);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.EndDocumentEdit())
                this.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_EDIT", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockReceive.PrintCount <= 0 || this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_PRINTED_EDIT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedEditPrintedDocument, new object[0]))))
            {
                try
                {
                    long docKey = this.myStockReceive.DocKey;
                    object obj = myDBSetting.ExecuteScalar("select a.Status from RPA_WO a inner join RPA_RCV b on a.DocNo=b.RefDocNo where b.DocKey=?", (object)docKey);
                    if (obj != null && obj != DBNull.Value)
                    {
                        if (obj.ToString() == WorkOrder.WorkOrderStatusOptions.Closed.ToString() || obj.ToString() == WorkOrder.WorkOrderStatusOptions.Cancel.ToString())                           
                        {
                            BCE.Application.AppMessage.ShowErrorMessage("Status Document WO has been " + obj.ToString() + ", Edit aborted...");
                            return;
                        }
                        else
                        {
                            this.myStockReceive.Edit();
                            DBSetting dbSetting = this.myDBSetting;
                            string docType = "RV";
                           
                            long eventKey = 0L;
                            // ISSUE: variable of a boxed type
                            StockReceiveString local = StockReceiveString.EditedStockReceive;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string loginUserId = this.myUserAuthentication.LoginUserID;
                            objArray[index1] = (object)loginUserId;
                            int index2 = 1;
                            string str = this.myStockReceive.DocNo.ToString();
                            objArray[index2] = (object)str;
                            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                            string detail = "";
                            Activity.Log(dbSetting, docType, docKey, eventKey, @string, detail);
                        }
                    }
                    else
                    {
                        this.myStockReceive.Edit();
                        DBSetting dbSetting = this.myDBSetting;
                        string docType = "RV";

                        long eventKey = 0L;
                        // ISSUE: variable of a boxed type
                        StockReceiveString local = StockReceiveString.EditedStockReceive;
                        object[] objArray = new object[2];
                        int index1 = 0;
                        string loginUserId = this.myUserAuthentication.LoginUserID;
                        objArray[index1] = (object)loginUserId;
                        int index2 = 1;
                        string str = this.myStockReceive.DocNo.ToString();
                        objArray[index2] = (object)str;
                        string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                        string detail = "";
                        Activity.Log(dbSetting, docType, docKey, eventKey, @string, detail);
                    }
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                    return;
                }
                this.SetControlState();
                this.SwitchToEditMode();
            }
        }

        private void SwitchToEditMode()
        {
            if (this.myStockReceive != null)
            {
                FormStockReceiveEntry.FormEventArgs formEventArgs1 = new FormStockReceiveEntry.FormEventArgs(this, this.myStockReceive);
               // ScriptObject scriptObject1 = this.myScriptObject;
               // string name1 = "OnSwitchToEditMode";
               // System.Type[] types1 = new System.Type[1];
               // int index1 = 0;
               // System.Type type1 = formEventArgs1.GetType();
               // types1[index1] = type1;
               // object[] objArray1 = new object[1];
               // int index2 = 0;
               // FormStockReceiveEntry.FormEventArgs formEventArgs2 = formEventArgs1;
               // objArray1[index2] = (object)formEventArgs2;
               // scriptObject1.RunMethod(name1, types1, objArray1);
               // ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
               // string name2 = "OnSwitchToEditMode";
               // System.Type[] types2 = new System.Type[1];
               // int index3 = 0;
               // System.Type type2 = formEventArgs1.GetType();
               // types2[index3] = type2;
               // object[] objArray2 = new object[1];
               // int index4 = 0;
               // FormStockReceiveEntry.FormEventArgs formEventArgs3 = formEventArgs1;
               // objArray2[index4] = (object)formEventArgs3;
               // scriptObject2.RunMethod(name2, types2, objArray2);
               //Production.StockReceive.FormStockReceiveEventArgs formEventArgs4 = new Production.StockReceive.FormStockReceiveEventArgs(this.myStockReceive, this.panelHeader, this.myGridControl, this.tabControl1);
               // ScriptObject scriptObject3 = this.myStockReceive.ScriptObject;
               // string name3 = "OnSwitchToEditMode";
               // System.Type[] types3 = new System.Type[2];
               // int index5 = 0;
               // System.Type type3 = typeof(object);
               // types3[index5] = type3;
               // int index6 = 1;
               // System.Type type4 = formEventArgs4.GetType();
               // types3[index6] = type4;
               // object[] objArray3 = new object[2];
               // int index7 = 0;
               // FormStockReceiveEntry formStockReceiveEntry = this;
               // objArray3[index7] = (object)formStockReceiveEntry;
               // int index8 = 1;
               // Production.StockReceive.FormStockReceiveEventArgs formEventArgs5 = formEventArgs4;
               // objArray3[index8] = (object)formEventArgs5;
               // scriptObject3.RunMethod(name3, types3, objArray3);
            }
        }

        private void DeleteSelectedRows()
        {
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gridViewStockDetail);
        }

        private void sbtnDeleteDTL_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.DeleteSelectedRows));
        }

        private void MoveUp()
        {
            BCE.XtraUtils.GridViewUtils.MoveUp(this.gridViewStockDetail);
        }

        private void sbtnUp_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveUp));
        }

        private void MoveDown()
        {
            BCE.XtraUtils.GridViewUtils.MoveDown(this.gridViewStockDetail);
        }

        private void sbtnDown_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveDown));
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_DELETE", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockReceive.PrintCount <= 0 || this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_PRINTED_DELETE", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedDeletePrintedDocument, new object[0]))))
            {
                long docKey = this.myStockReceive.DocKey;
                object obj = myDBSetting.ExecuteScalar("select a.Status from RPA_WO a inner join RPA_RCV b on a.DocNo=b.RefDocNo where b.DocKey=?", (object)docKey);
                if (obj != null && obj != DBNull.Value)
                {
                    if (obj.ToString() == WorkOrder.WorkOrderStatusOptions.Closed.ToString() || obj.ToString() == WorkOrder.WorkOrderStatusOptions.Cancel.ToString())
                        BCE.Application.AppMessage.ShowErrorMessage("Status Document WO has been " + obj.ToString() + ", Delete aborted...");
                    return;
                }
                // ISSUE: variable of a boxed type
                StockReceiveStringId local1 = StockReceiveStringId.ConfirmMessage_DeleteStockReceive;
                object[] objArray = new object[1];
                int index = 0;
                // ISSUE: variable of a boxed type
                DBString local2 = this.myStockReceive.DocNo;
                objArray[index] = (object)local2;
                if (AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum)local1, objArray)))
                {
                    this.DisableAutoSave();
                    try
                    {
                        BCE.Data.Convert.ToInt64(this.myStockReceive.DataTableMaster.Rows[0]["DocKey"]);
                        this.myStockReceive.Delete();
                        AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString(StockReceiveStringId.ShowMessage_DeleteSuccessfully, new object[0]));
                        this.Close();
                    }
                    catch (DBConcurrencyException ex)
                    {
                        DocumentEditHelper.HandleDBConcurrencyExceptionWhenDeleteDocument();
                    }
                    catch (AppException ex)
                    {
                        AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                    }
                    finally
                    {
                        this.EnableAutoSave();
                    }
                }
            }
        }

        private void FormStockReceiveEntry_Closing(object sender, CancelEventArgs e)
        {
            if (this.myStockReceive != null)
            {
                if (this.myStockReceive.Action != StockReceiveAction.View && this.Visible && !this.mySaveInKIV)
                {
                    if (!this.EndDocumentEdit())
                        return;
                    else if (this.myStockReceive.IsModified())
                    {
                        this.Activate();
                        DialogResult dialogResult = AppMessage.ShowConfirmSaveChangesMessage();
                        e.Cancel = dialogResult != DialogResult.Yes ? dialogResult != DialogResult.No : !this.Save(SaveDocumentAction.Save);
                    }
                }
                if (!e.Cancel)
                {
                    //FormStockReceiveEntry.FormClosedEventArgs formClosedEventArgs1 = new FormStockReceiveEntry.FormClosedEventArgs(this, this.myStockReceive);
                    //ScriptObject scriptObject1 = this.myScriptObject;
                    //string name1 = "OnFormClosed";
                    //System.Type[] types1 = new System.Type[1];
                    //int index1 = 0;
                    //System.Type type1 = formClosedEventArgs1.GetType();
                    //types1[index1] = type1;
                    //object[] objArray1 = new object[1];
                    //int index2 = 0;
                    //FormStockReceiveEntry.FormClosedEventArgs formClosedEventArgs2 = formClosedEventArgs1;
                    //objArray1[index2] = (object)formClosedEventArgs2;
                    //scriptObject1.RunMethod(name1, types1, objArray1);
                    //ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
                    //string name2 = "OnFormClosed";
                    //System.Type[] types2 = new System.Type[1];
                    //int index3 = 0;
                    //System.Type type2 = formClosedEventArgs1.GetType();
                    //types2[index3] = type2;
                    //object[] objArray2 = new object[1];
                    //int index4 = 0;
                    //FormStockReceiveEntry.FormClosedEventArgs formClosedEventArgs3 = formClosedEventArgs1;
                    //objArray2[index4] = (object)formClosedEventArgs3;
                    //scriptObject2.RunMethod(name2, types2, objArray2);
                    //Production.StockReceive.FormClosedEventArgs formClosedEventArgs4 = new Production.StockReceive.FormClosedEventArgs(this.myStockReceive, this.panelHeader, this.myGridControl, this.tabControl1);
                    //ScriptObject scriptObject3 = this.myStockReceive.ScriptObject;
                    //string name3 = "OnFormClosed";
                    //System.Type[] types3 = new System.Type[2];
                    //int index5 = 0;
                    //System.Type type3 = typeof(object);
                    //types3[index5] = type3;
                    //int index6 = 1;
                    //System.Type type4 = formClosedEventArgs4.GetType();
                    //types3[index6] = type4;
                    //object[] objArray3 = new object[2];
                    //int index7 = 0;
                    //FormStockReceiveEntry formStockReceiveEntry = this;
                    //objArray3[index7] = (object)formStockReceiveEntry;
                    //int index8 = 1;
                    //Production.StockReceive.FormClosedEventArgs formClosedEventArgs5 = formClosedEventArgs4;
                    //objArray3[index8] = (object)formClosedEventArgs5;
                    //scriptObject3.RunMethod(name3, types3, objArray3);
                    this.Cleanup();
                }
            }
        }

        private void Cleanup()
        {
            this.timer1.Enabled = false;
            this.timer2.Enabled = false;
            if (this.Visible)
            {
                this.SaveLocalSetting();
                if (!this.mySaveInKIV)
                {
                    if (this.myStockReceive.Action != StockReceiveAction.View)
                    {
                        try
                        {
                            this.DeleteTempDocument();
                        }
                        catch (Exception ex)
                        {
                            StandardExceptionHandler.WriteExceptionToErrorLog(ex);
                        }
                    }
                }
            }
            if (this.myStockReceive != null)
            {
                this.UnlinkDelegate();
               this.myStockReceive.PrepareForDispose();
                this.myStockReceive = (StockReceive)null;
            }
            this.myGridControl.DataSource = (object)null;
            ControlsHelper.ClearAllDataBinding((IEnumerable)this.Controls);
            if (this.myMasterRecordUndo != null)
            {
                this.myMasterRecordUndo.CanUndoChanged -= new EventHandler(this.myMasterRecordUndo_CanUndoChanged);
                this.myMasterRecordUndo.Dispose();
                this.myMasterRecordUndo = (UndoManager)null;
            }
            if (this.myDetailRecordUndo != null)
            {
                this.myDetailRecordUndo.CanUndoChanged -= new EventHandler(this.myDetailRecordUndo_CanUndoChanged);
                this.myDetailRecordUndo.Dispose();
                this.myDetailRecordUndo = (UndoManager)null;
            }
            if (this.timer1 != null)
                this.timer1.Dispose();
            if (this.timer2 != null)
                this.timer2.Dispose();
            if (this.myFormItemSearch != null)
            {
                this.myFormItemSearch.Dispose();
                this.myFormItemSearch = (FormItemSearch)null;
            }
        }

        private void chkedtNextRecord_CheckedChanged(object sender, EventArgs e)
        {
            this.SaveLocalSetting();
        }

        private void FormStockReceiveEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.myStockReceive != null)
            {
                if (e.Control && e.KeyCode == (Keys.LButton | Keys.ShiftKey | Keys.Space) && (this.sbtnShowInstantInfo.Visible && this.sbtnShowInstantInfo.Enabled))
                {
                    this.sbtnShowInstantInfo.PerformClick();
                    e.Handled = true;
                }
                else if (e.Shift && e.Control)
                {
                    if (e.KeyCode == (Keys)77)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPageMain)
                            this.tabControl1.SelectedTabPage = this.tabPageMain;
                        this.myGridControl.Focus();
                        e.Handled = true;
                    }
                    else if (e.KeyCode == (Keys)72)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPageMoreHeader)
                            this.tabControl1.SelectedTabPage = this.tabPageMoreHeader;
                        this.mruEdtRemark1.Focus();
                        e.Handled = true;
                    }
                    else if (e.KeyCode == (Keys)69)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPageExternalLink)
                            this.tabControl1.SelectedTabPage = this.tabPageExternalLink;
                        this.externalLinkBox1.Focus();
                        e.Handled = true;
                    }
                    else if (e.KeyCode == (Keys)78)
                    {
                        if (this.tabControl1.SelectedTabPage != this.tabPageNote)
                            this.tabControl1.SelectedTabPage = this.tabPageNote;
                        this.memoEdtNote.Focus();
                        e.Handled = true;
                    }
                }
                else if (this.myStockReceive.Action != StockReceiveAction.View)
                {
                    if (!e.Shift && !e.Control && !e.Alt)
                    {
                        if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            this.AddNewDetail();
                            this.myGridControl.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)114)
                        {
                            this.sbtnSave.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)119)
                        {
                            this.sbtnSavePreview.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)118)
                        {
                            this.sbtnSavePrint.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)120 && this.sbtnItemSearch.Enabled)
                        {
                            this.sbtnItemSearch.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)123 && this.sbtnRangeSet.Enabled)
                        {
                            this.sbtnRangeSet.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)117)
                        {
                            Control control = this.ActiveControl;
                            while (control != null && !(control is XtraTabPage))
                                control = control.Parent;
                            if (control == null)
                            {
                                if (this.tabControl1.SelectedTabPage == this.tabPageMain)
                                {
                                    if (this.myGridControl.CanFocus)
                                        this.myGridControl.Focus();
                                }
                                else if (this.tabControl1.SelectedTabPage == this.tabPageMoreHeader)
                                {
                                    if (this.mruEdtRemark1.CanFocus)
                                        this.mruEdtRemark1.Focus();
                                }
                                else if (this.tabControl1.SelectedTabPage == this.tabPageExternalLink)
                                {
                                    if (this.externalLinkBox1.CanFocus)
                                        this.externalLinkBox1.Focus();
                                }
                                else if (this.memoEdtNote.CanFocus)
                                    this.memoEdtNote.Focus();
                            }
                            else if (this.mruEdtDescription.CanFocus)
                                this.mruEdtDescription.Focus();
                        }
                    }
                    else if (e.Control && e.Alt)
                    {
                        if (e.KeyCode == (Keys)65)
                        {
                            this.gridViewStockDetail.SelectAll();
                            this.myGridControl.Focus();
                            e.Handled = true;
                        }
                    }
                    else if (e.Alt && e.Shift)
                    {
                        if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Space))
                        {
                            if (this.sbtnUp.Enabled)
                            {
                                this.sbtnUp.PerformClick();
                                e.Handled = true;
                            }
                        }
                        else if (e.KeyCode == (Keys.Back | Keys.Space) && this.sbtnDown.Enabled)
                        {
                            this.sbtnDown.PerformClick();
                            e.Handled = true;
                        }
                    }
                    else if (e.Control)
                    {
                        if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space) && this.sbtnDeleteDTL.Enabled)
                        {
                            this.sbtnDeleteDTL.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            this.InsertDetailBefore();
                            this.myGridControl.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)90 && this.sbtnUndo.Enabled)
                        {
                            this.sbtnUndo.PerformClick();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)70)
                        {
                            this.repositoryItemButtonEditFurtherDescription_ButtonPressed((object)this.repItemBtnEdt_FurtherDescription, (ButtonPressedEventArgs)null);
                            e.Handled = true;
                        }
                    }
                }
                else if (e.KeyCode == (Keys)113 && this.sbtnEdit.Enabled)
                {
                    this.sbtnEdit.PerformClick();
                    e.Handled = true;
                }
                else if (e.KeyCode == (Keys)119 && this.btnPreview.Enabled)
                {
                    this.btnPreview.PerformClick();
                    e.Handled = true;
                }
                else if (e.KeyCode == (Keys)118 && this.btnPrint.Enabled)
                {
                    this.btnPrint.PerformClick();
                    e.Handled = true;
                }
                else if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space) && this.sbtnDelete.Enabled)
                {
                    this.sbtnDelete.PerformClick();
                    e.Handled = true;
                }
            }
        }

        private void FormStockReceiveEntry_Activated(object sender, EventArgs e)
        {
            if (!this.mySkipExecuteFormActivated)
            {
                this.SetUseLookupEditToInputItemCode();
                if (this.myHasDeactivated)
                    this.RefreshLookupEdit();
                else
                    this.FocusFirstControl();
            }
        }

        private void gridViewStockDetail_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            DataRow dataRow = this.gridViewStockDetail.GetDataRow(e.RowHandle);
            if (dataRow != null)
            {
                string[] strArray = new string[6];
                int index1 = 0;
                string str1 = "ItemCode";
                strArray[index1] = str1;
                int index2 = 1;
                string str2 = "UOM";
                strArray[index2] = str2;
                int index3 = 2;
                string str3 = "Location";
                strArray[index3] = str3;
                int index4 = 3;
                string str4 = "ProjNo";
                strArray[index4] = str4;
                int index5 = 4;
                string str5 = "DeptNo";
                strArray[index5] = str5;
                int index6 = 5;
                string str6 = "BatchNo";
                strArray[index6] = str6;
                foreach (string index7 in strArray)
                {
                    if (e.Column.FieldName == index7)
                    {
                        e.DisplayText = dataRow[index7].ToString();
                        break;
                    }
                }
            }
        }

        private void btnCancelDoc_Click(object sender, EventArgs e)
        {
            if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_CANCEL", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockReceive.PrintCount <= 0 || this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_PRINTED_CANCEL", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedCancelOrUncancelPrintedDocument, new object[0]))))
            {
                long docKey = this.myStockReceive.DocKey;
                string string1;
                object obj = myDBSetting.ExecuteScalar("select a.Status from RPA_WO a inner join RPA_RCV b on a.DocNo=b.RefDocNo where b.DocKey=?", (object)docKey);
                if (obj != null && obj != DBNull.Value)
                {                   
                    if (obj.ToString() == WorkOrder.WorkOrderStatusOptions.Closed.ToString() || obj.ToString() == WorkOrder.WorkOrderStatusOptions.Cancel.ToString())
                    {
                        BCE.Application.AppMessage.ShowErrorMessage("Status Document WO has been " + obj.ToString() + ", Cancel / Uncancel aborted...");
                        return;
                    }
                    else
                    {
                        if (this.myStockReceive.Cancelled)
                        {
                            // ISSUE: variable of a boxed type
                            StockReceiveStringId local1 = StockReceiveStringId.ConfirmMessage_UncancelStockReceiveAction;
                            object[] objArray = new object[1];
                            int index = 0;
                            // ISSUE: variable of a boxed type
                            DBString local2 = this.myStockReceive.DocNo;
                            objArray[index] = (object)local2;
                            string1 = BCE.Localization.Localizer.GetString((Enum)local1, objArray);
                        }
                        else
                        {
                            // ISSUE: variable of a boxed type
                            StockReceiveStringId local1 = StockReceiveStringId.ConfirmMessage_CancelStockReceiveAction;
                            object[] objArray = new object[1];
                            int index = 0;
                            // ISSUE: variable of a boxed type
                            DBString local2 = this.myStockReceive.DocNo;
                            objArray[index] = (object)local2;
                            string1 = BCE.Localization.Localizer.GetString((Enum)local1, objArray);
                        }
                        if (AppMessage.ShowConfirmMessage(string1))
                        {
                            this.DisableAutoSave();
                            try
                            {
                                if (this.myStockReceive.Cancelled)
                                {
                                    this.myStockReceive.UncancelDocument(this.myUserAuthentication.LoginUserID);
                                    DBSetting dbSetting = this.myDBSetting;
                                    string docType = "RV";
                                  
                                    long eventKey = 0L;
                                    // ISSUE: variable of a boxed type
                                    StockReceiveString local = StockReceiveString.UncancelStockReceive;
                                    object[] objArray = new object[2];
                                    int index1 = 0;
                                    string loginUserId = this.myUserAuthentication.LoginUserID;
                                    objArray[index1] = (object)loginUserId;
                                    int index2 = 1;
                                    string str = this.myStockReceive.DocNo.ToString();
                                    objArray[index2] = (object)str;
                                    string string2 = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                                    string detail = "";
                                    Activity.Log(dbSetting, docType, docKey, eventKey, string2, detail);
                                }
                                else
                                {
                                    this.myStockReceive.CancelDocument(this.myUserAuthentication.LoginUserID);
                                    DBSetting dbSetting = this.myDBSetting;
                                    string docType = "RV";
                                   // long docKey = this.myStockReceive.DocKey;
                                    long eventKey = 0L;
                                    // ISSUE: variable of a boxed type
                                    StockReceiveString local = StockReceiveString.CancelStockReceive;
                                    object[] objArray = new object[2];
                                    int index1 = 0;
                                    string loginUserId = this.myUserAuthentication.LoginUserID;
                                    objArray[index1] = (object)loginUserId;
                                    int index2 = 1;
                                    string str = this.myStockReceive.DocNo.ToString();
                                    objArray[index2] = (object)str;
                                    string string2 = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                                    string detail = "";
                                    Activity.Log(dbSetting, docType, docKey, eventKey, string2, detail);
                                }
                                this.SetControlState();
                            }
                            catch (DBConcurrencyException ex)
                            {
                                DocumentEditHelper.HandleDBConcurrencyException(this.myDBSetting, "RPA_RCV", this.myStockReceive.DocKey);
                            }
                            catch (AppException ex)
                            {
                                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                            }
                            finally
                            {
                                this.EnableAutoSave();
                            }
                        }
                    }
                }
                   else
                {
                    if (this.myStockReceive.Cancelled)
                    {
                        // ISSUE: variable of a boxed type
                        StockReceiveStringId local1 = StockReceiveStringId.ConfirmMessage_UncancelStockReceiveAction;
                        object[] objArray = new object[1];
                        int index = 0;
                        // ISSUE: variable of a boxed type
                        DBString local2 = this.myStockReceive.DocNo;
                        objArray[index] = (object)local2;
                        string1 = BCE.Localization.Localizer.GetString((Enum)local1, objArray);
                    }
                    else
                    {
                        // ISSUE: variable of a boxed type
                        StockReceiveStringId local1 = StockReceiveStringId.ConfirmMessage_CancelStockReceiveAction;
                        object[] objArray = new object[1];
                        int index = 0;
                        // ISSUE: variable of a boxed type
                        DBString local2 = this.myStockReceive.DocNo;
                        objArray[index] = (object)local2;
                        string1 = BCE.Localization.Localizer.GetString((Enum)local1, objArray);
                    }
                    if (AppMessage.ShowConfirmMessage(string1))
                    {
                        this.DisableAutoSave();
                        try
                        {
                            if (this.myStockReceive.Cancelled)
                            {
                                this.myStockReceive.UncancelDocument(this.myUserAuthentication.LoginUserID);
                                DBSetting dbSetting = this.myDBSetting;
                                string docType = "RV";

                                long eventKey = 0L;
                                // ISSUE: variable of a boxed type
                                StockReceiveString local = StockReceiveString.UncancelStockReceive;
                                object[] objArray = new object[2];
                                int index1 = 0;
                                string loginUserId = this.myUserAuthentication.LoginUserID;
                                objArray[index1] = (object)loginUserId;
                                int index2 = 1;
                                string str = this.myStockReceive.DocNo.ToString();
                                objArray[index2] = (object)str;
                                string string2 = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                                string detail = "";
                                Activity.Log(dbSetting, docType, docKey, eventKey, string2, detail);
                            }
                            else
                            {
                                this.myStockReceive.CancelDocument(this.myUserAuthentication.LoginUserID);
                                DBSetting dbSetting = this.myDBSetting;
                                string docType = "RV";
                                // long docKey = this.myStockReceive.DocKey;
                                long eventKey = 0L;
                                // ISSUE: variable of a boxed type
                                StockReceiveString local = StockReceiveString.CancelStockReceive;
                                object[] objArray = new object[2];
                                int index1 = 0;
                                string loginUserId = this.myUserAuthentication.LoginUserID;
                                objArray[index1] = (object)loginUserId;
                                int index2 = 1;
                                string str = this.myStockReceive.DocNo.ToString();
                                objArray[index2] = (object)str;
                                string string2 = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                                string detail = "";
                                Activity.Log(dbSetting, docType, docKey, eventKey, string2, detail);
                            }
                            this.SetControlState();
                        }
                        catch (DBConcurrencyException ex)
                        {
                            DocumentEditHelper.HandleDBConcurrencyException(this.myDBSetting, "RPA_RCV", this.myStockReceive.DocKey);
                        }
                        catch (AppException ex)
                        {
                            AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                        }
                        finally
                        {
                            this.EnableAutoSave();
                        }
                    }
                }
            }
        }

        private void ItemSearch()
        {
            this.timer2.Stop();
            if (this.myFormItemSearch == null)
                this.myFormItemSearch = new FormItemSearch(this.myDBSetting, BCE.AutoCount.Stock.Item.Action.GeneralSearch);
            int num = (int)this.myFormItemSearch.ShowDialog((IWin32Window)this);
            if (this.myFormItemSearch.DialogResult == DialogResult.OK)
            {
                int seq = -1;
                this.gridViewStockDetail.BeginUpdate();
                this.myStockReceive.BeginLoadDetailData();

                try
                {
                    foreach (StockItemListElement stockItemListElement in this.myFormItemSearch.ResultList)
                    {
                        StockReceiveDetail stockReceiveDetail = this.myStockReceive.AddDetail(null);
                        stockReceiveDetail.ItemCode = BCE.Data.Convert.ToDBString((object)stockItemListElement.ItemCode);
                        stockReceiveDetail.UOM = BCE.Data.Convert.ToDBString((object)stockItemListElement.UOM);
                        if (stockItemListElement.Description != DBNull.Value)
                            stockReceiveDetail.Description = (DBString)stockItemListElement.Description.ToString();
                        if (stockItemListElement.FurtherDescription != DBNull.Value)
                            stockReceiveDetail.FurtherDescription = (DBString)stockItemListElement.FurtherDescription.ToString();
                        if (stockItemListElement.Cost != DBNull.Value)
                            stockReceiveDetail.UnitCost = stockItemListElement.Cost;
                        stockReceiveDetail.Location = (DBString)this.myUserAuthentication.MainLocation;
                        //StockReceiveRecalculateDetailEventArgs recalculateDetailEventArgs1 = new StockReceiveRecalculateDetailEventArgs(this.myDBSetting, stockReceiveDetail.Row);
                        //ScriptObject scriptObject = this.myStockReceive.ScriptObject;
                        //string name = "OnRecalculateDetail";
                        //System.Type[] types = new System.Type[1];
                        //int index1 = 0;
                        //System.Type type = recalculateDetailEventArgs1.GetType();
                        //types[index1] = type;
                        //object[] objArray = new object[1];
                        //int index2 = 0;
                        //StockReceiveRecalculateDetailEventArgs recalculateDetailEventArgs2 = recalculateDetailEventArgs1;
                        //objArray[index2] = (object)recalculateDetailEventArgs2;
                        //scriptObject.RunMethod(name, types, objArray);
                        seq = stockReceiveDetail.Seq;
                    }
                }
                finally
                {
                    this.myStockReceive.EndLoadDetailData();
                    this.gridViewStockDetail.EndUpdate();
                }
                if (seq > -1)
                    BCE.AutoCount.XtraUtils.GridViewUtils.SelectRowBySeq(this.gridViewStockDetail, seq);
            }
            this.timer2.Start();
        }

        private void sbtnItemSearch_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.ItemSearch));
        }

        private void FilterItemBatch()
        {
            if (this.gridViewStockDetail.FocusedColumn != null && (this.gridViewStockDetail.FocusedColumn.FieldName == "BatchNo" || this.gridViewStockDetail.FocusedColumn.FieldName == "UOM"))
            {
                DataRow dataRow = this.gridViewStockDetail.GetDataRow(this.gridViewStockDetail.FocusedRowHandle);
                if (dataRow != null)
                {
                    if (this.gridViewStockDetail.FocusedColumn.FieldName == "UOM")
                    {
                        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.FilterItemCode(dataRow["ItemCode"].ToString());
                    }
                    else
                    {
                        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.FilterItemCode(dataRow["ItemCode"].ToString(), (DateTime)this.myStockReceive.DocDate);
                       //// StockReceiveAfterFilterItemBatchEventArgs itemBatchEventArgs1 = new StockReceiveAfterFilterItemBatchEventArgs(DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchForReceiveLookupEditBuilder.GetDataTable(), this.myStockReceive, dataRow);
                       // ScriptObject scriptObject = this.myStockReceive.ScriptObject;
                       // string name = "AfterFilterItemBatch";
                       // System.Type[] types = new System.Type[1];
                       // int index1 = 0;
                       // System.Type type = itemBatchEventArgs1.GetType();
                       // types[index1] = type;
                       // object[] objArray = new object[1];
                       // int index2 = 0;
                       // StockReceiveAfterFilterItemBatchEventArgs itemBatchEventArgs2 = itemBatchEventArgs1;
                       // objArray[index2] = (object)itemBatchEventArgs2;
                       // scriptObject.RunMethod(name, types, objArray);
                    }
                }
            }
        }

        private void gridViewStockDetail_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            DataRow dataRow = this.gridViewStockDetail.GetDataRow(this.gridViewStockDetail.FocusedRowHandle);

            this.FilterItemBatch();
            if (this.gridViewStockDetail.FocusedColumn != null && dataRow!=null &&(this.gridViewStockDetail.FocusedColumn.FieldName == "BatchRM" || this.gridViewStockDetail.FocusedColumn.FieldName == "ItemCode"))
            {
                if (dataRow["FromDocDtlKey"] != null && dataRow["FromDocDtlKey"] != DBNull.Value)
                {
                    batchnolp = new BatchNoRMLookupEditBuilder();
                    batchnolp.BuildLookupEdit(repleBatchNo, myDBSetting);
                    batchnolp.FilterWONo(dataRow["FromDocDtlKey"].ToString());
                }
            }
        }

        private void gridViewStockDetail_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            this.FilterItemBatch();
            this.ucInquiryReload();
            this.SetDetailButtonState();
            //this.InitializeBatchNoMaster();
            DataRow dataRow = this.gridViewStockDetail.GetDataRow(this.gridViewStockDetail.FocusedRowHandle);
            if (dataRow != null)
                this.ToggleSerialNumberButtonVisibility(dataRow["ItemCode"].ToString());
            else
                this.ToggleSerialNumberButtonVisibility("");

            if (this.gridViewStockDetail.FocusedColumn != null && dataRow != null && (this.gridViewStockDetail.FocusedColumn.FieldName == "BatchRM" || this.gridViewStockDetail.FocusedColumn.FieldName == "ItemCode"))
            {
                if (dataRow["FromDocDtlKey"] != null && dataRow["FromDocDtlKey"] != DBNull.Value)
                {
                    batchnolp = new BatchNoRMLookupEditBuilder();
                    batchnolp.BuildLookupEdit(repleBatchNo, myDBSetting);
                    batchnolp.FilterWONo(dataRow["FromDocDtlKey"].ToString());
                }
            }
        }

        private void ucInquiryReload()
        {
            if (this.ucInquiryStock.Visible)
            {
                DataRow dataRow = this.gridViewStockDetail.GetDataRow(this.gridViewStockDetail.FocusedRowHandle);
                if (dataRow != null)
                    this.ucInquiryStock.Reload(dataRow["ItemCode"].ToString(), dataRow["UOM"].ToString(), "", "");
            }
        }

        private void repositoryItemButtonEditFurtherDescription_ButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            if (this.EndGridEdit())
            {
                int focusedRowHandle = this.gridViewStockDetail.FocusedRowHandle;
                if (this.gridViewStockDetail.GetDataRow(focusedRowHandle) != null)
                {
                    using (BCE.AutoCount.CommonForms.FormRichTextEditor formRichTextEditor = new BCE.AutoCount.CommonForms.FormRichTextEditor(this.myDBSetting, this.myStockReceive.DataTableDetail, this.gridViewStockDetail.GetDataSourceRowIndex(focusedRowHandle), "FurtherDescription", "Further Description", this.myStockReceive.Action != StockReceiveAction.View))
                    {
                        int num = (int)formRichTextEditor.ShowDialog((IWin32Window)this);
                    }
                }
            }
        }

        private bool EndGridEdit()
        {
            if (this.myStockReceive == null)
                return false;
            else if (this.myStockReceive.Action == StockReceiveAction.View || this.myGridControl.FocusedView == null)
            {
                return true;
            }
            else
            {
                BaseView focusedView = this.myGridControl.FocusedView;
                try
                {
                    if (focusedView.IsEditing)
                    {
                        if (!focusedView.ValidateEditor())
                            return false;
                        else
                            focusedView.CloseEditor();
                    }
                    return focusedView.UpdateCurrentRow();
                }
                catch
                {
                    return false;
                }
            }
        }

        private bool EndDocumentEdit()
        {
            if (this.myStockReceive == null)
                return false;
            else if (this.myStockReceive.Action == StockReceiveAction.View)
            {
                return true;
            }
            else
            {
                //Control control = this.ActiveControl;
                //while (control != null && !(control is BaseEdit))
                //{
                //    if (control.Parent != null)
                //        control = control.Parent;
                //    else
                //        break;
                //}
                //if (control is BaseEdit && !(control as BaseEdit).DoValidate() || !this.myLastValidationSuccess)
                //{
                //    return false;
                //}
                //else
                {
                    CurrencyManager currencyManager = (CurrencyManager)this.BindingContext[(object)this.myStockReceive.DataTableMaster];
                    if (currencyManager != null)
                    {
                        currencyManager.EndCurrentEdit();
                        currencyManager.Refresh();
                    }
                    return this.EndGridEdit();
                }
            }
        }

        private string GetSelectedDetailsXml()
        {
            DataSet selectedDetailsDataSet = this.GetSelectedDetailsDataSet();
            if (selectedDetailsDataSet != null)
            {
                StringWriter stringWriter = new StringWriter();
                selectedDetailsDataSet.WriteXml((TextWriter)stringWriter, XmlWriteMode.WriteSchema);
                return stringWriter.ToString();
            }
            else
                return "";
        }

        private DataSet GetSelectedDetailsDataSet()
        {
            int[] selectedRows = this.gridViewStockDetail.GetSelectedRows();
            if (selectedRows == null || selectedRows.Length == 0)
            {
                return (DataSet)null;
            }
            else
            {
                DataSet dataSet = this.myStockReceive.StockReceiveDataSet.Clone();
                DataTable dataTable = dataSet.Tables[1];
                for (int index = 0; index < selectedRows.Length; ++index)
                {
                    DataRow dataRow = this.gridViewStockDetail.GetDataRow(selectedRows[index]);
                    if (dataRow != null)
                        dataTable.ImportRow(dataRow);
                }
                return dataSet;
            }
        }

        private void panelHeader_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.EndDocumentEdit())
            {
                this.myLastDragDataSet = this.myStockReceive.StockReceiveDataSet;
                int num = (int)this.DoDragDrop((object)new DocumentCarrier(this.myLastDragDataSet, true), DragDropEffects.Copy);
            }
        }

        private void gridViewStockDetail_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.gridViewStockDetail.CalcHitInfo(new Point(e.X, e.Y)).InRow)
                this.myCanDrag = true;
        }

        private void gridViewStockDetail_MouseUp(object sender, MouseEventArgs e)
        {
            this.myCanDrag = false;
        }

        private void gridViewStockDetail_ShowingEditor(object sender, CancelEventArgs e)
        {
            this.myCanDrag = false;
        }

        private void gridViewStockDetail_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left && this.myCanDrag && this.EndDocumentEdit())
            {
                this.myLastDragDataSet = this.gridViewStockDetail.SelectedRowsCount != 0 ? this.GetSelectedDetailsDataSet() : this.myStockReceive.StockReceiveDataSet;
                if (this.myLastDragDataSet != null)
                {
                    int num = (int)this.DoDragDrop((object)new DocumentCarrier(this.myLastDragDataSet, false), DragDropEffects.Copy);
                }
            }
        }

        private void FormStockReceiveEntry_DragOver(object sender, DragEventArgs e)
        {
            if (this.myStockReceive.Action == StockReceiveAction.View)
                e.Effect = DragDropEffects.None;
            else if (e.Data.GetDataPresent(typeof(DocumentCarrier)))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void Drop()
        {
            this.gridViewStockDetail.BeginUpdate();
            try
            {
                this.myStockReceive.ImportFromDataSet(this.myDropDocCarrier.CarriedDataSet, this.myDropDocCarrier.AcceptWholeDocument);
            }
            finally
            {
                this.gridViewStockDetail.EndUpdate();
            }
        }

        private void FormStockReceiveEntry_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(DocumentCarrier)))
            {
                this.myDropDocCarrier = (DocumentCarrier)e.Data.GetData(typeof(DocumentCarrier));
                if (this.myLastDragDataSet != this.myDropDocCarrier.CarriedDataSet)
                    this.ExecuteWithPauseUndo(new MethodInvoker(this.Drop));
            }
        }

        private void barSubItem1_Popup(object sender, EventArgs e)
        {
            this.barItemCopySelectedDetails.Enabled = this.gridViewStockDetail.SelectedRowsCount > 0;
            this.barItemPasteItemDetailOnly.Enabled = this.myStockReceive.Action != StockReceiveAction.View;
            this.barItemPasteWholeDocument.Enabled = this.myStockReceive.Action != StockReceiveAction.View;
            this.barBtnSaveInKIV.Enabled = this.myStockReceive.Action != StockReceiveAction.View;
           // this.barCheckReallocatePurchaseByProject.Checked = this.myStockReceive.ReallocatePurchaseByProject;
        }

        private void barItemCopyWholeDocument_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.EndDocumentEdit())
                ClipboardHelper.SetDataObject((object)this.myStockReceive.ExportAsXml());
        }

        private void barItemCopySelectedDetails_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.EndGridEdit())
                ClipboardHelper.SetDataObject((object)this.GetSelectedDetailsXml());
        }

        private void PasteFromClipboard(bool wholeDocument)
        {
            IDataObject dataObject;
            try
            {
                dataObject = Clipboard.GetDataObject();
            }
            catch (Exception ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message + BCE.Localization.Localizer.GetString(StockReceiveStringId.ErrorMessage_Pleasetryagain, new object[0]));
                return;
            }
            if (dataObject.GetDataPresent(DataFormats.UnicodeText))
            {
                string str = (string)dataObject.GetData(DataFormats.UnicodeText);
                this.gridViewStockDetail.BeginUpdate();
                try
                {
                    this.myStockReceive.ImportFromXml(str, wholeDocument);
                }
                catch (XmlException ex1)
                {
                    try
                    {
                        if (!this.myStockReceive.ImportFromTabDelimitedText(str, wholeDocument))
                            AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString(StockReceiveStringId.ShowMessage_InvalidTextFormat, new object[0]));
                    }
                    catch (DataAccessException ex2)
                    {
                        AppMessage.ShowErrorMessage((IWin32Window)this, ex2.Message);
                    }
                }
                finally
                {
                    this.gridViewStockDetail.EndUpdate();
                }
            }
        }

        private void PasteWholeDocument()
        {
            this.PasteFromClipboard(true);
        }

        private void barItemPasteWholeDocument_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.EndDocumentEdit())
                this.ExecuteWithPauseUndo(new MethodInvoker(this.PasteWholeDocument));
        }

        private void PasteItemDetailOnly()
        {
            this.PasteFromClipboard(false);
        }

        private void barItemPasteItemDetailOnly_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.EndGridEdit())
                this.ExecuteWithPauseUndo(new MethodInvoker(this.PasteItemDetailOnly));
        }

        private void barSubItem2_Popup(object sender, EventArgs e)
        {
            this.barItemCopyFrom.Enabled = this.myStockReceive.Action != StockReceiveAction.View;
        }

        private void CopyFrom()
        {
            using (FormStockReceiveSearch stockReceiveSearch = new FormStockReceiveSearch(this.myStockReceive.Command, this.myDBSetting, "Copy from other Stock Receive", false))
            {
                if (stockReceiveSearch.ShowDialog((IWin32Window)this) == DialogResult.OK)
                {
                    StockReceive fromDoc = this.myStockReceive.Command.Edit(stockReceiveSearch.SelectedDocKeys[0]);
                    this.myStockReceive.ImportFromDataSet(fromDoc.StockReceiveDataSet, true);
                    //FormStockReceiveEntry.AfterCopyFromOtherDocumentEventArgs documentEventArgs1 = new FormStockReceiveEntry.AfterCopyFromOtherDocumentEventArgs(this.myStockReceive, fromDoc);
                    //ScriptObject scriptObject1 = this.myScriptObject;
                    //string name1 = "AfterCopyFromOtherDocument";
                    //System.Type[] types1 = new System.Type[1];
                    //int index1 = 0;
                    //System.Type type1 = documentEventArgs1.GetType();
                    //types1[index1] = type1;
                    //object[] objArray1 = new object[1];
                    //int index2 = 0;
                    //FormStockReceiveEntry.AfterCopyFromOtherDocumentEventArgs documentEventArgs2 = documentEventArgs1;
                    //objArray1[index2] = (object)documentEventArgs2;
                    //scriptObject1.RunMethod(name1, types1, objArray1);
                    //ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
                    //string name2 = "AfterCopyFromOtherDocument";
                    //System.Type[] types2 = new System.Type[1];
                    //int index3 = 0;
                    //System.Type type2 = documentEventArgs1.GetType();
                    //types2[index3] = type2;
                    //object[] objArray2 = new object[1];
                    //int index4 = 0;
                    //FormStockReceiveEntry.AfterCopyFromOtherDocumentEventArgs documentEventArgs3 = documentEventArgs1;
                    //objArray2[index4] = (object)documentEventArgs3;
                    //scriptObject2.RunMethod(name2, types2, objArray2);
                }
            }
        }

        private void barItemCopyFrom_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.EndDocumentEdit())
                this.ExecuteWithPauseUndo(new MethodInvoker(this.CopyFrom));
        }

        private void barItemCopyTo_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.EndDocumentEdit())
            {
                try
                {
                    if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_NEW", true))
                    {
                        StockReceive stockReceive = this.myStockReceive.Command.AddNew();
                        if (stockReceive != null)
                        {
                            stockReceive.ImportFromDataSet(this.myStockReceive.StockReceiveDataSet, true);
                            //FormStockReceiveEntry.AfterCopyToNewDocumentEventArgs documentEventArgs1 = new FormStockReceiveEntry.AfterCopyToNewDocumentEventArgs(stockReceive, this.myStockReceive);
                            //ScriptObject scriptObject1 = this.myScriptObject;
                            //string name1 = "AfterCopyToNewDocument";
                            //System.Type[] types1 = new System.Type[1];
                            //int index1 = 0;
                            //System.Type type1 = documentEventArgs1.GetType();
                            //types1[index1] = type1;
                            //object[] objArray1 = new object[1];
                            //int index2 = 0;
                            //FormStockReceiveEntry.AfterCopyToNewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
                            //objArray1[index2] = (object)documentEventArgs2;
                            //scriptObject1.RunMethod(name1, types1, objArray1);
                            //ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
                            //string name2 = "AfterCopyToNewDocument";
                            //System.Type[] types2 = new System.Type[1];
                            //int index3 = 0;
                            //System.Type type2 = documentEventArgs1.GetType();
                            //types2[index3] = type2;
                            //object[] objArray2 = new object[1];
                            //int index4 = 0;
                            //FormStockReceiveEntry.AfterCopyToNewDocumentEventArgs documentEventArgs3 = documentEventArgs1;
                            //objArray2[index4] = (object)documentEventArgs3;
                            //scriptObject2.RunMethod(name2, types2, objArray2);
                            FormStockReceiveCmd.StartEntryForm(stockReceive);
                        }
                    }
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                }
            }
        }

        private void iUndoMaster_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.myStockReceive.Action != StockReceiveAction.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.myMasterRecordUndo.Undo();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void CalcSubTotal(DataRow row, string[] columnChanged)
        {
            if (columnChanged.Length != 0)
                this.myStockReceive.CalcSubTotal(row);
        }

        private void sbtnUndo_Click(object sender, EventArgs e)
        {
            if (this.myStockReceive.Action != StockReceiveAction.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gridViewStockDetail.BeginUpdate();
                    this.myStockReceive.BeginLoadDetailData();
                    this.myDetailRecordUndo.Undo();
                    this.myStockReceive.EndLoadDetailData();
                    this.gridViewStockDetail.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void RangeSet()
        {
            System.Collections.Generic.List<string> noUOMItemCodeList = new System.Collections.Generic.List<string>();
            this.myStockReceive.BeginLoadDetailData();
            FormStockDocumentRangeSet.RangeSet((IWin32Window)this, this.gridViewStockDetail, this.myDBSetting, new FillRowValueDelegate(this.CalcSubTotal), noUOMItemCodeList);
            this.myStockReceive.EndLoadDetailData();
            if (noUOMItemCodeList.Count > 0)
            {
                FormStockReceiveEntry formStockReceiveEntry = this;
                // ISSUE: variable of a boxed type
                StockReceiveStringId  local=StockReceiveStringId.InfoMessage_NoHaveSelectedUOM;
                object[] objArray = new object[1];
                int index = 0;
                string str = string.Join(", ", noUOMItemCodeList.ToArray());
                objArray[index] = (object)str;
                string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                AppMessage.ShowInformationMessage((IWin32Window)formStockReceiveEntry, @string);
            }
        }

        private void sbtnBulkSet_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.RangeSet));
        }

        private void sbtnSelectAll_Click(object sender, EventArgs e)
        {
            this.gridViewStockDetail.SelectAll();
            this.myGridControl.Focus();
        }

        private void gridViewStockDetail_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.myStockReceive != null)
                this.sbtnRangeSet.Enabled = this.myStockReceive.Action != StockReceiveAction.View && this.gridViewStockDetail.SelectedRowsCount > 0;
        }

        private void iEditMRU_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelper.EditMRUItems();
        }

        private void ExecuteWithPauseUndo(MethodInvoker sDelegate)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            if (this.myMasterRecordUndo != null)
                this.myMasterRecordUndo.PauseCapture();
            if (this.myDetailRecordUndo != null)
                this.myDetailRecordUndo.PauseCapture();
            try
            {
                sDelegate();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
            finally
            {
                if (this.myMasterRecordUndo != null)
                {
                    this.myMasterRecordUndo.ResumeCapture();
                    if (!this.myMasterRecordUndo.IsPauseCapture())
                        this.myMasterRecordUndo.SaveState();
                }
                if (this.myDetailRecordUndo != null)
                {
                    this.myDetailRecordUndo.ResumeCapture();
                    if (!this.myDetailRecordUndo.IsPauseCapture())
                        this.myDetailRecordUndo.SaveState();
                }
                Cursor.Current = current;
            }
        }

        private void repositoryItemLookUpEditItem_EditValueChanged(object sender, EventArgs e)
        {
            if (this.myItemCodeFromPopup)
            {
                this.EndGridEdit();
                this.myItemCodeFromPopup = false;
            }
            this.ucInquiryReload();
            DevExpress.XtraEditors.LookUpEdit editor = sender as DevExpress.XtraEditors.LookUpEdit;
            object value = editor.GetColumnValue("BOMCode");
            DataRow drDetail = gridViewStockDetail.GetDataRow(gridViewStockDetail.FocusedRowHandle);
            drDetail["BOMCode"] = value;
        }

        private void luEdtDocNoFormat_EditValueChanged(object sender, EventArgs e)
        {
            if (this.myStockReceive != null)
                this.myStockReceive.DocNoFormatName = this.luEdtDocNoFormat.Text;
            this.SetWindowCaption();
        }

        private void repositoryItemLookUpEditItem_CloseUp(object sender, CloseUpEventArgs e)
        {
            if (e.AcceptValue)
                this.myItemCodeFromPopup = true;
        }

        private void FormStockReceiveEntry_Deactivate(object sender, EventArgs e)
        {
            this.myHasDeactivated = true;
        }

        private void sbtnShowInstantInfo_Click(object sender, EventArgs e)
        {
            this.ucInquiryStock.Visible = !this.ucInquiryStock.Visible;
            this.splitterControl1.Visible = this.ucInquiryStock.Visible;
            if (this.ucInquiryStock.Visible)
            {
                lblTotal.Visible = false;
                textEdtTotal.Visible = false;
                this.panelBottom.Height = this.myInstantInfoHeight;
                this.sbtnShowInstantInfo.Text = BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_ShowFooter, new object[0]);
                this.ucInquiryReload();
            }
            else
            {
                lblTotal.Visible = true;
                textEdtTotal.Visible = true;
                this.panelBottom.Height =100;
                this.sbtnShowInstantInfo.Text = BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_ShowInstantInfo, new object[0]);
            }
        }

        private void repositoryItemLookUpEditItemUOM_EditValueChanged(object sender, EventArgs e)
        {
            this.ucInquiryReload();
        }

        private void sbtnSave_Click(object sender, EventArgs e)
        {
            if (this.Save(SaveDocumentAction.Save))
                this.CheckContinueToNewDocument();
            else
                this.DialogResult = DialogResult.None;
        }

        private void sbtnSavePreview_Click(object sender, EventArgs e)
        {
            if (this.Save(SaveDocumentAction.SaveAndPreview))
                this.CheckContinueToNewDocument();
            else
                this.DialogResult = DialogResult.None;
        }

        private void sbtnSavePrint_Click(object sender, EventArgs e)
        {
            if (this.Save(SaveDocumentAction.SaveAndPrint))
                this.CheckContinueToNewDocument();
            else
                this.DialogResult = DialogResult.None;
        }

        private void CheckContinueToNewDocument()
        {
            if (this.chkedtNextRecord.Visible && this.chkedtNextRecord.Checked)
            {
                StockReceive newStockReceive = this.myStockReceive.Command.AddNew();
                if (newStockReceive != null)
                {
                    this.SetStockReceive(newStockReceive);
                    this.FocusFirstControl();
                    return;
                }
            }
            this.Close();
        }

        private void gridViewStockDetail_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            BCE.AutoCount.XtraUtils.GridViewUtils.DefaultDrawEmptyForeground(e, BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_StockReceive, new object[0]));
        }

        public bool SaveToKIVFolder(string saveReason)
        {
            if (!this.EndDocumentEdit())
            {
                return false;
            }
            else
            {
                try
                {
                    int num = this.SaveTempDocument(saveReason) ? 1 : 0;
                    if (num != 0)
                        this.mySaveInKIV = true;
                    return num != 0;
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                    return false;
                }
            }
        }

        private bool SaveTempDocument(string saveReason)
        {
            if (this.myInProcessTempDocument > 0)
            {
                return false;
            }
            else
            {
                FormStockReceiveEntry formStockReceiveEntry = this;
                bool lockTaken = false;
                try
                {
                    Monitor.Enter((object)formStockReceiveEntry, ref lockTaken);
                    this.myInProcessTempDocument = this.myInProcessTempDocument + 1;
                    try
                    {
                        this.myStockReceive.SaveToTempDocument(saveReason);
                    }
                    finally
                    {
                        if (this.myInProcessTempDocument > 0)
                            this.myInProcessTempDocument = this.myInProcessTempDocument - 1;
                    }
                }
                finally
                {
                    if (lockTaken)
                        Monitor.Exit((object)formStockReceiveEntry);
                }
                return true;
            }
        }

        private void DeleteTempDocument()
        {
            FormStockReceiveEntry formStockReceiveEntry = this;
            bool lockTaken = false;
            try
            {
                Monitor.Enter((object)formStockReceiveEntry, ref lockTaken);
                this.myInProcessTempDocument = this.myInProcessTempDocument + 1;
                try
                {
                    TempDocument.Delete(this.myDBSetting, this.myStockReceive.DocKey);
                }
                finally
                {
                    if (this.myInProcessTempDocument > 0)
                        this.myInProcessTempDocument = this.myInProcessTempDocument - 1;
                }
            }
            finally
            {
                if (lockTaken)
                    Monitor.Exit((object)formStockReceiveEntry);
            }
        }

        private void barBtnSaveInKIV_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.SaveToKIVFolder("K.I.V."))
                this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.Visible && this.myStockReceive.Action != StockReceiveAction.View && TempDocumentSetting.Default.EnableAutoSave)
            {
                if (!this.IsAutoSaveDisabled())
                {
                    try
                    {
                        this.SaveTempDocument("Auto-save");
                    }
                    catch (Exception ex)
                    {
                        StandardExceptionHandler.WriteExceptionToErrorLog(ex);
                    }
                }
            }
        }

        private void navigator_ButtonClick(object sender,BCE.Controls.NavigatorButtonClickEventArgs e)
        {
            StockReceive newStockReceive = (StockReceive)null;
            try
            {
                if (e.ButtonType == BCE.Controls.NavigatorButtonType.First)
                    newStockReceive = this.myStockReceive.Command.ViewFirst();
                else if (e.ButtonType == BCE.Controls.NavigatorButtonType.Prev)
                    newStockReceive = this.myStockReceive.Command.ViewPrev((string)this.myStockReceive.DocNo);
                else if (e.ButtonType == BCE.Controls.NavigatorButtonType.Next)
                    newStockReceive = this.myStockReceive.Command.ViewNext((string)this.myStockReceive.DocNo);
                else if (e.ButtonType == BCE.Controls.NavigatorButtonType.Last)
                    newStockReceive = this.myStockReceive.Command.ViewLast();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
                return;
            }
            if (newStockReceive != null)
                this.SetStockReceive(newStockReceive);
        }

        private void barCheckReallocatePurchaseByProject_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            //bool purchaseByProject = this.myStockReceive.ReallocatePurchaseByProject;
            ///this.myStockReceive.ReallocatePurchaseByProject = this.barCheckReallocatePurchaseByProject.Checked;
            //if (this.myStockReceive.ReallocatePurchaseByProject && this.myStockReceive.ReallocatePurchaseByProject != purchaseByProject)
            //{
            //    using (FormSelectReallocatePurchaseByProjectNo purchaseByProjectNo = new FormSelectReallocatePurchaseByProjectNo(this.myDBSetting, this.myStockReceive.DataTableMaster, "ReallocatePurchaseByProjectNo"))
            //    {
            //        int num = (int)purchaseByProjectNo.ShowDialog((IWin32Window)this);
            //    }
            //}
        }

        private void sbtnSerialNo_Click(object sender, EventArgs e)
        {
            if (this.gridViewStockDetail.FocusedRowHandle >= 0)
            {
                DataRow dataRow = this.gridViewStockDetail.GetDataRow(this.gridViewStockDetail.FocusedRowHandle);
                if (dataRow != null)
                    this.ShowSerialNumberEntryForm(dataRow);
            }
        }

        private void ShowSerialNumberEntryForm(DataRow dr)
        {
            if (dr != null)
            {
                Decimal itemRate = InvoicingHelper.GetItemRate(this.myDBSetting, dr["ItemCode"].ToString(), dr["UOM"].ToString());
                if (!(itemRate == Decimal.Zero))
                {
                    long reqQty = System.Convert.ToInt64(this.myDecimalSetting.RoundQuantity(itemRate * BCE.Data.Convert.ToDecimal(dr["Qty"])));
                    long dtlKey = BCE.Data.Convert.ToInt64(dr["DtlKey"]);
                    bool flag = true;
                    using (FormSerialNumberEntry serialNumberEntry = new FormSerialNumberEntry(this.myDBSetting, "RV", this.myStockReceive.DocKey, dtlKey, dr["ItemCode"].ToString(), dr["Description"].ToString(), reqQty, this.myStockReceive.DataTableSerialNo, this.myStockReceive.Action == StockReceiveAction.View, dr["Location"].ToString(), dr["BatchNo"].ToString(), this.myStockReceive.DataTableDetail, (DataTable)null))
                    {
                        int num = (int)serialNumberEntry.ShowDialog((IWin32Window)this);
                        flag = serialNumberEntry.NeedToUpdateQty;
                    }
                    if (this.myStockReceive.Action == StockReceiveAction.New || this.myStockReceive.Action == StockReceiveAction.Edit)
                    {
                        this.myStockReceive.BeginLoadDetailData();
                        try
                        {
                            if (flag)
                            {
                                long serialNumberCount = this.myStockReceive.GetSerialNumberCount(dtlKey);
                                dr["Qty"] = (object)this.myDecimalSetting.RoundQuantity((Decimal)serialNumberCount / itemRate);
                                this.myStockReceive.UpdateUnitCost(dr);
                                this.myStockReceive.CalcSubTotal(dr);
                                this.myStockReceive.UpdateSubTotal();
                            }
                            dr["SerialNoList"] = (object)SerialNumberHelper.GenerateSNMemoString(this.myStockReceive.DataTableSerialNo, dtlKey);
                        }
                        finally
                        {
                            this.myStockReceive.EndLoadDetailData();
                        }
                    }
                }
            }
        }

        private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_DOC_REPORT_PREVIEW", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockReceive.PrintCount <= 0 || this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_PRINTED_PREVIEW", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPreviewPrintedDocument, new object[0]))))
            {
                // ISSUE: variable of a boxed type
                StockReceiveString local1 = StockReceiveString.StockReceive;
                object[] objArray1 = new object[1];
                int index1 = 0;
                // ISSUE: variable of a boxed type
                DBString local2 = this.myStockReceive.DocNo;
                objArray1[index1] = (object)local2;
                ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum)local1, objArray1), "RPA_RCV_DOC_REPORT_PRINT", "RPA_RCV_DOC_REPORT_EXPORT", "");
                reportInfo.DocType = "RV";
                reportInfo.DocKey = this.myStockReceive.DocKey;
                reportInfo.UpdatePrintCountTableName = "RPA_RCV";
                reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
                reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
                reportInfo.EmailAndFaxInfo = StockReceiveCommand.GetEmailAndFaxInfo(this.myStockReceive.DocKey, this.myDBSetting);
                //BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs documentEventArgs1 = new BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs(reportInfo.EmailAndFaxInfo, reportInfo.DocKey, this.myDBSetting);
                //ScriptObject scriptObject1 = this.myScriptObject;
                //string name1 = "BeforePreviewDocument";
                //System.Type[] types1 = new System.Type[1];
                //int index2 = 0;
                //System.Type type1 = documentEventArgs1.GetType();
                //types1[index2] = type1;
                //object[] objArray2 = new object[1];
                //int index3 = 0;
                //BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
                //objArray2[index3] = (object)documentEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray2);
                //ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
                //string name2 = "BeforePreviewDocument";
                //System.Type[] types2 = new System.Type[1];
                //int index4 = 0;
                //System.Type type2 = documentEventArgs1.GetType();
                //types2[index4] = type2;
                //object[] objArray3 = new object[1];
                //int index5 = 0;
                //BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs documentEventArgs3 = documentEventArgs1;
                //objArray3[index5] = (object)documentEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray3);
                //reportInfo.Tag = (object)documentEventArgs1;
                ReportTool.PreviewReport("Stock Receive Document", this.myStockReceive.Command.GetReportDataSource(this.myStockReceive.DocKey), this.myDBSetting, e.DefaultReport, false, this.myStockReceive.Command.ReportOption, reportInfo);
            }
        }

        private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_DOC_REPORT_PRINT", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int)this.myStockReceive.PrintCount <= 0 || this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_PRINTED_PRINT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0]))))
            {
                // ISSUE: variable of a boxed type
                StockReceiveString local1 = StockReceiveString.StockReceive;
                object[] objArray = new object[1];
                int index = 0;
                // ISSUE: variable of a boxed type
                DBString local2 = this.myStockReceive.DocNo;
                objArray[index] = (object)local2;
                ReportTool.PrintReport("Stock Receive Document", this.myStockReceive.Command.GetReportDataSource(this.myStockReceive.DocKey), this.myDBSetting, e.DefaultReport, this.myStockReceive.Command.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum)local1, objArray), "RPA_RCV_DOC_REPORT_PRINT", "RPA_RCV_DOC_REPORT_EXPORT", "")
                {
                    DocType = "RV",
                    DocKey = this.myStockReceive.DocKey,
                    UpdatePrintCountTableName = "RPA_RCV",
                    EmailAndFaxInfo = StockReceiveCommand.GetEmailAndFaxInfo(this.myStockReceive.DocKey, this.myDBSetting)
                });
            }
        }

        private void hyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            int[] numArray1 = this.gridViewStockDetail.GetSelectedRows();
            if (numArray1.Length == 0 && this.gridViewStockDetail.FocusedRowHandle >= 0)
            {
                int[] numArray2 = new int[1];
                int index = 0;
                int focusedRowHandle = this.gridViewStockDetail.FocusedRowHandle;
                numArray2[index] = focusedRowHandle;
                numArray1 = numArray2;
            }
            for (int index = 0; index < numArray1.Length; ++index)
            {
                DataRow dataRow = this.gridViewStockDetail.GetDataRow(numArray1[index]);
                if (dataRow != null)
                    this.myStockReceive.UpdateUnitCostWithReferenceCost(dataRow);
            }
        }

        private void hyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            int[] numArray1 = this.gridViewStockDetail.GetSelectedRows();
            if (numArray1.Length == 0 && this.gridViewStockDetail.FocusedRowHandle >= 0)
            {
                int[] numArray2 = new int[1];
                int index = 0;
                int focusedRowHandle = this.gridViewStockDetail.FocusedRowHandle;
                numArray2[index] = focusedRowHandle;
                numArray1 = numArray2;
            }
            for (int index = 0; index < numArray1.Length; ++index)
            {
                DataRow dataRow = this.gridViewStockDetail.GetDataRow(numArray1[index]);
                if (dataRow != null)
                    this.myStockReceive.UpdateUnitCostWithUpToDateCost(dataRow);
            }
        }

        private void splitterControl1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (this.splitterControl1.Visible)
            {
                this.myInstantInfoHeight = this.panelBottom.Height;
                this.SaveLocalSetting();
            }
        }

        private void txtEdtStockReceiveNo_Validated(object sender, EventArgs e)
        {
            this.SetWindowCaption();
        }

        private void scanBarcodeControl1_OnBarCodeInput(object sender, BarCodeInputEventArgs e)
        {
            if (this.myStockReceive != null)
            {
                bool flag = false;
                if (this.myIsAutomaticMergeSameItemCode)
                    flag = InvoicingHelper.PerformScanBarcodeItemMerge(e, this.myStockReceive.DataTableDetail);
                if (!flag)
                {
                    this.sbtnAddDTL.PerformClick();
                    if (e.ItemCode.HasValue)
                        this.gridViewStockDetail.SetFocusedRowCellValue(this.colItemCode, (object)e.ItemCode.ToString());
                    if (e.UOM.HasValue)
                        this.gridViewStockDetail.SetFocusedRowCellValue(this.colUOM, (object)e.UOM.ToString());
                    if (e.BatchNo.HasValue)
                        this.gridViewStockDetail.SetFocusedRowCellValue(this.colBatchNo, (object)e.BatchNo.ToString());
                    if (e.Qty.HasValue)
                        this.gridViewStockDetail.SetFocusedRowCellValue(this.colQty, (object)(Decimal)e.Qty);
                    if (e.UnitPrice.HasValue)
                        this.gridViewStockDetail.SetFocusedRowCellValue(this.colUnitCost, (object)(Decimal)e.UnitPrice);
                    if (e.SubTotal.HasValue)
                        this.gridViewStockDetail.SetFocusedRowCellValue(this.colSubTotal, (object)(Decimal)e.SubTotal);
                }
            }
        }

        private void iEditRemark1MRU_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelperForRemark1.EditMRUItems();
        }

        private void iEditRemark2MRU_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelperForRemark2.EditMRUItems();
        }

        private void iEditRemark3MRU_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelperForRemark3.EditMRUItems();
        }

        private void iEditRemark4MRU_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myMRUHelperForRemark4.EditMRUItems();
        }

        private void txtEdtStockReceiveNo_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Control && e.KeyCode == (Keys.Back | Keys.Space) || e.KeyCode == (Keys)115) && this.luEdtDocNoFormat.Visible)
            {
                this.luEdtDocNoFormat.ShowPopup();
                this.luEdtDocNoFormat.Focus();
                e.Handled = true;
            }
        }

        private void gridViewStockDetail_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {

            DataRow drDetail = gridViewStockDetail.GetDataRow(e.RowHandle);
            if (e.Column == this.colItemCode && e.Value != null)
            {
                this.ToggleSerialNumberButtonVisibility(e.Value.ToString());
                int index = repItemLkEdt_ItemCode.GetDataSourceRowIndex("ItemCode", e.Value);
                object obj = repItemLkEdt_ItemCode.GetDataSourceValue("DocType", index);
                if (obj != null && obj != DBNull.Value)
                    drDetail["FromDocType"] = obj;
                else
                    drDetail["FromDocType"] = null;
                obj = repItemLkEdt_ItemCode.GetDataSourceValue("Code", index);
                if (obj != null && obj != DBNull.Value)
                    drDetail["FromDocDtlKey"] = obj;
                else
                    drDetail["FromDocDtlKey"] = DBNull.Value;
                obj = repItemLkEdt_ItemCode.GetDataSourceValue("WONo", index);
                if (obj != null && obj != DBNull.Value)
                    drDetail["FromDocNo"] = obj;
                else
                    drDetail["FromDocNo"] = DBNull.Value;
                this.InitializeBatchNoMaster();
                if (drDetail["FromDocDtlKey"] != null && drDetail["FromDocDtlKey"] != DBNull.Value)
                {
                    batchnolp = new BatchNoRMLookupEditBuilder();
                    batchnolp.BuildLookupEdit(repleBatchNo, myDBSetting);
                    batchnolp.FilterWONo(drDetail["FromDocDtlKey"].ToString());
                }

                //DataRow drMaster = DataTableMaster.Rows[0];
                //DataRow dr = myDBSetting.GetFirstDataRow("select * from ItemBatch where ItemCode=? and BatchNo=?", (object)sItemCode, (object)luBatchNo.EditValue);
                //if (dr != null)
                //{

                //    obj = myDBSetting.ExecuteScalar("select HasBatchNo from item where itemcode=?", (object)drDetail["ItemCode"]);
                //    if (obj != null && obj != DBNull.Value)
                //    {
                //        if (BCE.Data.Convert.TextToBoolean(obj))
                //        {
                //            //if()
                //            obj = myDBSetting.ExecuteScalar("select count(*) from ItemBatch where ItemCode=? and BatchNo=?", (object)drDetail["ItemCode"], (object)luBatchNo.EditValue);
                //            if (obj != null && obj != DBNull.Value)
                //            {
                //                if (BCE.Data.Convert.ToDecimal(obj) == 0)
                //                {
                //                    ItemBatchNo itembatchno = ItemBatchNo.Create(myDBSetting);
                //                    itembatchno.Load(drDetail["ItemCode"].ToString());
                //                    itembatchno.Add(dr["BatchNo"].ToString(), dr["Description"].ToString(), (object)dr["ManufacturedDate"], (object)dr["ExpiryDate"], (object)dr["LastSaleDate"]);
                //                    itembatchno.Save();
                //                }
                //            }
                //            drDetail["BatchNo"] = luBatchNo.EditValue;
                //            //itembatchno.Add(drdtl["BatchNo"].ToString(),)
                //        }
                //    }


                //}

            }


            if (e.Column == this.colBatchRM && e.Value != null)
            {
               
            }


        }

        private void FormStockReceiveEntry_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.AdjustBottomButtons();
                //FormStockReceiveEntry.FormShowEventArgs formShowEventArgs1 = new FormStockReceiveEntry.FormShowEventArgs(this, this.myStockReceive);
                //ScriptObject scriptObject1 = this.myScriptObject;
                //string name1 = "OnFormShow";
                //System.Type[] types1 = new System.Type[1];
                //int index1 = 0;
                //System.Type type1 = formShowEventArgs1.GetType();
                //types1[index1] = type1;
                //object[] objArray1 = new object[1];
                //int index2 = 0;
                //FormStockReceiveEntry.FormShowEventArgs formShowEventArgs2 = formShowEventArgs1;
                //objArray1[index2] = (object)formShowEventArgs2;
                //scriptObject1.RunMethod(name1, types1, objArray1);
                //ScriptObject scriptObject2 = this.myStockReceive.ScriptObject;
                //string name2 = "OnFormShow";
                //System.Type[] types2 = new System.Type[1];
                //int index3 = 0;
                //System.Type type2 = formShowEventArgs1.GetType();
                //types2[index3] = type2;
                //object[] objArray2 = new object[1];
                //int index4 = 0;
                //FormStockReceiveEntry.FormShowEventArgs formShowEventArgs3 = formShowEventArgs1;
                //objArray2[index4] = (object)formShowEventArgs3;
                //scriptObject2.RunMethod(name2, types2, objArray2);
            }
        }

        private bool CheckBeforePrint(ReportInfo reportInfo)
        {
            if (!((BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs)reportInfo.Tag).AllowPrint || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RCV", reportInfo.DocKey) > 0 && !this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_PRINTED_PRINT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0])))
                return false;
            else
                return true;
        }

        private bool CheckBeforeExport(ReportInfo reportInfo)
        {
            if (!((BCE.AutoCount.Stock.StockReceive.BeforePreviewDocumentEventArgs)reportInfo.Tag).AllowExport || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RCV", reportInfo.DocKey) > 0 && !this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCV_PRINTED_EXPORT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedExportPrintedDocument, new object[0])))
                return false;
            else
                return true;
        }

        private void dateEdtDate_EditValueChanged(object sender, EventArgs e)
        {
            if (this.myStockReceive.Command != null)
                this.myIsBeforeStartDate = BCE.Data.Convert.ToDateTime(this.dateEdtDate.EditValue) < this.myStockReceive.Command.myFiscalYear.FiscalYearStartDate;
        }

        private void DisableAutoSave()
        {
            this.myDisableAutoSaveCounter = this.myDisableAutoSaveCounter + 1;
        }

        private void EnableAutoSave()
        {
            if (this.myDisableAutoSaveCounter > 0)
                this.myDisableAutoSaveCounter = this.myDisableAutoSaveCounter - 1;
        }

        private bool IsAutoSaveDisabled()
        {
            return this.myDisableAutoSaveCounter > 0;
        }

        private void dateEdtDate_Validating(object sender, CancelEventArgs e)
        {
            if (this.myStockReceive.Command != null)
            {
                DateEdit dateEdit1 = sender as DateEdit;
                FiscalYear orCreate = FiscalYear.GetOrCreate(this.myDBSetting);
                if (!orCreate.IsValidTransactionDate(dateEdit1.DateTime))
                {
                    DateTime firstDate;
                    DateTime lastDate;
                    if (orCreate.GetValidTransactionDateRange(out firstDate, out lastDate))
                    {
                        DateEdit dateEdit2 = dateEdit1;
                        // ISSUE: variable of a boxed type
                        InvoicingStringId  local=InvoicingStringId.ErrorMessage_ValidDocumentDate;
                        object[] objArray = new object[2];
                        int index1 = 0;
                        string str1 = this.myStockReceive.Command.GeneralSetting.FormatDate(firstDate);
                        objArray[index1] = (object)str1;
                        int index2 = 1;
                        string str2 = this.myStockReceive.Command.GeneralSetting.FormatDate(lastDate);
                        objArray[index2] = (object)str2;
                        string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                        dateEdit2.ErrorText = @string;
                    }
                    else
                        dateEdit1.ErrorText = BCE.Localization.Localizer.GetString((Enum)InvoicingStringId.ErrorMessage_InvalidDocumentDate, new object[0]);
                    e.Cancel = true;
                    this.myLastValidationSuccess = false;
                }
                else
                    this.myLastValidationSuccess = true;
            }
        }

        private void gridViewStockDetail_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (sender != null && e.Value != null && e.Value != DBNull.Value)
            {
                GridView gridView = sender as GridView;
                this.mySkipExecuteFormActivated = true;
                try
                {
                    if (gridView.FocusedColumn.FieldName == "ItemCode" && !this.myUseLookupEditToInputItemCode && (e.Value.ToString().Length > 0 && !FormPartialMatchItemCode.IsValidAndActiveItemCode(this.myDBSetting, e.Value.ToString())))
                    {
                        DataTable partialMatchItemCode1 = FormPartialMatchItemCode.GetPartialMatchItemCode(this.myDBSetting, e.Value.ToString());
                        if (partialMatchItemCode1 == null || partialMatchItemCode1.Rows.Count == 0)
                        {
                            e.ErrorText = BCE.Localization.Localizer.GetString((Enum)FormPartialMatchItemCodeStringId.ErrorMessage_InvalidOrInactiveItemCode, new object[0]);
                            AppMessage.ShowErrorMessage(e.ErrorText);
                            e.Valid = false;
                        }
                        else if (partialMatchItemCode1.Rows.Count == 1)
                        {
                            e.Value = (object)partialMatchItemCode1.Rows[0]["ItemCode"].ToString();
                        }
                        else
                        {
                            using (FormPartialMatchItemCode partialMatchItemCode2 = new FormPartialMatchItemCode(this.myDBSetting, partialMatchItemCode1, e.Value.ToString(), true))
                            {
                                if (partialMatchItemCode2.ShowDialog() == DialogResult.OK)
                                {
                                    e.Value = (object)partialMatchItemCode2.SelectedItemCode;
                                }
                                else
                                {
                                    e.ErrorText = BCE.Localization.Localizer.GetString((Enum)FormPartialMatchItemCodeStringId.ErrorMessage_InvalidOrInactiveItemCode, new object[0]);
                                    e.Valid = false;
                                }
                            }
                        }
                    }
                }
                finally
                {
                    this.mySkipExecuteFormActivated = false;
                }
            }
        }

        private void barItemCopyAsTabDelimitedText_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.EndGridEdit())
            {
                ClipboardHelper.SetDataObject((object)this.myStockReceive.ExportAsTabDelimiterText());
                AppMessage.ShowInformationMessage(BCE.Localization.Localizer.GetString(StockReceiveStringId.InfoMessage_SwitchToExcelToPaste, new object[0]));
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.timer2.Stop();
            if (this.myFormItemSearch != null)
            {
                this.myFormItemSearch.Dispose();
                this.myFormItemSearch = (FormItemSearch)null;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!this.myHasUnlinkLookupEditEventHandlers)
            {
                this.UnlinkLookupEditEventHandlers();
                this.myHasUnlinkLookupEditEventHandlers = true;
            }
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStockReceiveEntry));
            this.panelHeader = new DevExpress.XtraEditors.PanelControl();
            this.luWONo = new DevExpress.XtraEditors.LookUpEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.luMachineCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEdtStockReceiveNo = new DevExpress.XtraEditors.TextEdit();
            this.lblCancelled = new System.Windows.Forms.Label();
            this.dateEdtDate = new DevExpress.XtraEditors.DateEdit();
            this.lblRefDocNo = new System.Windows.Forms.Label();
            this.lblStockAdjNo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.mruEdtDescription = new DevExpress.XtraEditors.MRUEdit();
            this.luEdtDocNoFormat = new DevExpress.XtraEditors.LookUpEdit();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel2 = new DevExpress.XtraEditors.PanelControl();
            this.btnPrint = new BCE.AutoCount.Controls.PrintButton();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSavePreview = new DevExpress.XtraEditors.SimpleButton();
            this.btnPreview = new BCE.AutoCount.Controls.PreviewButton();
            this.sbtnCancelDoc = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSave = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSavePrint = new DevExpress.XtraEditors.SimpleButton();
            this.navigator = new BCE.Controls.Navigator();
            this.sbtnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.chkedtNextRecord = new DevExpress.XtraEditors.CheckEdit();
            this.tabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageMain = new DevExpress.XtraTab.XtraTabPage();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.myGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridViewStockDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBatchNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemLkEdt_ItemBatch = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDeptNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemLkEdt_Dept = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFurtherDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemBtnEdt_FurtherDescription = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemLkEdt_ItemCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemLkEdt_Location = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colNumbering = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemCkEdt_General = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colProjNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemLkEdt_ProjNo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWIPCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeq = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNoList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemLkEdt_ItemUOM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDebit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemLkEdt_AccNo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colCredit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDocNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDocType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDocDtlKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coBOMCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBatchRM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleBatchNo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelBottom = new DevExpress.XtraEditors.PanelControl();
            this.lblTotal = new System.Windows.Forms.Label();
            this.textEdtTotal = new DevExpress.XtraEditors.TextEdit();
            this.ucInquiryStock = new BCE.AutoCount.Inquiry.UserControls.UCInquiryStock();
            this.panel5 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnSelectAll = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUndo = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddDTL = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDeleteDTL = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDown = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnUp = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInsertBefore = new DevExpress.XtraEditors.SimpleButton();
            this.hylinkEditUpdateRealCost = new DevExpress.XtraEditors.HyperLinkEdit();
            this.hylinkEditUpdateReferenceCost = new DevExpress.XtraEditors.HyperLinkEdit();
            this.sbtnShowInstantInfo = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnRangeSet = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnItemSearch = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSerialNo = new DevExpress.XtraEditors.SimpleButton();
            this.scanBarcodeControl1 = new BCE.AutoCount.Stock.ScanBarcodeControl();
            this.tabPageMoreHeader = new DevExpress.XtraTab.XtraTabPage();
            this.labelRemark4 = new System.Windows.Forms.Label();
            this.labelRemark3 = new System.Windows.Forms.Label();
            this.labelRemark2 = new System.Windows.Forms.Label();
            this.labelRemark1 = new System.Windows.Forms.Label();
            this.mruEdtRemark1 = new DevExpress.XtraEditors.MRUEdit();
            this.mruEdtRemark2 = new DevExpress.XtraEditors.MRUEdit();
            this.mruEdtRemark3 = new DevExpress.XtraEditors.MRUEdit();
            this.mruEdtRemark4 = new DevExpress.XtraEditors.MRUEdit();
            this.tabPageExternalLink = new DevExpress.XtraTab.XtraTabPage();
            this.externalLinkBox1 = new BCE.Controls.ExternalLinkBox();
            this.tabPageNote = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdtNote = new BCE.Controls.MemoEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barItemCopyWholeDocument = new DevExpress.XtraBars.BarButtonItem();
            this.barItemCopySelectedDetails = new DevExpress.XtraBars.BarButtonItem();
            this.barItemCopyAsTabDelimitedText = new DevExpress.XtraBars.BarButtonItem();
            this.barItemPasteWholeDocument = new DevExpress.XtraBars.BarButtonItem();
            this.barItemPasteItemDetailOnly = new DevExpress.XtraBars.BarButtonItem();
            this.iUndoMaster = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckReallocatePurchaseByProject = new DevExpress.XtraBars.BarCheckItem();
            this.barBtnSaveInKIV = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.iEditMRU = new DevExpress.XtraBars.BarButtonItem();
            this.iEditRemark1MRU = new DevExpress.XtraBars.BarButtonItem();
            this.iEditRemark2MRU = new DevExpress.XtraBars.BarButtonItem();
            this.iEditRemark3MRU = new DevExpress.XtraBars.BarButtonItem();
            this.iEditRemark4MRU = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barItemCopyFrom = new DevExpress.XtraBars.BarButtonItem();
            this.barItemCopyTo = new DevExpress.XtraBars.BarButtonItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelHeader)).BeginInit();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luWONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luMachineCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEdtStockReceiveNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdtDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdtDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luEdtDocNoFormat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtNextRecord.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStockDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_ItemBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemBtnEdt_FurtherDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_ItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_Location)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemCkEdt_General)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_ProjNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_ItemUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_AccNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleBatchNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelBottom)).BeginInit();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel5)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hylinkEditUpdateRealCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hylinkEditUpdateReferenceCost.Properties)).BeginInit();
            this.tabPageMoreHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark4.Properties)).BeginInit();
            this.tabPageExternalLink.SuspendLayout();
            this.tabPageNote.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelHeader.Controls.Add(this.luWONo);
            this.panelHeader.Controls.Add(this.luMachineCode);
            this.panelHeader.Controls.Add(this.label5);
            this.panelHeader.Controls.Add(this.txtEdtStockReceiveNo);
            this.panelHeader.Controls.Add(this.lblCancelled);
            this.panelHeader.Controls.Add(this.dateEdtDate);
            this.panelHeader.Controls.Add(this.lblRefDocNo);
            this.panelHeader.Controls.Add(this.lblStockAdjNo);
            this.panelHeader.Controls.Add(this.lblDate);
            this.panelHeader.Controls.Add(this.lblDescription);
            this.panelHeader.Controls.Add(this.mruEdtDescription);
            this.panelHeader.Controls.Add(this.luEdtDocNoFormat);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1006, 102);
            this.panelHeader.TabIndex = 1;
            this.panelHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelHeader_MouseDown);
            // 
            // luWONo
            // 
            this.luWONo.Location = new System.Drawing.Point(101, 44);
            this.luWONo.MenuManager = this.barManager1;
            this.luWONo.Name = "luWONo";
            this.luWONo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luWONo.Size = new System.Drawing.Size(118, 20);
            this.luWONo.TabIndex = 25;
            this.luWONo.EditValueChanged += new System.EventHandler(this.luWONo_EditValueChanged);
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 23;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1006, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 581);
            this.barDockControlBottom.Size = new System.Drawing.Size(1006, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 581);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1006, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 581);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            // 
            // luMachineCode
            // 
            this.luMachineCode.Location = new System.Drawing.Point(101, 68);
            this.luMachineCode.Name = "luMachineCode";
            this.luMachineCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luMachineCode.Size = new System.Drawing.Size(118, 20);
            this.luMachineCode.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(14, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 15);
            this.label5.TabIndex = 22;
            this.label5.Text = "Machine No";
            // 
            // txtEdtStockReceiveNo
            // 
            this.txtEdtStockReceiveNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEdtStockReceiveNo.Location = new System.Drawing.Point(839, 20);
            this.txtEdtStockReceiveNo.Name = "txtEdtStockReceiveNo";
            this.txtEdtStockReceiveNo.Size = new System.Drawing.Size(152, 20);
            this.txtEdtStockReceiveNo.TabIndex = 0;
            this.txtEdtStockReceiveNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEdtStockReceiveNo_KeyDown);
            this.txtEdtStockReceiveNo.Validated += new System.EventHandler(this.txtEdtStockReceiveNo_Validated);
            // 
            // lblCancelled
            // 
            this.lblCancelled.BackColor = System.Drawing.Color.Transparent;
            this.lblCancelled.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCancelled.ForeColor = System.Drawing.Color.Red;
            this.lblCancelled.Location = new System.Drawing.Point(520, 33);
            this.lblCancelled.Name = "lblCancelled";
            this.lblCancelled.Size = new System.Drawing.Size(187, 30);
            this.lblCancelled.TabIndex = 1;
            this.lblCancelled.Text = "CANCELLED";
            // 
            // dateEdtDate
            // 
            this.dateEdtDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEdtDate.EditValue = new System.DateTime(2019, 3, 18, 0, 0, 0, 0);
            this.dateEdtDate.Location = new System.Drawing.Point(839, 43);
            this.dateEdtDate.Name = "dateEdtDate";
            this.dateEdtDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdtDate.Properties.NullDate = "";
            this.dateEdtDate.Size = new System.Drawing.Size(152, 20);
            this.dateEdtDate.TabIndex = 3;
            this.dateEdtDate.EditValueChanged += new System.EventHandler(this.dateEdtDate_EditValueChanged);
            this.dateEdtDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdtDate_Validating);
            this.dateEdtDate.Validated += new System.EventHandler(this.txtEdtStockReceiveNo_Validated);
            // 
            // lblRefDocNo
            // 
            this.lblRefDocNo.BackColor = System.Drawing.Color.Transparent;
            this.lblRefDocNo.Location = new System.Drawing.Point(14, 46);
            this.lblRefDocNo.Name = "lblRefDocNo";
            this.lblRefDocNo.Size = new System.Drawing.Size(64, 18);
            this.lblRefDocNo.TabIndex = 4;
            this.lblRefDocNo.Text = "No. WO";
            // 
            // lblStockAdjNo
            // 
            this.lblStockAdjNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStockAdjNo.BackColor = System.Drawing.Color.Transparent;
            this.lblStockAdjNo.Location = new System.Drawing.Point(686, 23);
            this.lblStockAdjNo.Name = "lblStockAdjNo";
            this.lblStockAdjNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblStockAdjNo.Size = new System.Drawing.Size(147, 23);
            this.lblStockAdjNo.TabIndex = 5;
            this.lblStockAdjNo.Text = "Stock Receive No";
            // 
            // lblDate
            // 
            this.lblDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(733, 46);
            this.lblDate.Name = "lblDate";
            this.lblDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDate.Size = new System.Drawing.Size(100, 23);
            this.lblDate.TabIndex = 6;
            this.lblDate.Text = "Date";
            // 
            // lblDescription
            // 
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Location = new System.Drawing.Point(14, 23);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(64, 20);
            this.lblDescription.TabIndex = 7;
            this.lblDescription.Text = "Description";
            // 
            // mruEdtDescription
            // 
            this.mruEdtDescription.Location = new System.Drawing.Point(101, 20);
            this.mruEdtDescription.Name = "mruEdtDescription";
            this.mruEdtDescription.Size = new System.Drawing.Size(333, 20);
            this.mruEdtDescription.TabIndex = 8;
            this.mruEdtDescription.SelectedIndexChanged += new System.EventHandler(this.mruEdtDescription_SelectedIndexChanged);
            // 
            // luEdtDocNoFormat
            // 
            this.luEdtDocNoFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.luEdtDocNoFormat.Location = new System.Drawing.Point(839, 20);
            this.luEdtDocNoFormat.Name = "luEdtDocNoFormat";
            this.luEdtDocNoFormat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luEdtDocNoFormat.Size = new System.Drawing.Size(152, 20);
            this.luEdtDocNoFormat.TabIndex = 9;
            this.luEdtDocNoFormat.TabStop = false;
            this.luEdtDocNoFormat.EditValueChanged += new System.EventHandler(this.luEdtDocNoFormat_EditValueChanged);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel2.Controls.Add(this.btnPrint);
            this.panel2.Controls.Add(this.sbtnCancel);
            this.panel2.Controls.Add(this.sbtnSavePreview);
            this.panel2.Controls.Add(this.btnPreview);
            this.panel2.Controls.Add(this.sbtnCancelDoc);
            this.panel2.Controls.Add(this.sbtnDelete);
            this.panel2.Controls.Add(this.sbtnSave);
            this.panel2.Controls.Add(this.sbtnSavePrint);
            this.panel2.Controls.Add(this.navigator);
            this.panel2.Controls.Add(this.sbtnEdit);
            this.panel2.Controls.Add(this.chkedtNextRecord);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 540);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1006, 41);
            this.panel2.TabIndex = 2;
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(572, 8);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.ReportType = "";
            this.btnPrint.Size = new System.Drawing.Size(72, 23);
            this.btnPrint.TabIndex = 0;
            this.btnPrint.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnCancel.Location = new System.Drawing.Point(916, 8);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.sbtnCancel.TabIndex = 1;
            this.sbtnCancel.Text = "Cancel";
            this.sbtnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sbtnSavePreview
            // 
            this.sbtnSavePreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnSavePreview.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnSavePreview.Location = new System.Drawing.Point(546, 8);
            this.sbtnSavePreview.Name = "sbtnSavePreview";
            this.sbtnSavePreview.Size = new System.Drawing.Size(98, 23);
            this.sbtnSavePreview.TabIndex = 2;
            this.sbtnSavePreview.Text = "Save && Preview";
            this.sbtnSavePreview.Click += new System.EventHandler(this.sbtnSavePreview_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreview.Location = new System.Drawing.Point(494, 8);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.ReportType = "";
            this.btnPreview.Size = new System.Drawing.Size(72, 23);
            this.btnPreview.TabIndex = 3;
            this.btnPreview.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // sbtnCancelDoc
            // 
            this.sbtnCancelDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancelDoc.Location = new System.Drawing.Point(733, 8);
            this.sbtnCancelDoc.Name = "sbtnCancelDoc";
            this.sbtnCancelDoc.Size = new System.Drawing.Size(93, 23);
            this.sbtnCancelDoc.TabIndex = 4;
            this.sbtnCancelDoc.Text = "Cancel Doc";
            this.sbtnCancelDoc.Click += new System.EventHandler(this.btnCancelDoc_Click);
            // 
            // sbtnDelete
            // 
            this.sbtnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnDelete.Appearance.ForeColor = System.Drawing.Color.Red;
            this.sbtnDelete.Appearance.Options.UseForeColor = true;
            this.sbtnDelete.Location = new System.Drawing.Point(403, 8);
            this.sbtnDelete.Name = "sbtnDelete";
            this.sbtnDelete.Size = new System.Drawing.Size(75, 23);
            this.sbtnDelete.TabIndex = 5;
            this.sbtnDelete.Text = "Delete";
            this.sbtnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // sbtnSave
            // 
            this.sbtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnSave.Location = new System.Drawing.Point(310, 8);
            this.sbtnSave.Name = "sbtnSave";
            this.sbtnSave.Size = new System.Drawing.Size(75, 23);
            this.sbtnSave.TabIndex = 6;
            this.sbtnSave.Text = "Save";
            this.sbtnSave.Click += new System.EventHandler(this.sbtnSave_Click);
            // 
            // sbtnSavePrint
            // 
            this.sbtnSavePrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnSavePrint.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnSavePrint.Location = new System.Drawing.Point(837, 8);
            this.sbtnSavePrint.Name = "sbtnSavePrint";
            this.sbtnSavePrint.Size = new System.Drawing.Size(75, 23);
            this.sbtnSavePrint.TabIndex = 7;
            this.sbtnSavePrint.Text = "Save && Print";
            this.sbtnSavePrint.Click += new System.EventHandler(this.sbtnSavePrint_Click);
            // 
            // navigator
            // 
            this.navigator.Location = new System.Drawing.Point(9, 8);
            this.navigator.Name = "navigator";
            this.navigator.Size = new System.Drawing.Size(92, 23);
            this.navigator.TabIndex = 8;
            this.navigator.ButtonClick += new BCE.Controls.NavigatorButtonClickEventHandler(this.navigator_ButtonClick);
            // 
            // sbtnEdit
            // 
            this.sbtnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnEdit.Location = new System.Drawing.Point(650, 8);
            this.sbtnEdit.Name = "sbtnEdit";
            this.sbtnEdit.Size = new System.Drawing.Size(80, 23);
            this.sbtnEdit.TabIndex = 9;
            this.sbtnEdit.Text = "Edit";
            this.sbtnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // chkedtNextRecord
            // 
            this.chkedtNextRecord.Location = new System.Drawing.Point(10, 8);
            this.chkedtNextRecord.Name = "chkedtNextRecord";
            this.chkedtNextRecord.Properties.Caption = "After &save, proceed with new Stock Receive";
            this.chkedtNextRecord.Size = new System.Drawing.Size(284, 19);
            this.chkedtNextRecord.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabPage = this.tabPageMain;
            this.tabControl1.Size = new System.Drawing.Size(1006, 438);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageMain,
            this.tabPageMoreHeader,
            this.tabPageExternalLink,
            this.tabPageNote});
            // 
            // tabPageMain
            // 
            this.tabPageMain.Controls.Add(this.splitterControl1);
            this.tabPageMain.Controls.Add(this.myGridControl);
            this.tabPageMain.Controls.Add(this.panelBottom);
            this.tabPageMain.Controls.Add(this.panel5);
            this.tabPageMain.Controls.Add(this.scanBarcodeControl1);
            this.tabPageMain.Name = "tabPageMain";
            this.tabPageMain.Size = new System.Drawing.Size(1000, 410);
            this.tabPageMain.Text = "Main";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitterControl1.Location = new System.Drawing.Point(0, 285);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1000, 5);
            this.splitterControl1.TabIndex = 0;
            this.splitterControl1.TabStop = false;
            this.splitterControl1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitterControl1_SplitterMoved);
            // 
            // myGridControl
            // 
            this.myGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.myGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.myGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.myGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.myGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.myGridControl.Location = new System.Drawing.Point(0, 73);
            this.myGridControl.MainView = this.gridViewStockDetail;
            this.myGridControl.Name = "myGridControl";
            this.myGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repItemCkEdt_General,
            this.repItemBtnEdt_FurtherDescription,
            this.repItemLkEdt_Dept,
            this.repItemLkEdt_ItemBatch,
            this.repItemLkEdt_ItemCode,
            this.repItemLkEdt_ItemUOM,
            this.repItemLkEdt_Location,
            this.repItemLkEdt_ProjNo,
            this.repItemLkEdt_AccNo,
            this.repleBatchNo});
            this.myGridControl.Size = new System.Drawing.Size(1000, 217);
            this.myGridControl.TabIndex = 1;
            this.myGridControl.UseEmbeddedNavigator = true;
            this.myGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStockDetail});
            this.myGridControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.myGridControl_KeyDown);
            // 
            // gridViewStockDetail
            // 
            this.gridViewStockDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBatchNo,
            this.colDeptNo,
            this.colDescription,
            this.colFurtherDescription,
            this.colItemCode,
            this.colLocation,
            this.colNumbering,
            this.colPrintOut,
            this.colProjNo,
            this.colQty,
            this.colWIPCost,
            this.colSeq,
            this.colSerialNoList,
            this.colSubTotal,
            this.colUnitCost,
            this.colUOM,
            this.colDebit,
            this.colCredit,
            this.colFromDocNo,
            this.colFromDocType,
            this.colFromDocDtlKey,
            this.coBOMCode,
            this.colBatchRM});
            this.gridViewStockDetail.GridControl = this.myGridControl;
            this.gridViewStockDetail.Name = "gridViewStockDetail";
            this.gridViewStockDetail.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewStockDetail_CustomDrawCell);
            this.gridViewStockDetail.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewStockDetail_CustomRowCellEdit);
            this.gridViewStockDetail.CustomRowCellEditForEditing += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewStockDetail_CustomRowCellEditForEditing);
            this.gridViewStockDetail.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewStockDetail_SelectionChanged);
            this.gridViewStockDetail.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridViewStockDetail_CustomDrawEmptyForeground);
            this.gridViewStockDetail.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewStockDetail_ShowingEditor);
            this.gridViewStockDetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewStockDetail_FocusedRowChanged);
            this.gridViewStockDetail.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gridViewStockDetail_FocusedColumnChanged);
            this.gridViewStockDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewStockDetail_CellValueChanged);
            this.gridViewStockDetail.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridViewStockDetail_MouseDown);
            this.gridViewStockDetail.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewStockDetail_MouseUp);
            this.gridViewStockDetail.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridViewStockDetail_MouseMove);
            this.gridViewStockDetail.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewStockDetail_ValidatingEditor);
            // 
            // colBatchNo
            // 
            this.colBatchNo.ColumnEdit = this.repItemLkEdt_ItemBatch;
            this.colBatchNo.FieldName = "BatchNo";
            this.colBatchNo.Name = "colBatchNo";
            this.colBatchNo.Visible = true;
            this.colBatchNo.VisibleIndex = 2;
            this.colBatchNo.Width = 63;
            // 
            // repItemLkEdt_ItemBatch
            // 
            this.repItemLkEdt_ItemBatch.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLkEdt_ItemBatch.Name = "repItemLkEdt_ItemBatch";
            // 
            // colDeptNo
            // 
            this.colDeptNo.ColumnEdit = this.repItemLkEdt_Dept;
            this.colDeptNo.FieldName = "DeptNo";
            this.colDeptNo.Name = "colDeptNo";
            // 
            // repItemLkEdt_Dept
            // 
            this.repItemLkEdt_Dept.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLkEdt_Dept.Name = "repItemLkEdt_Dept";
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 130;
            // 
            // colFurtherDescription
            // 
            this.colFurtherDescription.ColumnEdit = this.repItemBtnEdt_FurtherDescription;
            this.colFurtherDescription.FieldName = "FurtherDescription";
            this.colFurtherDescription.MinWidth = 21;
            this.colFurtherDescription.Name = "colFurtherDescription";
            this.colFurtherDescription.Visible = true;
            this.colFurtherDescription.VisibleIndex = 3;
            this.colFurtherDescription.Width = 77;
            // 
            // repItemBtnEdt_FurtherDescription
            // 
            this.repItemBtnEdt_FurtherDescription.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repItemBtnEdt_FurtherDescription.Name = "repItemBtnEdt_FurtherDescription";
            this.repItemBtnEdt_FurtherDescription.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repItemBtnEdt_FurtherDescription.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditFurtherDescription_ButtonPressed);
            this.repItemBtnEdt_FurtherDescription.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditFurtherDescription_ButtonPressed);
            // 
            // colItemCode
            // 
            this.colItemCode.ColumnEdit = this.repItemLkEdt_ItemCode;
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 0;
            this.colItemCode.Width = 78;
            // 
            // repItemLkEdt_ItemCode
            // 
            this.repItemLkEdt_ItemCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLkEdt_ItemCode.Name = "repItemLkEdt_ItemCode";
            this.repItemLkEdt_ItemCode.ValidateOnEnterKey = true;
            this.repItemLkEdt_ItemCode.CloseUp += new DevExpress.XtraEditors.Controls.CloseUpEventHandler(this.repositoryItemLookUpEditItem_CloseUp);
            this.repItemLkEdt_ItemCode.EditValueChanged += new System.EventHandler(this.repositoryItemLookUpEditItem_EditValueChanged);
            // 
            // colLocation
            // 
            this.colLocation.ColumnEdit = this.repItemLkEdt_Location;
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 4;
            this.colLocation.Width = 52;
            // 
            // repItemLkEdt_Location
            // 
            this.repItemLkEdt_Location.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLkEdt_Location.Name = "repItemLkEdt_Location";
            // 
            // colNumbering
            // 
            this.colNumbering.FieldName = "Numbering";
            this.colNumbering.Name = "colNumbering";
            // 
            // colPrintOut
            // 
            this.colPrintOut.ColumnEdit = this.repItemCkEdt_General;
            this.colPrintOut.FieldName = "PrintOut";
            this.colPrintOut.Name = "colPrintOut";
            // 
            // repItemCkEdt_General
            // 
            this.repItemCkEdt_General.Caption = "Check";
            this.repItemCkEdt_General.Name = "repItemCkEdt_General";
            this.repItemCkEdt_General.ValueChecked = "T";
            this.repItemCkEdt_General.ValueUnchecked = "F";
            // 
            // colProjNo
            // 
            this.colProjNo.ColumnEdit = this.repItemLkEdt_ProjNo;
            this.colProjNo.FieldName = "ProjNo";
            this.colProjNo.Name = "colProjNo";
            this.colProjNo.Visible = true;
            this.colProjNo.VisibleIndex = 5;
            this.colProjNo.Width = 67;
            // 
            // repItemLkEdt_ProjNo
            // 
            this.repItemLkEdt_ProjNo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLkEdt_ProjNo.Name = "repItemLkEdt_ProjNo";
            // 
            // colQty
            // 
            this.colQty.Caption = "Qty";
            this.colQty.FieldName = "Qty";
            this.colQty.Name = "colQty";
            this.colQty.Visible = true;
            this.colQty.VisibleIndex = 7;
            this.colQty.Width = 42;
            // 
            // colWIPCost
            // 
            this.colWIPCost.Caption = "WIPCost";
            this.colWIPCost.Name = "colWIPCost";
            this.colWIPCost.OptionsColumn.AllowEdit = false;
            this.colWIPCost.Visible = true;
            this.colWIPCost.VisibleIndex = 10;
            this.colWIPCost.Width = 59;
            // 
            // colSeq
            // 
            this.colSeq.FieldName = "Seq";
            this.colSeq.Name = "colSeq";
            // 
            // colSerialNoList
            // 
            this.colSerialNoList.FieldName = "SerialNoList";
            this.colSerialNoList.Name = "colSerialNoList";
            // 
            // colSubTotal
            // 
            this.colSubTotal.FieldName = "SubTotal";
            this.colSubTotal.Name = "colSubTotal";
            this.colSubTotal.OptionsColumn.AllowEdit = false;
            this.colSubTotal.Visible = true;
            this.colSubTotal.VisibleIndex = 9;
            this.colSubTotal.Width = 90;
            // 
            // colUnitCost
            // 
            this.colUnitCost.FieldName = "UnitCost";
            this.colUnitCost.Name = "colUnitCost";
            this.colUnitCost.Visible = true;
            this.colUnitCost.VisibleIndex = 8;
            this.colUnitCost.Width = 67;
            // 
            // colUOM
            // 
            this.colUOM.ColumnEdit = this.repItemLkEdt_ItemUOM;
            this.colUOM.FieldName = "UOM";
            this.colUOM.Name = "colUOM";
            this.colUOM.Visible = true;
            this.colUOM.VisibleIndex = 6;
            this.colUOM.Width = 46;
            // 
            // repItemLkEdt_ItemUOM
            // 
            this.repItemLkEdt_ItemUOM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLkEdt_ItemUOM.Name = "repItemLkEdt_ItemUOM";
            this.repItemLkEdt_ItemUOM.EditValueChanged += new System.EventHandler(this.repositoryItemLookUpEditItemUOM_EditValueChanged);
            // 
            // colDebit
            // 
            this.colDebit.Caption = "Debit";
            this.colDebit.ColumnEdit = this.repItemLkEdt_AccNo;
            this.colDebit.FieldName = "Debit";
            this.colDebit.Name = "colDebit";
            this.colDebit.Visible = true;
            this.colDebit.VisibleIndex = 11;
            this.colDebit.Width = 72;
            // 
            // repItemLkEdt_AccNo
            // 
            this.repItemLkEdt_AccNo.AutoHeight = false;
            this.repItemLkEdt_AccNo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLkEdt_AccNo.Name = "repItemLkEdt_AccNo";
            // 
            // colCredit
            // 
            this.colCredit.Caption = "Credit";
            this.colCredit.ColumnEdit = this.repItemLkEdt_AccNo;
            this.colCredit.FieldName = "Credit";
            this.colCredit.Name = "colCredit";
            this.colCredit.Visible = true;
            this.colCredit.VisibleIndex = 12;
            this.colCredit.Width = 101;
            // 
            // colFromDocNo
            // 
            this.colFromDocNo.Caption = "FromDocNo";
            this.colFromDocNo.FieldName = "FromDocNo";
            this.colFromDocNo.Name = "colFromDocNo";
            this.colFromDocNo.OptionsColumn.AllowEdit = false;
            // 
            // colFromDocType
            // 
            this.colFromDocType.Caption = "FromDocType";
            this.colFromDocType.FieldName = "FromDocType";
            this.colFromDocType.Name = "colFromDocType";
            this.colFromDocType.OptionsColumn.AllowEdit = false;
            // 
            // colFromDocDtlKey
            // 
            this.colFromDocDtlKey.Caption = "FromDocDtlKey";
            this.colFromDocDtlKey.FieldName = "FromDocDtlKey";
            this.colFromDocDtlKey.Name = "colFromDocDtlKey";
            this.colFromDocDtlKey.OptionsColumn.AllowEdit = false;
            this.colFromDocDtlKey.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // coBOMCode
            // 
            this.coBOMCode.Caption = "BOMCode";
            this.coBOMCode.FieldName = "BOMCode";
            this.coBOMCode.Name = "coBOMCode";
            this.coBOMCode.Visible = true;
            this.coBOMCode.VisibleIndex = 13;
            this.coBOMCode.Width = 89;
            // 
            // colBatchRM
            // 
            this.colBatchRM.Caption = "BatchRM";
            this.colBatchRM.ColumnEdit = this.repleBatchNo;
            this.colBatchRM.FieldName = "BatchRM";
            this.colBatchRM.Name = "colBatchRM";
            this.colBatchRM.Visible = true;
            this.colBatchRM.VisibleIndex = 14;
            // 
            // repleBatchNo
            // 
            this.repleBatchNo.AutoHeight = false;
            this.repleBatchNo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleBatchNo.DisplayMember = "BatchNo";
            this.repleBatchNo.Name = "repleBatchNo";
            this.repleBatchNo.ValueMember = "BatchNo";
            this.repleBatchNo.EditValueChanged += new System.EventHandler(this.repleBatchNo_EditValueChanged);
            // 
            // panelBottom
            // 
            this.panelBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelBottom.Controls.Add(this.lblTotal);
            this.panelBottom.Controls.Add(this.textEdtTotal);
            this.panelBottom.Controls.Add(this.ucInquiryStock);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 290);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1000, 120);
            this.panelBottom.TabIndex = 2;
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.Location = new System.Drawing.Point(832, 9);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(50, 15);
            this.lblTotal.TabIndex = 1;
            this.lblTotal.Text = "Total";
            // 
            // textEdtTotal
            // 
            this.textEdtTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdtTotal.Location = new System.Drawing.Point(888, 6);
            this.textEdtTotal.Name = "textEdtTotal";
            this.textEdtTotal.Size = new System.Drawing.Size(100, 20);
            this.textEdtTotal.TabIndex = 2;
            this.textEdtTotal.TabStop = false;
            // 
            // ucInquiryStock
            // 
            this.ucInquiryStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucInquiryStock.Location = new System.Drawing.Point(0, 0);
            this.ucInquiryStock.Name = "ucInquiryStock";
            this.ucInquiryStock.Size = new System.Drawing.Size(1000, 120);
            this.ucInquiryStock.TabIndex = 0;
            this.ucInquiryStock.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel5.Controls.Add(this.sbtnSelectAll);
            this.panel5.Controls.Add(this.sbtnUndo);
            this.panel5.Controls.Add(this.sbtnAddDTL);
            this.panel5.Controls.Add(this.sbtnDeleteDTL);
            this.panel5.Controls.Add(this.sbtnDown);
            this.panel5.Controls.Add(this.sbtnUp);
            this.panel5.Controls.Add(this.sbtnInsertBefore);
            this.panel5.Controls.Add(this.hylinkEditUpdateRealCost);
            this.panel5.Controls.Add(this.hylinkEditUpdateReferenceCost);
            this.panel5.Controls.Add(this.sbtnShowInstantInfo);
            this.panel5.Controls.Add(this.sbtnRangeSet);
            this.panel5.Controls.Add(this.sbtnItemSearch);
            this.panel5.Controls.Add(this.sbtnSerialNo);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 32);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1000, 41);
            this.panel5.TabIndex = 3;
            // 
            // sbtnSelectAll
            // 
            this.sbtnSelectAll.Image = ((System.Drawing.Image)(resources.GetObject("sbtnSelectAll.Image")));
            this.sbtnSelectAll.ImageIndex = 4;
            this.sbtnSelectAll.ImageList = this.imageList1;
            this.sbtnSelectAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnSelectAll.Location = new System.Drawing.Point(228, 8);
            this.sbtnSelectAll.Name = "sbtnSelectAll";
            this.sbtnSelectAll.Size = new System.Drawing.Size(29, 23);
            this.sbtnSelectAll.TabIndex = 13;
            this.sbtnSelectAll.TabStop = false;
            this.sbtnSelectAll.Click += new System.EventHandler(this.sbtnSelectAll_Click);
            // 
            // sbtnUndo
            // 
            this.sbtnUndo.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndo.Image")));
            this.sbtnUndo.ImageIndex = 3;
            this.sbtnUndo.ImageList = this.imageList1;
            this.sbtnUndo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnUndo.Location = new System.Drawing.Point(115, 8);
            this.sbtnUndo.Name = "sbtnUndo";
            this.sbtnUndo.Size = new System.Drawing.Size(33, 23);
            this.sbtnUndo.TabIndex = 14;
            this.sbtnUndo.TabStop = false;
            this.sbtnUndo.Click += new System.EventHandler(this.sbtnUndo_Click);
            // 
            // sbtnAddDTL
            // 
            this.sbtnAddDTL.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddDTL.Image")));
            this.sbtnAddDTL.ImageIndex = 0;
            this.sbtnAddDTL.ImageList = this.imageList1;
            this.sbtnAddDTL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddDTL.Location = new System.Drawing.Point(10, 8);
            this.sbtnAddDTL.Name = "sbtnAddDTL";
            this.sbtnAddDTL.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddDTL.TabIndex = 15;
            this.sbtnAddDTL.TabStop = false;
            this.sbtnAddDTL.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // sbtnDeleteDTL
            // 
            this.sbtnDeleteDTL.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeleteDTL.Image")));
            this.sbtnDeleteDTL.ImageIndex = 1;
            this.sbtnDeleteDTL.ImageList = this.imageList1;
            this.sbtnDeleteDTL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeleteDTL.Location = new System.Drawing.Point(80, 8);
            this.sbtnDeleteDTL.Name = "sbtnDeleteDTL";
            this.sbtnDeleteDTL.Size = new System.Drawing.Size(29, 23);
            this.sbtnDeleteDTL.TabIndex = 16;
            this.sbtnDeleteDTL.TabStop = false;
            this.sbtnDeleteDTL.Click += new System.EventHandler(this.sbtnDeleteDTL_Click);
            // 
            // sbtnDown
            // 
            this.sbtnDown.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDown.Image")));
            this.sbtnDown.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDown.Location = new System.Drawing.Point(189, 8);
            this.sbtnDown.Name = "sbtnDown";
            this.sbtnDown.Size = new System.Drawing.Size(32, 23);
            this.sbtnDown.TabIndex = 17;
            this.sbtnDown.TabStop = false;
            this.sbtnDown.Click += new System.EventHandler(this.sbtnDown_Click);
            // 
            // sbtnUp
            // 
            this.sbtnUp.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUp.Image")));
            this.sbtnUp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnUp.Location = new System.Drawing.Point(153, 8);
            this.sbtnUp.Name = "sbtnUp";
            this.sbtnUp.Size = new System.Drawing.Size(30, 23);
            this.sbtnUp.TabIndex = 18;
            this.sbtnUp.TabStop = false;
            this.sbtnUp.Text = "Up";
            this.sbtnUp.Click += new System.EventHandler(this.sbtnUp_Click);
            // 
            // sbtnInsertBefore
            // 
            this.sbtnInsertBefore.Image = ((System.Drawing.Image)(resources.GetObject("sbtnInsertBefore.Image")));
            this.sbtnInsertBefore.ImageIndex = 2;
            this.sbtnInsertBefore.ImageList = this.imageList1;
            this.sbtnInsertBefore.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnInsertBefore.Location = new System.Drawing.Point(42, 8);
            this.sbtnInsertBefore.Name = "sbtnInsertBefore";
            this.sbtnInsertBefore.Size = new System.Drawing.Size(32, 23);
            this.sbtnInsertBefore.TabIndex = 19;
            this.sbtnInsertBefore.TabStop = false;
            this.sbtnInsertBefore.Click += new System.EventHandler(this.sbtnInsertBefore_Click);
            // 
            // hylinkEditUpdateRealCost
            // 
            this.hylinkEditUpdateRealCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hylinkEditUpdateRealCost.EditValue = "Update Real Cost";
            this.hylinkEditUpdateRealCost.Location = new System.Drawing.Point(753, 13);
            this.hylinkEditUpdateRealCost.Name = "hylinkEditUpdateRealCost";
            this.hylinkEditUpdateRealCost.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.hylinkEditUpdateRealCost.Size = new System.Drawing.Size(129, 18);
            this.hylinkEditUpdateRealCost.TabIndex = 0;
            this.hylinkEditUpdateRealCost.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkEdit1_OpenLink);
            // 
            // hylinkEditUpdateReferenceCost
            // 
            this.hylinkEditUpdateReferenceCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hylinkEditUpdateReferenceCost.EditValue = "Update Reference Cost";
            this.hylinkEditUpdateReferenceCost.Location = new System.Drawing.Point(605, 13);
            this.hylinkEditUpdateReferenceCost.Name = "hylinkEditUpdateReferenceCost";
            this.hylinkEditUpdateReferenceCost.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.hylinkEditUpdateReferenceCost.Size = new System.Drawing.Size(142, 18);
            this.hylinkEditUpdateReferenceCost.TabIndex = 1;
            this.hylinkEditUpdateReferenceCost.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkEdit2_OpenLink);
            // 
            // sbtnShowInstantInfo
            // 
            this.sbtnShowInstantInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnShowInstantInfo.Location = new System.Drawing.Point(888, 8);
            this.sbtnShowInstantInfo.Name = "sbtnShowInstantInfo";
            this.sbtnShowInstantInfo.Size = new System.Drawing.Size(105, 23);
            this.sbtnShowInstantInfo.TabIndex = 2;
            this.sbtnShowInstantInfo.TabStop = false;
            this.sbtnShowInstantInfo.Text = "Show Instant Info";
            this.sbtnShowInstantInfo.Click += new System.EventHandler(this.sbtnShowInstantInfo_Click);
            // 
            // sbtnRangeSet
            // 
            this.sbtnRangeSet.Image = ((System.Drawing.Image)(resources.GetObject("sbtnRangeSet.Image")));
            this.sbtnRangeSet.ImageIndex = 5;
            this.sbtnRangeSet.ImageList = this.imageList1;
            this.sbtnRangeSet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnRangeSet.Location = new System.Drawing.Point(263, 8);
            this.sbtnRangeSet.Name = "sbtnRangeSet";
            this.sbtnRangeSet.Size = new System.Drawing.Size(30, 23);
            this.sbtnRangeSet.TabIndex = 5;
            this.sbtnRangeSet.TabStop = false;
            this.sbtnRangeSet.Text = "Range Set";
            this.sbtnRangeSet.Click += new System.EventHandler(this.sbtnBulkSet_Click);
            // 
            // sbtnItemSearch
            // 
            this.sbtnItemSearch.Image = ((System.Drawing.Image)(resources.GetObject("sbtnItemSearch.Image")));
            this.sbtnItemSearch.ImageIndex = 6;
            this.sbtnItemSearch.ImageList = this.imageList1;
            this.sbtnItemSearch.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnItemSearch.Location = new System.Drawing.Point(299, 8);
            this.sbtnItemSearch.Name = "sbtnItemSearch";
            this.sbtnItemSearch.Size = new System.Drawing.Size(27, 23);
            this.sbtnItemSearch.TabIndex = 6;
            this.sbtnItemSearch.TabStop = false;
            this.sbtnItemSearch.Text = "S";
            this.sbtnItemSearch.Click += new System.EventHandler(this.sbtnItemSearch_Click);
            // 
            // sbtnSerialNo
            // 
            this.sbtnSerialNo.Location = new System.Drawing.Point(332, 8);
            this.sbtnSerialNo.Name = "sbtnSerialNo";
            this.sbtnSerialNo.Size = new System.Drawing.Size(75, 23);
            this.sbtnSerialNo.TabIndex = 12;
            this.sbtnSerialNo.TabStop = false;
            this.sbtnSerialNo.Text = "Serial No";
            this.sbtnSerialNo.Click += new System.EventHandler(this.sbtnSerialNo_Click);
            // 
            // scanBarcodeControl1
            // 
            this.scanBarcodeControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.scanBarcodeControl1.EnableItemPackage = false;
            this.scanBarcodeControl1.Location = new System.Drawing.Point(0, 0);
            this.scanBarcodeControl1.Name = "scanBarcodeControl1";
            this.scanBarcodeControl1.Size = new System.Drawing.Size(1000, 32);
            this.scanBarcodeControl1.TabIndex = 4;
            this.scanBarcodeControl1.OnBarCodeInput += new BCE.AutoCount.Stock.BarCodeInputEventHandler(this.scanBarcodeControl1_OnBarCodeInput);
            // 
            // tabPageMoreHeader
            // 
            this.tabPageMoreHeader.Controls.Add(this.labelRemark4);
            this.tabPageMoreHeader.Controls.Add(this.labelRemark3);
            this.tabPageMoreHeader.Controls.Add(this.labelRemark2);
            this.tabPageMoreHeader.Controls.Add(this.labelRemark1);
            this.tabPageMoreHeader.Controls.Add(this.mruEdtRemark1);
            this.tabPageMoreHeader.Controls.Add(this.mruEdtRemark2);
            this.tabPageMoreHeader.Controls.Add(this.mruEdtRemark3);
            this.tabPageMoreHeader.Controls.Add(this.mruEdtRemark4);
            this.tabPageMoreHeader.Name = "tabPageMoreHeader";
            this.tabPageMoreHeader.Size = new System.Drawing.Size(1000, 410);
            this.tabPageMoreHeader.Text = "More Header";
            // 
            // labelRemark4
            // 
            this.labelRemark4.BackColor = System.Drawing.Color.Transparent;
            this.labelRemark4.Location = new System.Drawing.Point(14, 116);
            this.labelRemark4.Name = "labelRemark4";
            this.labelRemark4.Size = new System.Drawing.Size(77, 17);
            this.labelRemark4.TabIndex = 0;
            this.labelRemark4.Text = "Remark 4";
            // 
            // labelRemark3
            // 
            this.labelRemark3.BackColor = System.Drawing.Color.Transparent;
            this.labelRemark3.Location = new System.Drawing.Point(14, 86);
            this.labelRemark3.Name = "labelRemark3";
            this.labelRemark3.Size = new System.Drawing.Size(77, 17);
            this.labelRemark3.TabIndex = 1;
            this.labelRemark3.Text = "Remark 3";
            // 
            // labelRemark2
            // 
            this.labelRemark2.BackColor = System.Drawing.Color.Transparent;
            this.labelRemark2.Location = new System.Drawing.Point(14, 56);
            this.labelRemark2.Name = "labelRemark2";
            this.labelRemark2.Size = new System.Drawing.Size(77, 17);
            this.labelRemark2.TabIndex = 2;
            this.labelRemark2.Text = "Remark 2";
            // 
            // labelRemark1
            // 
            this.labelRemark1.BackColor = System.Drawing.Color.Transparent;
            this.labelRemark1.Location = new System.Drawing.Point(14, 26);
            this.labelRemark1.Name = "labelRemark1";
            this.labelRemark1.Size = new System.Drawing.Size(77, 17);
            this.labelRemark1.TabIndex = 3;
            this.labelRemark1.Text = "Remark 1";
            // 
            // mruEdtRemark1
            // 
            this.mruEdtRemark1.Location = new System.Drawing.Point(97, 23);
            this.mruEdtRemark1.Name = "mruEdtRemark1";
            this.mruEdtRemark1.Size = new System.Drawing.Size(336, 20);
            this.mruEdtRemark1.TabIndex = 4;
            // 
            // mruEdtRemark2
            // 
            this.mruEdtRemark2.Location = new System.Drawing.Point(97, 53);
            this.mruEdtRemark2.Name = "mruEdtRemark2";
            this.mruEdtRemark2.Size = new System.Drawing.Size(336, 20);
            this.mruEdtRemark2.TabIndex = 5;
            // 
            // mruEdtRemark3
            // 
            this.mruEdtRemark3.Location = new System.Drawing.Point(97, 83);
            this.mruEdtRemark3.Name = "mruEdtRemark3";
            this.mruEdtRemark3.Size = new System.Drawing.Size(336, 20);
            this.mruEdtRemark3.TabIndex = 6;
            // 
            // mruEdtRemark4
            // 
            this.mruEdtRemark4.Location = new System.Drawing.Point(97, 113);
            this.mruEdtRemark4.Name = "mruEdtRemark4";
            this.mruEdtRemark4.Size = new System.Drawing.Size(336, 20);
            this.mruEdtRemark4.TabIndex = 7;
            // 
            // tabPageExternalLink
            // 
            this.tabPageExternalLink.Controls.Add(this.externalLinkBox1);
            this.tabPageExternalLink.Name = "tabPageExternalLink";
            this.tabPageExternalLink.Size = new System.Drawing.Size(1000, 410);
            this.tabPageExternalLink.Text = "External Link";
            // 
            // externalLinkBox1
            // 
            this.externalLinkBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.externalLinkBox1.Location = new System.Drawing.Point(0, 0);
            this.externalLinkBox1.Name = "externalLinkBox1";
            this.externalLinkBox1.Size = new System.Drawing.Size(1000, 410);
            this.externalLinkBox1.TabIndex = 0;
            // 
            // tabPageNote
            // 
            this.tabPageNote.Controls.Add(this.memoEdtNote);
            this.tabPageNote.Name = "tabPageNote";
            this.tabPageNote.Size = new System.Drawing.Size(1000, 410);
            this.tabPageNote.Text = "Note";
            // 
            // memoEdtNote
            // 
            this.memoEdtNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdtNote.Location = new System.Drawing.Point(0, 0);
            this.memoEdtNote.Name = "memoEdtNote";
            this.memoEdtNote.Size = new System.Drawing.Size(1000, 410);
            this.memoEdtNote.TabIndex = 0;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(859, 260);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.tabControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 102);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1006, 438);
            this.panelControl1.TabIndex = 0;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.Popup += new System.EventHandler(this.barSubItem1_Popup);
            // 
            // barItemCopyWholeDocument
            // 
            this.barItemCopyWholeDocument.Id = 1;
            this.barItemCopyWholeDocument.Name = "barItemCopyWholeDocument";
            this.barItemCopyWholeDocument.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopyWholeDocument_ItemClick);
            // 
            // barItemCopySelectedDetails
            // 
            this.barItemCopySelectedDetails.Id = 7;
            this.barItemCopySelectedDetails.Name = "barItemCopySelectedDetails";
            this.barItemCopySelectedDetails.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopySelectedDetails_ItemClick);
            // 
            // barItemCopyAsTabDelimitedText
            // 
            this.barItemCopyAsTabDelimitedText.Id = 22;
            this.barItemCopyAsTabDelimitedText.Name = "barItemCopyAsTabDelimitedText";
            this.barItemCopyAsTabDelimitedText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopyAsTabDelimitedText_ItemClick);
            // 
            // barItemPasteWholeDocument
            // 
            this.barItemPasteWholeDocument.Id = 2;
            this.barItemPasteWholeDocument.Name = "barItemPasteWholeDocument";
            this.barItemPasteWholeDocument.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemPasteWholeDocument_ItemClick);
            // 
            // barItemPasteItemDetailOnly
            // 
            this.barItemPasteItemDetailOnly.Id = 3;
            this.barItemPasteItemDetailOnly.Name = "barItemPasteItemDetailOnly";
            this.barItemPasteItemDetailOnly.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemPasteItemDetailOnly_ItemClick);
            // 
            // iUndoMaster
            // 
            this.iUndoMaster.Id = 8;
            this.iUndoMaster.Name = "iUndoMaster";
            this.iUndoMaster.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iUndoMaster_ItemClick);
            // 
            // barCheckReallocatePurchaseByProject
            // 
            this.barCheckReallocatePurchaseByProject.CloseSubMenuOnClick = false;
            this.barCheckReallocatePurchaseByProject.Id = 16;
            this.barCheckReallocatePurchaseByProject.Name = "barCheckReallocatePurchaseByProject";
            this.barCheckReallocatePurchaseByProject.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckReallocatePurchaseByProject_CheckedChanged);
            // 
            // barBtnSaveInKIV
            // 
            this.barBtnSaveInKIV.Id = 14;
            this.barBtnSaveInKIV.Name = "barBtnSaveInKIV";
            this.barBtnSaveInKIV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnSaveInKIV_ItemClick);
            // 
            // barSubItem3
            // 
            this.barSubItem3.Id = 17;
            this.barSubItem3.Name = "barSubItem3";
            // 
            // iEditMRU
            // 
            this.iEditMRU.Id = 10;
            this.iEditMRU.Name = "iEditMRU";
            this.iEditMRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditMRU_ItemClick);
            // 
            // iEditRemark1MRU
            // 
            this.iEditRemark1MRU.Id = 18;
            this.iEditRemark1MRU.Name = "iEditRemark1MRU";
            this.iEditRemark1MRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditRemark1MRU_ItemClick);
            // 
            // iEditRemark2MRU
            // 
            this.iEditRemark2MRU.Id = 19;
            this.iEditRemark2MRU.Name = "iEditRemark2MRU";
            this.iEditRemark2MRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditRemark2MRU_ItemClick);
            // 
            // iEditRemark3MRU
            // 
            this.iEditRemark3MRU.Id = 20;
            this.iEditRemark3MRU.Name = "iEditRemark3MRU";
            this.iEditRemark3MRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditRemark3MRU_ItemClick);
            // 
            // iEditRemark4MRU
            // 
            this.iEditRemark4MRU.Id = 21;
            this.iEditRemark4MRU.Name = "iEditRemark4MRU";
            this.iEditRemark4MRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iEditRemark4MRU_ItemClick);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Id = 4;
            this.barSubItem2.Name = "barSubItem2";
            this.barSubItem2.Popup += new System.EventHandler(this.barSubItem2_Popup);
            // 
            // barItemCopyFrom
            // 
            this.barItemCopyFrom.Id = 5;
            this.barItemCopyFrom.Name = "barItemCopyFrom";
            this.barItemCopyFrom.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopyFrom_ItemClick);
            // 
            // barItemCopyTo
            // 
            this.barItemCopyTo.Id = 6;
            this.barItemCopyTo.Name = "barItemCopyTo";
            this.barItemCopyTo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barItemCopyTo_ItemClick);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 30000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 300000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(859, 251);
            this.xtraTabPage2.Text = "xtraTabPage2";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // FormStockReceiveEntry
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnCancel;
            this.ClientSize = new System.Drawing.Size(1006, 581);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormStockReceiveEntry";
            this.ShowInTaskbar = false;
            this.Activated += new System.EventHandler(this.FormStockReceiveEntry_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormStockReceiveEntry_Closing);
            this.Deactivate += new System.EventHandler(this.FormStockReceiveEntry_Deactivate);
            this.Load += new System.EventHandler(this.FormStockReceiveEntry_Load);
            this.VisibleChanged += new System.EventHandler(this.FormStockReceiveEntry_VisibleChanged);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FormStockReceiveEntry_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.FormStockReceiveEntry_DragOver);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormStockReceiveEntry_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelHeader)).EndInit();
            this.panelHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.luWONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luMachineCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEdtStockReceiveNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdtDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdtDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luEdtDocNoFormat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkedtNextRecord.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.myGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStockDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_ItemBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemBtnEdt_FurtherDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_ItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_Location)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemCkEdt_General)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_ProjNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_ItemUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLkEdt_AccNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleBatchNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelBottom)).EndInit();
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel5)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.hylinkEditUpdateRealCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hylinkEditUpdateReferenceCost.Properties)).EndInit();
            this.tabPageMoreHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEdtRemark4.Properties)).EndInit();
            this.tabPageExternalLink.ResumeLayout(false);
            this.tabPageNote.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        private class MyFilterOption : EnterKeyMessageFilter.FilterOption
        {
            private StockReceive myDocument;

            public StockReceive Document
            {
                get
                {
                    return this.myDocument;
                }
                set
                {
                    this.myDocument = value;
                }
            }

            public MyFilterOption()
            {
                this.AddGridControl("myGridControl");
                this.SetNoFilterTabPage("tabPageMoreHeader");
                this.SetNoFilterTabPage("tabPageExternalLink");
                this.SetNoFilterTabPage("tabPageNote");
            }

            protected override bool CanDelete(DataRow row)
            {
                if (this.myDocument != null && this.myDocument.Action == StockReceiveAction.View || (row["ItemCode"] != DBNull.Value || row["Description"].ToString().Length != 0))
                    return false;
                else
                    return BCE.Data.Convert.ToDecimal(row["SubTotal"]) == Decimal.Zero;
            }
        }

        public class FormEventArgs
        {
            private FormStockReceiveEntry myForm;
            private StockReceive myStockReceive;

            public FormStockReceiveEntry Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public PanelControl HeaderPanel
            {
                get
                {
                    return this.myForm.panelHeader;
                }
            }

            public GridControl GridControl
            {
                get
                {
                    return this.myForm.myGridControl;
                }
            }

            public XtraTabControl TabControl
            {
                get
                {
                    return this.myForm.tabControl1;
                }
            }

            public StockReceive StockReceive
            {
                get
                {
                    return this.myStockReceive;
                }
            }

            public DataTable MasterTable
            {
                get
                {
                    return this.myStockReceive.DataTableMaster;
                }
            }

            public DataTable DetailTable
            {
                get
                {
                    return this.myStockReceive.DataTableDetail;
                }
            }

            public EditWindowMode EditWindowMode
            {
                get
                {
                    if (this.myStockReceive.Action == StockReceiveAction.New)
                        return EditWindowMode.New;
                    else if (this.myStockReceive.Action == StockReceiveAction.Edit)
                        return EditWindowMode.Edit;
                    else
                        return EditWindowMode.View;
                }
            }

            public DBSetting DBSetting
            {
                get
                {
                    return this.myForm.myDBSetting;
                }
            }

            public FormEventArgs(FormStockReceiveEntry form, StockReceive doc)
            {
                this.myForm = form;
                this.myStockReceive = doc;
            }
        }

        public class FormInitializeEventArgs : FormStockReceiveEntry.FormEventArgs
        {
            public FormInitializeEventArgs(FormStockReceiveEntry form, StockReceive doc)
              : base(form, doc)
            {
            }
        }

        public class FormDataBindingEventArgs : FormStockReceiveEntry.FormEventArgs
        {
            public FormDataBindingEventArgs(FormStockReceiveEntry form, StockReceive doc)
              : base(form, doc)
            {
            }
        }

        public class FormShowEventArgs : FormStockReceiveEntry.FormEventArgs
        {
            public FormShowEventArgs(FormStockReceiveEntry form, StockReceive doc)
              : base(form, doc)
            {
            }
        }

        public class FormClosedEventArgs : FormStockReceiveEntry.FormEventArgs
        {
            public FormClosedEventArgs(FormStockReceiveEntry form, StockReceive doc)
              : base(form, doc)
            {
            }
        }

        public class FormBeforeSaveEventArgs : FormStockReceiveEntry.FormEventArgs
        {
            public FormBeforeSaveEventArgs(FormStockReceiveEntry form, StockReceive doc)
              : base(form, doc)
            {
            }
        }

        public class AfterCopyToNewDocumentEventArgs : StockReceiveEventArgs
        {
            private StockReceive myFromStock;

            public StockReceiveRecord FromMasterRecord
            {
                get
                {
                    return new StockReceiveRecord(this.myFromStock);
                }
            }

            internal AfterCopyToNewDocumentEventArgs(StockReceive doc, StockReceive fromDoc)
              : base(doc)
            {
                this.myFromStock = fromDoc;
            }
        }

        public class AfterCopyFromOtherDocumentEventArgs : StockReceiveEventArgs
        {
            private StockReceive myFromStock;

            public StockReceiveRecord FromMasterRecord
            {
                get
                {
                    return new StockReceiveRecord(this.myFromStock);
                }
            }

            internal AfterCopyFromOtherDocumentEventArgs(StockReceive doc, StockReceive fromDoc)
              : base(doc)
            {
                this.myFromStock = fromDoc;
            }
        }

        public class BeforePromptSerialNumberEntryEventArgs : FormStockReceiveEntry.FormEventArgs
        {
            private long myDtlkey = -1L;
            private string myDocType = "RV";
            private bool myHandled;

            public bool Handled
            {
                get
                {
                    return this.myHandled;
                }
                set
                {
                    this.myHandled = value;
                }
            }

            public long SelectedDetailRowDtlKey
            {
                get
                {
                    return this.myDtlkey;
                }
            }

            public BeforePromptSerialNumberEntryEventArgs(FormStockReceiveEntry form, StockReceive doc, long dtlKey)
              : base(form, doc)
            {
                this.myDtlkey = dtlKey;
            }
        }
        private string GetWOWhereSql(SqlCommand cmd)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.AddFilter(this.myWOFilter);
            return searchCriteria.BuildSQL((IDbCommand)cmd);
        }
        private void btnApplyFilter_Click(object sender, EventArgs e)
        {
            DataTable mytablefilter = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            //string str123 = myDebtorFilter.ToString();
            string whereSql = this.GetWOWhereSql(sqlCommand);
            if (whereSql.Length != 0)
                whereSql = "AND " + whereSql;
            string strQuery = "select * from vRPA_TransWOtoRCV a where a.status!='Closed' " + whereSql + " Order By DocNo";

            string strResult = !(whereSql == string.Empty) ? string.Format(strQuery, (object)whereSql) : string.Format(strQuery, (object)"(1=1)");
            sqlCommand.CommandText = strResult;
            sqlCommand.CommandTimeout =30;
            try
            {
                //if (this.myFromDate != null)
                //    sqlCommand.Parameters.Add("@FromDate", (object)this.myFromDate.Date);
                //else
                //    sqlCommand.Parameters.Add("@FromDate", (object)DXMOSWebApp.Application.Application.ActualDataStartPeriod.Date);
                //if (this.myToDate != null)
                //    sqlCommand.Parameters.Add("@ToDate", (object)this.myToDate.Date);
                sqlConnection.Open();
                new SqlDataAdapter(sqlCommand).Fill(mytablefilter);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            if (mytablefilter.Rows.Count > 0)
                this.myStockReceive.ClearDetails();
            string strMachineCode = "";
            foreach (DataRow drdtl in mytablefilter.Rows)
            {
                if (drdtl["MachineCode"] != null && drdtl["MachineCode"] != DBNull.Value)
                {
                    strMachineCode = drdtl["MachineCode"].ToString();
                }
                if (this.tabControl1.SelectedTabPage != this.tabPageMain)
                    this.tabControl1.SelectedTabPage = this.tabPageMain;
                BCE.XtraUtils.GridViewUtils.UpdateData(this.gridViewStockDetail);
                BCE.AutoCount.XtraUtils.GridViewUtils.SelectRowBySeq(this.gridViewStockDetail, this.myStockReceive.AddDetail(drdtl).Seq);              

            }
           // this.InitializeBatchNoMaster();
            luMachineCode.EditValue = strMachineCode;
            luMachineCode.Text = strMachineCode;
        }

      
        private void luWONo_EditValueChanged(object sender, EventArgs e)
        {
            itembuilder.FilterWONo(luWONo.Text);
            //InitializeBatchNoMaster();


        }

       

        private void gridViewStockDetail_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
          
        }

        private void RepItemLkEdt_ItemCode_ListChanged(object sender, ListChangedEventArgs e)
        {
           // string str = "";
        }

        private void RepItemLkEdt_ItemCode_EditValueChanged(object sender, EventArgs e)
        {
           
        }

        private void gridViewStockDetail_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            
            //if (e.Column.FieldName == "ItemCode")
            //{
            //    RepositoryItemLookUpEdit luitem = (RepositoryItemLookUpEdit)e.RepositoryItem;
            //    luitem.ListChanged += RepItemLkEdt_ItemCode_ListChanged;
            //}
        }

        private void repleBatchNo_EditValueChanged(object sender, EventArgs e)
        {
           // DataRow drDetail = gridViewStockDetail.GetDataRow(gridViewStockDetail.FocusedRowHandle);
           // drDetail[""]
        }

        private void mruEdtDescription_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void myGridControl_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
