﻿
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;

namespace Production.StockReceive
{
    [Obsolete("Use FormStockReceiveEntry.FormInitializeEventArgs instead")]
    public class FormInitializeEventArgs : FormStockReceiveEventArgs
    {
        public FormInitializeEventArgs(StockReceive doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
          : base(doc, headerPanel, gridControl, tabControl)
        {
        }
    }
}
