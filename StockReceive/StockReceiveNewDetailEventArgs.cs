﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveNewDetailEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;
using System.Data;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveNewDetailEventArgs
  {
    private DataRow myDetailRow;
    private StockReceive myStock;

    public StockReceiveRecord MasterRecord
    {
      get
      {
        return new StockReceiveRecord(this.myStock);
      }
    }

    public StockReceiveDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockReceiveDetailRecord(this.myStock.Command.DBSetting, this.myDetailRow);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.myDBSetting;
      }
    }

    internal StockReceiveNewDetailEventArgs(StockReceive doc, DataRow detailRow)
    {
      this.myStock = doc;
      this.myDetailRow = detailRow;
    }
  }
}
