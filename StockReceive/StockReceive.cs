﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceive
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.ContextException;
using BCE.AutoCount.Data;
using BCE.AutoCount.Document;
using BCE.AutoCount.Invoicing;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SerialNumber2;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
//using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;

namespace Production.StockReceive
{
    public class StockReceive : IImportExport
    {
        private string myDocNoFormatName = "";
        internal DataSet myDataSet;
        private StockReceiveAction myAction;
        private DataRow myRow;
        private StockReceiveCommand myCommand;
        private DataTable myMasterTable;
        private DataTable myDetailTable;
        private DataTable mySerialNoTable;
        private bool myEnableAutoLoadItemDetail;
        private int myDisableColumnChangedCounter;
        internal ScriptObject myScriptObject;
        private object myUserData;
        private bool myKeepLastModifiedInfo;
        protected DecimalSetting myDecimalSetting;
        protected UserAuthentication myUserAuthentication;
        private ConfirmChangingItemCodeEventHandler ConfirmChangingItemCodeEvent;
        private ConfirmSerialNumberQuantityNotMatchEventHandler ConfirmSerialNumberQuantityNotMatchEvent;
        private ShowSerialNumberEntryFormEventHandler ShowSerialNumberEntryFormEvent;
        private SetDocNoFormatEventHandler SetDocNoFormatEvent;

        public object UserData
        {
            get
            {
                return this.myUserData;
            }
            set
            {
                this.myUserData = value;
            }
        }

        public ScriptObject ScriptObject
        {
            get
            {
                return this.myScriptObject;
            }
        }

        public string DocNoFormatName
        {
            get
            {
                return this.myDocNoFormatName;
            }
            set
            {
                if (this.SetDocNoFormatEvent != null)
                    this.SetDocNoFormatEvent(value);
                this.myDocNoFormatName = value;
            }
        }

        public bool EnableAutoLoadItemDetail
        {
            get
            {
                return this.myEnableAutoLoadItemDetail;
            }
            set
            {
                this.myEnableAutoLoadItemDetail = value;
            }
        }

        public StockReceiveCommand Command
        {
            get
            {
                return this.myCommand;
            }
        }

        public DataTable DataTableSerialNo
        {
            get
            {
                return this.mySerialNoTable;
            }
        }

        public DataTable DataTableMaster
        {
            get
            {
                return this.myMasterTable;
            }
        }

        public DataTable DataTableDetail
        {
            get
            {
                return this.myDetailTable;
            }
        }

        public DataSet StockReceiveDataSet
        {
            get
            {
                return this.myDataSet;
            }
        }

        public int DetailCount
        {
            get
            {
                return this.GetValidDetailRows().Length;
            }
        }

        public StockReceiveAction Action
        {
            get
            {
                return this.myAction;
            }
        }

        public bool KeepLastModifiedInfo
        {
            get
            {
                return this.myKeepLastModifiedInfo;
            }
            set
            {
                this.myKeepLastModifiedInfo = value;
            }
        }

        public long DocKey
        {
            get
            {
                return BCE.Data.Convert.ToInt64(this.myRow["DocKey"]);
            }
        }

        public DBString DocNo
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["DocNo"]);
            }
            set
            {
                this.myRow["DocNo"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBDateTime DocDate
        {

            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myRow["DocDate"]);
            }
            set
            {
                if (value.HasValue)
                    this.myRow["DocDate"] = BCE.Data.Convert.ToDBObject((DBDateTime)(DateTime)value);
                else
                    this.myRow["DocDate"] = (object)DBNull.Value;
            }


        }

        public DBString Description
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Description"]);
            }
            set
            {
                this.myRow["Description"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBDecimal Total
        {
            get
            {
                return BCE.Data.Convert.ToDBDecimal(this.myRow["Total"]);
            }
        }

        public DBString Note
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Note"]);
            }
            set
            {
                if (value.HasValue)
                    this.myRow["Note"] = (object)Rtf.ToArialRichText((string)value);
                else
                    this.myRow["Note"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Remark1
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Remark1"]);
            }
            set
            {
                this.myRow["Remark1"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Remark2
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Remark2"]);
            }
            set
            {
                this.myRow["Remark2"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Remark3
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Remark3"]);
            }
            set
            {
                this.myRow["Remark3"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public DBString Remark4
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["Remark4"]);
            }
            set
            {
                this.myRow["Remark4"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public short PrintCount
        {
            get
            {
                return BCE.Data.Convert.ToInt16(this.myRow["PrintCount"]);
            }
        }

        public bool Cancelled
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myRow["Cancelled"]);
            }
        }

        public DBDateTime LastModified
        {
            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myRow["LastModified"]);
            }
        }

        public DBString LastModifiedUserID
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["LastModifiedUserID"]);
            }
        }

        public DBDateTime CreatedTimeStamp
        {
            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myRow["CreatedTimeStamp"]);
            }
        }

        public DBString CreatedUserID
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["CreatedUserID"]);
            }
        }

        public DBString RefDocNo
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["RefDocNo"]);
            }
            set
            {
                this.myRow["RefDocNo"] = BCE.Data.Convert.ToDBObject(value);
            }
        }
      
        public bool CanSync
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myRow["CanSync"]);
            }
            set
            {
                this.myRow["CanSync"] = (object)BCE.Data.Convert.BooleanToText(value);
            }
        }

        public DBString ExternalLinkText
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["ExternalLink"]);
            }
            set
            {
                this.myRow["ExternalLink"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public ExternalLink ExternalLink
        {
            get
            {
                return new ExternalLink(this.myRow, "ExternalLink");
            }
        }

        //public bool ReallocatePurchaseByProject
        //{
        //  get
        //  {
        //    return BCE.Data.Convert.TextToBoolean(this.myRow["ReallocatePurchaseByProject"]);
        //  }
        //  set
        //  {
        //    this.myRow["ReallocatePurchaseByProject"] = (object) BCE.Data.Convert.BooleanToText(value);
        //  }
        //}

        public long ReallocatePurchaseByProjectJEDocKey
        {
            get
            {
                return BCE.Data.Convert.ToInt64(this.myRow["ReallocatePurchaseByProjectJEDocKey"]);
            }
        }

        public DBString ReallocatePurchaseByProjectNo
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["ReallocatePurchaseByProjectNo"]);
            }
            set
            {
                this.myRow["ReallocatePurchaseByProjectNo"] = BCE.Data.Convert.ToDBObject(value);
            }
        }

        public UDFRecord UDF
        {
            get
            {
                return new UDFRecord(this.myRow);
            }
        }

        public event ConfirmChangingItemCodeEventHandler ConfirmChangingItemCodeEvent2
        {
            add
            {
                ConfirmChangingItemCodeEventHandler codeEventHandler = this.ConfirmChangingItemCodeEvent;
                ConfirmChangingItemCodeEventHandler comparand;
                do
                {
                    comparand = codeEventHandler;
                    codeEventHandler = Interlocked.CompareExchange<ConfirmChangingItemCodeEventHandler>(ref this.ConfirmChangingItemCodeEvent, comparand + value, comparand);
                }
                while (codeEventHandler != comparand);
            }
            remove
            {
                ConfirmChangingItemCodeEventHandler codeEventHandler = this.ConfirmChangingItemCodeEvent;
                ConfirmChangingItemCodeEventHandler comparand;
                do
                {
                    comparand = codeEventHandler;
                    codeEventHandler = Interlocked.CompareExchange<ConfirmChangingItemCodeEventHandler>(ref this.ConfirmChangingItemCodeEvent, comparand - value, comparand);
                }
                while (codeEventHandler != comparand);
            }
        }

        public event ConfirmSerialNumberQuantityNotMatchEventHandler ConfirmSerialNumberQuantityNotMatchEvent2
        {
            add
            {
                ConfirmSerialNumberQuantityNotMatchEventHandler matchEventHandler = this.ConfirmSerialNumberQuantityNotMatchEvent;
                ConfirmSerialNumberQuantityNotMatchEventHandler comparand;
                do
                {
                    comparand = matchEventHandler;
                    matchEventHandler = Interlocked.CompareExchange<ConfirmSerialNumberQuantityNotMatchEventHandler>(ref this.ConfirmSerialNumberQuantityNotMatchEvent, comparand + value, comparand);
                }
                while (matchEventHandler != comparand);
            }
            remove
            {
                ConfirmSerialNumberQuantityNotMatchEventHandler matchEventHandler = this.ConfirmSerialNumberQuantityNotMatchEvent;
                ConfirmSerialNumberQuantityNotMatchEventHandler comparand;
                do
                {
                    comparand = matchEventHandler;
                    matchEventHandler = Interlocked.CompareExchange<ConfirmSerialNumberQuantityNotMatchEventHandler>(ref this.ConfirmSerialNumberQuantityNotMatchEvent, comparand - value, comparand);
                }
                while (matchEventHandler != comparand);
            }
        }

        public event ShowSerialNumberEntryFormEventHandler ShowSerialNumberEntryFormEvent2
        {
            add
            {
                ShowSerialNumberEntryFormEventHandler formEventHandler = this.ShowSerialNumberEntryFormEvent;
                ShowSerialNumberEntryFormEventHandler comparand;
                do
                {
                    comparand = formEventHandler;
                    formEventHandler = Interlocked.CompareExchange<ShowSerialNumberEntryFormEventHandler>(ref this.ShowSerialNumberEntryFormEvent, comparand + value, comparand);
                }
                while (formEventHandler != comparand);
            }
            remove
            {
                ShowSerialNumberEntryFormEventHandler formEventHandler = this.ShowSerialNumberEntryFormEvent;
                ShowSerialNumberEntryFormEventHandler comparand;
                do
                {
                    comparand = formEventHandler;
                    formEventHandler = Interlocked.CompareExchange<ShowSerialNumberEntryFormEventHandler>(ref this.ShowSerialNumberEntryFormEvent, comparand - value, comparand);
                }
                while (formEventHandler != comparand);
            }
        }

        public event SetDocNoFormatEventHandler SetDocNoFormatEvent2
        {
            add
            {
                SetDocNoFormatEventHandler formatEventHandler = this.SetDocNoFormatEvent;
                SetDocNoFormatEventHandler comparand;
                do
                {
                    comparand = formatEventHandler;
                    formatEventHandler = Interlocked.CompareExchange<SetDocNoFormatEventHandler>(ref this.SetDocNoFormatEvent, comparand + value, comparand);
                }
                while (formatEventHandler != comparand);
            }
            remove
            {
                SetDocNoFormatEventHandler formatEventHandler = this.SetDocNoFormatEvent;
                SetDocNoFormatEventHandler comparand;
                do
                {
                    comparand = formatEventHandler;
                    formatEventHandler = Interlocked.CompareExchange<SetDocNoFormatEventHandler>(ref this.SetDocNoFormatEvent, comparand - value, comparand);
                }
                while (formatEventHandler != comparand);
            }
        }

        internal StockReceive(StockReceiveCommand command, DataSet aDataSet, StockReceiveAction action)
        {
            this.myEnableAutoLoadItemDetail = false;
            this.myCommand = command;
            this.myDataSet = aDataSet;
            this.myAction = action;
            this.myScriptObject = ScriptManager.CreateObject(command.DBSetting, "RPA_RCV");
            this.myMasterTable = this.myDataSet.Tables["Master"];
            this.myDetailTable = this.myDataSet.Tables["Detail"];
            this.mySerialNoTable = this.myDataSet.Tables["SerialNoTrans"];
            this.myRow = this.myMasterTable.Rows[0];
            this.myMasterTable.ColumnChanged += new DataColumnChangeEventHandler(this.myMasterTable_ColumnChanged);
            this.myDetailTable.RowChanged += new DataRowChangeEventHandler(this.myDetailTable_RowChanged);
            this.myDetailTable.ColumnChanged += new DataColumnChangeEventHandler(this.myDetailTable_ColumnChanged);
            this.myDetailTable.RowDeleting += new DataRowChangeEventHandler(this.myDetailTable_RowDeleting);
            this.myDetailTable.RowDeleted += new DataRowChangeEventHandler(this.myDetailTable_RowDeleted);
            this.myDetailTable.ColumnChanging += new DataColumnChangeEventHandler(this.myDetailTable_ColumnChanging);
            this.myDecimalSetting = DecimalSetting.GetOrCreate(command.DBSetting);
            this.myUserAuthentication = UserAuthentication.GetOrCreate(command.DBSetting);
        }

        public static bool CanCopyMasterField(string columnName)
        {
            string[] strArray = new string[13];
            int index1 = 0;
            string str1 = "DocKey";
            strArray[index1] = str1;
            int index2 = 1;
            string str2 = "DocNo";
            strArray[index2] = str2;
            int index3 = 2;
            string str3 = "DocDate";
            strArray[index3] = str3;
            int index4 = 3;
            string str4 = "Total";
            strArray[index4] = str4;
            int index5 = 4;
            string str5 = "PrintCount";
            strArray[index5] = str5;
            int index6 = 5;
            string str6 = "Cancelled";
            strArray[index6] = str6;
            int index7 = 6;
            string str7 = "LastModified";
            strArray[index7] = str7;
            int index8 = 7;
            string str8 = "LastModifiedUserID";
            strArray[index8] = str8;
            int index9 = 8;
            string str9 = "CreatedTimeStamp";
            strArray[index9] = str9;
            int index10 = 9;
            string str10 = "CreatedUserID";
            strArray[index10] = str10;
            int index11 = 10;
            string str11 = "LastUpdate";
            strArray[index11] = str11;
            int index12 = 11;
            string str12 = "CanSync";
            strArray[index12] = str12;
            int index13 = 12;
            string str13 = "Guid";
            strArray[index13] = str13;
            foreach (string strA in strArray)
            {
                if (string.Compare(strA, columnName, true) == 0)
                    return false;
            }
            return true;
        }

        public static bool CanCopyDetailField(string columnName)
        {
            string[] strArray = new string[4];
            int index1 = 0;
            string str1 = "DtlKey";
            strArray[index1] = str1;
            int index2 = 1;
            string str2 = "DocKey";
            strArray[index2] = str2;
            int index3 = 2;
            string str3 = "Seq";
            strArray[index3] = str3;
            int index4 = 3;
            string str4 = "Guid";
            strArray[index4] = str4;
            foreach (string strA in strArray)
            {
                if (string.Compare(strA, columnName, true) == 0)
                    return false;
            }
            return true;
        }

        public long GetSerialNumberCount(long dtlKey)
        {
            long num = 0L;
            foreach (DataRow dataRow in this.mySerialNoTable.Select("DtlKey = " + dtlKey.ToString()))
                num += BCE.Data.Convert.ToInt64(dataRow["Count"]);
            return num;
        }

        public void SaveToTempDocument(string saveReason)
        {
            this.myCommand.SaveToTempDocument(this, saveReason);
        }

        [Obsolete("The userID parameter will be ignored.")]
        public void Save(string userID, bool cancelDoc)
        {
            this.Save(cancelDoc,false);
        }
        public void Save(string userID, bool cancelDoc,bool postTrans)
        {
            this.Save(cancelDoc, postTrans);
        }

        public void Save(bool cancelDoc,bool PostTrans)
        {
            if (this.myAction == StockReceiveAction.View)
            {
                throw new Exception("Cannot save read-only Stock Receive.");
            }
            else
            {
                this.myUserAuthentication.CheckHasLogined();
                TransactionControl.CheckTransactionCount(this.myCommand.DBSetting);
                this.myCommand.myFiscalYear.CheckTransactionDate((DateTime)this.DocDate, "StockReceive", this.myCommand.DBSetting);
                this.CheckSerialNumberQuantityNotMatch();
                bool flag = this.myRow.RowState != DataRowState.Unchanged;
                foreach (DataRow dataRow in this.GetValidDetailRows())
                {
                    if (!flag && dataRow.RowState != DataRowState.Unchanged)
                        flag = true;
                    if (dataRow["ItemCode"].ToString().Length > 0)
                    {
                        if (dataRow["Location"] == DBNull.Value)
                            throw new NullLocationException();
                        else if (BCE.Data.Convert.ToDecimal(dataRow["Qty"]) <= Decimal.Zero)
                            throw new NotAllowNegativeQuantityException();
                    }
                }
                if (!flag && this.myDetailTable.Select("", "Seq", DataViewRowState.Deleted).Length != 0)
                    flag = true;
                if (flag)
                {
                    if (!this.myKeepLastModifiedInfo || this.myAction == StockReceiveAction.New)
                    {
                        this.myRow["LastModifiedUserID"] = (object)this.myUserAuthentication.LoginUserID;
                        this.myRow["LastModified"] = (object)this.myCommand.DBSetting.GetServerTime();
                    }
                    if (BCE.Data.Convert.ToDateTime(this.myRow["CreatedTimeStamp"]) == DateTime.MinValue)
                        this.myRow["CreatedTimeStamp"] = this.myRow["LastModified"];
                    if (this.myRow["CreatedUserID"].ToString().Length == 0)
                        this.myRow["CreatedUserID"] = this.myRow["LastModifiedUserID"];
                    this.myRow["LastUpdate"] = (object)(BCE.Data.Convert.ToInt32(this.myRow["LastUpdate"]) + 1);
                    this.myRow.EndEdit();
                    this.myCommand.SaveData(this, cancelDoc, PostTrans);
                }
                this.myAction = StockReceiveAction.View;
            }
        }

        private void CheckSerialNumberQuantityNotMatch()
        {
            DataTable numberNotMatchTable = SerialNumberHelper.GetSerialNumberNotMatchTable(this.myCommand.DBSetting, this.myDetailTable, this.mySerialNoTable);
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            if (numberNotMatchTable != null && this.ConfirmSerialNumberQuantityNotMatchEvent != null && !this.ConfirmSerialNumberQuantityNotMatchEvent(this.myCommand.DBSetting, numberNotMatchTable))
                throw new AbortException();
        }

        public void Cancel()
        {
            if (this.myAction == StockReceiveAction.View)
            {
                throw new Exception("Cannot cancel read-only Stock Receive.");
            }
            else
            {
                this.myDataSet.RejectChanges();
                this.myAction = StockReceiveAction.View;
            }
        }

        public void Edit()
        {
            if (this.myAction == StockReceiveAction.View)
            {
                this.myCommand.myFiscalYear.CheckTransactionDate((DateTime)this.DocDate, "StockReceive", this.myCommand.DBSetting);
                this.myAction = StockReceiveAction.Edit;
            }
            else
                throw new InvalidOperationException("Cannot edit the document which is in Edit/New mode.");
        }

        public void Delete()
        {
            if (this.myAction == StockReceiveAction.View)
                this.myCommand.Delete(this.DocKey);
            else
                throw new InvalidOperationException("Cannot delete the document which is in Edit/New mode.");
        }

        public void CancelDocument(string userID)
        {
            if (this.myAction == StockReceiveAction.View)
            {
                this.Edit();
                this.myRow["Cancelled"] = (object)BCE.Data.Convert.BooleanToText(true);
                try
                {
                    this.Save(userID, true);
                }
                catch
                {
                    this.myAction = StockReceiveAction.View;
                    this.myRow.RejectChanges();
                    throw;
                }
            }
            else
                throw new InvalidOperationException("Cannot void the document which is in Edit/New mode.");
        }

        public void UncancelDocument(string userID)
        {
            if (this.myAction == StockReceiveAction.View)
            {
                this.Edit();
                this.myRow["Cancelled"] = (object)BCE.Data.Convert.BooleanToText(false);
                try
                {
                    this.Save(userID, true);
                }
                catch
                {
                    this.myAction = StockReceiveAction.View;
                    this.myRow.RejectChanges();
                    throw;
                }
            }
            else
                throw new InvalidOperationException("Cannot un-void the document which is in Edit/New mode.");
        }

        public string ExportAsXml()
        {
            StringWriter stringWriter = new StringWriter();
            this.myDataSet.WriteXml((TextWriter)stringWriter, XmlWriteMode.WriteSchema);
            return stringWriter.ToString();
        }

        public void ImportFromXml(string xmlString, bool wholeDocument)
        {
            DataSet newDataSet = new DataSet();
            StringReader stringReader = new StringReader(xmlString);
            int num = (int)newDataSet.ReadXml((TextReader)stringReader, XmlReadMode.Auto);
            this.ImportFromDataSet(newDataSet, wholeDocument);
        }

        public void ImportFromDataSet(DataSet newDataSet, bool wholeDocument)
        {
            if (this.myAction == StockReceiveAction.View)
                throw new Exception("Cannot import into read-only Stock Receive.");
            else if (newDataSet.Tables.Count > 0 && newDataSet.Tables[0].TableName == "Master")
            {
                if (wholeDocument)
                {
                    this.myMasterTable.BeginLoadData();
                    try
                    {
                        DataTable dataTable = newDataSet.Tables[0];
                        if (dataTable.Rows.Count > 0)
                        {
                            DataRow dataRow = dataTable.Rows[0];
                            foreach (DataColumn index1 in (InternalDataCollectionBase)dataTable.Columns)
                            {
                                if (StockReceive.CanCopyMasterField(index1.ColumnName))
                                {
                                    int index2 = this.myMasterTable.Columns.IndexOf(index1.ColumnName);
                                    if (index2 >= 0)
                                        this.myRow[index2] = dataRow[index1];
                                }
                            }
                        }
                    }
                    finally
                    {
                        this.myMasterTable.EndLoadData();
                    }
                }
                if (newDataSet.Tables.Count > 1)
                {
                    DataTable table = newDataSet.Tables[1];
                    DataView dataView = new DataView(table);
                    if (table.Columns.IndexOf("Seq") >= 0)
                        dataView.Sort = "Seq";
                    this.myDetailTable.BeginLoadData();
                    try
                    {
                        for (int index1 = 0; index1 < dataView.Count; ++index1)
                        {
                            DataRowView dataRowView = dataView[index1];
                            DataRow dataRow = (DataRow)null;
                            foreach (DataColumn dataColumn in (InternalDataCollectionBase)table.Columns)
                            {
                                if (StockReceive.CanCopyDetailField(dataColumn.ColumnName))
                                {
                                    if (dataColumn.ColumnName == "Location" && dataRow != null)
                                    {
                                        if (dataRowView.DataView.Table.Columns.Contains("UOM"))
                                            dataRow["UOM"] = dataRowView["UOM"];
                                        dataRow["Qty"] = dataRowView["Qty"];
                                    }
                                    int index2 = this.myDetailTable.Columns.IndexOf(dataColumn.ColumnName);
                                    if (index2 >= 0)
                                    {
                                        if (dataRow == null)
                                            dataRow = this.AddDetail(null).Row;
                                        dataRow[index2] = dataRowView[dataColumn.ColumnName];
                                    }
                                }
                            }
                        }
                    }
                    finally
                    {
                        this.myDetailTable.EndLoadData();
                    }
                }
            }
        }

        public bool ImportFromTabDelimitedText(string tabText, bool wholeDocument)
        {
            DataTable tempTable = StringHelper.TabDelimittedTextToDataTable(tabText);
            if (tempTable.Columns.Count < 2)
            {
                return false;
            }
            else
            {
                bool hadApply = false;
                DataTable dataTable1 = this.myDataSet.Tables["Master"];
                DataTable dataTable2 = this.myDataSet.Tables["Detail"];
                DataRow masterRow = dataTable1.Rows[0];
                for (int currentRowIndex1 = 0; currentRowIndex1 < tempTable.Rows.Count; ++currentRowIndex1)
                {
                    DataRow row = tempTable.Rows[currentRowIndex1];
                    if (wholeDocument)
                        hadApply = this.ImportToMasterTable(hadApply, masterRow, row, currentRowIndex1);
                    if (string.Compare(row[0].ToString(), "ItemCode", true, CultureInfo.InvariantCulture) == 0)
                    {
                        DataRow columnRow = row;
                        for (int currentRowIndex2 = currentRowIndex1 + 1; currentRowIndex2 < tempTable.Rows.Count; ++currentRowIndex2)
                            hadApply = this.ImportToDetailTable(tempTable, hadApply, currentRowIndex2, columnRow);
                        break;
                    }
                }
                return hadApply;
            }
        }

        private bool ImportToMasterTable(bool hadApply, DataRow masterRow, DataRow row, int currentRowIndex)
        {
            if (row[1].ToString().Length > 0)
            {
                Hashtable hashtable1 = new Hashtable();
                hashtable1.Add((object)"docno", (object)null);
                hashtable1.Add((object)"description", (object)null);
                hashtable1.Add((object)"refdocno", (object)null);
                hashtable1.Add((object)"remark1", (object)null);
                hashtable1.Add((object)"remark2", (object)null);
                hashtable1.Add((object)"remark3", (object)null);
                hashtable1.Add((object)"remark4", (object)null);
                Hashtable hashtable2 = new Hashtable();
                Hashtable hashtable3 = new Hashtable();
                Hashtable hashtable4 = new Hashtable();
                Hashtable hashtable5 = new Hashtable();
                //foreach (UDFColumn udfColumn in new UDFUtil(this.myCommand.myDBSetting).GetUDF("RPA_RCV"))
                //{
                //  if (udfColumn.Type == UDFType.Boolean)
                //    hashtable4.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.Date)
                //    hashtable5.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.Text || udfColumn.Type == UDFType.System || udfColumn.Type == UDFType.ImageLink)
                //    hashtable1.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.Decimal)
                //    hashtable2.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.Integer)
                //    hashtable3.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //}
                bool flag = false;
                string name = row[0].ToString().ToLowerInvariant();
                if (name == "date")
                    name = "docdate";
                if (masterRow.Table.Columns.Contains(name))
                {
                    if (hashtable1.ContainsKey((object)name))
                    {
                        int maxLength = masterRow.Table.Columns[name].MaxLength;
                        masterRow[name] = maxLength <= 0 || row[1].ToString().Length <= maxLength ? (object)row[1].ToString() : (object)row[1].ToString().Substring(0, maxLength);
                        flag = true;
                    }
                    else if (hashtable5.ContainsKey((object)name))
                    {
                        try
                        {
                            masterRow[name] = this.ConvertToDateTimeFormat(row[1].ToString());
                        }
                        catch
                        {
                            // ISSUE: variable of a boxed type
                            StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidDateFormat;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = name;
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            // ISSUE: variable of a boxed type
                            int local2 = currentRowIndex;
                            objArray[index2] = (object)local2;
                            throw new AppException(Localizer.GetString((Enum)local1, objArray));
                        }
                        flag = true;
                    }
                    else if (hashtable2.ContainsKey((object)name))
                    {
                        try
                        {
                            masterRow[name] = (object)BCE.Data.Convert.ToDecimal(row[1]);
                        }
                        catch
                        {
                            // ISSUE: variable of a boxed type
                            StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidNumberFormat;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = name;
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            // ISSUE: variable of a boxed type
                            int local2 = currentRowIndex;
                            objArray[index2] = (object)local2;
                            throw new AppException(Localizer.GetString((Enum)local1, objArray));
                        }
                        flag = true;
                    }
                    else if (hashtable3.ContainsKey((object)name))
                    {
                        try
                        {
                            masterRow[name] = (object)BCE.Data.Convert.ToInt32(row[1]);
                        }
                        catch
                        {
                            // ISSUE: variable of a boxed type
                            StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidNumberFormat;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = name;
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            // ISSUE: variable of a boxed type
                            int local2 = currentRowIndex;
                            objArray[index2] = (object)local2;
                            throw new AppException(Localizer.GetString((Enum)local1, objArray));
                        }
                        flag = true;
                    }
                    else if (hashtable4.ContainsKey((object)name))
                    {
                        masterRow[name] = (object)BCE.Data.Convert.BooleanToText(BCE.Data.Convert.TextToBoolean(row[1]));
                        flag = true;
                    }
                    else if (name == "docdate")
                    {
                        try
                        {
                            object obj = this.ConvertToDateTimeFormat(row[1].ToString());
                            if (obj != DBNull.Value)
                                this.DocDate = (DBDateTime)((DateTime)obj);
                        }
                        catch
                        {
                            // ISSUE: variable of a boxed type
                            StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidDateFormat;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = "Date";
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            // ISSUE: variable of a boxed type
                            int local2 = currentRowIndex;
                            objArray[index2] = (object)local2;
                            throw new AppException(Localizer.GetString((Enum)local1, objArray));
                        }
                        flag = true;
                    }
                }
                if (flag)
                    hadApply = true;
            }
            return hadApply;
        }

        private bool ImportToDetailTable(DataTable tempTable, bool hadApply, int currentRowIndex, DataRow columnRow)
        {
            StockReceiveDetail stockReceiveDetail = this.AddDetail(null);
            DataRow row = stockReceiveDetail.Row;
            if (stockReceiveDetail != null)
            {
                DataRow dataRow = tempTable.Rows[currentRowIndex];
                Hashtable hashtable1 = new Hashtable();
                hashtable1.Add((object)"itemcode", (object)null);
                hashtable1.Add((object)"description", (object)null);
                hashtable1.Add((object)"uom", (object)null);
                hashtable1.Add((object)"batchno", (object)null);
                hashtable1.Add((object)"location", (object)null);
                hashtable1.Add((object)"projno", (object)null);
                hashtable1.Add((object)"deptno", (object)null);
                Hashtable hashtable2 = new Hashtable();
                hashtable2.Add((object)"furtherdescription", (object)null);
                Hashtable hashtable3 = new Hashtable();
                hashtable3.Add((object)"printout", (object)null);
                Hashtable hashtable4 = new Hashtable();
                Hashtable hashtable5 = new Hashtable();
                Hashtable hashtable6 = new Hashtable();
                Hashtable hashtable7 = new Hashtable();
                //foreach (UDFColumn udfColumn in new UDFUtil(this.myCommand.myDBSetting).GetUDF("RPA_RCVDtl"))
                //{
                //  if (udfColumn.Type == UDFType.Boolean)
                //    hashtable3.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.Date)
                //    hashtable4.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.Memo)
                //    hashtable5.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.RichText)
                //    hashtable2.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.Text || udfColumn.Type == UDFType.System || udfColumn.Type == UDFType.ImageLink)
                //    hashtable1.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.Decimal)
                //    hashtable6.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //  else if (udfColumn.Type == UDFType.Integer)
                //    hashtable7.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
                //}
                foreach (DataColumn index1 in (InternalDataCollectionBase)tempTable.Columns)
                {
                    if (dataRow[index1].ToString().Length > 0)
                    {
                        bool flag = false;
                        string name = columnRow[index1].ToString().ToLowerInvariant();
                        if (row.Table.Columns.Contains(name))
                        {
                            if (hashtable1.ContainsKey((object)name))
                            {
                                int maxLength = row.Table.Columns[name].MaxLength;
                                row[name] = maxLength <= 0 || dataRow[index1].ToString().Length <= maxLength ? (object)dataRow[index1].ToString() : (object)dataRow[index1].ToString().Substring(0, maxLength);
                                flag = true;
                            }
                            else if (hashtable5.ContainsKey((object)name))
                            {
                                row[name] = (object)dataRow[index1].ToString();
                                flag = true;
                            }
                            else if (hashtable2.ContainsKey((object)name))
                            {
                                row[name] = (object)Rtf.ToArialRichText(dataRow[index1].ToString());
                                flag = true;
                            }
                            else if (hashtable4.ContainsKey((object)name))
                            {
                                try
                                {
                                    row[name] = this.ConvertToDateTimeFormat(dataRow[index1].ToString());
                                }
                                catch
                                {
                                    // ISSUE: variable of a boxed type
                                    StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidDateFormat;
                                    object[] objArray = new object[2];
                                    int index2 = 0;
                                    string str = name;
                                    objArray[index2] = (object)str;
                                    int index3 = 1;
                                    // ISSUE: variable of a boxed type
                                    int local2 = currentRowIndex;
                                    objArray[index3] = (object)local2;
                                    throw new AppException(Localizer.GetString((Enum)local1, objArray));
                                }
                                flag = true;
                            }
                            else if (hashtable3.ContainsKey((object)name))
                            {
                                row[name] = (object)BCE.Data.Convert.BooleanToText(BCE.Data.Convert.TextToBoolean(dataRow[index1]));
                                flag = true;
                            }
                            else if (hashtable6.ContainsKey((object)name))
                            {
                                try
                                {
                                    row[name] = (object)BCE.Data.Convert.ToDecimal(dataRow[index1]);
                                    flag = true;
                                }
                                catch
                                {
                                    // ISSUE: variable of a boxed type
                                    StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidNumberFormat;
                                    object[] objArray = new object[2];
                                    int index2 = 0;
                                    string str = name;
                                    objArray[index2] = (object)str;
                                    int index3 = 1;
                                    // ISSUE: variable of a boxed type
                                    int local2 = currentRowIndex;
                                    objArray[index3] = (object)local2;
                                    throw new AppException(Localizer.GetString((Enum)local1, objArray));
                                }
                            }
                            else if (hashtable7.ContainsKey((object)name))
                            {
                                try
                                {
                                    row[name] = (object)BCE.Data.Convert.ToInt32(dataRow[index1]);
                                    flag = true;
                                }
                                catch
                                {
                                    // ISSUE: variable of a boxed type
                                    StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidNumberFormat;
                                    object[] objArray = new object[2];
                                    int index2 = 0;
                                    string str = name;
                                    objArray[index2] = (object)str;
                                    int index3 = 1;
                                    // ISSUE: variable of a boxed type
                                    int local2 = currentRowIndex;
                                    objArray[index3] = (object)local2;
                                    throw new AppException(Localizer.GetString((Enum)local1, objArray));
                                }
                            }
                            else if (name == "qty")
                            {
                                try
                                {
                                    row[name] = (object)this.myDecimalSetting.RoundQuantity(BCE.Data.Convert.ToDecimal(dataRow[index1]));
                                    flag = true;
                                }
                                catch
                                {
                                    // ISSUE: variable of a boxed type
                                    StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidNumberFormat;
                                    object[] objArray = new object[2];
                                    int index2 = 0;
                                    string str = name;
                                    objArray[index2] = (object)str;
                                    int index3 = 1;
                                    // ISSUE: variable of a boxed type
                                    int local2 = currentRowIndex;
                                    objArray[index3] = (object)local2;
                                    throw new AppException(Localizer.GetString((Enum)local1, objArray));
                                }
                            }
                            else if (name == "unitcost")
                            {
                                try
                                {
                                    row[name] = (object)this.myDecimalSetting.RoundCost(BCE.Data.Convert.ToDecimal(dataRow[index1]));
                                    flag = true;
                                }
                                catch
                                {
                                    // ISSUE: variable of a boxed type
                                    StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidNumberFormat;
                                    object[] objArray = new object[2];
                                    int index2 = 0;
                                    string str = name;
                                    objArray[index2] = (object)str;
                                    int index3 = 1;
                                    // ISSUE: variable of a boxed type
                                    int local2 = currentRowIndex;
                                    objArray[index3] = (object)local2;
                                    throw new AppException(Localizer.GetString((Enum)local1, objArray));
                                }
                            }
                            else if (name == "subtotal")
                            {
                                try
                                {
                                    row["Subtotal"] = (object)this.myDecimalSetting.RoundCurrency(BCE.Data.Convert.ToDecimal(dataRow[index1]));
                                    flag = true;
                                }
                                catch
                                {
                                    // ISSUE: variable of a boxed type
                                    StockReceiveStringId local1 = StockReceiveStringId.ErrorMessage_InvalidNumberFormat;
                                    object[] objArray = new object[2];
                                    int index2 = 0;
                                    string str = "SubTotal";
                                    objArray[index2] = (object)str;
                                    int index3 = 1;
                                    // ISSUE: variable of a boxed type
                                    int local2 = currentRowIndex;
                                    objArray[index3] = (object)local2;
                                    throw new AppException(Localizer.GetString((Enum)local1, objArray));
                                }
                            }
                        }
                        if (flag)
                            hadApply = true;
                    }
                }
            }
            return hadApply;
        }

        private object ConvertToDateTimeFormat(string dateTimeString)
        {
            DateTimeFormatInfo dateTimeFormatInfo = new DateTimeFormatInfo();
            dateTimeFormatInfo.ShortDatePattern = this.myCommand.GeneralSetting.ShortDateFormat;
            try
            {
                return (object)System.Convert.ToDateTime(dateTimeString, (IFormatProvider)dateTimeFormatInfo);
            }
            catch
            {
                try
                {
                    dateTimeFormatInfo.ShortDatePattern = "MM/dd/yyyy";
                    return (object)System.Convert.ToDateTime(dateTimeString, (IFormatProvider)dateTimeFormatInfo);
                }
                catch
                {
                    return (object)DBNull.Value;
                }
            }
        }

        public string ExportAsTabDelimiterText()
        {
            string str1 = "Stock Receive";
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.AppendLine(string.Format("{0} {1}", (object)OEM.GetCurrentOEM().ProductName, (object)str1));
            stringBuilder1.AppendLine();
            DataTable dataTable1 = this.myDataSet.Tables["Master"];
            DataTable dataTable2 = this.myDataSet.Tables["Detail"];
            DataRow dataRow1 = dataTable1.Rows[0];
            stringBuilder1.AppendLine("DocNo\t" + this.DocNo.ToString());
            DateTime date = (DateTime)this.DocDate;
            stringBuilder1.AppendLine("Date\t" + this.myCommand.GeneralSetting.FormatDate(date));
            stringBuilder1.AppendLine("Description\t" + this.Description.ToString());
            stringBuilder1.AppendLine("RefDocNo\t" + this.RefDocNo.ToString());
            stringBuilder1.AppendLine("Remark1\t" + this.Remark1.ToString());
            stringBuilder1.AppendLine("Remark2\t" + this.Remark2.ToString());
            stringBuilder1.AppendLine("Remark3\t" + this.Remark3.ToString());
            stringBuilder1.AppendLine("Remark4\t" + this.Remark4.ToString());
            //foreach (UDFColumn udfColumn in new UDFUtil(this.myCommand.myDBSetting).GetUDF("RPA_RCV"))
            //{
            //  if (dataTable1.Columns.Contains(udfColumn.FieldName))
            //  {
            //    if (udfColumn.Type == UDFType.Text || udfColumn.Type == UDFType.System || (udfColumn.Type == UDFType.Memo || udfColumn.Type == UDFType.ImageLink))
            //      stringBuilder1.AppendLine(udfColumn.FieldName + "\t" + StringHelper.AddDoubleQuoteString(dataRow1[udfColumn.FieldName].ToString()));
            //    else if (udfColumn.Type == UDFType.Boolean)
            //      stringBuilder1.AppendLine(udfColumn.FieldName + "\t" + BCE.Data.Convert.TextToBoolean(dataRow1[udfColumn.FieldName]).ToString());
            //    else if (udfColumn.Type == UDFType.Date)
            //    {
            //      string str2 = "";
            //      if (dataRow1[udfColumn.FieldName] != DBNull.Value)
            //        str2 = this.myCommand.GeneralSetting.FormatDate(BCE.Data.Convert.ToDateTime(dataRow1[udfColumn.FieldName]));
            //      stringBuilder1.AppendLine(udfColumn.FieldName + "\t" + str2);
            //    }
            //    else if (udfColumn.Type == UDFType.Decimal)
            //      stringBuilder1.AppendLine(udfColumn.FieldName + (object) "\t" + (string) (object) BCE.Data.Convert.ToDecimal(dataRow1[udfColumn.FieldName]));
            //    else if (udfColumn.Type == UDFType.Integer)
            //      stringBuilder1.AppendLine(udfColumn.FieldName + (object) "\t" + (string) (object) BCE.Data.Convert.ToInt32(dataRow1[udfColumn.FieldName]));
            //  }
            //}
            //UDFColumn[] udf = new UDFUtil(this.myCommand.myDBSetting).GetUDF("RPA_RCVDtl");
            StringBuilder stringBuilder2 = new StringBuilder();
            if (dataTable2.Columns.Contains("ItemCode"))
                stringBuilder2.Append("ItemCode\t");
            if (dataTable2.Columns.Contains("Description"))
                stringBuilder2.Append("Description\t");
            if (dataTable2.Columns.Contains("UOM"))
                stringBuilder2.Append("UOM\t");
            if (dataTable2.Columns.Contains("BatchNo"))
                stringBuilder2.Append("BatchNo\t");
            if (dataTable2.Columns.Contains("ProjNo"))
                stringBuilder2.Append("ProjNo\t");
            if (dataTable2.Columns.Contains("DeptNo"))
                stringBuilder2.Append("DeptNo\t");
            if (dataTable2.Columns.Contains("Qty"))
                stringBuilder2.Append("Qty\t");
           
            if (dataTable2.Columns.Contains("SubTotal"))
                stringBuilder2.Append("SubTotal\t");
            if (dataTable2.Columns.Contains("PrintOut"))
                stringBuilder2.Append("PrintOut\t");
            if (dataTable2.Columns.Contains("Location"))
                stringBuilder2.Append("Location\t");
            if (dataTable2.Columns.Contains("UnitCost"))
                stringBuilder2.Append("UnitCost\t");
            if (dataTable2.Columns.Contains("FurtherDescription"))
                stringBuilder2.Append("FurtherDescription");
            //foreach (UDFColumn udfColumn in udf)
            //{
            //  if (dataTable2.Columns.Contains(udfColumn.FieldName))
            //  {
            //    stringBuilder2.Append('\t');
            //    stringBuilder2.Append(udfColumn.FieldName);
            //  }
            //}
            stringBuilder1.AppendLine(((object)stringBuilder2).ToString());
            foreach (DataRow dataRow2 in dataTable2.Select("", "Seq"))
            {
                string str2 = "";
                if (dataRow2.Table.Columns.Contains("ItemCode"))
                    str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["ItemCode"].ToString()) + "\t";
                if (dataRow2.Table.Columns.Contains("Description"))
                    str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["Description"].ToString()) + "\t";
                if (dataRow2.Table.Columns.Contains("UOM"))
                    str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["UOM"].ToString()) + "\t";
                if (dataRow2.Table.Columns.Contains("BatchNo"))
                    str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["BatchNo"].ToString()) + "\t";
                if (dataRow2.Table.Columns.Contains("ProjNo"))
                    str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["ProjNo"].ToString()) + "\t";
                if (dataRow2.Table.Columns.Contains("DeptNo"))
                    str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["DeptNo"].ToString()) + "\t";
                if (dataRow2.Table.Columns.Contains("Qty"))
                {
                    Decimal number = BCE.Data.Convert.ToDecimal(dataRow2["Qty"]);
                    str2 = str2 + this.myDecimalSetting.FormatQuantity(number) + "\t";
                }
                
                if (dataRow2.Table.Columns.Contains("SubTotal"))
                {
                    Decimal number = BCE.Data.Convert.ToDecimal(dataRow2["SubTotal"]);
                    str2 = str2 + this.myDecimalSetting.FormatCurrency(number) + "\t";
                }
                if (dataRow2.Table.Columns.Contains("PrintOut"))
                    str2 = str2 + BCE.Data.Convert.TextToBoolean(dataRow2["PrintOut"]).ToString() + "\t";
                if (dataRow2.Table.Columns.Contains("Location"))
                    str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["Location"].ToString()) + "\t";
                if (dataRow2.Table.Columns.Contains("UnitCost"))
                {
                    Decimal number = BCE.Data.Convert.ToDecimal(dataRow2["UnitCost"]);
                    str2 = str2 + this.myDecimalSetting.FormatCost(number) + "\t";
                }
                if (dataRow2.Table.Columns.Contains("FurtherDescription"))
                {
                    string str3 = dataRow2["FurtherDescription"].ToString();
                    int length = str3.Length;
                    while (length > 0 && (int)str3[length - 1] == 0)
                        --length;
                    string aStr = str3.Substring(0, length);
                    str2 = str2 + StringHelper.AddDoubleQuoteString(aStr);
                }
                //foreach (UDFColumn udfColumn in udf)
                //{
                //  if (dataRow2.Table.Columns.Contains(udfColumn.FieldName))
                //  {
                //    if (udfColumn.Type == UDFType.Text || udfColumn.Type == UDFType.System || (udfColumn.Type == UDFType.Memo || udfColumn.Type == UDFType.ImageLink))
                //      str2 = str2 + "\t" + StringHelper.AddDoubleQuoteString(dataRow2[udfColumn.FieldName].ToString());
                //    else if (udfColumn.Type == UDFType.RichText)
                //    {
                //      string str3 = dataRow2[udfColumn.FieldName].ToString();
                //      int length = str3.Length;
                //      while (length > 0 && (int) str3[length - 1] == 0)
                //        --length;
                //      string aStr = str3.Substring(0, length);
                //      str2 = str2 + "\t" + StringHelper.AddDoubleQuoteString(aStr);
                //    }
                //    else if (udfColumn.Type == UDFType.Boolean)
                //      str2 = str2 + "\t" + BCE.Data.Convert.TextToBoolean(dataRow2[udfColumn.FieldName]).ToString();
                //    else if (udfColumn.Type == UDFType.Date)
                //    {
                //      string str3 = "";
                //      if (dataRow2[udfColumn.FieldName] != DBNull.Value)
                //        str3 = this.myCommand.GeneralSetting.FormatDate(BCE.Data.Convert.ToDateTime(dataRow2[udfColumn.FieldName]));
                //      str2 = str2 + "\t" + str3;
                //    }
                //    else if (udfColumn.Type == UDFType.Decimal)
                //      str2 = str2 + (object) "\t" + (string) (object) BCE.Data.Convert.ToDecimal(dataRow2[udfColumn.FieldName]);
                //    else if (udfColumn.Type == UDFType.Integer)
                //      str2 = str2 + (object) "\t" + (string) (object) BCE.Data.Convert.ToInt32(dataRow2[udfColumn.FieldName]);
                //  }
                //}
                stringBuilder1.AppendLine(str2);
            }
            return ((object)stringBuilder1).ToString();
        }

        public void BeginLoadDetailData()
        {
            this.myDetailTable.ColumnChanged -= new DataColumnChangeEventHandler(this.myDetailTable_ColumnChanged);
            this.myDetailTable.BeginLoadData();
        }

        public void EndLoadDetailData()
        {
            this.myDetailTable.EndLoadData();
            this.myDetailTable.ColumnChanged += new DataColumnChangeEventHandler(this.myDetailTable_ColumnChanged);
            this.UpdateSubTotal();
        }

        public DataRow[] GetValidDetailRows()
        {
            return this.myDetailTable.Select("", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
        }

        public void PrepareForDispose()
        {
            this.myCommand = (StockReceiveCommand)null;
            this.myMasterTable.ColumnChanged -= new DataColumnChangeEventHandler(this.myMasterTable_ColumnChanged);
            this.myDetailTable.RowChanged -= new DataRowChangeEventHandler(this.myDetailTable_RowChanged);
            this.myDetailTable.ColumnChanged -= new DataColumnChangeEventHandler(this.myDetailTable_ColumnChanged);
            this.myDetailTable.RowDeleting -= new DataRowChangeEventHandler(this.myDetailTable_RowDeleting);
            this.myDetailTable.RowDeleted -= new DataRowChangeEventHandler(this.myDetailTable_RowDeleted);
            this.myDetailTable.ColumnChanging -= new DataColumnChangeEventHandler(this.myDetailTable_ColumnChanging);
            this.myMasterTable.Clear();
            this.myDetailTable.Clear();
            this.myMasterTable.PrimaryKey = (DataColumn[])null;
            this.myMasterTable.Columns.Clear();
            this.myDetailTable.PrimaryKey = (DataColumn[])null;
            this.myDetailTable.Columns.Clear();
        }

        public bool IsModified()
        {
            if (this.myDataSet.Tables.IndexOf("ShadowMaster") < 0)
                return false;
            else if (this.myDetailTable.GetChanges() != null)
                return true;
            else
                return !DataTableHelper.IsTablesEqual(this.myDataSet.Tables["Master"], this.myDataSet.Tables["ShadowMaster"]);
        }

        public void UpdateUnitCostWithUpToDateCost(DataRow r)
        {
            if (r["ItemCode"] != DBNull.Value && r["Location"] != DBNull.Value)
            {
                Decimal num = BCE.Data.Convert.ToDecimal(r["Qty"]);
                if (num > Decimal.Zero)
                {
                    UTDCosting utdCosting = UTDCosting.Create(this.myCommand.myDBSetting);
                    ComputedCost computedCost = (ComputedCost)null;
                    UTDCostHelper utdCostHelper = UTDCostHelper.Create(this.myCommand.DBSetting, r["ItemCode"].ToString(), r["UOM"].ToString(), r["Location"].ToString(), r["BatchNo"], (DateTime)this.DocDate);
                    if (r.RowState == DataRowState.Added)
                        computedCost = utdCosting.GetNewUTDCost(utdCostHelper, num, false);
                    else if (r.RowState == DataRowState.Modified || r.RowState == DataRowState.Unchanged)
                    {
                        FIFOCost fifoCostForDtlKey = StockCosting.Create(this.myCommand.myDBSetting).GetFIFOCostForDtlKey(BCE.Data.Convert.ToInt64(r["DtlKey"]));
                        computedCost = utdCosting.GetEditUTDCost(utdCostHelper, BCE.Data.Convert.ToDecimal(r["Qty", DataRowVersion.Original]), num, BCE.Data.Convert.ToDecimal(r["UnitCost", DataRowVersion.Original]), fifoCostForDtlKey, false);
                    }
                    if (computedCost != null)
                    {
                        r["UnitCost"] = (object)(-computedCost.TotalCost / num);
                        r.EndEdit();
                    }
                }
            }
        }

        public void UpdateUnitCostWithReferenceCost(DataRow r)
        {
            StockDocumentItem stockDocumentItem = this.myCommand.myHelper.LoadStockDocumentItem(r["ItemCode"].ToString());
            r["UnitCost"] = stockDocumentItem.Cost;
            r.EndEdit();
        }

        public StockReceiveDetail AddDetail(DataRow drImport)
        {
            if (this.myAction == StockReceiveAction.View)
                throw new Exception("Cannot edit read-only Stock Receive.");
            else
            {
                if (drImport == null)
                    return this.InternalAddDetail(SeqUtils.GetLastSeq(this.GetValidDetailRows()));
                else
                    return this.InternalAddDetail(SeqUtils.GetLastSeq(this.GetValidDetailRows()), drImport);

            }
        }

        public StockReceiveDetail GetDetailByDtlKey(long dtlKey)
        {
            DataRow row = this.myDetailTable.Rows.Find((object)dtlKey);
            if (row == null || row.RowState == DataRowState.Deleted)
                return (StockReceiveDetail)null;
            else
                return new StockReceiveDetail(row, this);
        }

        public StockReceiveDetail InsertDetailBefore(int index)
        {
            if (this.myAction == StockReceiveAction.View)
                throw new Exception("Cannot edit read-only Stock Receive.");
            else
                return this.InternalAddDetail(SeqUtils.GetNewSeqAtThisIndex(index, this.GetValidDetailRows()));
        }

        public StockReceiveDetail EditDetail(int index)
        {
            if (this.myAction == StockReceiveAction.View)
            {
                throw new InvalidOperationException("Cannot edit detail in the document which is in View mode.");
            }
            else
            {
                DataRow[] validDetailRows = this.GetValidDetailRows();
                // validDetailRows.
                if (index >= 0 && index < validDetailRows.Length)
                    return new StockReceiveDetail(validDetailRows[index], this);
                else
                    return (StockReceiveDetail)null;
            }
        }

        public StockReceiveDetail EditDetail(long dtlKey)
        {
            if (this.myAction == StockReceiveAction.View)
            {
                throw new InvalidOperationException("Cannot edit detail in the document which is in View mode.");
            }
            else
            {
                DataRow row = this.myDetailTable.Rows.Find((object)dtlKey);
                if (row == null)
                    return (StockReceiveDetail)null;
                else if (row.RowState == DataRowState.Deleted)
                    throw new InvalidOperationException("Cannot edit a deleted detail record.");
                else
                    return new StockReceiveDetail(row, this);
            }
        }

        public bool DeleteDetail(int index)
        {
            if (this.myAction == StockReceiveAction.View)
            {
                throw new InvalidOperationException("Cannot delete detail in the document which is in View mode.");
            }
            else
            {
                DataRow[] validDetailRows = this.GetValidDetailRows();
                if (index >= 0 && index < validDetailRows.Length)
                {
                    validDetailRows[index].Delete();
                    return true;
                }
                else
                    return false;
            }
        }

        public bool DeleteDetail(long dtlKey)
        {
            if (this.myAction == StockReceiveAction.View)
            {
                throw new InvalidOperationException("Cannot delete detail in the document which is in View mode.");
            }
            else
            {
                DataRow dataRow = this.myDetailTable.Rows.Find((object)dtlKey);
                if (dataRow == null || dataRow.RowState == DataRowState.Deleted)
                {
                    return false;
                }
                else
                {
                    dataRow.Delete();
                    return true;
                }
            }
        }

        public void ClearDetails()
        {
            if (this.myAction == StockReceiveAction.View)
            {
                throw new InvalidOperationException("Cannot clear all details in the document which is in View mode.");
            }
            else
            {
                foreach (DataRow dataRow in this.GetValidDetailRows())
                    dataRow.Delete();
            }
        }

        private StockReceiveDetail InternalAddDetail(int seq)
        {
            DataRow row = this.myDetailTable.NewRow();
            if (row.Table.Columns.Contains("Guid"))
                row["Guid"] = (object)Guid.NewGuid();
            row["DtlKey"] = (object)this.myCommand.DBReg.IncOne((IRegistryID)new GlobalUniqueKey());
            row["DocKey"] = this.myRow["DocKey"];
            row["Seq"] = (object)seq;
            row["Qty"] = (object)0;
           
            row["WIPCost"] = (object)0;
            row["PrintOut"] = (object)BCE.Data.Convert.BooleanToText(true);
            row["ProjNo"] = this.myUserAuthentication.DefaultProject;
            row["DeptNo"] = this.myUserAuthentication.DefaultDepartment;
            this.CopyLastDetailRow(row);
            row.EndEdit();
            this.myDetailTable.Rows.Add(row);
            return new StockReceiveDetail(row, this);
        }
        private StockReceiveDetail InternalAddDetail(int seq, DataRow drimport)
        {
            //BeginLoadDetailData();
            DataRow row = this.myDetailTable.NewRow();
            if (row.Table.Columns.Contains("Guid"))
                row["Guid"] = (object)Guid.NewGuid();
            row["DtlKey"] = (object)this.myCommand.DBReg.IncOne((IRegistryID)new GlobalUniqueKey());
            row["DocKey"] = this.myRow["DocKey"];
            row["Seq"] = (object)seq;
            row["PrintOut"] = (object)BCE.Data.Convert.BooleanToText(true);
            row["ProjNo"] = this.myUserAuthentication.DefaultProject;
            row["DeptNo"] = this.myUserAuthentication.DefaultDepartment;
            row["ItemCode"] = drimport["ItemCode"];
            row["Location"] = this.myUserAuthentication.MainLocation;
            object obj = myCommand.DBSetting.ExecuteScalar("select BaseUOM from item where ItemCode=?", (object)drimport["ItemCode"]);
            if (obj != null && obj != DBNull.Value)
            {
                row["UOM"] = obj;
            }
            
            row["WIPCost"] = (object)0;
            // row["Qty"] = drimport["Qty"];
            // row["UnitCost"] = drimport["UnitCost"];

            row["FromDocNo"] = drimport["DocNo"];
            row["FromDocDtlKey"] = drimport["Code"];
            if (drimport["DocType"].ToString() == "Alternatif Product")
                row["FromDocType"] = "AP";
            else if (drimport["DocType"].ToString() == "Product sortiran")
                row["FromDocType"] = "BS";
            else if (drimport["DocType"].ToString() == "Main Product")
                row["FromDocType"] = "MP";
            DataRow drItemGroup = myCommand.DBSetting.GetFirstDataRow("select PurchaseCode,UDF_WIP from ItemGroup inner join Item on Item.ItemGroup=ItemGroup.ItemGroup where ItemCode=?", (object)row["ItemCode"]);
            if (drItemGroup != null)
            {
                if (!row["Credit"].Equals(drItemGroup["UDF_WIP"]))
                    row["Credit"] = drItemGroup["UDF_WIP"];
                if (!row["Debit"].Equals(drItemGroup["PurchaseCode"]))
                    row["Debit"] = drItemGroup["PurchaseCode"];

            }
            this.CopyLastDetailRow(row);
            row.EndEdit();
            this.myDetailTable.Rows.Add(row);
            UpdateUnitCost(row);
            UpdateSubTotal();
            // EndLoadDetailData()
            return new StockReceiveDetail(row, this);
        }
        private DataRow GetLastDetailRow(int seq)
        {
            DataRow[] dataRowArray = this.myDetailTable.Select(string.Format("Seq < {0}", (object)seq), "Seq DESC");
            if (dataRowArray.Length == 0)
                dataRowArray = this.myDetailTable.Select(string.Format("Seq > {0}", (object)seq), "Seq");
            if (dataRowArray.Length != 0)
                return dataRowArray[0];
            else
                return (DataRow)null;
        }

        private void CopyLastDetailRow(DataRow row)
        {
            string[] strArray1 = new string[2];
            int index1 = 0;
            string str1 = "ProjNo";
            strArray1[index1] = str1;
            int index2 = 1;
            string str2 = "DeptNo";
            strArray1[index2] = str2;
            string[] strArray2 = strArray1;
            DataRow lastDetailRow = this.GetLastDetailRow(BCE.Data.Convert.ToInt32(row["Seq"]));
            if (lastDetailRow != null)
            {
                foreach (string name in strArray2)
                {
                    if (row.Table.Columns.Contains(name))
                        row[name] = lastDetailRow[name];
                }
            }
        }

        private void myDetailTable_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            if (string.Compare(e.Column.ColumnName, "ItemCode", true) == 0)
            {
                StockDocumentItem stockDocumentItem = this.myCommand.myHelper.LoadStockDocumentItem(e.Row[e.Column].ToString());
                if (stockDocumentItem != null)
                {
                    if (!e.Row["ItemCode"].ToString().Equals(stockDocumentItem.ItemCode))
                    {
                        this.DisableColumnChanged();
                        e.Row["ItemCode"] = (object)stockDocumentItem.ItemCode;


                        this.EnableColumnChanged();
                    }
                    if (this.myEnableAutoLoadItemDetail)
                    {
                        if (!e.Row["Description"].Equals(stockDocumentItem.Description))
                            e.Row["Description"] = stockDocumentItem.Description;
                        if (!e.Row["FurtherDescription"].Equals(stockDocumentItem.FurtherDescription))
                            e.Row["FurtherDescription"] = stockDocumentItem.FurtherDescription == DBNull.Value ? (object)DBNull.Value : (object)Rtf.ToArialRichText(stockDocumentItem.FurtherDescription.ToString());
                        if (!e.Row["UOM"].Equals(stockDocumentItem.UOM))
                            e.Row["UOM"] = stockDocumentItem.UOM;
                        // if(stockDocumentItem.)

                    }
                    if (!e.Row.IsNull("BatchNo"))
                        e.Row["BatchNo"] = (object)DBNull.Value;
                    if (e.Row["ItemCode"] == DBNull.Value)
                    {
                        if (!e.Row.IsNull("Location"))
                            e.Row["Location"] = (object)DBNull.Value;
                    }
                    else
                    {
                        DataRow lastDetailRow = this.GetLastDetailRow(BCE.Data.Convert.ToInt32(e.Row["Seq"]));
                        if (lastDetailRow != null && !lastDetailRow.Equals((object)e.Row) && lastDetailRow["Location"] != DBNull.Value)
                        {
                            if (!e.Row["Location"].Equals(lastDetailRow["Location"]))
                                e.Row["Location"] = lastDetailRow["Location"];
                        }
                        else if (!e.Row["Location"].Equals((object)this.myUserAuthentication.MainLocation))
                            e.Row["Location"] = (object)this.myUserAuthentication.MainLocation;
                    }
                    DataRow drItemGroup = myCommand.DBSetting.GetFirstDataRow("select PurchaseCode,UDF_WIP from ItemGroup inner join Item on Item.ItemGroup=ItemGroup.ItemGroup where ItemCode=?", (object)e.Row["ItemCode"]);
                    if (drItemGroup != null)
                    {
                        if (!e.Row["Credit"].Equals(drItemGroup["UDF_WIP"]))
                            e.Row["Credit"] = drItemGroup["UDF_WIP"];
                        if (!e.Row["Debit"].Equals(drItemGroup["PurchaseCode"]))
                            e.Row["Debit"] = drItemGroup["PurchaseCode"];

                    }
                    object obj = myCommand.DBSetting.ExecuteScalar("select CostType from RPA_ItemBOM where BOMCode='"+ e.Row["ItemCode"] + "'");
                    if(obj!=null && obj!=DBNull.Value)
                    {
                        if (obj.ToString() == "Standard")
                            this.UpdateUnitCostWithReferenceCost(e.Row);
                        else
                            this.UpdateUnitCost(e.Row);
                    }
                    else
                        this.UpdateUnitCost(e.Row);
                }
            }
            else if (string.Compare(e.Column.ColumnName, "UOM", true) == 0 || string.Compare(e.Column.ColumnName, "Location", true) == 0 || string.Compare(e.Column.ColumnName, "BatchNo", true) == 0)
            {
                object obj = myCommand.DBSetting.ExecuteScalar("select CostType from RPA_ItemBOM where BOMCode='" + e.Row["ItemCode"] + "'");
                if (obj != null && obj != DBNull.Value)
                {
                    if (obj.ToString() == "Standard")
                        this.UpdateUnitCostWithReferenceCost(e.Row);
                    else
                        this.UpdateUnitCost(e.Row);
                }
                else
                    this.UpdateUnitCost(e.Row);
                if (string.Compare(e.Column.ColumnName, "UOM", true) == 0)
                {
                    long num = System.Convert.ToInt64(this.myDecimalSetting.RoundQuantity(BCE.Data.Convert.ToDecimal(e.Row["Qty"]) * InvoicingHelper.GetItemRate(this.myCommand.DBSetting, e.Row["ItemCode"].ToString(), e.Row["UOM"].ToString())));
                    // ISSUE: reference to a compiler-generated field
                    if (this.GetSerialNumberCount(BCE.Data.Convert.ToInt64(e.Row["DtlKey"])) != num && this.ShowSerialNumberEntryFormEvent != null)
                    {
                        // ISSUE: reference to a compiler-generated field
                        this.ShowSerialNumberEntryFormEvent(BCE.Data.Convert.ToInt64(e.Row["DtlKey"]));
                    }
                }
            }
            else if (string.Compare(e.Column.ColumnName, "Qty", true) == 0 || string.Compare(e.Column.ColumnName, "UnitCost", true) == 0)
            {
                if (string.Compare(e.Column.ColumnName, "Qty", true) == 0)
                {
                    object obj = myCommand.DBSetting.ExecuteScalar("select CostType from RPA_ItemBOM where BOMCode='" + e.Row["ItemCode"] + "'");
                    if (obj != null && obj != DBNull.Value)
                    {
                        if (obj.ToString() == "Standard")
                            this.UpdateUnitCostWithReferenceCost(e.Row);
                        else
                            this.UpdateUnitCost(e.Row);
                    }
                    else
                        this.UpdateUnitCost(e.Row);
                    long num = System.Convert.ToInt64(this.myDecimalSetting.RoundQuantity(BCE.Data.Convert.ToDecimal(e.Row["Qty"]) * InvoicingHelper.GetItemRate(this.myCommand.DBSetting, e.Row["ItemCode"].ToString(), e.Row["UOM"].ToString())));
                    // ISSUE: reference to a compiler-generated field
                    if (this.GetSerialNumberCount(BCE.Data.Convert.ToInt64(e.Row["DtlKey"])) != num && this.ShowSerialNumberEntryFormEvent != null)
                    {
                        // ISSUE: reference to a compiler-generated field
                        this.ShowSerialNumberEntryFormEvent(BCE.Data.Convert.ToInt64(e.Row["DtlKey"]));
                    }
                }
                this.CalcSubTotal(e.Row);
            }
            else if (string.Compare(e.Column.ColumnName, "SubTotal", true) == 0)
                this.UpdateSubTotal();
            StockReceiveDetailColumnChangedEventArgs changedEventArgs1 = new StockReceiveDetailColumnChangedEventArgs(e.Column.ColumnName, this, e.Row);
            ScriptObject scriptObject = this.myScriptObject;
            string name = "OnDetailColumnChanged";
            Type[] types = new Type[1];
            int index1 = 0;
            Type type = changedEventArgs1.GetType();
            types[index1] = type;
            object[] objArray = new object[1];
            int index2 = 0;
            StockReceiveDetailColumnChangedEventArgs changedEventArgs2 = changedEventArgs1;
            objArray[index2] = (object)changedEventArgs2;
            scriptObject.RunMethod(name, types, objArray);
        }

        private void myDetailTable_RowDeleting(object sender, DataRowChangeEventArgs e)
        {
            StockReceiveDetailDeletingEventArgs deletingEventArgs1 = new StockReceiveDetailDeletingEventArgs(this, e.Row);
            ScriptObject scriptObject = this.myScriptObject;
            string name = "OnDetailDeleting";
            Type[] types = new Type[1];
            int index1 = 0;
            Type type = deletingEventArgs1.GetType();
            types[index1] = type;
            object[] objArray = new object[1];
            int index2 = 0;
            StockReceiveDetailDeletingEventArgs deletingEventArgs2 = deletingEventArgs1;
            objArray[index2] = (object)deletingEventArgs2;
            scriptObject.RunMethod(name, types, objArray);
        }

        private void myDetailTable_RowDeleted(object sender, DataRowChangeEventArgs e)
        {
            this.UpdateSubTotal();
            StockReceiveDetailDeletedEventArgs deletedEventArgs1 = new StockReceiveDetailDeletedEventArgs(this);
            ScriptObject scriptObject = this.myScriptObject;
            string name = "OnDetailDeleted";
            Type[] types = new Type[1];
            int index1 = 0;
            Type type = deletedEventArgs1.GetType();
            types[index1] = type;
            object[] objArray = new object[1];
            int index2 = 0;
            StockReceiveDetailDeletedEventArgs deletedEventArgs2 = deletedEventArgs1;
            objArray[index2] = (object)deletedEventArgs2;
            scriptObject.RunMethod(name, types, objArray);
        }

        public void CalcSubTotal(DataRow row)
        {
            if (row["Qty"] == DBNull.Value && row["UnitCost"] == DBNull.Value)
            {
                row["SubTotal"] = (object)DBNull.Value;
            }
            else
            {
                Decimal num = this.myDecimalSetting.RoundCurrency(this.myDecimalSetting.RoundQuantity(row["Qty"]) * this.myDecimalSetting.RoundCost(row["UnitCost"]));
                row["SubTotal"] = (object)num;
            }
        }

        public void UpdateSubTotal()
        {

            Decimal num = new Decimal();
            foreach (DataRow dataRow in this.GetValidDetailRows())
                num += BCE.Data.Convert.ToDecimal(dataRow["SubTotal"]);
            this.myRow.BeginEdit();
            this.myRow["Total"] = (object)num;
            this.myRow.EndEdit();
        }

        public void RecalcRows(DataRow[] rowList)
        {
            this.BeginLoadDetailData();
            foreach (DataRow row in rowList)
                this.CalcSubTotal(row);
            this.EndLoadDetailData();
        }

        public void RecalcAll()
        {
            this.RecalcRows(this.GetValidDetailRows());
        }

        public void UpdateUnitCost(DataRow r)
        {
            if (r["ItemCode"] != DBNull.Value && r["Location"] != DBNull.Value)
            {
                Decimal num = BCE.Data.Convert.ToDecimal(r["Qty"]);
                if (num > Decimal.Zero)
                {
                    UTDCosting utdCosting = UTDCosting.Create(this.myCommand.myDBSetting);
                    ComputedCost computedCost = (ComputedCost)null;
                    UTDCostHelper utdCostHelper = UTDCostHelper.Create(this.myCommand.DBSetting, r["ItemCode"].ToString(), r["UOM"].ToString(), r["Location"].ToString(), r["BatchNo"], (DateTime)this.DocDate);
                    if (r.RowState == DataRowState.Added)
                        computedCost = utdCosting.GetNewUTDCost(utdCostHelper, num, false);
                    else if (r.RowState == DataRowState.Modified || r.RowState == DataRowState.Unchanged)
                    {
                        FIFOCost fifoCostForDtlKey = StockCosting.Create(this.myCommand.myDBSetting).GetFIFOCostForDtlKey(BCE.Data.Convert.ToInt64(r["DtlKey"]));
                        computedCost = utdCosting.GetEditUTDCost(utdCostHelper, BCE.Data.Convert.ToDecimal(r["Qty", DataRowVersion.Original]), num, BCE.Data.Convert.ToDecimal(r["UnitCost", DataRowVersion.Original]), fifoCostForDtlKey, false);
                    }
                    if (computedCost != null)
                    {
                        r["UnitCost"] = (object)(-computedCost.TotalCost / num);
                        r.EndEdit();
                    }
                }
            }
        }

        private void myDetailTable_ColumnChanging(object sender, DataColumnChangeEventArgs e)
        {
            // ISSUE: reference to a compiler-generated field
            if (string.Compare(e.Column.ColumnName, "ItemCode", true) == 0 && e.Row["DtlKey"].ToString().Trim().Length != 0 && this.ConfirmChangingItemCodeEvent != null)
            {
                // ISSUE: reference to a compiler-generated field
                if (this.ConfirmChangingItemCodeEvent(BCE.Data.Convert.ToInt64(e.Row["DtlKey"]), e.ProposedValue.ToString()))
                {
                    foreach (DataRow dataRow in this.mySerialNoTable.Select("DtlKey = " + e.Row["DtlKey"].ToString()))
                        dataRow.Delete();
                }
                else
                    e.ProposedValue = e.Row["ItemCode"];
            }
        }

        public void DisableColumnChanged()
        {
            this.myDisableColumnChangedCounter = this.myDisableColumnChangedCounter + 1;
        }

        public void EnableColumnChanged()
        {
            if (this.myDisableColumnChangedCounter > 0)
                this.myDisableColumnChangedCounter = this.myDisableColumnChangedCounter - 1;
        }

        public bool IsColumnChangedDisable()
        {
            return this.myDisableColumnChangedCounter > 0;
        }

        private void myMasterTable_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            if (!this.IsColumnChangedDisable())
            {
                StockReceiveMasterColumnChangedEventArgs changedEventArgs1 = new StockReceiveMasterColumnChangedEventArgs(e.Column.ColumnName, this);
                ScriptObject scriptObject = this.myScriptObject;
                string name = "OnMasterColumnChanged";
                Type[] types = new Type[1];
                int index1 = 0;
                Type type = changedEventArgs1.GetType();
                types[index1] = type;
                object[] objArray = new object[1];
                int index2 = 0;
                StockReceiveMasterColumnChangedEventArgs changedEventArgs2 = changedEventArgs1;
                objArray[index2] = (object)changedEventArgs2;
                scriptObject.RunMethod(name, types, objArray);
            }
        }

        private void myDetailTable_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Add)
            {
                StockReceiveNewDetailEventArgs newDetailEventArgs1 = new StockReceiveNewDetailEventArgs(this, e.Row);
                ScriptObject scriptObject = this.myScriptObject;
                string name = "OnNewDetail";
                Type[] types = new Type[1];
                int index1 = 0;
                Type type = newDetailEventArgs1.GetType();
                types[index1] = type;
                object[] objArray = new object[1];
                int index2 = 0;
                StockReceiveNewDetailEventArgs newDetailEventArgs2 = newDetailEventArgs1;
                objArray[index2] = (object)newDetailEventArgs2;
                scriptObject.RunMethod(name, types, objArray);
            }
        }
    }
}
