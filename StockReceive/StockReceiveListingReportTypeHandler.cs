﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveListingReportTypeHandler
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Stock;
using BCE.Data;

namespace BCE.AutoCount.Stock.StockReceive
{
  public class StockReceiveListingReportTypeHandler : StockDocumentReportTypeHandler
  {
    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      return StockReceiveCommand.Create(dbSetting).GetDocumentListingReportDesignerDataSource((StockReceiveReportingCriteria) null);
    }
  }
}
