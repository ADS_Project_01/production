﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveRecord
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Stock;
using BCE.Data;
using System;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveRecord : StockDocumentRecord
  {
    private StockReceive myStock;

    public object UserData
    {
      get
      {
        return this.myStock.UserData;
      }
      set
      {
        this.myStock.UserData = value;
      }
    }

    internal StockReceiveRecord(StockReceive doc)
      : base(doc.DataTableMaster.Rows[0], doc.GetValidDetailRows())
    {
      this.myStock = doc;
    }

    public StockReceiveDetailRecord GetDetailRecord(int index)
    {
      return new StockReceiveDetailRecord(this.myStock.Command.DBSetting, this.myDetailRows[index]);
    }

    public StockReceiveDetailRecord NewDetail()
    {
      StockReceiveDetail stockReceiveDetail = this.myStock.AddDetail(null);
      this.myDetailRows = this.myStock.GetValidDetailRows();
      return new StockReceiveDetailRecord(this.myStock.Command.DBSetting, stockReceiveDetail.Row);
    }

    public bool DeleteDetailRecord(int index)
    {
      if (index < 0 || index >= this.myDetailRows.Length)
        throw new ArgumentException("index not in valid range.");
      else if (this.myStock.DeleteDetail(BCE.Data.Convert.ToInt64(this.myDetailRows[index]["DtlKey"])))
      {
        this.myDetailRows = this.myStock.GetValidDetailRows();
        return true;
      }
      else
        return false;
    }

    public void SetDocNoFormatName(string formatName)
    {
      this.myStock.DocNoFormatName = formatName;
    }
  }
}
