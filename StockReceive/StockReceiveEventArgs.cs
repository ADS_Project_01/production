﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveEventArgs
  {
    private StockReceive myStock;

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.DBSetting;
      }
    }

    public StockReceiveRecord MasterRecord
    {
      get
      {
        return new StockReceiveRecord(this.myStock);
      }
    }

    internal StockReceiveEventArgs(StockReceive doc)
    {
      this.myStock = doc;
    }
  }
}
