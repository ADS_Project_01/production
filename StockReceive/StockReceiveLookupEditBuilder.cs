﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.ItemBOMLookupEditBuilder
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll
using BCE.AutoCount;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
namespace Production.StockReceive
{
  public class StockReceiveLookupEditBuilder : LookupEditBuilderEx
  {
        private string myLastDocNo = "!@#!@#!!@#";
        public StockReceiveLookupEditBuilder()
    {
      //this.myUseCaching = true;
      //this.myCacheFileName = "RPA_RCV.Data";
      this.myCmdString = "SELECT DocNo,DocDate, Description FROM RPA_RCV";
    }

    protected override void SetupLookupEdit(RepositoryItemLookUpEdit myLookupEdit)
    {
      base.SetupLookupEdit(myLookupEdit);
      myLookupEdit.DisplayMember = "DocNo";
      myLookupEdit.ValueMember = "DocNo";
      myLookupEdit.DataSource = (object) this.myDataTable;
      myLookupEdit.Columns.Clear();
            myLookupEdit.Columns.Add(new LookUpColumnInfo("DocNo", "DocNo",150));
            myLookupEdit.Columns.Add(new LookUpColumnInfo("DocDate", "DocDate",100));
            myLookupEdit.Columns.Add(new LookUpColumnInfo("Description", BCE.Localization.Localizer.GetString((Enum)Production.GeneralMaintenance.ItemBOM.ItemBOMString.Description, new object[0]), 150));
      myLookupEdit.PopupWidth = 400;
      myLookupEdit.AutoSearchColumnIndex = 1;
      myLookupEdit.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
      myLookupEdit.KeyPress += new KeyPressEventHandler(this.LookupEdt_KeyPress);
    }
        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastDocNo = "!@#!@#!!@#";
        }

        public void FilterLookup(string docno)
        {
            if (!(docno == this.myLastDocNo) || this.NeedUpdate())
            {
                this.myLastDocNo = docno;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command = sqlConnection.CreateCommand();
                        string str = "SELECT DocNo,DocDate, Description FROM RPA_RCV where exists(select 1 from RPA_RCVDTL WHERE COALESCE(Qty, 0) - COALESCE(TransferedQty, 0) > 0 and RPA_RCVDTL.DocKey = RPA_RCV.DocKey )";
                        command.CommandText = str;
                        //command.Parameters.AddWithValue("@ItemCode", (object)docno);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        StockReceiveLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }

        protected override int GetChangeCount()
    {
      return this.myChangeCount.GetChangeCount("RPA_WO");
    }

    public override void UnlinkEventHandlers(RepositoryItemLookUpEdit lookupEdit)
    {
      base.UnlinkEventHandlers(lookupEdit);
      lookupEdit.KeyPress -= new KeyPressEventHandler(this.LookupEdt_KeyPress);
    }

    private void LookupEdt_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (sender is LookUpEdit)
      {
        LookUpEdit lookUpEdit = sender as LookUpEdit;
        if (!lookUpEdit.IsPopupOpen)
        {
          BaseView baseView = (BaseView) null;
          if ((int) e.KeyChar >= 32 && (int) e.KeyChar <= 126)
          {
            if (baseView != null)
            {
              baseView.ShowEditorByKey(new KeyEventArgs((Keys) 115));
              baseView.ShowEditorByKeyPress(new KeyPressEventArgs(e.KeyChar));
            }
            else
            {
              lookUpEdit.ShowPopup();
              lookUpEdit.SendKey((object) new Message(), new KeyPressEventArgs(e.KeyChar));
            }
            e.Handled = true;
          }
        }
      }
    }
  }
}
