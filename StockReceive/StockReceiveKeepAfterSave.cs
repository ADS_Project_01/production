﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveKeepAfterSave
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveKeepAfterSave
  {
    public bool CheckKeepAfterSave;
    public int InstantInfoHeight;
    public bool MaximizeWindow;
  }
}
