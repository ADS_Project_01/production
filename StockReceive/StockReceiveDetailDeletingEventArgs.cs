﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveDetailDeletingEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;
using System.Data;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveDetailDeletingEventArgs : StockReceiveNewDetailEventArgs
  {
    internal StockReceiveDetailDeletingEventArgs(StockReceive doc, DataRow detailRow)
      : base(doc, detailRow)
    {
    }
  }
}
