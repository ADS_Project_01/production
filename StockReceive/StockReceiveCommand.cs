﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveCommand
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.Data;
using BCE.AutoCount.Document;
using BCE.AutoCount.GL.JournalEntry;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.RegistryID.LastSavedDescriptionID;
using BCE.AutoCount.RegistryID.Misc;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.SerialNumber2;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Production.StockReceive
{
  public class StockReceiveCommand
  {
    public const string DocumentStyleReportType = "Stock Receive FG Document";
    public const string ListingStyleReportType = "RPA Stock Receive FG Listing";
    protected const string ReportOptionSettingFileName = "StockReceiveReportOption.setting";
    internal const string MasterTableName = "Master";
    internal const string ShadowMasterTableName = "ShadowMaster";
    internal const string DetailTableName = "Detail";
    internal const string SerialNoTableName = "SerialNoTrans";
    protected DataTable myDataTableAllMaster;
    protected internal StockHelper myHelper;
    protected internal DBSetting myDBSetting;
    internal FiscalYear myFiscalYear;
    internal GeneralSetting myGeneralSetting;
    private BasicReportOption myReportOption;
    protected const string LoadMasterDataSQL = "SELECT * FROM RPA_RCV WHERE DocKey=@DocKey";
    protected const string LoadDetailDataSQL = "SELECT * FROM RPA_RCVDtl WHERE DocKey=@DocKey ORDER BY Seq";
    protected const string LoadMasterDataByDocNoSQL = "SELECT * FROM RPA_RCV WHERE DocNo=@DocNo";
    protected const string LoadDetailDataByDocNoSQL = "SELECT A.* FROM RPA_RCVDtl A, RPA_RCV B WHERE A.DocKey=B.DocKey AND B.DocNo=@DocNo ORDER BY A.Seq";
    protected const string SaveMasterDataSQL = "SELECT * FROM RPA_RCV";
    protected const string SaveDetailDataSQL = "SELECT * FROM RPA_RCVDtl";
    protected const string DeleteMasterDataSQL = "DELETE FROM RPA_RCV WHERE DocKey=@DocKey";
    protected const string DeleteDetailDataSQL = "DELETE FROM RPA_RCVDtl WHERE DocKey=@DocKey";
    protected const string LoadAllMasterDataSQL = "SELECT {0} FROM RPA_RCV {1}";
    protected const string LoadSearchMasterDataSQL = "SELECT DISTINCT {0} FROM RPA_RCV A, RPA_RCVDtl B WHERE (A.DocKey=B.DocKey)";
    protected const string LoadSearchMasterDataOnlySQL = "SELECT {0} FROM RPA_RCV A";
    protected const string LoadDesignReportMasterDataSQL = "SELECT TOP 5 * FROM vRM ORDER BY DocKey";
    protected const string LoadDesignReportDetailDataSQL = "SELECT * FROM vRMDtl WHERE DocKey IN (SELECT TOP 5 DocKey FROM vRM ORDER BY DocKey) AND PrintOut='T' ORDER BY DocKey, Seq";
    protected const string LoadReportMasterDataSQL = "SELECT * FROM vRM WHERE DocKey=@DocKey";
    protected const string LoadReportDetailDataSQL = "SELECT * FROM vRMDtl WHERE DocKey=@DocKey AND PrintOut='T' ORDER BY Seq";
    protected const string BasicSearchSQL = " SELECT {0} FROM RPA_RCV WHERE DocKey In (SELECT DISTINCT RPA_RCV.DocKey FROM RPA_RCV INNER JOIN RPA_RCVDtl ON RPA_RCV.DocKey = RPA_RCVDtl.DocKey WHERE {1})";
    protected const string LoadDocumentListingReportMasterDataSQL = "SELECT * FROM vRM WHERE DocKey IN ({0}) ";
    protected const string LoadDocumentListingReportDetailDataSQL = "SELECT * FROM vRMDtl WHERE DocKey IN ({0}) ORDER BY Seq";
    protected const string LoadDesignDocumentListingReportMasterDataSQL = "SELECT TOP 100 * FROM vRM ORDER BY DocKey";
    protected const string LoadDesignDocumentListingReportDetailDataSQL = "SELECT * FROM vRMDtl WHERE DocKey IN (SELECT TOP 100 DocKey FROM vRM ORDER BY DocKey) ORDER BY DocKey, Seq";
    protected const string DocumentStyleReportingBasicSearchSQL = " SELECT DISTINCT {0} FROM RPA_RCV LEFT OUTER JOIN RPA_RCVDtl ON (RPA_RCV.DocKey=RPA_RCVDtl.DocKey) WHERE {1}";
    private static AsyncDataSetUpdate myDataSetUpdate;
    private DBRegistry myDBReg;
    protected DecimalSetting myDecimalSetting;
    protected UserAuthentication myUserAuthentication;

    public static AsyncDataSetUpdate DataSetUpdate
    {
      get
      {
        if (StockReceiveCommand.myDataSetUpdate == null)
          StockReceiveCommand.myDataSetUpdate = new AsyncDataSetUpdate();
        return StockReceiveCommand.myDataSetUpdate;
      }
    }

    public BasicReportOption ReportOption
    {
      get
      {
        return this.myReportOption;
      }
    }

    public DataTable DataTableAllMaster
    {
      get
      {
        return this.myDataTableAllMaster;
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    public DBRegistry DBReg
    {
      get
      {
        return this.myDBReg;
      }
    }

    public GeneralSetting GeneralSetting
    {
      get
      {
        return this.myGeneralSetting;
      }
    }

    static StockReceiveCommand()
    {
    }

    internal StockReceiveCommand()
    {
      this.myDataTableAllMaster = new DataTable();
      try
      {
        this.myReportOption = (BasicReportOption) PersistenceUtil.LoadUserSetting("StockReceiveReportOption.setting");
      }
      catch
      {
      }
      if (this.myReportOption == null)
        this.myReportOption = new BasicReportOption();
    }

    public static StockReceiveCommand Create(DBSetting dbSetting)
    {
      UserAuthentication.GetOrCreate(dbSetting).CheckHasLogined();
      StockReceiveCommand stockReceiveCommand = (StockReceiveCommand) null;
      if (dbSetting.ServerType == DBServerType.SQL2000)
        stockReceiveCommand = (StockReceiveCommand) new StockReceiveCommandSQL();
      else
        dbSetting.ThrowServerTypeNotSupportedException();
      stockReceiveCommand.myDBSetting = dbSetting;
      stockReceiveCommand.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
      stockReceiveCommand.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
      stockReceiveCommand.myHelper = StockHelper.Create(dbSetting);
      stockReceiveCommand.myDBReg = DBRegistry.Create(dbSetting);
      stockReceiveCommand.myFiscalYear = FiscalYear.GetOrCreate(dbSetting);
      stockReceiveCommand.myGeneralSetting = GeneralSetting.GetOrCreate(dbSetting);
      return stockReceiveCommand;
    }

    protected virtual DataSet LoadData(long docKey)
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadData(string docNo)
    {
      return (DataSet) null;
    }

    protected virtual long LoadFirst()
    {
      return -1L;
    }

    protected virtual long LoadLast()
    {
      return -1L;
    }

    protected virtual long LoadNext(string docNo)
    {
      return -1L;
    }

    protected virtual long LoadPrev(string docNo)
    {
      return -1L;
    }

    protected internal virtual void SaveData(StockReceive stockReceive, bool canceldoc,bool PostTrans)
    {
    }

    protected virtual void DeleteData(long docKey)
    {
    }

    public virtual void DocumentListingBasicSearch(StockReceiveReportingCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
    {
    }

    public virtual void AdvanceSearch(AdvancedStockReceiveCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
    {
    }

    public virtual void BasicSearch(StockReceiveCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
    {
    }

    public virtual void SaveDocumentInfoTable(DataTable table)
    {
    }

    public virtual DataTable GetDocumentInfoTable(long[] docKeys)
    {
      return (DataTable) null;
    }

    private void CreateShadowMasterTable(DataSet ds)
    {
      if (ds.Tables.IndexOf("ShadowMaster") < 0)
      {
        DataTable table = ds.Tables["Master"].Copy();
        table.TableName = "ShadowMaster";
        ds.Tables.Add(table);
      }
    }

    public StockReceive LoadFromTempDocument(long docKey)
    {
      DataSet dataSet = TempDocument.Create(this.myDBSetting).Load("RV", docKey);
      if (dataSet == null)
      {
        return (StockReceive) null;
      }
      else
      {
        this.CreateShadowMasterTable(dataSet);
        StockReceiveAction action = dataSet.Tables["Master"].Rows[0].RowState != DataRowState.Added ? StockReceiveAction.Edit : StockReceiveAction.New;
        return new StockReceive(this, dataSet, action);
      }
    }

    public void SaveToTempDocument(StockReceive document, string saveReason)
    {
      TempDocument.Create(this.myDBSetting).Save(document.DocKey, "RV", (string) document.DocNo, saveReason, (string) document.Description, document.myDataSet);
    }

    public StockReceive AddNew()
    {
      DataSet dataSet = this.LoadData(-1L);
      DataRow row = dataSet.Tables["Master"].NewRow();
      this.InitNewMasterRow(row);
      dataSet.Tables["Master"].Rows.Add(row);
      this.CreateShadowMasterTable(dataSet);
      StockReceive doc = new StockReceive(this, dataSet, StockReceiveAction.New);
      StockReceiveEventArgs stockReceiveEventArgs1 = new StockReceiveEventArgs(doc);
      ScriptObject scriptObject = doc.ScriptObject;
      string name = "OnNewDocument";
      Type[] types = new Type[1];
      int index1 = 0;
      Type type = stockReceiveEventArgs1.GetType();
      types[index1] = type;
      object[] objArray = new object[1];
      int index2 = 0;
      StockReceiveEventArgs stockReceiveEventArgs2 = stockReceiveEventArgs1;
      objArray[index2] = (object) stockReceiveEventArgs2;
      scriptObject.RunMethod(name, types, objArray);
      return doc;
    }

    private void InitNewMasterRow(DataRow row)
    {
      row.BeginEdit();
      if (row.Table.Columns.Contains("Guid"))
        row["Guid"] = (object) Guid.NewGuid();
      row["DocKey"] = (object) this.myDBReg.IncOne((IRegistryID) new GlobalUniqueKey());
      row["PrintCount"] = (object) 0;
      row["Cancelled"] = (object) BCE.Data.Convert.BooleanToText(false);
      row["CanSync"] = (object) BCE.Data.Convert.BooleanToText(true);
      row["DocNo"] = (object) "<<New>>";
            row["DocDate"] = (object) Application.SystemDate;
      row["LastUpdate"] = (object) -1;
      row["LastModifiedUserID"] = (object) "";
      row["LastModified"] = (object) DateTime.MinValue;
      row["CreatedUserID"] = (object) "";
      row["CreatedTimeStamp"] = (object) DateTime.MinValue;
      //string str = this.myDBReg.GetString((IRegistryID) new StockReceiveDescriptionID());
      //int maxLength = row.Table.Columns["Description"].MaxLength;
      //if (str.Length > maxLength)
      //  str = str.Substring(0, maxLength);
      row["Description"] = (object) "STOCK RECEIVE FINISH GOODS";
      //row["ReallocatePurchaseByProject"] = (object) BCE.Data.Convert.BooleanToText(this.myDBReg.GetBoolean((IRegistryID) new ReallocatePurchaseByProject()));
      row.EndEdit();
    }

    public StockReceive View(long docKey)
    {
      return this.InternalView(this.LoadData(docKey));
    }

    public StockReceive View(string docNo)
    {
      return this.InternalView(this.LoadData(docNo));
    }

    public StockReceive ViewFirst()
    {
      long docKey = this.LoadFirst();
      if (docKey < 0L)
        return (StockReceive) null;
      else
        return this.View(docKey);
    }

    public StockReceive ViewLast()
    {
      long docKey = this.LoadLast();
      if (docKey < 0L)
        return (StockReceive) null;
      else
        return this.View(docKey);
    }

    public StockReceive ViewNext(string docNo)
    {
      long docKey = this.LoadNext(docNo);
      if (docKey < 0L)
        return (StockReceive) null;
      else
        return this.View(docKey);
    }

    public StockReceive ViewPrev(string docNo)
    {
      long docKey = this.LoadPrev(docNo);
      if (docKey < 0L)
        return (StockReceive) null;
      else
        return this.View(docKey);
    }

    private StockReceive InternalView(DataSet newDataSet)
    {
      if (newDataSet.Tables["Master"].Rows.Count == 0)
      {
        return (StockReceive) null;
      }
      else
      {
        this.CreateShadowMasterTable(newDataSet);
        return new StockReceive(this, newDataSet, StockReceiveAction.View);
      }
    }

    public StockReceive Edit(long docKey)
    {
      return this.InternalEdit(this.LoadData(docKey));
    }

    public StockReceive Edit(string docNo)
    {
      return this.InternalEdit(this.LoadData(docNo));
    }

    private StockReceive InternalEdit(DataSet newDataSet)
    {
      if (newDataSet.Tables["Master"].Rows.Count == 0)
      {
        return (StockReceive) null;
      }
      else
      {
        this.myFiscalYear.CheckTransactionDate(BCE.Data.Convert.ToDateTime(newDataSet.Tables["Master"].Rows[0]["DocDate"]), "StockReceive", this.myDBSetting);
        this.CreateShadowMasterTable(newDataSet);
        return new StockReceive(this, newDataSet, StockReceiveAction.Edit);
      }
    }

    private long GetDocKeyByDocNo(string docNo)
    {
      DBSetting dbSetting = this.myDBSetting;
      string cmdText = "SELECT DocKey FROM RPA_RCV WHERE DocNo=?";
      object[] objArray = new object[1];
      int index = 0;
      string str = docNo;
      objArray[index] = (object) str;
      object obj = dbSetting.ExecuteScalar(cmdText, objArray);
      if (obj == null)
        return -1L;
      else
        return BCE.Data.Convert.ToInt64(obj);
    }

    public void Delete(long docKey)
    {
      this.DeleteData(docKey);
    }

    public void Delete(string docNo)
    {
      this.DeleteData(this.GetDocKeyByDocNo(docNo));
    }

    public bool CancelDocument(long docKey, string userID)
    {
      StockReceive stockReceive = this.View(docKey);
      if (stockReceive != null)
      {
        stockReceive.CancelDocument(userID);
        return true;
      }
      else
        return false;
    }

    public bool CancelDocument(string docNo, string userID)
    {
      StockReceive stockReceive = this.View(docNo);
      if (stockReceive != null)
      {
        stockReceive.CancelDocument(userID);
        return true;
      }
      else
        return false;
    }

    public bool UncancelDocument(long docKey, string userID)
    {
      StockReceive stockReceive = this.View(docKey);
      if (stockReceive != null)
      {
        stockReceive.UncancelDocument(userID);
        return true;
      }
      else
        return false;
    }

    public bool UncancelDocument(string docNo, string userID)
    {
      StockReceive stockReceive = this.View(docNo);
      if (stockReceive != null)
      {
        stockReceive.UncancelDocument(userID);
        return true;
      }
      else
        return false;
    }

    protected void SaveSerialNo(DataSet ds, DBSetting newDBSetting)
    {
      DataTable dataTable = ds.Tables["Detail"];
      DataTable dtblSerialNo = ds.Tables["SerialNoTrans"];
      if (dataTable.Rows.Count != 0 && dtblSerialNo.Rows.Count != 0 && !BCE.Data.Convert.TextToBoolean(ds.Tables["Master"].Rows[0]["Cancelled"]))
      {
        SerialNumberHelper serialNumberHelper = new SerialNumberHelper(newDBSetting, "RV");
        foreach (DataRow detailRw in (InternalDataCollectionBase) dataTable.Rows)
          serialNumberHelper.SaveSerialNo(ds.Tables["Master"].Rows[0], detailRw, dtblSerialNo);
      }
    }

    protected void CancelUncancelSerialNo(DataSet ds, DBSetting newDBSetting, bool isCancelled)
    {
      DataTable dataTable = ds.Tables["Detail"];
      DataTable dtblSerialNo = ds.Tables["SerialNoTrans"];
      if (dataTable.Rows.Count != 0 && dtblSerialNo.Rows.Count != 0)
      {
        SerialNumberHelper serialNumberHelper = new SerialNumberHelper(newDBSetting, "RV");
        foreach (DataRow detailRow in (InternalDataCollectionBase) dataTable.Rows)
          serialNumberHelper.CancellationSN(detailRow, dtblSerialNo, isCancelled);
      }
    }

    protected void DeleteSerialNo(DataSet ds, DBSetting newDBSetting)
    {
      DataTable dataTable = ds.Tables["Detail"];
      DataTable dtblSerialNo = ds.Tables["SerialNoTrans"];
      if (dataTable.Rows.Count != 0 && dtblSerialNo.Rows.Count != 0)
      {
        DataRow dataRow = ds.Tables["Master"].Rows[0];
        string index1 = "Cancelled";
        bool isCancelled = BCE.Data.Convert.TextToBoolean(dataRow[index1]);
        SerialNumberHelper serialNumberHelper = new SerialNumberHelper(newDBSetting, "RV");
        string index2 = "DocKey";
        long docKey = BCE.Data.Convert.ToInt64(dataRow[index2]);
        foreach (DataRow detailRow in (InternalDataCollectionBase) dataTable.Rows)
          serialNumberHelper.DeleteSN(docKey, detailRow, dtblSerialNo, isCancelled, true);
      }
    }

    public virtual int InquireAllMaster(string columnSQL, bool hasYearMonth)
    {
      return 0;
    }

    public virtual int SearchMaster(SearchCriteria criteria, string columnSQL, DataTable resultTable, string MultiSelectColumnName)
    {
      return 0;
    }

        protected void PostToStockCosting(DataSet ds, DBSetting newDBSetting)
        {
            DataRow dataRow1 = ds.Tables["Master"].Rows[0];
            DataTable dataTable = ds.Tables["Detail"];
            DateTime docDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            long docKey = BCE.Data.Convert.ToInt64(dataRow1["DocKey"]);
            if (docDate >= this.myFiscalYear.ActualDataStartDate && !BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
            {
                bool enable = ModuleControl.GetOrCreate(newDBSetting).ModuleController.AdvancedMultiUOM.Enable;
                StockTrans trans = new StockTrans(newDBSetting, "SR", docKey);
                foreach (DataRow dataRow2 in dataTable.Select("ItemCode IS NOT NULL", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent))
                {
                    Decimal cost = BCE.Data.Convert.ToDecimal(dataRow2["UnitCost"]);
                    Decimal qty = BCE.Data.Convert.ToDecimal(dataRow2["Qty"]);
                    string itemCode = dataRow2["ItemCode"].ToString();
                    string str = dataRow2["UOM"].ToString();
                    if (!enable)
                    {
                        StockHelper stockHelper = StockHelper.Create(newDBSetting);
                        Decimal itemUomRate = stockHelper.GetItemUOMRate(itemCode, str);
                        if (itemUomRate != Decimal.Zero)
                        {
                            str = stockHelper.GetBaseUOM(dataRow2["ItemCode"].ToString());
                            qty = this.myDecimalSetting.RoundQuantity(qty * itemUomRate);
                            cost = this.myDecimalSetting.RoundCost(cost / itemUomRate);
                        }
                    }
                    trans.Add(BCE.Data.Convert.ToInt64(dataRow2["DtlKey"]), itemCode, str, dataRow2["Location"].ToString(), dataRow2["BatchNo"], dataRow2["ProjNo"], dataRow2["DeptNo"], docDate, qty, cost, 0L);
                }
                StockCosting.Create(newDBSetting).Add(trans);
            }
            else
                this.DeleteFromStockCosting(docKey, newDBSetting);
        }

        protected void DeleteFromStockCosting(long docKey, DBSetting newDBSetting)
    {
      StockCosting.Create(newDBSetting).Remove("RV", docKey);
    }

    protected void PostAuditLog(DataSet ds, DBSetting dbSetting, AuditTrail.EventType eventType)
    {
      DataRow dataRow = ds.Tables["Master"].Rows[0];
      AuditColumnMap auditColumnMap1 = new AuditColumnMap(Localizer.GetString((Enum) StockReceiveColumnStringId.StockReceive, new object[0]), ds.Tables["Master"]);
      auditColumnMap1.AddColumn("DocNo", Localizer.GetString((Enum) StockReceiveColumnStringId.DocumentNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap1.AddColumn("DocDate", Localizer.GetString((Enum) StockReceiveColumnStringId.Date, new object[0]), DocumentAuditTrail.ColumnType.Date);
      auditColumnMap1.AddColumn("Description", Localizer.GetString((Enum) StockReceiveColumnStringId.Description, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap1.AddColumn("RefDocNo", Localizer.GetString((Enum) StockReceiveColumnStringId.RefDocNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      RemarkNameEntity remarkName = RemarkName.Create(dbSetting).GetRemarkName("RV");
      if (remarkName == null)
      {
        auditColumnMap1.AddColumn("Remark1", Localizer.GetString((Enum) StockReceiveColumnStringId.Remark1, new object[0]), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark2", Localizer.GetString((Enum) StockReceiveColumnStringId.Remark2, new object[0]), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark3", Localizer.GetString((Enum) StockReceiveColumnStringId.Remark3, new object[0]), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark4", Localizer.GetString((Enum) StockReceiveColumnStringId.Remark4, new object[0]), DocumentAuditTrail.ColumnType.String);
      }
      else
      {
        auditColumnMap1.AddColumn("Remark1", remarkName.GetRemarkName(1), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark2", remarkName.GetRemarkName(2), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark3", remarkName.GetRemarkName(3), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark4", remarkName.GetRemarkName(4), DocumentAuditTrail.ColumnType.String);
      }
      AuditColumnMap auditColumnMap2 = new AuditColumnMap(Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Detail, new object[0]), ds.Tables["Detail"]);
      auditColumnMap2.AddColumn("ItemCode", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.ItemCode, new object[0]), DocumentAuditTrail.ColumnType.String, true);
      auditColumnMap2.AddColumn("UOM", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.UOM, new object[0]), DocumentAuditTrail.ColumnType.String, true);
      auditColumnMap2.AddColumn("Description", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Description, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("Location", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Location, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("BatchNo", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.BatchNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("ProjNo", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.ProjectNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("DeptNo", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.DepartmentNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("Qty", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Quantity, new object[0]), DocumentAuditTrail.ColumnType.Quantity);
      auditColumnMap2.AddColumn("UnitCost", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.UnitCost, new object[0]), DocumentAuditTrail.ColumnType.Cost);
      auditColumnMap2.AddColumn("SerialNoList", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.SerialNoList, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("Numbering", Localizer.GetString((Enum) StockReceiveDetailColumnStringId.Numbering, new object[0]), DocumentAuditTrail.ColumnType.String);
      DocumentAuditTrail documentAuditTrail = new DocumentAuditTrail(dbSetting, "RV");
      AuditColumnMap masterColMap1 = auditColumnMap1;
      AuditColumnMap detailColMap1 = auditColumnMap2;
      DBSetting newDBSetting = dbSetting;
      int num1 = (int) eventType;
      documentAuditTrail.PostAuditLog(masterColMap1, detailColMap1, newDBSetting, (AuditTrail.EventType) num1);
      string detail = "";
      AuditColumnMap masterColMap2 = auditColumnMap1;
      AuditColumnMap detailColMap2 = auditColumnMap2;
      int num2 = (int) eventType;
      // ISSUE: explicit reference operation
      // ISSUE: variable of a reference type
      string message = @detail;
      documentAuditTrail.BuildAuditLog(masterColMap2, detailColMap2, (AuditTrail.EventType) num2,ref message);
      long docKey = dataRow.RowState != DataRowState.Deleted ? BCE.Data.Convert.ToInt64(dataRow["DocKey"]) : BCE.Data.Convert.ToInt64(dataRow["DocKey", DataRowVersion.Original]);
      if (eventType == AuditTrail.EventType.New)
      {
        // ISSUE: variable of a boxed type
        StockReceiveString local =  StockReceiveString.CreateNewStockReceive;
        object[] objArray = new object[2];
        int index1 = 0;
        string loginUserId = this.myUserAuthentication.LoginUserID;
        objArray[index1] = (object) loginUserId;
        int index2 = 1;
        string str = dataRow["DocNo"].ToString();
        objArray[index2] = (object) str;
        string @string = Localizer.GetString((Enum) local, objArray);
        Activity.Log(this.myDBSetting, "RV", docKey, 0L, @string, detail);
      }
      else if (eventType == AuditTrail.EventType.Edit)
      {
        if (detail.Length > 0)
        {
          // ISSUE: variable of a boxed type
          StockReceiveString local =  StockReceiveString.UpdatedStockReceive;
          object[] objArray = new object[2];
          int index1 = 0;
          string loginUserId = this.myUserAuthentication.LoginUserID;
          objArray[index1] = (object) loginUserId;
          int index2 = 1;
          string str = dataRow["DocNo", DataRowVersion.Original].ToString();
          objArray[index2] = (object) str;
          string @string = Localizer.GetString((Enum) local, objArray);
          Activity.Log(this.myDBSetting, "RV", docKey, 0L, @string, detail);
        }
      }
      else if (eventType == AuditTrail.EventType.Delete)
      {
        // ISSUE: variable of a boxed type
        StockReceiveString local = StockReceiveString.DeletedStockReceive;
        object[] objArray = new object[2];
        int index1 = 0;
        string loginUserId = this.myUserAuthentication.LoginUserID;
        objArray[index1] = (object) loginUserId;
        int index2 = 1;
        string str = dataRow["DocNo", DataRowVersion.Original].ToString();
        objArray[index2] = (object) str;
        string @string = Localizer.GetString((Enum) local, objArray);
        Activity.Log(this.myDBSetting, "RV", docKey, 0L, @string, detail);
      }
    }

    protected void UpdateStockUOMConv(StockReceive doc)
    {
      DBSetting dbSetting = this.myDBSetting;
      string cmdText = "SELECT FromDocNo FROM UOMConv WHERE FromDocKey = ?";
      object[] objArray = new object[1];
      int index = 0;
      // ISSUE: variable of a boxed type
      long local =  doc.DocKey;
      objArray[index] = (object) local;
      object obj = dbSetting.ExecuteScalar(cmdText, objArray);
      if (obj != null && obj.ToString() != (string) doc.DocNo)
        this.myDBSetting.ExecuteNonQuery(string.Format("UPDATE UOMConv SET FromDocNo = '{0}' WHERE FromDocKey = {1}", (object) doc.DocNo, (object) doc.DocKey), new object[0]);
    }

  

    public void SaveReportOption()
    {
      PersistenceUtil.SaveUserSetting((object) this.myReportOption, "StockReceiveReportOption.setting");
    }

    protected virtual DataSet LoadDesignReportData()
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadReportData(long docKey)
    {
      return (DataSet) null;
    }

    private DocumentReportDataSet PreparingReportDataSet(DataSet dsReportData)
    {
      DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, "Stock Receive FG", "Stock Receive FG Master", "Stock Receive FG Detail");
      documentReportDataSet.Tables.Add(dsReportData.Tables[0].Copy());
      documentReportDataSet.Tables.Add(dsReportData.Tables[1].Copy());
      DataRelation relation = new DataRelation("MasterDetailRelation", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["Detail"].Columns["DocKey"], false);
      documentReportDataSet.Relations.Add(relation);
      documentReportDataSet.AddDefaultTables();
      return documentReportDataSet;
    }

    public object GetReportDesignerDataSource()
    {
      return (object) this.PreparingReportDataSet(this.LoadDesignReportData());
    }

    public object GetReportDataSource(long docKey)
    {
      return (object) this.PreparingReportDataSet(this.LoadReportData(docKey));
    }

    public object GetReportDataSource(string dockeys)
    {
      return (object) this.PreparingReportDataSet(this.LoadDocumentListingReportData(dockeys));
    }

    public void PrintReport(SearchCriteria criteria)
    {
    }

    private DocumentReportDataSet PreparingDocumentListingReportDataSet(DataSet dsReportData, string docKeys, StockReceiveReportingCriteria criteria, string dataName)
    {
      DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, dataName, "Stock Receive FG Master", "Stock Receive FG Detail");
      documentReportDataSet.Tables.Add(dsReportData.Tables[0].Copy());
      documentReportDataSet.Tables.Add(dsReportData.Tables[1].Copy());
      documentReportDataSet.Relations.Add("MasterDetailRelation", documentReportDataSet.Tables[0].Columns["DocKey"], documentReportDataSet.Tables[1].Columns["DocKey"], false);
      StockReceiveCommand.AddFinalizeTables(this.myDBSetting, criteria, (DataSet) documentReportDataSet, docKeys, "RPA_RCV");
      documentReportDataSet.AddDefaultTables();
      return documentReportDataSet;
    }

    public object GetDocumentListingReportDataSource(string docKeys, StockReceiveReportingCriteria criteria)
    {
      return (object) this.PreparingDocumentListingReportDataSet(this.LoadDocumentListingReportData(docKeys), docKeys, criteria, "RPA Stock Receive FG Listing");
    }

    public object GetDocumentListingReportDesignerDataSource(StockReceiveReportingCriteria criteria)
    {
      DataSet dsReportData = this.LoadDocumentListingReportDesignerData();
      string docKeys = "";
      foreach (DataRow dataRow in (InternalDataCollectionBase) dsReportData.Tables["Master"].Rows)
        docKeys = docKeys + dataRow["Dockey"] + ", ";
      return (object) this.PreparingDocumentListingReportDataSet(dsReportData, docKeys, criteria, "RPA Stock Receive FG Listing");
    }

    protected virtual DataSet LoadDocumentListingReportData(string docKeys)
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadDocumentListingReportDesignerData()
    {
      return (DataSet) null;
    }

    public static EmailAndFaxInfo GetEmailAndFaxInfo(long docKey, DBSetting dbSetting)
    {
      string str1 = "Select A.DocNo, A.DocDate From RPA_RCV A WHERE A.DocKey = ?";
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      object[] objArray = new object[1];
      int index = 0;
      // ISSUE: variable of a boxed type
     long local =  docKey;
      objArray[index] = (object) local;
      DataRow firstDataRow = dbSetting1.GetFirstDataRow(cmdText, objArray);
      if (firstDataRow == null)
      {
        return new EmailAndFaxInfo();
      }
      else
      {
        EmailAndFaxInfo emailAndFaxInfo = new EmailAndFaxInfo();
        string str2 = string.Format("{0} {1}, dated {2}", (object) DocumentType.ToLocalizedString("RV"), (object) firstDataRow["DocNo"].ToString(), (object) GeneralSetting.GetOrCreate(dbSetting).FormatDate(BCE.Data.Convert.ToDateTime(firstDataRow["DocDate"])));
        emailAndFaxInfo.Subject = str2;
        string str3 = StringHelper.ConvertToValidFilename(string.Format("{0} {1}", (object) DocumentType.ToLocalizedString("RV"), (object) firstDataRow["DocNo"].ToString()));
        emailAndFaxInfo.AttachmentPdfFilename = str3;
        return emailAndFaxInfo;
      }
    }

    public static void AddFinalizeTables(DBSetting dbSetting, StockReceiveReportingCriteria criteria, DataSet dsReport, string docKeys, string masterTableName)
    {
      StockReceiveCommand.CreateReportOptionTable(criteria, dsReport);
      StockReceiveCommand.CreateGroupIDAndSortIDColumn(criteria, dsReport);
      StockReceiveCommand.AddSummaryTable(dbSetting, dsReport, docKeys, masterTableName);
    }

    public static void AddSummaryTable(DBSetting dbSetting, DataSet ds, string docKeys, string masterTableName)
    {
      ds.Tables.Add(StockReceiveCommand.GetItemAnalysisSummaryTable(dbSetting, docKeys, masterTableName + "DTL"));
      ds.Tables.Add(StockReceiveCommand.GetDateAnalysisSummaryTable(dbSetting, docKeys, masterTableName, masterTableName + "DTL"));
      ds.Tables.Add(StockReceiveCommand.GetMonthAnalysisSummaryTable(dbSetting, docKeys, masterTableName, masterTableName + "DTL"));
      ds.Tables.Add(StockReceiveCommand.GetYearAnalysisSummaryTable(dbSetting, docKeys, masterTableName, masterTableName + "DTL"));
    }

    public static void CreateReportOptionTable(StockReceiveReportingCriteria criteria, DataSet dsReport)
    {
      DataTable table = new DataTable("Report Option");
      table.Columns.Add("Criteria", typeof (string));
      table.Columns.Add("ShowCriteria", typeof (string));
      table.Columns.Add("GroupBy", typeof (string));
      table.Columns.Add("SortBy", typeof (string));
      table.Columns.Add("FromDate", typeof (DateTime));
      table.Columns.Add("ToDate", typeof (DateTime));
      table.Columns.Add("DocumentFilterTypeString", typeof (string));
      table.Columns.Add("CancelledStatus", typeof (string));
      if (criteria == null)
        criteria = new StockReceiveReportingCriteria();
      DataRow row = table.NewRow();
      row["Criteria"] = (object) criteria.ReadableText;
      row["ShowCriteria"] = criteria.IsShowCriteria ? (object) "Yes" : (object) "No";
      row["GroupBy"] = (object) criteria.GroupBy;
      row["SortBy"] = (object) criteria.SortBy;
      if (criteria.DateFilter.Type == FilterType.ByRange)
      {
        row["FromDate"] = criteria.DateFilter.From == null || criteria.DateFilter.From.ToString().Length == 0 ? (object) DateTime.MinValue : (object) (DateTime) criteria.DateFilter.From;
        row["ToDate"] = criteria.DateFilter.To == null || criteria.DateFilter.To.ToString().Length == 0 ? (object) DateTime.MaxValue : (object) (DateTime) criteria.DateFilter.To;
      }
      else
      {
        row["FromDate"] = (object) DateTime.MinValue;
        row["ToDate"] = (object) DateTime.MaxValue;
      }
      row["DocumentFilterTypeString"] = (object) criteria.DocumentFilter.ToFilterTypeString();
      row["CancelledStatus"] = criteria.IsPrintCancelled != CancelledDocumentOption.All ? (criteria.IsPrintCancelled != CancelledDocumentOption.Cancelled ? (object) "Show Uncancelled" : (object) "Show Cancelled") : (object) "Show All";
      table.Rows.Add(row);
      dsReport.Tables.Add(table);
    }

    public static void CreateGroupIDAndSortIDColumn(StockReceiveReportingCriteria criteria, DataSet dsReport)
    {
      dsReport.Tables[0].Columns.Add("GroupID", typeof (string));
      dsReport.Tables[0].Columns.Add("GroupIDDisplay", typeof (string));
      dsReport.Tables[0].Columns.Add("GroupIDName", typeof (string));
      dsReport.Tables[0].Columns.Add("GroupIDDescription", typeof (string));
      dsReport.Tables[0].Columns.Add("SortID", typeof (string));
      foreach (DataRow dataRow1 in (InternalDataCollectionBase) dsReport.Tables[0].Rows)
      {
        if (criteria != null)
        {
          if (criteria.SortBy == ListingSortByOption.Date)
          {
            string str = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]).ToString("yyyyMMdd");
            dataRow1["SortID"] = (object) str;
          }
          else
            dataRow1["SortID"] = (object) dataRow1["DocNo"].ToString();
          int year;
          if (criteria.GroupBy == ListingGroupByOption.Date)
          {
            DateTime dateTime = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            dataRow1["GroupID"] = (object) BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]).ToString("yyyy/MM/dd");
            dataRow1["GroupIDDisplay"] = (object) BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]).ToString("dd/MM/yyyy");
            dataRow1["GroupIDName"] = (object) (Localizer.GetString((Enum) ListingGroupByOption.Date, new object[0]) + ":");
            dataRow1["GroupIDDescription"] = (object) (Localizer.GetString((Enum) StockStringId.GroupBy, new object[0]) + " " + dateTime.ToString("dd/MM/yyyy"));
            dataRow1.EndEdit();
          }
          else if (criteria.GroupBy == ListingGroupByOption.Month)
          {
            DateTime dateTime = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            string str1 = dateTime.ToString("yyyyMM");
            dataRow1["GroupID"] = (object) str1;
            DataRow dataRow2 = dataRow1;
            string index1 = "GroupIDDisplay";
            string str2 = Utils.NumberToMonth(dateTime.Month);
            string str3 = " ";
            year = dateTime.Year;
            string str4 = year.ToString();
            string str5 = str2 + str3 + str4;
            dataRow2[index1] = (object) str5;
            dataRow1["GroupIDName"] = (object) (Localizer.GetString((Enum) ListingGroupByOption.Month, new object[0]) + ":");
            DataRow dataRow3 = dataRow1;
            string index2 = "GroupIDDescription";
            string[] strArray = new string[5];
            int index3 = 0;
            string @string = Localizer.GetString((Enum) StockStringId.GroupBy, new object[0]);
            strArray[index3] = @string;
            int index4 = 1;
            string str6 = " ";
            strArray[index4] = str6;
            int index5 = 2;
            string str7 = Utils.NumberToMonth(dateTime.Month);
            strArray[index5] = str7;
            int index6 = 3;
            string str8 = " ";
            strArray[index6] = str8;
            int index7 = 4;
            year = dateTime.Year;
            string str9 = year.ToString();
            strArray[index7] = str9;
            string str10 = string.Concat(strArray);
            dataRow3[index2] = (object) str10;
            dataRow1.EndEdit();
          }
          else if (criteria.GroupBy == ListingGroupByOption.Year)
          {
            DateTime dateTime = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            DataRow dataRow2 = dataRow1;
            string index1 = "GroupID";
            year = dateTime.Year;
            string str1 = year.ToString();
            dataRow2[index1] = (object) str1;
            dataRow1["GroupIDDisplay"] = dataRow1["GroupID"];
            dataRow1["GroupIDName"] = (object) (Localizer.GetString((Enum) ListingGroupByOption.Year, new object[0]) + ":");
            DataRow dataRow3 = dataRow1;
            string index2 = "GroupIDDescription";
            string @string = Localizer.GetString((Enum) StockStringId.GroupBy, new object[0]);
            string str2 = " ";
            year = dateTime.Year;
            string str3 = year.ToString();
            string str4 = @string + str2 + str3;
            dataRow3[index2] = (object) str4;
            dataRow1.EndEdit();
          }
        }
      }
    }


        protected void PostToStockCostingWIP(DataSet ds, DBSetting newDBSetting)
        {
            DataRow dataRow1 = ds.Tables["Master"].Rows[0];
            DataTable dataTable = ds.Tables["Detail"];
            DataTable subdataTable = new DataTable(); ;// null;// ds.Tables["SubDetail"];
            DateTime docDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            long docKey = BCE.Data.Convert.ToInt64(dataRow1["DocKey"]);
            Int32 iKey = 0;
            this.DeleteFromStockCostingWIP(docKey, newDBSetting);
            if (docDate >= this.myFiscalYear.ActualDataStartDate && !BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
            {
                bool enable = ModuleControl.GetOrCreate(newDBSetting).ModuleController.AdvancedMultiUOM.Enable;
                Production.StockCard.StockTrans trans = new Production.StockCard.StockTrans(newDBSetting, "RV", docKey);
                foreach (DataRow dataRow2 in dataTable.Select("ItemCode IS NOT NULL", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent))
                {
                    if(dataRow2["FromDocType"]!=null && dataRow2["FromDocType"] != DBNull.Value)
                    { 
                        if(dataRow2["FromDocType"].ToString()== "Main Product")
                        {
                            //subdataTable = myDBSetting.GetDataTable("SELECT d.*,(d.Qty-COALESCE(d.TransferedQty,0)) as QtyRM FROM RPA_WO a with(nolock) inner join RPA_WOProduct  b with(nolock) on a.DocKey=b.DocKey inner join RPA_WODtl  c with(nolock) on a.DocKey=c.DocKey and c.FromDocDtlKey=b.DtlKey inner join (select rm.* from RPA_RMDTL rm with(nolock) inner join RPA_RM rmd with(nolock) on rm.DocKey=rmd.DocKey where Cancelled='F') d   on  d.FromDocDtlKey=c.DtlKey and d.FromDocNo=a.DocNo where a.DocNo=? and b.DtlKey=? and ISNULL(c.Level,0)=0", false,(object)dataRow2["FromDocNo"], (object)dataRow2["FromDocDtlKey"]);

                            subdataTable = myDBSetting.GetDataTable("SELECT d.*,case when c.ProductRatio>1 then (?*b.Rate)/ProductRatio else ((?*b.Rate)/h.Qty)*c.Rate end as QtyRM FROM RPA_WO a with(nolock) inner join RPA_WOProduct b with(nolock) on a.DocKey=b.DocKey inner join RPA_WODtl  c with(nolock) on a.DocKey=c.DocKey and c.FromDocDtlKey=b.DtlKey inner join RPA_ItemBOM  h with(nolock) on h.BOMCode=c.BOMCode inner join (select rm.* from RPA_RMDTL rm with(nolock) inner join RPA_RM rmd with(nolock) on rm.DocKey=rmd.DocKey where Cancelled='F') d   on  d.FromDocDtlKey=c.DtlKey and d.FromDocNo=a.DocNo where a.DocNo=? and b.DtlKey=? and ISNULL(c.Level,0)=0 and b.ItemCode=?", false, (object)BCE.Data.Convert.ToDecimal(dataRow2["Qty"]), (object)BCE.Data.Convert.ToDecimal(dataRow2["Qty"]), (object)dataRow2["FromDocNo"], (object)dataRow2["FromDocDtlKey"], (object)dataRow2["ItemCode"]);

                            //DataRow[] drSubDetailArray = subdataTable.Select("DtlKey=" + dataRow2["DtlKey"] + "", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
                            if (subdataTable.Rows.Count > 0)
                            {
                                foreach (DataRow drSubDetail in subdataTable.Rows)
                                {
                                    Decimal num = BCE.Data.Convert.ToDecimal(drSubDetail["QtyRM"]);
                                    Decimal dunitcost = BCE.Data.Convert.ToDecimal(drSubDetail["UnitCost"]);
                                    string itemCode = drSubDetail["ItemCode"].ToString();
                                    string strUOM = drSubDetail["UOM"].ToString();
                                    string strWONo = "";
                                    string strBOMCode = "";
                                    string strProductCode = "";
                                    if (dataRow2["FromDocNo"] != null && dataRow2["FromDocNo"] != DBNull.Value)
                                        strWONo = dataRow2["FromDocNo"].ToString();
                                    if (drSubDetail["BOMCode"] != null && drSubDetail["BOMCode"] != DBNull.Value)
                                        strBOMCode = drSubDetail["BOMCode"].ToString();
                                    //object obj = myDBSetting.ExecuteScalar("select ItemCode from RPA_ItemBOM where BOMCode=?", (object)strBOMCode);
                                    //if (obj != null && obj != DBNull.Value)
                                    //{
                                    //    strProductCode = obj.ToString();
                                    //}
                                    strProductCode = dataRow2["ItemCode"].ToString();
                                    if (!enable)
                                    {

                                        StockHelper stockHelper = StockHelper.Create(myDBSetting);
                                        try
                                        {
                                            Decimal itemUomRate = stockHelper.GetItemUOMRate(itemCode, strUOM);
                                            if (itemUomRate != Decimal.Zero)
                                            {
                                                dunitcost = 0;
                                                strUOM = stockHelper.GetBaseUOM(itemCode);
                                                num = this.myDecimalSetting.RoundQuantity(num * itemUomRate);

                                                StockDocumentItem stockDocumentItem = myHelper.LoadStockDocumentItem(itemCode);
                                                dunitcost = BCE.Data.Convert.ToDecimal(stockDocumentItem.Cost);

                                                if (dunitcost == 0)
                                                {
                                                   object obj = myDBSetting.ExecuteScalar("select RealCost from itemuom where ItemCode=? and UOM=?", (object)itemCode, (object)strUOM);
                                                    if (obj != null && obj != DBNull.Value)
                                                        dunitcost = BCE.Data.Convert.ToDecimal(obj);
                                                }
                                            }
                                        }
                                        catch { }
                                    }
                                    iKey++;
                                    object oBatchNo = null;
                                    if(dataRow2["BatchNo"]!=null)
                                         oBatchNo = dataRow2["BatchNo"].ToString() == "" ? null : dataRow2["BatchNo"];

                                    trans.Add(BCE.Data.Convert.ToInt64(iKey), dataRow1["DocNo"].ToString(), strWONo, strProductCode, strBOMCode, itemCode, strUOM, dataRow2["Location"].ToString(),null, oBatchNo, dataRow2["ProjNo"], dataRow2["DeptNo"], docDate, -num, dunitcost, 0L);
                                }
                            }
                        }
                    }
                  

                }
                Production.StockCard.StockCosting.Create(newDBSetting).Add(trans);
            }
            else
                this.DeleteFromStockCostingWIP(docKey, newDBSetting);
        }

        protected void DeleteFromStockCostingWIP(long docKey, DBSetting newDBSetting)
        {
            Production.StockCard.StockCosting.Create(newDBSetting).Remove("RV", docKey);
        }
        protected bool PostToSR(StockReceive stockissue, DataSet ds, DBSetting newDBSetting, StockReceiveAction myAction, bool canceldoc)
        {
            DataRow dataRow1 = ds.Tables["Master"].Rows[0];
            DataTable detailTable = ds.Tables["Detail"];
            // DataTable crewTable = ds.Tables["Crew"];
            // DataTable crewcatchTable = ds.Tables["CrewCatch"];
            //Int64 lJEKey = 0;
            //if (dataRow1["JEKey"] != null && dataRow1["JEKey"] != DBNull.Value)
            //{
            //    lJEKey = BCE.Data.Convert.ToInt64(dataRow1["JEKey"]);
            //}
            bool bexists = false;
            object obj = myDBSetting.ExecuteScalar("select count(*) from RCV WITH(NOLOCK) where DocNo=?", (object)stockissue.DocNo.ToString());
            if (obj != null && obj != DBNull.Value)
            {
                if (BCE.Data.Convert.ToDecimal(obj) > 0)
                    bexists = true;
            }
            DBSetting TransdbSetting = newDBSetting.StartTransaction();
            try
            {
                BCE.AutoCount.Stock.StockReceive.StockReceiveCommand srCommand = BCE.AutoCount.Stock.StockReceive.StockReceiveCommand.Create(TransdbSetting);


                if (myAction == StockReceiveAction.New || myAction == StockReceiveAction.Edit)
                {

                    BCE.AutoCount.Stock.StockReceive.StockReceive srentity;

                    if (bexists)
                    {
                        srentity = srCommand.Edit(stockissue.DocNo);
                        //srentity.DocNo = dataRow1["DocNo"].ToString();
                        srentity.DocDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
                        srentity.DataTableMaster.Rows[0]["Cancelled"] = dataRow1["Cancelled"];
                        srentity.DataTableMaster.Rows[0]["RefDocNo"] = dataRow1["RefDocNo"];
                        srentity.DataTableMaster.Rows[0]["Description"] = dataRow1["Description"];

                        srentity.UDF["JNSTRANS"] = "FG";
                        // jeentity.CurrencyCode = "IDR";
                        // jeentity.CurrencyRate = (decimal)1;
                        srentity.ClearDetails();
                        for (int i = 0; i < stockissue.GetValidDetailRows().Length; i++)
                        {
                            DataRow drDetail = stockissue.GetValidDetailRows()[i];
                           
                            BCE.AutoCount.Stock.StockReceive.StockReceiveDetail dtlrecord = srentity.AddDetail();

                            dtlrecord.Row["ItemCode"] = drDetail["ItemCode"];
                            dtlrecord.Row["Description"] = drDetail["Description"];
                            dtlrecord.Row["BatchNo"] = drDetail["BatchNo"];
                            dtlrecord.Row["FurtherDescription"] = drDetail["FurtherDescription"];
                            dtlrecord.Row["ProjNo"] = drDetail["ProjNo"];
                            dtlrecord.Row["Location"] = drDetail["Location"];
                            // dTotalDR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                            dtlrecord.Row["UOM"] = drDetail["UOM"];
                            dtlrecord.Row["Qty"] = drDetail["Qty"];
                            dtlrecord.Row["UnitCost"] = drDetail["UnitCost"];
                            //dtlrecord.Row["SubTotal"] = drDetail["SubTotal"];
                           
                            dtlrecord.UDF["Debet"] = drDetail["Debit"];
                            dtlrecord.UDF["Kredit"] = drDetail["Credit"];
                            dtlrecord.EndEdit();
                            // TransdbSetting.ExecuteNonQuery("insert into BG_TransOutstandingBP(TransKey,TransNo,TransDate,TransType,ProjNo,BucketTotal,PointTotal,Amount,TotalAmount,TransferAmt,Percentage,ReceiveCode,ReceiveName) values(?,?,?,?,?,?,?,?,?,?,?,?,?)", (object)dataRow1["Dockey"], (object)dataRow1["DocNo"], (object)dataRow1["DocDate"], (object)drgroup["TransType"], (object)drgroup["ProjNo"], (object)drgroup["BucketTotal"], (object)drgroup["PointTotal"], (object)drgroup["Amount"], (object)drgroup["TotalAmount"], (object)0, (object)drgroup["Percentage"], (object)drgroup["ReceiveCode"], (object)drgroup["ReceiveName"]);

                        }
                        srentity.Save(this.myUserAuthentication.LoginUserID);
                        //if(stockissue.RefDocNo!=null && stockissue.RefDocNo!=DBNull.Value)
                        
                    }
                    else
                    {
                        srentity = srCommand.AddNew();
                        srentity.DocNo = dataRow1["DocNo"].ToString();
                        srentity.DocDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
                        srentity.DataTableMaster.Rows[0]["Cancelled"] = dataRow1["Cancelled"];
                        srentity.DataTableMaster.Rows[0]["RefDocNo"] = dataRow1["RefDocNo"];
                        srentity.DataTableMaster.Rows[0]["Description"] = dataRow1["Description"];
                        srentity.UDF["JNSTRANS"] = "FG";

                        // jeentity.CurrencyCode = "IDR";
                        // jeentity.CurrencyRate = (decimal)1;
                        srentity.ClearDetails();
                        for (int i = 0; i < stockissue.GetValidDetailRows().Length; i++)
                        {
                            DataRow drDetail = stockissue.GetValidDetailRows()[i];
                           

                            BCE.AutoCount.Stock.StockReceive.StockReceiveDetail dtlrecord = srentity.AddDetail();
                            dtlrecord.Row["ItemCode"] = drDetail["ItemCode"];
                            dtlrecord.Row["Description"] = drDetail["Description"];
                            dtlrecord.Row["BatchNo"] = drDetail["BatchNo"];
                            dtlrecord.Row["Location"] = drDetail["Location"];

                            // dtlrecord.SE
                            dtlrecord.Row["FurtherDescription"] = drDetail["FurtherDescription"];
                            dtlrecord.Row["ProjNo"] = drDetail["ProjNo"];
                            // dTotalDR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                            dtlrecord.Row["UOM"] = drDetail["UOM"];
                            dtlrecord.Row["Qty"] = drDetail["Qty"];
                            dtlrecord.Row["UnitCost"] = drDetail["UnitCost"];
                            //dtlrecord.Row["SubTotal"] = drDetail["SubTotal"];
                            dtlrecord.UDF["Debet"] = drDetail["Debit"];
                            dtlrecord.UDF["Kredit"] = drDetail["Credit"];
                            dtlrecord.EndEdit();
                            // TransdbSetting.ExecuteNonQuery("insert into BG_TransOutstandingBP(TransKey,TransNo,TransDate,TransType,ProjNo,BucketTotal,PointTotal,Amount,TotalAmount,TransferAmt,Percentage,ReceiveCode,ReceiveName) values(?,?,?,?,?,?,?,?,?,?,?,?,?)", (object)dataRow1["Dockey"], (object)dataRow1["DocNo"], (object)dataRow1["DocDate"], (object)drgroup["TransType"], (object)drgroup["ProjNo"], (object)drgroup["BucketTotal"], (object)drgroup["PointTotal"], (object)drgroup["Amount"], (object)drgroup["TotalAmount"], (object)0, (object)drgroup["Percentage"], (object)drgroup["ReceiveCode"], (object)drgroup["ReceiveName"]);

                        }
                        srentity.Save(this.myUserAuthentication.LoginUserID);
                    }
                }
                else
                {
                    if (BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
                        srCommand.CancelDocument(stockissue.DocKey, this.myUserAuthentication.LoginUserID);
                    else if (!BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
                        srCommand.UncancelDocument(stockissue.DocKey, this.myUserAuthentication.LoginUserID);

                }
                TransdbSetting.Commit();

            }
            catch (SqlException ex)
            {
                TransdbSetting.Rollback();
                BCE.Data.DataError.HandleSqlException(ex);
                return false;
            }
            finally
            {
                TransdbSetting.EndTransaction();
                // AccountBookControl.EnableTransactionCounter();
            }
            return true;
        }

        public bool PostToJE(string projnoBOM, DataTable overheadtable,StockReceive stockissue, DataSet ds, DBSetting newDBSetting, StockReceiveAction myAction,bool canceldoc)
        {
            DataRow dataRow1 = ds.Tables["Master"].Rows[0];
            DataTable detailTable = ds.Tables["Detail"];
            // DataTable crewTable = ds.Tables["Crew"];
            // DataTable crewcatchTable = ds.Tables["CrewCatch"];
            Int64 lJEKey = 0;
            if (dataRow1["JEKey"] != null && dataRow1["JEKey"] != DBNull.Value)
            {
                lJEKey = BCE.Data.Convert.ToInt64(dataRow1["JEKey"]);
            }

            //decimal dGrandTotalBOM = 0;
            //decimal dTotalAllQty

            DBSetting TransdbSetting = newDBSetting.StartTransaction();
            try
            {    
                
                    BCE.AutoCount.GL.JournalEntry.JournalEntryCommand jeCmd = BCE.AutoCount.GL.JournalEntry.JournalEntryCommand.Create(TransdbSetting);
                if (myAction == StockReceiveAction.New || myAction == StockReceiveAction.Edit)
                {
                    if (lJEKey > 0)
                    {
                        try
                        {
                            jeCmd.Delete(lJEKey);
                        }
                        catch { }
                    }

                    {
                        jeCmd = BCE.AutoCount.GL.JournalEntry.JournalEntryCommand.Create(TransdbSetting);
                        BCE.AutoCount.GL.JournalEntry.JournalEntry jeentity = jeCmd.AddNew();
                        //  jeentity.CurrencyCode = "IDR";
                        // jeentity.CurrencyRate = (decimal)1;
                        jeentity.DocNo = dataRow1["DocNo"].ToString();
                        //jeentity.Description = dataRow1["DocNo"].ToString();
                        jeentity.DocDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
                        jeentity.Description = dataRow1["Description"] != null ? dataRow1["Description"].ToString() : "";

                        jeentity.MasterRow["Cancelled"] = dataRow1["Cancelled"];
                        decimal dTotalDR = 0;
                        decimal dTotalCR = 0;
                        decimal dTotalQty = 0;
                        string AccNoCR = "";
                        string AccNoDR = "";
                        bool bStandard = false;
                        DataTable NewTable = new DataTable();
                        // NewTable = jeentity.DataTableDetail.Copy();
                        //  NewTable.Clear();
                        NewTable.Columns.Add("AccNo", typeof(string));
                        NewTable.Columns.Add("Description", typeof(string));
                        NewTable.Columns.Add("RefNo2", typeof(string));
                        NewTable.Columns.Add("ProjNo", typeof(string));
                        NewTable.Columns.Add("DR", typeof(decimal));
                        NewTable.Columns.Add("CR", typeof(decimal));

                        NewTable.Constraints.Add("AccNo", NewTable.Columns["AccNo"], true);
                        //NewTable.Constraints.Add("ProjNo", NewTable.Columns["ProjNo"], false);
                        jeentity.ClearDetails();
                        //  BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordDR;
                        for (int i = 0; i < stockissue.GetValidDetailRows().Length; i++)
                        {
                            DataRow drDetail = stockissue.GetValidDetailRows()[i];

                            Decimal numSumRV = 0;

                            object oSumQtyRV = myDBSetting.ExecuteScalar("select sum(TotalCost) from RPA_TransRawMaterial with(nolock) where WONo=? and ProductCode=? and DocType='RV' and BOMCode=?", (object)drDetail["FromDocNo"], (object)drDetail["ItemCode"], (object)drDetail["BOMCode"]);
                            if (oSumQtyRV != null && oSumQtyRV != DBNull.Value)
                                numSumRV = BCE.Data.Convert.ToDecimal(oSumQtyRV);



                            if (drDetail["FromDocType"] != null && drDetail["FromDocType"] != DBNull.Value)
                            {
                                if (drDetail["FromDocType"].ToString() != "Product sortiran")
                                {
                                    dTotalQty += BCE.Data.Convert.ToDecimal(drDetail["Qty"]);
                                }
                            }
                            if (i == 0)
                            {
                                object obj = myDBSetting.ExecuteScalar("select CostType from RPA_ItemBOM where ItemCode=? and BOMCode=?", (object)drDetail["ItemCode"], (object)drDetail["BOMCode"]);
                                if (obj != DBNull.Value && obj != null)
                                {
                                    if (obj.ToString() == "Standard")
                                        bStandard = true;
                                    else
                                        bStandard = false;
                                }
                            }
                            string sProjNoNew = "";
                            string sProjNo = "";
                            sProjNo = drDetail["ProjNo"] != null ? drDetail["ProjNo"].ToString() : null;
                            //Add DR
                            //BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordDR = jeentity.AddDetail();
                            //dtlrecordDR.AccNo = BCE.Data.Convert.ToDBString(drDetail["Debit"]);
                            //if(drDetail["ProjNo"].ToString()!="")
                            //     dtlrecordDR.ProjNo = drDetail["ProjNo"].ToString();
                            // dtlrecordDR.DR =(BCE.Data.Convert.ToDecimal(drDetail["SubTotal"]));
                            dTotalDR += (BCE.Data.Convert.ToDecimal(drDetail["SubTotal"]));
                            //dtlrecordDR.CR = 0;
                            // dTotalDR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                            // dtlrecordDR.RefNo2 = dataRow1["DocNo"].ToString();
                            // dtlrecordDR.EndEdit();
                            dTotalCR +=  Math.Abs(numSumRV);//(BCE.Data.Convert.ToDecimal(drDetail["Qty"]) * BCE.Data.Convert.ToDecimal(drDetail["WIPCost"]));
                            DataRow NewRow = NewTable.Rows.Find(drDetail["Debit"]);
                            if (NewRow != null)
                            {
                                sProjNoNew = NewRow["ProjNo"] != null ? NewRow["ProjNo"].ToString() : null;
                                if (sProjNoNew == sProjNo)
                                {
                                    NewRow["DR"] = BCE.Data.Convert.ToDecimal(NewRow["DR"]) + BCE.Data.Convert.ToDecimal(drDetail["SubTotal"]);
                                }
                                else
                                {
                                    DataRow NewRow2 = NewTable.NewRow();
                                    NewRow2.BeginEdit();
                                    NewRow2["AccNo"] = drDetail["Debit"];
                                    NewRow2["ProjNo"] = sProjNo != "" ? sProjNo : null;
                                    NewRow2["RefNo2"] = dataRow1["DocNo"].ToString();
                                    NewRow2["DR"] = BCE.Data.Convert.ToDecimal(drDetail["SubTotal"]);
                                    NewRow2.EndEdit();
                                    NewTable.Rows.Add(NewRow2);
                                }
                            }
                            else
                            {
                                DataRow NewRow2 = NewTable.NewRow();
                                NewRow2.BeginEdit();
                                NewRow2["AccNo"] = drDetail["Debit"];
                                NewRow2["ProjNo"] = sProjNo != "" ? sProjNo : null;
                                NewRow2["RefNo2"] = dataRow1["DocNo"].ToString();
                                NewRow2["DR"] = BCE.Data.Convert.ToDecimal(drDetail["SubTotal"]);
                                NewRow2.EndEdit();
                                NewTable.Rows.Add(NewRow2);

                            }


                            //Add CR
                            AccNoDR = drDetail["Debit"] != null ? drDetail["Debit"].ToString() : "";

                            AccNoCR = drDetail["Credit"] != null ? drDetail["Credit"].ToString() : "";
                        }
                        //  BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordCR = jeentity.AddDetail();
                        //dtlrecordCR.AccNo = AccNoCR;
                        // dTotalCR += (BCE.Data.Convert.ToDecimal(dTotalBOMAmt));
                        //  dtlrecordCR.CR =(BCE.Data.Convert.ToDecimal(dTotalBOMAmt));
                        //dTotalCR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                        // dtlrecordCR.RefNo2 = dataRow1["DocNo"].ToString();
                        //dtlrecordCR.EndEdit();
                        DataRow NewRow3 = NewTable.NewRow();
                        NewRow3.BeginEdit();
                        NewRow3["AccNo"] = AccNoCR;
                        NewRow3["ProjNo"] = projnoBOM != "" ? projnoBOM : null;
                        NewRow3["RefNo2"] = dataRow1["DocNo"].ToString();
                        NewRow3["CR"] = BCE.Data.Convert.ToDecimal(dTotalCR);
                        NewRow3.EndEdit();
                        NewTable.Rows.Add(NewRow3);
                        foreach (DataRow ovdrow in overheadtable.Rows)
                        {
                            string sProjNoNew = "";
                            string sProjNo = "";
                            //sProjNo = drDetail["ProjNo"] != null ? drDetail["ProjNo"].ToString() : null;
                            object obj = myDBSetting.ExecuteScalar("select AccNo from RPA_Overhead where OverheadCode=?", (object)ovdrow["OverheadCode"]);
                            if (obj != null && obj != DBNull.Value)
                            {
                                //Add CR
                                // BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordCR2 = jeentity.AddDetail();
                                //  dtlrecordCR2.AccNo = BCE.Data.Convert.ToDBString(obj);
                                //if (drDetail["ProjNo"].ToString() != "")
                                //    dtlrecordCR.ProjNo = drDetail["ProjNo"].ToString();
                                dTotalCR += (BCE.Data.Convert.ToDecimal(ovdrow["Amount"]) * dTotalQty);
                                //dtlrecordCR.DR = 0;
                                //dtlrecordCR2.CR =(BCE.Data.Convert.ToDecimal(ovdrow["Amount"]) * dTotalQty);
                                //dTotalCR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                                //obj = myDBSetting.ExecuteScalar("select DocNo from RPA_WO where DocKey=?", (object)ovdrow["DocKey"]);
                                //if (obj != null && obj != DBNull.Value)
                                //{
                                //    dtlrecordCR2.RefNo2 = dataRow1["DocNo"].ToString();
                                //}
                                //dtlrecordCR2.EndEdit();
                                DataRow NewRow = NewTable.Rows.Find(obj);
                                if (NewRow != null)
                                {
                                    sProjNoNew = NewRow["ProjNo"] != null ? NewRow["ProjNo"].ToString() : null;
                                    if (sProjNoNew == projnoBOM)
                                    {
                                        NewRow["CR"] = BCE.Data.Convert.ToDecimal(NewRow["CR"]) + (BCE.Data.Convert.ToDecimal(ovdrow["Amount"]) * dTotalQty);
                                    }
                                    else
                                    {
                                        DataRow NewRow2 = NewTable.NewRow();
                                        NewRow2.BeginEdit();
                                        NewRow2["AccNo"] = obj;
                                        NewRow2["ProjNo"] = projnoBOM != "" ? projnoBOM : null;
                                        NewRow2["RefNo2"] = dataRow1["DocNo"].ToString();
                                        NewRow2["CR"] = BCE.Data.Convert.ToDecimal(ovdrow["Amount"]) * dTotalQty;
                                        NewRow2.EndEdit();
                                        NewTable.Rows.Add(NewRow2);
                                    }
                                }
                                else
                                {
                                    DataRow NewRow2 = NewTable.NewRow();
                                    NewRow2.BeginEdit();
                                    NewRow2["AccNo"] = obj;
                                    NewRow2["ProjNo"] = projnoBOM != "" ? projnoBOM : null;
                                    NewRow2["RefNo2"] = dataRow1["DocNo"].ToString();
                                    NewRow2["CR"] = BCE.Data.Convert.ToDecimal(ovdrow["Amount"]) * dTotalQty;
                                    NewRow2.EndEdit();
                                    NewTable.Rows.Add(NewRow2);

                                }
                            }

                        }
                        dTotalDR = 0;
                        dTotalCR = 0;
                        foreach (DataRow JEDetailRow in NewTable.Rows)
                        {

                            decimal ddebit = 0;
                            decimal dcredit = 0;
                            BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecord = jeentity.AddDetail();
                            dtlrecord.AccNo = JEDetailRow["AccNo"].ToString();
                            dtlrecord.Description = JEDetailRow["Description"] != null ? JEDetailRow["Description"].ToString() : null;
                            if (JEDetailRow["ProjNo"] != null && JEDetailRow["ProjNo"] != DBNull.Value)
                            {
                                if (JEDetailRow["ProjNo"].ToString().Length > 0)
                                    dtlrecord.ProjNo = JEDetailRow["ProjNo"].ToString();
                            }
                            ddebit = JEDetailRow["DR"] != DBNull.Value ? BCE.Data.Convert.ToDecimal(JEDetailRow["DR"]) : 0;
                            dcredit = JEDetailRow["CR"] != DBNull.Value ? BCE.Data.Convert.ToDecimal(JEDetailRow["CR"]) : 0;
                            if (ddebit > 0)
                                dtlrecord.DR = ddebit;
                            else
                                dtlrecord.CR = dcredit;
                            dtlrecord.EndEdit();
                            dTotalDR += ddebit;
                            dTotalCR += dcredit;
                        }

                        string strAccRound = "";
                        string strAccRoundDesc = "";
                        object objRound = myDBSetting.ExecuteScalar("select RoundAccNo from RPA_Settings");
                        if (objRound != null && objRound != DBNull.Value)
                        {
                            strAccRound = objRound.ToString();
                        }
                        objRound = myDBSetting.ExecuteScalar("select Description from GLMast where accno=?", (object)strAccRound);
                        if (objRound != null && objRound != DBNull.Value)
                        {
                            strAccRoundDesc = objRound.ToString();
                        }
                        if (dTotalDR > dTotalCR)
                        {
                            if (Decimal.Compare(Math.Round(dTotalDR, 2), Math.Round(dTotalCR, 2)) != 0 && strAccRound.Length > 0)
                            {
                                BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordCR2 = jeentity.AddDetail();
                                //if (bStandard)
                                //{
                                //    dtlrecordCR2.AccNo = AccNoCR;
                                //    dtlrecordCR2.Description = "Adjustment process WIP";
                                //}
                                //else
                                {
                                    dtlrecordCR2.AccNo = strAccRound;
                                    dtlrecordCR2.Description = strAccRoundDesc;
                                }
                                
                                if (projnoBOM != "")
                                    dtlrecordCR2.ProjNo = projnoBOM;
                                dtlrecordCR2.CR = dTotalDR - dTotalCR;
                                dTotalCR += dTotalDR - dTotalCR;
                                //decimal dSelisih = 0;
                                //dSelisih = dTotalDR - dTotalCR;
                                //int icount = 0;
                                //foreach (DataRow JEDetailRow in jeentity.GetValidDetailRows())
                                //{                                    
                                //    BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordCR2 = jeentity.EditDetail(icount);
                                //    if (dtlrecordCR2.DR != null && dtlrecordCR2.DR != DBNull.Value)
                                //    {
                                //        dtlrecordCR2.DR = BCE.Data.Convert.ToDecimal(JEDetailRow["DR"]) - (dSelisih);
                                //    }
                                //    dSelisih = 0;
                                //    icount++;
                                //}
                            }
                        }
                        else if (dTotalDR < dTotalCR)
                        {
                            if (Decimal.Compare(Math.Round(dTotalDR, 2), Math.Round(dTotalCR, 2)) != 0 && strAccRound.Length > 0)
                            {
                                BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordDR2 = jeentity.AddDetail();
                                //if (bStandard)
                                //{
                                //    dtlrecordDR2.AccNo = AccNoCR;
                                //    dtlrecordDR2.Description = "Adjustment process WIP";
                                //}
                                //else
                                {
                                    dtlrecordDR2.AccNo = strAccRound;
                                    dtlrecordDR2.Description = strAccRoundDesc;
                                }
                                if (projnoBOM != "")
                                    dtlrecordDR2.ProjNo = projnoBOM;
                                dtlrecordDR2.DR = dTotalCR - dTotalDR;
                                dTotalDR += dTotalCR - dTotalDR;


                                //decimal dSelisih = 0;
                                //dSelisih = dTotalDR - dTotalCR;
                                //int icount = 0;
                                //foreach (DataRow JEDetailRow in jeentity.GetValidDetailRows())
                                //{
                                //    BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordCR2 = jeentity.EditDetail(icount);
                                //    if (dtlrecordCR2.CR != null && dtlrecordCR2.CR != DBNull.Value)
                                //    {
                                //        dtlrecordCR2.CR = BCE.Data.Convert.ToDecimal(JEDetailRow["CR"]) - (dSelisih);
                                //    }
                                //    dSelisih = 0;
                                //    icount++;
                                //}
                            }
                        }
                        dTotalDR = (dTotalDR * 1);
                        dTotalCR = (dTotalCR * 1);
                        jeentity.Save(this.myUserAuthentication.LoginUserID);

                        dataRow1["JEKey"] = jeentity.DocKey;
                        dataRow1["JENo"] = jeentity.DocNo;
                    }
                }
                else
                {
                    if (BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
                        jeCmd.CancelDocument(lJEKey, this.myUserAuthentication.LoginUserID);
                    else if (!BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
                        jeCmd.UncancelDocument(lJEKey, this.myUserAuthentication.LoginUserID);

                }
                    TransdbSetting.Commit();
                
            }
            catch (SqlException ex)
            {
                TransdbSetting.Rollback();
                BCE.Data.DataError.HandleSqlException(ex);
                return false;
            }
            finally
            {
                TransdbSetting.EndTransaction();
                // AccountBookControl.EnableTransactionCounter();
            }
            return true;
        }

        public static DataTable GetItemAnalysisSummaryTable(DBSetting dbSetting, string docKeys, string tableName)
    {
      string str1 = string.Format("SELECT A.ItemCode, A.UOM, B.Description AS ItemDescription, C.Rate, SUM(A.Qty) AS Qty, SUM(A.Subtotal) AS Amount FROM {0} A, Item B, ItemUOM C WHERE A.DocKey IN (SELECT * FROM LIST(@DocKeyList)) AND A.ItemCode=B.ItemCode AND A.ItemCode=C.ItemCode AND A.UOM=C.UOM Group By A.ItemCode, A.UOM, B.Description, C.Rate ORDER BY A.ItemCode, A.UOM", (object) tableName);
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray[index] = (object) sqlParameter;
      DataTable dataTable = dbSetting1.GetDataTable(cmdText, num != 0, objArray);
      string str2 = "Item Code Analysis";
      dataTable.TableName = str2;
      return dataTable;
    }

    public static DataTable GetDateAnalysisSummaryTable(DBSetting dbSetting, string docKeys, string masterTableName, string detailTableName)
    {
      string str1 = string.Format("SELECT convert(nvarchar(10),A.DocDate,112) AS Date, SUM(D.Qty) AS Qty, SUM(A.Total) AS Amount FROM {0} A LEFT OUTER JOIN (SELECT B.DocKey, SUM(B.Qty) AS Qty FROM {1} B WHERE B.ItemCode IS NOT NULL Group By B.DocKey) AS D ON A.DocKey = D.DocKey WHERE A.DocKey IN (SELECT * FROM LIST(@DocKeyList)) Group By A.DocDate ", (object) masterTableName, (object) detailTableName);
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray[index] = (object) sqlParameter;
      DataTable dataTable = dbSetting1.GetDataTable(cmdText, num != 0, objArray);
      string str2 = "Analysis by Date";
      dataTable.TableName = str2;
      return dataTable;
    }

    public static DataTable GetMonthAnalysisSummaryTable(DBSetting dbSetting, string docKeys, string masterTableName, string detailTableName)
    {
      string str1 = string.Format("SELECT DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4)) AS Month, SUM(D.Qty) AS Qty, SUM(A.Total) AS Amount FROM {0} A LEFT OUTER JOIN (SELECT B.DocKey, SUM(B.Qty) AS Qty FROM {1} B WHERE B.ItemCode IS NOT NULL Group By B.DocKey) AS D ON A.DocKey = D.DocKey WHERE A.DocKey IN (SELECT * FROM LIST(@DocKeyList)) Group By DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4))", (object) masterTableName, (object) detailTableName);
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray[index] = (object) sqlParameter;
      DataTable dataTable = dbSetting1.GetDataTable(cmdText, num != 0, objArray);
      string str2 = "Analysis by Month";
      dataTable.TableName = str2;
      return dataTable;
    }

    public static DataTable GetYearAnalysisSummaryTable(DBSetting dbSetting, string docKeys, string masterTableName, string detailTableName)
    {
      string str1 = string.Format("SELECT Year(A.DocDate) AS Year, SUM(D.Qty) AS Qty, SUM(A.Total) AS Amount FROM {0} A LEFT OUTER JOIN (SELECT B.DocKey, SUM(B.Qty) AS Qty FROM {1} B WHERE B.ItemCode IS NOT NULL Group By B.DocKey) AS D ON A.DocKey = D.DocKey WHERE A.DocKey IN (SELECT * FROM LIST(@DocKeyList)) Group By Year(A.DocDate) ", (object) masterTableName, (object) detailTableName);
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray[index] = (object) sqlParameter;
      DataTable dataTable = dbSetting1.GetDataTable(cmdText, num != 0, objArray);
      string str2 = "Analysis by Year";
      dataTable.TableName = str2;
      return dataTable;
    }
  }
}
