﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveValidateItemQtyEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;

namespace Production.StockReceive
{
  [Serializable]
  public class StockReceiveValidateItemQtyEventArgs : StockReceiveEventArgs
  {
    private bool myHandled;

    public bool Handled
    {
      get
      {
        return this.myHandled;
      }
      set
      {
        this.myHandled = value;
      }
    }

    internal StockReceiveValidateItemQtyEventArgs(StockReceive doc)
      : base(doc)
    {
    }
  }
}
