﻿// Type: BCE.AutoCount.Stock.StockReceive.FormEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Document;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;
using System.Data;

namespace Production.StockReceive
{
    [Obsolete("Use FormStockReceiveEntry.FormEventArgs instead")]
    public class FormStockReceiveEventArgs
    {
        private StockReceive myStockReceive;
        private PanelControl myHeaderPanel;
        private GridControl myGridControl;
        private XtraTabControl myTabControl;

        public PanelControl HeaderPanel
        {
            get
            {
                return this.myHeaderPanel;
            }
        }

        public GridControl GridControl
        {
            get
            {
                return this.myGridControl;
            }
        }

        public XtraTabControl TabControl
        {
            get
            {
                return this.myTabControl;
            }
        }

        public StockReceive StockReceive
        {
            get
            {
                return this.myStockReceive;
            }
        }

        public DataTable MasterTable
        {
            get
            {
                return this.myStockReceive.DataTableMaster;
            }
        }

        public DataTable DetailTable
        {
            get
            {
                return this.myStockReceive.DataTableDetail;
            }
        }

        public EditWindowMode EditWindowMode
        {
            get
            {
                if (this.myStockReceive.Action == StockReceiveAction.New)
                    return EditWindowMode.New;
                else if (this.myStockReceive.Action == StockReceiveAction.Edit)
                    return EditWindowMode.Edit;
                else
                    return EditWindowMode.View;
            }
        }

        public FormStockReceiveEventArgs(StockReceive doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
        {
            this.myStockReceive = doc;
            this.myHeaderPanel = headerPanel;
            this.myGridControl = gridControl;
            this.myTabControl = tabControl;
        }
    }
}
