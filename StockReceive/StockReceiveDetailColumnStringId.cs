﻿// Type: BCE.AutoCount.Stock.StockReceive.StockReceiveDetailColumnStringId
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Localization;

namespace Production.StockReceive
{
  [LocalizableString]
  public enum StockReceiveDetailColumnStringId
  {
    [DefaultString("Detail Search Fields")] DetailSearchFields,
    [DefaultString("Detail")] Detail,
    [DefaultString("Item Code")] ItemCode,
    [DefaultString("UOM")] UOM,
    [DefaultString("Description")] Description,
    [DefaultString("Item Description")] ItemDescription,
    [DefaultString("Item Further Description")] ItemFurtherDescription,
    [DefaultString("Location")] Location,
    [DefaultString("Batch No.")] BatchNo,
    [DefaultString("Project No.")] ProjectNo,
    [DefaultString("Department No.")] DepartmentNo,
    [DefaultString("Project")] Project,
    [DefaultString("Department")] Department,
    [DefaultString("Sub-Total")] SubTotal,
    [DefaultString("Quantity")] Quantity,
    [DefaultString("Unit Cost")] UnitCost,
    [DefaultString("Serial No List")] SerialNoList,
    [DefaultString("Numbering")] Numbering,
  }
}
