USE [AED_PROD_13022020]
GO

/****** Object:  View [dbo].[vRPA_ItemDefaultBOM]    Script Date: 11/18/2020 06:11:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vRPA_ItemDefaultBOM]
AS
select ItemCode,BOMCode,BOMDesc, Description, Desc2, SUM(Rate*BalQty) AS BalQty,
 SUM(ISNULL(Rate*CSGNQty,0)) AS CSGNQty, SUM(Rate*(BalQty-ISNULL(CSGNQty,0))) AS AfterCSGNQty
 from(
SELECT A.ItemCode,
(select top 1 BOMCode from RPA_ItemBOM IB where IB.ItemCode=A.ItemCode and IB.UOM=B.UOM) AS BOMCode,
(select top 1 Desc2 from RPA_ItemBOM IB where IB.ItemCode=A.ItemCode and IB.UOM=B.UOM)  as BOMDesc, A.Description, A.Desc2,B.Rate,B.BalQty,B.CSGNQty
 FROM Item A  LEFT OUTER JOIN ItemUOM B ON(A.ItemCode = B.ItemCode) WHERE A.IsActive = 'T' 
 )
 result
  GROUP BY ItemCode,BOMCode,BOMDesc, Description, Desc2
GO


