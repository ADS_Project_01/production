ALTER VIEW [dbo].[vBG_BonusPointCard] as
select CAST(bto.DocKey AS nvarchar(20))+ISNULL(bpay.DocNo,'')+ISNULL(bto.ProjNo,'') as UniqueKey,bto.*,BP.DONo1,BP.DONo2,bpay.DocNo as VoucherNo,bpay.DocDate as VoucherDate,ISNULL(bpay.PaymentAmt,0) as PaymentAmt,ISNULL(bpay.SubKnockAmt,0) as SubKnockOff,prj.Description as ProjDesc,ISNULL(BTO.Outstanding,TotalAmount) AS Outstanding2
 from BG_TransOutstandingBP bto
left outer join
(select py.*,pyk.KnockOffDocKey,pyk.Amount as SubKnockAmt from BG_PaymentBPKnockOff pyk
inner join
BG_PaymentBP py
on py.DocKey=pyk.DocKey) as bpay
on bto.DocKey=bpay.KnockOffDocKey
left outer join Project prj
on bto.ProjNo=prj.ProjNo
LEFT OUTER JOIN
(
SELECT a.DocKey,
 STUFF(ISNULL((SELECT ', ' + b.DONo
                FROM BG_BPDtl b
               WHERE b.DocKey = a.DocKey
            GROUP BY b.DONo
             FOR XML PATH (''), TYPE).value('.','VARCHAR(max)'), ''), 1, 2, '') as DONo1,       
       ISNULL((SELECT ', ' + b.DONo
                FROM BG_BPDtl b
               WHERE b.DocKey = a.DocKey
            GROUP BY b.DONo
             FOR XML PATH (''), TYPE).value('.','VARCHAR(max)'), '') as DONo2
  FROM BG_BP a) BP
  ON BP.DocKey=bto.TransKey
  WHERE
  TransType in('Crew Kirim','Crew Tangkap','Sales','Purchase')
  union all
select  CAST(bto.DocKey AS nvarchar(20))+ISNULL(bpay.DocNo,'')+ISNULL(bto.ProjNo,'') as UniqueKey,bto.*,'' AS DONo1,'' AS DONo2,bpay.DocNo as VoucherNo,bpay.DocDate as VoucherDate,ISNULL(bpay.PaymentAmt,0) AS PaymentAmt,ISNULL(bpay.SubKnockAmt,0) AS SubKnockAmt,prj.Description as ProjDesc,ISNULL(BTO.Outstanding,TotalAmount) AS Outstanding2
 from BG_TransOutstandingBP bto
left outer join
(select py.*,pyk.KnockOffDocKey,pyk.Amount as SubKnockAmt from BG_PaymentBPKnockOff pyk
inner join
BG_PaymentBP py
on py.DocKey=pyk.DocKey) as bpay
on bto.DocKey=bpay.KnockOffDocKey
left outer join Project prj
on bto.ProjNo=prj.ProjNo
  WHERE
  TransType in('Debtor','Collector')
GO
CREATE VIEW [dbo].[vBG_BPCardIVDtl] AS 
select distinct CAST(m.DocKey AS nvarchar(20))+ISNULL(m.VoucherNo,'')+ISNULL(m.ProjNo,'') as UniqueKey, m.DocKey,DebtorName,m.TransNo,m.TransDate,m.TransType,m.ProjNo,
p.DebtorCode,p.DocNo,p.DocDate,p.NetTotal,p.PaymentAmt,p.Outstanding 
from vBG_BonusPointCard m
left outer join
(select ISNULL(DebtorProjNo,'')+ISNULL(a.DocNo,'')+'Debtor' QKey, b.DebtorProjNo,b.DebtorName, a.DocNo as TransNo,a.DocKey as TransKey,c.*,'Debtor' as TransType from BG_BC a inner join BG_BCDtl b on a.DocKey=b.DocKey 
inner join ARInvoice c on b.IVNo=c.DocNo) p on p.TransType=m.TransType and m.TransNo=p.TransNo and m.ProjNo=p.DebtorProjNo


