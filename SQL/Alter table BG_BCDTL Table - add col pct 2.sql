ALTER VIEW [dbo].[vBG_BonusDebtorTransfer]
as
select * ,((TotalBucket*BonusPointPct)/100)*BonusPointAmt as Jumlah,
(select UDF_IDColl from ARPayment where DocKey=RIKey) as IDCollector,
(select UDF_ColName from ARPayment where DocKey=RIKey) as CollectorName,
(TotalBucket* CollectorPct) / 100 AS JmlPoint,
((TotalBucket* CollectorPct) / 100)*CollectorAmt as JmlBonus
 from(
select distinct CAST(0 as bit) as Checklist,a.DocKey,a.DocNo,a.DocDate,a.DebtorCode,IV.DebtorName,
(select top 1 DocNo from ARPayment inner join ARPaymentKnockOff on ARPayment.DocKey=ARPaymentKnockOff.DocKey where
					ARPaymentKnockOff.KnockOffDocKey=a.DocKey and KnockOffDocType='RI') RINo,
					(select top 1 ARPayment.DocKey from ARPayment inner join ARPaymentKnockOff on ARPayment.DocKey=ARPaymentKnockOff.DocKey where
					ARPaymentKnockOff.KnockOffDocKey=a.DocKey and KnockOffDocType='RI') RIKey,
(select top 1 DocDate from ARPayment inner join ARPaymentKnockOff on ARPayment.DocKey=ARPaymentKnockOff.DocKey where
					ARPaymentKnockOff.KnockOffDocKey=a.DocKey and KnockOffDocType='RI') RIDate,
					(SELECT COUNT(*) FROM IVDTL WHERE DocKey=IV.DocKey GROUP BY DocKey) as TotalBucket,
					(SELECT top 1 UDF_PlatNo FROM IVDTL WHERE DocKey=IV.DocKey) as PlatNo,
					d.UDF_ProjNo as DebtorProjNo,
					(select DebtorBonusAmt from BG_SettingBP) as BonusPointAmt,
					(select DebtorBonusPct from BG_SettingBP) as BonusPointPct,
						(select CollectorAmt from BG_SettingBP) as CollectorAmt,
					(select CollectorPct from BG_SettingBP) as CollectorPct
 from 
 IV 
 inner join 
ARInvoice a
on IV.DocKey=a.SourceKey
inner join
ARPaymentKnockOff b
on a.DocKey=b.KnockOffDocKey
inner join
ARPayment c
on 
b.DocKey=c.DocKey
left outer join
Debtor d
on IV.DebtorCode=D.AccNo
where
DATEDIFF(day,a.DocDate,c.DocDate)<=(select InvoiceAgeMax from BG_SettingBP)
AND a.Outstanding=0 and b.KnockOffDocType='RI'
and isnull(IV.UDF_IsTransfer,'F')='F'
) result where DocDate>='2017/11/25' and RIDate>='2017/11/25'
GO
alter table BG_BCDtl
add JmlBonus [dbo].[d_Price] NULL
GO
alter table BG_BCDtl
add JmlPoint [dbo].[d_Qty] NULL

