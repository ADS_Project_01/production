USE [AED_BERKAH_2014]
GO

/****** Object:  View [dbo].[vBG_LaporanTabungan]    Script Date: 12/09/2017 00:17:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vBG_LaporanTabungan] as
select 0 AS DocKey,result.*,GLMast.Description,GLMast.Desc2 from (
Select 'Debtor' DocType,ProjNo,AccNo,JournalType,SourceType,RefNo1,DocNo,DocDate,Ekor,Kilo,ISNULL(HomeDR,0) AS HomeDR,ISNULL(HomeCR,0) AS HomeCR, ISNULL(HomeCR,0)/Kilo AS Tab_Per_Kilo,ISNULL(HomeDR,0)-ISNULL(HomeCR,0) AS HomeBalance, 0 as HomeBalance2 From GLDTL A with(Nolock) Inner Join (SELECT Sum(IVDTL.UDF_Tekor) as Ekor,Sum(IVDTL.Qty) as Kilo,DocNo,DocDate FROM IVDTL with(Nolock) INNER JOIN IV with(Nolock) ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' 
Group by ProjNo,DocNo,DocDate) B
ON A.RefNo1=B.DocNo 
where AccNo=(select AgentSaveAcc from BG_SettingBP)
union all
Select 'Creditor' DocType,ProjNo,AccNo,JournalType,SourceType,RefNo1,DocNo,DocDate,Ekor,Kilo,ISNULL(HomeDR,0) AS HomeDR,ISNULL(HomeCR,0) AS HomeCR, ISNULL(HomeDR,0)/Kilo AS Tab_Per_Kilo,ISNULL(HomeDR,0)-ISNULL(HomeCR,0) AS HomeBalance, 0 as HomeBalance2  From GLDTL A with(Nolock) Inner Join (SELECT Sum(PIDTL.UDF_Tekor) as Ekor,Sum(PIDTL.Qty) as Kilo,DocNo,DocDate FROM PIDTL with(Nolock) INNER JOIN [PI]with(Nolock) ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN'
Group by ProjNo,DocNo,DocDate) B
ON A.RefNo1=B.DocNo
where AccNo=(select CreditorSaveAcc from BG_SettingBP)
) result 
inner join GLMast on result.AccNo=GLMast.AccNo
GO


