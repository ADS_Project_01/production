

CREATE FUNCTION [dbo].[F_RPA_WOStatus] (@DocKey bigint)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @Status VARCHAR(250)
    
    DECLARE @CountDetail int
	DECLARE @CountPlanned int
	DECLARE @CountProcess int
	DECLARE @CountAssembly int
	DECLARE @CountClose int
	DECLARE @CountPending int
	DECLARE @CountCancelHeader int
	DECLARE @CountCancel int
   select @CountCancelHeader=COUNT(*) from RPA_WO with(nolock) where DocKey=@DocKey and Cancelled='T'
   select @CountCancel=COUNT(*) from RPA_WOProduct with(nolock) where DocKey=@DocKey and Status='Cancel'
   select @CountDetail=COUNT(*) from RPA_WOProduct with(nolock) where DocKey=@DocKey and Status!='Cancel' and Status!='Closed'
   select @CountPending=COUNT(*) from RPA_WOProduct with(nolock) where DocKey=@DocKey and Status='Pending' and Status!='Closed'
   select @CountPlanned=COUNT(*) from RPA_WOProduct with(nolock) where DocKey=@DocKey and Status='Planned' and Status!='Closed'
   select @CountProcess=COUNT(*) from RPA_WOProduct with(nolock) where DocKey=@DocKey and Status='InProcess' and Status!='Closed'
   select @CountAssembly=COUNT(*) from RPA_WOProduct with(nolock) where DocKey=@DocKey and Status='Assembly' and Status!='Closed' 
   select @CountClose=COUNT(*) from RPA_WOProduct with(nolock) where DocKey=@DocKey and Status='Closed'
	if @CountCancelHeader>0
		begin
			set @Status='Cancel';	
		end
	else
		begin
			if @CountClose=0
			begin
				if @CountPending=@CountDetail and @CountPending>0
				begin
				set @Status='Pending';
				end
			
				if @CountPlanned=@CountDetail and @CountPlanned>0
				begin
				set @Status='Planned';
				end
				
				if @CountProcess=@CountDetail  and @CountProcess>0
				begin
				set @Status='Full InProcess';
				end
				else if @CountProcess<@CountDetail  and @CountProcess>0
				begin
				set @Status='Partial InProcess';
				end
				
				if @CountAssembly=@CountDetail   and @CountAssembly>0
				begin
				set @Status='Full Assembly';
				end
				else if @CountAssembly<@CountDetail and @CountAssembly>0
				begin
				set @Status='Partial Assembly';
				end
				
				
				if @CountCancel=@CountDetail   and @CountCancel>0
				begin
				set @Status='Full Assembly';
				end
			
			end
			else
			begin
				set @Status='Closed';
			end
	end
		

	
    RETURN @Status
END


