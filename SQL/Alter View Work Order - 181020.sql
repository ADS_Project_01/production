ALTER view [dbo].[vRPA_WorkOrderProduct]
as
select * from RPA_WOProduct a with(nolock) 
left outer join 
(select FromDocDtlKey as FromDocDtlKey2,SUM(ActQty) as ActQty from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
GROUP BY rcvdtl.FromDocDtlKey) subb group by subb.FromDocDtlKey) b on a.DtlKey=b.FromDocDtlKey2
GO
ALTER view [dbo].[vRPA_WorkOrderDetail]
as
SELECT a.*,b.ActQty,b.ActUnitCost,b.FromDocDtlKey as FromDocDtlKey2,
b.FromDocNo as FromDocNo2,b.OrgActQty,b.ReturQty FROM RPA_WODtl a with(nolock) 
left outer join(
select FromDocNo,FromDocDtlKey,FromDocType,SUM(COALESCE(rmdtl.Qty,0)) as OrgActQty,
SUM(COALESCE(rmdtl.TransferedQty,0)) ReturQty,
SUM(COALESCE(rmdtl.Qty,0)-COALESCE(rmdtl.TransferedQty,0)) ActQty,
AVG(COALESCE(rmdtl.UnitCost,0)) as ActUnitCost  from RPA_RMDTL rmdtl with(nolock) 
inner join RPA_RM rm  with(nolock) on rmdtl.DocKey=rm.DocKey where Cancelled='F' GROUP BY FromDocNo,FromDocDtlKey,FromDocType)
b  on a.DtlKey=b.FromDocDtlKey and b.FromDocType='WO'
GO
ALTER view [dbo].[vRPA_WorkOrderAP]
as
select * from RPA_WOAP a with(nolock) 
left outer join 
(select FromDocDtlKey,SUM(ActQty) as ActQty from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
AND FromDocType in ('AP','Alternatif Product')
GROUP BY rcvdtl.FromDocDtlKey) subb group by subb.FromDocDtlKey) b on a.DtlKey=b.FromDocDtlKey
GO
ALTER view [dbo].[vRPA_WorkOrderBS]
as
select * from RPA_WOBS a with(nolock) 
left outer join 
(select FromDocDtlKey FromDocDtlKey2,SUM(ActQty) as ActQty,SUM(ActUnitCost) as ActUnitCost from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty,AVG(COALESCE(rcvdtl.UnitCost,0)) as ActUnitCost  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
AND FromDocType in ('BS','Product sortiran')
GROUP BY rcvdtl.FromDocDtlKey) subb group by subb.FromDocDtlKey) b on a.DtlKey=b.FromDocDtlKey2
