CREATE TABLE [dbo].[RPA_WOProduct](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[UOM] [dbo].[d_UOM] NULL,
	[Rate] [dbo].[d_Qty] NULL,
	[Qty] [dbo].[d_Qty] NULL,
	[BOMCode] [dbo].[d_ItemCode] NOT NULL,
	[TransferedQty] [dbo].[d_Qty] NULL,
	[Status] [nvarchar](30) NOT NULL,
	[COGM] [dbo].[d_Cost] NULL,
	[FromDocType] [dbo].[d_DocType] NULL,
	[FromDocNo] [dbo].[d_DocNo] NULL,
	[FromDocDtlKey] [bigint] NULL,
	[DebtorCode] [dbo].[d_AccNo] NULL,
	[DebtorName] [dbo].[d_AccDescription] NULL,
	[Style] [nvarchar](50) NULL,
	[Length] [decimal](18, 2) NULL,
	[UOMLength] [nvarchar](10) NULL,
	[Width] [decimal](18, 2) NULL,
	[UOMWidth] [nvarchar](10) NULL,
	[Size] [nvarchar](50) NULL,
	[Tickness] [decimal](18, 4) NULL,
	[PrintOut] [dbo].[d_Boolean] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Weight] [dbo].[d_Number] NULL,
	[WeightUOM] [dbo].[d_UOM] NULL,
	[Volume] [dbo].[d_Number] NULL,
	[VolumeUOM] [dbo].[d_UOM] NULL,
	[Cancelled] [char](1) NULL,
	[CancelledUserID] [dbo].[d_UserID] NULL,
	[CancelledDate] [datetime] NULL,
 CONSTRAINT [PK_RPA_WOProduct] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[RPA_WOOvd](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[OverheadCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[Amount] [dbo].[d_Money] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[FromDocDtlKey] [bigint] NULL,
	[FromBOMCode] [dbo].[d_ItemCode] NULL,
	[ParentBOMCode] [dbo].[d_ItemCode] NULL,
 CONSTRAINT [PK_RPA_WOOvd] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[RPA_WODtl](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[UOM] [dbo].[d_UOM] NULL,
	[Rate] [dbo].[d_Qty] NULL,
	[Qty] [dbo].[d_Qty] NULL,
	[TransferedQty] [dbo].[d_Qty] NULL,
	[BalQty] [dbo].[d_Qty] NULL,
	[UnitCost] [dbo].[d_Cost] NULL,
	[TotalCost] [dbo].[d_Money] NULL,
	[PrintOut] [dbo].[d_Boolean] NULL,
	[StockReceived] [dbo].[d_Boolean] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[FromDocDtlKey] [bigint] NULL,
	[FromBOMCode] [nvarchar](200) NULL,
	[ParentBOMCode] [nvarchar](200) NULL,
	[BOMCode] [dbo].[d_ItemCode] NULL,
	[Level] [int] NULL,
	[ProductRatio] [dbo].[d_Money] NULL,
	[RateUOM] [dbo].[d_Qty] NULL,
 CONSTRAINT [PK_RPA_WODtl] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_WOBS](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[CostPercent] [dbo].[d_Money] NULL,
	[CostMax] [dbo].[d_Money] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[ActHPP] [dbo].[d_Money] NULL,
	[FromDocDtlKey] [bigint] NULL,
	[FromBOMCode] [nvarchar](30) NULL,
	[ParentBOMCode] [nvarchar](30) NULL,
 CONSTRAINT [PK_RPA_WOBS] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_WOAP](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[ActHPP] [dbo].[d_Money] NULL,
 CONSTRAINT [PK_RPA_WOAP] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_WO](
	[DocKey] [bigint] NOT NULL,
	[DocNo] [dbo].[d_DocNo] NOT NULL,
	[DocDate] [datetime] NOT NULL,
	[Description] [nvarchar](100) NULL,
	[DebtorCode] [nvarchar](12) NULL,
	[DebtorName] [nvarchar](100) NULL,
	[ProjNo] [nvarchar](10) NULL,
	[MachineCode] [nvarchar](30) NOT NULL,
	[Location] [dbo].[d_Location] NOT NULL,
	[Status] [nvarchar](30) NOT NULL,
	[ProductionStartDate] [datetime] NULL,
	[ProductionEndDate] [datetime] NULL,
	[ToDocType] [dbo].[d_DocType] NULL,
	[ToDocDtlKey] [bigint] NULL,
	[ToDocKey] [bigint] NULL,
	[Note] [ntext] NULL,
	[Remark1] [dbo].[d_Remark] NULL,
	[Remark2] [dbo].[d_Remark] NULL,
	[Remark3] [dbo].[d_Remark] NULL,
	[Remark4] [dbo].[d_Remark] NULL,
	[PrintCount] [smallint] NULL,
	[Cancelled] [dbo].[d_Boolean] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedUserID] [dbo].[d_UserID] NOT NULL,
	[CreatedTimeStamp] [datetime] NOT NULL,
	[CreatedUserID] [dbo].[d_UserID] NOT NULL,
	[ExternalLink] [ntext] NULL,
	[RefDocNo] [dbo].[d_DocNo] NULL,
	[LastUpdate] [int] NOT NULL,
	[CanSync] [dbo].[d_Boolean] NULL,
	[TotalAssemblyRequestQty] [dbo].[d_Qty] NULL,
	[AssemblyStatus] [smallint] NULL,
	[LastAOPModified] [datetime] NULL,
	[LastAOPModifiedUserID] [dbo].[d_UserID] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[DocType] [varchar](20) NULL,
	[FromBOMCode] [nvarchar](100) NULL,
	[ParentBOMCode] [nvarchar](100) NULL,
 CONSTRAINT [PK__RPA_WO__434EFC1B2863CE43] PRIMARY KEY NONCLUSTERED 
(
	[DocKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_RPA_WO_DocNo] UNIQUE NONCLUSTERED 
(
	[DocNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_TransRawMaterial](
	[StockDTLKey] [bigint] IDENTITY(1,1) NOT NULL,
	[DocType] [dbo].[d_DocType] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[DtlKey] [bigint] NOT NULL,
	[DocNo] [dbo].[d_DocNo] NULL,
	[DocDate] [datetime] NULL,
	[WONo] [dbo].[d_DocNo] NOT NULL,
	[BOMCode] [dbo].[d_ItemCode] NOT NULL,
	[ProductCode] [dbo].[d_ItemCode] NOT NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[BatchNo] [dbo].[d_BatchNo] NULL,
	[Location] [dbo].[d_Location] NOT NULL,
	[UOM] [dbo].[d_UOM] NOT NULL,
	[Qty] [dbo].[d_Qty] NOT NULL,
	[Cost] [dbo].[d_Cost] NULL,
	[TotalCost] [dbo].[d_Cost] NULL,
	[LastModified] [datetime] NULL,
	[Seq] [bigint] NULL,
	[ProjNo] [dbo].[d_Project] NULL,
	[DeptNo] [dbo].[d_Dept] NULL,
	[AdjustedCost] [dbo].[d_Cost] NULL,
	[CostType] [dbo].[d_CostType] NULL,
	[ReferTo] [bigint] NOT NULL,
	[InputCost] [dbo].[d_Cost] NOT NULL,
	[BatchNoProduct] [nvarchar](100) NULL,
 CONSTRAINT [PK_RPA_TransRawMaterial] PRIMARY KEY CLUSTERED 
(
	[StockDTLKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_Settings](
	[rk] [decimal](27, 0) NULL,
	[rkd] [decimal](27, 0) NOT NULL,
	[NextNumber] [int] NOT NULL,
	[Format] [nvarchar](30) NOT NULL,
	[NextNumberRM] [int] NULL,
	[FormatRM] [nvarchar](30) NULL,
	[NextNumberRV] [int] NULL,
	[FormatRV] [nvarchar](30) NULL,
	[NextNumberRCVRM] [int] NULL,
	[FormatRCVRM] [nvarchar](30) NULL,
	[NextNumberRCVFR] [int] NULL,
	[FormatRCVFR] [nvarchar](30) NULL,
	[RoundAccNo] [dbo].[d_AccNo] NULL,
	[IsMachineNo] [varchar](1) NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_RMSubDtl](
	[SubDtlKey] [bigint] NOT NULL,
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[UOM] [dbo].[d_UOM] NULL,
	[BatchNo] [dbo].[d_BatchNo] NULL,
	[Description] [nvarchar](40) NULL,
	[ManufacturedDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[Qty] [dbo].[d_Qty] NULL,
 CONSTRAINT [PK_RPA_RMSubDtl] PRIMARY KEY CLUSTERED 
(
	[SubDtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_RMDTL](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[ItemCode] [dbo].[d_ItemCode] NULL,
	[Location] [dbo].[d_Location] NULL,
	[BatchNo] [dbo].[d_BatchNo] NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[FurtherDescription] [ntext] NULL,
	[ProjNo] [dbo].[d_Project] NULL,
	[DeptNo] [dbo].[d_Dept] NULL,
	[Qty] [dbo].[d_Qty] NULL,
	[UOM] [dbo ].[d_UOM] NULL,
	[UnitCost] [dbo].[d_Cost] NULL,
	[SubTotal] [dbo].[d_Money] NULL,
	[PrintOut] [dbo].[d_Boolean] NOT NULL,
	[SerialNoList] [ntext] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Debit] [dbo].[d_AccNo] NULL,
	[Credit] [dbo].[d_AccNo] NULL,
	[FromDocType] [dbo].[d_DocType] NULL,
	[FromDocNo] [dbo].[d_DocNo] NULL,
	[FromDocDtlKey] [bigint] NULL,
	[ToJENo] [nvarchar](20) NULL,
	[ToJEDtlKey] [bigint] NULL,
	[TransferedQty] [dbo].[d_Qty] NULL,
	[BOMCode] [dbo].[d_ItemCode] NULL,
PRIMARY KEY NONCLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_RM](
	[DocKey] [bigint] NOT NULL,
	[DocNo] [dbo].[d_DocNo] NOT NULL,
	[DocDate] [datetime] NOT NULL,
	[Description] [nvarchar](100) NULL,
	[Total] [dbo].[d_Money] NULL,
	[Note] [ntext] NULL,
	[Remark1] [dbo].[d_Remark] NULL,
	[Remark2] [dbo].[d_Remark] NULL,
	[Remark3] [dbo].[d_Remark] NULL,
	[Remark4] [dbo].[d_Remark] NULL,
	[PrintCount] [smallint] NOT NULL,
	[Cancelled] [dbo].[d_Boolean] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedUserID] [dbo].[d_UserID] NOT NULL,
	[CreatedTimeStamp] [datetime] NOT NULL,
	[CreatedUserID] [dbo].[d_UserID] NOT NULL,
	[ExternalLink] [ntext] NULL,
	[RefDocNo] [dbo].[d_DocNo] NULL,
	[LastUpdate] [int] NOT NULL,
	[CanSync] [dbo].[d_Boolean] NOT NULL,
	[ReallocatePurchaseByProject] [dbo].[d_Boolean] NOT NULL,
	[ReallocatePurchaseByProjectJEDocKey] [bigint] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[ReallocatePurchaseByProjectNo] [dbo].[d_Project] NULL,
	[DocType] [dbo].[d_ItemGroup] NULL,
	[AccNo] [dbo].[d_AccNo] NULL,
	[JEKey] [bigint] NULL,
	[JENo] [nvarchar](30) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[DocKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_RPA_RM_DocNo] UNIQUE NONCLUSTERED 
(
	[DocNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_RCVRMDTL](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[ItemCode] [dbo].[d_ItemCode] NULL,
	[Location] [dbo].[d_Location] NULL,
	[BatchNo] [dbo].[d_BatchNo] NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[FurtherDescription] [ntext] NULL,
	[ProjNo] [dbo].[d_Project] NULL,
	[DeptNo] [dbo].[d_Dept] NULL,
	[Qty] [dbo].[d_Qty] NULL,
	[UOM] [dbo].[d_UOM] NULL,
	[UnitCost] [dbo].[d_Cost] NULL,
	[SubTotal] [dbo].[d_Money] NULL,
	[PrintOut] [dbo].[d_Boolean] NOT NULL,
	[SerialNoList] [ntext] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Debit] [dbo].[d_AccNo] NULL,
	[Credit] [dbo].[d_AccNo] NULL,
	[FromDocType] [dbo].[d_DocType] NULL,
	[FromDocNo] [dbo].[d_DocNo] NULL,
	[FromDocDtlKey] [bigint] NULL,
	[JEKey] [bigint] NULL,
	[JENo] [nvarchar](30) NULL,
	[BOMCode] [dbo].[d_ItemCode] NULL,
PRIMARY KEY NONCLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_RCVRM](
	[DocKey] [bigint] NOT NULL,
	[DocNo] [dbo].[d_DocNo] NOT NULL,
	[DocType] [dbo].[d_DocType] NULL,
	[MachineCode] [nvarchar](30) NULL,
	[DocDate] [datetime] NOT NULL,
	[BatchNo] [dbo].[d_BatchNo] NULL,
	[Description] [nvarchar](100) NULL,
	[Total] [dbo].[d_Money] NULL,
	[Note] [ntext] NULL,
	[Remark1] [dbo].[d_Remark] NULL,
	[Remark2] [dbo].[d_Remark] NULL,
	[Remark3] [dbo].[d_Remark] NULL,
	[Remark4] [dbo].[d_Remark] NULL,
	[PrintCount] [smallint] NOT NULL,
	[Cancelled] [dbo].[d_Boolean] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedUserID] [dbo].[d_UserID] NOT NULL,
	[CreatedTimeStamp] [datetime] NOT NULL,
	[CreatedUserID] [dbo].[d_UserID] NOT NULL,
	[ExternalLink] [ntext] NULL,
	[RefDocNo] [dbo].[d_DocNo] NULL,
	[LastUpdate] [int] NOT NULL,
	[CanSync] [dbo].[d_Boolean] NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[JEKey] [bigint] NULL,
	[JENo] [nvarchar](30) NULL,
 CONSTRAINT [PK__RPA_RCVRM__434EFC1B5812E165] PRIMARY KEY NONCLUSTERED 
(
	[DocKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_RPA_RCVRM_DocNo] UNIQUE NONCLUSTERED 
(
	[DocNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_RCVDTL](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[Seq] [int] NOT NULL,
	[Numbering] [dbo].[d_Numbering] NULL,
	[ItemCode] [dbo].[d_ItemCode] NULL,
	[Location] [dbo].[d_Location] NULL,
	[BatchNo] [dbo].[d_BatchNo] NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[FurtherDescription] [ntext] NULL,
	[ProjNo] [dbo].[d_Project] NULL,
	[DeptNo] [dbo].[d_Dept] NULL,
	[Qty] [dbo].[d_Qty] NULL,
	[UOM] [dbo].[d_UOM] NULL,
	[UnitCost] [dbo].[d_Cost] NULL,
	[SubTotal] [dbo].[d_Money] NULL,
	[PrintOut] [dbo].[d_Boolean] NOT NULL,
	[SerialNoList] [ntext] NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Debit] [dbo].[d_AccNo] NULL,
	[Credit] [dbo].[d_AccNo] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[RPA_RCVDTL] ADD [FromDocType] [varchar](50) NULL
ALTER TABLE [dbo].[RPA_RCVDTL] ADD [FromDocNo] [dbo].[d_DocNo] NULL
ALTER TABLE [dbo].[RPA_RCVDTL] ADD [FromDocDtlKey] [bigint] NULL
ALTER TABLE [dbo].[RPA_RCVDTL] ADD [JEKey] [bigint] NULL
ALTER TABLE [dbo].[RPA_RCVDTL] ADD [JENo] [nvarchar](30) NULL
ALTER TABLE [dbo].[RPA_RCVDTL] ADD [WIPCost] [decimal](25, 8) NULL
ALTER TABLE [dbo].[RPA_RCVDTL] ADD [BOMCode] [dbo].[d_ItemCode] NULL
/****** Object:  Index [PK__RPA_RCVD__FB997988051A9206]    Script Date: 11/16/2019 15:37:12 ******/
ALTER TABLE [dbo].[RPA_RCVDTL] ADD PRIMARY KEY NONCLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_RCV](
	[DocKey] [bigint] NOT NULL,
	[DocNo] [dbo].[d_DocNo] NOT NULL,
	[DocType] [dbo].[d_DocType] NULL,
	[MachineCode] [nvarchar](30) NULL,
	[DocDate] [datetime] NOT NULL,
	[Description] [nvarchar](100) NULL,
	[Total] [dbo].[d_Money] NULL,
	[Note] [ntext] NULL,
	[Remark1] [dbo].[d_Remark] NULL,
	[Remark2] [dbo].[d_Remark] NULL,
	[Remark3] [dbo].[d_Remark] NULL,
	[Remark4] [dbo].[d_Remark] NULL,
	[PrintCount] [smallint] NOT NULL,
	[Cancelled] [dbo].[d_Boolean] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedUserID] [dbo].[d_UserID] NOT NULL,
	[CreatedTimeStamp] [datetime] NOT NULL,
	[CreatedUserID] [dbo].[d_UserID] NOT NULL,
	[ExternalLink] [ntext] NULL,
	[RefDocNo] [dbo].[d_DocNo] NULL,
	[LastUpdate] [int] NOT NULL,
	[CanSync] [dbo].[d_Boolean] NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	
	[JEKey] [bigint] NULL,
	[JENo] [nvarchar](30) NULL,
 CONSTRAINT [PK__RPA_RCV__434EFC1B5812E165] PRIMARY KEY NONCLUSTERED 
(
	[DocKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_RPA_RCV_DocNo] UNIQUE NONCLUSTERED 
(
	[DocNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_Overhead](
	[OverheadCode] [nvarchar](30) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[AccNo] [dbo].[d_AccNo] NULL,
	[AccDesc] [dbo].[d_AccDescription] NULL,
	[IsActive] [char](1) NOT NULL,
 CONSTRAINT [PK_RPA_OverHead] PRIMARY KEY CLUSTERED 
(
	[OverheadCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_Machine](
	[MachineCode] [nvarchar](30) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[Desc2] [nvarchar](100) NULL,
	[IsActive] [char](1) NOT NULL,
 CONSTRAINT [PK_RPA_Machine] PRIMARY KEY CLUSTERED 
(
	[MachineCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_ItemBOMOvd](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[OverheadCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[Amount] [dbo].[d_Money] NULL,
 CONSTRAINT [PK_RPA_ItemBOMOvd] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_ItemBOMDtl](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[BOMCode] [nvarchar](30) NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[UOM] [dbo].[d_UOM] NOT NULL,
	[Rate] [dbo].[d_Qty] NOT NULL,
	[Qty] [dbo].[d_Qty] NOT NULL,
	[UnitCost] [dbo].[d_Cost] NULL,
	[TotalCost] [dbo].[d_Money] NULL,
	[ItemBOMCode] [nvarchar](70) NULL,
	[ProductRatio] [dbo].[d_Money] NULL,
 CONSTRAINT [PK_RPA_ItemBOMDtl] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[RPA_ItemBOMBS](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[CostPercent] [dbo].[d_Money] NULL,
	[CostMax] [dbo].[d_Money] NULL,
	[UOM] [varchar](100) NULL,
	[Rate] [decimal](18, 4) NULL,
	[FOHCharge] [char](1) NULL,
 CONSTRAINT [PK_RPA_ItemBOMBS] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_ItemBOMAP](
	[DtlKey] [bigint] NOT NULL,
	[DocKey] [bigint] NOT NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[Description] [dbo].[d_ItemDescription] NULL,
	[CostPercent] [dbo].[d_Money] NULL,
 CONSTRAINT [PK_RPA_ItemBOMAP] PRIMARY KEY CLUSTERED 
(
	[DtlKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[RPA_ItemBOM](
	[DocKey] [bigint] NOT NULL,
	[BOMCode] [dbo].[d_ItemCode] NOT NULL,
	[ItemCode] [dbo].[d_ItemCode] NOT NULL,
	[UOM] [dbo].[d_UOM] NULL,
	[Description] [nvarchar](100) NULL,
	[Desc2] [nvarchar](100) NULL,
	[IsActive] [dbo].[d_Boolean] NOT NULL,
	[LastUpdate] [int] NOT NULL,
	[Qty] [dbo].[d_Qty] NOT NULL,
	[Cost] [dbo].[d_Cost] NOT NULL,
	[CostType] [nvarchar](20) NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedUserID] [dbo].[d_UserID] NOT NULL,
	[CreatedTimeStamp] [datetime] NOT NULL,
	[CreatedUserID] [dbo].[d_UserID] NOT NULL,
	[ProjNo] [nvarchar](10) NULL,
 CONSTRAINT [PK_RPA_ItemBOM] PRIMARY KEY CLUSTERED 
(
	[DocKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[RPA_CustomerLocation](
	[DebtorCode] [dbo].[d_AccNo] NOT NULL,
	[DebtorName] [dbo].[d_AccDescription] NULL,
	[Location] [dbo].[d_Location] NOT NULL,
	[LocationDesc] [nvarchar](80) NULL,
	[Description] [nvarchar](100) NULL,
	[PricePerDay] [dbo].[d_Price] NULL,
	[IsActive] [char](1) NOT NULL,
 CONSTRAINT [PK_RPA_CustomerLocation] PRIMARY KEY CLUSTERED 
(
	[DebtorCode] ASC,
	[Location] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPA_ItemBOMAP]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ItemBOMAP_Item] FOREIGN KEY([ItemCode])
REFERENCES [dbo].[Item] ([ItemCode])
GO
ALTER TABLE [dbo].[RPA_ItemBOMAP] CHECK CONSTRAINT [FK_RPA_ItemBOMAP_Item]
GO
ALTER TABLE [dbo].[RPA_ItemBOMAP]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ItemBOMAP_RPA_ItemBOM] FOREIGN KEY([DocKey])
REFERENCES [dbo].[RPA_ItemBOM] ([DocKey])
GO
ALTER TABLE [dbo].[RPA_ItemBOMAP] CHECK CONSTRAINT [FK_RPA_ItemBOMAP_RPA_ItemBOM]
GO
ALTER TABLE [dbo].[RPA_ItemBOMBS]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ItemBOMBS_Item] FOREIGN KEY([ItemCode])
REFERENCES [dbo].[Item] ([ItemCode])
GO

ALTER TABLE [dbo].[RPA_ItemBOMBS] CHECK CONSTRAINT [FK_RPA_ItemBOMBS_Item]
GO

ALTER TABLE [dbo].[RPA_ItemBOMBS]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ItemBOMBS_RPA_ItemBOM] FOREIGN KEY([DocKey])
REFERENCES [dbo].[RPA_ItemBOM] ([DocKey])
GO

ALTER TABLE [dbo].[RPA_ItemBOMBS] CHECK CONSTRAINT [FK_RPA_ItemBOMBS_RPA_ItemBOM]
GO
ALTER TABLE [dbo].[RPA_ItemBOMDtl]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ItemBOMDtl_Item] FOREIGN KEY([ItemCode])
REFERENCES [dbo].[Item] ([ItemCode])
GO

ALTER TABLE [dbo].[RPA_ItemBOMDtl] CHECK CONSTRAINT [FK_RPA_ItemBOMDtl_Item]
GO

ALTER TABLE [dbo].[RPA_ItemBOMDtl]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ItemBOMDtl_RPA_ItemBOM] FOREIGN KEY([DocKey])
REFERENCES [dbo].[RPA_ItemBOM] ([DocKey])
GO

ALTER TABLE [dbo].[RPA_ItemBOMDtl] CHECK CONSTRAINT [FK_RPA_ItemBOMDtl_RPA_ItemBOM]
GO
ALTER TABLE [dbo].[RPA_ItemBOMOvd]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ItemBOMOvd_RPA_Overhead] FOREIGN KEY([OverheadCode])
REFERENCES [dbo].[RPA_Overhead] ([OverheadCode])
GO
ALTER TABLE [dbo].[RPA_ItemBOMOvd] CHECK CONSTRAINT [FK_RPA_ItemBOMOvd_RPA_Overhead]
GO
ALTER TABLE [dbo].[RPA_RCV]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RCV_CreatedUserID] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[RPA_RCV] CHECK CONSTRAINT [FK_RPA_RCV_CreatedUserID]
GO
ALTER TABLE [dbo].[RPA_RCV]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RCV_LastModifiedUserID] FOREIGN KEY([LastModifiedUserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[RPA_RCV] CHECK CONSTRAINT [FK_RPA_RCV_LastModifiedUserID]
GO
ALTER TABLE [dbo].[RPA_RCV] ADD  CONSTRAINT [DF__RPA_RCV__Guid__5BE37249]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[RPA_RCVRM]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RCVRM_CreatedUserID] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[RPA_RCVRM] CHECK CONSTRAINT [FK_RPA_RCVRM_CreatedUserID]
GO
ALTER TABLE [dbo].[RPA_RCVRM]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RCVRM_LastModifiedUserID] FOREIGN KEY([LastModifiedUserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[RPA_RCVRM] CHECK CONSTRAINT [FK_RPA_RCVRM_LastModifiedUserID]
GO
ALTER TABLE [dbo].[RPA_RCVRM] ADD  CONSTRAINT [DF__RPA_RCVRM__Guid__5BE37249]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[RPA_RM]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RM_CreatedUserID] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[RPA_RM] CHECK CONSTRAINT [FK_RPA_RM_CreatedUserID]
GO
ALTER TABLE [dbo].[RPA_RM]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RM_LastModifiedUserID] FOREIGN KEY([LastModifiedUserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[RPA_RM] CHECK CONSTRAINT [FK_RPA_RM_LastModifiedUserID]
GO
ALTER TABLE [dbo].[RPA_RM]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RM_ReallocatePurchaseByProjectNo] FOREIGN KEY([ReallocatePurchaseByProjectNo])
REFERENCES [dbo].[Project] ([ProjNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RPA_RM] CHECK CONSTRAINT [FK_RPA_RM_ReallocatePurchaseByProjectNo]
GO
ALTER TABLE [dbo].[RPA_RM] ADD  DEFAULT ('F') FOR [ReallocatePurchaseByProject]
GO
ALTER TABLE [dbo].[RPA_RM] ADD  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[RPA_RMSubDtl]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RMSubDtl_RPA_RMDTL] FOREIGN KEY([DtlKey])
REFERENCES [dbo].[RPA_RMDTL] ([DtlKey])
GO
ALTER TABLE [dbo].[RPA_RMSubDtl] CHECK CONSTRAINT [FK_RPA_RMSubDtl_RPA_RMDTL]
GO

ALTER TABLE [dbo].[RPA_RMSubDtl] ADD  CONSTRAINT [DF_Table_1_BalQty]  DEFAULT ((0)) FOR [Qty]
GO
ALTER TABLE [dbo].[RPA_WO]  WITH CHECK ADD  CONSTRAINT [FK_RPA_WO_CreatedUserID] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[RPA_TransRawMaterial] ADD  CONSTRAINT [DF_RPA_TransRawMaterial_InputCost]  DEFAULT ((0)) FOR [InputCost]
GO
ALTER TABLE [dbo].[RPA_WO] CHECK CONSTRAINT [FK_RPA_WO_CreatedUserID]
GO
ALTER TABLE [dbo].[RPA_WO]  WITH CHECK ADD  CONSTRAINT [FK_RPA_WO_LastAOPModifiedUserID] FOREIGN KEY([LastAOPModifiedUserID])
REFERENCES [dbo].[Users] ([UserID])
GO

ALTER TABLE [dbo].[RPA_WO] CHECK CONSTRAINT [FK_RPA_WO_LastAOPModifiedUserID]
GO

ALTER TABLE [dbo].[RPA_WO]  WITH CHECK ADD  CONSTRAINT [FK_RPA_WO_LastModifiedUserID] FOREIGN KEY([LastModifiedUserID])
REFERENCES [dbo].[Users] ([UserID])
GO

ALTER TABLE [dbo].[RPA_WO] CHECK CONSTRAINT [FK_RPA_WO_LastModifiedUserID]
GO

ALTER TABLE [dbo].[RPA_WO]  WITH CHECK ADD  CONSTRAINT [FK_RPA_WO_Location] FOREIGN KEY([Location])
REFERENCES [dbo].[Location] ([Location])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[RPA_WO] CHECK CONSTRAINT [FK_RPA_WO_Location]
GO

ALTER TABLE [dbo].[RPA_WO] ADD  CONSTRAINT [DF__RPA_WO__Guid__2F10CBD2]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[RPA_WOOvd]  WITH CHECK ADD  CONSTRAINT [FK_RPA_WOOvd_RPA_Overhead] FOREIGN KEY([OverheadCode])
REFERENCES [dbo].[RPA_Overhead] ([OverheadCode])
GO

ALTER TABLE [dbo].[RPA_WOOvd] CHECK CONSTRAINT [FK_RPA_WOOvd_RPA_Overhead]
GO

ALTER TABLE [dbo].[RPA_WOOvd] ADD  CONSTRAINT [DF_RPA_WOOvd_Guid]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[RPA_WODtl]  WITH CHECK ADD  CONSTRAINT [FK_RPA_WODtl_RPA_WO] FOREIGN KEY([DocKey])
REFERENCES [dbo].[RPA_WO] ([DocKey])
GO

ALTER TABLE [dbo].[RPA_WODtl] CHECK CONSTRAINT [FK_RPA_WODtl_RPA_WO]
GO

ALTER TABLE [dbo].[RPA_WODtl] ADD  CONSTRAINT [DF_RPA_WODtl_StockReceived]  DEFAULT ('F') FOR [StockReceived]
GO

ALTER TABLE [dbo].[RPA_WODtl] ADD  CONSTRAINT [DF_RPA_WODtl_Guid]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[RPA_WOBS]  WITH CHECK ADD  CONSTRAINT [FK_RPA_WOBS_RPA_WO] FOREIGN KEY([DocKey])
REFERENCES [dbo].[RPA_WO] ([DocKey])
GO

ALTER TABLE [dbo].[RPA_WOBS] CHECK CONSTRAINT [FK_RPA_WOBS_RPA_WO]
GO

ALTER TABLE [dbo].[RPA_WOBS] ADD  CONSTRAINT [DF_RPA_WOBS_Guid]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[RPA_WOAP]  WITH CHECK ADD  CONSTRAINT [FK_RPA_WOAP_RPA_WO] FOREIGN KEY([DocKey])
REFERENCES [dbo].[RPA_WO] ([DocKey])
GO

ALTER TABLE [dbo].[RPA_WOAP] CHECK CONSTRAINT [FK_RPA_WOAP_RPA_WO]
GO

ALTER TABLE [dbo].[RPA_WOAP] ADD  CONSTRAINT [DF_RPA_WOAP_Guid]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[RPA_RMDTL]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RMDTL_DeptNo] FOREIGN KEY([DeptNo])
REFERENCES [dbo].[Dept] ([DeptNo])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[RPA_RMDTL] CHECK CONSTRAINT [FK_RPA_RMDTL_DeptNo]
GO

ALTER TABLE [dbo].[RPA_RMDTL]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RMDTL_ItemBatch] FOREIGN KEY([ItemCode], [BatchNo])
REFERENCES [dbo].[ItemBatch] ([ItemCode], [BatchNo])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[RPA_RMDTL] CHECK CONSTRAINT [FK_RPA_RMDTL_ItemBatch]
GO

ALTER TABLE [dbo].[RPA_RMDTL]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RMDTL_ItemCode] FOREIGN KEY([ItemCode])
REFERENCES [dbo].[Item] ([ItemCode])
GO

ALTER TABLE [dbo].[RPA_RMDTL] CHECK CONSTRAINT [FK_RPA_RMDTL_ItemCode]
GO

ALTER TABLE [dbo].[RPA_RMDTL]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RMDTL_ItemUOM] FOREIGN KEY([ItemCode], [UOM])
REFERENCES [dbo].[ItemUOM] ([ItemCode], [UOM])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[RPA_RMDTL] CHECK CONSTRAINT [FK_RPA_RMDTL_ItemUOM]
GO

ALTER TABLE [dbo].[RPA_RMDTL]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RMDTL_Location] FOREIGN KEY([Location])
REFERENCES [dbo].[Location] ([Location])
GO

ALTER TABLE [dbo].[RPA_RMDTL] CHECK CONSTRAINT [FK_RPA_RMDTL_Location]
GO

ALTER TABLE [dbo].[RPA_RMDTL]  WITH CHECK ADD  CONSTRAINT [FK_RPA_RMDTL_ProjNo] FOREIGN KEY([ProjNo])
REFERENCES [dbo].[Project] ([ProjNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RPA_RMDTL] CHECK CONSTRAINT [FK_RPA_RMDTL_ProjNo]
GO
ALTER TABLE [dbo].[RPA_RMDTL] ADD  DEFAULT (newid()) FOR [Guid]
GO
CREATE view [dbo].[vRPA_BatchNoWO]
AS
SELECT a.DocNo,a.DocDate,a.DocKey, c.ItemCode,c.Description as ItemDescription,d.UOM,d.BatchNo,e.Description,e.ManufacturedDate,e.ExpiryDate,e.BalQty FROM RPA_WO a inner join RPA_WOProduct b on a.DocKey=b.DocKey inner join RPA_WODtl c on b.DtlKey=c.FromDocDtlKey inner join ItemBatchBalQty d on c.ItemCode=d.ItemCode and d.UOM=c.UOM inner join ItemBatch e on e.ItemCode=d.ItemCode where d.BalQty>0
GO
CREATE VIEW [dbo].[vRPA_ItemWithBOM]
AS
SELECT A.ItemCode +' '+isnull(C.BOMCode,'') AS ItemBOMCode, A.ItemCode,C.BOMCode,C.Desc2 as BOMDesc, A.Description, A.Desc2, SUM(B.Rate*B.BalQty) AS BalQty,
 SUM(ISNULL(B.Rate*B.CSGNQty,0)) AS CSGNQty, SUM(B.Rate*(B.BalQty-ISNULL(B.CSGNQty,0))) AS AfterCSGNQty FROM Item A LEFT OUTER  JOIN
  RPA_ItemBOM C ON A.ItemCode = C.ItemCode LEFT OUTER JOIN ItemUOM B ON(A.ItemCode = B.ItemCode) WHERE A.IsActive = 'T' 
  GROUP BY A.ItemCode,C.BOMCode,C.Desc2, A.Description, A.Desc2, A.IsActive
GO
CREATE VIEW [dbo].[vRPA_StockReceive] AS
SELECT A.DocKey, A.DocNo, A.DocDate, A.Description, A.Total, A.Note,A.DocType,
A.Remark1, A.Remark2, A.Remark3, A.Remark4, A.PrintCount, A.Cancelled,
A.LastModified, A.LastModifiedUserID, B.UserName AS LastModifiedUserName,
A.CreatedTimeStamp, A.CreatedUserID, C.UserName AS CreatedUserName,
A.ExternalLink, A.RefDocNo, A.CanSync
,A.MachineCode, D.Description as MachineName,A.JENo,A.JEKey
FROM RPA_RCV A
LEFT OUTER JOIN Users B ON (A.LastModifiedUserID=B.UserID)
LEFT OUTER JOIN Users C ON (A.CreatedUserID=C.UserID)
LEFT OUTER JOIN RPA_Machine D ON (A.MachineCode=D.MachineCode)

GO
CREATE VIEW [dbo].[vRPA_StockReceiveDetail] AS
SELECT A.DtlKey, A.DocKey, A.Seq, A.Numbering,A.FromDocNo,A.FromDocType,A.FromDocDtlKey,
B.*,
C.*,
D.*,
A.Description, A.FurtherDescription,
E.*,
F.*,
G.*,
A.Qty, A.UnitCost, A.SubTotal, A.PrintOut, A.SerialNoList,
H.BalQty AS LocationBalQty, I.BalQty AS BatchBalQty
, A.Debit, A.Credit
FROM RPA_RCVDTL A
LEFT OUTER JOIN vItem B ON (A.ItemCode=B.ItemCode)
LEFT OUTER JOIN vItemBatch C ON (A.ItemCode=C.BatchItemCode AND A.BatchNo=C.BatchNo)
LEFT OUTER JOIN vLocation D ON (A.Location=D.Location)
LEFT OUTER JOIN vProject E ON (A.ProjNo=E.ProjNo)
LEFT OUTER JOIN vDept F ON (A.DeptNo=F.DeptNo)
LEFT OUTER JOIN vItemUOM G ON (A.ItemCode=G.UOMItemCode AND A.UOM=G.UOM)
LEFT OUTER JOIN ItemBalQty H ON (A.ItemCode=H.ItemCode AND A.UOM=H.UOM AND A.Location=H.Location)
LEFT OUTER JOIN ItemBatchBalQty I ON (A.ItemCode=I.ItemCode AND A.UOM=I.UOM AND A.Location=I.Location AND A.BatchNo=I.BatchNo)

GO
CREATE VIEW [dbo].[vRPA_StockReceiveRM] AS
SELECT A.DocKey, A.DocNo, A.DocDate, A.Description, A.Total, A.Note, A.BatchNo,A.DocType,
A.Remark1, A.Remark2, A.Remark3, A.Remark4, A.PrintCount, A.Cancelled,
A.LastModified, A.LastModifiedUserID, B.UserName AS LastModifiedUserName,
A.CreatedTimeStamp, A.CreatedUserID, C.UserName AS CreatedUserName,
A.ExternalLink, A.RefDocNo, A.CanSync
,A.JENo,A.JEKey
FROM RPA_RCVRM A
LEFT OUTER JOIN Users B ON (A.LastModifiedUserID=B.UserID)
LEFT OUTER JOIN Users C ON (A.CreatedUserID=C.UserID)

GO

CREATE VIEW [dbo].[vRPA_StockReceiveRMDetail] AS
SELECT A.DtlKey, A.DocKey, A.Seq, A.Numbering,A.FromDocNo,A.FromDocType,A.FromDocDtlKey,
B.*,
C.*,
D.*,
A.Description, A.FurtherDescription,
E.*,
F.*,
G.*,
A.Qty, A.UnitCost, A.SubTotal, A.PrintOut, A.SerialNoList,
H.BalQty AS LocationBalQty, I.BalQty AS BatchBalQty
, A.Debit, A.Credit
FROM RPA_RCVRMDTL A
LEFT OUTER JOIN vItem B ON (A.ItemCode=B.ItemCode)
LEFT OUTER JOIN vItemBatch C ON (A.ItemCode=C.BatchItemCode AND A.BatchNo=C.BatchNo)
LEFT OUTER JOIN vLocation D ON (A.Location=D.Location)
LEFT OUTER JOIN vProject E ON (A.ProjNo=E.ProjNo)
LEFT OUTER JOIN vDept F ON (A.DeptNo=F.DeptNo)
LEFT OUTER JOIN vItemUOM G ON (A.ItemCode=G.UOMItemCode AND A.UOM=G.UOM)
LEFT OUTER JOIN ItemBalQty H ON (A.ItemCode=H.ItemCode AND A.UOM=H.UOM AND A.Location=H.Location)
LEFT OUTER JOIN ItemBatchBalQty I ON (A.ItemCode=I.ItemCode AND A.UOM=I.UOM AND A.Location=I.Location AND A.BatchNo=I.BatchNo)

GO
CREATE view [dbo].[vRPA_TransWOtoRCV]
AS
select * from(
select  b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Others Product' as DocType,MachineCode,'' AS BOMCode, b.ItemCode,b.Description
from RPA_WO a with(nolock) inner join RPA_WOBS b with(nolock) on a.DocKey=b.DocKey where a.status in ('InProcess','Assembly') 
and Cancelled='F' 
union all
select b.DtlKey as Code,a.DocNo,a.Status,a.DocDate,'Main Product' as DocType,MachineCode,B.BOMCode, b.ItemCode,b.Description
from RPA_WO a with(nolock) inner join RPA_WOProduct b with(nolock) on a.DocKey=b.DocKey inner join Item c with(nolock) on b.ItemCode=c.ItemCode where b.status in ('InProcess','Assembly') 
and a.Cancelled='F' and ISNULL(b.Cancelled,'F')='F'
) result
GO
CREATE view [dbo].[vRPA_WorkOrder]
as
select a.*,b.Description as MachineDesc,
c.CostType,
c.Cost,
d.ActQty as ActQty2,
f.Description as ItemDescription
 from RPA_WO a with(nolock)
 inner join RPA_WOProduct a1 on a1.DocKey=a.DocKey
left outer join RPA_Machine b on a.MachineCode=b.MachineCode
left outer join RPA_ItemBOM c with(nolock) on a1.BOMCode=c.BOMCode
left outer join Item f with(nolock) on a1.ItemCode=f.ItemCode
Left outer join (
select FromDocDtlKey,SUM(ActQty) as ActQty from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
AND FromDocType in ('WO','Main Product')
GROUP BY rcvdtl.FromDocDtlKey) subb group by subb.FromDocDtlKey) d on a.DocKey=d.FromDocDtlKey

GO

CREATE view [dbo].[vRPA_WorkOrderAP]
as
select * from RPA_WOAP a with(nolock) 
left outer join 
(select FromDocDtlKey,SUM(ActQty) as ActQty from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
AND FromDocType in ('AP','Alternatif Product')
GROUP BY rcvdtl.FromDocDtlKey
) subb group by subb.FromDocDtlKey) b on a.DtlKey=b.FromDocDtlKey

GO

create view [dbo].[vRPA_WorkOrderBS]
as
select a.*,b.ActQty,b.ActUnitCost from RPA_WOBS a with(nolock) 
left outer join 
(select FromDocDtlKey,SUM(ActQty) as ActQty,SUM(ActUnitCost) as ActUnitCost from (
select rcvdtl.FromDocDtlKey,
SUM(COALESCE(rcvdtl.Qty,0)) as ActQty,AVG(COALESCE(rcvdtl.UnitCost,0)) as ActUnitCost  from RPA_RCVDTL rcvdtl with(nolock) 
inner join RPA_RCV rcv  with(nolock) on rcv.DocKey=rcvdtl.DocKey 
where Cancelled='F' 
AND FromDocType in ('BS','Product sortiran')
GROUP BY rcvdtl.FromDocDtlKey) subb group by subb.FromDocDtlKey) b on a.DtlKey=b.FromDocDtlKey

GO
create view [dbo].[vRPA_WorkOrderDetail]
as
SELECT a.*,b.ActQty,b.ActUnitCost,b.OrgActQty,b.ReturQty FROM RPA_WODtl a with(nolock) 
left outer join(
select FromDocNo,FromDocDtlKey,FromDocType,SUM(COALESCE(rmdtl.Qty,0)) as OrgActQty,
SUM(COALESCE(rmdtl.TransferedQty,0)) ReturQty,
SUM(COALESCE(rmdtl.Qty,0)-COALESCE(rmdtl.TransferedQty,0)) ActQty,
AVG(COALESCE(rmdtl.UnitCost,0)) as ActUnitCost  from RPA_RMDTL rmdtl with(nolock) 
inner join RPA_RM rm  with(nolock) on rmdtl.DocKey=rm.DocKey where Cancelled='F' GROUP BY FromDocNo,FromDocDtlKey,FromDocType)
b  on a.DtlKey=b.FromDocDtlKey and b.FromDocType='WO'

GO
create view [dbo].[vRPA_WorkOrderOvd]
as
select * from RPA_WOOvd  with(nolock)


GO
CREATE PROCEDURE [dbo].[RPA_BOM_Hierarchy_p](@BOMCode nvarchar(100))
as
WITH Tree
AS (
SELECT
    a.BOMCode id,
    a.ItemCode,
    a.BOMCode as parent,
    0 AS Level,
    a.BOMCode AS Root,
    CAST(a.BOMCode AS VARCHAR(MAX)
    ) AS Sort
from 
RPA_ItemBOM a with(NOLOCK) inner join RPA_ItemBOMDtl b  with(NOLOCK) on a.DocKey=b.DocKey where a.BOMCode=@BOMCode
UNION ALL
SELECT 
    st.ChildBOMCode id,
    st.ItemCode2 as ItemCode,
    st.BOMCode as parent,
    Level + 1 AS Level,
    st.BOMCode  AS Root,
   -- st.hasRights AS HasRights,
    uh.sort + CASE 1 WHEN 0 THEN '' ELSE '/' + CAST(st.ChildBOMCode AS VARCHAR(20)) END AS Sort
FROM (select a.*,b.BOMCode as ChildBOMCode,B.ItemCode AS ItemCode2 from RPA_ItemBOM a with(NOLOCK) inner join RPA_ItemBOMDtl b  with(NOLOCK) on a.DocKey=b.DocKey) AS st
    JOIN Tree uh ON uh.id = st.BOMCode    
)
SELECT distinct id,parent,ItemCode,Level,Root FROM Tree AS t   where Sort is not null 

GO

INSERT INTO RPA_Settings
VALUES(0,0,1,'WO-<00000>',1,'RM-<00000>',1,'FG-<00000>',1,'RR-<00000>',1,'FR-<000000>',NULL,'T')
GO
INSERT INTO [dbo].[DocNoFormat]
           ([Name]
           ,[DocType]
           ,[NextNumber]
           ,[Format]
           ,[Sample]
           ,[IsDefault]
           ,[OneMonthOneSet]
           ,[MaxNumber])
     VALUES
           ('RPA_WO'
           ,'SW'
           ,1
           ,'WO-<00000>'
           ,'WO-000001'
           ,'T'
           ,'F'
           ,NULL)
GO
INSERT INTO [dbo].[DocNoFormat]
           ([Name]
           ,[DocType]
           ,[NextNumber]
           ,[Format]
           ,[Sample]
           ,[IsDefault]
           ,[OneMonthOneSet]
           ,[MaxNumber])
     VALUES
           ('RPA_RM'
           ,'RM'
           ,1
           ,'RM-<00000>'
           ,'RM-000001'
           ,'T'
           ,'F'
           ,NULL)
GO
INSERT INTO [dbo].[DocNoFormat]
           ([Name]
           ,[DocType]
           ,[NextNumber]
           ,[Format]
           ,[Sample]
           ,[IsDefault]
           ,[OneMonthOneSet]
           ,[MaxNumber])
     VALUES
           ('RPA_RCV'
           ,'RV'
           ,1
           ,'FG-<00000>'
           ,'FG-000001'
           ,'T'
           ,'F'
           ,NULL)
GO
INSERT INTO [dbo].[DocNoFormat]
           ([Name]
           ,[DocType]
           ,[NextNumber]
           ,[Format]
           ,[Sample]
           ,[IsDefault]
           ,[OneMonthOneSet]
           ,[MaxNumber])
     VALUES
           ('RPA_RCVRM'
           ,'RR'
           ,1
           ,'RR-<00000>'
           ,'RR-000001'
           ,'T'
           ,'F'
           ,NULL)
           GO
INSERT INTO [dbo].[DocNoFormat]
           ([Name]
           ,[DocType]
           ,[NextNumber]
           ,[Format]
           ,[Sample]
           ,[IsDefault]
           ,[OneMonthOneSet]
           ,[MaxNumber])
     VALUES
           ('RPA_RCVFR'
           ,'FR'
           ,1
           ,'FR-<000000>'
           ,'FR-000001'
           ,'T'
           ,'F'
           ,NULL)
 
