--_______________________________________Query Tabungan pedagang_______________________________________________________________________________

SELECT  D.Description as PIDesc, A.AccNo, B.Description AS AccDescription, B.Desc2 AS AccDesc2, A.DEAccNo, C.Description AS DEAccDescription, C.Desc2 AS DEAccDesc2, A.JournalType, A.CurrencyCode, A.CurrencyRate, A.ToHomeRate, Sum(A.OrgDR) AS OrgDR, Sum(A.OrgCR) AS OrgCR, Sum(A.DR) AS DR, Sum(A.CR) AS CR, SUM(A.DR-A.CR) AS Balance, SUM(A.HomeDR) AS HomeDR, SUM(A.HomeCR) AS HomeCR,
SUM(A.HomeDR-A.HomeCR) AS HomeBalance, A.TransDate, A.Description, A.RefNo1, A.RefNo2,   A.UserID, A.SourceType, A.SourceKey,
SUM(Ekor) as TotalEkor,SUM(Kilo) as TotalKilo,SUM(Sub) as TotalSub,SUM(Tabungan) as TotalTabungan
FROM (
select *,
 subresultRI.Sub/Kilo as Tabungan
 from(
SELECT *,(SELECT Sum(IVDTL.UDF_Tekor) as Tekor FROM IVDTL INNER JOIN IV ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' AND IV.DocNo =GLDTL.RefNo1) as Ekor,
(SELECT Sum(IVDTL.Qty) as Qty FROM IVDTL INNER JOIN IV ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' AND IV.DocNo =GLDTL.RefNo1) as Kilo,
(SELECT Sum(IVDTL.LocalSubTotal) as localsubtotal FROM IVDTL INNER JOIN IV ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' AND IV.DocNo =GLDTL.RefNo1) Sub
from GLDTL
WHERE GLDTL.SourceType='RI') subresultRI
union all
select *,
 subresultPB.Sub/Kilo as Tabungan
 from(
SELECT *,(SELECT Sum(PIDTL.UDF_Tekor) as Tekor FROM PIDTL INNER JOIN PI ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN' AND PI.DocNo =GLDTL.RefNo1) as Ekor,
(SELECT Sum(PIDTL.Qty) as Qty FROM PIDTL INNER JOIN PI ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN' AND PI.DocNo =GLDTL.RefNo1) as Kilo,
(SELECT Sum(PIDTL.LocalSubTotal) as localsubtotal FROM PIDTL INNER JOIN PI ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN' AND PI.DocNo =GLDTL.RefNo1) Sub
from GLDTL
WHERE GLDTL.SourceType='PB') subresultPB) A LEFT OUTER JOIN GLMast B ON (A.AccNo=B.AccNo)
LEFT OUTER JOIN GLMast C ON (A.DEAccNo=C.AccNo)
LEFT OUTER JOIN GLDTL D ON  A.GLDTLKey=D.GLDTLKey
WHERE A.TransDate>=GETDATE()-10000 AND A.TransDate<=GETDATE()
AND (A.AccNo NOT IN (SELECT DISTINCT ParentAccNo FROM GLMast WHERE ParentAccNo IS NOT NULL))
GROUP BY A.AccNo, B.Description, B.Desc2, A.DEAccNo, C.Description, C.Desc2, A.JournalType, A.CurrencyCode, A.CurrencyRate, A.ToHomeRate, A.TransDate, A.Description, A.RefNo1, A.RefNo2,   A.UserID, A.SourceType, A.SourceKey,D.Description
ORDER BY A.AccNo, A.TransDate, A.RefNo1
--____________________________________________________________________________________________________________________________


/*
SELECT  D.Description as PIDesc, A.AccNo, B.Description AS AccDescription, B.Desc2 AS AccDesc2, A.DEAccNo, C.Description AS DEAccDescription, C.Desc2 AS DEAccDesc2, A.JournalType, A.CurrencyCode, A.CurrencyRate, A.ToHomeRate, Sum(A.OrgDR) AS OrgDR, Sum(A.OrgCR) AS OrgCR, Sum(A.DR) AS DR, Sum(A.CR) AS CR, SUM(A.DR-A.CR) AS Balance, SUM(A.HomeDR) AS HomeDR, SUM(A.HomeCR) AS HomeCR,
SUM(A.HomeDR-A.HomeCR) AS HomeBalance, A.TransDate, A.Description, A.RefNo1, A.RefNo2,   A.UserID, A.SourceType, A.SourceKey,
(SELECT Sum(IVDTL.UDF_Tekor) as Tekor FROM IVDTL INNER JOIN IV ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' AND IV.DocNo =A.RefNo1)
FROM GLDTL A LEFT OUTER JOIN GLMast B ON (A.AccNo=B.AccNo)
LEFT OUTER JOIN GLMast C ON (A.DEAccNo=C.AccNo)
LEFT OUTER JOIN GLDTL D ON  A.GLDTLKey=D.GLDTLKey
WHERE A.TransDate>=GETDATE()-10000 AND A.TransDate<=GETDATE()
AND (A.AccNo NOT IN (SELECT DISTINCT ParentAccNo FROM GLMast WHERE ParentAccNo IS NOT NULL))
AND A.SourceType='RI'
GROUP BY A.AccNo, B.Description, B.Desc2, A.DEAccNo, C.Description, C.Desc2, A.JournalType, A.CurrencyCode, A.CurrencyRate, A.ToHomeRate, A.TransDate, A.Description, A.RefNo1, A.RefNo2,   A.UserID, A.SourceType, A.SourceKey,D.Description

select * from(
select *,
 subresultRI.Sub/Kilo as Tabungan
 from(
SELECT *,(SELECT Sum(IVDTL.UDF_Tekor) as Tekor FROM IVDTL INNER JOIN IV ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' AND IV.DocNo =GLDTL.RefNo1) as Ekor,
(SELECT Sum(IVDTL.Qty) as Qty FROM IVDTL INNER JOIN IV ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' AND IV.DocNo =GLDTL.RefNo1) as Kilo,
(SELECT Sum(IVDTL.LocalSubTotal) as localsubtotal FROM IVDTL INNER JOIN IV ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' AND IV.DocNo =GLDTL.RefNo1) Sub
from GLDTL
WHERE GLDTL.SourceType='RI') subresultRI
union all
select *,
 subresultPB.Sub/Kilo as Tabungan
 from(
SELECT *,(SELECT Sum(PIDTL.UDF_Tekor) as Tekor FROM PIDTL INNER JOIN PI ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN' AND PI.DocNo =GLDTL.RefNo1) as Ekor,
(SELECT Sum(PIDTL.Qty) as Qty FROM PIDTL INNER JOIN PI ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN' AND PI.DocNo =GLDTL.RefNo1) as Kilo,
(SELECT Sum(PIDTL.LocalSubTotal) as localsubtotal FROM PIDTL INNER JOIN PI ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN' AND PI.DocNo =GLDTL.RefNo1) Sub
from GLDTL
WHERE GLDTL.SourceType='PB') subresultPB)


SELECT Sum(IVDTL.UDF_Tekor) as Tekor FROM IVDTL INNER JOIN IV ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' AND IV.DocNo =


SELECT DISTINCT SourceType from GLDTL
*/


--_______________________________________Query Tabungan Pedagang/Debtor/Customer

Select AccNo,JournalType,SourceType,RefNo1,DOcNo,DocDate,Ekor,Kilo,ISNULL(HomeCR,0) AS Tabungan, ISNULL(HomeCR,0)/Kilo AS Tab_Per_Kilo  From GLDTL A with(Nolock) Inner Join (SELECT Sum(IVDTL.UDF_Tekor) as Ekor,Sum(IVDTL.Qty) as Kilo,DocNo,DocDate FROM IVDTL with(Nolock) INNER JOIN IV with(Nolock) ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' 
Group by DocNo,DocDate) B
ON A.RefNo1=B.DocNo 
Where A.AccNo='200-0002'
AND B.DocNo='I-1401-025283'

-------------------Query Tabungan Supplier

Select AccNo,JournalType,SourceType,RefNo1,DOcNo,DocDate,Ekor,Kilo,ISNULL(HomeDR,0) AS Tabungan, ISNULL(HomeDR,0)/Kilo AS Tab_Per_Kilo  From GLDTL A with(Nolock) Inner Join (SELECT Sum(PIDTL.UDF_Tekor) as Ekor,Sum(PIDTL.Qty) as Kilo,DocNo,DocDate FROM PIDTL with(Nolock) INNER JOIN [PI]with(Nolock) ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN'
Group by DocNo,DocDate) B
ON A.RefNo1=B.DocNo 
Where A.AccNo='110-0002'
AND B.DocNo='PI-1401-002137'


Select AccNo,JournalType,SourceType,RefNo1,DOcNo,DocDate,Ekor,Kilo,ISNULL(HomeCR,0) AS Tabungan, ISNULL(HomeCR,0)/Kilo AS Tab_Per_Kilo  From GLDTL A with(Nolock) Inner Join (SELECT Sum(IVDTL.UDF_Tekor) as Ekor,Sum(IVDTL.Qty) as Kilo,DocNo,DocDate FROM IVDTL with(Nolock) INNER JOIN IV with(Nolock) ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' 
Group by DocNo,DocDate) B
ON A.RefNo1=B.DocNo 
union all
Select AccNo,JournalType,SourceType,RefNo1,DOcNo,DocDate,Ekor,Kilo,ISNULL(HomeDR,0) AS Tabungan, ISNULL(HomeDR,0)/Kilo AS Tab_Per_Kilo  From GLDTL A with(Nolock) Inner Join (SELECT Sum(PIDTL.UDF_Tekor) as Ekor,Sum(PIDTL.Qty) as Kilo,DocNo,DocDate FROM PIDTL with(Nolock) INNER JOIN [PI]with(Nolock) ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN'
Group by DocNo,DocDate) B
ON A.RefNo1=B.DocNo 



select HOMEDR-HOMECR from PBalance where projno='AAS' AND ACCNO=(select AgentSaveAcc from BG_SettingBP)