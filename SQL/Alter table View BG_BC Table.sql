ALTER VIEW [dbo].[vBG_ProsesBonusTangkap] AS
SELECT	*
FROM BG_BC A
GO
ALTER VIEW [dbo].[vBG_ProsesBonusTangkapDetail]
AS
SELECT   A.*, B.DebtorCompanyName, 
                      B.DebtorDesc2, B.DebtorAddress1, B.DebtorAddress2, B.DebtorAddress3, B.DebtorAddress4, B.DebtorDeliverAddress1, B.DebtorDeliverAddress2, 
                      B.DebtorDeliverAddress3, B.DebtorDeliverAddress4, B.DebtorAttention, B.DebtorPhone1, B.DebtorPhone2, B.DebtorFax1, B.DebtorFax2, B.DebtorTaxType, 
                      B.DebtorTaxRegisterNo, B.DebtorExemptNo, B.DebtorExemptExpiryDate, B.DebtorNote, B.DebtorPriceCategory, B.DebtorStatementType, B.DebtorAgingOn, 
                      B.DebtorRegisterNo, B.DebtorNatureOfBusiness, B.DebtorWebURL, B.DebtorEmailAddress, B.DebtorDisplayTerm, B.DebtorContactInfo, B.DebtorCreditLimit, 
                      B.DebtorOverdueLimit, B.DebtorCurrencyCode, B.DebtorPostCode, B.DebtorDeliverPostCode, B.DebtorLastModified, B.DebtorLastModifiedUserID, 
                      B.DebtorCreatedTimestamp, B.DebtorCreatedUserID, B.DebtorAreaCode, B.DebtorAreaDescription, B.DebtorAreaDesc2, B.DebtorType, B.DebtorTypeDescription, 
                      B.DebtorTypeDesc2, B.DebtorSalesAgent, B.DebtorSalesAgentDescription, B.DebtorSalesAgentDesc2, B.DebtorSalesAgentSignature, B.DebtorParentAccNo, 
                      B.DebtorUDF_ProjNo, B.DebtorUDF_TABKG, 
                      C.Name as CollectorName2,C.ProjNo AS CollectorProjNo
FROM         dbo.BG_BCDtl AS A LEFT OUTER JOIN
                      dbo.vDebtor AS B ON A.DebtorCode = B.DebtorCode
				LEFT OUTER JOIN	 dbo.BG_CollectorBP AS C ON A.IDCollector = C.IDCollector