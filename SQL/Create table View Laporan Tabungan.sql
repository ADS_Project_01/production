USE [AED_NEW_BG]
GO

/****** Object:  View [dbo].[vBG_LaporanTabungan]    Script Date: 12/06/2017 07:37:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vBG_LaporanTabungan] as
select ROW_NUMBER() OVER(ORDER BY DocNo ASC) AS DocKey,result.*,GLMast.Description,GLMast.Desc2 from (
Select 'Debtor' DocType,AccNo,JournalType,SourceType,RefNo1,DocNo,DocDate,Ekor,Kilo,ISNULL(HomeCR,0) AS Tabungan, ISNULL(HomeCR,0)/Kilo AS Tab_Per_Kilo  From GLDTL A with(Nolock) Inner Join (SELECT Sum(IVDTL.UDF_Tekor) as Ekor,Sum(IVDTL.Qty) as Kilo,DocNo,DocDate FROM IVDTL with(Nolock) INNER JOIN IV with(Nolock) ON IVDTL.DocKey=IV.DocKey WHERE IVDTL.ItemCode <> 'TABUNGAN' 
Group by DocNo,DocDate) B
ON A.RefNo1=B.DocNo 
union all
Select 'Creditor' DocType,AccNo,JournalType,SourceType,RefNo1,DocNo,DocDate,Ekor,Kilo,ISNULL(HomeDR,0) AS Tabungan, ISNULL(HomeDR,0)/Kilo AS Tab_Per_Kilo  From GLDTL A with(Nolock) Inner Join (SELECT Sum(PIDTL.UDF_Tekor) as Ekor,Sum(PIDTL.Qty) as Kilo,DocNo,DocDate FROM PIDTL with(Nolock) INNER JOIN [PI]with(Nolock) ON PIDTL.DocKey=PI.DocKey WHERE PIDTL.ItemCode <> 'TABUNGAN'
Group by DocNo,DocDate) B
ON A.RefNo1=B.DocNo) result 
inner join GLMast on result.AccNo=GLMast.AccNo
GO


