using System;

namespace Production
{
	/// <summary>
	/// AccessRightConst class defines a lsit of command constant value for use in BCE.DBUtils.AccessRight.IsAccessible method.
	/// </summary>
	public class AccessRightConst
	{
        public const string RPA = "RPA";
        public const string RPA_GEN = "RPA_GEN";
        /// <summary>
        /// 
        /// General Setting
        /// </summary>
        public const string RPA_TOOLS = "RPA_TOOLS";
        /// <summary>
        /// 
        /// General Setting Show
        /// </summary>
        public const string RPA_TOOLS_RECALCULATE = "RPA_TOOLS_RECALCULATE";
        /// <summary>
        /// 
        /// General Setting Open
        /// </summary>
        public const string RPA_TOOLS_RECALCULATE_SHOW= "RPA_TOOLS_RECALCULATE_SHOW";
        /// <summary>
        /// 
        /// General Setting Open
        /// </summary>
        public const string RPA_TOOLS_RECALCULATE_OPEN = "RPA_TOOLS_RECALCULATE_OPEN";

        /// <summary>
        /// 
        /// General Setting
        /// </summary>
        public const string RPA_REPORTS = "RPA_REPORTS";
        /// <summary>
        /// 
        /// General Setting Show
        /// </summary>
        public const string RPA_REPORTS_STOCKCARDWIP = "RPA_REPORTS_STOCKCARDWIP";
        /// <summary>
        /// 
        /// General Setting Open
        /// </summary>
        public const string RPA_REPORTS_STOCKCARDWIP_SHOW = "RPA_REPORTS_STOCKCARDWIP_SHOW";
        /// <summary>
        /// 
        /// General Setting Open
        /// </summary>
        public const string RPA_REPORTS_STOCKCARDWIP_OPEN = "RPA_REPORTS_STOCKCARDWIP_OPEN";

        /// <summary>
        /// 
        /// General Setting
        /// </summary>
        public const string RPA_SETTINGS = "RPA_SETTINGS";
        /// <summary>
        /// 
        /// General Setting Show
        /// </summary>
        public const string RPA_SETTINGS_SHOW = "RPA_SETTINGS_SHOW";
        /// <summary>
        /// 
        /// General Setting Open
        /// </summary>
        public const string RPA_SETTINGS_OPEN = "RPA_SETTINGS_OPEN";
        /// <summary>
        /// 
        /// Machine Maintenance
        /// </summary>
        public const string RPA_GEN_MACHINE = "RPA_GEN_MACHINE";

		/// <summary>
		/// Delete Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE_DELETE = "RPA_GEN_MACHINE_DELETE";

        /// <summary>
        /// View Machine Maintenance
        /// </summary>
        public const string RPA_GEN_MACHINE_VIEW = "RPA_GEN_MACHINE_VIEW";

        /// <summary>
        /// Edit Machine Maintenance
        /// </summary>
        public const string RPA_GEN_MACHINE_EDIT = "RPA_GEN_MACHINE_EDIT";

		/// <summary>
		/// New Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE_NEW = "RPA_GEN_MACHINE_NEW";

		/// <summary>
		/// Open Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE_OPEN = "RPA_GEN_MACHINE_OPEN";

		/// <summary>
		/// Show Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE_SHOW = "RPA_GEN_MACHINE_SHOW";



        /// <summary>
        /// 
        /// ROUTING Maintenance
        /// </summary>
        public const string RPA_GEN_ROUTING = "RPA_GEN_ROUTING";

        /// <summary>
        /// Delete ROUTING Maintenance
        /// </summary>
        public const string RPA_GEN_ROUTING_DELETE = "RPA_GEN_ROUTING_DELETE";

        /// <summary>
        /// View ROUTING Maintenance
        /// </summary>
        public const string RPA_GEN_ROUTING_VIEW = "RPA_GEN_ROUTING_VIEW";

        /// <summary>
        /// Edit ROUTING Maintenance
        /// </summary>
        public const string RPA_GEN_ROUTING_EDIT = "RPA_GEN_ROUTING_EDIT";

        /// <summary>
        /// New ROUTING Maintenance
        /// </summary>
        public const string RPA_GEN_ROUTING_NEW = "RPA_GEN_ROUTING_NEW";

        /// <summary>
        /// Open ROUTING Maintenance
        /// </summary>
        public const string RPA_GEN_ROUTING_OPEN = "RPA_GEN_ROUTING_OPEN";

        /// <summary>
        /// Show ROUTING Maintenance
        /// </summary>
        public const string RPA_GEN_ROUTING_SHOW = "RPA_GEN_ROUTING_SHOW";


        /// <summary>
        /// 
        /// Overhead Maintenance
        /// </summary>
        public const string RPA_GEN_OVERHEAD = "RPA_GEN_OVERHEAD";

        /// <summary>
        /// Delete Overhead Maintenance
        /// </summary>
        public const string RPA_GEN_OVERHEAD_DELETE = "RPA_GEN_OVERHEAD_DELETE";

        /// <summary>
        /// View Overhead Maintenance
        /// </summary>
        public const string RPA_GEN_OVERHEAD_VIEW = "RPA_GEN_OVERHEAD_VIEW";

        /// <summary>
        /// Edit Overhead Maintenance
        /// </summary>
        public const string RPA_GEN_OVERHEAD_EDIT = "RPA_GEN_OVERHEAD_EDIT";

        /// <summary>
        /// New Overhead Maintenance
        /// </summary>
        public const string RPA_GEN_OVERHEAD_NEW = "RPA_GEN_OVERHEAD_NEW";

        /// <summary>
        /// Open Overhead Maintenance
        /// </summary>
        public const string RPA_GEN_OVERHEAD_OPEN = "RPA_GEN_OVERHEAD_OPEN";

        /// <summary>
        /// Show Overhead Maintenance
        /// </summary>
        public const string RPA_GEN_OVERHEAD_SHOW = "RPA_GEN_OVERHEAD_SHOW";


        /// <summary>
        /// 
        /// CUSTOMERLOC Maintenance
        /// </summary>
        public const string RPA_GEN_CUSTOMERLOC = "RPA_GEN_CUSTOMERLOC";

        /// <summary>
        /// Delete CUSTOMERLOC Maintenance
        /// </summary>
        public const string RPA_GEN_CUSTOMERLOC_DELETE = "RPA_GEN_CUSTOMERLOC_DELETE";

        /// <summary>
        /// View CUSTOMERLOC Maintenance
        /// </summary>
        public const string RPA_GEN_CUSTOMERLOC_VIEW = "RPA_GEN_CUSTOMERLOC_VIEW";

        /// <summary>
        /// Edit CUSTOMERLOC Maintenance
        /// </summary>
        public const string RPA_GEN_CUSTOMERLOC_EDIT = "RPA_GEN_CUSTOMERLOC_EDIT";

        /// <summary>
        /// New CUSTOMERLOC Maintenance
        /// </summary>
        public const string RPA_GEN_CUSTOMERLOC_NEW = "RPA_GEN_CUSTOMERLOC_NEW";

        /// <summary>
        /// Open CUSTOMERLOC Maintenance
        /// </summary>
        public const string RPA_GEN_CUSTOMERLOC_OPEN = "RPA_GEN_CUSTOMERLOC_OPEN";

        /// <summary>
        /// Show CUSTOMERLOC Maintenance
        /// </summary>
        public const string RPA_GEN_CUSTOMERLOC_SHOW = "RPA_GEN_CUSTOMERLOC_SHOW";




        /// <summary>
        /// 
        /// ITEMBOM Maintenance
        /// </summary>
        public const string RPA_GEN_ITEMBOM = "RPA_GEN_ITEMBOM";

        /// <summary>
        /// Delete ITEMBOM Print Listing
        /// </summary>
        public const string RPA_GEN_ITEMBOM_PRINT = "RPA_GEN_ITEMBOM_PRINT_LISTING";

        /// <summary>
        /// Delete ITEMBOM Maintenance
        /// </summary>
        public const string RPA_GEN_ITEMBOM_DELETE = "RPA_GEN_ITEMBOM_DELETE";

        /// <summary>
        /// View ITEMBOM Maintenance
        /// </summary>
        public const string RPA_GEN_ITEMBOM_VIEW = "RPA_GEN_ITEMBOM_VIEW";

        /// <summary>
        /// Edit ITEMBOM Maintenance
        /// </summary>
        public const string RPA_GEN_ITEMBOM_EDIT = "RPA_GEN_ITEMBOM_EDIT";

        /// <summary>
        /// New ITEMBOM Maintenance
        /// </summary>
        public const string RPA_GEN_ITEMBOM_NEW = "RPA_GEN_ITEMBOM_NEW";

        /// <summary>
        /// Open ITEMBOM Maintenance
        /// </summary>
        public const string RPA_GEN_ITEMBOM_OPEN = "RPA_GEN_ITEMBOM_OPEN";

        /// <summary>
        /// Show ITEMBOM Maintenance
        /// </summary>
        public const string RPA_GEN_ITEMBOM_SHOW = "RPA_GEN_ITEMBOM_SHOW";



        /// <summary>
        /// 
        /// WO Maintenance
        /// </summary>
        public const string RPA_WO = "RPA_WO";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_PRINTED = "RPA_WO_PRINTED";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_PRINTED_PRINT = "RPA_WO_PRINTED_PRINT";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_PRINTED_CANCEL = "RPA_WO_PRINTED_CANCEL";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_PRINTED_PREVIEW = "RPA_WO_PRINTED_PREVIEW";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_PRINTED_EXPORT = "RPA_WO_PRINTED_EXPORT";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_PRINTED_EDIT = "RPA_WO_PRINTED_EDIT";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_PRINTED_DELETE = "RPA_WO_PRINTED_DELETE";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_DOC_REPORT = "RPA_WO_DOC_REPORT";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_DOC_REPORT_EXPORT = "RPA_WO_DOC_REPORT_EXPORT";
        /// <summary>
        /// Report WO
        /// </summary>
        public const string RPA_WO_DOC_REPORT_PRINT = "RPA_WO_DOC_REPORT_PRINT";
        /// <summary>
        /// Delete WO Print Listing
        /// </summary>
        public const string RPA_WO_DOC_REPORT_PREVIEW = "RPA_WO_DOC_REPORT_PREVIEW";

        /// <summary>
        /// Delete WO Detail Print Listing
        /// </summary>
        public const string RPA_WO_DETAIL_PRINT = "RPA_WO_DETAIL_PRINT_LISTING";
        /// <summary>
        /// Close WO Maintenance
        /// </summary>
        public const string RPA_WO_CLOSE = "RPA_WO_CLOSE";
        /// <summary>
        /// Delete WO Maintenance
        /// </summary>
        public const string RPA_WO_DELETE = "RPA_WO_DELETE";
        /// <summary>
        /// Delete WO Maintenance
        /// </summary>
        public const string RPA_WO_CANCEL = "RPA_WO_CANCEL";
        
        /// <summary>
        /// View WO Maintenance
        /// </summary>
        public const string RPA_WO_VIEW = "RPA_WO_VIEW";

        /// <summary>
        /// Edit WO Maintenance
        /// </summary>
        public const string RPA_WO_EDIT = "RPA_WO_EDIT";

        /// <summary>
        /// New WO Maintenance
        /// </summary>
        public const string RPA_WO_NEW = "RPA_WO_NEW";

        /// <summary>
        /// Open WO Maintenance
        /// </summary>
        public const string RPA_WO_OPEN = "RPA_WO_OPEN";

        /// <summary>
        /// Show WO Maintenance
        /// </summary>
        public const string RPA_WO_SHOW = "RPA_WO_SHOW";



        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_WO_LISTING_REPORT = "RPA_WO_LISTING_REPORT";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_WO_LISTING_REPORT_SHOW = "RPA_WO_LISTING_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_WO_LISTING_REPORT_OPEN = "RPA_WO_LISTING_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive After Frozen Print Listing
        /// </summary>
        public const string RPA_WO_LISTING_REPORT_PREVIEW = "RPA_WO_LISTING_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_WO_LISTING_REPORT_PRINT = "RPA_WO_LISTING_REPORT_PRINT";
        /// <summary>
        /// DeleteStock Receive After Frozen Print Listing
        /// </summary>
        public const string RPA_WO_LISTING_REPORT_EXPORT = "RPA_WO_LISTING_REPORT_EXPORT";



        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_WO_DTLLIST_REPORT = "RPA_WO_DTLLIST_REPORT";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_WO_DTLLIST_REPORT_SHOW = "RPA_WO_DTLLIST_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_WO_DTLLIST_REPORT_OPEN = "RPA_WO_DTLLIST_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive After Frozen Print Listing
        /// </summary>
        public const string RPA_WO_DTLLIST_REPORT_PREVIEW = "RPA_WO_DTLLIST_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_WO_DTLLIST_REPORT_PRINT = "RPA_WO_DTLLIST_REPORT_PRINT";





        /// <summary>
        /// 
        ///Stock Issue Raw Material Maintenance
        /// </summary>
        public const string RPA_ISS_RM = "RPA_ISS_RM";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_PRINTED = "RPA_ISS_RM_PRINTED";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_PRINTED_PRINT = "RPA_ISS_RM_PRINTED_PRINT";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_PRINTED_CANCEL = "RPA_ISS_RM_PRINTED_CANCEL";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_PRINTED_PREVIEW = "RPA_ISS_RM_PRINTED_PREVIEW";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_PRINTED_EXPORT = "RPA_ISS_RM_PRINTED_EXPORT";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_PRINTED_EDIT = "RPA_ISS_RM_PRINTED_EDIT";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_PRINTED_DELETE = "RPA_ISS_RM_PRINTED_DELETE";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_DOC_REPORT = "RPA_ISS_RM_DOC_REPORT";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_DOC_REPORT_EXPORT = "RPA_ISS_RM_DOC_REPORT_EXPORT";
        /// <summary>
        /// ReportStock Issue Raw Material
        /// </summary>
        public const string RPA_ISS_RM_DOC_REPORT_PRINT = "RPA_ISS_RM_DOC_REPORT_PRINT";
        /// <summary>
        /// DeleteStock Issue Raw Material Print Listing
        /// </summary>
        public const string RPA_ISS_RM_DOC_REPORT_PREVIEW = "RPA_ISS_RM_DOC_REPORT_PREVIEW";

        /// <summary>
        /// DeleteStock Issue Raw Material Detail Print Listing
        /// </summary>
        public const string RPA_ISS_RM_DETAIL_PRINT = "RPA_ISS_RM_DETAIL_PRINT_LISTING";

        /// <summary>
        /// DeleteStock Issue Raw Material Maintenance
        /// </summary>
        public const string RPA_ISS_RM_DELETE = "RPA_ISS_RM_DELETE";
        /// <summary>
        /// DeleteStock Issue Raw Material Maintenance
        /// </summary>
        public const string RPA_ISS_RM_CANCEL = "RPA_ISS_RM_CANCEL";

        /// <summary>
        /// ViewStock Issue Raw Material Maintenance
        /// </summary>
        public const string RPA_ISS_RM_VIEW = "RPA_ISS_RM_VIEW";

        /// <summary>
        /// EditStock Issue Raw Material Maintenance
        /// </summary>
        public const string RPA_ISS_RM_EDIT = "RPA_ISS_RM_EDIT";

        /// <summary>
        /// NewStock Issue Raw Material Maintenance
        /// </summary>
        public const string RPA_ISS_RM_NEW = "RPA_ISS_RM_NEW";

        /// <summary>
        /// OpenStock Issue Raw Material Maintenance
        /// </summary>
        public const string RPA_ISS_RM_OPEN = "RPA_ISS_RM_OPEN";

        /// <summary>
        /// ShowStock Issue Raw Material Maintenance
        /// </summary>
        public const string RPA_ISS_RM_SHOW = "RPA_ISS_RM_SHOW";

        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_ISS_RM_LISTING_REPORT = "RPA_ISS_RM_LISTING_REPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_ISS_RM_LISTING_REPORT_SHOW = "RPA_ISS_RM_LISTING_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_ISS_RM_LISTING_REPORT_OPEN = "RPA_ISS_RM_LISTING_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_ISS_RM_LISTING_REPORT_PREVIEW = "RPA_ISS_RM_LISTING_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_ISS_RM_LISTING_REPORT_PRINT = "RPA_ISS_RM_LISTING_REPORT_PRINT";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_ISS_RM_LISTING_REPORT_EXPORT = "RPA_ISS_RM_LISTING_REPORT_EXPORT";



        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_ISS_RM_DTLLIST_REPORT = "RPA_ISS_RM_DTLLIST_REPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_ISS_RM_DTLLIST_REPORT_SHOW = "RPA_ISS_RM_DTLLIST_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_ISS_RM_DTLLIST_REPORT_OPEN = "RPA_ISS_RM_DTLLIST_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_ISS_RM_DTLLIST_REPORT_PREVIEW = "RPA_ISS_RM_DTLLIST_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_ISS_RM_DTLLIST_REPORT_PRINT = "RPA_ISS_RM_DTLLIST_REPORT_PRINT";


        /// <summary>
        /// 
        ///Stock Receive Maintenance
        /// </summary>
        public const string RPA_RCV = "RPA_RCV";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_PRINTED = "RPA_RCV_PRINTED";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_PRINTED_PRINT = "RPA_RCV_PRINTED_PRINT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_PRINTED_CANCEL = "RPA_RCV_PRINTED_CANCEL";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_PRINTED_PREVIEW = "RPA_RCV_PRINTED_PREVIEW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_PRINTED_EXPORT = "RPA_RCV_PRINTED_EXPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_PRINTED_EDIT = "RPA_RCV_PRINTED_EDIT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_PRINTED_DELETE = "RPA_RCV_PRINTED_DELETE";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_DOC_REPORT = "RPA_RCV_DOC_REPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_DOC_REPORT_EXPORT = "RPA_RCV_DOC_REPORT_EXPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_DOC_REPORT_PRINT = "RPA_RCV_DOC_REPORT_PRINT";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_RCV_DOC_REPORT_PREVIEW = "RPA_RCV_DOC_REPORT_PREVIEW";

        /// <summary>
        /// DeleteStock Receive Detail Print Listing
        /// </summary>
        public const string RPA_RCV_DETAIL_PRINT = "RPA_RCV_DETAIL_PRINT_LISTING";

        /// <summary>
        /// DeleteStock Receive Maintenance
        /// </summary>
        public const string RPA_RCV_DELETE = "RPA_RCV_DELETE";
        /// <summary>
        /// DeleteStock Receive Maintenance
        /// </summary>
        public const string RPA_RCV_CANCEL = "RPA_RCV_CANCEL";

        /// <summary>
        /// ViewStock Receive Maintenance
        /// </summary>
        public const string RPA_RCV_VIEW = "RPA_RCV_VIEW";

        /// <summary>
        /// EditStock Receive Maintenance
        /// </summary>
        public const string RPA_RCV_EDIT = "RPA_RCV_EDIT";

        /// <summary>
        /// NewStock Receive Maintenance
        /// </summary>
        public const string RPA_RCV_NEW = "RPA_RCV_NEW";

        /// <summary>
        /// OpenStock Receive Maintenance
        /// </summary>
        public const string RPA_RCV_OPEN = "RPA_RCV_OPEN";

        /// <summary>
        /// ShowStock Receive Maintenance
        /// </summary>
        public const string RPA_RCV_SHOW = "RPA_RCV_SHOW";

        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_LISTING_REPORT = "RPA_RCV_LISTING_REPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_LISTING_REPORT_SHOW = "RPA_RCV_LISTING_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_LISTING_REPORT_OPEN = "RPA_RCV_LISTING_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_RCV_LISTING_REPORT_PREVIEW = "RPA_RCV_LISTING_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_LISTING_REPORT_PRINT = "RPA_RCV_LISTING_REPORT_PRINT";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_RCV_LISTING_REPORT_EXPORT = "RPA_RCV_LISTING_REPORT_EXPORT";



        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_DTLLIST_REPORT = "RPA_RCV_DTLLIST_REPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_DTLLIST_REPORT_SHOW = "RPA_RCV_DTLLIST_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_DTLLIST_REPORT_OPEN = "RPA_RCV_DTLLIST_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_RCV_DTLLIST_REPORT_PREVIEW = "RPA_RCV_DTLLIST_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCV_DTLLIST_REPORT_PRINT = "RPA_RCV_DTLLIST_REPORT_PRINT";




        /// <summary>
        /// 
        ///Stock Receive Maintenance
        /// </summary>
        public const string RPA_RCVRM = "RPA_RCVRM";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_PRINTED = "RPA_RCVRM_PRINTED";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_PRINTED_PRINT = "RPA_RCVRM_PRINTED_PRINT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_PRINTED_CANCEL = "RPA_RCVRM_PRINTED_CANCEL";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_PRINTED_PREVIEW = "RPA_RCVRM_PRINTED_PREVIEW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_PRINTED_EXPORT = "RPA_RCVRM_PRINTED_EXPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_PRINTED_EDIT = "RPA_RCVRM_PRINTED_EDIT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_PRINTED_DELETE = "RPA_RCVRM_PRINTED_DELETE";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_DOC_REPORT = "RPA_RCVRM_DOC_REPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_DOC_REPORT_EXPORT = "RPA_RCVRM_DOC_REPORT_EXPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_DOC_REPORT_PRINT = "RPA_RCVRM_DOC_REPORT_PRINT";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_RCVRM_DOC_REPORT_PREVIEW = "RPA_RCVRM_DOC_REPORT_PREVIEW";

        /// <summary>
        /// DeleteStock Receive Detail Print Listing
        /// </summary>
        public const string RPA_RCVRM_DETAIL_PRINT = "RPA_RCVRM_DETAIL_PRINT_LISTING";

        /// <summary>
        /// DeleteStock Receive Maintenance
        /// </summary>
        public const string RPA_RCVRM_DELETE = "RPA_RCVRM_DELETE";
        /// <summary>
        /// DeleteStock Receive Maintenance
        /// </summary>
        public const string RPA_RCVRM_CANCEL = "RPA_RCVRM_CANCEL";

        /// <summary>
        /// ViewStock Receive Maintenance
        /// </summary>
        public const string RPA_RCVRM_VIEW = "RPA_RCVRM_VIEW";

        /// <summary>
        /// EditStock Receive Maintenance
        /// </summary>
        public const string RPA_RCVRM_EDIT = "RPA_RCVRM_EDIT";

        /// <summary>
        /// NewStock Receive Maintenance
        /// </summary>
        public const string RPA_RCVRM_NEW = "RPA_RCVRM_NEW";

        /// <summary>
        /// OpenStock Receive Maintenance
        /// </summary>
        public const string RPA_RCVRM_OPEN = "RPA_RCVRM_OPEN";

        /// <summary>
        /// ShowStock Receive Maintenance
        /// </summary>
        public const string RPA_RCVRM_SHOW = "RPA_RCVRM_SHOW";

        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_LISTING_REPORT = "RPA_RCVRM_LISTING_REPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_LISTING_REPORT_SHOW = "RPA_RCVRM_LISTING_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_LISTING_REPORT_OPEN = "RPA_RCVRM_LISTING_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_RCVRM_LISTING_REPORT_PREVIEW = "RPA_RCVRM_LISTING_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_LISTING_REPORT_PRINT = "RPA_RCVRM_LISTING_REPORT_PRINT";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_RCVRM_LISTING_REPORT_EXPORT = "RPA_RCVRM_LISTING_REPORT_EXPORT";



        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_DTLLIST_REPORT = "RPA_RCVRM_DTLLIST_REPORT";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_DTLLIST_REPORT_SHOW = "RPA_RCVRM_DTLLIST_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_DTLLIST_REPORT_OPEN = "RPA_RCVRM_DTLLIST_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive Print Listing
        /// </summary>
        public const string RPA_RCVRM_DTLLIST_REPORT_PREVIEW = "RPA_RCVRM_DTLLIST_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive
        /// </summary>
        public const string RPA_RCVRM_DTLLIST_REPORT_PRINT = "RPA_RCVRM_DTLLIST_REPORT_PRINT";




        /// <summary>
        /// 
        ///Stock Receive After Frozen Maintenance
        /// </summary>
        public const string RPA_RCVFR = "RPA_RCVFR";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_PRINTED = "RPA_RCVFR_PRINTED";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_PRINTED_PRINT = "RPA_RCVFR_PRINTED_PRINT";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_PRINTED_CANCEL = "RPA_RCVFR_PRINTED_CANCEL";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_PRINTED_PREVIEW = "RPA_RCVFR_PRINTED_PREVIEW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_PRINTED_EXPORT = "RPA_RCVFR_PRINTED_EXPORT";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_PRINTED_EDIT = "RPA_RCVFR_PRINTED_EDIT";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_PRINTED_DELETE = "RPA_RCVFR_PRINTED_DELETE";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_DOC_REPORT = "RPA_RCVFR_DOC_REPORT";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_DOC_REPORT_EXPORT = "RPA_RCVFR_DOC_REPORT_EXPORT";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_DOC_REPORT_PRINT = "RPA_RCVFR_DOC_REPORT_PRINT";
        /// <summary>
        /// DeleteStock Receive After Frozen Print Listing
        /// </summary>
        public const string RPA_RCVFR_DOC_REPORT_PREVIEW = "RPA_RCVFR_DOC_REPORT_PREVIEW";

        /// <summary>
        /// DeleteStock Receive After Frozen Detail Print Listing
        /// </summary>
        public const string RPA_RCVFR_DETAIL_PRINT = "RPA_RCVFR_DETAIL_PRINT_LISTING";

        /// <summary>
        /// DeleteStock Receive After Frozen Maintenance
        /// </summary>
        public const string RPA_RCVFR_DELETE = "RPA_RCVFR_DELETE";
        /// <summary>
        /// DeleteStock Receive After Frozen Maintenance
        /// </summary>
        public const string RPA_RCVFR_CANCEL = "RPA_RCVFR_CANCEL";

        /// <summary>
        /// ViewStock Receive After Frozen Maintenance
        /// </summary>
        public const string RPA_RCVFR_VIEW = "RPA_RCVFR_VIEW";

        /// <summary>
        /// EditStock Receive After Frozen Maintenance
        /// </summary>
        public const string RPA_RCVFR_EDIT = "RPA_RCVFR_EDIT";

        /// <summary>
        /// NewStock Receive After Frozen Maintenance
        /// </summary>
        public const string RPA_RCVFR_NEW = "RPA_RCVFR_NEW";

        /// <summary>
        /// OpenStock Receive After Frozen Maintenance
        /// </summary>
        public const string RPA_RCVFR_OPEN = "RPA_RCVFR_OPEN";

        /// <summary>
        /// ShowStock Receive After Frozen Maintenance
        /// </summary>
        public const string RPA_RCVFR_SHOW = "RPA_RCVFR_SHOW";

        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_LISTING_REPORT = "RPA_RCVFR_LISTING_REPORT";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_LISTING_REPORT_SHOW = "RPA_RCVFR_LISTING_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_LISTING_REPORT_OPEN = "RPA_RCVFR_LISTING_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive After Frozen Print Listing
        /// </summary>
        public const string RPA_RCVFR_LISTING_REPORT_PREVIEW = "RPA_RCVFR_LISTING_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_LISTING_REPORT_PRINT = "RPA_RCVFR_LISTING_REPORT_PRINT";
        /// <summary>
        /// DeleteStock Receive After Frozen Print Listing
        /// </summary>
        public const string RPA_RCVFR_LISTING_REPORT_EXPORT = "RPA_RCVFR_LISTING_REPORT_EXPORT";



        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_DTLLIST_REPORT = "RPA_RCVFR_DTLLIST_REPORT";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_DTLLIST_REPORT_SHOW = "RPA_RCVFR_DTLLIST_REPORT_SHOW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_DTLLIST_REPORT_OPEN = "RPA_RCVFR_DTLLIST_REPORT_OPEN";
        /// <summary>
        /// DeleteStock Receive After Frozen Print Listing
        /// </summary>
        public const string RPA_RCVFR_DTLLIST_REPORT_PREVIEW = "RPA_RCVFR_DTLLIST_REPORT_PREVIEW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_RCVFR_DTLLIST_REPORT_PRINT = "RPA_RCVFR_DTLLIST_REPORT_PRINT";



         /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_PRODUCTION_LISTING = "RPA_PRODUCTION_LISTING";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_PRODUCTION_LISTING_SHOW = "RPA_PRODUCTION_LISTING_SHOW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_PRODUCTION_LISTING_OPEN = "RPA_PRODUCTION_LISTING_OPEN";
        /// <summary>
        /// DeleteStock Receive After Frozen Print Listing
        /// </summary>
        public const string RPA_PRODUCTION_LISTING_PREVIEW = "RPA_PRODUCTION_LISTING_PREVIEW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_PRODUCTION_LISTING_PRINT = "RPA_PRODUCTION_LISTING_PRINT";



        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_ITEMWASTE_LISTING = "RPA_ITEMWASTE_LISTING";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_ITEMWASTE_LISTING_SHOW = "RPA_ITEMWASTE_LISTING_SHOW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_ITEMWASTE_LISTING_OPEN = "RPA_ITEMWASTE_LISTING_OPEN";
        /// <summary>
        /// DeleteStock Receive After Frozen Print Listing
        /// </summary>
        public const string RPA_ITEMWASTE_LISTING_PREVIEW = "RPA_ITEMWASTE_LISTING_PREVIEW";
        /// <summary>
        /// ReportStock Receive After Frozen
        /// </summary>
        public const string RPA_ITEMWASTE_LISTING_PRINT = "RPA_ITEMWASTE_LISTING_PRINT";

    }
}
