﻿using BCE.Localization;
using System;
namespace Production
{
    public enum DealerStringId
    {
        [DefaultString("All")]
        All = 0,
        [DefaultString("Document No")]
        DocumentNo = 1,
        [DefaultString("Document Date")]
        DocumentDate = 2,
        [DefaultString("Item")]
        Item = 3,
        [DefaultString("Batch Print Stock Adjustment")]
        BatchPrintStockAdjustment = 4,
        [DefaultString("Print Stock Adjustment Listing")]
        PrintStockAdjustmentListing = 5,
        [DefaultString("Batch Print Stock Issue")]
        BatchPrintStockIssue = 6,
        [DefaultString("Print Stock Issue Listing")]
        PrintStockIssueListing = 7,
        [DefaultString("Batch Print Stock Receive")]
        BatchPrintStockReceive = 8,
        [DefaultString("Print Stock Receive Listing")]
        PrintStockReceiveListing = 9,
        [DefaultString("Batch Print Stock Transfer")]
        BatchPrintStockTransfer = 10,
        [DefaultString("Print Stock Transfer Listing")]
        PrintStockTransferListing = 11,
        [DefaultString("Batch Print Stock UOM Conversion")]
        BatchPrintStockUOMConversion = 12,
        [DefaultString("Print Stock UOM Conversion Listing")]
        PrintStockUOMConversionListing = 13,
        [DefaultString("Batch Print Stock Update Cost")]
        BatchPrintStockUpdateCost = 14,
        [DefaultString("Print Stock Update Cost Listing")]
        PrintStockUpdateCostListing = 15,
        [DefaultString("Batch Print Stock Write Off")]
        BatchPrintStockWriteOff = 16,
        [DefaultString("Print Stock Write Off Listing")]
        PrintStockWriteOffListing = 17,
        [DefaultString("Report Date: {0}")]
        ReportDate = 18,
        [DefaultString("Doc Date")]
        DocDate = 19,
        [DefaultString("Doc No")]
        DocNo = 20,
        [DefaultString("Item Code")]
        ItemCode = 21,
        [DefaultString("Item Group")]
        ItemGroup = 22,
        [DefaultString("Item Type")]
        ItemType = 23,
        [DefaultString("Location")]
        Location = 24,
        [DefaultString("Project No")]
        ProjectNo = 25,
        [DefaultString("Department No")]
        DepartmentNo = 26,
        [DefaultString("Yes")]
        Yes = 27,
        [DefaultString("No")]
        No = 28,
        [DefaultString("Group By")]
        GroupBy = 29,
        [DefaultString("From Location")]
        FromLocation = 30,
        [DefaultString("To Location")]
        ToLocation = 31,
        [DefaultString("From Date: {0}")]
        FromDate = 32,
        [DefaultString("To Date: {0}")]
        ToDate = 33,
        [DefaultString("Show By Location")]
        ShowByLocation = 34,
        [DefaultString("UOM Options: {0}")]
        UOMOption = 35,
        [DefaultString("Stock Item")]
        StockItem = 36,
        [DefaultString("Stock Group")]
        StockGroup = 37,
        [DefaultString("Date: {0}")]
        Date = 38,
        [DefaultString("By Location")]
        ByLocation = 39,
        [DefaultString("Batch Option: {0}")]
        BatchOption = 40,
        [DefaultString("Print Active Item")]
        PrintActiveItem = 41,
        [DefaultString("Print Inactive Item")]
        PrintInactiveItem = 42,
        [DefaultString("Include Zero Balance")]
        IncludeZeroBalance = 43,
        [DefaultString("Location Code")]
        LocationCode = 44,
        [DefaultString("Stock Item Type")]
        StockItemType = 45,
        [DefaultString("Stock Item Batch Number")]
        StockItemBatch = 46,
        [DefaultString("Sales Consignment")]
        SalesConsignment = 47,
        [DefaultString("Purchase Consignment")]
        PurchaseConsignment = 48,
        [DefaultString("Show By Branch")]
        ShowByBranch = 49,
        [DefaultString("Use Desc2 for Header")]
        UseDesc2ForHeader = 50,
        [DefaultString("Use Desc for Header")]
        UseDescForHeader = 51,
        [DefaultString("Sales Agent")]
        SalesAgent = 52,
        [DefaultString("Show Batch No")]
        ShowBatchNo = 53,
        [DefaultString("Detail")]
        Detail = 54,
        [DefaultString("Calculate All UOMs")]
        CalculateAllUOMs = 55,
        [DefaultString("Include Outstanding P/O & Outstanding S/O")]
        IncludeOutstandingPOOutstandingSO = 56,
        [DefaultString("Show Batch")]
        ShowBatch = 57,
        [DefaultString("Don't Show Batch")]
        DontShowBatch = 58,
        [DefaultString("Include Empty Batch")]
        IncludeEmptyBatch = 59,
        [DefaultString("Grid Options : Aging Months: {0}")]
        GridOptionsAgingMonths = 60,
        [DefaultString("Show Quantity")]
        ShowQuantity = 61,
        [DefaultString("Show Cost")]
        ShowCost = 62,
        [DefaultString("Item Active Option: {0}")]
        ItemActiveOption = 63,
        [DefaultString("None")]
        None = 64,
        [DefaultString("Merge Same Cost(FIFO and LIFO only)")]
        MergeSameCostFIFOAndLIFO = 65,
        [DefaultString("Stock Location: {0}")]
        StockLocation = 66,
        [DefaultString("Do Not Show Zero Balance Quantity")]
        DoNotShowZeroBalanceQuantity = 67,
        [DefaultString("Do Not Show Zero Balance Quantity And Zero Balance Cost")]
        DoNotShowZeroBalanceQuantityAndZeroBalanceCost = 68,
        [DefaultString("Show All Records")]
        ShowAllRecords = 69,
        [DefaultString("Print Stock Control")]
        PrintStockControl = 70,
        [DefaultString("Print Non-Stock Control")]
        PrintNonStockControl = 71,
        [DefaultString("Dealer Code: {0}")]
        DealerCode = 72,

    }
}
