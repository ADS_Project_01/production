using System;

namespace BCE.AutoCount.Const
{
	/// <summary>
	/// AccessRightConst class defines a lsit of command constant value for use in BCE.DBUtils.AccessRight.IsAccessible method.
	/// </summary>
	public class AccessRightConst
	{
		/// <summary>
		/// Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE = "RPA_GEN_MACHINE";

		/// <summary>
		/// Delete Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE_DELETE = "RPA_GEN_MACHINE_DELETE";

		/// <summary>
		/// Edit Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE_EDIT = "RPA_GEN_MACHINE_EDIT";

		/// <summary>
		/// New Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE_NEW = "RPA_GEN_MACHINE_NEW";

		/// <summary>
		/// Open Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE_OPEN = "RPA_GEN_MACHINE_OPEN";

		/// <summary>
		/// Show Machine Maintenance
		/// </summary>
		public const string RPA_GEN_MACHINE_SHOW = "RPA_GEN_MACHINE_SHOW";
	}
}
