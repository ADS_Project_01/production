using System;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using BCE.Data;

namespace Production
{
	/// <summary>
	/// Summary description for MainEntry.
	/// </summary>
	public class MainEntry
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
            // Here you must provide a dbSetting which points to your AutoCount 2006 database.
            //DBSetting dbSetting = new DBSetting(DBServerType.SQL2000, @"(local)\A2006", "AED_NEW_BG");
            //BCE.AutoCount.MainEntry.MainEntry.SubProjectStartup(dbSetting);

            // untuk memunculkan form login
            BCE.AutoCount.MainEntry.Startup.Default.SubProjectStartupWithLogin("", "");


            DBSetting dbSetting = BCE.AutoCount.Application.DBSetting;
			
			BCE.AutoCount.PlugIn.BeforeLoadArgs e = new BCE.AutoCount.PlugIn.BeforeLoadArgs(dbSetting,Guid.NewGuid(), System.Windows.Forms.Application.StartupPath, "PT. BUNGA MAS", "JL. SUNTER AGUNG UTARA BLOK M No.26", BCE.AutoCount.LicenseControl.LicenseControlType.LicenseCode,new byte[0]);

            //if (Plugin.Initialize(e))
			{
                FormMain form = new FormMain(dbSetting);
				System.Windows.Forms.Application.Run(form);
			}
		}
	}
}
