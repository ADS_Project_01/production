﻿//// Type: BCE.AutoCount.FilterUI.UCProsesBonusCrewSelector
//// Assembly: BCE.AutoCount.CommonAccounting, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
//// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.CommonAccounting.dll

//using BCE.AutoCount.SearchFilter;
//using BCE.AutoCount.XtraUtils.LookupEditBuilder;
//using BCE.Data;
//using DevExpress.XtraEditors;
//using DevExpress.XtraEditors.Controls;
//using System;
//using System.ComponentModel;
//using System.Globalization;
//using System.Windows.Forms;
//using BCE.AutoCount.FilterUI;
//namespace Production
//{
//    public class UCDealerSelector : XtraUserControl
//    {
//        private DBSetting myDBSetting;
//        private BCE.AutoCount.SearchFilter.Filter myFilter;
//        private SampleDealerLookupEditBuilder myLookupEditBuilder;
//        private LookUpEdit lookUpEditTo;
//        private Label label2;
//        private LookUpEdit lookUpEditFrom;
//        private Label label1;
//        private ComboBoxEdit comboBoxEditOption;
//        private LinkLabel lblMultiSelect;
//        private bool myInApplyFilter;
//        private PanelControl panel1;
//        private Container components;

//        public UCDealerSelector()
//        {
//            this.InitializeComponent();
//            this.panel1.Visible = false;
//            this.lblMultiSelect.Visible = false;
//            this.comboBoxEditOption.Properties.HotTrackItems = false;
//            this.comboBoxEditOption.Properties.UseCtrlScroll = true;
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && this.components != null)
//                this.components.Dispose();
//            base.Dispose(disposing);
//        }

//        private void InitializeComponent()
//        {
//            this.comboBoxEditOption = new DevExpress.XtraEditors.ComboBoxEdit();
//            this.label2 = new System.Windows.Forms.Label();
//            this.lookUpEditFrom = new DevExpress.XtraEditors.LookUpEdit();
//            this.lookUpEditTo = new DevExpress.XtraEditors.LookUpEdit();
//            this.label1 = new System.Windows.Forms.Label();
//            this.lblMultiSelect = new System.Windows.Forms.LinkLabel();
//            this.panel1 = new DevExpress.XtraEditors.PanelControl();
//            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditOption.Properties)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditFrom.Properties)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditTo.Properties)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
//            this.panel1.SuspendLayout();
//            this.SuspendLayout();
//            // 
//            // comboBoxEditOption
//            // 
//            this.comboBoxEditOption.EditValue = "No filter";
//            this.comboBoxEditOption.Location = new System.Drawing.Point(0, 0);
//            this.comboBoxEditOption.Name = "comboBoxEditOption";
//            this.comboBoxEditOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
//            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
//            this.comboBoxEditOption.Properties.Items.AddRange(new object[] {
//            "No filter",
//            "Filter by range",
//            "Filter by multi-select"});
//            this.comboBoxEditOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
//            this.comboBoxEditOption.Size = new System.Drawing.Size(128, 20);
//            this.comboBoxEditOption.TabIndex = 0;
//            this.comboBoxEditOption.EditValueChanged += new System.EventHandler(this.comboBoxEditOption_EditValueChanged);
//            // 
//            // label2
//            // 
//            this.label2.Location = new System.Drawing.Point(3, 0);
//            this.label2.Name = "label2";
//            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
//            this.label2.Size = new System.Drawing.Size(29, 20);
//            this.label2.TabIndex = 0;
//            this.label2.Text = "from";
//            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
//            // 
//            // lookUpEditFrom
//            // 
//            this.lookUpEditFrom.Location = new System.Drawing.Point(34, 1);
//            this.lookUpEditFrom.Name = "lookUpEditFrom";
//            this.lookUpEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
//            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
//            this.lookUpEditFrom.Size = new System.Drawing.Size(90, 20);
//            this.lookUpEditFrom.TabIndex = 2;
//            this.lookUpEditFrom.Validated += new System.EventHandler(this.lookUpEditFrom_Validated);
//            // 
//            // lookUpEditTo
//            // 
//            this.lookUpEditTo.Location = new System.Drawing.Point(145, 1);
//            this.lookUpEditTo.Name = "lookUpEditTo";
//            this.lookUpEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
//            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
//            this.lookUpEditTo.Size = new System.Drawing.Size(90, 20);
//            this.lookUpEditTo.TabIndex = 3;
//            this.lookUpEditTo.Validated += new System.EventHandler(this.lookUpEditTo_Validated);
//            // 
//            // label1
//            // 
//            this.label1.Location = new System.Drawing.Point(124, 4);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(17, 15);
//            this.label1.TabIndex = 1;
//            this.label1.Text = "to";
//            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
//            // 
//            // lblMultiSelect
//            // 
//            this.lblMultiSelect.Location = new System.Drawing.Point(133, 2);
//            this.lblMultiSelect.Name = "lblMultiSelect";
//            this.lblMultiSelect.Size = new System.Drawing.Size(100, 18);
//            this.lblMultiSelect.TabIndex = 2;
//            this.lblMultiSelect.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblMultiSelect_LinkClicked);
//            // 
//            // panel1
//            // 
//            this.panel1.Controls.Add(this.label2);
//            this.panel1.Controls.Add(this.lookUpEditTo);
//            this.panel1.Controls.Add(this.lookUpEditFrom);
//            this.panel1.Controls.Add(this.label1);
//            this.panel1.Location = new System.Drawing.Point(128, -1);
//            this.panel1.Name = "panel1";
//            this.panel1.Size = new System.Drawing.Size(241, 25);
//            this.panel1.TabIndex = 3;
//            this.panel1.Text = "panelControl1";
//            // 
//            // UCProsesBonusCrewSelector
//            // 
//            this.Controls.Add(this.panel1);
//            this.Controls.Add(this.lblMultiSelect);
//            this.Controls.Add(this.comboBoxEditOption);
//            this.Name = "UCProsesBonusCrewSelector";
//            this.Size = new System.Drawing.Size(374, 26);
//            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditOption.Properties)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditFrom.Properties)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditTo.Properties)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
//            this.panel1.ResumeLayout(false);
//            this.ResumeLayout(false);

//        }

//        public void Initialize(DBSetting dbSetting, BCE.AutoCount.SearchFilter.Filter filter)
//        {
//            this.myDBSetting = dbSetting;
//            this.myLookupEditBuilder = new SampleDealerLookupEditBuilder("");
//            this.myLookupEditBuilder.BuildLookupEdit(this.lookUpEditFrom.Properties, this.myDBSetting);
//            this.myLookupEditBuilder.BuildLookupEdit(this.lookUpEditTo.Properties, this.myDBSetting);
//            this.ApplyFilter(filter);
//        }

//        public void RefreshLookupEditBuilder()
//        {
//            if (this.myLookupEditBuilder != null)
//                this.myLookupEditBuilder.Refresh();
//        }

//        public void ApplyFilter(BCE.AutoCount.SearchFilter.Filter filter)
//        {
//            if (filter != null)
//            {
//                this.myInApplyFilter = true;
//                try
//                {
//                    this.myFilter = filter;
//                    this.comboBoxEditOption.SelectedIndex = this.myFilter.Type != FilterType.None ? (this.myFilter.Type != FilterType.ByRange ? 2 : 1) : 0;
//                    this.lookUpEditFrom.EditValue = this.myFilter.From;
//                    this.lookUpEditTo.EditValue = this.myFilter.To;
//                    this.lblMultiSelect.Text = this.myFilter.ToString();
//                    this.SelectionChanged();
//                }
//                finally
//                {
//                    this.myInApplyFilter = false;
//                }
//            }
//        }

//        private void LaunchMultiSelect()
//        {
//            using (FormMultiSelectDealer selectProsesBonusCrew = new FormMultiSelectDealer(this.myDBSetting, this.myFilter))
//            {
//                int num = (int)selectProsesBonusCrew.ShowDialog();
//            }
//            this.myFilter.Type = FilterType.ByIndividual;
//            this.lblMultiSelect.Text = this.myFilter.ToString();
//        }

//        private void SelectionChanged()
//        {
//            if (this.myFilter == null)
//            {
//                throw new NullReferenceException("Initialize method has not been called.");
//            }
//            else
//            {
//                this.lookUpEditFrom.Enabled = this.comboBoxEditOption.SelectedIndex == 1;
//                this.lookUpEditTo.Enabled = this.comboBoxEditOption.SelectedIndex == 1;
//                this.panel1.Visible = this.comboBoxEditOption.SelectedIndex == 1;
//                this.lblMultiSelect.Visible = this.comboBoxEditOption.SelectedIndex == 2;
//                if (this.comboBoxEditOption.SelectedIndex == 2)
//                {
//                    if (!this.myInApplyFilter)
//                        this.LaunchMultiSelect();
//                }
//                else if (this.comboBoxEditOption.SelectedIndex == 0)
//                {
//                    this.myFilter.Type = FilterType.None;
//                }
//                else
//                {
//                    this.myFilter.Type = FilterType.ByRange;
//                    if (!this.myInApplyFilter)
//                        this.lookUpEditFrom.Focus();
//                }
//            }
//        }

//        private void lookUpEditFrom_Validated(object sender, EventArgs e)
//        {
//            this.myFilter.From = this.lookUpEditFrom.EditValue;
//            if (this.lookUpEditTo.EditValue == null || string.Compare(this.lookUpEditTo.Text, this.lookUpEditFrom.Text, true, CultureInfo.InvariantCulture) < 0)
//            {
//                this.lookUpEditTo.Text = this.lookUpEditFrom.Text;
//                this.myFilter.To = this.myFilter.From;
//            }
//        }

//        private void lookUpEditTo_Validated(object sender, EventArgs e)
//        {
//            this.myFilter.To = this.lookUpEditTo.EditValue;
//            if (this.lookUpEditFrom.EditValue == null || string.Compare(this.lookUpEditTo.Text, this.lookUpEditFrom.Text, true, CultureInfo.InvariantCulture) < 0)
//            {
//                this.lookUpEditFrom.Text = this.lookUpEditTo.Text;
//                this.myFilter.From = this.myFilter.To;
//            }
//        }

//        private void comboBoxEditOption_EditValueChanged(object sender, EventArgs e)
//        {
//            this.SelectionChanged();
//        }

//        private void lblMultiSelect_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
//        {
//            this.LaunchMultiSelect();
//        }

//        public void Reset()
//        {
//            this.lookUpEditFrom.EditValue = (object)null;
//            this.lookUpEditTo.EditValue = (object)null;
//            this.comboBoxEditOption.SelectedIndex = 0;
//            this.SelectionChanged();
//            this.myFilter.Reset();
//        }

//        private void panel1_Paint(object sender, PaintEventArgs e)
//        {

//        }
//    }
//}
