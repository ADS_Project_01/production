﻿using System;
using System.Data;
using System.Data.SqlClient;
using BCE.Data;

namespace Production.GeneralMaintenance
{
    /// <summary>
    /// Summary description for Routing
    /// </summary>
    public class Routing
    {
        protected DBSetting myDBSetting;
        protected DataTable myBrowseTable;

        internal Routing()
        {
            myBrowseTable = new DataTable();
        }

        public DBSetting DBSetting
        {
            get { return myDBSetting; }
        }

        public static Routing Create(DBSetting dbSetting)
        {
            Routing aRouting;
            if (dbSetting.ServerType == DBServerType.SQL2000)
                aRouting = new RoutingSQL();
            else
                throw new ArgumentException("Server type: " + dbSetting.ServerType + " not supported.");

            aRouting.myDBSetting = dbSetting;

            return aRouting;
        }

        public DataTable BrowseTable
        {
            get { return myBrowseTable; }
        }

        public virtual DataTable LoadBrowseTable(string columnSQL)
        {
            return null;
        }

        public virtual DataTable LoadDealerListing(string fromRoutingCode, string toRoutingCode)
        {
            return null;
        }

        public RoutingEntity NewEntity()
        {
            DataSet ds = LoadData("");

            DataRow row = ds.Tables[0].NewRow();
            //object obj = myDBSetting.ExecuteScalar("select top 1 RoutingCode from Routing order by RoutingCode Desc");
            //if (obj != null && obj != DBNull.Value)
            //{
            //    row["RoutingCode"] = (1 + BCE.Data.Convert.ToDecimal(obj)).ToString();
            //}
            //else
            //{
            //    row["RoutingCode"]= (1).ToString();
            //}
            row["RoutingCode"] = "";
            row["IsActive"] = BCE.Data.Convert.BooleanToText(true);
            ds.Tables[0].Rows.Add(row);
            try { ds.Tables[0].Constraints.Add("RoutingCode", myBrowseTable.Columns["RoutingCode"], true); } catch { }

            return new RoutingEntity(this, ds);
        }

        public RoutingEntity GetEntity(string RoutingCode)
        {
            DataSet ds = LoadData(RoutingCode);

            if (ds.Tables[0].Rows.Count == 0)
                return null;

            return new RoutingEntity(this, ds);
        }

        public void SaveEntity(RoutingEntity entity)
        {
            if (entity.RoutingCode.Length == 0)
                throw new EmptyRoutingCodeException();
            SaveData(entity.myDataSet);

            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(entity.RoutingCode);
            if (r == null)
            {
                r = myBrowseTable.NewRow();
                foreach (DataColumn col in entity.RoutingTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
                myBrowseTable.Rows.Add(r);
            }
            else
            {
                foreach (DataColumn col in entity.RoutingTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
            }
            myBrowseTable.AcceptChanges();
        }

        protected virtual DataSet LoadData(string RoutingCode)
        {
            return null;
        }

        public virtual void Delete(string RoutingCode)
        {
        }

        protected virtual void SaveData(DataSet ds)
        {
        }
    }

    public class RoutingSQL : Routing
    {
        public override DataTable LoadBrowseTable(string columnSQL)
        {
            myBrowseTable.Clear();
            myDBSetting.LoadDataTable(myBrowseTable, string.Format("SELECT {0} FROM RPA_Routing", columnSQL), true);
            return myBrowseTable;
        }

        public override DataTable LoadDealerListing(string fromRoutingCode, string toRoutingCode)
        {
            return myDBSetting.GetDataTable("SELECT * FROM  RPA_Routing WHERE RoutingCode>=? AND RoutingCode<=?", false, fromRoutingCode, toRoutingCode);
        }

        protected override DataSet LoadData(string RoutingCode)
        {
            SqlConnection conn = new SqlConnection(myDBSetting.ConnectionString);

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("SELECT * FROM RPA_Routing WHERE RoutingCode=@RoutingCode", conn);
                cmd.Parameters.AddWithValue("@RoutingCode", RoutingCode);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                if (RoutingCode.Length == 0)
                    adapter.FillSchema(ds, SchemaType.Mapped, "RPA_Routing");
                else
                    adapter.Fill(ds, "RPA_Routing");
                return ds;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        public override void Delete(string RoutingCode)
        {
            myDBSetting.ExecuteNonQuery("DELETE FROM RPA_Routing WHERE RoutingCode=?", RoutingCode);

            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(RoutingCode);
            if (r != null)
                r.Delete();
            myBrowseTable.AcceptChanges();
        }

        protected override void SaveData(DataSet ds)
        {
            myDBSetting.SimpleSaveDataSet(ds, "RPA_Routing", "SELECT * FROM RPA_Routing");
        }
    }

    public class RoutingEntity
    {
        private Routing myRouting;
        internal DataSet myDataSet;
        private DataRow myRow;

        public RoutingEntity(Routing aRouting, DataSet ds)
        {
            myRouting = aRouting;
            myDataSet = ds;
            myRow = myDataSet.Tables[0].Rows[0];
        }

        public Routing Routing
        {
            get { return myRouting; }
        }

        internal DataRow Row
        {
            get { return myRow; }
        }

        public void Save()
        {
            myRouting.SaveEntity(this);
        }

        public bool IsModified
        {
            get { return myDataSet.GetChanges() != null; }
        }

        public DataTable RoutingTable
        {
            get { return myDataSet.Tables[0]; }
        }

        public string RoutingCode
        {
            get { return myRow["RoutingCode"].ToString(); }
            set { myRow["RoutingCode"] = value; }
        }

        public object Description
        {
            get { return myRow["Description"]; }
            set { myRow["Description"] = value; }
        }

        public object Desc2
        {
            get { return myRow["Desc2"]; }
            set { myRow["Desc2"] = value; }
        }
       
        public bool IsActive
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myRow["IsActive"]);
            }
            set
            {
                this.myRow["IsActive"] = (object)BCE.Data.Convert.BooleanToText(value);
            }
        }
       
    }

    public class EmptyRoutingCodeException : BCE.Application.AppException
    {
        public EmptyRoutingCodeException()
            : base("Empty Routing is not allowed.")
        {
        }
    }
}
