﻿using System;
using System.Data;
using System.Data.SqlClient;
using BCE.Data;

namespace Production.GeneralMaintenance
{
    /// <summary>
    /// Summary description for Lookup
    /// </summary>
    public class Lookup
    {
        protected DBSetting myDBSetting;
        protected DataTable myBrowseTable;

        internal Lookup()
        {
            myBrowseTable = new DataTable();
        }

        public DBSetting DBSetting
        {
            get { return myDBSetting; }
        }

        public static Lookup Create(DBSetting dbSetting)
        {
            Lookup aLookup;
            if (dbSetting.ServerType == DBServerType.SQL2000)
                aLookup = new LookupSQL();
            else
                throw new ArgumentException("Server type: " + dbSetting.ServerType + " not supported.");

            aLookup.myDBSetting = dbSetting;

            return aLookup;
        }

        public DataTable BrowseTable
        {
            get { return myBrowseTable; }
        }

        public virtual DataTable LoadBrowseTable(string columnSQL)
        {
            return null;
        }

        public virtual DataTable LoadDealerListing(string fromCode, string toCode)
        {
            return null;
        }

        public LookupEntity NewEntity()
        {
            DataSet ds = LoadData("");

            DataRow row = ds.Tables[0].NewRow();
            //object obj = myDBSetting.ExecuteScalar("select top 1 Code from Lookup order by Code Desc");
            //if (obj != null && obj != DBNull.Value)
            //{
            //    row["Code"] = (1 + BCE.Data.Convert.ToDecimal(obj)).ToString();
            //}
            //else
            //{
            //    row["Code"]= (1).ToString();
            //}
            //row["Code"] = "";
            row["IsActive"] = BCE.Data.Convert.BooleanToText(true);
            ds.Tables[0].Rows.Add(row);
            try { ds.Tables[0].Constraints.Add("Code", myBrowseTable.Columns["Code"], true); } catch { }

            return new LookupEntity(this, ds);
        }

        public LookupEntity GetEntity(string Code)
        {
            DataSet ds = LoadData(Code);

            if (ds.Tables[0].Rows.Count == 0)
                return null;

            return new LookupEntity(this, ds);
        }

        public void SaveEntity(LookupEntity entity)
        {
            if (entity.Code.Length == 0)
                throw new EmptyCodeException();
            SaveData(entity.myDataSet);

            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(entity.Code);
            if (r == null)
            {
                r = myBrowseTable.NewRow();
                foreach (DataColumn col in entity.LookupTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
                myBrowseTable.Rows.Add(r);
            }
            else
            {
                foreach (DataColumn col in entity.LookupTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
            }
            myBrowseTable.AcceptChanges();
        }

        protected virtual DataSet LoadData(string Code)
        {
            return null;
        }

        public virtual void Delete(string Code)
        {
        }

        protected virtual void SaveData(DataSet ds)
        {
        }
    }

    public class LookupSQL : Lookup
    {
        public override DataTable LoadBrowseTable(string columnSQL)
        {
            myBrowseTable.Clear();
            myDBSetting.LoadDataTable(myBrowseTable, string.Format("SELECT {0} FROM RPA_Lookup", columnSQL), true);
            return myBrowseTable;
        }

        public override DataTable LoadDealerListing(string fromCode, string toCode)
        {
            return myDBSetting.GetDataTable("SELECT * FROM  RPA_Lookup WHERE Code>=? AND Code<=?", false, fromCode, toCode);
        }

        protected override DataSet LoadData(string Code)
        {
            SqlConnection conn = new SqlConnection(myDBSetting.ConnectionString);

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("SELECT * FROM RPA_Lookup WHERE Code=@Code", conn);
                cmd.Parameters.AddWithValue("@Code", Code);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                if (Code.Length == 0)
                    adapter.FillSchema(ds, SchemaType.Mapped, "RPA_Lookup");
                else
                    adapter.Fill(ds, "RPA_Lookup");
                return ds;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        public override void Delete(string Code)
        {
            myDBSetting.ExecuteNonQuery("DELETE FROM RPA_Lookup WHERE Code=?", Code);

            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(Code);
            if (r != null)
                r.Delete();
            myBrowseTable.AcceptChanges();
        }

        protected override void SaveData(DataSet ds)
        {
            myDBSetting.SimpleSaveDataSet(ds, "RPA_Lookup", "SELECT * FROM RPA_Lookup");
        }
    }

    public class LookupEntity
    {
        private Lookup myLookup;
        internal DataSet myDataSet;
        private DataRow myRow;

        public LookupEntity(Lookup aLookup, DataSet ds)
        {
            myLookup = aLookup;
            myDataSet = ds;
            myRow = myDataSet.Tables[0].Rows[0];
        }

        public Lookup Lookup
        {
            get { return myLookup; }
        }

        internal DataRow Row
        {
            get { return myRow; }
        }

        public void Save()
        {
            myLookup.SaveEntity(this);
        }

        public bool IsModified
        {
            get { return myDataSet.GetChanges() != null; }
        }

        public DataTable LookupTable
        {
            get { return myDataSet.Tables[0]; }
        }

        public string Code
        {
            get { return myRow["Code"].ToString(); }
            set { myRow["Code"] = value; }
        }

        public object Description
        {
            get { return myRow["Description"]; }
            set { myRow["Description"] = value; }
        }

        public object Desc2
        {
            get { return myRow["Desc2"]; }
            set { myRow["Desc2"] = value; }
        }
        public object TransType
        {
            get { return myRow["TransType"]; }
            set { myRow["TransType"] = value; }
        }
        public object Condition
        {
            get { return myRow["Condition"]; }
            set { myRow["Condition"] = value; }
        }
        public bool IsActive
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myRow["IsActive"]);
            }
            set
            {
                this.myRow["IsActive"] = (object)BCE.Data.Convert.BooleanToText(value);
            }
        }
       
    }

    public class EmptyCodeException : BCE.Application.AppException
    {
        public EmptyCodeException()
            : base("Empty Code is not allowed.")
        {
        }
    }
}
