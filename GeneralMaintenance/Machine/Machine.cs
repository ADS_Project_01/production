﻿using System;
using System.Data;
using System.Data.SqlClient;
using BCE.Data;

namespace Production.GeneralMaintenance
{
    /// <summary>
    /// Summary description for Machine
    /// </summary>
    public class Machine
    {
        protected DBSetting myDBSetting;
        protected DataTable myBrowseTable;

        internal Machine()
        {
            myBrowseTable = new DataTable();
        }

        public DBSetting DBSetting
        {
            get { return myDBSetting; }
        }

        public static Machine Create(DBSetting dbSetting)
        {
            Machine aMachine;
            if (dbSetting.ServerType == DBServerType.SQL2000)
                aMachine = new MachineSQL();
            else
                throw new ArgumentException("Server type: " + dbSetting.ServerType + " not supported.");

            aMachine.myDBSetting = dbSetting;

            return aMachine;
        }

        public DataTable BrowseTable
        {
            get { return myBrowseTable; }
        }

        public virtual DataTable LoadBrowseTable(string columnSQL)
        {
            return null;
        }

        public virtual DataTable LoadDealerListing(string fromMachineCode, string toMachineCode)
        {
            return null;
        }

        public MachineEntity NewEntity()
        {
            DataSet ds = LoadData("");

            DataRow row = ds.Tables[0].NewRow();
            //object obj = myDBSetting.ExecuteScalar("select top 1 MachineCode from Machine order by MachineCode Desc");
            //if (obj != null && obj != DBNull.Value)
            //{
            //    row["MachineCode"] = (1 + BCE.Data.Convert.ToDecimal(obj)).ToString();
            //}
            //else
            //{
            //    row["MachineCode"]= (1).ToString();
            //}
            row["MachineCode"] = "";
            row["IsActive"] = BCE.Data.Convert.BooleanToText(true);
            ds.Tables[0].Rows.Add(row);
            try { ds.Tables[0].Constraints.Add("MachineCode", myBrowseTable.Columns["MachineCode"], true); } catch { }

            return new MachineEntity(this, ds);
        }

        public MachineEntity GetEntity(string MachineCode)
        {
            DataSet ds = LoadData(MachineCode);

            if (ds.Tables[0].Rows.Count == 0)
                return null;

            return new MachineEntity(this, ds);
        }

        public void SaveEntity(MachineEntity entity)
        {
            if (entity.MachineCode.Length == 0)
                throw new EmptyMachineCodeException();
            SaveData(entity.myDataSet);

            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(entity.MachineCode);
            if (r == null)
            {
                r = myBrowseTable.NewRow();
                foreach (DataColumn col in entity.MachineTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
                myBrowseTable.Rows.Add(r);
            }
            else
            {
                foreach (DataColumn col in entity.MachineTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
            }
            myBrowseTable.AcceptChanges();
        }

        protected virtual DataSet LoadData(string MachineCode)
        {
            return null;
        }

        public virtual void Delete(string MachineCode)
        {
        }

        protected virtual void SaveData(DataSet ds)
        {
        }
    }

    public class MachineSQL : Machine
    {
        public override DataTable LoadBrowseTable(string columnSQL)
        {
            myBrowseTable.Clear();
            myDBSetting.LoadDataTable(myBrowseTable, string.Format("SELECT {0} FROM RPA_Machine", columnSQL), true);
            return myBrowseTable;
        }

        public override DataTable LoadDealerListing(string fromMachineCode, string toMachineCode)
        {
            return myDBSetting.GetDataTable("SELECT * FROM  RPA_Machine WHERE MachineCode>=? AND MachineCode<=?", false, fromMachineCode, toMachineCode);
        }

        protected override DataSet LoadData(string MachineCode)
        {
            SqlConnection conn = new SqlConnection(myDBSetting.ConnectionString);

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("SELECT * FROM RPA_Machine WHERE MachineCode=@MachineCode", conn);
                cmd.Parameters.AddWithValue("@MachineCode", MachineCode);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                if (MachineCode.Length == 0)
                    adapter.FillSchema(ds, SchemaType.Mapped, "RPA_Machine");
                else
                    adapter.Fill(ds, "RPA_Machine");
                return ds;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        public override void Delete(string MachineCode)
        {
            myDBSetting.ExecuteNonQuery("DELETE FROM RPA_Machine WHERE MachineCode=?", MachineCode);

            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(MachineCode);
            if (r != null)
                r.Delete();
            myBrowseTable.AcceptChanges();
        }

        protected override void SaveData(DataSet ds)
        {
            myDBSetting.SimpleSaveDataSet(ds, "RPA_Machine", "SELECT * FROM RPA_Machine");
        }
    }

    public class MachineEntity
    {
        private Machine myMachine;
        internal DataSet myDataSet;
        private DataRow myRow;

        public MachineEntity(Machine aMachine, DataSet ds)
        {
            myMachine = aMachine;
            myDataSet = ds;
            myRow = myDataSet.Tables[0].Rows[0];
        }

        public Machine Machine
        {
            get { return myMachine; }
        }

        internal DataRow Row
        {
            get { return myRow; }
        }

        public void Save()
        {
            myMachine.SaveEntity(this);
        }

        public bool IsModified
        {
            get { return myDataSet.GetChanges() != null; }
        }

        public DataTable MachineTable
        {
            get { return myDataSet.Tables[0]; }
        }

        public string MachineCode
        {
            get { return myRow["MachineCode"].ToString(); }
            set { myRow["MachineCode"] = value; }
        }

        public object Description
        {
            get { return myRow["Description"]; }
            set { myRow["Description"] = value; }
        }

        public object Desc2
        {
            get { return myRow["Desc2"]; }
            set { myRow["Desc2"] = value; }
        }
       
        public bool IsActive
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myRow["IsActive"]);
            }
            set
            {
                this.myRow["IsActive"] = (object)BCE.Data.Convert.BooleanToText(value);
            }
        }
       
    }

    public class EmptyMachineCodeException : BCE.Application.AppException
    {
        public EmptyMachineCodeException()
            : base("Empty Machine is not allowed.")
        {
        }
    }
}
