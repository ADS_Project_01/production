﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using BCE.Data;
namespace Production.GeneralMaintenance
{
    /// <summary>
    /// Summary description for FormMachineEntry.
    /// </summary>
    public class FormMachineEntry : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit edtMachine;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit edtMachineName;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sbtnOK;
        private DevExpress.XtraEditors.SimpleButton sbtnCancel;
        private MachineEntity myMachineEntity;
        private Label label3;
        private DBSetting myDBSetting = null;
        private DevExpress.XtraEditors.TextEdit txtDesc2;
        private DevExpress.XtraEditors.CheckEdit ceIsActive;
        private DevExpress.XtraEditors.LabelControl txtCheck;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public FormMachineEntry(MachineEntity entity, char cDoc, DBSetting dbsetting)
        {
            
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            myDBSetting = dbsetting;
            SetupLookupEdit();
            SetMachineEntity(entity);

            if (cDoc == 'N')
            {
                 edtMachine.Properties.ReadOnly = false;
                edtMachineName.Properties.ReadOnly = false;
                txtDesc2.Properties.ReadOnly = false;
                sbtnOK.Enabled = true;
            }
            else if (cDoc == 'E')
            {
                edtMachine.Properties.ReadOnly = true;
                edtMachineName.Properties.ReadOnly = false;
                txtDesc2.Properties.ReadOnly = false;
                sbtnOK.Enabled = true;

            }
            else if (cDoc == 'V')
            {
                edtMachine.Properties.ReadOnly = true;
                edtMachineName.Properties.ReadOnly = true;
                txtDesc2.Properties.ReadOnly = true;
                sbtnOK.Enabled = false;
            }
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.edtMachine = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.edtMachineName = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ceIsActive = new DevExpress.XtraEditors.CheckEdit();
            this.txtDesc2 = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtCheck = new DevExpress.XtraEditors.LabelControl();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.edtMachine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtMachineName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 23);
            this.label1.TabIndex = 8;
            this.label1.Text = "Machine Code";
            // 
            // edtMachine
            // 
            this.edtMachine.EditValue = "";
            this.edtMachine.Location = new System.Drawing.Point(116, 11);
            this.edtMachine.Name = "edtMachine";
            this.edtMachine.Properties.MaxLength = 30;
            this.edtMachine.Size = new System.Drawing.Size(176, 20);
            this.edtMachine.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Description";
            // 
            // edtMachineName
            // 
            this.edtMachineName.EditValue = "";
            this.edtMachineName.Location = new System.Drawing.Point(116, 37);
            this.edtMachineName.Name = "edtMachineName";
            this.edtMachineName.Properties.MaxLength = 100;
            this.edtMachineName.Size = new System.Drawing.Size(414, 20);
            this.edtMachineName.TabIndex = 3;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.ceIsActive);
            this.panelControl1.Controls.Add(this.edtMachine);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.txtDesc2);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.edtMachineName);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(542, 144);
            this.panelControl1.TabIndex = 0;
            // 
            // ceIsActive
            // 
            this.ceIsActive.Location = new System.Drawing.Point(453, 12);
            this.ceIsActive.Name = "ceIsActive";
            this.ceIsActive.Properties.Caption = "Is Active";
            this.ceIsActive.Properties.ValueChecked = "T";
            this.ceIsActive.Properties.ValueUnchecked = "F";
            this.ceIsActive.Size = new System.Drawing.Size(75, 19);
            this.ceIsActive.TabIndex = 11;
            this.ceIsActive.Validated += new System.EventHandler(this.checkEdit1_Validated);
            // 
            // txtDesc2
            // 
            this.txtDesc2.EditValue = "";
            this.txtDesc2.Location = new System.Drawing.Point(116, 63);
            this.txtDesc2.Name = "txtDesc2";
            this.txtDesc2.Properties.MaxLength = 30;
            this.txtDesc2.Size = new System.Drawing.Size(414, 20);
            this.txtDesc2.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(14, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Desc 2";
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.txtCheck);
            this.panelControl2.Controls.Add(this.sbtnCancel);
            this.panelControl2.Controls.Add(this.sbtnOK);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 104);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(542, 40);
            this.panelControl2.TabIndex = 1;
            // 
            // txtCheck
            // 
            this.txtCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtCheck.Location = new System.Drawing.Point(407, 9);
            this.txtCheck.Name = "txtCheck";
            this.txtCheck.Size = new System.Drawing.Size(0, 13);
            this.txtCheck.TabIndex = 12;
            this.txtCheck.TextChanged += new System.EventHandler(this.txtCheck_TextChanged);
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancel.Location = new System.Drawing.Point(455, 9);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.sbtnCancel.TabIndex = 1;
            this.sbtnCancel.Text = "&Cancel";
            this.sbtnCancel.Click += new System.EventHandler(this.sbtnCancel_Click);
            // 
            // sbtnOK
            // 
            this.sbtnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnOK.Location = new System.Drawing.Point(374, 9);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(75, 23);
            this.sbtnOK.TabIndex = 0;
            this.sbtnOK.Text = "&OK";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // FormMachineEntry
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(542, 144);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "FormMachineEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Machine Entry";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormMachineEntry_Closing);
            this.Load += new System.EventHandler(this.FormMachineEntry_Load);
            ((System.ComponentModel.ISupportInitialize)(this.edtMachine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtMachineName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ceIsActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private void SetMachineEntity(MachineEntity entity)
        {
            if (entity != myMachineEntity)
            {
                myMachineEntity = entity;
                BindingMasterData();
                SetupLookupEdit();
            }
        }

        private void BindingMasterData()
        {
            edtMachine.DataBindings.Clear();
            edtMachine.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", myMachineEntity.MachineTable, "MachineCode"));
            edtMachineName.DataBindings.Clear();
            edtMachineName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", myMachineEntity.MachineTable, "Description"));
            txtDesc2.DataBindings.Clear();
            txtDesc2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", myMachineEntity.MachineTable, "Desc2"));
            BCE.Controls.Utils.Bind((Control)this.ceIsActive, (object)this.myMachineEntity.MachineTable, "IsActive");
        }

        private void SetupLookupEdit()
        {
            //BCE.AutoCount.XtraUtils.DefaultLookupEditBuilder.DebtorLookupEditBuilder.BuildLookupEdit(luEdtDebtorCode.Properties);
            //BCE.AutoCount.XtraUtils.DefaultLookupEditBuilder.AreaLookupEditBuilder.BuildLookupEdit(luEdtAreaCode.Properties);
            //  BCE.AutoCount.XtraUtils.DefaultLookupEditBuilder.ProjectLookupEditBuilder.BuildLookupEdit(luProject.Properties,myDBSetting);
        }

        private void EndCurrentEdit()
        {
            edtMachine.BindingManager.EndCurrentEdit();
        }

        private bool Save()
        {
            try
            {
                myMachineEntity.Save();
            }
            catch (BCE.Application.AppException ex)
            {
                if (ex.Message == "Primary Key Error")
                    BCE.Application.AppMessage.ShowErrorMessage("Duplicate Machine");
                else
                    BCE.Application.AppMessage.ShowErrorMessage(ex.Message);
                return false;

            }
            return true;
        }

        private void sbtnOK_Click(object sender, System.EventArgs e)
        {

            EndCurrentEdit();

            if (Save())
                Close();
        }

        private void sbtnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void FormMachineEntry_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EndCurrentEdit();

            if (myMachineEntity.IsModified)
            {
                DialogResult dr = BCE.Application.AppMessage.ShowConfirmSaveChangesMessage();

                if (dr == DialogResult.Cancel)
                    e.Cancel = true;
                else if (dr == DialogResult.Yes)
                    Save();
            }
        }

        private void FormMachineEntry_Load(object sender, System.EventArgs e)
        {
            //luEdtDebtorCode.EditValueChanged += new System.EventHandler(luEdtDebtorCode_EditValueChanged);
        }

        private void checkEdit1_Validated(object sender, EventArgs e)
        {
          //  myMachineEntity.IsActive = ceValidation.Checked;
        }

        private void txtCheck_TextChanged(object sender, EventArgs e)
        {
           // ceIsActive.Checked = BCE.Data.Convert.TextToBoolean(txtCheck.Text);
        }
    }
}
