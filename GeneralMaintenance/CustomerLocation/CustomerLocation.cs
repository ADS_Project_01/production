﻿using System;
using System.Data;
using System.Data.SqlClient;
using BCE.Data;

namespace Production.GeneralMaintenance.CustomerLocation
{
    /// <summary>
    /// Summary description for CustomerLocation
    /// </summary>
    public class CustomerLocation
    {
        protected DBSetting myDBSetting;
        protected DataTable myBrowseTable;

        internal CustomerLocation()
        {
            myBrowseTable = new DataTable();
        }
        internal virtual DataRow LoadDebtorData(string debtorcode)
        {
            return (DataRow)null;
        }
        internal virtual DataRow LoadLocationData(string location)
        {
            return (DataRow)null;
        }
        public DBSetting DBSetting
        {
            get { return myDBSetting; }
        }

        public static CustomerLocation Create(DBSetting dbSetting)
        {
            CustomerLocation aCustomerLocation;
            if (dbSetting.ServerType == DBServerType.SQL2000)
                aCustomerLocation = new CustomerLocationSQL();
            else
                throw new ArgumentException("Server type: " + dbSetting.ServerType + " not supported.");

            aCustomerLocation.myDBSetting = dbSetting;

            return aCustomerLocation;
        }

        public DataTable BrowseTable
        {
            get { return myBrowseTable; }
        }

        public virtual DataTable LoadBrowseTable(string columnSQL)
        {
            return null;
        }

        public virtual DataTable LoadDealerListing(string fromCustomerLocationCode, string toCustomerLocationCode)
        {
            return null;
        }

        public CustomerLocationEntity NewEntity()
        {
            DataSet ds = LoadData("","");

            DataRow row = ds.Tables[0].NewRow();
            //object obj = myDBSetting.ExecuteScalar("select top 1 CustomerLocationCode from CustomerLocation order by CustomerLocationCode Desc");
            //if (obj != null && obj != DBNull.Value)
            //{
            //    row["CustomerLocationCode"] = (1 + BCE.Data.Convert.ToDecimal(obj)).ToString();
            //}
            //else
            //{
            //    row["CustomerLocationCode"]= (1).ToString();
            //}
            row["DebtorCode"] = "";
            row["Location"] = "";
            row["PricePerDay"] = 0;
            row["IsActive"] = BCE.Data.Convert.BooleanToText(true);
            ds.Tables[0].Rows.Add(row);
            try { ds.Tables[0].Constraints.Add("DebtorCode", myBrowseTable.Columns["DebtorCode"], true);
                try { ds.Tables[0].Constraints.Add("Location", myBrowseTable.Columns["Location"], true); } catch { }
            } catch { }

            return new CustomerLocationEntity(this, ds);
        }

        public CustomerLocationEntity GetEntity(string DebtorCode, string Location)
        {

            DataSet ds = LoadData(DebtorCode, Location);

            if (ds.Tables[0].Rows.Count == 0)
                return null;

            return new CustomerLocationEntity(this, ds);
        }

        public void SaveEntity(CustomerLocationEntity entity)
        {
            if (entity.DebtorCode.Length == 0 || entity.Location.Length == 0)
                throw new EmptyCustomerLocationCodeException();
            SaveData(entity.myDataSet);
            object[] objKeys = { entity.DebtorCode, entity.Location };
            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(objKeys);
            if (r == null)
            {
                r = myBrowseTable.NewRow();
                foreach (DataColumn col in entity.CustomerLocationTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
                myBrowseTable.Rows.Add(r);
            }
            else
            {
                foreach (DataColumn col in entity.CustomerLocationTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
            }
            myBrowseTable.AcceptChanges();
        }

        protected virtual DataSet LoadData(string DebtorCode,string Location)
        {
            return null;
        }

        public virtual void Delete(string DebtorCode,string Location)
        {
        }

        protected virtual void SaveData(DataSet ds)
        {
        }
    }

    public class CustomerLocationSQL : CustomerLocation
    {
        public override DataTable LoadBrowseTable(string columnSQL)
        {
            myBrowseTable.Clear();
            myDBSetting.LoadDataTable(myBrowseTable, string.Format("SELECT {0} FROM RPA_CustomerLocation", columnSQL), true);
            return myBrowseTable;
        }
        internal override DataRow LoadDebtorData(string debtorcode)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT CompanyName, CurrencyCode, Address1 FROM Debtor WHERE AccNo=?";
            object[] objArray = new object[1];
            int index = 0;
            string str = debtorcode;
            objArray[index] = (object)str;
            return dbSetting.GetFirstDataRow(cmdText, objArray);
        }
        internal override DataRow LoadLocationData(string location)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT Description FROM Location WHERE Location=?";
            object[] objArray = new object[1];
            int index = 0;
            string str = location;
            objArray[index] = (object)str;
            return dbSetting.GetFirstDataRow(cmdText, objArray);
        }
        public override DataTable LoadDealerListing(string fromDebtorCode, string toDebtorCode)
        {
            return myDBSetting.GetDataTable("SELECT * FROM  RPA_CustomerLocation WHERE DebtorCode>=? AND DebtorCode<=?", false, fromDebtorCode, toDebtorCode);
        }

        protected override DataSet LoadData(string DebtorCode,string Location)
        {
            SqlConnection conn = new SqlConnection(myDBSetting.ConnectionString);

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("SELECT * FROM RPA_CustomerLocation WHERE DebtorCode=@DebtorCode and Location=@Location", conn);
                cmd.Parameters.AddWithValue("@DebtorCode", DebtorCode);
                cmd.Parameters.AddWithValue("@Location", Location);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                if (DebtorCode.Length == 0 || Location.Length == 0)
                    adapter.FillSchema(ds, SchemaType.Mapped, "RPA_CustomerLocation");
                else
                    adapter.Fill(ds, "RPA_CustomerLocation");
                return ds;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        public override void Delete(string DebtorCode,string Location)
        {
            myDBSetting.ExecuteNonQuery("DELETE FROM RPA_CustomerLocation WHERE DebtorCode=? and Location=?",(object) DebtorCode, (object)Location);
            object[] objKeys = { DebtorCode, Location };
            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(objKeys);
            if (r != null)
                r.Delete();
            myBrowseTable.AcceptChanges();
        }

        protected override void SaveData(DataSet ds)
        {
            myDBSetting.SimpleSaveDataSet(ds, "RPA_CustomerLocation", "SELECT * FROM RPA_CustomerLocation");
        }
    }

    public class CustomerLocationEntity
    {
        private CustomerLocation myCustomerLocation;
        internal DataSet myDataSet;
        private DataRow myRow;

        public CustomerLocationEntity(CustomerLocation aCustomerLocation, DataSet ds)
        {
            myCustomerLocation = aCustomerLocation;
            myDataSet = ds;
            myRow = myDataSet.Tables[0].Rows[0];
            this.CustomerLocationTable.ColumnChanged += new DataColumnChangeEventHandler(this.myMasterTable_ColumnChanged);

        }
        private void myMasterTable_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            if (string.Compare(e.Column.ColumnName, "DebtorCode", true) == 0)
            {
                e.Row.BeginEdit();

                //e.Row["CurrencyRate"] = (object)1;
                //if (e.Row["AccDesc"] == DBNull.Value)
                //    e.Row["AccDesc"] = this.myRow["Description"];
                DataRow dataRow = this.myCustomerLocation.LoadDebtorData(e.Row[e.Column].ToString());
                if (dataRow != null)
                {
                    e.Row["DebtorName"] = dataRow["CompanyName"];
                }
                e.Row.EndEdit();
            }
            else if (string.Compare(e.Column.ColumnName, "Location", true) == 0)
            {
                e.Row.BeginEdit();

                //e.Row["CurrencyRate"] = (object)1;
                //if (e.Row["AccDesc"] == DBNull.Value)
                //    e.Row["AccDesc"] = this.myRow["Description"];
                DataRow dataRow = this.myCustomerLocation.LoadLocationData(e.Row[e.Column].ToString());
                if (dataRow != null)
                {
                    e.Row["LocationDesc"] = dataRow["Description"];
                }
                e.Row.EndEdit();
            }

        }
        public CustomerLocation CustomerLocation
        {
            get { return myCustomerLocation; }
        }

        internal DataRow Row
        {
            get { return myRow; }
        }

        public void Save()
        {
            myCustomerLocation.SaveEntity(this);
        }

        public bool IsModified
        {
            get { return myDataSet.GetChanges() != null; }
        }

        public DataTable CustomerLocationTable
        {
            get { return myDataSet.Tables[0]; }
        }

        public string DebtorCode
        {
            get { return myRow["DebtorCode"].ToString(); }
            set { myRow["DebtorCode"] = value; }
        }

        public object DebtorName
        {
            get { return myRow["DebtorName"]; }
            set { myRow["DebtorName"] = value; }
        }
        public string Location
        {
            get { return myRow["Location"].ToString(); }
            set { myRow["Location"] = value; }
        }

        public object LocationDesc
        {
            get { return myRow["LocationDesc"]; }
            set { myRow["LocationDesc"] = value; }
        }
        public object Description
        {
            get { return myRow["Description"]; }
            set { myRow["Description"] = value; }
        }
  
        public DBDecimal PricePerDay
        {
            get
            {
                return BCE.Data.Convert.ToDBDecimal(this.myRow["PricePerDay"]);
            }
            set
            {
                this.myRow["PricePerDay"] = (object)BCE.Data.Convert.ToDBDecimal(value);
            }
        }
        public bool IsActive
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myRow["IsActive"]);
            }
            set
            {
                this.myRow["IsActive"] = (object)BCE.Data.Convert.BooleanToText(value);
            }
        }
       
    }

    public class EmptyCustomerLocationCodeException : BCE.Application.AppException
    {
        public EmptyCustomerLocationCodeException()
            : base("Empty Debtor Code n Location is not allowed.")
        {
        }
    }
}
