﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using BCE.Data;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using BCE.AutoCount.Authentication;
namespace Production.GeneralMaintenance.CustomerLocation
{
    /// <summary>
    /// Summary description for FormSampleCustomerLocationMaint.
    /// </summary>
    ///

    //[BCE.AutoCount.PlugIn.MenuItem("CustomerLocation Maintenance",1,false, "RPA_CUSTOMERLOC_OPEN", "RPA_CUSTOMERLOC_SHOW")]
    //  [BCE.AutoCount.PlugIns.PlugInsEntryPoint("Pelihara Mobil", 2, false, "BG_CAR_SHOW", "BG_CAR_OPEN", "Maintenance", 1, false)]
    //[BCE.AutoCount.PlugIn.MenuItem("CustomerLocation Maintenance", MenuOrder = 10)]
   // [BCE.AutoCount.PlugIn.MenuItem("Customer Location Maintenance", BeginNewGroup = false, MenuOrder = 2, OpenAccessRight = "RPA_GEN_CUSTOMERLOC_OPEN", ParentBeginNewGroup = false, ParentMenuCaption = "General Maintenance", ParentMenuOrder = 1, ShowAsDialog = false, VisibleAccessRight = "RPA_GEN_CUSTOMERLOC_SHOW")]

    //[BCE.AutoCount.PlugIn.MenuItem("Customer Location Maintenance", 2, false, "RPA_GEN_CUSTOMERLOC_SHOW", "RPA_GEN_CUSTOMERLOC_OPEN")]
    //[BCE.Application.SingleInstanceThreadForm]
    public class FormCustomerLocationMaint : DevExpress.XtraEditors.XtraForm
    {
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton sbtnNew;
        private DevExpress.XtraEditors.SimpleButton sbtnEdit;
        private DevExpress.XtraEditors.SimpleButton sbtnDelete;
        private DevExpress.XtraEditors.SimpleButton sbtnRefresh;
        private CustomerLocation mySampleCustomerLocation;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private char cDoc = ' ';
        private DBSetting myDBSetting = null;
        private DevExpress.XtraEditors.SimpleButton sbtView;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colDebtorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colIsActive;
        private DevExpress.XtraGrid.GridControl gcMain;
        protected UserAuthentication myUserAuthentication;
        private DevExpress.XtraGrid.Columns.GridColumn colDebtorName;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colPricePerDay;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public FormCustomerLocationMaint(DBSetting dbSetting)
        {
            myDBSetting = dbSetting;
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            //new BCE.AutoCount.Controls.PanelHeader(this, "Show Sales Invoice", "");
            this.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            mySampleCustomerLocation =CustomerLocationSQL.Create(dbSetting);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sbtView = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnNew = new DevExpress.XtraEditors.SimpleButton();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDebtorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDebtorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPricePerDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMain = new DevExpress.XtraGrid.GridControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMain)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.sbtView);
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.sbtnRefresh);
            this.panelControl1.Controls.Add(this.sbtnDelete);
            this.panelControl1.Controls.Add(this.sbtnEdit);
            this.panelControl1.Controls.Add(this.sbtnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(792, 41);
            this.panelControl1.TabIndex = 0;
            // 
            // sbtView
            // 
            this.sbtView.Appearance.ForeColor = System.Drawing.Color.Black;
            this.sbtView.Appearance.Options.UseForeColor = true;
            this.sbtView.Location = new System.Drawing.Point(171, 9);
            this.sbtView.Name = "sbtView";
            this.sbtView.Size = new System.Drawing.Size(75, 24);
            this.sbtView.TabIndex = 9;
            this.sbtView.Text = "View";
            this.sbtView.Click += new System.EventHandler(this.sbtView_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(412, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 24);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // sbtnRefresh
            // 
            this.sbtnRefresh.Location = new System.Drawing.Point(331, 9);
            this.sbtnRefresh.Name = "sbtnRefresh";
            this.sbtnRefresh.Size = new System.Drawing.Size(75, 24);
            this.sbtnRefresh.TabIndex = 3;
            this.sbtnRefresh.Text = "Refresh";
            this.sbtnRefresh.Click += new System.EventHandler(this.sbtnRefresh_Click);
            // 
            // sbtnDelete
            // 
            this.sbtnDelete.Appearance.ForeColor = System.Drawing.Color.Red;
            this.sbtnDelete.Appearance.Options.UseForeColor = true;
            this.sbtnDelete.Location = new System.Drawing.Point(251, 9);
            this.sbtnDelete.Name = "sbtnDelete";
            this.sbtnDelete.Size = new System.Drawing.Size(75, 24);
            this.sbtnDelete.TabIndex = 2;
            this.sbtnDelete.Text = "Delete";
            this.sbtnDelete.Click += new System.EventHandler(this.sbtnDelete_Click);
            // 
            // sbtnEdit
            // 
            this.sbtnEdit.Location = new System.Drawing.Point(90, 9);
            this.sbtnEdit.Name = "sbtnEdit";
            this.sbtnEdit.Size = new System.Drawing.Size(75, 24);
            this.sbtnEdit.TabIndex = 1;
            this.sbtnEdit.Text = "Edit";
            this.sbtnEdit.Click += new System.EventHandler(this.sbtnEdit_Click);
            // 
            // sbtnNew
            // 
            this.sbtnNew.Location = new System.Drawing.Point(10, 9);
            this.sbtnNew.Name = "sbtnNew";
            this.sbtnNew.Size = new System.Drawing.Size(75, 24);
            this.sbtnNew.TabIndex = 0;
            this.sbtnNew.Text = "New";
            this.sbtnNew.Click += new System.EventHandler(this.sbtnNew_Click);
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = "T";
            this.repositoryItemCheckEdit1.ValueUnchecked = "F";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDebtorCode,
            this.colDebtorName,
            this.colLocation,
            this.colLocationDesc,
            this.colDescription,
            this.colPricePerDay,
            this.colIsActive});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1140, 455, 216, 178);
            this.gridView1.GridControl = this.gcMain;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            // 
            // colDebtorCode
            // 
            this.colDebtorCode.Caption = "DebtorCode";
            this.colDebtorCode.FieldName = "DebtorCode";
            this.colDebtorCode.Name = "colDebtorCode";
            this.colDebtorCode.Visible = true;
            this.colDebtorCode.VisibleIndex = 0;
            this.colDebtorCode.Width = 85;
            // 
            // colDebtorName
            // 
            this.colDebtorName.Caption = "DebtorName";
            this.colDebtorName.FieldName = "DebtorName";
            this.colDebtorName.Name = "colDebtorName";
            this.colDebtorName.Visible = true;
            this.colDebtorName.VisibleIndex = 1;
            this.colDebtorName.Width = 149;
            // 
            // colLocation
            // 
            this.colLocation.Caption = "Location";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 2;
            // 
            // colLocationDesc
            // 
            this.colLocationDesc.Caption = "LocationDesc";
            this.colLocationDesc.FieldName = "LocationDesc";
            this.colLocationDesc.Name = "colLocationDesc";
            this.colLocationDesc.Visible = true;
            this.colLocationDesc.VisibleIndex = 3;
            this.colLocationDesc.Width = 139;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 4;
            this.colDescription.Width = 186;
            // 
            // colPricePerDay
            // 
            this.colPricePerDay.Caption = "PricePerDay";
            this.colPricePerDay.FieldName = "PricePerDay";
            this.colPricePerDay.Name = "colPricePerDay";
            this.colPricePerDay.Visible = true;
            this.colPricePerDay.VisibleIndex = 5;
            this.colPricePerDay.Width = 86;
            // 
            // colIsActive
            // 
            this.colIsActive.Caption = "Is Active";
            this.colIsActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsActive.FieldName = "IsActive";
            this.colIsActive.Name = "colIsActive";
            this.colIsActive.Visible = true;
            this.colIsActive.VisibleIndex = 6;
            this.colIsActive.Width = 54;
            // 
            // gcMain
            // 
            this.gcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcMain.Location = new System.Drawing.Point(0, 41);
            this.gcMain.MainView = this.gridView1;
            this.gcMain.Name = "gcMain";
            this.gcMain.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gcMain.Size = new System.Drawing.Size(792, 491);
            this.gcMain.TabIndex = 1;
            this.gcMain.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // FormCustomerLocationMaint
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(792, 532);
            this.Controls.Add(this.gcMain);
            this.Controls.Add(this.panelControl1);
            this.Name = "FormCustomerLocationMaint";
            this.Text = "Customer Location Maintenance";
            this.Load += new System.EventHandler(this.FormSampleCustomerLocationMaint_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMain)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private void FormSampleCustomerLocationMaint_Load(object sender, System.EventArgs e)
        {
            BCE.AutoCount.XtraUtils.FormControlUtil formControlUtil = new BCE.AutoCount.XtraUtils.FormControlUtil(myDBSetting);
            formControlUtil.AddField("PricePerDay", "SalesPrice");
            formControlUtil.AddField("IsActive", "Boolean");
            formControlUtil.InitControls((Control)this);
            new BCE.AutoCount.Controls.PanelHeader(this, "Customer Location Maintenance", "Here you can maintain the Customer Location for the RPA PlugIns module.");
            new BCE.AutoCount.XtraUtils.CustomizeGridLayout(myDBSetting, this.Name, gridView1);
            string colSQL = BCE.AutoCount.XtraUtils.ColumnViewUtils.BuildSQLColumnListFromColumnView(gridView1, "");
            gcMain.DataSource = mySampleCustomerLocation.LoadBrowseTable(colSQL);
          
        }

        private void sbtnNew_Click(object sender, System.EventArgs e)
        {
            if (myUserAuthentication.AccessRight.IsAccessible(AccessRightConst.RPA_GEN_CUSTOMERLOC_NEW, true))
            {
                cDoc = 'N';
                CustomerLocationEntity entity;
                try
                {
                    entity = mySampleCustomerLocation.NewEntity();
                }
                catch (DataAccessException ex)
                {
                    BCE.Application.AppMessage.ShowErrorMessage(ex.Message);
                    return;
                }

                FormCustomerLocationEntry form = new FormCustomerLocationEntry(entity, cDoc, myDBSetting);
                form.ShowDialog();
            }
        }

        private void sbtnDelete_Click(object sender, System.EventArgs e)
        {
            if (myUserAuthentication.AccessRight.IsAccessible(AccessRightConst.RPA_GEN_CUSTOMERLOC_DELETE, true))
            {
                DataRow r = gridView1.GetDataRow(gridView1.FocusedRowHandle);

                if (r != null)
                {
                    if (!BCE.Application.AppMessage.ShowConfirmMessage(string.Format("Do you want to delete Customer Location {0}?", r["DebtorCode"].ToString() + " " + r["Location"].ToString())))
                        return;

                    try
                    {

                        mySampleCustomerLocation.Delete(r["DebtorCode"].ToString(),r["Location"].ToString());
                    }
                    catch (DataAccessException ex)
                    {
                        BCE.Application.AppMessage.ShowErrorMessage(ex.Message);
                    }
                }
            }
        }

        private void sbtnEdit_Click(object sender, System.EventArgs e)
        {
            if (myUserAuthentication.AccessRight.IsAccessible(AccessRightConst.RPA_GEN_CUSTOMERLOC_EDIT, true))
            {
                cDoc = 'E';
                DataRow r = gridView1.GetDataRow(gridView1.FocusedRowHandle);

                if (r != null)
                {
                    CustomerLocationEntity entity;
                    try
                    {
                        entity = mySampleCustomerLocation.GetEntity(r["DebtorCode"].ToString(), r["Location"].ToString());
                    }
                    catch (DataAccessException ex)
                    {
                        BCE.Application.AppMessage.ShowErrorMessage(ex.Message);
                        return;
                    }

                    FormCustomerLocationEntry form = new FormCustomerLocationEntry(entity, cDoc, myDBSetting);
                    form.ShowDialog();
                }
            }
        }
      

        private void sbtnRefresh_Click(object sender, System.EventArgs e)
        {
            string colSQL = BCE.AutoCount.XtraUtils.ColumnViewUtils.BuildSQLColumnListFromColumnView(gridView1, "");
            gcMain.DataSource = mySampleCustomerLocation.LoadBrowseTable(colSQL);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtView_Click(object sender, EventArgs e)
        {
            if (myUserAuthentication.AccessRight.IsAccessible(AccessRightConst.RPA_GEN_CUSTOMERLOC_VIEW, true))
            { 
                cDoc = 'V';
            DataRow r = gridView1.GetDataRow(gridView1.FocusedRowHandle);

                if (r != null)
                {
                    CustomerLocationEntity entity;
                    try
                    {
                        entity = mySampleCustomerLocation.GetEntity(r["DebtorCode"].ToString(), r["Location"].ToString());
                    }
                    catch (DataAccessException ex)
                    {
                        BCE.Application.AppMessage.ShowErrorMessage(ex.Message);
                        return;
                    }

                    FormCustomerLocationEntry form = new FormCustomerLocationEntry(entity, cDoc, myDBSetting);
                    form.ShowDialog();
                }
            }
        }
    }
}
