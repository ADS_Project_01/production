﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using BCE.Data;
using Production.XtraUtils;
namespace Production.GeneralMaintenance.CustomerLocation
{
    /// <summary>
    /// Summary description for FormCustomerLocationEntry.
    /// </summary>
    public class FormCustomerLocationEntry : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txtDescription;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sbtnOK;
        private DevExpress.XtraEditors.SimpleButton sbtnCancel;
        private CustomerLocationEntity myCustomerLocationEntity;
        private Label label3;
        private DBSetting myDBSetting = null;
        private DevExpress.XtraEditors.TextEdit txtDebtorName;
        private DevExpress.XtraEditors.CheckEdit ceIsActive;
        private DevExpress.XtraEditors.LabelControl txtCheck;
        private Label label4;
        private DevExpress.XtraEditors.LookUpEdit luDebtorCode;
        private Label label7;
        private Label label5;
        private DevExpress.XtraEditors.LookUpEdit luLocation;
        private DevExpress.XtraEditors.TextEdit txtLocDescription;
        private Label label6;
        private DevExpress.XtraEditors.TextEdit txtPrice;
        private Label label1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public FormCustomerLocationEntry(CustomerLocationEntity entity, char cDoc, DBSetting dbsetting)
        {
            
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            myDBSetting = dbsetting;
            SetupLookupEdit();
            SetCustomerLocationEntity(entity);
            this.InitFormControls();
            if (cDoc == 'N')
            {
                //  edtCustomerLocation.Properties.ReadOnly = false;
                txtDescription.Properties.ReadOnly = false;
                luDebtorCode.Properties.ReadOnly = false;
                luLocation.Properties.ReadOnly = false;
                txtPrice.Properties.ReadOnly = false;
                sbtnOK.Enabled = true;
                luDebtorCode.Enabled = true;

            }
            else if (cDoc == 'E')
            {
                // edtCustomerLocation.Properties.ReadOnly = true;
                txtDescription.Properties.ReadOnly = false;
                luDebtorCode.Properties.ReadOnly = false;
                luLocation.Properties.ReadOnly = false;
                txtPrice.Properties.ReadOnly = false;
                sbtnOK.Enabled = true;
                luDebtorCode.Enabled = true;

            }
            else if (cDoc == 'V')
            {
                // edtCustomerLocation.Properties.ReadOnly = true;
                txtDescription.Properties.ReadOnly = true;
                luDebtorCode.Properties.ReadOnly = true;
                luLocation.Properties.ReadOnly = true;
                txtPrice.Properties.ReadOnly = true;
                sbtnOK.Enabled = false;
                luDebtorCode.Enabled = false;
            }
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        private void InitFormControls()
        {
            //FormCustomerLocationEntry thisEntry = this;
            BCE.AutoCount.XtraUtils.FormControlUtil formControlUtil = new BCE.AutoCount.XtraUtils.FormControlUtil(myDBSetting);
            formControlUtil.AddField("PricePerDay", "SalesPrice");
            formControlUtil.AddField("IsActive", "Boolean");
            FormCustomerLocationEntry purchaseOrderEntry = this;
            formControlUtil.InitControls((Control)purchaseOrderEntry);

        }
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtDescription = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtPrice = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.luLocation = new DevExpress.XtraEditors.LookUpEdit();
            this.txtLocDescription = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.luDebtorCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ceIsActive = new DevExpress.XtraEditors.CheckEdit();
            this.txtDebtorName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtCheck = new DevExpress.XtraEditors.LabelControl();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luDebtorCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebtorName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Description";
            // 
            // txtDescription
            // 
            this.txtDescription.EditValue = "";
            this.txtDescription.Location = new System.Drawing.Point(116, 145);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Properties.MaxLength = 100;
            this.txtDescription.Size = new System.Drawing.Size(414, 20);
            this.txtDescription.TabIndex = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.txtPrice);
            this.panelControl1.Controls.Add(this.label7);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.luLocation);
            this.panelControl1.Controls.Add(this.txtLocDescription);
            this.panelControl1.Controls.Add(this.label6);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.luDebtorCode);
            this.panelControl1.Controls.Add(this.ceIsActive);
            this.panelControl1.Controls.Add(this.txtDebtorName);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.txtDescription);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(542, 212);
            this.panelControl1.TabIndex = 0;
            // 
            // txtPrice
            // 
            this.txtPrice.EditValue = "";
            this.txtPrice.Location = new System.Drawing.Point(116, 119);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Properties.MaxLength = 100;
            this.txtPrice.Size = new System.Drawing.Size(176, 20);
            this.txtPrice.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(14, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 16);
            this.label7.TabIndex = 18;
            this.label7.Text = "Price per Day";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(14, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 16);
            this.label5.TabIndex = 17;
            this.label5.Text = "Location";
            // 
            // luLocation
            // 
            this.luLocation.Location = new System.Drawing.Point(116, 67);
            this.luLocation.Name = "luLocation";
            this.luLocation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luLocation.Size = new System.Drawing.Size(176, 20);
            this.luLocation.TabIndex = 2;
            // 
            // txtLocDescription
            // 
            this.txtLocDescription.EditValue = "";
            this.txtLocDescription.Enabled = false;
            this.txtLocDescription.Location = new System.Drawing.Point(116, 93);
            this.txtLocDescription.Name = "txtLocDescription";
            this.txtLocDescription.Properties.MaxLength = 30;
            this.txtLocDescription.Size = new System.Drawing.Size(414, 20);
            this.txtLocDescription.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(14, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 16);
            this.label6.TabIndex = 16;
            this.label6.Text = "Location Desc";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(14, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 16);
            this.label4.TabIndex = 13;
            this.label4.Text = "Debtor Code";
            // 
            // luDebtorCode
            // 
            this.luDebtorCode.Location = new System.Drawing.Point(116, 15);
            this.luDebtorCode.Name = "luDebtorCode";
            this.luDebtorCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luDebtorCode.Size = new System.Drawing.Size(176, 20);
            this.luDebtorCode.TabIndex = 0;
            // 
            // ceIsActive
            // 
            this.ceIsActive.Location = new System.Drawing.Point(453, 12);
            this.ceIsActive.Name = "ceIsActive";
            this.ceIsActive.Properties.Caption = "Is Active";
            this.ceIsActive.Properties.ValueChecked = "T";
            this.ceIsActive.Properties.ValueUnchecked = "F";
            this.ceIsActive.Size = new System.Drawing.Size(75, 19);
            this.ceIsActive.TabIndex = 6;
            this.ceIsActive.Validated += new System.EventHandler(this.checkEdit1_Validated);
            // 
            // txtDebtorName
            // 
            this.txtDebtorName.EditValue = "";
            this.txtDebtorName.Enabled = false;
            this.txtDebtorName.Location = new System.Drawing.Point(116, 41);
            this.txtDebtorName.Name = "txtDebtorName";
            this.txtDebtorName.Properties.MaxLength = 30;
            this.txtDebtorName.Size = new System.Drawing.Size(414, 20);
            this.txtDebtorName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(14, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Debtor Name";
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.txtCheck);
            this.panelControl2.Controls.Add(this.sbtnCancel);
            this.panelControl2.Controls.Add(this.sbtnOK);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 172);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(542, 40);
            this.panelControl2.TabIndex = 1;
            // 
            // txtCheck
            // 
            this.txtCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtCheck.Location = new System.Drawing.Point(407, 9);
            this.txtCheck.Name = "txtCheck";
            this.txtCheck.Size = new System.Drawing.Size(0, 13);
            this.txtCheck.TabIndex = 12;
            this.txtCheck.TextChanged += new System.EventHandler(this.txtCheck_TextChanged);
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancel.Location = new System.Drawing.Point(455, 9);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.sbtnCancel.TabIndex = 8;
            this.sbtnCancel.Text = "&Cancel";
            this.sbtnCancel.Click += new System.EventHandler(this.sbtnCancel_Click);
            // 
            // sbtnOK
            // 
            this.sbtnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnOK.Location = new System.Drawing.Point(374, 9);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(75, 23);
            this.sbtnOK.TabIndex = 7;
            this.sbtnOK.Text = "&OK";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(298, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 14);
            this.label1.TabIndex = 19;
            this.label1.Text = "Per UOM (Satuan)";
            // 
            // FormCustomerLocationEntry
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(542, 212);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "FormCustomerLocationEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Location Entry";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormCustomerLocationEntry_Closing);
            this.Load += new System.EventHandler(this.FormCustomerLocationEntry_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luDebtorCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebtorName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private void SetCustomerLocationEntity(CustomerLocationEntity entity)
        {
            if (entity != myCustomerLocationEntity)
            {
                myCustomerLocationEntity = entity;
                BindingMasterData();
                SetupLookupEdit();
            }
        }

        private void BindingMasterData()
        {            
            luDebtorCode.DataBindings.Clear();
            luDebtorCode.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", myCustomerLocationEntity.CustomerLocationTable, "DebtorCode"));
            txtDebtorName.DataBindings.Clear();
            txtDebtorName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", myCustomerLocationEntity.CustomerLocationTable, "DebtorName"));
            txtDescription.DataBindings.Clear();
            txtDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", myCustomerLocationEntity.CustomerLocationTable, "Description"));
            luLocation.DataBindings.Clear();
            luLocation.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", myCustomerLocationEntity.CustomerLocationTable, "Location"));
            txtLocDescription.DataBindings.Clear();
            txtLocDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", myCustomerLocationEntity.CustomerLocationTable, "LocationDesc"));
            txtPrice.DataBindings.Clear();
            txtPrice.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", myCustomerLocationEntity.CustomerLocationTable, "PricePerDay"));

            BCE.Controls.Utils.Bind((Control)this.ceIsActive, (object)this.myCustomerLocationEntity.CustomerLocationTable, "IsActive");
        }

        private void SetupLookupEdit()
        {
            BCE.AutoCount.XtraUtils.DefaultLookupEditBuilder.GetOrCreate(myDBSetting).DebtorLookupEditBuilder.BuildLookupEdit(luDebtorCode.Properties,myDBSetting);
            //leDebtorBuilder luedtdebtor = new leDebtorBuilder();
            //luedtdebtor.BuildLookupEdit(luDebtorCode.Properties, myDBSetting);

            BCE.AutoCount.XtraUtils.DefaultLookupEditBuilder.GetOrCreate(myDBSetting).LocationLookupEditBuilder.BuildLookupEdit(luLocation.Properties, myDBSetting);
        }

        private void EndCurrentEdit()
        {
            luDebtorCode.BindingManager.EndCurrentEdit();
        }

        private bool Save()
        {
            try
            {
                myCustomerLocationEntity.Save();
            }
            catch (BCE.Application.AppException ex)
            {
                if (ex.Message == "Primary Key Error")
                    BCE.Application.AppMessage.ShowErrorMessage("Duplicate CustomerLocation");
                else
                    BCE.Application.AppMessage.ShowErrorMessage(ex.Message);
                return false;

            }
            return true;
        }

        private void sbtnOK_Click(object sender, System.EventArgs e)
        {
            

            if (ceIsActive.Checked)
            {
                object obj = myDBSetting.ExecuteScalar("select count(*) from RPA_CustomerLocation where Location=? and  DebtorCode!=? and IsActive='T'", (object)luLocation.EditValue, (object)luDebtorCode.EditValue);
                if (obj != null && obj != DBNull.Value)
                {
                    if (BCE.Data.Convert.ToDecimal(obj) > 0)
                    {
                        BCE.Application.AppMessage.ShowErrorMessage("this Location has been Assigned with another Customer. ");
                        return;
                    }
                }
            }
                EndCurrentEdit();

            if (Save())
                Close();
        }

        private void sbtnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void FormCustomerLocationEntry_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EndCurrentEdit();

            if (myCustomerLocationEntity.IsModified)
            {
                DialogResult dr = BCE.Application.AppMessage.ShowConfirmSaveChangesMessage();

                if (dr == DialogResult.Cancel)
                    e.Cancel = true;
                else if (dr == DialogResult.Yes)
                    Save();
            }
        }

        private void FormCustomerLocationEntry_Load(object sender, System.EventArgs e)
        {
            //luEdtDebtorCode.EditValueChanged += new System.EventHandler(luEdtDebtorCode_EditValueChanged);
        }

        private void checkEdit1_Validated(object sender, EventArgs e)
        {
          //  myCustomerLocationEntity.IsActive = ceValidation.Checked;
        }

        private void txtCheck_TextChanged(object sender, EventArgs e)
        {
           // ceIsActive.Checked = BCE.Data.Convert.TextToBoolean(txtCheck.Text);
        }
    }
}
