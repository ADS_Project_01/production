﻿using System;
using System.Data;
using System.Data.SqlClient;
using BCE.Data;

namespace Production.GeneralMaintenance.Overhead
{
    /// <summary>
    /// Summary description for Overhead
    /// </summary>
    public class Overhead
    {
        protected DBSetting myDBSetting;
        protected DataTable myBrowseTable;
        internal virtual DataRow LoadAccountData(string accNo)
        {
            return (DataRow)null;
        }
        internal Overhead()
        {
            myBrowseTable = new DataTable();
        }

        public DBSetting DBSetting
        {
            get { return myDBSetting; }
        }

        public static Overhead Create(DBSetting dbSetting)
        {
            Overhead aOverhead;
            if (dbSetting.ServerType == DBServerType.SQL2000)
                aOverhead = new OverheadSQL();
            else
                throw new ArgumentException("Server type: " + dbSetting.ServerType + " not supported.");

            aOverhead.myDBSetting = dbSetting;

            return aOverhead;
        }

        public DataTable BrowseTable
        {
            get { return myBrowseTable; }
        }

        public virtual DataTable LoadBrowseTable(string columnSQL)
        {
            return null;
        }

        public virtual DataTable LoadDealerListing(string fromOverheadCode, string toOverheadCode)
        {
            return null;
        }

        public OverheadEntity NewEntity()
        {
            DataSet ds = LoadData("");

            DataRow row = ds.Tables[0].NewRow();
            //object obj = myDBSetting.ExecuteScalar("select top 1 OverheadCode from Overhead order by OverheadCode Desc");
            //if (obj != null && obj != DBNull.Value)
            //{
            //    row["OverheadCode"] = (1 + BCE.Data.Convert.ToDecimal(obj)).ToString();
            //}
            //else
            //{
            //    row["OverheadCode"]= (1).ToString();
            //}
            row["OverheadCode"] = "";
            row["IsActive"] = BCE.Data.Convert.BooleanToText(true);
            ds.Tables[0].Rows.Add(row);
            try { ds.Tables[0].Constraints.Add("OverheadCode", myBrowseTable.Columns["OverheadCode"], true); } catch { }

            return new OverheadEntity(this, ds);
        }

        public OverheadEntity GetEntity(string OverheadCode)
        {
            DataSet ds = LoadData(OverheadCode);

            if (ds.Tables[0].Rows.Count == 0)
                return null;

            return new OverheadEntity(this, ds);
        }

        public void SaveEntity(OverheadEntity entity)
        {
            if (entity.OverheadCode.Length == 0)
                throw new EmptyOverheadCodeException();
            SaveData(entity.myDataSet);

            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(entity.OverheadCode);
            if (r == null)
            {
                r = myBrowseTable.NewRow();
                foreach (DataColumn col in entity.OverheadTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
                myBrowseTable.Rows.Add(r);
            }
            else
            {
                foreach (DataColumn col in entity.OverheadTable.Columns)
                {
                    if (myBrowseTable.Columns.Contains(col.ColumnName))
                        r[col.ColumnName] = entity.Row[col];
                }
            }
            myBrowseTable.AcceptChanges();
        }

        protected virtual DataSet LoadData(string OverheadCode)
        {
            return null;
        }

        public virtual void Delete(string OverheadCode)
        {
        }

        protected virtual void SaveData(DataSet ds)
        {
        }
    }

    public class OverheadSQL : Overhead
    {
        internal override DataRow LoadAccountData(string accNo)
        {
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = "SELECT Description, CurrencyCode, SpecialAccType FROM GLMast WHERE AccNo=?";
            object[] objArray = new object[1];
            int index = 0;
            string str = accNo;
            objArray[index] = (object)str;
            return dbSetting.GetFirstDataRow(cmdText, objArray);
        }
        public override DataTable LoadBrowseTable(string columnSQL)
        {
            myBrowseTable.Clear();
            myDBSetting.LoadDataTable(myBrowseTable, string.Format("SELECT {0} FROM RPA_Overhead", columnSQL), true);
            return myBrowseTable;
        }

        public override DataTable LoadDealerListing(string fromOverheadCode, string toOverheadCode)
        {
            return myDBSetting.GetDataTable("SELECT * FROM  RPA_Overhead WHERE OverheadCode>=? AND OverheadCode<=?", false, fromOverheadCode, toOverheadCode);
        }

        protected override DataSet LoadData(string OverheadCode)
        {
            SqlConnection conn = new SqlConnection(myDBSetting.ConnectionString);

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("SELECT * FROM RPA_Overhead WHERE OverheadCode=@OverheadCode", conn);
                cmd.Parameters.AddWithValue("@OverheadCode", OverheadCode);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                if (OverheadCode.Length == 0)
                    adapter.FillSchema(ds, SchemaType.Mapped, "RPA_Overhead");
                else
                    adapter.Fill(ds, "RPA_Overhead");
                return ds;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        public override void Delete(string OverheadCode)
        {
            myDBSetting.ExecuteNonQuery("DELETE FROM RPA_Overhead WHERE OverheadCode=?", OverheadCode);

            // Update BrowseTable
            DataRow r = myBrowseTable.Rows.Find(OverheadCode);
            if (r != null)
                r.Delete();
            myBrowseTable.AcceptChanges();
        }

        protected override void SaveData(DataSet ds)
        {
            myDBSetting.SimpleSaveDataSet(ds, "RPA_Overhead", "SELECT * FROM RPA_Overhead");
        }
    }

    public class OverheadEntity
    {
        private Overhead myOverhead;
        internal DataSet myDataSet;
        private DataRow myRow;

        public OverheadEntity(Overhead aOverhead, DataSet ds)
        {
            myOverhead = aOverhead;
            myDataSet = ds;
            myRow = myDataSet.Tables[0].Rows[0];
            this.OverheadTable.ColumnChanged += new DataColumnChangeEventHandler(this.myMasterTable_ColumnChanged);
        }
        private void myMasterTable_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            if (string.Compare(e.Column.ColumnName, "AccNo", true) == 0)
            {
                e.Row.BeginEdit();

                //e.Row["CurrencyRate"] = (object)1;
                //if (e.Row["AccDesc"] == DBNull.Value)
                //    e.Row["AccDesc"] = this.myRow["Description"];
                DataRow dataRow = this.myOverhead.LoadAccountData(e.Row[e.Column].ToString());
                if (dataRow != null)
                {
                    e.Row["AccDesc"] = dataRow["Description"];
                }
                e.Row.EndEdit();
            }
        }
        public Overhead Overhead
        {
            get { return myOverhead; }
        }

        internal DataRow Row
        {
            get { return myRow; }
        }

        public void Save()
        {
            myOverhead.SaveEntity(this);
        }

        public bool IsModified
        {
            get { return myDataSet.GetChanges() != null; }
        }

        public DataTable OverheadTable
        {
            get { return myDataSet.Tables[0]; }
        }

        public string OverheadCode
        {
            get { return myRow["OverheadCode"].ToString(); }
            set { myRow["OverheadCode"] = value; }
        }

        public object Description
        {
            get { return myRow["Description"]; }
            set { myRow["Description"] = value; }
        }
        public object AccNo
        {
            get { return myRow["AccNo"]; }
            set { myRow["AccNo"] = value; }
        }
        public object AccDesc
        {
            get { return myRow["AccDesc"]; }
            set { myRow["AccDesc"] = value; }
        }
       
        public bool IsActive
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myRow["IsActive"]);
            }
            set
            {
                this.myRow["IsActive"] = (object)BCE.Data.Convert.BooleanToText(value);
            }
        }
       
    }

    public class EmptyOverheadCodeException : BCE.Application.AppException
    {
        public EmptyOverheadCodeException()
            : base("Empty Overhead is not allowed.")
        {
        }
    }
}
