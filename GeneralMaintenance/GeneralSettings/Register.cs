﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BCE.Data;
using GLib;
namespace Production.GeneralMaintenance
{
    public partial class Register : Form
    {
        private DBSetting myDBSetting;
        public bool IsRegistered = false;
        GExecuteScalar es = new GExecuteScalar();

        public Register(DBSetting dbSetting)
        {
            myDBSetting = dbSetting;
            InitializeComponent();
            string smacAddr = "";
            string sCompanyName = "";
           // object obj = myDBSetting.ExecuteScalar("USE MASTER select loginame from sysprocesses where spid = @@SPID");
            object obj = myDBSetting.ExecuteScalar("USE MASTER select srvname from sys.sysservers");

            if (obj != null && obj != DBNull.Value)
            {
                smacAddr = obj.ToString();
            }
            obj = myDBSetting.ExecuteScalar("select CompanyName from Profile");
            if (obj != null && obj != DBNull.Value)
            {

                sCompanyName = obj.ToString();
            }

            tbProductID.Text =Production.GLib.G.GetProductID(smacAddr+sCompanyName + new AssemblyInfoEntity().Product, smacAddr) ;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            decimal dCode;
            dCode = GLib.G.Encrypt(tbProductID.Text.ToString());
            if (dCode == ntbProductKey.DecimalValue)
            {
                es.Query("UPDATE RPA_Settings SET rk=" + ntbProductKey.DecimalValue.ToString());
                GLib.G.MsgBox("SUCCESSFULLY REGISTERED!", "Registration");
                IsRegistered = true;
                Close();
            }
            else
            {
                GLib.G.MsgBox("Invalid Key!", "Registration");
            }
        }
    }
}
