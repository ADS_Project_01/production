﻿namespace Production.GeneralMaintenance
{
    partial class GeneralSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneralSettings));
            this.lblExpiry = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnActivate = new DevExpress.XtraEditors.SimpleButton();
            this.btnTry = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ceIsJournal = new DevExpress.XtraEditors.CheckEdit();
            this.ceMachine = new DevExpress.XtraEditors.CheckEdit();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.luAccRound = new DevExpress.XtraEditors.LookUpEdit();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.txtQtyDecimal = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.txtRateDecimal = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNextNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtEditDocNoFormat = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNextNoRM = new DevExpress.XtraEditors.TextEdit();
            this.txtDocNoFormatRM = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNextNoRV1 = new DevExpress.XtraEditors.TextEdit();
            this.txtDocNoFormatRV1 = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNextNoRR = new DevExpress.XtraEditors.TextEdit();
            this.txtDocNoFormatRR = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsJournal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceMachine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luAccRound.Properties)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQtyDecimal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRateDecimal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNextNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEditDocNoFormat.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNextNoRM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocNoFormatRM.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNextNoRV1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocNoFormatRV1.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNextNoRR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocNoFormatRR.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblExpiry
            // 
            this.lblExpiry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExpiry.AutoSize = true;
            this.lblExpiry.Font = new System.Drawing.Font("Arial Narrow", 8.25F);
            this.lblExpiry.ForeColor = System.Drawing.Color.Blue;
            this.lblExpiry.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblExpiry.Location = new System.Drawing.Point(57, 395);
            this.lblExpiry.Name = "lblExpiry";
            this.lblExpiry.Size = new System.Drawing.Size(140, 15);
            this.lblExpiry.TabIndex = 41;
            this.lblExpiry.Text = "This plug-ins will be expired on ";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(350, 395);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 43;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(430, 395);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 44;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnActivate
            // 
            this.btnActivate.Location = new System.Drawing.Point(373, 131);
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.Size = new System.Drawing.Size(98, 23);
            this.btnActivate.TabIndex = 45;
            this.btnActivate.Text = "&Activate Lisence";
            this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
            // 
            // btnTry
            // 
            this.btnTry.Location = new System.Drawing.Point(373, 160);
            this.btnTry.Name = "btnTry";
            this.btnTry.Size = new System.Drawing.Size(86, 23);
            this.btnTry.TabIndex = 46;
            this.btnTry.Text = "&Trial Lisence";
            this.btnTry.Click += new System.EventHandler(this.btnTry_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.ceIsJournal);
            this.panelControl1.Controls.Add(this.ceMachine);
            this.panelControl1.Controls.Add(this.xtraTabControl2);
            this.panelControl1.Controls.Add(this.groupControl3);
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Controls.Add(this.btnTry);
            this.panelControl1.Controls.Add(this.lblExpiry);
            this.panelControl1.Controls.Add(this.btnActivate);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(528, 430);
            this.panelControl1.TabIndex = 57;
            // 
            // ceIsJournal
            // 
            this.ceIsJournal.Location = new System.Drawing.Point(371, 214);
            this.ceIsJournal.Name = "ceIsJournal";
            this.ceIsJournal.Properties.Caption = "Is Journal";
            this.ceIsJournal.Properties.ValueChecked = "T";
            this.ceIsJournal.Properties.ValueUnchecked = "F";
            this.ceIsJournal.Size = new System.Drawing.Size(75, 19);
            this.ceIsJournal.TabIndex = 64;
            // 
            // ceMachine
            // 
            this.ceMachine.Location = new System.Drawing.Point(371, 189);
            this.ceMachine.Name = "ceMachine";
            this.ceMachine.Properties.Caption = "Is Machine";
            this.ceMachine.Properties.ValueChecked = "T";
            this.ceMachine.Properties.ValueUnchecked = "F";
            this.ceMachine.Size = new System.Drawing.Size(75, 19);
            this.ceMachine.TabIndex = 63;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Location = new System.Drawing.Point(4, 133);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage5;
            this.xtraTabControl2.Size = new System.Drawing.Size(300, 137);
            this.xtraTabControl2.TabIndex = 62;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage5,
            this.xtraTabPage6});
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.label11);
            this.xtraTabPage5.Controls.Add(this.luAccRound);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(294, 109);
            this.xtraTabPage5.Text = "Account Master";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(18, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 13);
            this.label11.TabIndex = 55;
            this.label11.Text = "Rounding Acc.";
            // 
            // luAccRound
            // 
            this.luAccRound.Location = new System.Drawing.Point(108, 20);
            this.luAccRound.Name = "luAccRound";
            this.luAccRound.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luAccRound.Size = new System.Drawing.Size(162, 20);
            this.luAccRound.TabIndex = 0;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.txtQtyDecimal);
            this.xtraTabPage6.Controls.Add(this.label10);
            this.xtraTabPage6.Controls.Add(this.txtRateDecimal);
            this.xtraTabPage6.Controls.Add(this.label9);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(294, 109);
            this.xtraTabPage6.Text = "Decimal Places";
            // 
            // txtQtyDecimal
            // 
            this.txtQtyDecimal.Location = new System.Drawing.Point(108, 42);
            this.txtQtyDecimal.Name = "txtQtyDecimal";
            this.txtQtyDecimal.Properties.Mask.EditMask = "d";
            this.txtQtyDecimal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtQtyDecimal.Properties.MaxLength = 30;
            this.txtQtyDecimal.Size = new System.Drawing.Size(64, 20);
            this.txtQtyDecimal.TabIndex = 58;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(18, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 57;
            this.label10.Text = "Qty";
            // 
            // txtRateDecimal
            // 
            this.txtRateDecimal.Location = new System.Drawing.Point(108, 16);
            this.txtRateDecimal.Name = "txtRateDecimal";
            this.txtRateDecimal.Properties.Mask.EditMask = "d";
            this.txtRateDecimal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtRateDecimal.Properties.MaxLength = 30;
            this.txtRateDecimal.Size = new System.Drawing.Size(64, 20);
            this.txtRateDecimal.TabIndex = 56;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(18, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 55;
            this.label9.Text = "Rate";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.xtraTabControl1);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(2, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(524, 123);
            this.groupControl3.TabIndex = 61;
            this.groupControl3.Text = "Document Numbering Format";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 21);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(520, 100);
            this.xtraTabControl1.TabIndex = 57;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            this.xtraTabControl1.Click += new System.EventHandler(this.xtraTabControl1_Click);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.txtNextNumber);
            this.xtraTabPage1.Controls.Add(this.txtEditDocNoFormat);
            this.xtraTabPage1.Controls.Add(this.label4);
            this.xtraTabPage1.Controls.Add(this.label1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(514, 72);
            this.xtraTabPage1.Text = "WO";
            // 
            // txtNextNumber
            // 
            this.txtNextNumber.Location = new System.Drawing.Point(108, 39);
            this.txtNextNumber.Name = "txtNextNumber";
            this.txtNextNumber.Properties.Mask.EditMask = "d";
            this.txtNextNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtNextNumber.Properties.MaxLength = 30;
            this.txtNextNumber.Size = new System.Drawing.Size(64, 20);
            this.txtNextNumber.TabIndex = 54;
            this.txtNextNumber.EditValueChanged += new System.EventHandler(this.textEdit1_EditValueChanged);
            // 
            // txtEditDocNoFormat
            // 
            this.txtEditDocNoFormat.Location = new System.Drawing.Point(108, 13);
            this.txtEditDocNoFormat.Name = "txtEditDocNoFormat";
            this.txtEditDocNoFormat.Properties.MaxLength = 30;
            this.txtEditDocNoFormat.Size = new System.Drawing.Size(187, 20);
            this.txtEditDocNoFormat.TabIndex = 52;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(18, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 51;
            this.label4.Text = "Doc No. Format";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(18, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "Next Number";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.txtNextNoRM);
            this.xtraTabPage2.Controls.Add(this.txtDocNoFormatRM);
            this.xtraTabPage2.Controls.Add(this.label2);
            this.xtraTabPage2.Controls.Add(this.label3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(514, 72);
            this.xtraTabPage2.Text = "RM";
            // 
            // txtNextNoRM
            // 
            this.txtNextNoRM.Location = new System.Drawing.Point(102, 38);
            this.txtNextNoRM.Name = "txtNextNoRM";
            this.txtNextNoRM.Properties.Mask.EditMask = "d";
            this.txtNextNoRM.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtNextNoRM.Properties.MaxLength = 30;
            this.txtNextNoRM.Size = new System.Drawing.Size(64, 20);
            this.txtNextNoRM.TabIndex = 58;
            // 
            // txtDocNoFormatRM
            // 
            this.txtDocNoFormatRM.Location = new System.Drawing.Point(102, 12);
            this.txtDocNoFormatRM.Name = "txtDocNoFormatRM";
            this.txtDocNoFormatRM.Properties.MaxLength = 30;
            this.txtDocNoFormatRM.Size = new System.Drawing.Size(187, 20);
            this.txtDocNoFormatRM.TabIndex = 56;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(12, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Doc No. Format";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(12, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 57;
            this.label3.Text = "Next Number";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.txtNextNoRV1);
            this.xtraTabPage3.Controls.Add(this.txtDocNoFormatRV1);
            this.xtraTabPage3.Controls.Add(this.label5);
            this.xtraTabPage3.Controls.Add(this.label6);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(514, 72);
            this.xtraTabPage3.Text = "Receive FG";
            // 
            // txtNextNoRV1
            // 
            this.txtNextNoRV1.Location = new System.Drawing.Point(109, 38);
            this.txtNextNoRV1.Name = "txtNextNoRV1";
            this.txtNextNoRV1.Properties.Mask.EditMask = "d";
            this.txtNextNoRV1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtNextNoRV1.Properties.MaxLength = 30;
            this.txtNextNoRV1.Size = new System.Drawing.Size(64, 20);
            this.txtNextNoRV1.TabIndex = 62;
            // 
            // txtDocNoFormatRV1
            // 
            this.txtDocNoFormatRV1.Location = new System.Drawing.Point(109, 12);
            this.txtDocNoFormatRV1.Name = "txtDocNoFormatRV1";
            this.txtDocNoFormatRV1.Properties.MaxLength = 30;
            this.txtDocNoFormatRV1.Size = new System.Drawing.Size(187, 20);
            this.txtDocNoFormatRV1.TabIndex = 60;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(19, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Doc No. Format";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(19, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 61;
            this.label6.Text = "Next Number";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.txtNextNoRR);
            this.xtraTabPage4.Controls.Add(this.txtDocNoFormatRR);
            this.xtraTabPage4.Controls.Add(this.label7);
            this.xtraTabPage4.Controls.Add(this.label8);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(514, 72);
            this.xtraTabPage4.Text = "Return Raw Material";
            // 
            // txtNextNoRR
            // 
            this.txtNextNoRR.Location = new System.Drawing.Point(105, 41);
            this.txtNextNoRR.Name = "txtNextNoRR";
            this.txtNextNoRR.Properties.Mask.EditMask = "d";
            this.txtNextNoRR.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtNextNoRR.Properties.MaxLength = 30;
            this.txtNextNoRR.Size = new System.Drawing.Size(64, 20);
            this.txtNextNoRR.TabIndex = 66;
            // 
            // txtDocNoFormatRR
            // 
            this.txtDocNoFormatRR.Location = new System.Drawing.Point(105, 15);
            this.txtDocNoFormatRR.Name = "txtDocNoFormatRR";
            this.txtDocNoFormatRR.Properties.MaxLength = 30;
            this.txtDocNoFormatRR.Size = new System.Drawing.Size(187, 20);
            this.txtDocNoFormatRR.TabIndex = 64;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(15, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 63;
            this.label7.Text = "Doc No. Format";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(15, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 65;
            this.label8.Text = "Next Number";
            // 
            // GeneralSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 430);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GeneralSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "General Setting";
            this.Activated += new System.EventHandler(this.GeneralSettings_Activated);
            this.Load += new System.EventHandler(this.GeneralSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsJournal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceMachine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage5.ResumeLayout(false);
            this.xtraTabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luAccRound.Properties)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            this.xtraTabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQtyDecimal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRateDecimal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNextNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEditDocNoFormat.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNextNoRM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocNoFormatRM.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNextNoRV1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocNoFormatRV1.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNextNoRR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocNoFormatRR.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblExpiry;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnActivate;
        private DevExpress.XtraEditors.SimpleButton btnTry;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.TextEdit txtNextNumber;
        private DevExpress.XtraEditors.TextEdit txtEditDocNoFormat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.TextEdit txtNextNoRM;
        private DevExpress.XtraEditors.TextEdit txtDocNoFormatRM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.TextEdit txtNextNoRV1;
        private DevExpress.XtraEditors.TextEdit txtDocNoFormatRV1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.TextEdit txtNextNoRR;
        private DevExpress.XtraEditors.TextEdit txtDocNoFormatRR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit luAccRound;
        private DevExpress.XtraEditors.CheckEdit ceMachine;
        private DevExpress.XtraEditors.CheckEdit ceIsJournal;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraEditors.TextEdit txtQtyDecimal;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit txtRateDecimal;
        private System.Windows.Forms.Label label9;
    }
}