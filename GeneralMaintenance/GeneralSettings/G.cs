﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Globalization;
using System.Runtime.InteropServices;
using System.IO;
using BCE.Data;
using System.Security;
using System.Security.Cryptography;
using GLib;
namespace Production.GLib
{
    public static class G
    {
        [DllImport("kernel32.dll")]
        public static extern long GetVolumeInformation(string PathName, StringBuilder VolumeNameBuffer, UInt32 VolumeNameSize, ref UInt32 VolumeSerialNumber, ref UInt32 MaximumComponentLength, ref UInt32 FileSystemFlags, StringBuilder FileSystemNameBuffer, UInt32 FileSystemNameSize);
        public static int monthDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = (12 * (startDate.Year - endDate.Year)) + ((startDate.Month - 1) - endDate.Month);
            return Math.Abs(monthsApart);
        }

        public static Guid Globalkey = new Guid("97815687-C48A-4E8E-8549-784850166A09");

        public static string MyKey= "b17ca5893a4e4133bbce2ea1415a1916";

        public static string RichTextBoxConvertToText(string rtf)
        {
            RichTextBox rtb = new RichTextBox();
            rtb.Rtf = rtf;
            return rtb.Text;
        }
        public static decimal CalcSumInsurance(string sRate, decimal dPrice)
        {
            decimal dDis1 = 0, dPrice1 = 0, dTotal1 = 0;
            string sdisc1 = sRate;
            decimal nol = 0;
            string snol;
            string sDiscVal1 = sdisc1;



            //*--------sDiscX1----------------
            string sDiscX1 = "";
            sDiscX1 = sRate;

            dPrice1 = dPrice;

            snol = nol.ToString();
            // string sDiscX1 = "";
            if (sDiscX1 != "")
            {
                if (sDiscX1.IndexOf('%') > 0)
                {
                    sDiscX1 = CleanString(sDiscX1);//Replace(sDiscX1, "%", "");
                    dDis1 = System.Convert.ToDecimal(sDiscX1);
                    dTotal1 = dPrice1 * (dDis1 / 100);
                }
                else
                {
                    CleanString(sDiscX1);
                    dDis1 = System.Convert.ToDecimal(sDiscX1);
                    dTotal1 = dDis1;
                }


            }
            else
            {
                dTotal1 = 0;
                //  e.MasterRecord.Footer1Amt = dTotal1;
            }





            return dTotal1;
        }
        public static string RichTextBoxConvertToText2(string rtf)
        {
            RichTextBox rtb = new RichTextBox();
            rtb.Rtf = rtf;
            string sText = "";
            string[] A = rtb.Text.Split(new char[] { '\n' });
            //sdesc2 = (A[1]).ToString().Trim();
            for (int i = 0; i < A.Length; i++)
            {
                sText += (A[i]).ToString().Trim() + " ";
            }
            return sText;
        }
        public static string MemoEditConvertToText(string rtf)
        {
            DevExpress.XtraEditors.MemoEdit rtb = new DevExpress.XtraEditors.MemoEdit();
            rtb.Text = rtf;
            return rtb.Text;

        }
        public static void DisplayPercentage(double ratio)
        {
            string percentage = string.Format("Percentage is {0:0.0%}", ratio);
            //return percentage;
        }
        /// <summary>
        /// This method writes the percentage of the top number to the bottom number.
        /// </summary>
        public static void DisplayPercentage(int top, int bottom)
        {
            DisplayPercentage((double)top / bottom);
        }
        public static string GetPercentageString(double ratio)
        {
            return ratio.ToString("0.0%");
        }

        public static DateTime RoundUp(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }

        public static DateTime RoundDown(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
        }

        public static DateTime EmptyDateTime()
        {
            return new DateTime(1800, 1, 1, 0, 0, 0); ;
        }

        public static bool IsEmptyDateTime(DateTime d)
        {
            DateTime dtNull = new DateTime(1800, 1, 1, 0, 0, 0);
            return (d == dtNull);
        }

        public static DateTime EmptySmallDateTime()
        {
            return new DateTime(1900, 1, 1, 0, 0, 0);
        }

        public static bool IsEmptySmallDateTime(DateTime d)
        {
            DateTime dtNull = new DateTime(1900, 1, 1, 0, 0, 0);
            return (d == dtNull);
        }

        public static string GSQLSmallDateTime(object dtS)
        {
            string sRetVal = "";
            if (dtS == System.DBNull.Value)
                sRetVal = "CAST('1900-01-01 00:00:00.000' AS smalldatetime)";
            else
            {
                DateTime dt = (DateTime)dtS;
                DateTime dtMinDate = new DateTime(1900, 1, 1, 0, 0, 0);
                if (dt < dtMinDate)
                    dt = dtMinDate;
                sRetVal = "CAST('" + String.Format("{0:D4}", dt.Year) + "-" +
                    String.Format("{0:D2}", dt.Month) + "-" + String.Format("{0:D2}", dt.Day) + " " +
                    String.Format("{0:D2}", dt.Hour) + ":" + String.Format("{0:D2}", dt.Minute) + ":" +
                    String.Format("{0:D2}", dt.Second) + ".000' AS smalldatetime)";
            }
            return sRetVal;
        }

        // cRound='U' -> Up. Time will be set to 59:59:59.000
        // cRound='D' -> Down. Time will be set to 00:00:00.000
        public static string GSQLSmallDateTime(object dtS, char cRound)
        {
            string sRetVal = "";
            if (dtS == System.DBNull.Value)
                sRetVal = "CAST('1900-01-01 00:00:00.000' AS smalldatetime)";
            else
            {
                DateTime dt = (DateTime)dtS;
                DateTime dtMinDate = new DateTime(1900, 1, 1, 0, 0, 0);
                if (dt < dtMinDate)
                    dt = dtMinDate;
                switch (cRound)
                {
                    case 'U':
                        sRetVal = "CAST('" + String.Format("{0:D4}", dt.Year) + "-" +
                            String.Format("{0:D2}", dt.Month) + "-" + String.Format("{0:D2}", dt.Day) + " " +
                            "23:59:59.999' AS smalldatetime)";
                        break;
                    case 'D':
                        sRetVal = "CAST('" + String.Format("{0:D4}", dt.Year) + "-" +
                            String.Format("{0:D2}", dt.Month) + "-" + String.Format("{0:D2}", dt.Day) + " " +
                            "00:00:00.000' AS smalldatetime)";
                        break;
                    default:
                        sRetVal = "CAST('" + String.Format("{0:D4}", dt.Year) + "-" +
                            String.Format("{0:D2}", dt.Month) + "-" + String.Format("{0:D2}", dt.Day) + " " +
                            String.Format("{0:D2}", dt.Hour) + ":" + String.Format("{0:D2}", dt.Minute) + ":" +
                            String.Format("{0:D2}", dt.Second) + ".000' AS smalldatetime)";
                        break;
                }
            }
            return sRetVal;
        }


        public static string GSQLDateTime(object dtS)
        {
            string sRetVal = "";
            if (dtS == System.DBNull.Value)
                sRetVal = "CAST('1800-01-01 00:00:00.000' AS datetime)";
            else
            {
                DateTime dt = (DateTime)dtS;
                DateTime dtMinDate = new DateTime(1800, 1, 1, 0, 0, 0);
                if (dt < dtMinDate)
                    dt = dtMinDate;
                sRetVal = "CAST('" + String.Format("{0:D4}", dt.Year) + "-" +
                    String.Format("{0:D2}", dt.Month) + "-" + String.Format("{0:D2}", dt.Day) + " " +
                    String.Format("{0:D2}", dt.Hour) + ":" + String.Format("{0:D2}", dt.Minute) + ":" +
                    String.Format("{0:D2}", dt.Second) + ".000' AS datetime)";
            }
            return sRetVal;
        }

        // cRound='U' -> Up. Time will be set to 59:59:59.999
        // cRound='D' -> Down. Time will be set to 00:00:00.000
        public static string GSQLDateTime(object dtS, char cRound)
        {
            string sRetVal = "";
            if (dtS == System.DBNull.Value)
                sRetVal = "CAST('1800-01-01 00:00:00.000' AS datetime)";
            else
            {
                DateTime dt = (DateTime)dtS;
                DateTime dtMinDate = new DateTime(1800, 1, 1, 0, 0, 0);
                if (dt < dtMinDate)
                    dt = dtMinDate;
                switch (cRound)
                {
                    case 'U':
                        sRetVal = "CAST('" + String.Format("{0:D4}", dt.Year) + "-" +
                            String.Format("{0:D2}", dt.Month) + "-" + String.Format("{0:D2}", dt.Day) + " " +
                            "23:59:59.000' AS datetime)";
                        break;
                    case 'D':
                        sRetVal = "CAST('" + String.Format("{0:D4}", dt.Year) + "-" +
                            String.Format("{0:D2}", dt.Month) + "-" + String.Format("{0:D2}", dt.Day) + " " +
                            "00:00:00.000' AS datetime)";
                        break;
                    default:
                        sRetVal = "CAST('" + String.Format("{0:D4}", dt.Year) + "-" +
                            String.Format("{0:D2}", dt.Month) + "-" + String.Format("{0:D2}", dt.Day) + " " +
                            String.Format("{0:D2}", dt.Hour) + ":" + String.Format("{0:D2}", dt.Minute) + ":" +
                            String.Format("{0:D2}", dt.Second) + ".000' AS datetime)";
                        break;
                }
            }
            return sRetVal;
        }

        public static DataGridViewRow CloneWithValues(DataGridViewRow row)
        {
            DataGridViewRow clonedRow = (DataGridViewRow)row.Clone();
            for (Int32 index = 0; index < row.Cells.Count; index++)
            {
                clonedRow.Cells[index].Value = row.Cells[index].Value;
            }
            return clonedRow;

        }

        public static string CleanString(string InputString)
        {
            string ResultString, NegSign = "-";
            CultureInfo ci = new CultureInfo(CultureInfo.CurrentCulture.ToString());
            if (ci.NumberFormat.NumberDecimalSeparator.ToString() == ".")
                ResultString = Regex.Replace(InputString, @"[^\d.]", "");
            else
                ResultString = Regex.Replace(InputString, @"[^\d,]", "");
            if (InputString.IndexOf('-') >= 0)
                ResultString = NegSign + ResultString;
            return ResultString;
        }
        public static string CleanInt(string InputInt)
        {
            string ResultInt = Regex.Replace(InputInt, @"[^\D.]", "");
            return ResultInt;
        }
        public static string CleanSeparator(string InputString)
        {
            string ResultString = Regex.Replace(InputString, @"[^\w.]", "");
            return ResultString;
        }
        public static string CleannonSeparator(string InputString)
        {
            string ResultString = Regex.Replace(InputString, @"[^\W.]", "");
            return ResultString;

        }
        public static string GQuoteString(string s)
        {
            int n, n1;
            n = 0;
            while (true)
            {
                n1 = s.Substring(n).IndexOf("'");
                if (n1 == -1)
                    break;
                n = n + n1;
                s = s.Substring(0, n) + "'" + s.Substring(n);
                n = n + 2;
            }
            return s;
        }
        public static void DataGridViewSort(DataGridView dgv, bool bAllowSort)
        {
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                if (bAllowSort)
                    dgv.Columns[i].SortMode = DataGridViewColumnSortMode.Automatic;
                else
                    dgv.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        public static void DataSetRemoveAllTables(DataSet ds)
        {
            int iTotalTable = ds.Tables.Count;
            for (int i = 0; i < iTotalTable; i++)
            {
                ds.Tables.Remove(ds.Tables[0].TableName);
            }
        }

        public static string GetHomeCurrency()
        {
            string s = "";
            GDataTable gdt = new GDataTable();
            DataTable dt = new DataTable();
            gdt.LoadDataTable(dt, "REGISTRY", "SELECT * FROM REGISTRY WHERE RegID=31");
            if (dt.Rows.Count > 0)
                s = dt.Rows[0]["RegValue"].ToString();
            return s;
        }

        public static decimal GetCurrencyRate(string sCurrencyCode, DateTime d)
        {

            GDataTable gdt = new GDataTable();
            DataTable dt = new DataTable();
            string s;
            s = "SELECT * FROM CURRRATE WHERE CurrencyCode=" +
                GLib.G.GSaveStrColl(sCurrencyCode) + " AND FromDate<=" + GSQLDateTime(d) + " AND ToDate>=" + GSQLDateTime(d);
            gdt.LoadDataTable(dt, "CURRRATE", s);
            if (dt.Rows.Count > 0)
                return (decimal)dt.Rows[0]["BankBuyRate"];
            gdt.LoadDataTable(dt, "CURRENCY", "SELECT * FROM CURRENCY WHERE CurrencyCode=" +
                GLib.G.GSaveStrColl(sCurrencyCode));
            if (dt.Rows.Count > 0)
                return (decimal)dt.Rows[0]["BankBuyRate"];
            System.Windows.Forms.MessageBox.Show(
                "Currency code " + sCurrencyCode + " is missing, returning 1 as the exchange rate.", "Data Integrity Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop, System.Windows.Forms.MessageBoxDefaultButton.Button1);
            return (decimal)1;
        }

        public static string NumberToString(object oNumber)
        {
            string sNumber = "";
            CultureInfo ci = new CultureInfo("en-US");
            sNumber = System.Convert.ToString(oNumber, ci).Trim();
            return sNumber;
        }

        // Convert datarow into SQL INSERT command
        // Does not work with sql_variant and timestamp datatype.
        // E.g:
        // string sUpdate = SQLINSERTCommand(dr,true);
        public static string SQLINSERTCommand(DataRow dr, bool bFirstFieldIdentity)
        {
            CultureInfo ci = new CultureInfo("en-US");
            string s = "INSERT INTO " + dr.Table.TableName + " (";
            string sColumnName, sDataType;
            bool bFirst = true;
            int iIdentity;
            if (bFirstFieldIdentity)
                iIdentity = 1;
            else
                iIdentity = 0;
            for (int i = 0 + iIdentity; i < dr.Table.Columns.Count; i++)
            {
                if (!bFirst)
                {
                    s = s + ",";
                }
                bFirst = false;
                sColumnName = dr.Table.Columns[i].ColumnName.ToString();
                s = s + sColumnName;
            }
            s = s + ") VALUES (";
            bFirst = true;
            for (int i = 0 + iIdentity; i < dr.Table.Columns.Count; i++)
            {
                if (!bFirst)
                {
                    s = s + ",";
                }
                bFirst = false;
                if (dr[i] == System.DBNull.Value)
                    sColumnName = "NULL";
                else
                {
                    sColumnName = dr.Table.Columns[i].ColumnName.ToString();
                    sDataType = dr.Table.Columns[i].DataType.ToString();
                    switch (sDataType)
                    {
                        case "System.String":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.DateTime":
                            sColumnName = G.GSQLDateTime(dr[i]);
                            break;
                        case "System.SmallDateTime":
                            sColumnName = G.GSQLSmallDateTime(dr[i]);
                            break;
                        case "System.Char":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.NChar":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.NText":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.NVarChar":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.Text":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.VarChar":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.Drawing.Bitmap":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.Drawing.Image":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.Boolean":
                            sColumnName = System.Convert.ToInt16(dr[i]).ToString();
                            break;
                        default:
                            sColumnName = System.Convert.ToString(dr[i], ci).Trim();
                            break;
                    }

                }
                s = s + sColumnName;
            }
            s = s + ")";
            return s;
        }
        public static string SQLINSERTCommand(DataRow dr, bool bFirstFieldIdentity, string TableName)
        {
            CultureInfo ci = new CultureInfo("en-US");
            string s = "INSERT INTO " + TableName + " (";
            string sColumnName, sDataType;
            bool bFirst = true;
            int iIdentity;
            if (bFirstFieldIdentity)
                iIdentity = 1;
            else
                iIdentity = 0;
            for (int i = 0 + iIdentity; i < dr.Table.Columns.Count; i++)
            {
                if (!bFirst)
                {
                    s = s + ",";
                }
                bFirst = false;
                sColumnName = dr.Table.Columns[i].ColumnName.ToString();
                s = s + sColumnName;
            }
            s = s + ") VALUES (";
            bFirst = true;
            for (int i = 0 + iIdentity; i < dr.Table.Columns.Count; i++)
            {
                if (!bFirst)
                {
                    s = s + ",";
                }
                bFirst = false;
                if (dr[i] == System.DBNull.Value)
                    sColumnName = "NULL";
                else
                {
                    sColumnName = dr.Table.Columns[i].ColumnName.ToString();
                    sDataType = dr.Table.Columns[i].DataType.ToString();
                    switch (sDataType)
                    {
                        case "System.String":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.DateTime":
                            sColumnName = G.GSQLDateTime(dr[i]);
                            break;
                        case "System.SmallDateTime":
                            sColumnName = G.GSQLSmallDateTime(dr[i]);
                            break;
                        case "System.Char":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.NChar":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.NText":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.NVarChar":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.Text":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.VarChar":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.Boolean":
                            sColumnName = System.Convert.ToInt16(dr[i]).ToString();
                            break;
                        default:
                            sColumnName = System.Convert.ToString(dr[i], ci).Trim();
                            break;
                    }
                }
                s = s + sColumnName;
            }
            s = s + ")";
            return s;
        }
        // Convert datarow into SQL UPDATE command
        // Does not work with sql_variant and timestamp datatype.
        // E.g:
        // string sUpdate = SQLUPDATECommand(dr,"DocNo='IV-0001'");
        public static string SQLUPDATECommand(DataRow dr, bool bFirstIdentityField, string sWhere)
        {
            CultureInfo ci = new CultureInfo("en-US");
            string s = "UPDATE " + dr.Table.TableName + " SET ";
            string sColumnName, sDataType;
            bool bFirst = true;
            int iIdentity;
            if (bFirstIdentityField)
                iIdentity = 1;
            else
                iIdentity = 0;
            try { dr.Table.Columns.Remove("Image1"); }
            catch { }
            try { dr.Table.Columns.Remove("Image2"); }
            catch { }
            try { dr.Table.Columns.Remove("Image3"); }
            catch { }
            try { dr.Table.Columns.Remove("Image4"); }
            catch { }
            try { dr.Table.Columns.Remove("Image5"); }
            catch { }
            try { dr.Table.Columns.Remove("ImageName1"); }
            catch { }
            try { dr.Table.Columns.Remove("ImageName2"); }
            catch { }
            try { dr.Table.Columns.Remove("ImageName3"); }
            catch { }
            try { dr.Table.Columns.Remove("ImageName4"); }
            catch { }
            try { dr.Table.Columns.Remove("ImageName5"); }
            catch { }
            try { dr.Table.Columns.Remove("Report"); }
            catch { }
            try { dr.Table.Columns.Remove("ImageReport"); }
            catch { }
            dr.Table.AcceptChanges();

            for (int i = 0 + iIdentity; i < dr.Table.Columns.Count; i++)
            {
                //if (dr.Table.Columns[i].ColumnName.ToString() == "Image1" || dr.Table.Columns[i].ColumnName.ToString() == "Image2" || dr.Table.Columns[i].ColumnName.ToString() == "Image3")
                //    break;

                if (!bFirst)
                {
                    s = s + ",";
                }
                bFirst = false;
                sColumnName = dr.Table.Columns[i].ColumnName.ToString();
                s = s + sColumnName + "=";
                if (dr[i] == System.DBNull.Value)
                    sColumnName = "NULL";
                else
                {
                    sColumnName = dr.Table.Columns[i].ColumnName.ToString();
                    sDataType = dr.Table.Columns[i].DataType.ToString();
                    switch (sDataType)
                    {
                        case "System.String":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.DateTime":
                            sColumnName = G.GSQLDateTime(dr[i]);
                            break;
                        case "System.SmallDateTime":
                            sColumnName = G.GSQLSmallDateTime(dr[i]);
                            break;
                        case "System.Char":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.NChar":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.NText":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.NVarChar":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.Text":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.VarChar":
                            sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
                            break;
                        case "System.Boolean":
                            sColumnName = System.Convert.ToInt16(dr[i]).ToString();
                            break;
                        default:
                            sColumnName = System.Convert.ToString(dr[i], ci).Trim();
                            break;
                    }
                }
                s = s + sColumnName;
            }
            if (sWhere.Length > 0)
                s = s + " WHERE " + sWhere;
            return s;
        }
        //public static string SQLUPDATECommand(DataRow dr,object obj, bool bFirstIdentityField, string sWhere)
        //{
        //    CultureInfo ci = new CultureInfo("en-US");
        //    string s = "UPDATE " + dr.Table.TableName + " SET ";
        //    string sColumnName, sDataType;
        //    bool bFirst = true;
        //    int iIdentity;
        //    if (bFirstIdentityField)
        //        iIdentity = 1;
        //    else
        //        iIdentity = 0;
        //    for (int i = 0 + iIdentity; i <dr.Table.Columns.Count; i++)
        //    {
        //        if (!bFirst)
        //        {
        //            s = s + ",";
        //        }
        //        bFirst = false;
        //        sColumnName = dr.Table.Columns[i].ColumnName.ToString();
        //        s = s + sColumnName + "=";
        //        if (dr[i] == System.DBNull.Value)
        //            sColumnName = "NULL";
        //        else
        //        {
        //            sColumnName = dr.Table.Columns[i].ColumnName.ToString();
        //            sDataType = dr.Table.Columns[i].DataType.ToString();
        //            switch (sDataType)
        //            {
        //                case "System.String":
        //                    sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
        //                    break;
        //                case "System.DateTime":
        //                    sColumnName = G.GSQLDateTime(dr[i]);
        //                    break;
        //                case "System.SmallDateTime":
        //                    sColumnName = G.GSQLSmallDateTime(dr[i]);
        //                    break;
        //                case "System.Char":
        //                    sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
        //                    break;
        //                case "System.NChar":
        //                    sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
        //                    break;
        //                case "System.NText":
        //                    sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
        //                    break;
        //                case "System.NVarChar":
        //                    sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
        //                    break;
        //                case "System.Text":
        //                    sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
        //                    break;
        //                case "System.VarChar":
        //                    sColumnName = "'" + GQuoteString(dr[i].ToString().Trim()) + "'";
        //                    break;
        //                case "System.Boolean":
        //                    sColumnName = System.Convert.ToInt16(dr[i]).ToString();
        //                    break;
        //                default:
        //                    sColumnName = System.Convert.ToString(dr[i], ci).Trim();
        //                    break;
        //            }
        //        }
        //        s = s + sColumnName;
        //    }
        //    if (sWhere.Length > 0)
        //        s = s + " WHERE " + sWhere;
        //    return s;
        //}

        public static void MsgBox(string sMessage, string sTitle)
        {
            System.Windows.Forms.MessageBox.Show(sMessage, sTitle, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop, System.Windows.Forms.MessageBoxDefaultButton.Button1);
        }

        // Obsolete, for backward compatibility only, use GetNextDocNoX()
        public static string GetNextDocNo(string sDocNo, int iFormatID, string sDocType, SqlConnection conn)
        {
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            // Update next document number
            if (iFormatID == 0 && sDocNo == "<<New>>")
            {
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND isdefault=1", conn);
                sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sDocNo; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNo"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 > 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sDocNo;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample;
                    }
                }
                else
                    sNextDocNo = sDocNo;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE PR_DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' AND isdefault=1";
                SqlCommand cmd = new SqlCommand(sCmdUpdate, conn);
                cmd.ExecuteNonQuery();
                return sNextDocNo;
            }
            if (iFormatID > 0)
            {
                // non-default numbering
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND formatid=" + iFormatID.ToString(), conn);
                sa.Fill(dt);
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNo"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 > 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sDocNo;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample;
                    }
                }
                else
                    sNextDocNo = sDocNo;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE PR_DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' AND formatid=" + iFormatID.ToString();
                SqlCommand cmd = new SqlCommand(sCmdUpdate, conn);
                cmd.ExecuteNonQuery();
                return sNextDocNo;
            }
            return sDocNo; // Manual No.
        }
        public static string GetNextDocNo(string sDocNo, int iFormatID, string sDocType, DBSetting transDBSetting)
        {
            object o;
            string sNextDocNo = "", sSample = "", sFormat, ss, s;
            int iNextNo, n1, n2;
            // Update next document number
            if (iFormatID == 0 && sDocNo == "<<New>>")
            {
                DataTable dt = new DataTable();
                o = transDBSetting.ExecuteScalar("SELECT NextNo FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND isdefault=1");
                if (o == null)
                    return sDocNo; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(o);
                // sSample = "";
                o = transDBSetting.ExecuteScalar("SELECT Format FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND isdefault=1");
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                /*
                if (n1 < n2 && n1 > 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sDocNo;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample;
                    }
                }
                else
                    sNextDocNo = sDocNo;
                 */
                //int iNextNo = ntbNextNo.IntValue;
                char cDelimiter = ' ';
                DateTime dtime = DateTime.Now;
                //  char cDelimeter = ' ';
                string sNext, sSample2 = "", sSample3 = "", sDateTime = "", sSimbol = "", sCoba = "", sDelimeter = "";
                // string sDocNo = tbFormat.Text.ToString();
                //string sFormat = tbFormat.Text.ToString();
                int n3, n4, n5, n6, n7;
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');

                sDelimeter = CleannonSeparator(sFormat);
                if (n1 < n2 && n1 > -1)
                {

                    sSample = sFormat.Remove(n1, 1).ToString();
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);

                    n6 = sSample.IndexOf('>');
                    if (n2 > -1)
                    {
                        try
                        {
                            sSample2 = sFormat.Substring(n2 + 1).ToString();
                        }
                        catch { };
                        try
                        {
                            sSample = sSample.Remove(n6, sSample.Length - n6).ToString();
                        }
                        catch { };

                    }

                    sNext = sSample;
                    if (sNext.Length <= iNextNo.ToString().Length)
                    {
                        sSample = sNext.Remove(0, sNext.Length).ToString();
                    }
                    else
                    {
                        sSample = sNext.Remove(0, iNextNo.ToString().Length).ToString();
                    }
                    n3 = sSample2.IndexOf('@');
                    n4 = sSample2.IndexOf('{');

                    if (n3 != -1)
                    {
                        sSample3 = sSample2.Remove(0, n3 + 1).ToString();
                        sSample2 = sSample2.Remove(n4, sSample2.Length - n4).ToString();

                        n7 = sSample3.IndexOf('}');
                        if (n7 > -1)
                        {
                            sSample3 = sSample3.Remove(n7, 1).ToString();

                        }

                        for (int i = 0; i < 1; i++)
                        {
                            cDelimiter = System.Convert.ToChar(sDelimeter.Substring(i, i + 1));
                        }
                        n6 = sDelimeter.IndexOf(cDelimiter);
                        // sCoba = ;
                        // sSample = sSample + dtime.ToString(sDateTime) + sSimbol + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sSample = String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);

                    }
                    if (n3 != -1)
                    {
                        sSample3 = CleanInt(sSample3);
                        //CleanString(sSample3);

                        try { sNextDocNo = CleanString(sSample) + sSample2 + dtime.ToString(sSample3); }
                        catch { };

                    }
                    else
                    {
                        sNextDocNo = sSample2 + CleanString(sSample);
                    }

                }
                else
                {
                    sNextDocNo = sDocNo;
                }
                // return sNextDocNo;
                // return sNextDocNo;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE PR_DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' AND isdefault=1";
                transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return sNextDocNo;
            }
            if (iFormatID > 0)
            {
                // non-default numbering
                DataTable dt = new DataTable();
                o = transDBSetting.ExecuteScalar("SELECT NextNo FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND FormatID=" + iFormatID + "");
                iNextNo = System.Convert.ToInt32(o);
                sSample = "";
                o = transDBSetting.ExecuteScalar("SELECT Format FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND FormatID=" + iFormatID + "");
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                char cDelimiter = ' ';
                DateTime dtime = DateTime.Now;
                //  char cDelimeter = ' ';
                string sNext, sSample2 = "", sSample3 = "", sDateTime = "", sSimbol = "", sCoba = "", sDelimeter = "";
                // string sDocNo = tbFormat.Text.ToString();
                //string sFormat = tbFormat.Text.ToString();
                int n3, n4, n5, n6, n7;
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');

                sDelimeter = CleannonSeparator(sFormat);
                if (n1 < n2 && n1 > -1)
                {

                    sSample = sFormat.Remove(n1, 1).ToString();
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);

                    n6 = sSample.IndexOf('>');
                    if (n2 > -1)
                    {
                        try
                        {
                            sSample2 = sFormat.Substring(n2 + 1).ToString();
                        }
                        catch { };
                        try
                        {
                            sSample = sSample.Remove(n6, sSample.Length - n6).ToString();
                        }
                        catch { };

                    }

                    sNext = sSample;
                    if (sNext.Length <= iNextNo.ToString().Length)
                    {
                        sSample = sNext.Remove(0, sNext.Length).ToString();
                    }
                    else
                    {
                        sSample = sNext.Remove(0, iNextNo.ToString().Length).ToString();
                    }
                    n3 = sSample2.IndexOf('@');
                    n4 = sSample2.IndexOf('{');

                    if (n3 != -1)
                    {
                        sSample3 = sSample2.Remove(0, n3 + 1).ToString();
                        sSample2 = sSample2.Remove(n4, sSample2.Length - n4).ToString();

                        n7 = sSample3.IndexOf('}');
                        if (n7 > -1)
                        {
                            sSample3 = sSample3.Remove(n7, 1).ToString();

                        }

                        for (int i = 0; i < 1; i++)
                        {
                            cDelimiter = System.Convert.ToChar(sDelimeter.Substring(i, i + 1));
                        }
                        n6 = sDelimeter.IndexOf(cDelimiter);
                        // sCoba = ;
                        // sSample = sSample + dtime.ToString(sDateTime) + sSimbol + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sSample = String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);

                    }
                    if (n3 != -1)
                    {
                        sSample3 = CleanInt(sSample3);
                        //CleanString(sSample3);

                        try { sNextDocNo = CleanString(sSample) + sSample2 + dtime.ToString(sSample3); }
                        catch { };

                    }
                    else
                    {
                        sNextDocNo = sSample2 + CleanString(sSample);
                    }



                    // return sNextDocNo;
                    // return sNextDocNo;

                }
                else
                    sNextDocNo = sDocNo;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE PR_DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' AND formatid=" + iFormatID.ToString();
                transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return sNextDocNo;
            }
            return sDocNo; // Manual No.
        }

        public static string GetNextDocNoCopy(string sDocNo, int iFormatID, string sDocType, DBSetting transDBSetting, string Division)
        {
            object o;
            string sNextDocNo = "", sSample = "", sFormat, ss, s;
            int iNextNo, n1, n2;
            // Update next document number
            if (iFormatID == 0 && sDocNo == "<<New>>")
            {
                DataTable dt = new DataTable();
                o = transDBSetting.ExecuteScalar("SELECT NextNo FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND Format Like '%" + Division + "%'");
                if (o == null)
                    return sDocNo; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(o);
                // sSample = "";
                o = transDBSetting.ExecuteScalar("SELECT Format FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND Format Like '%" + Division + "%'");
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                /*
                if (n1 < n2 && n1 > 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sDocNo;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample;
                    }
                }
                else
                    sNextDocNo = sDocNo;
                 */
                //int iNextNo = ntbNextNo.IntValue;
                char cDelimiter = ' ';
                DateTime dtime = DateTime.Now;
                //  char cDelimeter = ' ';
                string sNext, sSample2 = "", sSample3 = "", sDateTime = "", sSimbol = "", sCoba = "", sDelimeter = "";
                // string sDocNo = tbFormat.Text.ToString();
                //string sFormat = tbFormat.Text.ToString();
                int n3, n4, n5, n6, n7;
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');

                sDelimeter = CleannonSeparator(sFormat);
                if (n1 < n2 && n1 > -1)
                {

                    sSample = sFormat.Remove(n1, 1).ToString();
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);

                    n6 = sSample.IndexOf('>');
                    if (n2 > -1)
                    {
                        try
                        {
                            sSample2 = sFormat.Substring(n2 + 1).ToString();
                        }
                        catch { };
                        try
                        {
                            sSample = sSample.Remove(n6, sSample.Length - n6).ToString();
                        }
                        catch { };

                    }

                    sNext = sSample;
                    if (sNext.Length <= iNextNo.ToString().Length)
                    {
                        sSample = sNext.Remove(0, sNext.Length).ToString();
                    }
                    else
                    {
                        sSample = sNext.Remove(0, iNextNo.ToString().Length).ToString();
                    }
                    n3 = sSample2.IndexOf('@');
                    n4 = sSample2.IndexOf('{');

                    if (n3 != -1)
                    {
                        sSample3 = sSample2.Remove(0, n3 + 1).ToString();
                        sSample2 = sSample2.Remove(n4, sSample2.Length - n4).ToString();

                        n7 = sSample3.IndexOf('}');
                        if (n7 > -1)
                        {
                            sSample3 = sSample3.Remove(n7, 1).ToString();

                        }

                        for (int i = 0; i < 1; i++)
                        {
                            cDelimiter = System.Convert.ToChar(sDelimeter.Substring(i, i + 1));
                        }
                        n6 = sDelimeter.IndexOf(cDelimiter);
                        // sCoba = ;
                        // sSample = sSample + dtime.ToString(sDateTime) + sSimbol + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sSample = String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);

                    }
                    if (n3 != -1)
                    {
                        sSample3 = CleanInt(sSample3);
                        //CleanString(sSample3);

                        try { sNextDocNo = CleanString(sSample) + sSample2 + dtime.ToString(sSample3); }
                        catch { };

                    }
                    else
                    {
                        sNextDocNo = sSample2 + CleanString(sSample);
                    }

                }
                else
                {
                    sNextDocNo = sDocNo;
                }
                // return sNextDocNo;
                // return sNextDocNo;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE PR_DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' AND Format Like '%" + Division + "%'";
                transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return sNextDocNo;
            }
            if (iFormatID > 0)
            {
                // non-default numbering
                DataTable dt = new DataTable();
                o = transDBSetting.ExecuteScalar("SELECT NextNo FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND FormatID=" + iFormatID + "");
                iNextNo = System.Convert.ToInt32(o);
                sSample = "";
                o = transDBSetting.ExecuteScalar("SELECT Format FROM PR_DocNoFormat WHERE doctype='" + sDocType + "' AND FormatID=" + iFormatID + "");
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                char cDelimiter = ' ';
                DateTime dtime = DateTime.Now;
                //  char cDelimeter = ' ';
                string sNext, sSample2 = "", sSample3 = "", sDateTime = "", sSimbol = "", sCoba = "", sDelimeter = "";
                // string sDocNo = tbFormat.Text.ToString();
                //string sFormat = tbFormat.Text.ToString();
                int n3, n4, n5, n6, n7;
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');

                sDelimeter = CleannonSeparator(sFormat);
                if (n1 < n2 && n1 > -1)
                {

                    sSample = sFormat.Remove(n1, 1).ToString();
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);

                    n6 = sSample.IndexOf('>');
                    if (n2 > -1)
                    {
                        try
                        {
                            sSample2 = sFormat.Substring(n2 + 1).ToString();
                        }
                        catch { };
                        try
                        {
                            sSample = sSample.Remove(n6, sSample.Length - n6).ToString();
                        }
                        catch { };

                    }

                    sNext = sSample;
                    if (sNext.Length <= iNextNo.ToString().Length)
                    {
                        sSample = sNext.Remove(0, sNext.Length).ToString();
                    }
                    else
                    {
                        sSample = sNext.Remove(0, iNextNo.ToString().Length).ToString();
                    }
                    n3 = sSample2.IndexOf('@');
                    n4 = sSample2.IndexOf('{');

                    if (n3 != -1)
                    {
                        sSample3 = sSample2.Remove(0, n3 + 1).ToString();
                        sSample2 = sSample2.Remove(n4, sSample2.Length - n4).ToString();

                        n7 = sSample3.IndexOf('}');
                        if (n7 > -1)
                        {
                            sSample3 = sSample3.Remove(n7, 1).ToString();

                        }

                        for (int i = 0; i < 1; i++)
                        {
                            cDelimiter = System.Convert.ToChar(sDelimeter.Substring(i, i + 1));
                        }
                        n6 = sDelimeter.IndexOf(cDelimiter);
                        // sCoba = ;
                        // sSample = sSample + dtime.ToString(sDateTime) + sSimbol + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sSample = String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);

                    }
                    if (n3 != -1)
                    {
                        sSample3 = CleanInt(sSample3);
                        //CleanString(sSample3);

                        try { sNextDocNo = CleanString(sSample) + sSample2 + dtime.ToString(sSample3); }
                        catch { };

                    }
                    else
                    {
                        sNextDocNo = sSample2 + CleanString(sSample);
                    }



                    // return sNextDocNo;
                    // return sNextDocNo;

                }
                else
                    sNextDocNo = sDocNo;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE PR_DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' AND formatid=" + iFormatID.ToString();
                transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return sNextDocNo;
            }
            return sDocNo; // Manual No.
        }



        // Convert string to save from sql injection + COLLATE
        public static string GSaveStrColl(string s)
        {
            string sRetVal = "'" + GQuoteString(s) + "' COLLATE SQL_Latin1_General_CP437_BIN";
            return sRetVal;
        }

        public static DateTime GGetLastDayOfPrevMonth(DateTime dtDate)
        {
            // set return value to the last day of the month
            // for any date passed in to the method
            // create a datetime variable set to the passed in date
            DateTime dtTo = dtDate;

            // remove all of the days in the next month
            // to get bumped down to the last day of the
            // previous month
            dtTo = dtTo.AddDays(-(dtTo.Day));

            // return the last day of the month
            return dtTo;
        }

        public static DateTime GGetLastDayOfMonth(DateTime dtDate)
        {
            // set return value to the last day of the month
            // for any date passed in to the method
            // create a datetime variable set to the passed in date
            DateTime dtTo = dtDate;

            // overshoot the date by a month
            dtTo = dtTo.AddMonths(1);
            // remove all of the days in the next month
            // to get bumped down to the last day of the
            // previous month
            dtTo = dtTo.AddDays(-(dtTo.Day));

            // return the last day of the month
            return dtTo;
        }

        public static DateTime GGetLastDayOfNextMonth(DateTime dtDate)
        {
            // set return value to the last day of the month
            // for any date passed in to the method
            // create a datetime variable set to the passed in date
            DateTime dtTo = dtDate;

            // overshoot the date by a month


            dtTo = dtTo.AddMonths(1);

            dtTo = GGetLastDayOfMonth(dtTo);

            return dtTo;
        }

        public static DateTime GGetLastDayOfNextYear(DateTime dtDate)
        {
            // set return value to the last day of the month
            // for any date passed in to the method
            // create a datetime variable set to the passed in date
            DateTime dtTo = dtDate;

            // overshoot the date by a month
            dtTo = dtTo.AddYears(1);

            dtTo = GGetLastDayOfMonth(dtTo);

            return dtTo;
        }

        public static string BracketSelectSpecialCharacters(string s)
        {
            string sRetval = "", sC = "";
            string[] aSpecialChars = { "~", "(", ")", "#", "\\", "/", "=", ">", "<", "+", "-", "*", "%", "&", "|", "^", "'", "[", "]", "\"" };
            for (int i = 0; i < s.Length; i++)
            {
                sC = s.Substring(i, 1);
                if (Array.IndexOf(aSpecialChars, sC) == -1)
                    sRetval = sRetval + sC;
                else
                    sRetval = sRetval + "[" + sC + "]";
            }
            return sRetval;
        }

        public static uint GetVolumeSerial(string strDriveLetter)
        {
            uint serNum = 0;
            uint maxCompLen = 0;
            StringBuilder VolLabel = new StringBuilder(256); // Label
            UInt32 VolFlags = new UInt32();
            StringBuilder FSName = new StringBuilder(256); // File System Name
            strDriveLetter += ":\\"; // fix up the passed-in drive letter for the API call
            long Ret = GetVolumeInformation(strDriveLetter, VolLabel, (UInt32)VolLabel.Capacity, ref serNum, ref maxCompLen, ref VolFlags, FSName, (UInt32)FSName.Capacity);

            return serNum;
        }

        public static bool WriteArrayToFile(string[] ar, string sFileNamePath)
        {
            try
            {
                StreamWriter sw = new StreamWriter(sFileNamePath, false); //overwrite
                for (int i = 0; i < ar.Length; i++)
                {
                    sw.Write(ar[i] + "\r\n");
                }
                sw.Flush();
                sw.Close();
                return true;
            }
            catch
            { return false; }
        }

        public static bool WriteArrayToFile(int[] ar, string sFileNamePath)
        {
            try
            {
                StreamWriter sw = new StreamWriter(sFileNamePath, false); //overwrite
                for (int i = 0; i < ar.Length; i++)
                {
                    sw.Write(ar[i].ToString() + "\r\n");
                }
                sw.Flush();
                sw.Close();
                return true;
            }
            catch
            { return false; }
        }

        // If file does not exist, array is truncated to zero
        public static void ReadArrayFromFile(ref string[] ar, string sFileNamePath)
        {
            string line;
            Array.Resize(ref ar, 0);
            try
            {
                StreamReader sr = new StreamReader(sFileNamePath);
                while ((line = sr.ReadLine()) != null)
                {
                    Array.Resize(ref ar, ar.Length + 1);
                    ar[ar.Length - 1] = line;
                }
            }
            catch
            { }

        }

        // If file does not exist, array is truncated to zero
        public static void ReadArrayFromFile(ref int[] ar, string sFileNamePath)
        {
            string line;
            Array.Resize(ref ar, 0);
            try
            {
                StreamReader sr = new StreamReader(sFileNamePath);
                while ((line = sr.ReadLine()) != null)
                {
                    Array.Resize(ref ar, ar.Length + 1);
                    ar[ar.Length - 1] = System.Convert.ToInt32(line);
                }
            }
            catch
            { }

        }

        public static string AutoCountNextDocNo(string sDocType, string sName, DateTime dtDate, SqlConnection conn)
        {
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            // Update next document number
            if (sName == "<<New>>")
            {
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault='T'", conn);
                sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNumber"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE DocNoFormat SET nextnumber=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault='T'";
                SqlCommand cmd = new SqlCommand(sCmdUpdate, conn);
                cmd.ExecuteNonQuery();
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            if (sName != "<<New>>")
            {
                // non-default numbering
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND Name=" + GLib.G.GSaveStrColl(sName), conn);
                sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNumber"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE DocNoFormat SET NextNumber=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND Name=" + GLib.G.GSaveStrColl(sName);
                SqlCommand cmd = new SqlCommand(sCmdUpdate, conn);
                cmd.ExecuteNonQuery();
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            return sName; // Manual No.
        }
        public static string AutoCountNextDocNo(string sDocType, string sName, DateTime dtDate, DBSetting transDBSetting)
        {
            object o;
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            // Update next document number
            if (sName == "<<New>>")
            {
                o = transDBSetting.ExecuteScalar("SELECT NextNumber FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault='T'");
                if (o == null)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(o);
                sSample = "";
                o = transDBSetting.ExecuteScalar("SELECT Format  FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault='T'");
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE DocNoFormat SET nextnumber=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault='T'";
                int x = transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            if (sName != "<<New>>")
            {
                // non-default numbering
                o = transDBSetting.ExecuteScalar("SELECT NextNumber FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND Name=" + GLib.G.GSaveStrColl(sName));
                if (o == null)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(o);
                sSample = "";
                o = transDBSetting.ExecuteScalar("SELECT Format FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND Name=" + GLib.G.GSaveStrColl(sName));
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE DocNoFormat SET NextNumber=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND Name=" + GLib.G.GSaveStrColl(sName);
                transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            return sName; // Manual No.
        }
        // Get AutoCount Next Doc No without updating the DocNoFormat table and using own NextNo
        public static string AutoCountNextDocNo(string sDocType, string sName, DateTime dtDate, DBSetting transDBSetting, int iLastNo)
        {
            object o;
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            // Update next document number
            if (sName == "<<New>>")
            {
                o = transDBSetting.ExecuteScalar("SELECT Format FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault='T'");
                if (o == null)
                    return sName; // No default defined, considered manual docno
                iNextNo = iLastNo;
                sSample = "";
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            if (sName != "<<New>>")
            {
                // non-default numbering
                o = transDBSetting.ExecuteScalar("SELECT Format FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND Name=" + GLib.G.GSaveStrColl(sName));
                if (o == null)
                    return sName; // No default defined, considered manual docno
                sFormat = o.ToString();
                iNextNo = iLastNo;
                sSample = "";
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            return sName; // Manual No.
        }
        public static int GetAutoCountNextNo(string sDocType, string sName, DateTime dtDate, DBSetting transDBSetting)
        {
            object o;
            int iNextNo = 0;
            if (sName == "<<New>>")
            {
                o = transDBSetting.ExecuteScalar("SELECT NextNumber FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault='T'");
                if (o != null)
                    iNextNo = System.Convert.ToInt32(o);
            }
            else
            {
                o = transDBSetting.ExecuteScalar("SELECT NextNumber FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND Name=" + GLib.G.GSaveStrColl(sName));
                if (o != null)
                    iNextNo = System.Convert.ToInt32(o);
            }
            return iNextNo;
        }
        public static void UpdateAutoCountNextNo(string sDocType, string sName, DateTime dtDate, DBSetting transDBSetting, int iNextNo)
        {
            string sCmdUpdate;
            if (sName == "<<New>>")
            {
                sCmdUpdate = "UPDATE DocNoFormat SET nextnumber=" + System.Convert.ToString(iNextNo) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault='T'";
            }
            else
            {
                sCmdUpdate = "UPDATE DocNoFormat SET NextNumber=" + System.Convert.ToString(iNextNo) +
                   " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND Name=" + GLib.G.GSaveStrColl(sName);
            }
            transDBSetting.ExecuteNonQuery(sCmdUpdate);
        }

        public static string ParseDocNoFormat(string s, DateTime dt)
        {
            string sRetVal = s;
            string ss = "";
            int n1, n2;
            n1 = s.IndexOf('{');
            n2 = s.IndexOf('}');
            if (n1 > 0 && n2 > 0)
            {
                ss = s.Substring(n1 + 1, n2 - n1);
                ss = "{0:" + ss;
                ss = Regex.Replace(ss, @"@", ""); // Discard: @
                sRetVal = s.Substring(0, n1);
                try
                {
                    sRetVal = sRetVal + String.Format(ss, dt);
                }
                catch
                {
                }
                sRetVal = sRetVal + s.Substring(n2 + 1);
            }
            else
            {
                if (n1 > 0)
                {
                    ss = s.Substring(n1 + 1);
                }
            }
            if (sRetVal.IndexOf('{') > 0 && sRetVal.IndexOf('}') > 0)
                sRetVal = ParseDocNoFormat(sRetVal, dt);
            return sRetVal;
        }

        public static string AutoCountPossibleNextDocNo(string sDocType, string sName, DateTime dtDate, SqlConnection conn)
        {
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            // Update next document number
            if (sName == "<<New>>")
            {
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault='T'", conn);
                sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNumber"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            if (sName != "<<New>>")
            {
                // non-default numbering
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND Name=" + GLib.G.GSaveStrColl(sName), conn);
                sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNumber"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            return sName; // Manual No.
        }

        public static string GetNextDocNoX(string sDocType, string sName, int iFormatID, DateTime dtDate, SqlConnection conn, string sPlugIns)
        {
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            sPlugIns = sPlugIns + "_";

            // Update next document number
            if (sName == "<<New>>")
            {
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault=1", conn);
                sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNo"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE " + sPlugIns + "DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault=1";
                SqlCommand cmd = new SqlCommand(sCmdUpdate, conn);
                cmd.ExecuteNonQuery();
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            if (sName != "<<New>>")
            {
                // non-default numbering
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString(), conn);
                sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNo"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE " + sPlugIns + "DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString();
                SqlCommand cmd = new SqlCommand(sCmdUpdate, conn);
                cmd.ExecuteNonQuery();
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            return sName; // Manual No.
        }
        public static string GetNextDocNoX(string sDocType, string sName, int iFormatID, DateTime dtDate, DBSetting transDBSetting, string sPlugIns)
        {
            object o;
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            sPlugIns = sPlugIns + "_";

            // Update next document number
            if (sName == "<<New>>")
            {
                DataTable dt = new DataTable();
                o = transDBSetting.ExecuteScalar("SELECT NextNo FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault=1");
                if (o == null)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(o);
                o = transDBSetting.ExecuteScalar("SELECT Format FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault=1");
                sSample = "";
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE " + sPlugIns + "DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault=1";
                transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            if (sName != "<<New>>")
            {
                // non-default numbering
                DataTable dt = new DataTable();
                o = transDBSetting.ExecuteScalar("SELECT NextNo FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString());
                if (o == null)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(o);
                o = transDBSetting.ExecuteScalar("SELECT Format FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString());
                sSample = "";
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE " + sPlugIns + "DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString();
                transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            return sName; // Manual No.
        }
        public static string GetNextDocNoX(string sDocType, string sName, int iFormatID, DateTime dtDate, DBSetting transDBSetting, string sPlugIns, string AssetGroup)
        {
            object o;
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            sPlugIns = sPlugIns + "_";
            // Update next document number
            if (sName == "<<New>>")
            {
                DataTable dt = new DataTable();
                o = transDBSetting.ExecuteScalar("SELECT NextNo FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND AssetGroup='" + AssetGroup + "'");
                if (o == null)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(o);
                o = transDBSetting.ExecuteScalar("SELECT Format FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND AssetGroup='" + AssetGroup + "'");
                sSample = "";
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE " + sPlugIns + "DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND AssetGroup='" + AssetGroup + "'";
                transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            if (sName != "<<New>>")
            {
                // non-default numbering
                DataTable dt = new DataTable();
                o = transDBSetting.ExecuteScalar("SELECT NextNo FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString());
                if (o == null)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(o);
                o = transDBSetting.ExecuteScalar("SELECT Format FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString());
                sSample = "";
                sFormat = o.ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                string sCmdUpdate;
                sCmdUpdate = "UPDATE " + sPlugIns + "DocNoFormat SET nextno=" + System.Convert.ToString(iNextNo + 1) +
                    " WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString();
                transDBSetting.ExecuteNonQuery(sCmdUpdate);
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            return sName; // Manual No.
        }
        public static string PossibleNextDocNoX(string sDocType, string sName, int iFormatID, DateTime dtDate, SqlConnection conn, string sPlugIns)
        {
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            sPlugIns = sPlugIns + "_";
            // Update next document number
            if (sName == "<<New>>")
            {
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND isdefault=1", conn);
                sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNo"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }

            if (sName != "<<New>>")
            {
                // non-default numbering
                DataTable dt = new DataTable();
                SqlDataAdapter sa = new SqlDataAdapter("SELECT * FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString(), conn);
                sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNo"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            return sName; // Manual No.
        }
        public static string PossibleNextDocNoX(string sDocType, string sName, int iFormatID, DateTime dtDate, DBSetting dbsetting, string sPlugIns, string AssetGroup)
        {
            string sNextDocNo = "", sSample, sFormat, ss, s;
            int iNextNo, n1, n2;
            sPlugIns = sPlugIns + "_";
            // Update next document number
            if (sName == "<<New>>")
            {
                DataTable dt = new DataTable();
                dt = dbsetting.GetDataTable("SELECT * FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND AssetGroup='" + AssetGroup + "'", false, "");
                //sa.Fill(dt);

                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNo"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }

            if (sName != "<<New>>")
            {
                // non-default numbering
                DataTable dt = new DataTable();
                dt = dbsetting.GetDataTable("SELECT * FROM " + sPlugIns + "DocNoFormat WHERE doctype='" + sDocType + "' COLLATE SQL_Latin1_General_CP437_BIN AND FormatID=" + iFormatID.ToString(), false, "");
                // sa.Fill(dt);
                if (dt.Rows.Count == 0)
                    return sName; // No default defined, considered manual docno
                iNextNo = System.Convert.ToInt32(dt.Rows[0]["NextNo"]);
                sSample = "";
                sFormat = dt.Rows[0]["Format"].ToString();
                n1 = sFormat.IndexOf('<');
                n2 = sFormat.IndexOf('>');
                if (n1 < n2 && n1 >= 0)
                {
                    sSample = sFormat.Substring(0, n1);
                    s = sFormat.Substring(n1 + 1, n2 - n1 - 1);
                    ss = "";
                    for (int n = 0; n < s.Length; n++)
                    {
                        if (s.Substring(n, 1) == "0")
                            ss = ss + "0";
                    }
                    if (ss.Length != s.Length)
                        sNextDocNo = sName;
                    else
                    {
                        sSample = sSample + String.Format("{0:D" + System.Convert.ToString(s.Length) + "}", iNextNo);
                        sNextDocNo = sSample + sFormat.Substring(n2 + 1);
                    }
                }
                else
                    sNextDocNo = sName;
                return ParseDocNoFormat(sNextDocNo, dtDate);
            }
            return sName; // Manual No.
        }

        // If file does not exist, array is truncated to zero
        // Delimiter is comma
        // sPlugIns is without underscore -> FA will be FA_
        public static void ReadUserLayout(ref string[] arFields, ref int[] arWidths, string sUserID, string sLayout, string sPlugIns)
        {
            Array.Resize(ref arFields, 0);
            Array.Resize(ref arWidths, 0);
            try
            {
                GDataTable gdt = new GDataTable();
                DataTable dt = new DataTable();
                string sLine = "";
                gdt.LoadDataTable(dt, "Layout", "SELECT * FROM " + sPlugIns + "_Layout WHERE userid=" +
                    GLib.G.GSaveStrColl(sUserID) + " AND layout=" + GLib.G.GSaveStrColl(sLayout));
                if (dt.Rows.Count > 0)
                {
                    sLine = dt.Rows[0]["Fields"].ToString();
                    arFields = sLine.Split(new char[] { ',' });
                    sLine = dt.Rows[0]["Widths"].ToString();
                    string[] s = sLine.Split(new char[] { ',' });
                    for (int i = 0; i < s.Length; i++)
                    {
                        Array.Resize(ref arWidths, arWidths.Length + 1);
                        arWidths[arWidths.Length - 1] = System.Convert.ToInt32(s[i]);
                    }
                }
            }
            catch
            { }
        }
        public static bool WriteUserLayout(string[] arFields, int[] arWidths, string sUserID, string sLayout, string sPlugIns)
        {
            SqlConnection conn = new SqlConnection(BCE.AutoCount.Application.DBSetting.ConnectionString);
            string sSQL = ""; SqlCommand cmd;
            object o;
            try
            {
                string sFields = "";
                for (int i = 0; i < arFields.Length; i++)
                {
                    if (i > 0)
                        sFields = sFields + ",";
                    sFields = sFields + arFields[i];
                }
                string sWidths = "";
                for (int i = 0; i < arWidths.Length; i++)
                {
                    if (i > 0)
                        sWidths = sWidths + ",";
                    sWidths = sWidths + arWidths[i].ToString();
                }

                conn.Open();
                cmd = new SqlCommand("BEGIN TRANSACTION", conn);
                cmd.ExecuteNonQuery();
                sSQL = "SELECT Layout FROM " + sPlugIns + "_Layout WHERE UserID=" + GLib.G.GSaveStrColl(sUserID) +
                    " AND Layout=" + GLib.G.GSaveStrColl(sLayout);
                cmd = new SqlCommand(sSQL, conn);
                o = cmd.ExecuteScalar();
                if (o != null)
                    sSQL = "UPDATE " + sPlugIns + "_Layout SET UserID='" + GLib.G.GQuoteString(sUserID) + "'," +
                        "Layout='" + GLib.G.GQuoteString(sLayout) + "'," +
                        "Fields='" + GLib.G.GQuoteString(sFields) + "'," +
                        "Widths='" + GLib.G.GQuoteString(sWidths) + "'" +
                        " WHERE UserID=" + GLib.G.GSaveStrColl(sUserID) +
                        " AND Layout=" + GLib.G.GSaveStrColl(sLayout);
                else
                    sSQL = "INSERT INTO " + sPlugIns + "_Layout VALUES ('" +
                        GLib.G.GQuoteString(sUserID) + "'," +
                        "'" + GLib.G.GQuoteString(sLayout) + "'," +
                        "'" + GLib.G.GQuoteString(sFields) + "'," +
                        "'" + GLib.G.GQuoteString(sWidths) + "')";
                cmd = new SqlCommand(sSQL, conn);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand("COMMIT TRANSACTION", conn);
                cmd.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch
            {
                cmd = new SqlCommand("ROLLBACK TRANSACTION", conn);
                cmd.ExecuteNonQuery();
                conn.Close();
                return false;
            }
        }

        public static void SetDefaultDateFormat(ref DevExpress.XtraEditors.DateEdit de)
        {
            //de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            //de.Properties.DisplayFormat.FormatString = BCE.AutoCount.Application.ShortDateFormat;
            //de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            //de.Properties.EditFormat.FormatString = BCE.AutoCount.Application.ShortDateFormat;
            //de.Properties.EditMask = BCE.AutoCount.Application.ShortDateFormat;
        }

        public static void SetDefaultDateFormat(ref DateTimePicker dtp)
        {
            //dtp.Format = DateTimePickerFormat.Custom;
            //dtp.CustomFormat = BCE.AutoCount.Application.ShortDateFormat;
        }

        public static void CopyDataRow(DataRow drSource, ref DataRow drDestination)
        {
            for (int i = 0; i < drSource.Table.Columns.Count; i++)
            {
                drDestination[i] = drSource[i];
            }
        }

        #region registration block
        ////////////////////////////////////////////////////////////////////////////////
        // Registration block start        
        //
        // Program name adalah nama program terakhir baik DLL maupun EXE, contoh:FA.DLL ->FA. 
        //  HARUS HURUF BESAR SEMUA. TANPA NAMA EXTENTION.
        //  
        // Untuk mengecek validitas software:
        //  Decrypt dari key code yg diencrypt yang ada dalam database ===> A
        //  GLib.G.GetProductID(); ===> B
        // Apakah A==B

        // GUID is 36 digits
        // GUID will be taken from sys.database_files, if it is null sys.database (name=BCE.AutoCount.Application.DBSetting.DBName
        public static string GetProductID(string sProgramName)
        {

            //BCE.XtraUtils.();
            char c;
            string sRetVal = "";
            string sDefaultGUID = "5DD1B2D6-A629-4EDF-B869-F18A1EF69F8F", sGUID;
            string sDBName = BCE.AutoCount.Application.DBSetting.DBName.ToString();
            GExecuteScalar es = new GExecuteScalar();
            object o = es.Query("SELECT file_guid FROM sys.database_files");
            if (o == System.DBNull.Value)
            {
                o = es.Query("SELECT service_broker_guid FROM sys.databases WHERE name=" + GSaveStrColl(sDBName));
            }
            if (o == System.DBNull.Value)
                sGUID = sDefaultGUID;
            else
                sGUID = o.ToString().ToUpper();
            sGUID = sGUID.Substring(sGUID.Length - 9);
          //  BCE.Application.AppMessage.ShowErrorMessage("12");
            sProgramName = sProgramName.PadRight(sGUID.Length, 'G');
            for (int i = 0; i < sGUID.Length; i++)
            {
                c = sGUID[i];
                if (Char.IsUpper(sProgramName[i]) && Char.IsUpper(c) &&
                    (Char.IsUpper((char)(sProgramName[i] - c + 'A')) || Char.IsDigit((char)(sProgramName[i] - c + 'A'))))
                    sRetVal = sRetVal + ((char)(sProgramName[i] - c + 'A'));
                else
                    sRetVal = sRetVal + sGUID[i];
            }
            if (sRetVal.Length < sGUID.Length)
            {
                for (int i = sRetVal.Length; i < sGUID.Length; i++)
                {
                    sRetVal = sRetVal + sGUID[i];
                }
            }
            return sRetVal;
        }
        public static string GetProductID(string sProgramName,string macKey)
        {
            //BCE.XtraUtils.();
            char c;
            string sRetVal = "";
            string sDefaultGUID = "5DD1B2D6-A629-4EDF-B869-F18A1EF69F8F", sGUID;
            string sDBName = BCE.AutoCount.Application.DBSetting.DBName.ToString();

            sProgramName = EncryptString(sProgramName);
            GExecuteScalar es = new GExecuteScalar();
            object o = es.Query("SELECT file_guid FROM sys.database_files");
            if (o == System.DBNull.Value)
            {
                o = es.Query("SELECT service_broker_guid FROM sys.databases WHERE name=" + GSaveStrColl(sDBName));
            }
            if (o == System.DBNull.Value)
                sGUID = sDefaultGUID;
            else
                sGUID = o.ToString().ToUpper();
            sGUID = sGUID.Substring(sGUID.Length - 12);
            //BCE.Application.AppMessage.ShowErrorMessage("12");
            sProgramName = sProgramName.PadRight(sGUID.Length, 'G');
            for (int i = 0; i < sGUID.Length; i++)
            {
                c = sGUID[i];
                if (Char.IsUpper(sProgramName[i]) && Char.IsUpper(c) &&
                    (Char.IsUpper((char)(sProgramName[i] - c + 'A')) || Char.IsDigit((char)(sProgramName[i] - c + 'A'))))
                    sRetVal = sRetVal + ((char)(sProgramName[i] - c + 'A'));
                else
                    sRetVal = sRetVal + sGUID[i];
            }
            if (sRetVal.Length < sGUID.Length)
            {
                for (int i = sRetVal.Length; i < sGUID.Length; i++)
                {
                    sRetVal = sRetVal + sGUID[i];
                }
            }



            return sRetVal;
        }
        //// C drive volume will be encoded as product ID
        //public static string GetProductID(string sProgramName)
        //{
        //    char c;
        //    string sRetVal = "",s;
        //    uint serNum=GetVolumeSerial("C");
        //    long numID = (serNum ^ 235);
        //    s = B10_B36(numID);
        //    sProgramName = sProgramName.PadRight(s.Length, 'G');
        //    for (int i = 0; i<s.Length; i++)
        //    {
        //        c = s[i];
        //        if (Char.IsUpper(sProgramName[i]) && Char.IsUpper(c) && 
        //            (Char.IsUpper((char)(c - sProgramName[i] + 'A')) || Char.IsDigit((char)(c - sProgramName[i] + 'A'))))
        //            sRetVal = sRetVal + ((char)(c - sProgramName[i] + 'A'));
        //        else
        //            sRetVal = sRetVal + s[i];
        //    }
        //    if (sRetVal.Length < s.Length)
        //    {
        //        for (int i = sRetVal.Length; i < s.Length; i++)
        //        {
        //            sRetVal = sRetVal + s[i];
        //        }
        //    }
        //    return sRetVal.PadLeft(7,'0');
        //}
        public static string EncryptString( string plainText)
        {
            
            byte[] iv = new byte[16];
            byte[] array;
            //var key = "b14ca5898a4e4133bbce2ea2315a1916";
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(MyKey);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return System.Convert.ToBase64String(array);
        }

        public static string DecryptString(string key, string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = System.Convert.FromBase64String(cipherText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

        public static void EncryptFile(string inputFile, string outputFile)
        {

            try
            {
                string password = @"myKey123"; // Your Key Here
                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(password);

                string cryptFile = outputFile;
                FileStream fsCrypt = new FileStream(cryptFile, FileMode.Create);

                RijndaelManaged RMCrypto = new RijndaelManaged();

                CryptoStream cs = new CryptoStream(fsCrypt,
                    RMCrypto.CreateEncryptor(key, key),
                    CryptoStreamMode.Write);

                FileStream fsIn = new FileStream(inputFile, FileMode.Open);
                int data;
                while ((data = fsIn.ReadByte()) != -1)
                    cs.WriteByte((byte)data);


                fsIn.Close();
                cs.Close();
                fsCrypt.Close();
            }
            catch
            {
                MessageBox.Show("Encryption failed!", "Error");
            }
        }
        public static void DecryptFile(string inputFile, string outputFile)
        {

            {
                string password = @"myKey123"; // Your Key Here

                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(password);

                FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);

                RijndaelManaged RMCrypto = new RijndaelManaged();

                CryptoStream cs = new CryptoStream(fsCrypt,
                    RMCrypto.CreateDecryptor(key, key),
                    CryptoStreamMode.Read);

                FileStream fsOut = new FileStream(outputFile, FileMode.Create);

                int data;
                while ((data = cs.ReadByte()) != -1)
                    fsOut.WriteByte((byte)data);

                fsOut.Close();
                cs.Close();
                fsCrypt.Close();

            }
        }
        public static void AddEncryption(string FileName)
        {

            File.Encrypt("Encrypt.CSV");

        }

        // Decrypt a file.
        public static void RemoveEncryption(string FileName)
        {
            File.Decrypt(FileName);
        }

        public static bool IsRegistered(string sTableName, string sProgramName)
        {
            string sStoredCode = "", sCode = "";
            string sQuery = "SELECT rk FROM " + sTableName;
            GExecuteScalar es = new GExecuteScalar();

            object o;
            try
            {
                o = es.Query(sQuery);
                if (o == System.DBNull.Value)
                    return false;
                sStoredCode = o.ToString();//Decrypt(System.Convert.ToDecimal(o))
                sCode = Encrypt(GetProductID(sProgramName)).ToString();
                if (sCode == sStoredCode)
                    return true;
            }
            catch
            {
                return false;
            }
            return false;
        }

        public static bool IsRegistered(string sTableName, string sProgramName,string sMacKey)
        {
            string sStoredCode = "", sCode = "";
            string sQuery = "SELECT rk FROM " + sTableName;
            GExecuteScalar es = new GExecuteScalar();

            object o;
            try
            {
                o = es.Query(sQuery);
                if (o == System.DBNull.Value)
                    return false;
                sStoredCode =o.ToString();//Decrypt(System.Convert.ToDecimal(o))
                sCode =Encrypt(GetProductID(sProgramName, sMacKey)).ToString();                

                if (sCode == sStoredCode)
                    return true;
            }
            catch(Exception ex)
            {
                BCE.Application.AppMessage.ShowErrorMessage(ex.Message);
                return false;
            }
            return false;
        }

        // digits to encrypt must be = 9
        public static decimal Encrypt(string s)
        {
            s = s.Substring(0, s.Length - 2);
            decimal dRetVal = 0;
            string sTmp = "", s1;
            char c;
            for (int i = 0; i < s.Length; i++)
            {
                c = s[i];
                c = (char)(c ^ 109);
                c = (char)(c ^ ((i + 1) * 27));
                s1 = ((int)c).ToString("000");
                //s1 = s1.PadLeft(3, '0');
                sTmp += s1;
            }
            dRetVal = System.Convert.ToDecimal(sTmp);
            return dRetVal;
        }
        public static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
        {
            // Create a MemoryStream to accept the encrypted bytes 

            MemoryStream ms = new MemoryStream();

            // Create a symmetric algorithm. 

            // We are going to use Rijndael because it is strong and

            // available on all platforms. 

            // You can use other algorithms, to do so substitute the

            // next line with something like 

            //      TripleDES alg = TripleDES.Create(); 

            Rijndael alg = Rijndael.Create();

            // Now set the key and the IV. 

            // We need the IV (Initialization Vector) because

            // the algorithm is operating in its default 

            // mode called CBC (Cipher Block Chaining).

            // The IV is XORed with the first block (8 byte) 

            // of the data before it is encrypted, and then each

            // encrypted block is XORed with the 

            // following block of plaintext.

            // This is done to make encryption more secure. 


            // There is also a mode called ECB which does not need an IV,

            // but it is much less secure. 

            alg.Key = Key;
            alg.IV = IV;

            // Create a CryptoStream through which we are going to be

            // pumping our data. 

            // CryptoStreamMode.Write means that we are going to be

            // writing data to the stream and the output will be written

            // in the MemoryStream we have provided. 

            CryptoStream cs = new CryptoStream(ms,
               alg.CreateEncryptor(), CryptoStreamMode.Write);

            // Write the data and make it do the encryption 

            cs.Write(clearData, 0, clearData.Length);

            // Close the crypto stream (or do FlushFinalBlock). 

            // This will tell it that we have done our encryption and

            // there is no more data coming in, 

            // and it is now a good time to apply the padding and

            // finalize the encryption process. 

            cs.Close();

            // Now get the encrypted data from the MemoryStream.

            // Some people make a mistake of using GetBuffer() here,

            // which is not the right way. 

            byte[] encryptedData = ms.ToArray();

            return encryptedData;
        }

        public static byte[] Encrypt(byte[] clearData, string Password)
        {
            // We need to turn the password into Key and IV. 

            // We are using salt to make it harder to guess our key

            // using a dictionary attack - 

            // trying to guess a password by enumerating all possible words. 

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the encryption using the function

            // that accepts byte arrays. 

            // Using PasswordDeriveBytes object we are first getting

            // 32 bytes for the Key 

            // (the default Rijndael key length is 256bit = 32bytes)

            // and then 16 bytes for the IV. 

            // IV should always be the block size, which is by default

            // 16 bytes (128 bit) for Rijndael. 

            // If you are using DES/TripleDES/RC2 the block size is 8

            // bytes and so should be the IV size. 

            // You can also read KeySize/BlockSize properties off the

            // algorithm to find out the sizes. 

            return Encrypt(clearData, pdb.GetBytes(32), pdb.GetBytes(16));

        }
        public static string Decrypt(string cipherText, string Password)
        {
            // First we need to turn the input string into a byte array. 

            // We presume that Base64 encoding was used 

            byte[] cipherBytes = System.Convert.FromBase64String(cipherText);

            // Then, we need to turn the password into Key and IV 

            // We are using salt to make it harder to guess our key

            // using a dictionary attack - 

            // trying to guess a password by enumerating all possible words. 

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 
            0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the decryption using

            // the function that accepts byte arrays. 

            // Using PasswordDeriveBytes object we are first

            // getting 32 bytes for the Key 

            // (the default Rijndael key length is 256bit = 32bytes)

            // and then 16 bytes for the IV. 

            // IV should always be the block size, which is by

            // default 16 bytes (128 bit) for Rijndael. 

            // If you are using DES/TripleDES/RC2 the block size is

            // 8 bytes and so should be the IV size. 

            // You can also read KeySize/BlockSize properties off

            // the algorithm to find out the sizes. 

            byte[] decryptedData = Decrypt(cipherBytes,
                pdb.GetBytes(32), pdb.GetBytes(16));

            // Now we need to turn the resulting byte array into a string. 

            // A common mistake would be to use an Encoding class for that.

            // It does not work 

            // because not all byte values can be represented by characters. 

            // We are going to be using Base64 encoding that is 

            // designed exactly for what we are trying to do. 

            return System.Text.Encoding.Unicode.GetString(decryptedData);
        }

        // Decrypt bytes into bytes using a password 

        //    Uses Decrypt(byte[], byte[], byte[]) 


        public static byte[] Decrypt(byte[] cipherData, string Password)
        {
            // We need to turn the password into Key and IV. 

            // We are using salt to make it harder to guess our key

            // using a dictionary attack - 

            // trying to guess a password by enumerating all possible words. 

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the Decryption using the 

            //function that accepts byte arrays. 

            // Using PasswordDeriveBytes object we are first getting

            // 32 bytes for the Key 

            // (the default Rijndael key length is 256bit = 32bytes)

            // and then 16 bytes for the IV. 

            // IV should always be the block size, which is by default

            // 16 bytes (128 bit) for Rijndael. 

            // If you are using DES/TripleDES/RC2 the block size is

            // 8 bytes and so should be the IV size. 


            // You can also read KeySize/BlockSize properties off the

            // algorithm to find out the sizes. 

            return Decrypt(cipherData, pdb.GetBytes(32), pdb.GetBytes(16));
        }
        public static byte[] Decrypt(byte[] cipherData,
                               byte[] Key, byte[] IV)
        {
            // Create a MemoryStream that is going to accept the

            // decrypted bytes 

            MemoryStream ms = new MemoryStream();

            // Create a symmetric algorithm. 

            // We are going to use Rijndael because it is strong and

            // available on all platforms. 

            // You can use other algorithms, to do so substitute the next

            // line with something like 

            //     TripleDES alg = TripleDES.Create(); 

            Rijndael alg = Rijndael.Create();

            // Now set the key and the IV. 

            // We need the IV (Initialization Vector) because the algorithm

            // is operating in its default 

            // mode called CBC (Cipher Block Chaining). The IV is XORed with

            // the first block (8 byte) 

            // of the data after it is decrypted, and then each decrypted

            // block is XORed with the previous 

            // cipher block. This is done to make encryption more secure. 

            // There is also a mode called ECB which does not need an IV,

            // but it is much less secure. 

            alg.Key = Key;
            alg.IV = IV;

            // Create a CryptoStream through which we are going to be

            // pumping our data. 

            // CryptoStreamMode.Write means that we are going to be

            // writing data to the stream 

            // and the output will be written in the MemoryStream

            // we have provided. 

            CryptoStream cs = new CryptoStream(ms,
                alg.CreateDecryptor(), CryptoStreamMode.Write);

            // Write the data and make it do the decryption 

            cs.Write(cipherData, 0, cipherData.Length);

            // Close the crypto stream (or do FlushFinalBlock). 

            // This will tell it that we have done our decryption

            // and there is no more data coming in, 

            // and it is now a good time to remove the padding

            // and finalize the decryption process. 

            cs.Close();

            // Now get the decrypted data from the MemoryStream. 

            // Some people make a mistake of using GetBuffer() here,

            // which is not the right way. 

            byte[] decryptedData = ms.ToArray();

            return decryptedData;
        }


        public static string Encrypt(string clearText, string Password)
        {
            // First we need to turn the input string into a byte array. 

            byte[] clearBytes =
              System.Text.Encoding.Unicode.GetBytes(clearText);

            // Then, we need to turn the password into Key and IV 

            // We are using salt to make it harder to guess our key

            // using a dictionary attack - 

            // trying to guess a password by enumerating all possible words. 

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the encryption using the

            // function that accepts byte arrays. 

            // Using PasswordDeriveBytes object we are first getting

            // 32 bytes for the Key 

            // (the default Rijndael key length is 256bit = 32bytes)

            // and then 16 bytes for the IV. 

            // IV should always be the block size, which is by default

            // 16 bytes (128 bit) for Rijndael. 

            // If you are using DES/TripleDES/RC2 the block size is

            // 8 bytes and so should be the IV size. 

            // You can also read KeySize/BlockSize properties off

            // the algorithm to find out the sizes. 

            byte[] encryptedData = Encrypt(clearBytes,
                     pdb.GetBytes(32), pdb.GetBytes(16));

            // Now we need to turn the resulting byte array into a string. 

            // A common mistake would be to use an Encoding class for that.

            //It does not work because not all byte values can be

            // represented by characters. 

            // We are going to be using Base64 encoding that is designed

            //exactly for what we are trying to do. 

            return System.Convert.ToBase64String(encryptedData);

        }

        public static string Decrypt(decimal dCode)
        {
            string s = dCode.ToString(), sRetVal = "";
            //s = s.Substring(0, s.Length - 2);
            s = s.PadLeft(27, '0');

            char c;
            for (int i = 0; i < s.Length; i = i + 3)
            {
                c = System.Convert.ToChar(System.Convert.ToInt32(s.Substring(i, 3)));
                c = (char)(c ^ ((i / 3 + 1) * 27));
                c = (char)(c ^ 109);
                sRetVal += c;
            }
            return sRetVal;
        }

        // Base 36 to base 10
        // Case insensitive
        public static long B36_B10(string s)
        {
            long n10;
            int nLen;
            char c;
            nLen = s.Length;
            s = s.ToUpper();
            n10 = 0;
            for (int n = nLen; n > 0; n--)
            {
                c = s[n - 1];
                if (Char.IsDigit(c) || Char.IsUpper(c))
                {
                    if (Char.IsUpper(c))
                        n10 = n10 + (long)(System.Math.Pow(36, (nLen - n)) * (System.Convert.ToInt16(c) - 65 + 10));
                    else
                        n10 = n10 + (long)(System.Math.Pow(36, (nLen - n)) * (System.Convert.ToInt16(c) - '0'));
                }
                else
                    return 0;
            }
            return n10;
        }

        //* Base 10 to base 36
        //* Case insensitive
        public static string B10_B36(long n10)
        {
            string c = "";
            long n;
            while (true)
            {
                n = n10 - ((long)(n10 / 36)) * 36;
                n10 = (long)(n10 / 36);
                if (n <= 9)
                    c = ((char)(n + 48)).ToString() + c;
                else
                    c = ((char)(n - 10 + 'A')).ToString() + c;
                if (n10 < 36)
                {
                    if (n10 <= 9)
                        c = ((char)(n10 + 48)).ToString() + c;
                    else
                        c = ((char)(n10 - 10 + 'A')).ToString() + c;
                    break;
                }
            }
            return c;
        }

        public static decimal EncryptDateTime(DateTime dt)
        {
            string s = dt.Year.ToString("0000") + dt.Month.ToString("00") + dt.Day.ToString("00") + "X";
            //string sCode = "JWPARVDISE", s1 = "";
            //for (int i = 0; i < s.Length; i++)
            //{
            //    s1 = s1 + sCode[System.Convert.ToInt32(s.Substring(i, 1))].ToString();
            //}
            decimal d = GLib.G.Encrypt(s);
            return d;
        }

        public static DateTime DecryptDateTime(decimal d)
        {
            string s = GLib.G.Decrypt(d);
            //string sCode = "JWPARVDISE", s1 = "";
            //for (int i = 0; i < s.Length; i++)
            //{
            //    s1 = s1 + sCode.IndexOf(s[i]);
            //}
            DateTime dt = EmptyDateTime();
            try
            {
                dt = new DateTime(System.Convert.ToInt32(s.Substring(0, 4)),
                System.Convert.ToInt32(s.Substring(4, 2)),
                System.Convert.ToInt32(s.Substring(6, 2)));
            }
            catch
            {
                return EmptyDateTime();
            }
            return dt;
        }

        public static DateTime GetLicenseDate(string sTableName, string sProgramName)
        {
            GExecuteScalar es = new GExecuteScalar();

            object o;
            decimal d;
            try
            {
                o = es.Query("SELECT rkd FROM " + sTableName);
                if (o == System.DBNull.Value)
                    return EmptyDateTime();
                d = System.Convert.ToDecimal(o);
                return DecryptDateTime(d);

            }
            catch
            {
                return EmptyDateTime();
            }
            return EmptyDateTime();
        }

        // End of Registration block
        ////////////////////////////////////////////////////////////////////////////////
        #endregion

        // E.g.: FilterSortData(dt,"Price>1000","ItemCode,LocatioN")
        public static DataTable FilterSortData(DataTable dtSrc, string filter, string sort)
        {
            DataTable dt = dtSrc.Clone();
            DataRow[] drs = dtSrc.Select(filter, sort);
            foreach (DataRow dr in drs)
            {
                dt.ImportRow(dr);
            }

            return dt;
        }
        public static DataTable FilterSortData(DataTable dtSrc, string sort)
        {
            DataTable dt = dtSrc.Clone();
            DataRow[] drs = dtSrc.Select(null, sort);
            foreach (DataRow dr in drs)
            {
                dt.ImportRow(dr);
            }

            return dt;
        }

        public static void SetNumericTextEdit(ref DevExpress.XtraEditors.TextEdit te, int iDecimal)
        {
            string sFormatString = "N";
            te.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.EditFormat.FormatString = sFormatString + iDecimal.ToString();
            te.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.DisplayFormat.FormatString = sFormatString + iDecimal.ToString();
            te.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
        }

        // Default delimiter: ' ', ',', '.', ':', '\t'
        // Usage:
        // string[] sa = ArrayOfString("Satu,Dua,Tiga");
        public static string[] ArrayOfString(string saString)
        {
            char[] delimiterChars = { ' ', ',', '.', ':', '\t' };

            string[] words = saString.Split(delimiterChars);

            return words;
        }

        // Usage:
        // string[] sa = ArrayOfString("Satu,Dua,Tiga",{','});
        public static string[] ArrayOfString(string saString, char[] delimiterChars)
        {
            string[] words = saString.Split(delimiterChars);

            return words;
        }

        #region List Delimited block
        // Get N's data from string delimited data
        // E.g.: s='The,Best,Of,Us'
        //  GDList(s,2) returns 'Best'
        public static string GDList(string sList, int nPos, char cDelimiter)
        {
            string sRetVal = "";
            string[] split = sList.Split(cDelimiter);
            if (nPos - 1 < split.Length)
                return split[nPos - 1];
            return sRetVal;
        }
        //  Default delimiter is comma
        public static string GDList(string sList, int nPos)
        {
            char cDelimiter = ',';
            string sRetVal = "";
            string[] split = sList.Split(cDelimiter);
            if (nPos - 1 < split.Length)
                return split[nPos - 1];
            return sRetVal;
        }
        public static int GDListTotal(string sList, char cDelimiter)
        {
            string[] split = sList.Split(cDelimiter);
            return split.Length;
        }
        public static int GDListTotal(string sList)
        {
            char cDelimiter = ',';
            string[] split = sList.Split(cDelimiter);
            return split.Length;
        }
        public static bool GDListFind(string sList, string sKey, char cDelimiter)
        {
            string[] split = sList.Split(cDelimiter);
            for (int i = 0; i < split.Length; i++)
            {
                if (split[i].ToString() == sKey)
                {
                    return true;
                }
            }
            return false;
        }
        public static bool GDListFind(string sList, string sKey)
        {
            char cDelimiter = ',';
            string[] split = sList.Split(cDelimiter);
            for (int i = 0; i < split.Length; i++)
            {
                if (split[i].ToString() == sKey)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

    }

}
