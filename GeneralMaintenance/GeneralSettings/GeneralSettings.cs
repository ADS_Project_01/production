﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using BCE.Data;
using System.Globalization;
using System.Threading;
using System.IO;
using GLib;
namespace Production.GeneralMaintenance
{
    [BCE.AutoCount.PlugIn.MenuItem("General Settings", BeginNewGroup = false, MenuOrder = 1, OpenAccessRight = "RPA_SETTINGS_OPEN", ParentBeginNewGroup = false, ParentMenuCaption = "General Maintenance", ParentMenuOrder = 1, ShowAsDialog = false, VisibleAccessRight = "RPA_SETTINGS_SHOW")]
   // [BCE.AutoCount.PlugIn.MenuItem("General Settings", 1, false, "RPA_SETTINGS_SHOW", "RPA_SETTINGS_OPEN")]
    [BCE.Application.SingleInstanceThreadForm]

    //[BCE.AutoCount.PlugIns.PlugInsEntryPoint("General Settings", 1, false, "BG_GENERAL_SETTINGS", "BG_GENERAL_SETTINGS", "Maintenance", 1, false)]
    public partial class GeneralSettings : DevExpress.XtraEditors.XtraForm
    {
        bool bFirstTime;
        private GExecuteScalar es = new GExecuteScalar();
        private GDataTable gdt = new GDataTable();
        DataTable dtSettings = new DataTable();
        protected DBSetting dbSetting;
        private string sLanguage;
        private AssemblyInfoEntity info;
        public GeneralSettings(DBSetting dbs)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Location = new Point((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                          (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
            //this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            info = new AssemblyInfoEntity();
            bFirstTime = true;
            dbSetting = dbs;
            gdt.LoadDataTable(dtSettings, "Settings", "SELECT * FROM RPA_Settings");
            sLanguage = "en-US";
            if (BCE.Application.ThreadForm.UILanguage.ToString() == "Indonesian")
            {
                sLanguage = "id";
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("id");
            }
            InitializeComponent();
            txtEditDocNoFormat.EditValue = dtSettings.Rows[0]["Format"].ToString();
            txtDocNoFormatRM.EditValue = dtSettings.Rows[0]["FormatRM"].ToString();
            txtDocNoFormatRV1.EditValue = dtSettings.Rows[0]["FormatRV"].ToString();
            txtDocNoFormatRR.EditValue = dtSettings.Rows[0]["FormatRCVRM"].ToString();
            luAccRound.EditValue= dtSettings.Rows[0]["RoundAccNo"].ToString();
            txtQtyDecimal.EditValue = dtSettings.Rows[0]["QtyDecimal"].ToString();
            txtRateDecimal.EditValue = dtSettings.Rows[0]["RateDecimal"].ToString();
            ceMachine.Checked= BCE.Data.Convert.TextToBoolean(dtSettings.Rows[0]["IsMachineNo"]);
            ceIsJournal.Checked = BCE.Data.Convert.TextToBoolean(dtSettings.Rows[0]["IsAutoJournal"]);
            BCE.AutoCount.XtraUtils.DefaultLookupEditBuilder.GetOrCreate(dbSetting).AllAccountLookupEditBuilder.BuildLookupEdit(luAccRound.Properties, dbSetting);
            object obj = dbSetting.ExecuteScalar("select NextNumber from docnoformat where DocType='SW' AND Name='RPA_WO'");
            if (obj != DBNull.Value && obj != null)
            {
                txtNextNumber.EditValue = BCE.Data.Convert.ToInt16(obj);
            }
            obj = dbSetting.ExecuteScalar("select nextnumber from docnoformat where DocType='RM' AND Name='RPA_RM'");
            if (obj != DBNull.Value && obj != null)
            {
                txtNextNoRM.EditValue = BCE.Data.Convert.ToInt16(obj);
            }
            obj = dbSetting.ExecuteScalar("select nextnumber from docnoformat where DocType='RV' AND Name='RPA_RCV'");
            if (obj != DBNull.Value && obj != null)
            {
                txtNextNoRV1.EditValue = BCE.Data.Convert.ToInt16(obj);
            }
            obj = dbSetting.ExecuteScalar("select nextnumber from docnoformat where DocType='RR' AND Name='RPA_RCVRM'");
            if (obj != DBNull.Value && obj != null)
            {
                txtNextNoRR.EditValue = BCE.Data.Convert.ToInt16(obj);
            }
            bool bCanEditIsJournal = true;
            obj = dbSetting.ExecuteScalar("select count(*) from vRPA_TransWOtoRCV");
            if (obj != DBNull.Value && obj != null)
            {
                if (BCE.Data.Convert.ToDecimal(obj) > 0)
                    bCanEditIsJournal = false;
            }
            string sMacAddr = "";
            string sCompanyName = "";
           
            obj = dbSetting.ExecuteScalar("USE MASTER select srvname from sys.sysservers");
            if (obj != null && obj != DBNull.Value)
            {
                sMacAddr = obj.ToString();

            }
            obj = dbSetting.ExecuteScalar("select CompanyName from Profile");
            if (obj != null && obj != DBNull.Value)
            {

                sCompanyName = obj.ToString();
            }
            ceIsJournal.Enabled = bCanEditIsJournal;
            if (Production.GLib.G.IsRegistered("RPA_Settings", sMacAddr+sCompanyName + info.Product, sMacAddr))
                btnActivate.Enabled = false;
            else
                btnActivate.Enabled = true;

            if (Production.GLib.G.IsRegistered("RPA_Settings", sMacAddr+ sCompanyName + info.Product, sMacAddr))
            {
                btnTry.Enabled = false;
                btnActivate.Enabled = false;
                lblExpiry.Text = "";
            }
            else
            {
                btnTry.Enabled = true;
                btnActivate.Enabled = true;
                // Trial
                decimal d = System.Convert.ToDecimal(dtSettings.Rows[0]["rkd"]);
                string sExpiry = "This plug-ins will be expired on " + GLib.G.DecryptDateTime(d).ToLongDateString();
                lblExpiry.Text = sExpiry;

            }

        }

      
       
        private void GeneralSettings_Activated(object sender, EventArgs e)
        {
            if (bFirstTime)
            {
                bFirstTime = false;
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
           // dbSetting.ExecuteNonQuery("UPDATE docnoformat SET NextNumber=? where DocType='BG' AND Name='BG_DO'", (object)BCE.Data.Convert.ToInt16(txtNextNumber.EditValue));
            

            es.Query("UPDATE RPA_Settings SET RateDecimal="+txtRateDecimal.EditValue+ ",QtyDecimal=" + txtQtyDecimal.EditValue + ",IsAutoJournal='" + BCE.Data.Convert.BooleanToText(ceIsJournal.Checked) + "',IsMachineNo='" + BCE.Data.Convert.BooleanToText(ceMachine.Checked)+"',Format='" +txtEditDocNoFormat.EditValue.ToString() + "'," +
                "NextNumber=" + txtNextNumber.EditValue.ToString()+ ", FormatRM='" +txtDocNoFormatRM.EditValue.ToString() + "'," +
                "NextNumberRM=" + txtNextNoRM.EditValue.ToString() + ", " +
                "FormatRV = '" +txtDocNoFormatRV1.EditValue.ToString() + "', " +
                "NextNumberRV=" + txtNextNoRV1.EditValue.ToString() + ", " +             
                  "FormatRCVRM = '" + txtDocNoFormatRR.EditValue.ToString() + "', " +
                "NextNumberRCVRM=" + txtNextNoRR.EditValue.ToString() + ", " +
            "RoundAccNo='" +luAccRound.EditValue.ToString()+"'") ;
            // BCE.AutoCount.GeneralMaint.DocumentNoFormat.DocumentNoMaintenanceSQL DocNoFormat = BCE.AutoCount.GeneralMaint.DocumentNoFormat.DocumentNoMaintenanceSQL.CreateDocumentNoMaint(dbSetting);
            // BCE.AutoCount.GeneralMaint.DocumentNoFormat.DialogDocumentNoMaintenance form =new BCE.AutoCount.GeneralMaint.DocumentNoFormat.DialogDocumentNoMaintenance(dbSetting);
            // form.Show();
            //DataTable myDocNoFormatTable = DocNoFormat.LoadDocNoFormatList("BG");
            //myDocNoFormatTable.Rows[0]["Format"]=txtEditDocNoFormat.EditValue;
            //myDocNoFormatTable.Rows[0]["NextNumber"] =txtNextNumber.EditValue;
            //DocNoFormat.Save(myDocNoFormatTable);

            es.Query("UPDATE DocNoFormat SET Format='" + txtEditDocNoFormat.EditValue.ToString() + "'," +
               "NextNumber=" + txtNextNumber.EditValue.ToString() + " where Name='RPA_WO' and DocType='SW'");

            es.Query("UPDATE DocNoFormat SET Format='" + txtDocNoFormatRM.EditValue.ToString() + "'," +
                 "NextNumber=" + txtNextNoRM.EditValue.ToString() + " where Name='RPA_RM' and DocType='RM'");

            es.Query("UPDATE DocNoFormat SET Format='" + txtDocNoFormatRV1.EditValue.ToString() + "'," +
                 "NextNumber=" + txtNextNoRV1.EditValue.ToString() + " where Name='RPA_RCV' and DocType='RV'");
            es.Query("UPDATE DocNoFormat SET Format='" + txtDocNoFormatRR.EditValue.ToString() + "'," +
                 "NextNumber=" + txtNextNoRV1.EditValue.ToString() + " where Name='RPA_RCVRM' and DocType='RR'");
          
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            Register reg = new Register(dbSetting);
            reg.ShowDialog();
            if (reg.IsRegistered)
            {
                btnActivate.Enabled = false;
                btnTry.Enabled = false;
                lblExpiry.Text = "";
            }
        }

        private void btnTry_Click(object sender, EventArgs e)
        {
            openFileDialog1.CheckFileExists = false;
            openFileDialog1.DefaultExt = "GDF";
            openFileDialog1.Title = "Load Registration File";
            openFileDialog1.FileName = "PlugInsReg.GDF";

            openFileDialog1.Filter = "Registration file (*.GDF)|*.GDF|All files (*.*)|*.*";

            string s;
            decimal d;
            DateTime dt;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFileDialog1.FileName.ToString());
                sr.ReadLine(); sr.ReadLine();
                s = sr.ReadLine();
                d = System.Convert.ToDecimal(s);
                dt = GLib.G.DecryptDateTime(d);
                GExecuteScalar es = new GExecuteScalar();
                es.Query("UPDATE RPA_Settings SET rkd=" + d.ToString());
                lblExpiry.Text = "This plug-ins will be expired on " + GLib.G.DecryptDateTime(d).ToLongDateString();

                System.Windows.Forms.MessageBox.Show("This plug-ins will be expired on " + dt.ToLongDateString(), "Register Trial", MessageBoxButtons.OK);
            }
        }

        private void GeneralSettings_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " +info.AssemblyVersion;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void xtraTabControl1_Click(object sender, EventArgs e)
        {

        }
    }
}
