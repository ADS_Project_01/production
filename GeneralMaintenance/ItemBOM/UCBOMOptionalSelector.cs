﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.UCItemBOMSelector
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using BCE.Localization;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace Production.GeneralMaintenance.ItemBOM
{
    public class UCItemBOMSelector : XtraUserControl
    {
        private DBSetting myDBSetting;
        private BCE.AutoCount.SearchFilter.Filter myFilter;
        private ItemBOMLookupEditBuilder myLookupEditBuilder;
        private bool myInApplyFilter;
        private IContainer components;
        private ComboBoxEdit comboBoxEditOption;
        private LookUpEdit lookUpEditFrom;
        private LookUpEdit lookUpEditTo;
        private Label label1;
        private Label label2;
        private LinkLabel lblMultiSelect;
        private Panel panel1;

        public BCE.AutoCount.SearchFilter.Filter Filter
        {
            get
            {
                return this.myFilter;
            }
        }

        public UCItemBOMSelector()
        {
            this.InitializeComponent();
            this.panel1.Visible = false;
            this.lblMultiSelect.Visible = false;
        }

        public void Initialize(DBSetting dbSetting, BCE.AutoCount.SearchFilter.Filter filter)
        {

            this.myDBSetting = dbSetting;
            this.myLookupEditBuilder = new ItemBOMLookupEditBuilder();
            this.myLookupEditBuilder.BuildLookupEdit(this.lookUpEditFrom.Properties, this.myDBSetting);
            this.myLookupEditBuilder.BuildLookupEdit(this.lookUpEditTo.Properties, this.myDBSetting);
            this.ApplyFilter(filter);
        }

        public void RefreshLookupEditBuilder()
        {
            if (this.myLookupEditBuilder != null)
            {
                ItemBOMLookupEditBuilder lookupEditBuilder = this.myLookupEditBuilder;
                RepositoryItemLookUpEdit[] lookupEdits = new RepositoryItemLookUpEdit[2];
                int index1 = 0;
                RepositoryItemLookUpEdit properties1 = this.lookUpEditFrom.Properties;
                lookupEdits[index1] = properties1;
                int index2 = 1;
                RepositoryItemLookUpEdit properties2 = this.lookUpEditTo.Properties;
                lookupEdits[index2] = properties2;
                lookupEditBuilder.Refresh(lookupEdits);
            }
        }

        public void ApplyFilter(BCE.AutoCount.SearchFilter.Filter filter)
        {
            if (filter != null)
            {
                this.myInApplyFilter = true;
                try
                {
                    this.myFilter = filter;
                    this.comboBoxEditOption.SelectedIndex = this.myFilter.Type != FilterType.None ? (this.myFilter.Type != FilterType.ByRange ? 2 : 1) : 0;
                    this.lookUpEditFrom.EditValue = this.myFilter.From;
                    this.lookUpEditTo.EditValue = this.myFilter.To;
                    this.lblMultiSelect.Text = this.myFilter.ToString();
                    this.SelectionChanged();
                }
                finally
                {
                    this.myInApplyFilter = false;
                }
            }
        }

        public void ApplyFilter()
        {
            if (this.myFilter != null)
                this.ApplyFilter(this.myFilter);
        }

        public void Reset()
        {
            this.lookUpEditFrom.EditValue = (object)null;
            this.lookUpEditTo.EditValue = (object)null;
            this.comboBoxEditOption.SelectedIndex = 0;
            this.SelectionChanged();
            this.myFilter.Reset();
        }

        private void LaunchMultiSelect()
        {
            using (FormMultiSelectItemBOM selectBomOptional = new FormMultiSelectItemBOM(this.myDBSetting, this.myFilter))
            {
                int num = (int)selectBomOptional.ShowDialog();
            }
            this.myFilter.Type = FilterType.ByIndividual;
            this.lblMultiSelect.Text = this.myFilter.ToString();
        }

        private void SelectionChanged()
        {
            if (this.myFilter == null)
            {
                throw new NullReferenceException(BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_InitializeMethodNotCalled, new object[0]));
            }
            else
            {
                this.lookUpEditFrom.Enabled = this.comboBoxEditOption.SelectedIndex == 1;
                this.lookUpEditTo.Enabled = this.comboBoxEditOption.SelectedIndex == 1;
                this.panel1.Visible = this.comboBoxEditOption.SelectedIndex == 1;
                this.lblMultiSelect.Visible = this.comboBoxEditOption.SelectedIndex == 2;
                if (this.comboBoxEditOption.SelectedIndex == 2)
                {
                    if (!this.myInApplyFilter)
                        this.LaunchMultiSelect();
                }
                else if (this.comboBoxEditOption.SelectedIndex == 0)
                {
                    this.myFilter.Type = FilterType.None;
                }
                else
                {
                    this.myFilter.Type = FilterType.ByRange;
                    if (!this.myInApplyFilter)
                        this.lookUpEditFrom.Focus();
                }
            }
        }

        private void lookUpEditFrom_EditValueChanged(object sender, EventArgs e)
        {
            this.myFilter.From = this.lookUpEditFrom.EditValue;
            if (this.lookUpEditTo.EditValue == null || string.Compare(this.lookUpEditTo.Text, this.lookUpEditFrom.Text, true, CultureInfo.InvariantCulture) < 0)
                this.lookUpEditTo.Text = this.lookUpEditFrom.Text;
        }

        private void lookUpEditTo_EditValueChanged(object sender, EventArgs e)
        {
            this.myFilter.To = this.lookUpEditTo.EditValue;
            if (this.lookUpEditFrom.EditValue == null || string.Compare(this.lookUpEditTo.Text, this.lookUpEditFrom.Text, true, CultureInfo.InvariantCulture) < 0)
                this.lookUpEditFrom.Text = this.lookUpEditTo.Text;
        }

        private void comboBoxEditOption_EditValueChanged(object sender, EventArgs e)
        {
            this.SelectionChanged();
        }

        private void lblMultiSelect_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.LaunchMultiSelect();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lookUpEditFrom = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditTo = new DevExpress.XtraEditors.LookUpEdit();
            this.comboBoxEditOption = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblMultiSelect = new System.Windows.Forms.LinkLabel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditOption.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lookUpEditFrom);
            this.panel1.Controls.Add(this.lookUpEditTo);
            this.panel1.Location = new System.Drawing.Point(127, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(322, 27);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(152, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "to";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "from";
            // 
            // lookUpEditFrom
            // 
            this.lookUpEditFrom.Location = new System.Drawing.Point(37, 3);
            this.lookUpEditFrom.Name = "lookUpEditFrom";
            this.lookUpEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditFrom.Size = new System.Drawing.Size(106, 20);
            this.lookUpEditFrom.TabIndex = 2;
            this.lookUpEditFrom.EditValueChanged += new System.EventHandler(this.lookUpEditFrom_EditValueChanged);
            // 
            // lookUpEditTo
            // 
            this.lookUpEditTo.Location = new System.Drawing.Point(177, 3);
            this.lookUpEditTo.Name = "lookUpEditTo";
            this.lookUpEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditTo.Size = new System.Drawing.Size(118, 20);
            this.lookUpEditTo.TabIndex = 3;
            this.lookUpEditTo.EditValueChanged += new System.EventHandler(this.lookUpEditTo_EditValueChanged);
            // 
            // comboBoxEditOption
            // 
            this.comboBoxEditOption.Location = new System.Drawing.Point(3, 3);
            this.comboBoxEditOption.Name = "comboBoxEditOption";
            this.comboBoxEditOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditOption.Properties.Items.AddRange(new object[] {
            "No filter",
            "Filter by range",
            "Filter by multi-select"});
            this.comboBoxEditOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditOption.Size = new System.Drawing.Size(118, 20);
            this.comboBoxEditOption.TabIndex = 0;
            this.comboBoxEditOption.EditValueChanged += new System.EventHandler(this.comboBoxEditOption_EditValueChanged);
            // 
            // lblMultiSelect
            // 
            this.lblMultiSelect.Location = new System.Drawing.Point(133, 4);
            this.lblMultiSelect.Name = "lblMultiSelect";
            this.lblMultiSelect.Size = new System.Drawing.Size(100, 19);
            this.lblMultiSelect.TabIndex = 2;
            this.lblMultiSelect.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblMultiSelect_LinkClicked);
            // 
            // UCItemBOMSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.comboBoxEditOption);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblMultiSelect);
            this.Name = "UCItemBOMSelector";
            this.Size = new System.Drawing.Size(449, 26);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditOption.Properties)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
