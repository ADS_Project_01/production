﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.UCItemBOMGrid
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.XtraUtils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
namespace Production.GeneralMaintenance.ItemBOM
{
    public class UCItemBOMGrid : XtraUserControl
    {
        private EventHandler ReloadAllColumnsEvent;
        private CustomizeGridLayout myGridLayout;
        private IContainer components;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private RepositoryItemComboBox repositoryItemComboBox1;
        private GridColumn colItemBOMCode;
        private GridColumn colDesc2;
        private GridColumn colDescription;
        private GridColumn colIsActive;
        private GridColumn colTick;
        private GridControl myGridControl;
        private BindingSource rPAItemBOMBindingSource;
        private AED_NEW_BAFDataSet aED_NEW_BAFDataSet;
        private AED_NEW_BAFDataSetTableAdapters.RPA_ItemBOMTableAdapter rPA_ItemBOMTableAdapter;
        private GridColumn colDocKey;
        private GridColumn colBOMCode;
        private GridColumn colItemCode;
        private GridColumn colUOM;
        private GridColumn colDescription1;
        private GridColumn colDesc21;
        private GridColumn colIsActive1;
        private GridColumn colLastUpdate;
        private GridColumn colQty;
        private GridColumn colCost;
        private GridColumn colCostType;
        private GridColumn colLastModified;
        private GridColumn colLastModifiedUserID;
        private GridColumn colCreatedTimeStamp;
        private GridColumn colCreatedUserID;
        private GridColumn colProjNo;
        private GridView myGridView;

        [Description("The DataSource of the GridControl")]
        [Category("User Control")]
        [DefaultValue(null)]
        public object DataSource
        {
            get
            {
                return this.myGridControl.DataSource;
            }
            set
            {
                this.myGridControl.DataSource = value;
            }
        }

        [Description("The Main GridView")]
        [Category("User Control")]
        public GridView GridView
        {
            get
            {
                return this.myGridView;
            }
        }

        [Description("Specify the FieldName of the Tick Column, this property will be used if the ShowTickColumn is True.")]
        [Category("User Control")]
        [DefaultValue("Tick")]
        public string TickFieldName
        {
            get
            {
                return this.colTick.FieldName;
            }
            set
            {
                this.colTick.FieldName = value;
            }
        }

        [Description("Specify whether the Tick Column will be show or hide, default is hide.")]
        [Category("User Control")]
        [DefaultValue(false)]
        public bool ShowTickColumn
        {
            get
            {
                return this.colTick.Visible;
            }
            set
            {
                this.colTick.Visible = value;
                if (value)
                    this.colTick.VisibleIndex = 0;
            }
        }

        [Description("Specify Column Auto Width")]
        [Category("User Control")]
        [DefaultValue(false)]
        public bool ColumnAutoWidth
        {
            get
            {
                return this.myGridView.OptionsView.ColumnAutoWidth;
            }
            set
            {
                this.myGridView.OptionsView.ColumnAutoWidth = value;
            }
        }

        [Description("The CustomizeGridLayout object of the main GridView.")]
        [Category("User Control")]
        [DefaultValue(null)]
        [Browsable(false)]
        public CustomizeGridLayout GridLayout
        {
            get
            {
                return this.myGridLayout;
            }
        }

        [Description("Occurs whenever there is a new column appears on the grid.")]
        [Category("User Control")]
        public event EventHandler ReloadAllColumnsEvent2
        {
            add
            {
                EventHandler eventHandler = this.ReloadAllColumnsEvent;
                EventHandler comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.ReloadAllColumnsEvent, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler eventHandler = this.ReloadAllColumnsEvent;
                EventHandler comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.ReloadAllColumnsEvent, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        public UCItemBOMGrid()
        {
            this.InitializeComponent();
        }

        private void ReloadAllColumns(object sender, EventArgs e)
        {
            // ISSUE: reference to a compiler-generated field
            if (this.ReloadAllColumnsEvent != null)
            {
                // ISSUE: reference to a compiler-generated field
                this.ReloadAllColumnsEvent(sender, e);
            }
        }

        public void Initialize(DBSetting dbSetting)
        {
            this.myGridControl.ForceInitialize();
            FormControlUtil formControlUtil = new FormControlUtil(BCE.AutoCount.Application.DBSetting);
            formControlUtil.AddField("Qty", "Quantity");
            formControlUtil.AddField("Cost", "Currency");
            formControlUtil.AddField("LastModified", "DateTime");
            formControlUtil.AddField("CreatedTimeStamp", "DateTime");
            formControlUtil.AddField("CreatedTimeStamp", "DateTime");
            // formControlUtil.AddField("Transferable","Boolean");
            // formControlUtil.AddField("To", "Boolean");
            // formControlUtil.AddField("Transferable", "Boolean");
            formControlUtil.InitControls((Control)this);
            if (this.ParentForm != null)
                this.myGridLayout = new CustomizeGridLayout(dbSetting, this.ParentForm.Name, this.myGridView, new EventHandler(this.ReloadAllColumns));
            //new UDFUtil(dbSetting).SetupReadOnlyGrid(this.myGridView, "ItemBOM", false);
            this.colTick.OptionsColumn.ReadOnly = false;
            this.colTick.OptionsColumn.AllowEdit = true;
        }

        public DataRow GetDataRow()
        {
            return this.myGridView.GetDataRow(this.myGridView.FocusedRowHandle);
        }

        public void EndEdit()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.myGridView);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.myGridControl = new DevExpress.XtraGrid.GridControl();
            this.myGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTick = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDocKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOMCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesc21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rPAItemBOMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aED_NEW_BAFDataSet = new Production.AED_NEW_BAFDataSet();
            this.colItemBOMCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesc2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.rPA_ItemBOMTableAdapter = new Production.AED_NEW_BAFDataSetTableAdapters.RPA_ItemBOMTableAdapter();
            this.colProjNo = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.myGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rPAItemBOMBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aED_NEW_BAFDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // myGridControl
            // 
            this.myGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.myGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.myGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.myGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.myGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.myGridControl.Location = new System.Drawing.Point(0, 0);
            this.myGridControl.MainView = this.myGridView;
            this.myGridControl.Name = "myGridControl";
            this.myGridControl.Size = new System.Drawing.Size(855, 342);
            this.myGridControl.TabIndex = 0;
            this.myGridControl.UseEmbeddedNavigator = true;
            this.myGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.myGridView});
            // 
            // myGridView
            // 
            this.myGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTick,
            this.colDocKey,
            this.colBOMCode,
            this.colItemCode,
            this.colUOM,
            this.colDescription1,
            this.colDesc21,
            this.colIsActive1,
            this.colLastUpdate,
            this.colQty,
            this.colCost,
            this.colCostType,
            this.colLastModified,
            this.colLastModifiedUserID,
            this.colCreatedTimeStamp,
            this.colCreatedUserID,
            this.colProjNo});
            this.myGridView.GridControl = this.myGridControl;
            this.myGridView.Name = "myGridView";
            // 
            // colTick
            // 
            this.colTick.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTick.FieldName = "Tick";
            this.colTick.Name = "colTick";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.ValueChecked = "T";
            this.repositoryItemCheckEdit1.ValueUnchecked = "F";
            // 
            // colDocKey
            // 
            this.colDocKey.FieldName = "DocKey";
            this.colDocKey.Name = "colDocKey";
            this.colDocKey.OptionsColumn.AllowEdit = false;
            this.colDocKey.OptionsColumn.ShowInCustomizationForm = false;
            this.colDocKey.Width = 56;
            // 
            // colBOMCode
            // 
            this.colBOMCode.FieldName = "BOMCode";
            this.colBOMCode.Name = "colBOMCode";
            this.colBOMCode.OptionsColumn.AllowEdit = false;
            this.colBOMCode.Visible = true;
            this.colBOMCode.VisibleIndex = 0;
            this.colBOMCode.Width = 83;
            // 
            // colItemCode
            // 
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 2;
            this.colItemCode.Width = 79;
            // 
            // colUOM
            // 
            this.colUOM.FieldName = "UOM";
            this.colUOM.Name = "colUOM";
            this.colUOM.OptionsColumn.AllowEdit = false;
            this.colUOM.Visible = true;
            this.colUOM.VisibleIndex = 5;
            this.colUOM.Width = 39;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Item Description";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 3;
            this.colDescription1.Width = 150;
            // 
            // colDesc21
            // 
            this.colDesc21.Caption = "Description";
            this.colDesc21.FieldName = "Desc2";
            this.colDesc21.Name = "colDesc21";
            this.colDesc21.OptionsColumn.AllowEdit = false;
            this.colDesc21.Visible = true;
            this.colDesc21.VisibleIndex = 1;
            this.colDesc21.Width = 106;
            // 
            // colIsActive1
            // 
            this.colIsActive1.FieldName = "IsActive";
            this.colIsActive1.Name = "colIsActive1";
            this.colIsActive1.OptionsColumn.AllowEdit = false;
            this.colIsActive1.Visible = true;
            this.colIsActive1.VisibleIndex = 4;
            this.colIsActive1.Width = 72;
            // 
            // colLastUpdate
            // 
            this.colLastUpdate.FieldName = "LastUpdate";
            this.colLastUpdate.Name = "colLastUpdate";
            this.colLastUpdate.OptionsColumn.AllowEdit = false;
            // 
            // colQty
            // 
            this.colQty.FieldName = "Qty";
            this.colQty.Name = "colQty";
            this.colQty.OptionsColumn.AllowEdit = false;
            this.colQty.Visible = true;
            this.colQty.VisibleIndex = 6;
            this.colQty.Width = 40;
            // 
            // colCost
            // 
            this.colCost.FieldName = "Cost";
            this.colCost.Name = "colCost";
            this.colCost.OptionsColumn.AllowEdit = false;
            this.colCost.Visible = true;
            this.colCost.VisibleIndex = 8;
            this.colCost.Width = 98;
            // 
            // colCostType
            // 
            this.colCostType.FieldName = "CostType";
            this.colCostType.Name = "colCostType";
            this.colCostType.OptionsColumn.AllowEdit = false;
            this.colCostType.Visible = true;
            this.colCostType.VisibleIndex = 7;
            this.colCostType.Width = 73;
            // 
            // colLastModified
            // 
            this.colLastModified.FieldName = "LastModified";
            this.colLastModified.Name = "colLastModified";
            this.colLastModified.OptionsColumn.AllowEdit = false;
            this.colLastModified.Visible = true;
            this.colLastModified.VisibleIndex = 10;
            this.colLastModified.Width = 67;
            // 
            // colLastModifiedUserID
            // 
            this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
            this.colLastModifiedUserID.Name = "colLastModifiedUserID";
            this.colLastModifiedUserID.OptionsColumn.AllowEdit = false;
            this.colLastModifiedUserID.Visible = true;
            this.colLastModifiedUserID.VisibleIndex = 11;
            this.colLastModifiedUserID.Width = 67;
            // 
            // colCreatedTimeStamp
            // 
            this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
            this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
            this.colCreatedTimeStamp.OptionsColumn.AllowEdit = false;
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.Name = "colCreatedUserID";
            this.colCreatedUserID.OptionsColumn.AllowEdit = false;
            this.colCreatedUserID.Visible = true;
            this.colCreatedUserID.VisibleIndex = 12;
            this.colCreatedUserID.Width = 121;
            // 
            // rPAItemBOMBindingSource
            // 
            this.rPAItemBOMBindingSource.DataMember = "RPA_ItemBOM";
            this.rPAItemBOMBindingSource.DataSource = this.aED_NEW_BAFDataSet;
            // 
            // aED_NEW_BAFDataSet
            // 
            this.aED_NEW_BAFDataSet.DataSetName = "AED_NEW_BAFDataSet";
            this.aED_NEW_BAFDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colItemBOMCode
            // 
            this.colItemBOMCode.FieldName = "ItemBOMCode";
            this.colItemBOMCode.Name = "colItemBOMCode";
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            // 
            // colDesc2
            // 
            this.colDesc2.FieldName = "Desc2";
            this.colDesc2.Name = "colDesc2";
            // 
            // colIsActive
            // 
            this.colIsActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsActive.FieldName = "IsActive";
            this.colIsActive.Name = "colIsActive";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // rPA_ItemBOMTableAdapter
            // 
            this.rPA_ItemBOMTableAdapter.ClearBeforeFill = true;
            // 
            // colProjNo
            // 
            this.colProjNo.Caption = "ProjNo";
            this.colProjNo.FieldName = "ProjNo";
            this.colProjNo.Name = "colProjNo";
            this.colProjNo.Visible = true;
            this.colProjNo.VisibleIndex = 9;
            this.colProjNo.Width = 83;
            // 
            // UCItemBOMGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.myGridControl);
            this.Name = "UCItemBOMGrid";
            this.Size = new System.Drawing.Size(855, 342);
            ((System.ComponentModel.ISupportInitialize)(this.myGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rPAItemBOMBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aED_NEW_BAFDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
