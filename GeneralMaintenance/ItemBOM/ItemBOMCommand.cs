﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.ItemBOMCommand
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.Data;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Production.GeneralMaintenance.ItemBOM
{
    public class ItemBOMCommand
    {
        protected Form myFormOwner;
        protected DataTable myDtblBrowse;
        protected internal DBSetting myDBSetting;
        protected internal ItemBOMCommand.Action myAction;
        protected internal GeneralSetting myGeneralSetting;
        private bool myIsReportBuilder;
        private BasicReportOption myReportOption;
        public const string BOMOPTIONALSQL = "SELECT * FROM RPA_ItemBOM";
        //public const string DETAILALLSQL = "SELECT d.*, i.Description, i.Desc2 FROM RPA_ItemBOMDtl d LEFT OUTER JOIN Item i ON i.ItemCode=d.SubItemCode";
        //public const string LINKDETAILALLSQL = "SELECT l.*, i.Description, i.Desc2 FROM RPA_ItemBOMLink l LEFT OUTER JOIN Item i ON i.ItemCode=l.BOMItemCode";
        protected internal StockHelper myHelper;

        public DataTable DtblItemBOM
        {
            get
            {
                return this.myDtblBrowse;
            }
        }

        public Form FormOwner
        {
            get
            {
                return this.myFormOwner;
            }
            set
            {
                this.myFormOwner = value;
            }
        }

        public bool IsReportBuilder
        {
            set
            {
                this.myIsReportBuilder = value;
            }
        }

        public BasicReportOption ReportOption
        {
            get
            {
                return this.myReportOption;
            }
        }

        internal ItemBOMCommand()
        {
            this.myDtblBrowse = new DataTable();
            this.myFormOwner = (Form)null;
            this.myReportOption = (BasicReportOption)PersistenceUtil.LoadUserSetting("RPA_ItemBOMReportOption.setting");
            if (this.myReportOption == null)
                this.myReportOption = new BasicReportOption();
        }

        public static ItemBOMCommand Create(DBSetting dbSetting)
        {
            ItemBOMCommand bomOptionalCommand = (ItemBOMCommand)null;
            if (dbSetting.ServerType == DBServerType.SQL2000)
                bomOptionalCommand = (ItemBOMCommand)new ItemBOMCommandSQL();
            else
                dbSetting.ThrowServerTypeNotSupportedException();
            bomOptionalCommand.myDBSetting = dbSetting;
            bomOptionalCommand.myGeneralSetting = GeneralSetting.GetOrCreate(dbSetting);
            bomOptionalCommand.myHelper = StockHelper.Create(dbSetting);
            return bomOptionalCommand;
        }

        public void SaveReportOption()
        {
            PersistenceUtil.SaveUserSetting((object)this.myReportOption, "RPA_ItemBOMReportOption.setting");
        }

        public ItemBOMEntity NewItemBOM()
        {
            this.myAction = ItemBOMCommand.Action.New;
            DataSet ds = this.LoadData(-1L);
            DataTable dtblMaster = ds.Tables["Master"];
            this.InitMasterNewRow(ref dtblMaster);
            return new ItemBOMEntity(this, ds);
        }

        public ItemBOMEntity ViewEditItemBOM(long docKey, ItemBOMCommand.Action action)
        {
            this.myAction = action;
            ItemBOMEntity bomOptionalEntity = (ItemBOMEntity)null;
            DataSet ds = this.LoadData(docKey);
            if (ds.Tables["Master"].Rows.Count != 0)
                bomOptionalEntity = new ItemBOMEntity(this, ds);
            return bomOptionalEntity;
        }

        public string ReportBuilderString()
        {
            if (this.myIsReportBuilder)
                return "TOP 10 ";
            else
                return "";
        }

        public DataTable GetPrintPreviewDoc(long docKey)
        {
            string strSQL = string.Format("SELECT * FROM RPA_ItemBOM", (object)"'F' as Tick, t.Name AS TechnicianName,", (object)"");
            return this.LoadData(docKey).Tables["Master"];
        }

        private void InitMasterNewRow(ref DataTable dtblMaster)
        {
            DataRow row = dtblMaster.NewRow();
            DBRegistry dbRegistry = DBRegistry.Create(this.myDBSetting);
            row["DocKey"] = (object)dbRegistry.IncOne((IRegistryID)new GlobalUniqueKey());
            row["BOMCode"] = (object)string.Empty;
            row["ItemCode"] = (object)string.Empty;
            row["UOM"] = (object)string.Empty;
            row["Qty"] = (object)1;
            row["Cost"] = (object)0;
            row["CostType"] = (object)CostTypeOptions.Standard;

            row["LastModified"] = (object)System.DateTime.Now;
            row["CreatedUserID"] = (object)BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID;
            row["LastModifiedUserID"] = (object)BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID;
            row["CreatedTimeStamp"] = (object)System.DateTime.Now;
            row["IsActive"] = (object)"T";
            row["LastUpdate"] = (object)-1;
            dtblMaster.Rows.Add(row);
        }

        public virtual void BrowseAll(string sql)
        {
        }

        public virtual DataSet LoadData(long docKey)
        {
            return (DataSet)null;
        }

        public virtual void SaveData(ItemBOMEntity entity)
        {
        }

        public virtual void DeleteData(long[] docKey)
        {
        }

        public virtual DataSet BrowseSearchData(SqlCommand cmd, SearchCriteria criteria, string strSQL, string strWhereSQL)
        {
            return (DataSet)null;
        }

        public virtual DataRow GetItemDtl(object itemCode, object uom)
        {
            return (DataRow)null;
        }

        public virtual DataTable GetBSDetailData(DataTable masterTable)
        {
            return (DataTable)null;
        }

        public virtual DataTable GetRepDetailData(DataTable masterTable)
        {
            return (DataTable)null;
        }

        public virtual DataTable GetAPDetailData(DataTable masterTable)
        {
            return (DataTable)null;
        }

        public virtual DataTable GetOvdDetailData(DataTable masterTable)
        {
            return (DataTable)null;
        }
        public virtual DataTable GetDetailData(DataTable masterTable)
        {
            return (DataTable)null;
        }

        public virtual DataTable GetServiceHistory(long contractKey)
        {
            return (DataTable)null;
        }

        public enum Action
        {
            New,
            Edit,
            View,
            Delete,
        }

        protected delegate void OnUpdateBrowseDelegate(ItemBOMEntity entity);

        protected delegate void OnDeleteBrowseDelegate(long[] docKeys);
    }
}
