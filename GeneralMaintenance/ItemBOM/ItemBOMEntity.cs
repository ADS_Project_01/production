﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.ItemBOMEntity
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using System;
using System.Data;
using System.IO;
using BCE.AutoCount.Settings;
namespace Production.GeneralMaintenance.ItemBOM
{
    public class ItemBOMEntity
    {
        private ItemBOMCommand myItemBOMCmd;
        private DataSet myDs;
        private DataTable myDtblMaster;
        private DataTable myDtblDetail;
        private DataTable myDtbLAPDetail;
        private DataTable myDtbLBSDetail;
        private DataTable myDtblOvdDetail;
        private DataTable myDtblRepDetail;
        private DataRow myMasterRow;
        private ItemBOMCommand.Action myAction;
        private string myFormat;
        private int myNextNumber;
        private bool myDetailTableHvChange;
        private bool myBSDetailTableHvChange;
        private bool myAPDetailTableHvChange;
        private bool myOVDDetailTableHvChange;
        private bool myRepDetailTableHvChange;
        protected DecimalSetting myDecimalSetting;
        public ItemBOMCommand ItemBOMCmd
        {
            get
            {
                return this.myItemBOMCmd;
            }
        }

        public DataTable MasterTable
        {
            get
            {
                return this.myDtblMaster;
            }
        }

        public DataTable DetailTable
        {
            get
            {
                return this.myDtblDetail;
            }
        }

        public DataTable APDetailTable
        {
            get
            {
                return this.myDtbLAPDetail;
            }
        }

        public DataTable BSDetailTable
        {
            get
            {
                return this.myDtbLBSDetail;
            }
        }
        public DataTable OvdDetailTable
        {
            get
            {
                return this.myDtblOvdDetail;
            }
        }

        public DataTable RepDetailTable
        {
            get
            {
                return this.myDtblRepDetail;
            }
        }

        public ItemBOMCommand.Action Action
        {
            get
            {
                return this.myAction;
            }
            set
            {
                this.myAction = value;
            }
        }

        public object HasChange
        {
            get
            {
                if (this.myMasterRow.RowState != DataRowState.Unchanged || this.myDetailTableHvChange || this.myBSDetailTableHvChange || this.myAPDetailTableHvChange || this.myOVDDetailTableHvChange)
                    return (object)true;
                else
                    return (object)false;
            }
        }

        public bool DetailTableHvChange
        {
            set
            {
                this.myDetailTableHvChange = value;
            }
        }

        public bool BSTableHvChange
        {
            set
            {
                this.myBSDetailTableHvChange = value;
            }
        }
        public bool APTableHvChange
        {
            set
            {
                this.myAPDetailTableHvChange = value;
            }
        }
        public bool OVDTableHvChange
        {
            set
            {
                this.myOVDDetailTableHvChange = value;
            }
        }

        public bool RepTableHvChange
        {
            set
            {
                this.myRepDetailTableHvChange = value;
            }
        }
        public object DocKey
        {
            get
            {
                return this.myMasterRow["DocKey"];
            }
        }

        public object BOMCode
        {
            get
            {
                return this.myMasterRow["BOMCode"];
            }
            set
            {
                this.myMasterRow["BOMCode"] = value;
            }
        }
        public object ItemCode
        {
            get
            {
                return this.myMasterRow["ItemCode"];
            }
            set
            {
                this.myMasterRow["ItemCode"] = value;
            }
        }
        public object UOM
        {
            get
            {
                return this.myMasterRow["UOM"];
            }
            set
            {
                this.myMasterRow["UOM"] = value;
            }
        }
        public object Description
        {
            get
            {
                return this.myMasterRow["Description"];
            }
            set
            {
                this.myMasterRow["Description"] = value;
            }
        }
        public object Desc2
        {
            get
            {
                return this.myMasterRow["Desc2"];
            }
            set
            {
                this.myMasterRow["Desc2"] = value;
            }
        }
        public DBDecimal Qty
        {
            get
            {
                return BCE.Data.Convert.ToDBDecimal(this.myMasterRow["Qty"]);
            }
            set
            {
                this.myMasterRow["Qty"] = (object)BCE.Data.Convert.ToDBDecimal(value);
            }
        }
        public DBDecimal Cost
        {
            get
            {
                return BCE.Data.Convert.ToDBDecimal(this.myMasterRow["Cost"]);
            }
            set
            {
                this.myMasterRow["Cost"] = (object)BCE.Data.Convert.ToDBDecimal(value);
            }
        }
        public object CostType
        {
            get
            {
                return this.myMasterRow["CostType"];
            }
            set
            {
                this.myMasterRow["CostType"] = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return BCE.Data.Convert.TextToBoolean(this.myMasterRow["IsActive"]);
            }
            set
            {
                this.myMasterRow["IsActive"] = (object)BCE.Data.Convert.BooleanToText(value);
            }
        }
        public string Format
        {
            get
            {
                return this.myFormat;
            }
            set
            {
                this.myFormat = value;
            }
        }

        public int NextNumber
        {
            get
            {
                return this.myNextNumber;
            }
            set
            {
                this.myNextNumber = value;
            }
        }
        public int LastUpdate
        {
            get
            {
                return BCE.Data.Convert.ToInt32(this.myMasterRow["LastUpdate"]);
            }
            set
            {
                this.myMasterRow["LastUpdate"] = (object)BCE.Data.Convert.ToInt32(value);
            }
        }
        public DataSet DataSetItemBOM
        {
            get
            {
                return this.myDs;
            }
        }
        public DBDateTime LastModified
        {
            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myMasterRow["LastModified"]);
            }
        }

        public DBString LastModifiedUserID
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myMasterRow["LastModifiedUserID"]);
            }
        }

        public DBDateTime CreatedTimeStamp
        {
            get
            {
                return BCE.Data.Convert.ToDBDateTime(this.myMasterRow["CreatedTimeStamp"]);
            }
        }

        public DBString CreatedUserID
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myMasterRow["CreatedUserID"]);
            }
        }
        internal ItemBOMEntity(ItemBOMCommand bomOptionalCmd, DataSet ds)
        {
            myDecimalSetting =DecimalSetting.GetOrCreate(bomOptionalCmd.myDBSetting);
            this.myItemBOMCmd = bomOptionalCmd;
            this.myDs = ds;
            this.myDtblMaster = ds.Tables["Master"];
            this.myDtblDetail = ds.Tables["Detail"];
            this.myDtblOvdDetail = ds.Tables["OVDDetail"];
            this.myDtbLAPDetail = ds.Tables["APDetail"];
            this.myDtbLBSDetail = ds.Tables["BSDetail"];
            this.myDtblRepDetail = ds.Tables["RepDetail"];
            this.myMasterRow = this.myDtblMaster.Rows[0];
            this.myAction = this.myItemBOMCmd.myAction;
            this.myDtblMaster.ColumnChanged += new DataColumnChangeEventHandler(this.MasterDataColumnChangeEventHandler);
            this.myDtblDetail.ColumnChanged += new DataColumnChangeEventHandler(this.DetailDataColumnChangeEventHandler);
            this.myDtblDetail.RowDeleted += new DataRowChangeEventHandler(this.DetailDataRowChangeEventHandler);
            this.myDtbLBSDetail.ColumnChanged += new DataColumnChangeEventHandler(this.BSDetailDataColumnChangeEventHandler);
            this.myDtbLBSDetail.RowDeleted += new DataRowChangeEventHandler(this.BSDetailDataRowChangeEventHandler);

            this.myDtbLAPDetail.ColumnChanged += new DataColumnChangeEventHandler(this.APDetailDataColumnChangeEventHandler);
            this.myDtbLAPDetail.RowDeleted += new DataRowChangeEventHandler(this.APDetailDataRowChangeEventHandler);

            this.myDtblOvdDetail.ColumnChanged += new DataColumnChangeEventHandler(this.OVDDetailDataColumnChangeEventHandler);
            this.myDtblOvdDetail.RowDeleted += new DataRowChangeEventHandler(this.OVDDetailDataRowChangeEventHandler);

            this.myDtblRepDetail.ColumnChanged += new DataColumnChangeEventHandler(this.RepDetailDataColumnChangeEventHandler);
            this.myDtblRepDetail.RowDeleted += new DataRowChangeEventHandler(this.RepDetailDataRowChangeEventHandler);

        }

        public void Save()
        {
            this.myItemBOMCmd.SaveData(this);
        }

        public void ClearDetail()
        {
            if (this.myDtblDetail.Rows.Count != 0)
            {
                foreach (DataRow dataRow in this.myDtblDetail.Select())
                    dataRow.Delete();
            }
        }

        public void ClearBSDetail()
        {
            if (this.myDtbLBSDetail.Rows.Count != 0)
            {
                foreach (DataRow dataRow in this.myDtbLBSDetail.Select())
                    dataRow.Delete();
            }
        }

        public void ClearOVDDetail()
        {
            if (this.myDtblOvdDetail.Rows.Count != 0)
            {
                foreach (DataRow dataRow in this.myDtblOvdDetail.Select())
                    dataRow.Delete();
            }
        }
        public void ClearAPDetail()
        {
            if (this.myDtbLAPDetail.Rows.Count != 0)
            {
                foreach (DataRow dataRow in this.myDtbLAPDetail.Select())
                    dataRow.Delete();
            }
        }

        public void ClearRepDetail()
        {
            if (this.myDtblRepDetail.Rows.Count != 0)
            {
                foreach (DataRow dataRow in this.myDtblRepDetail.Select())
                    dataRow.Delete();
            }
        }

        public DataRow[] GetValidDetailRows()
        {
            return this.myDtblDetail.Select("", "", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
        }
        public DataRow[] GetValidBSDetailRows()
        {
            return this.myDtbLBSDetail.Select("", "", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
        }
        public DataRow[] GetValidAPDetailRows()
        {
            return this.myDtbLAPDetail.Select("", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
        }
        public DataRow[] GetValidOVDDetailRows()
        {
            return this.myDtblOvdDetail.Select("", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
        }
        public DataRow[] GetValidRepDetailRows()
        {
            return this.myDtblRepDetail.Select("", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
        }
        public string ExportAsXml()
        {
            StringWriter stringWriter = new StringWriter();
            this.myDs.WriteXml((TextWriter)stringWriter, XmlWriteMode.WriteSchema);
            return stringWriter.ToString();
        }

        public static bool CanCopyMasterField(string columnName, bool createRenewCont)
        {
            string[] array = new string[2];
            int index1 = 0;
            string str1 = "DocKey";
            array[index1] = str1;
            int index2 = 1;
            string str2 = "BOMCode";
            array[index2] = str2;
            string str3 = columnName;
            if (Array.IndexOf<string>(array, str3) != -1)
                return false;
            else
                return true;
        }

        public DetailEntity AddDetail()
        {
            if (this.myAction == ItemBOMCommand.Action.View)
                throw new Exception(Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_CannotEdit, new object[0]));
            else
                return this.InternalAddDetail();
        }

        private DetailEntity InternalAddDetail()
        {
            DataRow dataRow = this.myDtblDetail.NewRow();
            DBRegistry dbRegistry = DBRegistry.Create(this.myItemBOMCmd.myDBSetting);
            dataRow["DtlKey"] = (object)dbRegistry.IncOne((IRegistryID)new GlobalUniqueKey());
            dataRow["DocKey"] = this.myMasterRow["DocKey"];
            dataRow["BOMCode"] = (object)"";
            dataRow["ItemCode"] = (object)"";
            dataRow["UOM"] = (object)"";
            dataRow["Rate"] = (object)1;
            dataRow["ProductRatio"] = (object)1;
            dataRow["Qty"] = (object)1;
            dataRow["UnitCost"] = (object)0;
            dataRow["TotalCost"] = (object)0;
            dataRow.EndEdit();
            this.myDtblDetail.Rows.Add(dataRow);
            return new DetailEntity(dataRow);
        }

        public BSDetailEntity AddBSDetail()
        {
            if (this.myAction == ItemBOMCommand.Action.View)
                throw new Exception(Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_CannotEdit, new object[0]));
            else
                return this.InternalAddBSDetail();
        }
        public APDetailEntity AddAPDetail()
        {
            if (this.myAction == ItemBOMCommand.Action.View)
                throw new Exception(Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_CannotEdit, new object[0]));
            else
                return this.InternalAddAPDetail();
        }
        public OvdDetailEntity AddOvdDetail()
        {
            if (this.myAction == ItemBOMCommand.Action.View)
                throw new Exception(Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_CannotEdit, new object[0]));
            else
                return this.InternalAddOVDDetail();
        }

        public RepDetailEntity AddRepDetail()
        {
            if (this.myAction == ItemBOMCommand.Action.View)
                throw new Exception(Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_CannotEdit, new object[0]));
            else
                return this.InternalAddRepDetail();
        }

        private BSDetailEntity InternalAddBSDetail()
        {
            DataRow dataRow = this.myDtbLBSDetail.NewRow();
            DBRegistry dbRegistry = DBRegistry.Create(this.myItemBOMCmd.myDBSetting);
            dataRow["DtlKey"] = (object)dbRegistry.IncOne((IRegistryID)new GlobalUniqueKey());
            dataRow["DocKey"] = this.myMasterRow["DocKey"];
            dataRow["ItemCode"] = (object)"";
            dataRow["UOM"] = (object)"";
            dataRow["Rate"] = (object)1;
            dataRow["CostPercent"] = (object)0;
            dataRow["CostMax"] = (object)0;
            dataRow["FOHCharge"] = (object)"F";
            dataRow.EndEdit();
            this.myDtbLBSDetail.Rows.Add(dataRow);
            return new BSDetailEntity(dataRow);
        }

        private APDetailEntity InternalAddAPDetail()
        {
            DataRow dataRow = this.myDtbLAPDetail.NewRow();
            DBRegistry dbRegistry = DBRegistry.Create(this.myItemBOMCmd.myDBSetting);
            dataRow["DtlKey"] = (object)dbRegistry.IncOne((IRegistryID)new GlobalUniqueKey());
            dataRow["DocKey"] = this.myMasterRow["DocKey"];
            dataRow["ItemCode"] = (object)"";
            dataRow["CostPercent"] = (object)0;
            dataRow.EndEdit();
            this.myDtbLAPDetail.Rows.Add(dataRow);
            return new APDetailEntity(dataRow);
        }

        private OvdDetailEntity InternalAddOVDDetail()
        {
            DataRow dataRow = this.myDtblOvdDetail.NewRow();
            DBRegistry dbRegistry = DBRegistry.Create(this.myItemBOMCmd.myDBSetting);
            dataRow["DtlKey"] = (object)dbRegistry.IncOne((IRegistryID)new GlobalUniqueKey());
            dataRow["DocKey"] = this.myMasterRow["DocKey"];
            dataRow["OverheadCode"] = (object)"";
            dataRow["Amount"] = (object)0;
            dataRow.EndEdit();
            this.myDtblOvdDetail.Rows.Add(dataRow);
            return new OvdDetailEntity(dataRow);
        }

        private RepDetailEntity InternalAddRepDetail()
        {
            DataRow dataRow = this.myDtblRepDetail.NewRow();
            DBRegistry dbRegistry = DBRegistry.Create(this.myItemBOMCmd.myDBSetting);
            dataRow["DtlKey"] = (object)dbRegistry.IncOne((IRegistryID)new GlobalUniqueKey());
            dataRow["DocKey"] = this.myMasterRow["DocKey"];
            dataRow["ItemCode"] = (object)"";
            dataRow["UOM"] = (object)"";
            dataRow["ProductRatio"] = (object)1;
            dataRow["Qty"] = (object)1;
            dataRow.EndEdit();
            this.myDtblRepDetail.Rows.Add(dataRow);
            return new RepDetailEntity(dataRow);
        }

        private void MasterDataColumnChangeEventHandler(object sender, DataColumnChangeEventArgs e)
        {
            //if (string.Compare(e.Column.ColumnName, "ItemCode", true) == 0)
            //{
            //    StockDocumentItem stockDocumentItem = this.myItemBOMCmd.myHelper.LoadStockDocumentItem(e.Row[e.Column].ToString());
            //    if (stockDocumentItem != null)
            //    {
            //        e.Row["Description"] = stockDocumentItem.Description;
            //        string sUOM = this.myItemBOMCmd.myHelper.GetBaseUOM(stockDocumentItem.ItemCode);
            //        decimal dRate = this.myItemBOMCmd.myHelper.GetItemUOMRate(stockDocumentItem.ItemCode, sUOM);
            //        e.Row["UOM"] = sUOM;

            //    }
            //}
            if (string.Compare(e.Column.ColumnName, "Qty", true) == 0)
            {
                if (myDtblDetail.Rows.Count >0)
                {
                    foreach (DataRow drDetail in GetValidDetailRows())
                    {
                        CalcProductRatio(drDetail);
                    }
                }
            }

        }
        private void DetailDataColumnChangeEventHandler(object sender, DataColumnChangeEventArgs e)
        {
            this.myDetailTableHvChange = true;
            if (string.Compare(e.Column.ColumnName, "BOMCode", true) == 0)
            {
                //object firstRow = this.myItemBOMCmd.myDBSetting.ExecuteScalar("select ItemCode from RPA_ItemBOM where BOMCode=?",(object)e.Row["BOMCode"]);
                //if(firstRow!=null && firstRow!=DBNull.Value)
                //{
                //    e.Row["ItemCode"] = firstRow;
                //    StockDocumentItem stockDocumentItem = this.myItemBOMCmd.myHelper.LoadStockDocumentItem(e.Row["ItemCode"].ToString());
                //    if (stockDocumentItem != null)
                //    {
                //        e.Row["Description"] = stockDocumentItem.Description;
                //        string sUOM = this.myItemBOMCmd.myHelper.GetBaseUOM(stockDocumentItem.ItemCode);
                //        decimal dRate = this.myItemBOMCmd.myHelper.GetItemUOMRate(stockDocumentItem.ItemCode, sUOM);
                //        e.Row["UOM"] = sUOM;
                //        e.Row["Rate"] = dRate;
                //    }
                //}

            }
                if (string.Compare(e.Column.ColumnName, "ItemCode", true) == 0)
            {
                //if(e.Row["BOMCode"]!=null)
                //{
                //    if (e.Row["BOMCode"].ToString() != "")
                //    {
                //        e.Row["ItemCode"]
                //    }
                //}
                StockDocumentItem stockDocumentItem = this.myItemBOMCmd.myHelper.LoadStockDocumentItem(e.Row[e.Column].ToString());
                if (stockDocumentItem != null)
                {
                    e.Row["Description"] = stockDocumentItem.Description;
                    string sUOM = this.myItemBOMCmd.myHelper.GetBaseUOM(stockDocumentItem.ItemCode);
                    decimal dRate = this.myItemBOMCmd.myHelper.GetItemUOMRate(stockDocumentItem.ItemCode, sUOM);
                    e.Row["UOM"] = sUOM;
                    e.Row["Rate"] = dRate;
                }
            }
            if (string.Compare(e.Column.ColumnName, "ProductRatio", true) == 0 )
            {
                CalcProductRatio(e.Row);
            }
                if (string.Compare(e.Column.ColumnName, "UnitCost", true) == 0 || string.Compare(e.Column.ColumnName, "Qty", true) == 0)
            {
                
                CalcSubTotal(e.Row);
            }
        }
        public void CalcSubTotal(DataRow row)
        {
            if (row["Qty"] == DBNull.Value && row["UnitCost"] == DBNull.Value)
            {
                row["TotalCost"] = (object)DBNull.Value;
            }
            else
            {
                Decimal num = myDecimalSetting.RoundCurrency(myDecimalSetting.RoundQuantity(row["Qty"]) * myDecimalSetting.RoundCost(row["UnitCost"]));
                row["TotalCost"] = (object)num;
            }
        }
        private int GCD(int a, int b)
        {
            return b == 0 ? Math.Abs(a):GCD(b, a % b);
            //return a % b;
        }
        public void CalcProductRatio(DataRow row)
        {

            //if (myMasterRow["Qty"] != DBNull.Value && myMasterRow["Qty"] != DBNull.Value)
            //{
            //    if (BCE.Data.Convert.ToDecimal(row["ProductRatio"]) > 1)
            //    {
            //        //int qty = GCD( BCE.Data.Convert.ToInt32(myMasterRow["Qty"]), BCE.Data.Convert.ToInt32(row["ProductRatio"]));
            //        row["Qty"] = BCE.Data.Convert.ToDecimal(myMasterRow["Qty"])/ BCE.Data.Convert.ToDecimal(row["ProductRatio"]);
            //    }
            //}
        }
        private void DetailDataRowChangeEventHandler(object sender, DataRowChangeEventArgs e)
        {
            this.myDetailTableHvChange = true;
        }

        private void BSDetailDataColumnChangeEventHandler(object sender, DataColumnChangeEventArgs e)
        {
            this.myBSDetailTableHvChange = true;
            if (string.Compare(e.Column.ColumnName, "ItemCode", true) == 0)
            {
                //if(e.Row["BOMCode"]!=null)
                //{
                //    if (e.Row["BOMCode"].ToString() != "")
                //    {
                //        e.Row["ItemCode"]
                //    }
                //}
                StockDocumentItem stockDocumentItem = this.myItemBOMCmd.myHelper.LoadStockDocumentItem(e.Row[e.Column].ToString());
                if (stockDocumentItem != null)
                {
                    e.Row["Description"] = stockDocumentItem.Description;
                    string sUOM = this.myItemBOMCmd.myHelper.GetBaseUOM(stockDocumentItem.ItemCode);
                    decimal dRate = this.myItemBOMCmd.myHelper.GetItemUOMRate(stockDocumentItem.ItemCode, sUOM);
                    e.Row["UOM"] = sUOM;
                    e.Row["Rate"] = dRate;
                }
            }
        }

        private void BSDetailDataRowChangeEventHandler(object sender, DataRowChangeEventArgs e)
        {
            this.myBSDetailTableHvChange = true;
        }
        private void APDetailDataColumnChangeEventHandler(object sender, DataColumnChangeEventArgs e)
        {
            this.myAPDetailTableHvChange = true;
            if (string.Compare(e.Column.ColumnName, "ItemCode", true) == 0)
            {
                StockDocumentItem stockDocumentItem = this.myItemBOMCmd.myHelper.LoadStockDocumentItem(e.Row[e.Column].ToString());
                if (stockDocumentItem != null)
                    e.Row["Description"] = stockDocumentItem.Description;
            }
        }

        private void APDetailDataRowChangeEventHandler(object sender, DataRowChangeEventArgs e)
        {
            this.myAPDetailTableHvChange = true;
        }

        private void OVDDetailDataColumnChangeEventHandler(object sender, DataColumnChangeEventArgs e)
        {
            this.myOVDDetailTableHvChange = true;
            if (string.Compare(e.Column.ColumnName, "OverheadCode", true) == 0)
            {               

                object obj = BCE.AutoCount.Application.DBSetting.ExecuteScalar("select Description from RPA_Overhead where OverheadCode=?",(object)e.Row[e.Column].ToString());
                if (obj != null)
                    e.Row["Description"] = obj;
            }
        }

        private void RepDetailDataColumnChangeEventHandler(object sender, DataColumnChangeEventArgs e)
        {
            this.myRepDetailTableHvChange = true;
            if (string.Compare(e.Column.ColumnName, "ItemCode", true) == 0)
            {

                object obj = BCE.AutoCount.Application.DBSetting.ExecuteScalar("select Description from Item where ItemCode=?", (object)e.Row[e.Column].ToString());
                if (obj != null)
                    e.Row["Description"] = obj;
            }

            if (string.Compare(e.Column.ColumnName, "ItemCodeRep", true) == 0)
            {

                DataRow drItem = BCE.AutoCount.Application.DBSetting.GetFirstDataRow("select Description,BaseUOM from Item where ItemCode=?", (object)e.Row[e.Column].ToString());
                if (drItem != null)
                {
                    e.Row["DescriptionRep"] = drItem["Description"];
                    e.Row["UOM"] = drItem["BaseUOM"];
                }
            }
        }

        private void OVDDetailDataRowChangeEventHandler(object sender, DataRowChangeEventArgs e)
        {
            this.myOVDDetailTableHvChange = true;
        }
        private void RepDetailDataRowChangeEventHandler(object sender, DataRowChangeEventArgs e)
        {
            this.myRepDetailTableHvChange = true;
        }
        public void BeginLoadDetailData()
        {
            this.myDtblDetail.ColumnChanged -= new DataColumnChangeEventHandler(this.DetailDataColumnChangeEventHandler);
            this.myDtblDetail.BeginLoadData();
        }

        public void EndLoadDetailData()
        {
            this.myDtblDetail.EndLoadData();
            this.myDtblDetail.ColumnChanged += new DataColumnChangeEventHandler(this.DetailDataColumnChangeEventHandler);
        }

        public void BeginLoadBSDetailData()
        {
            this.myDtbLBSDetail.ColumnChanged -= new DataColumnChangeEventHandler(this.BSDetailDataColumnChangeEventHandler);
            this.myDtbLBSDetail.BeginLoadData();
        }

        public void EndLoadBSDetailData()
        {
            this.myDtbLBSDetail.EndLoadData();
            this.myDtbLBSDetail.ColumnChanged += new DataColumnChangeEventHandler(this.BSDetailDataColumnChangeEventHandler);
        }

        public void BeginLoadAPDetailData()
        {
            this.myDtbLAPDetail.ColumnChanged -= new DataColumnChangeEventHandler(this.APDetailDataColumnChangeEventHandler);
            this.myDtbLAPDetail.BeginLoadData();
        }

        public void EndLoadAPDetailData()
        {
            this.myDtbLAPDetail.EndLoadData();
            this.myDtbLAPDetail.ColumnChanged += new DataColumnChangeEventHandler(this.APDetailDataColumnChangeEventHandler);
        }

        public void BeginLoadOVDDetailData()
        {
            this.myDtblOvdDetail.ColumnChanged -= new DataColumnChangeEventHandler(this.OVDDetailDataColumnChangeEventHandler);
            this.myDtblOvdDetail.BeginLoadData();
        }

        public void EndLoadOVDDetailData()
        {
            this.myDtblOvdDetail.EndLoadData();
            this.myDtblOvdDetail.ColumnChanged += new DataColumnChangeEventHandler(this.OVDDetailDataColumnChangeEventHandler);
        }

        public void BeginLoadRepDetailData()
        {
            this.myDtblRepDetail.ColumnChanged -= new DataColumnChangeEventHandler(this.RepDetailDataColumnChangeEventHandler);
            this.myDtblRepDetail.BeginLoadData();
        }

        public void EndLoadRepDetailData()
        {
            this.myDtblRepDetail.EndLoadData();
            this.myDtblRepDetail.ColumnChanged += new DataColumnChangeEventHandler(this.RepDetailDataColumnChangeEventHandler);
        }
    }
}
