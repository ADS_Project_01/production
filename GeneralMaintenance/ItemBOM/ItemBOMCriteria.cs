﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.ItemBOMCriteria
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.SearchFilter;

namespace Production.GeneralMaintenance.ItemBOM
{
  public class ItemBOMCriteria : SearchCriteria
  {
    public SimpleSearch myDocNo;
    public SimpleSearch myDebtorName;
    public SimpleSearch myTechnicianCode;
    public SimpleSearch myContractNo;
    public SimpleSearch myServiceReportNo;
    public SimpleSearch myCopierModel;
    public SimpleSearch myCopierSerialNo;
    public SimpleSearch myCallerName;

    public string BOMCode
    {
      set
      {
        this.myDocNo.SearchData = value;
      }
    }

    public string ContractNo
    {
      set
      {
        this.myContractNo.SearchData = value;
      }
    }

    public string CopierSerialNo
    {
      set
      {
        this.myCopierSerialNo.SearchData = value;
      }
    }

    public string CopierModel
    {
      set
      {
        this.myCopierModel.SearchData = value;
      }
    }

    public string DebtorName
    {
      set
      {
        this.myDebtorName.SearchData = value;
      }
    }

    public string CallerName
    {
      set
      {
        this.myCallerName.SearchData = value;
      }
    }

    public string TechnicianCode
    {
      set
      {
        this.myTechnicianCode.SearchData = value;
      }
    }

    public string ServiceReportNo
    {
      set
      {
        this.myServiceReportNo.SearchData = value;
      }
    }

    public ItemBOMCriteria()
    {
      this.AddSearch(this.myDocNo = new SimpleSearch("", "BOMCode"));
    }
  }
}
