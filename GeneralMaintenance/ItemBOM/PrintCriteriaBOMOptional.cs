﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.PrintCriteriaItemBOM
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.SearchFilter;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Collections;

namespace RPASystem.GeneralMaintenance.ItemBOM
{
  [Serializable]
  public class PrintCriteriaItemBOM : ReportCriteria
  {
    private BCE.AutoCount.SearchFilter.Filter myItemBOMCodeFilter = new BCE.AutoCount.SearchFilter.Filter("", "ItemBOMCode");
    private ArrayList arrReadableTextList = new ArrayList();
    private bool myActiveItem = true;
    private bool myShowCriteriaInReport = true;
    private bool myInactiveItem;

    public BCE.AutoCount.SearchFilter.Filter ItemBOMCodeFilter
    {
      get
      {
        return this.myItemBOMCodeFilter;
      }
    }

    public bool ActiveItem
    {
      get
      {
        return this.myActiveItem;
      }
      set
      {
        this.myActiveItem = value;
      }
    }

    public bool InactiveItem
    {
      get
      {
        return this.myInactiveItem;
      }
      set
      {
        this.myInactiveItem = value;
      }
    }

    public bool ShowCriteriaInReport
    {
      get
      {
        return this.myShowCriteriaInReport;
      }
      set
      {
        this.myShowCriteriaInReport = value;
      }
    }

    protected override ArrayList GetFilterOptionsText()
    {
      ArrayList filterOptionsText = base.GetFilterOptionsText();
      string str = this.myItemBOMCodeFilter.BuildReadableText(false);
      if (str.Length > 0)
        filterOptionsText.Add((object) (Localizer.GetString((Enum) ItemBOMString.ItemBOMCode, new object[0]) + str));
      filterOptionsText.Add((object) Utils.BooleanToReadableText(Localizer.GetString((Enum) ItemBOMString.PrintActiveItemBOM, new object[0]), this.myActiveItem));
      filterOptionsText.Add((object) Utils.BooleanToReadableText(Localizer.GetString((Enum) ItemBOMString.PrintInactiveItemBOM, new object[0]), this.myInactiveItem));
      if (filterOptionsText.Count == 0)
        filterOptionsText.Add((object) Localizer.GetString((Enum) ReportCriteriaStringId.NoFilter, new object[0]));
      return filterOptionsText;
    }
  }
}
