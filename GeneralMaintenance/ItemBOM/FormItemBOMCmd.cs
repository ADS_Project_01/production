﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.FormItemBOMCmd
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Help;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Localization;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Production.GeneralMaintenance.ItemBOM
{

    [BCE.AutoCount.PlugIn.MenuItem("Item BOM Maintenance", BeginNewGroup = false, MenuOrder = 3, OpenAccessRight = "RPA_GEN_ITEMBOM_OPEN", ParentBeginNewGroup = false, ParentMenuCaption = "General Maintenance", ParentMenuOrder = 1, ShowAsDialog = false, VisibleAccessRight = "RPA_GEN_ITEMBOM_SHOW")]

    [SingleInstanceThreadForm]
  public class FormItemBOMCmd : XtraForm
  {
    private DBSetting myDBSetting;
    private ItemBOMCommand myItemBOMCommand;
    private UserAuthentication myUserAuthentication;
    private IContainer components;
    private PanelHeader panelHeader1;
    private BarButtonItem barBtnDocumentReport;
    private BarButtonItem barBtnListingReport;
    private BarButtonItem barBtnReportOption;
    private BarDockControl barDockControlBottom;
    private BarDockControl barDockControlLeft;
    private BarDockControl barDockControlRight;
    private BarDockControl barDockControlTop;
    private BarManager barManager1;
    private BarSubItem barSubItem1;
    private PanelControl panelCmd;
    private PanelControl panelGrid;
    private SimpleButton sbtnDel;
    private SimpleButton sbtnEdit;
    private SimpleButton sbtnNew;
    private SimpleButton sbtnPrint;
    private FormItemBOMPrint myFrmPrint;
        private Bar bar1;
        private BarSubItem barSubItem2;
        private BarButtonItem barButtonItem1;
        private BarButtonItem barButtonItem2;
        private BarButtonItem barButtonItem3;
        private BarButtonItem barButtonItem4;
        private UCItemBOMGrid ucbomOptionalGrid1;

    public FormItemBOMCmd(DBSetting dbSetting)
    {
      this.InitializeComponent();
      this.myDBSetting = dbSetting;
      this.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
      this.myItemBOMCommand = new ItemBOMCommand();
      this.myItemBOMCommand = ItemBOMCommand.Create(dbSetting);
      this.myItemBOMCommand.FormOwner = (Form) this;
      //string str = "";
      //foreach (UDFColumn udfColumn in new UDFUtil(this.myDBSetting).GetUDF("ItemBOM"))
       // str = str + ", " + udfColumn.FieldName;
      this.myItemBOMCommand.BrowseAll("SELECT * FROM RPA_ItemBOM");
      this.ucbomOptionalGrid1.Initialize(dbSetting);
      BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "BOM_Material_Usage_Inquiry.htm");
      DBSetting dbSetting1 = this.myDBSetting;
      string docType = "";
      long docKey = 0L;
      long eventKey = 0L;
      // ISSUE: variable of a boxed type
      ItemBOMString local = ItemBOMString.OpenItemBOMMaintenance;
      object[] objArray = new object[1];
      int index = 0;
      string loginUserId = this.myUserAuthentication.LoginUserID;
      objArray[index] = (object) loginUserId;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      string detail = "";
      Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
    }

    private void GeneralPrint()
    {
            if (this.myFrmPrint == null)
            {
                this.myFrmPrint = new FormItemBOMPrint(this.myItemBOMCommand);
                ((Control)this.myFrmPrint).Show();
            }
            else if (this.myFrmPrint.CanFocus)
            {
                this.myFrmPrint.WindowState = FormWindowState.Maximized;
                this.myFrmPrint.BringToFront();
            }
            else
            {
                this.myFrmPrint = new FormItemBOMPrint(this.myItemBOMCommand);
                ((Control)this.myFrmPrint).Show();
            }
        }

    private void NewDocument()
    {
      if (this.myUserAuthentication.AccessRight.IsAccessible("MF_BOPT_NEW", (XtraForm) this))
        FormItemBOMCmd.StartEntryForm(this.myItemBOMCommand.NewItemBOM(), this.myDBSetting);
    }

    private void EditDocument()
    {
      if (this.myUserAuthentication.AccessRight.IsAccessible("MF_BOPT_EDIT", (XtraForm) this))
      {
        try
        {
          DataRow dataRow = this.ucbomOptionalGrid1.GetDataRow();
          if (dataRow != null)
          {
            ItemBOMEntity bomOptionalEntity = this.myItemBOMCommand.ViewEditItemBOM((long) BCE.Data.Convert.ToDBInt64(dataRow["DocKey"]), ItemBOMCommand.Action.Edit);
            if (bomOptionalEntity != null)
              FormItemBOMCmd.StartEntryForm(bomOptionalEntity, this.myDBSetting);
            else
              AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum) ItemBOMMaintStringId.ErrorMessage_RecordsNotExist, new object[0]));
          }
          else
            AppMessage.ShowInformationMessage(BCE.Localization.Localizer.GetString((Enum) ItemBOMMaintStringId.ErrorMessage_RecordNotSelected, new object[0]));
        }
        catch (Exception ex)
        {
          AppMessage.ShowErrorMessage(ex.Message);
        }
      }
    }

    private void DeleteDocument()
    {
      if (this.myUserAuthentication.AccessRight.IsAccessible("MF_BOPT_DELETE", (XtraForm) this))
      {
        DataRow dataRow = this.ucbomOptionalGrid1.GetDataRow();
        if (dataRow != null)
        {
                    // ISSUE: variable of a boxed type
                    ItemBOMMaintStringId local = ItemBOMMaintStringId.ConfirmMessage_DeleteItemBOM;
          object[] objArray = new object[1];
          int index1 = 0;
          string str = dataRow["BOMCode"].ToString();
          objArray[index1] = (object) str;
          if (AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum) local, objArray)))
          {
            FormItemBOMCmd formBomOptionalCmd = this;
            long[] DocKey = new long[1];
            int index2 = 0;
            long num = (long) BCE.Data.Convert.ToDBInt64(dataRow["DocKey"]);
            DocKey[index2] = num;
            string BOMCode = dataRow["BOMCode"].ToString();
            formBomOptionalCmd.Delete(DocKey, BOMCode);
          }
        }
        else
          AppMessage.ShowInformationMessage(BCE.Localization.Localizer.GetString((Enum) ItemBOMMaintStringId.ErrorMessage_RecordNotSelected, new object[0]));
      }
    }

    private void Delete(long[] DocKey, string BOMCode)
    {
      try
      {
        this.myItemBOMCommand.DeleteData(DocKey);
        FormItemBOMCmd formBomOptionalCmd = this;
                // ISSUE: variable of a boxed type
                ItemBOMMaintStringId local1 = ItemBOMMaintStringId.ShowMessage_TotalDeletedSuccessfully;
        object[] objArray1 = new object[1];
        int index1 = 0;
        string str1 = DocKey.Length.ToString();
        objArray1[index1] = (object) str1;
        string string1 = BCE.Localization.Localizer.GetString((Enum) local1, objArray1);
        AppMessage.ShowInformationMessage((IWin32Window) formBomOptionalCmd, string1);
        DBSetting dbSetting = this.myDBSetting;
        string docType = "";
        long docKey = 0L;
        long eventKey = 0L;
        // ISSUE: variable of a boxed type
        ItemBOMString local2 = ItemBOMString.DeletedItemBOMCode;
        object[] objArray2 = new object[2];
        int index2 = 0;
        string loginUserId = this.myUserAuthentication.LoginUserID;
        objArray2[index2] = (object) loginUserId;
        int index3 = 1;
        string str2 = ((object) BOMCode).ToString();
        objArray2[index3] = (object) str2;
        string string2 = BCE.Localization.Localizer.GetString((Enum) local2, objArray2);
        string detail = "";
        Activity.Log(dbSetting, docType, docKey, eventKey, string2, detail);
      }
      catch (DBConcurrencyException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) ItemBOMMaintStringId.ErrorMessage_OperationAborted, new object[0]));
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
      }
    }

    public static void StartEntryForm(ItemBOMEntity bomOptionalEntity, DBSetting dbSetting)
    {
      string assemblyQualifiedName = typeof (FormItemBOMEntry).AssemblyQualifiedName;
      System.Type[] types = new System.Type[2];
      int index1 = 0;
      System.Type type1 = bomOptionalEntity.GetType();
      types[index1] = type1;
      int index2 = 1;
      System.Type type2 = dbSetting.GetType();
      types[index2] = type2;
      object[] parameters = new object[2];
      int index3 = 0;
      ItemBOMEntity bomOptionalEntity1 = bomOptionalEntity;
      parameters[index3] = (object) bomOptionalEntity1;
      int index4 = 1;
      DBSetting dbSetting1 = dbSetting;
      parameters[index4] = (object) dbSetting1;
      new ThreadForm(assemblyQualifiedName, types, parameters).Show();
    }

    private void sbtnEdit_Click(object sender, EventArgs e)
    {
            if (myUserAuthentication.AccessRight.IsAccessible(AccessRightConst.RPA_GEN_ITEMBOM_EDIT, true))
            {
                this.EditDocument();
            }
    }

    private void sbtnDel_Click(object sender, EventArgs e)
    {
            if (myUserAuthentication.AccessRight.IsAccessible(AccessRightConst.RPA_GEN_ITEMBOM_DELETE, true))
            {
                this.DeleteDocument();
            }
    }

    private void sbtnPrint_Click(object sender, EventArgs e)
    {
            if (myUserAuthentication.AccessRight.IsAccessible(AccessRightConst.RPA_GEN_ITEMBOM_PRINT, true))
            {
                this.GeneralPrint();
            }
    }

    private void sbtnView_Click(object sender, EventArgs e)
    {
            if (myUserAuthentication.AccessRight.IsAccessible(AccessRightConst.RPA_GEN_ITEMBOM_VIEW, true))
            {
                this.NewDocument();
            }
    }

    private void FormItemBOMCmd_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
        this.NewDocument();
      else if (e.KeyCode == (Keys) 113)
        this.EditDocument();
      else if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space))
        this.DeleteDocument();
      else if (e.KeyCode == (Keys) 118)
        this.GeneralPrint();
      else if (e.KeyCode == (Keys.LButton | Keys.RButton | Keys.Back | Keys.ShiftKey))
        this.Close();
    }

    private void FormItemBOMCmd_Load(object sender, EventArgs e)
    {
      this.ucbomOptionalGrid1.DataSource = (object) this.myItemBOMCommand.DtblItemBOM;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnDocumentReport = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnListingReport = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnReportOption = new DevExpress.XtraBars.BarButtonItem();
            this.panelCmd = new DevExpress.XtraEditors.PanelControl();
            this.sbtnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnNew = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.panelGrid = new DevExpress.XtraEditors.PanelControl();
            this.ucbomOptionalGrid1 = new Production.GeneralMaintenance.ItemBOM.UCItemBOMGrid();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCmd)).BeginInit();
            this.panelCmd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).BeginInit();
            this.panelGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem2,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 9;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Report";
            this.barSubItem2.Id = 4;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4, true)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Design Document Style Report";
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Design Listing Style Report";
            this.barButtonItem2.Id = 6;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Design Detail Listing Style Report";
            this.barButtonItem3.Id = 7;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Report Option";
            this.barButtonItem4.Id = 8;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(901, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 438);
            this.barDockControlBottom.Size = new System.Drawing.Size(901, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 416);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(901, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 416);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnDocumentReport
            // 
            this.barBtnDocumentReport.Id = 1;
            this.barBtnDocumentReport.Name = "barBtnDocumentReport";
            // 
            // barBtnListingReport
            // 
            this.barBtnListingReport.Id = 2;
            this.barBtnListingReport.Name = "barBtnListingReport";
            // 
            // barBtnReportOption
            // 
            this.barBtnReportOption.Id = 3;
            this.barBtnReportOption.Name = "barBtnReportOption";
            // 
            // panelCmd
            // 
            this.panelCmd.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCmd.Controls.Add(this.sbtnPrint);
            this.panelCmd.Controls.Add(this.sbtnDel);
            this.panelCmd.Controls.Add(this.sbtnNew);
            this.panelCmd.Controls.Add(this.sbtnEdit);
            this.panelCmd.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCmd.Location = new System.Drawing.Point(0, 102);
            this.panelCmd.Name = "panelCmd";
            this.panelCmd.Size = new System.Drawing.Size(901, 49);
            this.panelCmd.TabIndex = 1;
            // 
            // sbtnPrint
            // 
            this.sbtnPrint.Location = new System.Drawing.Point(257, 13);
            this.sbtnPrint.Name = "sbtnPrint";
            this.sbtnPrint.Size = new System.Drawing.Size(75, 23);
            this.sbtnPrint.TabIndex = 0;
            this.sbtnPrint.Text = "Print";
            this.sbtnPrint.Click += new System.EventHandler(this.sbtnPrint_Click);
            // 
            // sbtnDel
            // 
            this.sbtnDel.Appearance.ForeColor = System.Drawing.Color.Red;
            this.sbtnDel.Appearance.Options.UseForeColor = true;
            this.sbtnDel.Location = new System.Drawing.Point(176, 13);
            this.sbtnDel.Name = "sbtnDel";
            this.sbtnDel.Size = new System.Drawing.Size(75, 23);
            this.sbtnDel.TabIndex = 1;
            this.sbtnDel.Text = "Delete";
            this.sbtnDel.Click += new System.EventHandler(this.sbtnDel_Click);
            // 
            // sbtnNew
            // 
            this.sbtnNew.Location = new System.Drawing.Point(12, 13);
            this.sbtnNew.Name = "sbtnNew";
            this.sbtnNew.Size = new System.Drawing.Size(75, 23);
            this.sbtnNew.TabIndex = 2;
            this.sbtnNew.Text = "New";
            this.sbtnNew.Click += new System.EventHandler(this.sbtnView_Click);
            // 
            // sbtnEdit
            // 
            this.sbtnEdit.Location = new System.Drawing.Point(93, 13);
            this.sbtnEdit.Name = "sbtnEdit";
            this.sbtnEdit.Size = new System.Drawing.Size(75, 23);
            this.sbtnEdit.TabIndex = 3;
            this.sbtnEdit.Text = "Edit";
            this.sbtnEdit.Click += new System.EventHandler(this.sbtnEdit_Click);
            // 
            // panelGrid
            // 
            this.panelGrid.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelGrid.Controls.Add(this.ucbomOptionalGrid1);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 151);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(901, 287);
            this.panelGrid.TabIndex = 0;
            // 
            // ucbomOptionalGrid1
            // 
            this.ucbomOptionalGrid1.ColumnAutoWidth = true;
            this.ucbomOptionalGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucbomOptionalGrid1.Location = new System.Drawing.Point(0, 0);
            this.ucbomOptionalGrid1.Name = "ucbomOptionalGrid1";
            this.ucbomOptionalGrid1.Size = new System.Drawing.Size(901, 287);
            this.ucbomOptionalGrid1.TabIndex = 0;
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "BOM (Bill of Material) Maintenance";
            this.panelHeader1.HelpTopicId = "BOM_Material_Usage_Inquiry.htm";
            this.panelHeader1.Hint = "In this Bill of material Window, you can  create, modify, or delete Bill of Mater" +
    "ial";
            this.panelHeader1.Location = new System.Drawing.Point(0, 22);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(901, 80);
            this.panelHeader1.TabIndex = 2;
            // 
            // FormItemBOMCmd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(901, 438);
            this.Controls.Add(this.panelGrid);
            this.Controls.Add(this.panelCmd);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormItemBOMCmd";
            this.Text = "BOM Maintenance";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormItemBOMCmd_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormItemBOMCmd_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCmd)).EndInit();
            this.panelCmd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).EndInit();
            this.panelGrid.ResumeLayout(false);
            this.ResumeLayout(false);

    }
  }
}
