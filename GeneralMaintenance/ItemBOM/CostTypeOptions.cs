﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BCE.Localization;
namespace Production.GeneralMaintenance.ItemBOM
{
    [LocalizableString]
    public enum CostTypeOptions
    {
        [DefaultString("Fixed")]
        Fixed,
        [DefaultString("Percentage/Real Cost")]
        Percentage,
        [DefaultString("Standard")]
        Standard,
    }
}
