﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.FormItemBOMEntry
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.ContextException;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Help;
using BCE.AutoCount.Stock;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.UDF;
using BCE.AutoCount.WinForms;
using BCE.AutoCount.XtraUtils;
using BCE.Controls;
using BCE.Data;
using BCE.Localization;
using BCE.XtraUtils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Production.Tools.LookupEditBuilders;
using BCE.AutoCount.RegistryID.DecimalID;
namespace Production.GeneralMaintenance.ItemBOM
{
    public class FormItemBOMEntry : XtraForm
    {

        private DBSetting myDBSetting;
        private ItemBOMEntity myEntity;
        private DataTable myDtblMaster;
        private DataTable myDtblDetail;
        private DataTable myDtblBSDetail;
        private DataTable myDtblAPDetail;
        private DataTable myDtblOvdDetail;
        private DataTable myDtblRepDetail;
        private string strWindowCaption;
        private UndoManager myDetailRecordUndo;
        private UndoManager myBSDetailRecordUndo;
        private UndoManager myAPDetailRecordUndo;
        private UndoManager myOvdDetailRecordUndo;
        private UndoManager myRepDetailRecordUndo;
        private UserAuthentication myUserAuthentication;
        private FormItemBOMEditOption myEditOption;
        private IContainer components;
        private BarButtonItem barBtnCopyFrom;
        private BarButtonItem barBtnCopyTo;
        private BarButtonItem barBtnCreateProposeCont;
        private BarButtonItem barButtonItem1;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private BarDockControl barDockControlTop;
        private BarManager barManager1;
        private BarSubItem barSubItem1;
        private CheckEdit chkedtIsActive;
        private CheckEdit chkedtProceedNew;
        private PanelControl panelBottom;
        private PanelControl panelControl1;
        private PanelControl panelGridEdit;
        private PanelControl panelMiddle;
        private PanelControl panelTop;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private RepositoryItemComboBox repositoryItemComboBox1;
        private RepositoryItemComboBox repositoryItemComboBox2;
        private RepositoryItemComboBox repositoryItemComboBox3;
        private RepositoryItemComboBox repositoryItemComboBox4;
        private RepositoryItemLookUpEdit repBOMItemLookUpEdit;
        private RepositoryItemLookUpEdit repItemLuedtItem;
        private RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private RepositoryItemLookUpEdit repositoryItemLookUpEditUOM;
        private SimpleButton sbtnAddDTL;
        private SimpleButton sbtnAddBSDTL;
        private SimpleButton sBtnCancel;
        private SimpleButton sbtnDeleteDTL;
        private SimpleButton sbtnDeleteBSDTL;
        private SimpleButton sBtnOK;
        private SimpleButton sbtnUndo;
        private SimpleButton sbtnUndoBSDtl;
        private TextEdit txtedtBOMCode;
        private TextEdit txtedtDescription;
        private TextEdit txtedtDescription2;
        private GridColumn colBOMDesc;
        private GridColumn colBSCostPercent;
        private GridColumn colBSItemCode;
        private GridColumn ColBSCostMax;
        private GridColumn colDesc;
        private GridColumn colUnitCost;
        private GridColumn colQty;
        private GridColumn colItemCode;
        private GridControl gridCtlItem;
        private GridControl gridCtlIBOMBS;
        private GridView gvBOMBS;
        private GridView gvItem;
        private XtraTabControl tabCtlPriceI;
        private XtraTabControl xtraTabCtlItemBOM;
        private XtraTabPage xtraTabBOMItem;
        private XtraTabPage xtraTabCtlPriceSchemeII;
        private XtraTabPage xtraTabDetails;
        private XtraTabPage xtraTabPriceSchemeI;
        private ImageList imageList1;
        private Label label1;
        private XtraTabPage xtraTabPage1;
        private XtraTabPage xtraTabPage2;
        private XtraTabPage xtraTabPageAP;
        private XtraTabPage xtraTabPageOvd;
        private LookUpEdit luItemCode;
        private Label label6;
        private Label label4;
        private ComboBoxEdit cbCostType;
        private Label label3;
        private TextEdit txtQty;
        private Label label2;
        private GridControl gridCtlIBOMAP;
        private GridView gvBOMAP;
        private PanelControl panelControl2;
        private SimpleButton sbtnUndoAPDtl;
        private SimpleButton sbtnAddAPDtl;
        private SimpleButton sbtnDeleteAPDtl;
        private GridControl gridCtlIBOMOvd;
        private GridView gvBOMOvd;
        private PanelControl panelControl3;
        private SimpleButton sbtnUndoOvdDtl;
        private SimpleButton sbtnAddOvdDtl;
        private SimpleButton sbtnDeleteOvdDtl;
        private GridColumn gridColumn1;
        private GridColumn colTotalCost;
        private GridColumn colRate;
        private GridColumn colDocKey;
        private GridColumn colDtlKey;
        private GridColumn colUOM;
        private RepositoryItemLookUpEdit repositoryItemluUOM;
        private RepositoryItemLookUpEdit repItemBSItemCode;
        private GridColumn colAPItemCode;
        private RepositoryItemLookUpEdit repleAPItemCode;
        private GridColumn colDescription;
        private GridColumn colAPCostPercent;
        private GridColumn colOvdOverheadCode;
        private RepositoryItemLookUpEdit repleOvdOverheadCode;
        private GridColumn colOvdDescription;
        private GridColumn colOvdAmount;
        private SpinEdit spCost;
        private Label lblCostpercent;
        private LookUpEdit luUOM;
        private GridColumn colBOMCode;
        private RepositoryItemLookUpEdit repItemBOMCode;
        private Label label7;
        private LookUpEdit luProjNo;
        private Label lblLocation;
        private GridColumn colBSUOM;
        private RepositoryItemLookUpEdit repleBSUOM;
        private GridColumn colBSRate;
        private GridColumn colFOHCharge;
        private RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private Label label5;
        private GridColumn colItemBOMCode;
        private GridColumn colProductRatio;
        private Label label8;
        private Production.Tools.LookupEditBuilders.ItemBOMLookupEditBuilder itembomlookup;
        private XtraTabPage tabItemReplacement;
        private GridControl gridCtlIBOMRep;
        private GridView gvBOMRep;
        private GridColumn gridColumn2;
        private GridColumn gridColumn3;
        private GridColumn colItemCodeRep;
        private GridColumn colItemCode2;
        private GridColumn gridColumn6;
        private GridColumn colUOMRep;
        private RepositoryItemLookUpEdit repleItemCode2;
        private GridColumn colQtyRep;
        private RepositoryItemLookUpEdit repleItemCodeRep;
        private GridColumn colProductRatioRep;
        private RepositoryItemLookUpEdit repleUOM;
        private PanelControl panelControl4;
        private SimpleButton sbtnUndoRep;
        private SimpleButton sbtnAddRep;
        private SimpleButton sbtnDeleteRep;
        private GridColumn colDescriptionRep;
        private RepositoryItemLookUpEdit repBOMCode;
        private CustomizeGridLayout myGridLayout;
        public FormItemBOMEntry(ItemBOMEntity aEntity, DBSetting dbSetting)
          : this(aEntity, dbSetting, new FormItemBOMEditOption())
        {
        }

        public FormItemBOMEntry(ItemBOMEntity aEntity, DBSetting dbSetting, FormItemBOMEditOption editOption)
        {
            if (aEntity == null)
                throw new ArgumentNullException("aEntity");
            else if (dbSetting == null)
                throw new ArgumentNullException("dbSetting");
            else if (editOption == null)
            {
                throw new ArgumentNullException("editOption");
            }
            else
            {
                this.myEditOption = editOption;
                this.InitializeComponent();
                this.myDBSetting = dbSetting;
                this.myEntity = aEntity;
                this.myUserAuthentication = UserAuthentication.GetOrCreate(this.myDBSetting);
                this.myDtblMaster = this.myEntity.MasterTable;


                this.InitializeFormControl();

                this.DataBinding();
                this.ReconnectGridColumnEdit();
                this.SetControlState();
                this.InitUndoManager();
                //UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
                GridView gridView = this.gvItem;
                //string tableName1 = "ItemBOMDTL";
                //udfUtil.SetupGrid(gridView, tableName1);
                XtraTabControl tabControl = this.xtraTabCtlItemBOM;
                //string tableName2 = "ItemBOM";
                DataTable masterTable = this.myEntity.MasterTable;
                //udfUtil.SetupTabControl(tabControl, tableName2, (object) masterTable);
                if (aEntity.Action == ItemBOMCommand.Action.Edit)
                {
                    DBSetting dbSetting1 = dbSetting;
                    txtedtBOMCode.Enabled = false;
                    string docType = "";
                    long docKey = 0L;
                    long eventKey = 0L;
                    // ISSUE: variable of a boxed type
                    ItemBOMString local = ItemBOMString.ViewItemBOMCodeInEditMode;
                    object[] objArray = new object[2];
                    int index1 = 0;
                    string loginUserId = this.myUserAuthentication.LoginUserID;
                    objArray[index1] = (object)loginUserId;
                    int index2 = 1;
                    string str = aEntity.BOMCode.ToString();
                    objArray[index2] = (object)str;
                    string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                    string detail = "";
                    Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
                }
                BCE.AutoCount.Help.HelpProvider.SetHelpTopic(new System.Windows.Forms.HelpProvider(), (Control)this, "BOM_Material_Usage_Inquiry.htm");
            }
        }
        private void FilterItemBOM(GridView view)
        {
            if (view.FocusedColumn != null && view.FocusedColumn.FieldName == "BOMCode")
            {
                DataRow dataRow = view.GetDataRow(view.FocusedRowHandle);
                if (dataRow != null)
                {
                    //ItemBOMLookupEditBuilder itembomlookup = new ItemBOMLookupEditBuilder();
                    //tembomlookup.BuildLookupEdit(this.repleItemBOM, this.myDBSetting);
                    itembomlookup.FilterItemCode(dataRow["ItemCode"].ToString());
                }
            }
        }
        private void SetControlState()
        {
            ItemBOMCommand.Action action = this.myEntity.Action;
            Utils.SetReadOnly((IEnumerable)this.Controls, action == ItemBOMCommand.Action.View);
           // Object obj = myDBSetting.ExecuteScalar("select count(*) from RPA_WO WHERE Cancelled='F' AND BOMCode=?",(object)myEntity.BOMCode);
           /// if(obj!=null && obj!=DBNull.Value)
           // {
           //     if(BCE.Data.Convert.ToDecimal(obj)>0)
           //     {
           //         cbCostType.Enabled = false;
           //         luItemCode.Enabled = false;
           //         txtedtBOMCode.Enabled = false;
           //         txtQty.Enabled = false;
           //         txtedtDescription.Enabled = false;
           //         txtedtDescription2.Enabled = false;
           //         luUOM.Enabled = false;
           //         spCost.Enabled = false;
           //     }
           // }
            if (action == ItemBOMCommand.Action.New)
                this.barBtnCreateProposeCont.Enabled = false;
            else
                this.barBtnCreateProposeCont.Enabled = true;
            this.chkedtProceedNew.Visible = action == ItemBOMCommand.Action.New && this.myEditOption.ShowProceedNewItemBOM;
            this.TitleForm();
        }

        private void TitleForm()
        {
            bool flag = this.myEntity.Action == ItemBOMCommand.Action.View;
            if (this.myEntity.Action == ItemBOMCommand.Action.New)
            {
                this.strWindowCaption = BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.Code_NewItemBOM, new object[0]);
                this.Text = BCE.AutoCount.WinForms.FormHelper.SetUniqueWindowCaption(this.strWindowCaption, (Form)this);
            }
            else if (flag)
            {
                this.strWindowCaption = BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.Code_ViewItemBOM, new object[0]);
                this.Text = BCE.AutoCount.WinForms.FormHelper.SetUniqueWindowCaption(string.Format(this.strWindowCaption, (object)this.myEntity.BOMCode.ToString()), (Form)this);
            }
            else
            {
                this.strWindowCaption = BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.Code_EditItemBOM, new object[0]);
                this.Text = BCE.AutoCount.WinForms.FormHelper.SetUniqueWindowCaption(string.Format(this.strWindowCaption, (object)this.myEntity.BOMCode.ToString()), (Form)this);
            }
        }

        private void DataBinding()
        {

            this.txtedtBOMCode.DataBindings.Clear();
            this.txtedtBOMCode.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "BOMCode"));
            this.txtedtDescription.DataBindings.Clear();
            this.txtedtDescription.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "Description"));
            this.txtedtDescription2.DataBindings.Clear();
            this.txtedtDescription2.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "Desc2"));
            this.chkedtIsActive.DataBindings.Clear();
            this.chkedtIsActive.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "IsActive"));
            this.luItemCode.DataBindings.Clear();
            this.luItemCode.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "ItemCode"));
            this.luUOM.DataBindings.Clear();
            this.luUOM.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "UOM"));
            this.txtQty.DataBindings.Clear();
            this.txtQty.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "Qty"));
            this.cbCostType.DataBindings.Clear();
            this.cbCostType.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "CostType"));
            this.spCost.DataBindings.Clear();
            this.spCost.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "Cost"));
            this.luProjNo.DataBindings.Clear();
            this.luProjNo.DataBindings.Add(new Binding("EditValue", (object)this.myDtblMaster, "ProjNo"));
            this.myDtblDetail = this.myEntity.DetailTable;
            this.myDtblBSDetail = this.myEntity.BSDetailTable;
            this.myDtblAPDetail = this.myEntity.APDetailTable;
            this.myDtblOvdDetail = this.myEntity.OvdDetailTable;
            this.myDtblRepDetail = this.myEntity.RepDetailTable;
            this.gridCtlItem.DataSource = (object)this.myDtblDetail;
            this.gridCtlIBOMBS.DataSource = (object)this.myDtblBSDetail;
            this.gridCtlIBOMAP.DataSource = (object)this.myDtblAPDetail;
            this.gridCtlIBOMOvd.DataSource = (object)this.myDtblOvdDetail;
            this.gridCtlIBOMRep.DataSource = (object)this.myDtblRepDetail;
            InitializeFormControlCost((CostTypeOptions)cbCostType.SelectedIndex);

        }
        private void FilterItemUOM(GridView view)
        {
            if (view.FocusedColumn != null && view.FocusedColumn.FieldName == "UOM")
            {
                DataRow dataRow = view.GetDataRow(view.FocusedRowHandle);
                if (dataRow != null)
                    DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.FilterItemCode(dataRow["ItemCode"].ToString());
            }
        }
        private void FilterItemUOM2(GridView view)
        {
            if (view.FocusedColumn != null && view.FocusedColumn.FieldName == "UOM")
            {
                DataRow dataRow = view.GetDataRow(view.FocusedRowHandle);
                if (dataRow != null)
                    DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.FilterItemCode(dataRow["ItemCodeRep"].ToString());
            }
        }
        private void InitializeFormControlCost(CostTypeOptions costypeoptions)
        {
            FormItemBOMEntry bomOptionalEntry = this;

            FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting, false);
            if (costypeoptions == CostTypeOptions.Fixed)
            {
                colBSCostPercent.Visible = false;

                //string fieldname2 = "Cost";
                //string fieldtype2 = "Cost";
                //formControlUtil.AddField(fieldname2, fieldtype2);
                spCost.Properties.ReadOnly = false;
                //this.spCost.Properties.Mask.EditMask = "";
                //this.spCost.Properties.Mask.UseMaskAsDisplayFormat = false;
                this.spCost.Properties.MinValue = 0;
                this.spCost.Properties.MaxValue = 999999999999;
            }
            else if (costypeoptions == CostTypeOptions.Percentage)
            {
                colBSCostPercent.Visible = true;
                //string fieldname2 = "Cost";
                //string fieldtype2 = "Percentage";
                //formControlUtil.AddField(fieldname2, fieldtype2);
                spCost.Properties.ReadOnly = false;
                //this.spCost.Properties.Mask.UseMaskAsDisplayFormat = true;
                //this.spCost.Properties.Mask.EditMask = "p";
                this.spCost.Properties.MinValue = 0;
                this.spCost.Properties.MaxValue = 100;

            }
            else if (costypeoptions == CostTypeOptions.Standard)
            {
                colBSCostPercent.Visible = false;

                //string fieldname2 = "Cost";
                //string fieldtype2 = "Cost";
                //formControlUtil.AddField(fieldname2, fieldtype2);

                //this.spCost.Properties.Mask.UseMaskAsDisplayFormat = false;
                //this.spCost.Properties.Mask.EditMask = "";
                this.spCost.Properties.MinValue = 0;
                this.spCost.Properties.MaxValue = 999999999999;
                StockHelper myHelper = StockHelper.Create(myDBSetting);
                if (luItemCode.EditValue != null && luItemCode.EditValue != DBNull.Value)
                {
                    StockDocumentItem stockDocumentItem = myHelper.LoadStockDocumentItem(luItemCode.EditValue.ToString());
                    if (stockDocumentItem != null)
                    {
                        spCost.Value = BCE.Data.Convert.ToDecimal(stockDocumentItem.Cost);
                    }
                }
                spCost.Properties.ReadOnly = true;
                //this.spCost.EditValue = 90000;

            }
            //formControlUtil.InitControls((Control)bomOptionalEntry);


        }

        private void InitLookupItemMaterial()
        {
            DataTable dtItemMaterial = new DataTable();
            dtItemMaterial.Columns.Add("ItemCode", typeof(string));
            dtItemMaterial.Columns.Add("Description", typeof(string));
            dtItemMaterial.Constraints.Add("ItemCode", dtItemMaterial.Columns["ItemCode"], true);

            foreach(DataRow drDetailItem in myEntity.GetValidDetailRows())
            {
                DataRow drFind = dtItemMaterial.Rows.Find(drDetailItem["ItemCode"]);

                if (drFind == null)
                {
                    DataRow dtadd = dtItemMaterial.NewRow();
                    dtadd["ItemCode"] = drDetailItem["ItemCode"];
                    dtadd["Description"] = drDetailItem["Description"];
                    dtItemMaterial.Rows.Add(dtadd);
                }
            }
         
            repleItemCode2.DataSource = (object)dtItemMaterial;
            repleItemCode2.DisplayMember = "ItemCode";
            repleItemCode2.ValueMember = "ItemCode";
            repleItemCode2.Columns.Clear();
            repleItemCode2.Columns.Add(new LookUpColumnInfo("ItemCode", "Item Code", 100));
            repleItemCode2.Columns.Add(new LookUpColumnInfo("Description", "Description", 200));
        }
        private void FillItemInformation()
        {
            StockHelper myHelper = StockHelper.Create(myDBSetting);
            StockDocumentItem stockDocumentItem = myHelper.LoadStockDocumentItem(luItemCode.EditValue.ToString());
            if (stockDocumentItem != null)
            {
                txtedtDescription.EditValue = stockDocumentItem.Description;
                string sUOM = myHelper.GetBaseUOM(stockDocumentItem.ItemCode);
                decimal dRate = myHelper.GetItemUOMRate(stockDocumentItem.ItemCode, sUOM);
                luUOM.EditValue = sUOM;
              
            }



        }
        private void InitializeFormControl()
        {
            FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting, false);
            object obj = myDBSetting.ExecuteScalar("select QtyDecimal from RPA_Settings");
            if (obj != null && obj != DBNull.Value)
            {
                colQty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                colQty.DisplayFormat.FormatString = "N" + obj.ToString();

                colQtyRep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                colQtyRep.DisplayFormat.FormatString = "N" + obj.ToString();

                txtQty.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                txtQty.Properties.DisplayFormat.FormatString = "N" + obj.ToString();
                // colQty.DisplayFormat.FormatString = "d" + obj.ToString();
            }
            obj = myDBSetting.ExecuteScalar("select RateDecimal from RPA_Settings");
            if (obj != null && obj != DBNull.Value)
            {

                colProductRatioRep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                colProductRatioRep.DisplayFormat.FormatString = "N" + obj.ToString();
                colProductRatio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                colProductRatio.DisplayFormat.FormatString = "N" + obj.ToString();
                colBSRate.DisplayFormat.FormatString = "N" + obj.ToString();
                colBSRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;

                colRate.DisplayFormat.FormatString = "N" + obj.ToString();
                colRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;

            }
            //string fieldname1 = "Qty";
            //string fieldtype1 = "Quantity";
            //formControlUtil.AddField(fieldname1, fieldtype1);
            txtQty.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            txtQty.Properties.DisplayFormat.FormatString = BCE.AutoCount.Settings.DecimalSetting.GetOrCreate(myDBSetting).FormatQuantity(0);
            txtQty.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;

            txtQty.Properties.EditFormat.FormatString = BCE.AutoCount.Settings.DecimalSetting.GetOrCreate(myDBSetting).FormatQuantity(0);

            //txtQty.Properties.DisplayFormat.FormatString = BCE.AutoCount.Settings.DecimalSetting.GetOrCreate(myDBSetting).GetQuantityFormatString(0);
            //string fieldname2 = "Cost";
            //string fieldtype2 = "Cost";
            //formControlUtil.AddField(fieldname2, fieldtype2);
            string fieldname3 = "UnitCost";
            string fieldtype3 = "Cost";
            formControlUtil.AddField(fieldname3, fieldtype3);
            string fieldname4 = "TotalCost";
            string fieldtype4 = "Currency";
            formControlUtil.AddField(fieldname4, fieldtype4);

            string fieldname5 = "CostPercent";
            string fieldtype5 = "Quantity";
            formControlUtil.AddField(fieldname5, fieldtype5);
            string fieldname6 = "CostMax";
            string fieldtype6 = "Quantity";
            formControlUtil.AddField(fieldname6, fieldtype6);
            string fieldname7 = "Amount";
            string fieldtype7 = "Cost";
            formControlUtil.AddField(fieldname7, fieldtype7);
            //string fieldname8 = "ProductRatio";
            //string fieldtype8 = "Quantity";
            //formControlUtil.AddField(fieldname8, fieldtype8);
            FormItemBOMEntry bomOptionalEntry = this;
            formControlUtil.InitControls((Control)bomOptionalEntry);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.luItemCode.Properties, this.myDBSetting);
            OverheadCodeLookupEditBuilder overheadbuilder = OverheadCodeLookupEditBuilder.Create();
            overheadbuilder.BuildLookupEdit(repleOvdOverheadCode, myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.BuildLookupEdit(this.luProjNo.Properties, this.myDBSetting);
            string sitemcode = "";

            itembomlookup = new Production.Tools.LookupEditBuilders.ItemBOMLookupEditBuilder();
            itembomlookup.BuildLookupEdit(this.repItemBOMCode, this.myDBSetting);
            itembomlookup.FilterItemCode(sitemcode);
            Tools.LookupEditBuilders.ItemDefBOMLookupEditBuilder itembuilder = new Tools.LookupEditBuilders.ItemDefBOMLookupEditBuilder();
            // if (this.myUseLookupEditToInputItemCode && this.repItemLkEdt_ItemCode.Columns.Count == 0)
            itembuilder.BuildLookupEdit(this.repItemLuedtItem, this.myDBSetting);
            // repItemLuedtItem.EditValueChanged += RepItemLuedtItem_EditValueChanged;
         
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repleItemCodeRep, this.myDBSetting);

            InitLookupItemMaterial();
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repItemLuedtItem, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repBOMItemLookUpEdit, this.myDBSetting);
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repleAPItemCode, this.myDBSetting);
            OverheadCodeLookupEditBuilder ovdlookup = OverheadCodeLookupEditBuilder.Create();
            ovdlookup.BuildLookupEdit(this.repleOvdOverheadCode, this.myDBSetting);
            if (luItemCode.EditValue == null)
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.BuildLookupEdit(this.luUOM.Properties, this.myDBSetting);
            else
                DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.FilterItemCode(luItemCode.EditValue.ToString());

            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.BuildLookupEdit(this.repleUOM, this.myDBSetting);

            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.BuildLookupEdit(this.repositoryItemluUOM, this.myDBSetting);

            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.BuildLookupEdit(this.repleBSUOM, this.myDBSetting);
            try
            {
                this.cbCostType.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(CostTypeOptions)));

            }
            catch { }
            string sbomcode = myEntity.BOMCode != null ? myEntity.BOMCode.ToString() : "";
            //ItemBOMLookupEditBuilderFilter BOMBuilder = new ItemBOMLookupEditBuilderFilter(sbomcode);
            //BOMBuilder.BuildLookupEdit(repItemBOMCode, myDBSetting);
            //DBRegistry dbRegistry = DBRegistry.Create(this.myDBSetting);
            //BaseRegistryID baseRegistryId1 = (BaseRegistryID)new QuantityDecimal();
            //this.txtQty.EditValue = (Decimal)dbRegistry.GetInt32((IRegistryID)baseRegistryId1);
            //FillItemInformation();
            if (cbCostType.SelectedIndex == 1)
                lblCostpercent.Visible = true;
            else
                lblCostpercent.Visible = false;

            cbCostType.Enabled = true;

            obj = myDBSetting.ExecuteScalar("select count(*) from RPA_WO a with(NOLOCK) inner join RPA_WOProduct b with(NOLOCK) on a.DocKey=b.DocKey where  ISNULL(a.Cancelled,'F')='F' and ISNULL(b.Cancelled,'F')='F' and ItemCode=?", (object)myEntity.ItemCode);
            if (obj != null && obj != DBNull.Value)
            {
                if(BCE.Data.Convert.ToDecimal(obj)>0)
                {
                   cbCostType.Enabled = false;
                }
               
            }
        }
        private void RepItemLuedtItem_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (repItemLuedtItem.GetDataSourceValue("BOMCode", e.NewIndex) != null)
            {
                string str = repItemLuedtItem.GetDataSourceValue("BOMCode", e.NewIndex).ToString();
                DataRow drDetail = gvItem.GetDataRow(gvItem.FocusedRowHandle);
                drDetail["BOMCode"] = str;
            }
        }

        private void ReconnectGridColumnEdit()
        {
            //this.colBOMCode.ColumnEdit = (RepositoryItem)this.repItemBOMCode;
            this.colItemCode.ColumnEdit = (RepositoryItem)this.repItemLuedtItem;
            this.colAPItemCode.ColumnEdit = (RepositoryItem)this.repleAPItemCode;
            this.colBSItemCode.ColumnEdit = (RepositoryItem)this.repBOMItemLookUpEdit;
            this.colOvdOverheadCode.ColumnEdit = (RepositoryItem)this.repleOvdOverheadCode;
            this.colUOM.ColumnEdit = (RepositoryItem)this.repositoryItemluUOM;
            this.colUOMRep.ColumnEdit = (RepositoryItem)this.repleUOM;
            this.colBSUOM.ColumnEdit = (RepositoryItem)this.repleBSUOM;
            this.colFOHCharge.ColumnEdit = (RepositoryItem)this.repositoryItemCheckEdit4;
        }
        private void EndCurrentEdit()
        {
            this.txtedtBOMCode.BindingManager.EndCurrentEdit();
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvItem);
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMBS);
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMAP);
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMOvd);
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMRep);
        }
        private void sBtnOK_Click(object sender, EventArgs e)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            this.EndCurrentEdit();
            //if (BCE.Data.Convert.ToBoolean(this.myEntity.HasChange))
            {
                if (this.Save(FormItemBOMEntry.SaveAction.Save))
                {
                    this.myEntity.DetailTableHvChange = false;
                    this.myEntity.BSTableHvChange = false;
                    this.myEntity.APTableHvChange = false;
                    this.myEntity.OVDTableHvChange = false;
                    this.myEntity.RepTableHvChange = false;
                    Cursor.Current = current;
                    if (this.myEntity.Action == ItemBOMCommand.Action.New)
                    {
                        DBSetting dbSetting = this.myDBSetting;
                        string docType = "";
                        long docKey = 0L;
                        long eventKey = 0L;
                        // ISSUE: variable of a boxed type
                        ItemBOMString local = ItemBOMString.CreatedItemBOMCode;
                        object[] objArray = new object[2];
                        int index1 = 0;
                        string loginUserId = this.myUserAuthentication.LoginUserID;
                        objArray[index1] = (object)loginUserId;
                        int index2 = 1;
                        string str = this.myEntity.BOMCode.ToString();
                        objArray[index2] = (object)str;
                        string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                        string detail = "";
                        Activity.Log(dbSetting, docType, docKey, eventKey, @string, detail);
                    }
                    else if (this.myEntity.Action == ItemBOMCommand.Action.Edit)
                    {
                        DBSetting dbSetting = this.myDBSetting;
                        string docType = "";
                        long docKey = 0L;
                        long eventKey = 0L;
                        // ISSUE: variable of a boxed type
                        ItemBOMString local = ItemBOMString.UpdatedItemBOMCode;
                        object[] objArray = new object[2];
                        int index1 = 0;
                        string loginUserId = this.myUserAuthentication.LoginUserID;
                        objArray[index1] = (object)loginUserId;
                        int index2 = 1;
                        string str = this.myEntity.BOMCode.ToString();
                        objArray[index2] = (object)str;
                        string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                        string detail = "";
                        Activity.Log(dbSetting, docType, docKey, eventKey, @string, detail);
                    }
                    this.Close();
                }
            }
            //else
            //{
            //    Cursor.Current = current;
            //    this.Close();
            //}
        }
        
        private bool Save(FormItemBOMEntry.SaveAction saveAction)
        {
            
            #region Register Program
            string sMacAddr = "";
            string sCompanyName = "";
            AssemblyInfoEntity info = new AssemblyInfoEntity();
            object obj2 = myDBSetting.ExecuteScalar("USE MASTER select srvname from sys.sysservers");
            if (obj2 != null && obj2 != DBNull.Value)
            {
                sMacAddr = obj2.ToString();

            }
            obj2 = myDBSetting.ExecuteScalar("select CompanyName from Profile");
            if (obj2 != null && obj2 != DBNull.Value)
            {

                sCompanyName = obj2.ToString();
            }
            if (!Production.GLib.G.IsRegistered("RPA_Settings", sMacAddr + sCompanyName + info.Product, sMacAddr))
            {
                DateTime dtLicenseDate = Production.GLib.G.GetLicenseDate("RPA_Settings", "Production");
                if (DateTime.Now > dtLicenseDate)
                {
                    System.Windows.Forms.MessageBox.Show("Cannot save... Please activate your software.", "Transaction block after " + dtLicenseDate.ToLongDateString(), MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
            #endregion
            bool flag = false;

            decimal dCost = myEntity.Cost;
            decimal percentDtl = 0;
            CostTypeOptions costypeoptions = (CostTypeOptions)cbCostType.SelectedIndex;
            if (costypeoptions == CostTypeOptions.Percentage)
            {
                foreach(DataRow drDetailBS in myEntity.GetValidBSDetailRows())
                {
                    percentDtl += BCE.Data.Convert.ToDecimal(drDetailBS["CostPercent"]);
                }
                if (percentDtl +dCost>100)
                {
                    AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_PercentBOM, (object)""));
                    this.txtedtBOMCode.Focus();
                    return false;
                }
                else if (percentDtl + dCost < 100)
                {
                    AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_PercentBOM, (object)""));
                    this.txtedtBOMCode.Focus();
                    return false;
                }

            }
            if (this.txtedtBOMCode.EditValue.ToString().Trim() == string.Empty)
            {
                AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_DetailBOM,(object)gvItem.RowCount));
                this.txtedtBOMCode.Focus();
                return false;
            }
            if (this.txtedtBOMCode.EditValue.ToString().Trim() == string.Empty)
            {
                AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_EnterBomOptionalCode, new object[0]));
                this.txtedtBOMCode.Focus();
                return false;
            }
            else
            {
                try
                {
                    this.myEntity.Save();
                    if (this.chkedtProceedNew.Visible && this.chkedtProceedNew.Checked)
                    {
                        ItemBOMEntity newItemBOMEntity = this.myEntity.ItemBOMCmd.NewItemBOM();
                        if (newItemBOMEntity != null)
                            this.SetNewItemBOM(newItemBOMEntity);
                    }
                    else
                        flag = true;
                }
                catch (DBConcurrencyException ex)
                {
                    if (this.myEntity.ItemBOMCmd.ViewEditItemBOM((long)BCE.Data.Convert.ToDBInt64(this.myEntity.DocKey), ItemBOMCommand.Action.Edit) == null)
                        AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.ShowMessage_DeleteSuccessfully, new object[0]));
                    else
                        AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_RecordWasModified, new object[0]));
                    return false;
                }
                catch (StandardApplicationException ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                    return false;
                }
                catch (PrimaryKeyException ex)
                {
                    AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.ErrorMessage_ItemBOMCodeExist, new object[0]));
                    this.txtedtBOMCode.Focus();
                    return false;
                }
                catch (DataAccessException ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                    return false;
                }
                return flag;
            }
        }

        private void SetNewItemBOM(ItemBOMEntity newItemBOMEntity)
        {
            if (this.myEntity != newItemBOMEntity)
            {
                this.myEntity = newItemBOMEntity;
                this.myDtblMaster = this.myEntity.MasterTable;
                this.myDtblDetail = this.myEntity.DetailTable;
                this.myDtblBSDetail = this.myEntity.BSDetailTable;
                this.myDtblAPDetail = this.myEntity.APDetailTable;
                this.myDtblOvdDetail = this.myEntity.OvdDetailTable;
                this.DataBinding();
                this.SetControlState();
                string sbomcode = myEntity.BOMCode != null ? myEntity.BOMCode.ToString() : "";

               // ItemBOMLookupEditBuilderFilter BOMBuilder = new ItemBOMLookupEditBuilderFilter(sbomcode);
               // BOMBuilder.BuildLookupEdit(repItemBOMCode, myDBSetting);
            }
        }

        private void sBtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ExecuteWithPauseUndo(MethodInvoker sDelegate)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
          
            if (this.myDetailRecordUndo != null)
                this.myDetailRecordUndo.PauseCapture();
            try
            {
                sDelegate();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
            finally
            {
                
                if (this.myDetailRecordUndo != null)
                {
                    this.myDetailRecordUndo.ResumeCapture();
                    if (!this.myDetailRecordUndo.IsPauseCapture())
                        this.myDetailRecordUndo.SaveState();
                }
                Cursor.Current = current;
            }
        }
        private void sbtnAddDTL_Click(object sender, EventArgs e)
        {
            //this.ExecuteWithPauseUndo(new MethodInvoker(this.DeleteSelectedRows), this.myDetailRecordUndo);
            this.ExecuteWithPauseUndo(new MethodInvoker(this.AddNewDetail));
           
        }
        private bool EndGridEdit()
        {
            if (this.myEntity == null)
                return false;
            else if (this.myEntity.Action == ItemBOMCommand.Action.View || this.gridCtlItem.FocusedView == null)
            {
                return true;
            }
            else
            {
                BaseView focusedView = this.gridCtlItem.FocusedView;
                try
                {
                    if (focusedView.IsEditing)
                    {
                        if (!focusedView.ValidateEditor())
                            return false;
                        else
                            focusedView.CloseEditor();
                    }
                    return focusedView.UpdateCurrentRow();
                }
                catch
                {
                    return false;
                }
            }
        }

        private void AddNewDetail()
        {
            if (this.EndGridEdit())
            {
                BCE.XtraUtils.GridViewUtils.UpdateData(this.gvItem);
                try
                {                   
                    
                    this.myEntity.AddDetail();
                    this.gridCtlItem.Focus();
                    //this.gvItem.MoveLast();
                    //this.gvItem.FocusedColumn = this.colItemCode;
                }
                catch (Exception ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                }
            }
        }

        private void AddNewRepDetail()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMRep);
            try
            {
                this.myEntity.AddRepDetail();
                this.gridCtlIBOMRep.Focus();
                this.gvBOMRep.MoveLast();
                this.gvBOMRep.FocusedColumn = this.colItemCode2;
            }
            catch (Exception ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
            }
        }

        private void AddNewBSDetail()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMBS);
            try
            {
                this.myEntity.AddBSDetail();
                this.gridCtlIBOMBS.Focus();
                this.gvBOMBS.MoveLast();
                this.gvBOMBS.FocusedColumn = this.colBSItemCode;
            }
            catch (Exception ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
            }
        }
        private void AddNewAPDetail()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMAP);
            try
            {
                this.myEntity.AddAPDetail();
                this.gridCtlIBOMAP.Focus();
                this.gvBOMAP.MoveLast();
                this.gvBOMAP.FocusedColumn = this.colAPItemCode;
            }
            catch (Exception ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
            }
        }
        private void AddNewOvdDetail()
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMOvd);
            try
            {
                this.myEntity.AddOvdDetail();
                this.gridCtlIBOMOvd.Focus();
                this.gvBOMOvd.MoveLast();
                this.gvBOMOvd.FocusedColumn = this.colOvdOverheadCode;
            }
            catch (Exception ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
            }
        }
        private void InitUndoManager()
        {
            DataTable detailTable = this.myEntity.DetailTable;
            DataTable BSDetailTable = this.myEntity.BSDetailTable;
            DataTable APDetailTable = this.myEntity.APDetailTable;
            DataTable OvdDetailTable = this.myEntity.OvdDetailTable;
            DataTable RepDetailTable = this.myEntity.RepDetailTable;

            this.myDetailRecordUndo = new UndoManager(detailTable, false);
            this.myDetailRecordUndo.ExcludeFields.Add((object)"TotalCost");
            this.myDetailRecordUndo.CanUndoChanged += new EventHandler(this.myDetailRecordUndo_CanUndoChanged);
            this.sbtnUndo.Enabled = this.myDetailRecordUndo.CanUndo;
           

            this.myBSDetailRecordUndo = new UndoManager(BSDetailTable, false);
            //this.myBSDetailRecordUndo.ExcludeFields.Add((object)"SubTotal");
            this.myBSDetailRecordUndo.CanUndoChanged += new EventHandler(this.myBSDetailRecordUndo_CanUndoChanged);
            this.sbtnUndoBSDtl.Enabled = this.myBSDetailRecordUndo.CanUndo;

            this.myAPDetailRecordUndo = new UndoManager(APDetailTable, false);
            //this.myAPDetailRecordUndo.ExcludeFields.Add((object)"SubTotal");
            this.myAPDetailRecordUndo.CanUndoChanged += new EventHandler(this.myAPDetailRecordUndo_CanUndoChanged);
            this.sbtnUndoAPDtl.Enabled = this.myAPDetailRecordUndo.CanUndo;

            this.myOvdDetailRecordUndo = new UndoManager(OvdDetailTable, false);
            //this.myOvdDetailRecordUndo.ExcludeFields.Add((object)"SubTotal");
            this.myOvdDetailRecordUndo.CanUndoChanged += new EventHandler(this.myOvdDetailRecordUndo_CanUndoChanged);
            this.sbtnUndoOvdDtl.Enabled = this.myOvdDetailRecordUndo.CanUndo;

            this.myRepDetailRecordUndo = new UndoManager(RepDetailTable, false);
            //this.myRepDetailRecordUndo.ExcludeFields.Add((object)"SubTotal");
            this.myRepDetailRecordUndo.CanUndoChanged += new EventHandler(this.myRepDetailRecordUndo_CanUndoChanged);
            this.sbtnUndoRep.Enabled = this.myRepDetailRecordUndo.CanUndo;

            this.myDetailRecordUndo.SaveState();
            this.myDetailRecordUndo.KeepCurrentChanges();
        }

        private void ExecuteWithPauseUndo(MethodInvoker sDelegate, UndoManager myUndoManager)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;



            if (this.myDetailRecordUndo != null)
                this.myDetailRecordUndo.PauseCapture();
            try
            {
                sDelegate();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
            finally
            {
                myUndoManager.ResumeCapture();
                myUndoManager.SaveState();
                Cursor.Current = current;
                if (this.myDetailRecordUndo != null)
                {
                    this.myDetailRecordUndo.ResumeCapture();
                    if (!this.myDetailRecordUndo.IsPauseCapture())
                        this.myDetailRecordUndo.SaveState();
                }
                Cursor.Current = current;
            }
        }
        private void ExecuteWithPauseUndoBS(MethodInvoker sDelegate, UndoManager myUndoManager)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            this.myBSDetailRecordUndo.PauseCapture();
            try
            {
                sDelegate();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
            finally
            {
                myUndoManager.ResumeCapture();
                myUndoManager.SaveState();
                Cursor.Current = current;
            }
        }

        private void ExecuteWithPauseUndoOvd(MethodInvoker sDelegate, UndoManager myUndoManager)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            this.myOvdDetailRecordUndo.PauseCapture();
            try
            {
                sDelegate();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
            finally
            {
                myUndoManager.ResumeCapture();
                myUndoManager.SaveState();
                Cursor.Current = current;
            }
        }

        private void ExecuteWithPauseUndoAP(MethodInvoker sDelegate, UndoManager myUndoManager)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            this.myAPDetailRecordUndo.PauseCapture();
            try
            {
                sDelegate();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
            finally
            {
                myUndoManager.ResumeCapture();
                myUndoManager.SaveState();
                Cursor.Current = current;
            }
        }

        private void ExecuteWithPauseUndoRep(MethodInvoker sDelegate, UndoManager myUndoManager)
        {
            Cursor current = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            this.myRepDetailRecordUndo.PauseCapture();
            try
            {
                sDelegate();
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
            finally
            {
                myUndoManager.ResumeCapture();
                myUndoManager.SaveState();
                Cursor.Current = current;
            }
        }

        private void myDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndo.Enabled = this.myDetailRecordUndo.CanUndo;
        }

        private void DeleteSelectedRows()
        {
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvItem);
        }

        private void DeleteSelectedBSRows()
        {
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvBOMBS);
        }

        private void DeleteSelectedAPRows()
        {
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvBOMAP);
        }

        private void DeleteSelectedOvdRows()
        {
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvBOMOvd);
        }

        private void DeleteSelectedRepRows()
        {
            BCE.AutoCount.XtraUtils.GridViewUtils.DeleteSelectedRows(this.gvBOMRep);
        }

        private void sbtnDeleteDTL_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndo(new MethodInvoker(this.DeleteSelectedRows), this.myDetailRecordUndo);
        }

        private void sbtnUndo_Click(object sender, EventArgs e)
        {
            if (this.myEntity.Action != ItemBOMCommand.Action.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gvItem.BeginUpdate();
                    this.myEntity.BeginLoadDetailData();
                    this.myDetailRecordUndo.Undo();
                    this.myEntity.EndLoadDetailData();
                    this.gvItem.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void repItemLuedtItem_EditValueChanged(object sender, EventArgs e)
        {
                DevExpress.XtraEditors.LookUpEdit editor = sender as DevExpress.XtraEditors.LookUpEdit;
            object value = editor.GetColumnValue("BOMCode");
            DataRow drDetail = gvItem.GetDataRow(gvItem.FocusedRowHandle);
            drDetail["BOMCode"] = value;

        }

        private void sbtnAddDTL1_Click(object sender, EventArgs e)
        {
            this.AddNewBSDetail();
            InitLookupItemMaterial();
        }

        private void repBOMItemLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            BCE.XtraUtils.GridViewUtils.UpdateData(this.gvBOMBS);
        }

        private void myBSDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndoBSDtl.Enabled = this.myBSDetailRecordUndo.CanUndo;
        }

        private void myAPDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndoAPDtl.Enabled = this.myAPDetailRecordUndo.CanUndo;
        }

        private void myOvdDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndoOvdDtl.Enabled = this.myOvdDetailRecordUndo.CanUndo;
        }

        private void myRepDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
        {
            this.sbtnUndoRep.Enabled = this.myRepDetailRecordUndo.CanUndo;
        }

        private void sbtnDeleteDTL1_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndoBS(new MethodInvoker(this.DeleteSelectedBSRows), this.myBSDetailRecordUndo);
            InitLookupItemMaterial();
        }

        private void sbtnUndo1_Click(object sender, EventArgs e)
        {
            if (this.myEntity.Action != ItemBOMCommand.Action.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gvBOMBS.BeginUpdate();
                    this.myEntity.BeginLoadBSDetailData();
                    this.myBSDetailRecordUndo.Undo();
                    this.myEntity.EndLoadBSDetailData();
                    this.gvBOMBS.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void FormItemBOMEntry_KeyDown(object sender, KeyEventArgs e)
        {
           if (e.Shift && e.Control)
            {
                //if (e.KeyCode == (Keys)68)
                //{
                //    if (this.xtraTabCtlItemBOM.SelectedTabPage != this.xtraTabDetails)
                //        this.xtraTabCtlItemBOM.SelectedTabPage = this.xtraTabDetails;
                //    this.gridCtlItem.Focus();
                //    e.Handled = true;
                //}
                //else if (e.KeyCode == (Keys)65)
                //{
                //    if (this.xtraTabCtlItemBOM.SelectedTabPage != this.xtraTabBOMItem)
                //        this.xtraTabCtlItemBOM.SelectedTabPage = this.xtraTabBOMItem;
                //    this.gridCtlIBOMBS.Focus();
                //    e.Handled = true;
                //}
                //else if (e.KeyCode == (Keys)62)
                //{
                //    if (this.xtraTabCtlItemBOM.SelectedTabPage != this.xtraTabPageAP)
                //        this.xtraTabCtlItemBOM.SelectedTabPage = this.xtraTabPageAP;
                //    this.gridCtlIBOMAP.Focus();
                //    e.Handled = true;
                //}
                //else if (e.KeyCode == (Keys)59)
                //{
                //    if (this.xtraTabCtlItemBOM.SelectedTabPage != this.xtraTabPageOvd)
                //        this.xtraTabCtlItemBOM.SelectedTabPage = this.xtraTabPageOvd;
                //    this.gridCtlIBOMOvd.Focus();
                //    e.Handled = true;
                //}

            }
            else if (this.myEntity.Action != ItemBOMCommand.Action.View)            
            {
                if (!e.Shift && !e.Control && !e.Alt)
                {
                    if (this.xtraTabCtlItemBOM.SelectedTabPage == this.xtraTabDetails)
                    {
                        if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            
                            this.sbtnAddDTL.PerformClick();
                            this.gridCtlItem.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            this.sbtnDeleteDTL.PerformClick();
                            this.gridCtlItem.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)90)
                        {
                            this.sbtnUndo.PerformClick();
                            this.gridCtlItem.Focus();
                            e.Handled = true;
                        }
                    }
                    else if (this.xtraTabCtlItemBOM.SelectedTabPage == this.xtraTabBOMItem)
                    {
                        if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            this.sbtnAddBSDTL.PerformClick();
                            this.gridCtlItem.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            this.sbtnDeleteBSDTL.PerformClick();
                            this.gridCtlItem.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)90)
                        {
                            this.sbtnUndoBSDtl.PerformClick();
                            this.gridCtlItem.Focus();
                            e.Handled = true;
                        }
                    }
                    else if (this.xtraTabCtlItemBOM.SelectedTabPage == this.xtraTabPageAP)
                    {
                        if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            this.sbtnAddAPDtl.PerformClick();
                            this.gridCtlIBOMAP.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            this.sbtnDeleteAPDtl.PerformClick();
                            this.gridCtlIBOMAP.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)90)
                        {
                            this.sbtnUndoAPDtl.PerformClick();
                            this.gridCtlIBOMAP.Focus();
                            e.Handled = true;
                        }
                    }
                    else if (this.xtraTabCtlItemBOM.SelectedTabPage == this.xtraTabPageOvd)
                    {
                        if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            this.sbtnAddOvdDtl.PerformClick();
                            this.gridCtlIBOMOvd.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space))
                        {
                            this.sbtnDeleteOvdDtl.PerformClick();
                            this.gridCtlIBOMOvd.Focus();
                            e.Handled = true;
                        }
                        else if (e.KeyCode == (Keys)90)
                        {
                            this.sbtnUndoOvdDtl.PerformClick();
                            this.gridCtlIBOMOvd.Focus();
                            e.Handled = true;
                        }
                    }
                }
            }
            else if (e.KeyCode == (Keys)114 && this.sBtnOK.Enabled)
            {
                this.sBtnOK.PerformClick();
                e.Handled = true;
            }
            else if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
            {
                //if (this.xtraTabCtlItemBOM.SelectedTabPage == this.xtraTabDetails)
                //{
                //    this.sbtnAddDTL.PerformClick();
                //    this.gridCtlItem.Focus();
                //    e.Handled = true;
                //}
                //else if (this.xtraTabCtlItemBOM.SelectedTabPage == this.xtraTabBOMItem)
                //{
                //    this.sbtnAddBSDTL.PerformClick();
                //    this.gridCtlIBOMBS.Focus();
                //    e.Handled = true;
                //}
                //else if (this.xtraTabCtlItemBOM.SelectedTabPage == this.xtraTabPageAP)
                //{
                //    this.sbtnAddAPDtl.PerformClick();
                //    this.gridCtlIBOMAP.Focus();
                //    e.Handled = true;
                //}
                //else if (this.xtraTabCtlItemBOM.SelectedTabPage == this.xtraTabPageOvd)
                //{
                //    this.sbtnAddOvdDtl.PerformClick();
                //    this.gridCtlIBOMOvd.Focus();
                //    e.Handled = true;
                //}
            }
        }
        private void FormItemBOMEntry_Closing(object sender, CancelEventArgs e)
        {
            this.EndCurrentEdit();
            if (BCE.Data.Convert.ToBoolean(this.myEntity.HasChange) && this.myEntity.Action != ItemBOMCommand.Action.New)
            {
                switch (AppMessage.ShowConfirmSaveChangesMessage())
                {
                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                    case DialogResult.Yes:
                        this.Save(FormItemBOMEntry.SaveAction.Save);
                        break;
                }
            }
        }

        private void FormItemBOMEntry_Load(object sender, EventArgs e)
        {
            // this.xtraTabCtlItemBOM.SelectedTabPage = this.xtraTabBOMItem;
            this.xtraTabCtlItemBOM.SelectedTabPage = this.xtraTabDetails;
            this.txtedtBOMCode.Focus();

            this.SetModuleFeature();
            this.myGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gvItem);
            new CustomizeGridLayout(this.myDBSetting, this.Name+"1", this.gvBOMOvd);
            new CustomizeGridLayout(this.myDBSetting, this.Name+"2", this.gvBOMBS);

            ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.AddListener(new ModuleControllerListenerDelegate(this.InvokeModuleFeature), (Component)this);
            this.myUserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.InvokeAccessRightChanged), (Component)this);
        }

        private void InvokeModuleFeature(ModuleController controller)
        {
            this.BeginInvoke((Delegate)new MethodInvoker(this.SetModuleFeature));
        }

        private void InvokeAccessRightChanged()
        {
            this.SetModuleFeature();
        }

        private void SetModuleFeature()
        {
            //bool flag = this.myUserAuthentication.AccessRight.IsAccessible("SYS_BHV_VIEW_COST") && ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.BOM.Enable;
            //this.colUnitCost.Visible = flag;
            //this.colUnitCost.OptionsColumn.ShowInCustomizationForm = flag;
            //this.colUnitCost.Visible = flag;
            //this.colUnitCost.OptionsColumn.ShowInCustomizationForm = flag;

            if (this.myGridLayout != null)
                this.myGridLayout.SaveDefaultLayout();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormItemBOMEntry));
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.label8 = new System.Windows.Forms.Label();
            this.luProjNo = new DevExpress.XtraEditors.LookUpEdit();
            this.lblLocation = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.luUOM = new DevExpress.XtraEditors.LookUpEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.lblCostpercent = new System.Windows.Forms.Label();
            this.spCost = new DevExpress.XtraEditors.SpinEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbCostType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtQty = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.luItemCode = new DevExpress.XtraEditors.LookUpEdit();
            this.chkedtIsActive = new DevExpress.XtraEditors.CheckEdit();
            this.txtedtDescription2 = new DevExpress.XtraEditors.TextEdit();
            this.txtedtDescription = new DevExpress.XtraEditors.TextEdit();
            this.txtedtBOMCode = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelMiddle = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabCtlItemBOM = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabBOMItem = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlIBOMBS = new DevExpress.XtraGrid.GridControl();
            this.gvBOMBS = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBSItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repBOMItemLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colBOMDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBSUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleBSUOM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colBSRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBSCostPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColBSCostMax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFOHCharge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repItemBSItemCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnUndoBSDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddBSDTL = new DevExpress.XtraEditors.SimpleButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.sbtnDeleteBSDTL = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabDetails = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlItem = new DevExpress.XtraGrid.GridControl();
            this.gvItem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDocKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDtlKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOMCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemBOMCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemLuedtItem = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemluUOM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemBOMCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductRatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repBOMCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelGridEdit = new DevExpress.XtraEditors.PanelControl();
            this.sbtnUndo = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddDTL = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDeleteDTL = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageAP = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlIBOMAP = new DevExpress.XtraGrid.GridControl();
            this.gvBOMAP = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleAPItemCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPCostPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnUndoAPDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddAPDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDeleteAPDtl = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageOvd = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlIBOMOvd = new DevExpress.XtraGrid.GridControl();
            this.gvBOMOvd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOvdOverheadCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleOvdOverheadCode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOvdDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOvdAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnUndoOvdDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddOvdDtl = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDeleteOvdDtl = new DevExpress.XtraEditors.SimpleButton();
            this.tabItemReplacement = new DevExpress.XtraTab.XtraTabPage();
            this.gridCtlIBOMRep = new DevExpress.XtraGrid.GridControl();
            this.gvBOMRep = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCodeRep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleItemCodeRep = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colItemCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleItemCode2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDescriptionRep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUOMRep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repleUOM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colQtyRep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductRatioRep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnUndoRep = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddRep = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDeleteRep = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEditUOM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelBottom = new DevExpress.XtraEditors.PanelControl();
            this.chkedtProceedNew = new DevExpress.XtraEditors.CheckEdit();
            this.sBtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabCtlPriceSchemeII = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPriceSchemeI = new DevExpress.XtraTab.XtraTabPage();
            this.tabCtlPriceI = new DevExpress.XtraTab.XtraTabControl();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnCopyFrom = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnCopyTo = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnCreateProposeCont = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luProjNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luUOM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCostType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luItemCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtIsActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtedtDescription2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtedtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtedtBOMCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelMiddle)).BeginInit();
            this.panelMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabCtlItemBOM)).BeginInit();
            this.xtraTabCtlItemBOM.SuspendLayout();
            this.xtraTabBOMItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBOMItemLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleBSUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemBSItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.xtraTabDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemBOMCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLuedtItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemluUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBOMCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGridEdit)).BeginInit();
            this.panelGridEdit.SuspendLayout();
            this.xtraTabPageAP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleAPItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.xtraTabPageOvd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMOvd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMOvd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleOvdOverheadCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.tabItemReplacement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMRep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMRep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleItemCodeRep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleItemCode2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelBottom)).BeginInit();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtProceedNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabCtlPriceI)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelTop.Controls.Add(this.label8);
            this.panelTop.Controls.Add(this.luProjNo);
            this.panelTop.Controls.Add(this.lblLocation);
            this.panelTop.Controls.Add(this.label7);
            this.panelTop.Controls.Add(this.luUOM);
            this.panelTop.Controls.Add(this.lblCostpercent);
            this.panelTop.Controls.Add(this.spCost);
            this.panelTop.Controls.Add(this.label6);
            this.panelTop.Controls.Add(this.label4);
            this.panelTop.Controls.Add(this.cbCostType);
            this.panelTop.Controls.Add(this.label3);
            this.panelTop.Controls.Add(this.txtQty);
            this.panelTop.Controls.Add(this.label2);
            this.panelTop.Controls.Add(this.luItemCode);
            this.panelTop.Controls.Add(this.chkedtIsActive);
            this.panelTop.Controls.Add(this.txtedtDescription2);
            this.panelTop.Controls.Add(this.txtedtDescription);
            this.panelTop.Controls.Add(this.txtedtBOMCode);
            this.panelTop.Controls.Add(this.label5);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(853, 182);
            this.panelTop.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(355, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(254, 17);
            this.label8.TabIndex = 20;
            this.label8.Text = "* Mandatory using small UOM";
            // 
            // luProjNo
            // 
            this.luProjNo.Location = new System.Drawing.Point(682, 101);
            this.luProjNo.Name = "luProjNo";
            this.luProjNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luProjNo.Size = new System.Drawing.Size(118, 20);
            this.luProjNo.TabIndex = 6;
            // 
            // lblLocation
            // 
            this.lblLocation.BackColor = System.Drawing.Color.Transparent;
            this.lblLocation.Location = new System.Drawing.Point(615, 104);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(61, 13);
            this.lblLocation.TabIndex = 19;
            this.lblLocation.Text = "ProjNo";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(24, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 15);
            this.label7.TabIndex = 17;
            this.label7.Text = "Description";
            // 
            // luUOM
            // 
            this.luUOM.Location = new System.Drawing.Point(257, 101);
            this.luUOM.MenuManager = this.barManager1;
            this.luUOM.Name = "luUOM";
            this.luUOM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luUOM.Size = new System.Drawing.Size(92, 20);
            this.luUOM.TabIndex = 5;
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 5;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(853, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 598);
            this.barDockControlBottom.Size = new System.Drawing.Size(853, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 598);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(853, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 598);
            // 
            // lblCostpercent
            // 
            this.lblCostpercent.Location = new System.Drawing.Point(254, 147);
            this.lblCostpercent.Name = "lblCostpercent";
            this.lblCostpercent.Size = new System.Drawing.Size(68, 19);
            this.lblCostpercent.TabIndex = 15;
            this.lblCostpercent.Text = "%";
            this.lblCostpercent.Click += new System.EventHandler(this.label7_Click);
            // 
            // spCost
            // 
            this.spCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spCost.Location = new System.Drawing.Point(115, 144);
            this.spCost.MenuManager = this.barManager1;
            this.spCost.Name = "spCost";
            this.spCost.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, false, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.spCost.Properties.DisplayFormat.FormatString = "#,0.00";
            this.spCost.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spCost.Properties.EditFormat.FormatString = "#,0.00";
            this.spCost.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spCost.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spCost.Size = new System.Drawing.Size(137, 20);
            this.spCost.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(24, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 19);
            this.label6.TabIndex = 14;
            this.label6.Text = "Unit Cost";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(24, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 23);
            this.label4.TabIndex = 13;
            this.label4.Text = "Cost Type";
            // 
            // cbCostType
            // 
            this.cbCostType.Location = new System.Drawing.Point(115, 123);
            this.cbCostType.MenuManager = this.barManager1;
            this.cbCostType.Name = "cbCostType";
            this.cbCostType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCostType.Size = new System.Drawing.Size(137, 20);
            this.cbCostType.TabIndex = 7;
            this.cbCostType.SelectedIndexChanged += new System.EventHandler(this.cbCostType_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(24, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 23);
            this.label3.TabIndex = 10;
            this.label3.Text = "Qty";
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(115, 101);
            this.txtQty.Name = "txtQty";
            this.txtQty.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtQty.Size = new System.Drawing.Size(137, 20);
            this.txtQty.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(24, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 19);
            this.label2.TabIndex = 7;
            this.label2.Text = "Item Code";
            // 
            // luItemCode
            // 
            this.luItemCode.Location = new System.Drawing.Point(115, 58);
            this.luItemCode.MenuManager = this.barManager1;
            this.luItemCode.Name = "luItemCode";
            this.luItemCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luItemCode.Size = new System.Drawing.Size(234, 20);
            this.luItemCode.TabIndex = 2;
            this.luItemCode.EditValueChanged += new System.EventHandler(this.luItemCode_EditValueChanged);
            // 
            // chkedtIsActive
            // 
            this.chkedtIsActive.Location = new System.Drawing.Point(755, 6);
            this.chkedtIsActive.Name = "chkedtIsActive";
            this.chkedtIsActive.Properties.Caption = "Is Active";
            this.chkedtIsActive.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.chkedtIsActive.Properties.ValueChecked = "T";
            this.chkedtIsActive.Properties.ValueUnchecked = "F";
            this.chkedtIsActive.Size = new System.Drawing.Size(75, 19);
            this.chkedtIsActive.TabIndex = 9;
            // 
            // txtedtDescription2
            // 
            this.txtedtDescription2.Location = new System.Drawing.Point(115, 36);
            this.txtedtDescription2.Name = "txtedtDescription2";
            this.txtedtDescription2.Size = new System.Drawing.Size(685, 20);
            this.txtedtDescription2.TabIndex = 1;
            // 
            // txtedtDescription
            // 
            this.txtedtDescription.Location = new System.Drawing.Point(115, 79);
            this.txtedtDescription.Name = "txtedtDescription";
            this.txtedtDescription.Size = new System.Drawing.Size(685, 20);
            this.txtedtDescription.TabIndex = 3;
            // 
            // txtedtBOMCode
            // 
            this.txtedtBOMCode.Location = new System.Drawing.Point(115, 13);
            this.txtedtBOMCode.Name = "txtedtBOMCode";
            this.txtedtBOMCode.Size = new System.Drawing.Size(234, 20);
            this.txtedtBOMCode.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(24, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Description";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "BOM Code";
            // 
            // panelMiddle
            // 
            this.panelMiddle.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelMiddle.Controls.Add(this.xtraTabCtlItemBOM);
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMiddle.Location = new System.Drawing.Point(0, 182);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(853, 376);
            this.panelMiddle.TabIndex = 0;
            // 
            // xtraTabCtlItemBOM
            // 
            this.xtraTabCtlItemBOM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabCtlItemBOM.Location = new System.Drawing.Point(0, 0);
            this.xtraTabCtlItemBOM.Name = "xtraTabCtlItemBOM";
            this.xtraTabCtlItemBOM.SelectedTabPage = this.xtraTabBOMItem;
            this.xtraTabCtlItemBOM.Size = new System.Drawing.Size(853, 376);
            this.xtraTabCtlItemBOM.TabIndex = 0;
            this.xtraTabCtlItemBOM.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabDetails,
            this.xtraTabBOMItem,
            this.xtraTabPageAP,
            this.xtraTabPageOvd,
            this.tabItemReplacement});
            this.xtraTabCtlItemBOM.TabStop = false;
            // 
            // xtraTabBOMItem
            // 
            this.xtraTabBOMItem.Controls.Add(this.gridCtlIBOMBS);
            this.xtraTabBOMItem.Controls.Add(this.panelControl1);
            this.xtraTabBOMItem.Name = "xtraTabBOMItem";
            this.xtraTabBOMItem.Size = new System.Drawing.Size(847, 348);
            this.xtraTabBOMItem.Text = "Item Waste/ Not Good";
            // 
            // gridCtlIBOMBS
            // 
            this.gridCtlIBOMBS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlIBOMBS.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlIBOMBS.Location = new System.Drawing.Point(0, 33);
            this.gridCtlIBOMBS.MainView = this.gvBOMBS;
            this.gridCtlIBOMBS.Name = "gridCtlIBOMBS";
            this.gridCtlIBOMBS.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repItemBSItemCode,
            this.repBOMItemLookUpEdit,
            this.repleBSUOM,
            this.repositoryItemCheckEdit4});
            this.gridCtlIBOMBS.Size = new System.Drawing.Size(847, 315);
            this.gridCtlIBOMBS.TabIndex = 0;
            this.gridCtlIBOMBS.TabStop = false;
            this.gridCtlIBOMBS.UseEmbeddedNavigator = true;
            this.gridCtlIBOMBS.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBOMBS});
            // 
            // gvBOMBS
            // 
            this.gvBOMBS.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBSItemCode,
            this.colBOMDesc,
            this.colBSUOM,
            this.colBSRate,
            this.colBSCostPercent,
            this.ColBSCostMax,
            this.colFOHCharge});
            this.gvBOMBS.GridControl = this.gridCtlIBOMBS;
            this.gvBOMBS.Name = "gvBOMBS";
            this.gvBOMBS.ColumnChanged += new System.EventHandler(this.gvBOMBS_ColumnChanged);
            this.gvBOMBS.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvBOMBS_FocusedRowChanged);
            this.gvBOMBS.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gvBOMBS_FocusedColumnChanged);
            // 
            // colBSItemCode
            // 
            this.colBSItemCode.Caption = "Item Code";
            this.colBSItemCode.ColumnEdit = this.repBOMItemLookUpEdit;
            this.colBSItemCode.FieldName = "ItemCode";
            this.colBSItemCode.Name = "colBSItemCode";
            this.colBSItemCode.Visible = true;
            this.colBSItemCode.VisibleIndex = 0;
            this.colBSItemCode.Width = 147;
            // 
            // repBOMItemLookUpEdit
            // 
            this.repBOMItemLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repBOMItemLookUpEdit.Name = "repBOMItemLookUpEdit";
            this.repBOMItemLookUpEdit.EditValueChanged += new System.EventHandler(this.repBOMItemLookUpEdit_EditValueChanged);
            // 
            // colBOMDesc
            // 
            this.colBOMDesc.Caption = "Description";
            this.colBOMDesc.FieldName = "Description";
            this.colBOMDesc.Name = "colBOMDesc";
            this.colBOMDesc.Visible = true;
            this.colBOMDesc.VisibleIndex = 1;
            this.colBOMDesc.Width = 356;
            // 
            // colBSUOM
            // 
            this.colBSUOM.Caption = "UOM";
            this.colBSUOM.ColumnEdit = this.repleBSUOM;
            this.colBSUOM.FieldName = "UOM";
            this.colBSUOM.Name = "colBSUOM";
            this.colBSUOM.Visible = true;
            this.colBSUOM.VisibleIndex = 2;
            // 
            // repleBSUOM
            // 
            this.repleBSUOM.AutoHeight = false;
            this.repleBSUOM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleBSUOM.Name = "repleBSUOM";
            // 
            // colBSRate
            // 
            this.colBSRate.Caption = "Rate";
            this.colBSRate.FieldName = "Rate";
            this.colBSRate.Name = "colBSRate";
            this.colBSRate.OptionsColumn.AllowEdit = false;
            this.colBSRate.Visible = true;
            this.colBSRate.VisibleIndex = 3;
            this.colBSRate.Width = 66;
            // 
            // colBSCostPercent
            // 
            this.colBSCostPercent.Caption = "Cost (%)";
            this.colBSCostPercent.FieldName = "CostPercent";
            this.colBSCostPercent.Name = "colBSCostPercent";
            this.colBSCostPercent.Visible = true;
            this.colBSCostPercent.VisibleIndex = 4;
            this.colBSCostPercent.Width = 124;
            // 
            // ColBSCostMax
            // 
            this.ColBSCostMax.Caption = "CostMax";
            this.ColBSCostMax.FieldName = "CostMax";
            this.ColBSCostMax.Name = "ColBSCostMax";
            this.ColBSCostMax.Visible = true;
            this.ColBSCostMax.VisibleIndex = 5;
            this.ColBSCostMax.Width = 190;
            // 
            // colFOHCharge
            // 
            this.colFOHCharge.Caption = "FOHCharge";
            this.colFOHCharge.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colFOHCharge.FieldName = "FOHCharge";
            this.colFOHCharge.Name = "colFOHCharge";
            this.colFOHCharge.Visible = true;
            this.colFOHCharge.VisibleIndex = 6;
            this.colFOHCharge.Width = 78;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = "T";
            this.repositoryItemCheckEdit4.ValueUnchecked = "F";
            // 
            // repItemBSItemCode
            // 
            this.repItemBSItemCode.AutoHeight = false;
            this.repItemBSItemCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemBSItemCode.Name = "repItemBSItemCode";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.sbtnUndoBSDtl);
            this.panelControl1.Controls.Add(this.sbtnAddBSDTL);
            this.panelControl1.Controls.Add(this.sbtnDeleteBSDTL);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(847, 33);
            this.panelControl1.TabIndex = 1;
            // 
            // sbtnUndoBSDtl
            // 
            this.sbtnUndoBSDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndoBSDtl.Image")));
            this.sbtnUndoBSDtl.ImageIndex = 3;
            this.sbtnUndoBSDtl.Location = new System.Drawing.Point(84, 4);
            this.sbtnUndoBSDtl.Name = "sbtnUndoBSDtl";
            this.sbtnUndoBSDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnUndoBSDtl.TabIndex = 15;
            this.sbtnUndoBSDtl.TabStop = false;
            this.sbtnUndoBSDtl.Text = "Undo";
            this.sbtnUndoBSDtl.Click += new System.EventHandler(this.sbtnUndo1_Click);
            // 
            // sbtnAddBSDTL
            // 
            this.sbtnAddBSDTL.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddBSDTL.Image")));
            this.sbtnAddBSDTL.ImageIndex = 0;
            this.sbtnAddBSDTL.ImageList = this.imageList1;
            this.sbtnAddBSDTL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddBSDTL.Location = new System.Drawing.Point(20, 4);
            this.sbtnAddBSDTL.Name = "sbtnAddBSDTL";
            this.sbtnAddBSDTL.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddBSDTL.TabIndex = 13;
            this.sbtnAddBSDTL.TabStop = false;
            this.sbtnAddBSDTL.Click += new System.EventHandler(this.sbtnAddDTL1_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // sbtnDeleteBSDTL
            // 
            this.sbtnDeleteBSDTL.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeleteBSDTL.Image")));
            this.sbtnDeleteBSDTL.ImageIndex = 1;
            this.sbtnDeleteBSDTL.ImageList = this.imageList1;
            this.sbtnDeleteBSDTL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeleteBSDTL.Location = new System.Drawing.Point(52, 4);
            this.sbtnDeleteBSDTL.Name = "sbtnDeleteBSDTL";
            this.sbtnDeleteBSDTL.Size = new System.Drawing.Size(26, 23);
            this.sbtnDeleteBSDTL.TabIndex = 14;
            this.sbtnDeleteBSDTL.TabStop = false;
            this.sbtnDeleteBSDTL.Click += new System.EventHandler(this.sbtnDeleteDTL1_Click);
            // 
            // xtraTabDetails
            // 
            this.xtraTabDetails.Controls.Add(this.gridCtlItem);
            this.xtraTabDetails.Controls.Add(this.panelGridEdit);
            this.xtraTabDetails.Name = "xtraTabDetails";
            this.xtraTabDetails.Size = new System.Drawing.Size(847, 348);
            this.xtraTabDetails.Text = "Raw Materials";
            // 
            // gridCtlItem
            // 
            this.gridCtlItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlItem.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlItem.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlItem.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlItem.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlItem.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlItem.Location = new System.Drawing.Point(0, 32);
            this.gridCtlItem.MainView = this.gvItem;
            this.gridCtlItem.Name = "gridCtlItem";
            this.gridCtlItem.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemluUOM,
            this.repItemLuedtItem,
            this.repItemBOMCode,
            this.repBOMCode});
            this.gridCtlItem.Size = new System.Drawing.Size(847, 316);
            this.gridCtlItem.TabIndex = 1;
            this.gridCtlItem.UseEmbeddedNavigator = true;
            this.gridCtlItem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvItem});
            this.gridCtlItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridCtlItem_KeyDown);
            // 
            // gvItem
            // 
            this.gvItem.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDocKey,
            this.colDtlKey,
            this.colBOMCode,
            this.colItemCode,
            this.colDesc,
            this.colUOM,
            this.colRate,
            this.colQty,
            this.colUnitCost,
            this.colItemBOMCode,
            this.colProductRatio,
            this.colTotalCost});
            this.gvItem.GridControl = this.gridCtlItem;
            this.gvItem.Name = "gvItem";
            this.gvItem.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvItem_CustomRowCellEdit);
            this.gvItem.CustomRowCellEditForEditing += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvItem_CustomRowCellEditForEditing);
            this.gvItem.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvItem_FocusedRowChanged);
            this.gvItem.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gvItem_FocusedColumnChanged);
            this.gvItem.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvItem_CellValueChanged);
            this.gvItem.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gvItem_CustomUnboundColumnData);
            this.gvItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvItem_KeyDown);
            // 
            // colDocKey
            // 
            this.colDocKey.Caption = "DocKey";
            this.colDocKey.FieldName = "DocKey";
            this.colDocKey.Name = "colDocKey";
            this.colDocKey.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDtlKey
            // 
            this.colDtlKey.Caption = "DtlKey";
            this.colDtlKey.FieldName = "DtlKey";
            this.colDtlKey.Name = "colDtlKey";
            this.colDtlKey.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colBOMCode
            // 
            this.colBOMCode.Caption = "BOMCode";
            this.colBOMCode.ColumnEdit = this.repItemBOMCode;
            this.colBOMCode.FieldName = "BOMCode";
            this.colBOMCode.Name = "colBOMCode";
            this.colBOMCode.Visible = true;
            this.colBOMCode.VisibleIndex = 2;
            this.colBOMCode.Width = 117;
            // 
            // repItemBOMCode
            // 
            this.repItemBOMCode.AutoHeight = false;
            this.repItemBOMCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemBOMCode.Name = "repItemBOMCode";
            this.repItemBOMCode.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.OnlyInPopup;
            this.repItemBOMCode.EditValueChanged += new System.EventHandler(this.repItemBOMCode_EditValueChanged);
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item Code";
            this.colItemCode.ColumnEdit = this.repItemLuedtItem;
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 0;
            this.colItemCode.Width = 106;
            // 
            // repItemLuedtItem
            // 
            this.repItemLuedtItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemLuedtItem.Name = "repItemLuedtItem";
            this.repItemLuedtItem.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.OnlyInPopup;
            this.repItemLuedtItem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.repItemLuedtItem.EditValueChanged += new System.EventHandler(this.repItemLuedtItem_EditValueChanged_1);
            // 
            // colDesc
            // 
            this.colDesc.Caption = "Description";
            this.colDesc.FieldName = "Description";
            this.colDesc.Name = "colDesc";
            this.colDesc.OptionsColumn.AllowEdit = false;
            this.colDesc.Visible = true;
            this.colDesc.VisibleIndex = 1;
            this.colDesc.Width = 287;
            // 
            // colUOM
            // 
            this.colUOM.Caption = "UOM";
            this.colUOM.ColumnEdit = this.repositoryItemluUOM;
            this.colUOM.FieldName = "UOM";
            this.colUOM.Name = "colUOM";
            this.colUOM.Visible = true;
            this.colUOM.VisibleIndex = 3;
            this.colUOM.Width = 59;
            // 
            // repositoryItemluUOM
            // 
            this.repositoryItemluUOM.AutoHeight = false;
            this.repositoryItemluUOM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemluUOM.Name = "repositoryItemluUOM";
            // 
            // colRate
            // 
            this.colRate.Caption = "Rate";
            this.colRate.FieldName = "Rate";
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowEdit = false;
            this.colRate.Visible = true;
            this.colRate.VisibleIndex = 4;
            this.colRate.Width = 49;
            // 
            // colQty
            // 
            this.colQty.FieldName = "Qty";
            this.colQty.Name = "colQty";
            this.colQty.Visible = true;
            this.colQty.VisibleIndex = 6;
            this.colQty.Width = 54;
            // 
            // colUnitCost
            // 
            this.colUnitCost.Caption = "Unit Cost";
            this.colUnitCost.FieldName = "UnitCost";
            this.colUnitCost.Name = "colUnitCost";
            this.colUnitCost.Visible = true;
            this.colUnitCost.VisibleIndex = 7;
            this.colUnitCost.Width = 94;
            // 
            // colItemBOMCode
            // 
            this.colItemBOMCode.Caption = "ItemBOMCode";
            this.colItemBOMCode.ColumnEdit = this.repItemLuedtItem;
            this.colItemBOMCode.FieldName = "ItemBOMCode";
            this.colItemBOMCode.Name = "colItemBOMCode";
            this.colItemBOMCode.Width = 194;
            // 
            // colProductRatio
            // 
            this.colProductRatio.Caption = "Product Ratio";
            this.colProductRatio.FieldName = "ProductRatio";
            this.colProductRatio.Name = "colProductRatio";
            this.colProductRatio.Visible = true;
            this.colProductRatio.VisibleIndex = 5;
            this.colProductRatio.Width = 94;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 8;
            this.colTotalCost.Width = 130;
            // 
            // repBOMCode
            // 
            this.repBOMCode.AutoHeight = false;
            this.repBOMCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repBOMCode.Name = "repBOMCode";
            // 
            // panelGridEdit
            // 
            this.panelGridEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelGridEdit.Controls.Add(this.sbtnUndo);
            this.panelGridEdit.Controls.Add(this.sbtnAddDTL);
            this.panelGridEdit.Controls.Add(this.sbtnDeleteDTL);
            this.panelGridEdit.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelGridEdit.Location = new System.Drawing.Point(0, 0);
            this.panelGridEdit.Name = "panelGridEdit";
            this.panelGridEdit.Size = new System.Drawing.Size(847, 32);
            this.panelGridEdit.TabIndex = 1;
            // 
            // sbtnUndo
            // 
            this.sbtnUndo.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndo.Image")));
            this.sbtnUndo.ImageIndex = 3;
            this.sbtnUndo.ImageList = this.imageList1;
            this.sbtnUndo.Location = new System.Drawing.Point(87, 3);
            this.sbtnUndo.Name = "sbtnUndo";
            this.sbtnUndo.Size = new System.Drawing.Size(26, 23);
            this.sbtnUndo.TabIndex = 12;
            this.sbtnUndo.TabStop = false;
            this.sbtnUndo.Text = "Undo";
            this.sbtnUndo.Click += new System.EventHandler(this.sbtnUndo_Click);
            // 
            // sbtnAddDTL
            // 
            this.sbtnAddDTL.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddDTL.Image")));
            this.sbtnAddDTL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddDTL.Location = new System.Drawing.Point(21, 3);
            this.sbtnAddDTL.Name = "sbtnAddDTL";
            this.sbtnAddDTL.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddDTL.TabIndex = 10;
            this.sbtnAddDTL.TabStop = false;
            this.sbtnAddDTL.Click += new System.EventHandler(this.sbtnAddDTL_Click);
            // 
            // sbtnDeleteDTL
            // 
            this.sbtnDeleteDTL.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeleteDTL.Image")));
            this.sbtnDeleteDTL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeleteDTL.Location = new System.Drawing.Point(53, 3);
            this.sbtnDeleteDTL.Name = "sbtnDeleteDTL";
            this.sbtnDeleteDTL.Size = new System.Drawing.Size(26, 23);
            this.sbtnDeleteDTL.TabIndex = 11;
            this.sbtnDeleteDTL.TabStop = false;
            this.sbtnDeleteDTL.Click += new System.EventHandler(this.sbtnDeleteDTL_Click);
            // 
            // xtraTabPageAP
            // 
            this.xtraTabPageAP.Controls.Add(this.gridCtlIBOMAP);
            this.xtraTabPageAP.Controls.Add(this.panelControl2);
            this.xtraTabPageAP.Name = "xtraTabPageAP";
            this.xtraTabPageAP.Size = new System.Drawing.Size(847, 348);
            this.xtraTabPageAP.Text = "Alternatif Product";
            // 
            // gridCtlIBOMAP
            // 
            this.gridCtlIBOMAP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlIBOMAP.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlIBOMAP.Location = new System.Drawing.Point(0, 33);
            this.gridCtlIBOMAP.MainView = this.gvBOMAP;
            this.gridCtlIBOMAP.Name = "gridCtlIBOMAP";
            this.gridCtlIBOMAP.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repleAPItemCode});
            this.gridCtlIBOMAP.Size = new System.Drawing.Size(847, 315);
            this.gridCtlIBOMAP.TabIndex = 0;
            this.gridCtlIBOMAP.TabStop = false;
            this.gridCtlIBOMAP.UseEmbeddedNavigator = true;
            this.gridCtlIBOMAP.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBOMAP});
            // 
            // gvBOMAP
            // 
            this.gvBOMAP.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPItemCode,
            this.colDescription,
            this.colAPCostPercent});
            this.gvBOMAP.GridControl = this.gridCtlIBOMAP;
            this.gvBOMAP.Name = "gvBOMAP";
            // 
            // colAPItemCode
            // 
            this.colAPItemCode.Caption = "ItemCode";
            this.colAPItemCode.ColumnEdit = this.repleAPItemCode;
            this.colAPItemCode.FieldName = "ItemCode";
            this.colAPItemCode.Name = "colAPItemCode";
            this.colAPItemCode.Visible = true;
            this.colAPItemCode.VisibleIndex = 0;
            this.colAPItemCode.Width = 213;
            // 
            // repleAPItemCode
            // 
            this.repleAPItemCode.AutoHeight = false;
            this.repleAPItemCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleAPItemCode.Name = "repleAPItemCode";
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 616;
            // 
            // colAPCostPercent
            // 
            this.colAPCostPercent.Caption = "Cost (%)";
            this.colAPCostPercent.FieldName = "CostPercent";
            this.colAPCostPercent.Name = "colAPCostPercent";
            this.colAPCostPercent.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.sbtnUndoAPDtl);
            this.panelControl2.Controls.Add(this.sbtnAddAPDtl);
            this.panelControl2.Controls.Add(this.sbtnDeleteAPDtl);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(847, 33);
            this.panelControl2.TabIndex = 3;
            // 
            // sbtnUndoAPDtl
            // 
            this.sbtnUndoAPDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndoAPDtl.Image")));
            this.sbtnUndoAPDtl.ImageIndex = 3;
            this.sbtnUndoAPDtl.Location = new System.Drawing.Point(84, 4);
            this.sbtnUndoAPDtl.Name = "sbtnUndoAPDtl";
            this.sbtnUndoAPDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnUndoAPDtl.TabIndex = 18;
            this.sbtnUndoAPDtl.TabStop = false;
            this.sbtnUndoAPDtl.Text = "Undo";
            this.sbtnUndoAPDtl.Click += new System.EventHandler(this.sbtnUndoAPDtl_Click);
            // 
            // sbtnAddAPDtl
            // 
            this.sbtnAddAPDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddAPDtl.Image")));
            this.sbtnAddAPDtl.ImageIndex = 0;
            this.sbtnAddAPDtl.ImageList = this.imageList1;
            this.sbtnAddAPDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddAPDtl.Location = new System.Drawing.Point(20, 4);
            this.sbtnAddAPDtl.Name = "sbtnAddAPDtl";
            this.sbtnAddAPDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddAPDtl.TabIndex = 16;
            this.sbtnAddAPDtl.TabStop = false;
            this.sbtnAddAPDtl.Click += new System.EventHandler(this.sbtnAddAPDtl_Click);
            // 
            // sbtnDeleteAPDtl
            // 
            this.sbtnDeleteAPDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeleteAPDtl.Image")));
            this.sbtnDeleteAPDtl.ImageIndex = 1;
            this.sbtnDeleteAPDtl.ImageList = this.imageList1;
            this.sbtnDeleteAPDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeleteAPDtl.Location = new System.Drawing.Point(52, 4);
            this.sbtnDeleteAPDtl.Name = "sbtnDeleteAPDtl";
            this.sbtnDeleteAPDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnDeleteAPDtl.TabIndex = 17;
            this.sbtnDeleteAPDtl.TabStop = false;
            this.sbtnDeleteAPDtl.Click += new System.EventHandler(this.sbtnDeleteAPDtl_Click);
            // 
            // xtraTabPageOvd
            // 
            this.xtraTabPageOvd.Controls.Add(this.gridCtlIBOMOvd);
            this.xtraTabPageOvd.Controls.Add(this.panelControl3);
            this.xtraTabPageOvd.Name = "xtraTabPageOvd";
            this.xtraTabPageOvd.Size = new System.Drawing.Size(847, 348);
            this.xtraTabPageOvd.Text = "Factory Overhead Cost";
            // 
            // gridCtlIBOMOvd
            // 
            this.gridCtlIBOMOvd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlIBOMOvd.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlIBOMOvd.Location = new System.Drawing.Point(0, 33);
            this.gridCtlIBOMOvd.MainView = this.gvBOMOvd;
            this.gridCtlIBOMOvd.Name = "gridCtlIBOMOvd";
            this.gridCtlIBOMOvd.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repleOvdOverheadCode,
            this.repositoryItemCheckEdit3});
            this.gridCtlIBOMOvd.Size = new System.Drawing.Size(847, 315);
            this.gridCtlIBOMOvd.TabIndex = 0;
            this.gridCtlIBOMOvd.TabStop = false;
            this.gridCtlIBOMOvd.UseEmbeddedNavigator = true;
            this.gridCtlIBOMOvd.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBOMOvd});
            // 
            // gvBOMOvd
            // 
            this.gvBOMOvd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOvdOverheadCode,
            this.colOvdDescription,
            this.colOvdAmount});
            this.gvBOMOvd.GridControl = this.gridCtlIBOMOvd;
            this.gvBOMOvd.Name = "gvBOMOvd";
            // 
            // colOvdOverheadCode
            // 
            this.colOvdOverheadCode.Caption = "OverheadCode";
            this.colOvdOverheadCode.ColumnEdit = this.repleOvdOverheadCode;
            this.colOvdOverheadCode.FieldName = "OverheadCode";
            this.colOvdOverheadCode.Name = "colOvdOverheadCode";
            this.colOvdOverheadCode.Visible = true;
            this.colOvdOverheadCode.VisibleIndex = 0;
            this.colOvdOverheadCode.Width = 235;
            // 
            // repleOvdOverheadCode
            // 
            this.repleOvdOverheadCode.AutoHeight = false;
            this.repleOvdOverheadCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleOvdOverheadCode.Name = "repleOvdOverheadCode";
            // 
            // colOvdDescription
            // 
            this.colOvdDescription.Caption = "Description";
            this.colOvdDescription.FieldName = "Description";
            this.colOvdDescription.Name = "colOvdDescription";
            this.colOvdDescription.OptionsColumn.AllowEdit = false;
            this.colOvdDescription.Visible = true;
            this.colOvdDescription.VisibleIndex = 1;
            this.colOvdDescription.Width = 537;
            // 
            // colOvdAmount
            // 
            this.colOvdAmount.Caption = "Jumlah (Rp)";
            this.colOvdAmount.FieldName = "Amount";
            this.colOvdAmount.Name = "colOvdAmount";
            this.colOvdAmount.Visible = true;
            this.colOvdAmount.VisibleIndex = 2;
            this.colOvdAmount.Width = 306;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = "T";
            this.repositoryItemCheckEdit3.ValueUnchecked = "F";
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.sbtnUndoOvdDtl);
            this.panelControl3.Controls.Add(this.sbtnAddOvdDtl);
            this.panelControl3.Controls.Add(this.sbtnDeleteOvdDtl);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(847, 33);
            this.panelControl3.TabIndex = 3;
            // 
            // sbtnUndoOvdDtl
            // 
            this.sbtnUndoOvdDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndoOvdDtl.Image")));
            this.sbtnUndoOvdDtl.ImageIndex = 3;
            this.sbtnUndoOvdDtl.Location = new System.Drawing.Point(84, 4);
            this.sbtnUndoOvdDtl.Name = "sbtnUndoOvdDtl";
            this.sbtnUndoOvdDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnUndoOvdDtl.TabIndex = 21;
            this.sbtnUndoOvdDtl.TabStop = false;
            this.sbtnUndoOvdDtl.Text = "Undo";
            this.sbtnUndoOvdDtl.Click += new System.EventHandler(this.sbtnUndoOvdDtl_Click);
            // 
            // sbtnAddOvdDtl
            // 
            this.sbtnAddOvdDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddOvdDtl.Image")));
            this.sbtnAddOvdDtl.ImageIndex = 0;
            this.sbtnAddOvdDtl.ImageList = this.imageList1;
            this.sbtnAddOvdDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddOvdDtl.Location = new System.Drawing.Point(20, 4);
            this.sbtnAddOvdDtl.Name = "sbtnAddOvdDtl";
            this.sbtnAddOvdDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddOvdDtl.TabIndex = 19;
            this.sbtnAddOvdDtl.TabStop = false;
            this.sbtnAddOvdDtl.Click += new System.EventHandler(this.sbtnAddOvdDtl_Click);
            // 
            // sbtnDeleteOvdDtl
            // 
            this.sbtnDeleteOvdDtl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeleteOvdDtl.Image")));
            this.sbtnDeleteOvdDtl.ImageIndex = 1;
            this.sbtnDeleteOvdDtl.ImageList = this.imageList1;
            this.sbtnDeleteOvdDtl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeleteOvdDtl.Location = new System.Drawing.Point(52, 4);
            this.sbtnDeleteOvdDtl.Name = "sbtnDeleteOvdDtl";
            this.sbtnDeleteOvdDtl.Size = new System.Drawing.Size(26, 23);
            this.sbtnDeleteOvdDtl.TabIndex = 20;
            this.sbtnDeleteOvdDtl.TabStop = false;
            this.sbtnDeleteOvdDtl.Click += new System.EventHandler(this.sbtnDeleteOvdDtl_Click);
            // 
            // tabItemReplacement
            // 
            this.tabItemReplacement.Controls.Add(this.gridCtlIBOMRep);
            this.tabItemReplacement.Controls.Add(this.panelControl4);
            this.tabItemReplacement.Name = "tabItemReplacement";
            this.tabItemReplacement.Size = new System.Drawing.Size(847, 348);
            this.tabItemReplacement.Text = "Raw Material Replacement";
            // 
            // gridCtlIBOMRep
            // 
            this.gridCtlIBOMRep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtlIBOMRep.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridCtlIBOMRep.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridCtlIBOMRep.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridCtlIBOMRep.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridCtlIBOMRep.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridCtlIBOMRep.Location = new System.Drawing.Point(0, 32);
            this.gridCtlIBOMRep.MainView = this.gvBOMRep;
            this.gridCtlIBOMRep.Name = "gridCtlIBOMRep";
            this.gridCtlIBOMRep.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repleItemCode2,
            this.repleItemCodeRep,
            this.repleUOM});
            this.gridCtlIBOMRep.Size = new System.Drawing.Size(847, 316);
            this.gridCtlIBOMRep.TabIndex = 0;
            this.gridCtlIBOMRep.TabStop = false;
            this.gridCtlIBOMRep.UseEmbeddedNavigator = true;
            this.gridCtlIBOMRep.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBOMRep});
            // 
            // gvBOMRep
            // 
            this.gvBOMRep.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.colItemCodeRep,
            this.colItemCode2,
            this.colDescriptionRep,
            this.gridColumn6,
            this.colUOMRep,
            this.colQtyRep,
            this.colProductRatioRep});
            this.gvBOMRep.GridControl = this.gridCtlIBOMRep;
            this.gvBOMRep.Name = "gvBOMRep";
            this.gvBOMRep.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gvBOMRep_FocusedColumnChanged);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "DocKey";
            this.gridColumn2.FieldName = "DocKey";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "DtlKey";
            this.gridColumn3.FieldName = "DtlKey";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colItemCodeRep
            // 
            this.colItemCodeRep.Caption = "Item Code Replacement";
            this.colItemCodeRep.ColumnEdit = this.repleItemCodeRep;
            this.colItemCodeRep.FieldName = "ItemCodeRep";
            this.colItemCodeRep.Name = "colItemCodeRep";
            this.colItemCodeRep.Visible = true;
            this.colItemCodeRep.VisibleIndex = 2;
            this.colItemCodeRep.Width = 127;
            // 
            // repleItemCodeRep
            // 
            this.repleItemCodeRep.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleItemCodeRep.Name = "repleItemCodeRep";
            // 
            // colItemCode2
            // 
            this.colItemCode2.Caption = "Item Code";
            this.colItemCode2.ColumnEdit = this.repleItemCode2;
            this.colItemCode2.FieldName = "ItemCode";
            this.colItemCode2.Name = "colItemCode2";
            this.colItemCode2.Visible = true;
            this.colItemCode2.VisibleIndex = 0;
            this.colItemCode2.Width = 88;
            // 
            // repleItemCode2
            // 
            this.repleItemCode2.AutoHeight = false;
            this.repleItemCode2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleItemCode2.Name = "repleItemCode2";
            // 
            // colDescriptionRep
            // 
            this.colDescriptionRep.Caption = "Desc. Replacement";
            this.colDescriptionRep.FieldName = "DescriptionRep";
            this.colDescriptionRep.Name = "colDescriptionRep";
            this.colDescriptionRep.Visible = true;
            this.colDescriptionRep.VisibleIndex = 3;
            this.colDescriptionRep.Width = 342;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Description";
            this.gridColumn6.FieldName = "Description";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 276;
            // 
            // colUOMRep
            // 
            this.colUOMRep.Caption = "UOM";
            this.colUOMRep.ColumnEdit = this.repleUOM;
            this.colUOMRep.FieldName = "UOM";
            this.colUOMRep.Name = "colUOMRep";
            this.colUOMRep.Visible = true;
            this.colUOMRep.VisibleIndex = 4;
            this.colUOMRep.Width = 84;
            // 
            // repleUOM
            // 
            this.repleUOM.AutoHeight = false;
            this.repleUOM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repleUOM.Name = "repleUOM";
            // 
            // colQtyRep
            // 
            this.colQtyRep.FieldName = "Qty";
            this.colQtyRep.Name = "colQtyRep";
            this.colQtyRep.Visible = true;
            this.colQtyRep.VisibleIndex = 6;
            this.colQtyRep.Width = 81;
            // 
            // colProductRatioRep
            // 
            this.colProductRatioRep.Caption = "Product Ratio";
            this.colProductRatioRep.FieldName = "ProductRatio";
            this.colProductRatioRep.Name = "colProductRatioRep";
            this.colProductRatioRep.Visible = true;
            this.colProductRatioRep.VisibleIndex = 5;
            this.colProductRatioRep.Width = 80;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.sbtnUndoRep);
            this.panelControl4.Controls.Add(this.sbtnAddRep);
            this.panelControl4.Controls.Add(this.sbtnDeleteRep);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(847, 32);
            this.panelControl4.TabIndex = 2;
            // 
            // sbtnUndoRep
            // 
            this.sbtnUndoRep.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUndoRep.Image")));
            this.sbtnUndoRep.ImageIndex = 3;
            this.sbtnUndoRep.ImageList = this.imageList1;
            this.sbtnUndoRep.Location = new System.Drawing.Point(87, 3);
            this.sbtnUndoRep.Name = "sbtnUndoRep";
            this.sbtnUndoRep.Size = new System.Drawing.Size(26, 23);
            this.sbtnUndoRep.TabIndex = 24;
            this.sbtnUndoRep.TabStop = false;
            this.sbtnUndoRep.Text = "Undo";
            this.sbtnUndoRep.Click += new System.EventHandler(this.sbtnUndoRep_Click);
            // 
            // sbtnAddRep
            // 
            this.sbtnAddRep.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAddRep.Image")));
            this.sbtnAddRep.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnAddRep.Location = new System.Drawing.Point(21, 3);
            this.sbtnAddRep.Name = "sbtnAddRep";
            this.sbtnAddRep.Size = new System.Drawing.Size(26, 23);
            this.sbtnAddRep.TabIndex = 22;
            this.sbtnAddRep.TabStop = false;
            this.sbtnAddRep.Click += new System.EventHandler(this.sbtnAddRep_Click);
            // 
            // sbtnDeleteRep
            // 
            this.sbtnDeleteRep.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDeleteRep.Image")));
            this.sbtnDeleteRep.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnDeleteRep.Location = new System.Drawing.Point(53, 3);
            this.sbtnDeleteRep.Name = "sbtnDeleteRep";
            this.sbtnDeleteRep.Size = new System.Drawing.Size(26, 23);
            this.sbtnDeleteRep.TabIndex = 23;
            this.sbtnDeleteRep.TabStop = false;
            this.sbtnDeleteRep.Click += new System.EventHandler(this.sbtnDeleteRep_Click);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(847, 188);
            this.xtraTabPage1.Text = "Detail";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit2.ValueChecked = "T";
            this.repositoryItemCheckEdit2.ValueUnchecked = "F";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.ValueChecked = "T";
            this.repositoryItemCheckEdit1.ValueUnchecked = "F";
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemLookUpEditUOM
            // 
            this.repositoryItemLookUpEditUOM.Name = "repositoryItemLookUpEditUOM";
            // 
            // panelBottom
            // 
            this.panelBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelBottom.Controls.Add(this.chkedtProceedNew);
            this.panelBottom.Controls.Add(this.sBtnOK);
            this.panelBottom.Controls.Add(this.sBtnCancel);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 558);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(853, 40);
            this.panelBottom.TabIndex = 1;
            // 
            // chkedtProceedNew
            // 
            this.chkedtProceedNew.Location = new System.Drawing.Point(12, 9);
            this.chkedtProceedNew.Name = "chkedtProceedNew";
            this.chkedtProceedNew.Properties.Caption = "After save, proceed with new BOM";
            this.chkedtProceedNew.Properties.ValueChecked = "T";
            this.chkedtProceedNew.Properties.ValueUnchecked = "F";
            this.chkedtProceedNew.Size = new System.Drawing.Size(209, 19);
            this.chkedtProceedNew.TabIndex = 0;
            // 
            // sBtnOK
            // 
            this.sBtnOK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sBtnOK.Location = new System.Drawing.Point(674, 8);
            this.sBtnOK.Name = "sBtnOK";
            this.sBtnOK.Size = new System.Drawing.Size(75, 23);
            this.sBtnOK.TabIndex = 19;
            this.sBtnOK.Text = "&Save";
            this.sBtnOK.Click += new System.EventHandler(this.sBtnOK_Click);
            // 
            // sBtnCancel
            // 
            this.sBtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sBtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sBtnCancel.Location = new System.Drawing.Point(755, 8);
            this.sBtnCancel.Name = "sBtnCancel";
            this.sBtnCancel.Size = new System.Drawing.Size(75, 23);
            this.sBtnCancel.TabIndex = 20;
            this.sBtnCancel.Text = "&Cancel";
            this.sBtnCancel.Click += new System.EventHandler(this.sBtnCancel_Click);
            // 
            // xtraTabCtlPriceSchemeII
            // 
            this.xtraTabCtlPriceSchemeII.Name = "xtraTabCtlPriceSchemeII";
            this.xtraTabCtlPriceSchemeII.Size = new System.Drawing.Size(0, 0);
            // 
            // xtraTabPriceSchemeI
            // 
            this.xtraTabPriceSchemeI.Name = "xtraTabPriceSchemeI";
            this.xtraTabPriceSchemeI.Size = new System.Drawing.Size(0, 0);
            // 
            // tabCtlPriceI
            // 
            this.tabCtlPriceI.Location = new System.Drawing.Point(0, 0);
            this.tabCtlPriceI.Name = "tabCtlPriceI";
            this.tabCtlPriceI.Size = new System.Drawing.Size(300, 300);
            this.tabCtlPriceI.TabIndex = 0;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnCopyFrom
            // 
            this.barBtnCopyFrom.Id = 1;
            this.barBtnCopyFrom.Name = "barBtnCopyFrom";
            // 
            // barBtnCopyTo
            // 
            this.barBtnCopyTo.Id = 2;
            this.barBtnCopyTo.Name = "barBtnCopyTo";
            // 
            // barBtnCreateProposeCont
            // 
            this.barBtnCreateProposeCont.Id = 3;
            this.barBtnCreateProposeCont.Name = "barBtnCreateProposeCont";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 4;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(204, 194);
            this.xtraTabPage2.Text = "Biaya Overhead";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // FormItemBOMEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sBtnCancel;
            this.ClientSize = new System.Drawing.Size(853, 598);
            this.Controls.Add(this.panelMiddle);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormItemBOMEntry";
            this.Text = "New BOM";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormItemBOMEntry_Closing);
            this.Load += new System.EventHandler(this.FormItemBOMEntry_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormItemBOMEntry_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FormItemBOMEntry_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.luProjNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luUOM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCostType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luItemCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtIsActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtedtDescription2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtedtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtedtBOMCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelMiddle)).EndInit();
            this.panelMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabCtlItemBOM)).EndInit();
            this.xtraTabCtlItemBOM.ResumeLayout(false);
            this.xtraTabBOMItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBOMItemLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleBSUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemBSItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.xtraTabDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemBOMCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemLuedtItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemluUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBOMCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGridEdit)).EndInit();
            this.panelGridEdit.ResumeLayout(false);
            this.xtraTabPageAP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleAPItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.xtraTabPageOvd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMOvd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMOvd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleOvdOverheadCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.tabItemReplacement.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtlIBOMRep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBOMRep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleItemCodeRep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleItemCode2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repleUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelBottom)).EndInit();
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkedtProceedNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabCtlPriceI)).EndInit();
            this.ResumeLayout(false);

        }

        private enum SaveAction
        {
            Save,
            SaveAndPreview,
            SaveAndPrint,
        }

        private void gvItem_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (this.myEntity != null)
            {
                this.FilterItemUOM(this.gvItem);
                this.FilterItemBOM(this.gvItem);
            }
        }

        private void gvItem_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            if (this.myEntity != null)
            {

                if (e.FocusedColumn.FieldName == "UOM")
                    this.FilterItemUOM(this.gvItem);
                if (e.FocusedColumn.FieldName == "BOMCode")
                    this.FilterItemBOM(this.gvItem);

            }
        }

        private void sbtnAddAPDtl_Click(object sender, EventArgs e)
        {
            this.AddNewAPDetail();
        }

        private void sbtnDeleteAPDtl_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndoAP(new MethodInvoker(this.DeleteSelectedAPRows), this.myAPDetailRecordUndo);
        }

        private void sbtnUndoAPDtl_Click(object sender, EventArgs e)
        {
            if (this.myEntity.Action != ItemBOMCommand.Action.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gvBOMAP.BeginUpdate();
                    this.myEntity.BeginLoadAPDetailData();
                    this.myAPDetailRecordUndo.Undo();
                    this.myEntity.EndLoadAPDetailData();
                    this.gvBOMAP.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void sbtnAddOvdDtl_Click(object sender, EventArgs e)
        {
            this.AddNewOvdDetail();
        }

        private void sbtnDeleteOvdDtl_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndoOvd(new MethodInvoker(this.DeleteSelectedOvdRows), this.myOvdDetailRecordUndo);

        }

        private void sbtnUndoOvdDtl_Click(object sender, EventArgs e)
        {
            if (this.myEntity.Action != ItemBOMCommand.Action.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gvBOMOvd.BeginUpdate();
                    this.myEntity.BeginLoadOVDDetailData();
                    this.myOvdDetailRecordUndo.Undo();
                    this.myEntity.EndLoadOVDDetailData();
                    this.gvBOMOvd.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void cbCostType_SelectedIndexChanged(object sender, EventArgs e)
        {

            //if ((CostTypeOptions)cbCostType.SelectedIndex != CostTypeOptions.Standard)
            //spCost.EditValue = 0;
            if (cbCostType.SelectedIndex == 1)
                lblCostpercent.Visible = true;
            else
                lblCostpercent.Visible = false;
           
            InitializeFormControlCost((CostTypeOptions)cbCostType.SelectedIndex);
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void luItemCode_EditValueChanged(object sender, EventArgs e)
        {
            DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemUOMLookupEditBuilder.FilterItemCode(luItemCode.EditValue.ToString());
            FillItemInformation();
        }

        private void repItemBOMCode_EditValueChanged(object sender, EventArgs e)
        {
            //BCE.XtraUtils.GridViewUtils.UpdateData(this.gvItem);
        }

        private void gvItem_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {

           


        }

        private void gvItem_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            DataRow drDetail = gvItem.GetDataRow(e.RowHandle);

            //if (e.Column.FieldName.ToString() == "BOMCode")
            //{
            //    drDetail["BOMCode"] = e.CellValue;
            //}
            //if (e.Column.FieldName.ToString() == "ItemCode")
            //{
            //    if (drDetail["BOMCode"] != null)
            //    {
            //        if (drDetail["BOMCode"].ToString() != "")
            //        {
            //            e.RepositoryItem.ReadOnly = true;
            //        }
            //        else
            //        {
            //            e.RepositoryItem.ReadOnly = false;

            //        }
            //    }
            //    else
            //    {
            //        e.RepositoryItem.ReadOnly = false;

            //    }
            //}
        }

        private void gvBOMBS_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (this.myEntity != null)
            {
                this.FilterItemUOM(this.gvBOMBS);
            }
        }

        private void gvBOMBS_ColumnChanged(object sender, EventArgs e)
        {
            //if (this.myEntity != null)
            //{
            //    this.FilterItemUOM(this.gvBOMBS);
            //}
        }

        private void gvBOMBS_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            if (this.myEntity != null)
            {
                this.FilterItemUOM(this.gvBOMBS);
            }
        }

        private void gvItem_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            DataRow drDetail = gvItem.GetDataRow(e.RowHandle);
            if (e.Column.FieldName == "ItemCode")
            {
                object obj = myDBSetting.ExecuteScalar("select top 1 BOMCode from vRPA_ItemWithBOM where ItemCode=?",(object)e.Value);
                if(obj!=null && obj!=DBNull.Value)
                {
                    drDetail["BOMCode"] = obj;
                }
                else
                {
                    drDetail["BOMCode"] = null;

                }

                // repItemLuedtItem.index
                //string str = repItemLuedtItem.GetDataSourceValue("BOMCode", repItemLuedtItem.GetDataSourceRowIndex("ItemCode",e.Value)).ToString();
                //drDetail["BOMCode"] = str;
                //repItemLuedtItem.ListChanged += RepItemLuedtItem_ListChanged;
                //repItemLuedtItem.EditValueChanged += RepItemLuedtItem_EditValueChanged;
            }
            //if (e.Column.FieldName== "ItemBOMCode" && e.Value != null)
            //{
            //    // repItemLuedtItem.ind

            //    InitLookupItemMaterial();
            //    int index = repItemLuedtItem.GetDataSourceRowIndex("ItemBOMCode", e.Value);
            //    object obj = repItemLuedtItem.GetDataSourceValue("ItemCode", index);
            //    if (obj != null && obj != DBNull.Value)
            //        drDetail["ItemCode"] = obj;
            //    else
            //        drDetail["ItemCode"] = null;
            //    obj = repItemLuedtItem.GetDataSourceValue("BOMCode", index);
            //    if (obj != null && obj != DBNull.Value)
            //        drDetail["BOMCode"] = obj;
            //    else
            //        drDetail["BOMCode"] = null;

            //    //  this.InitializeBatchNoMaster();
            //}
        }

        private void RepItemLuedtItem_EditValueChanged(object sender, EventArgs e)
        {
            string str = "";
        }

        private void gvItem_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            
        }

        private void repItemLuedtItem_EditValueChanged_1(object sender, EventArgs e)
        {

        }

        private void sbtnAddRep_Click(object sender, EventArgs e)
        {
            this.AddNewRepDetail();
            InitLookupItemMaterial();
        }

        private void sbtnDeleteRep_Click(object sender, EventArgs e)
        {
            this.ExecuteWithPauseUndoRep(new MethodInvoker(this.DeleteSelectedRepRows), this.myRepDetailRecordUndo);
        }

        private void sbtnUndoRep_Click(object sender, EventArgs e)
        {
            if (this.myEntity.Action != ItemBOMCommand.Action.View)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.gvBOMRep.BeginUpdate();
                    this.myEntity.BeginLoadRepDetailData();
                    this.myRepDetailRecordUndo.Undo();
                    this.myEntity.EndLoadRepDetailData();
                    this.gvBOMRep.EndUpdate();
                }
                finally
                {
                    Cursor.Current = current;
                }
            }
        }

        private void gvBOMRep_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            if (this.myEntity != null)
            {

                if (e.FocusedColumn.FieldName == "UOM")
                    this.FilterItemUOM2(this.gvBOMRep);
                //if (e.FocusedColumn.FieldName == "BOMCode")
                //  this.FilterItemBOM(this.gvItem);

            }
        }

        private void gvItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                
                this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormItemBOMEntry_KeyDown);
                e.Handled = true;
            }
        }

        private void gridCtlItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormItemBOMEntry_KeyDown);
                e.Handled = true;
            }
        }

        private void FormItemBOMEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar ==System.Convert.ToChar(Keys.Enter))
            {
               // MessageBox.Show(" Enter pressed ");
            }
        }
    }
}
