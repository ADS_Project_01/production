﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.FormItemBOMEditOption
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

namespace Production.GeneralMaintenance.ItemBOM
{
  public class FormItemBOMEditOption
  {
    public bool ShowProceedNewItemBOM { get; set; }
  }
}
