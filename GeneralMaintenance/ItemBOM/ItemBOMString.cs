﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.ItemBOMString
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Localization;

namespace Production.GeneralMaintenance.ItemBOM
{
  [LocalizableString]
  public enum ItemBOMString
  {
    [DefaultString("Description")] Description,
    [DefaultString("{0} opened Item BOM Maintenance List window.")] OpenItemBOMMaintenance,
    [DefaultString("{0} viewed Item BOM Code {1} in edit mode.")] ViewItemBOMCodeInEditMode,
    [DefaultString("{0} created new Item BOM Code {1}.")] CreatedItemBOMCode,
    [DefaultString("{0} updated new Item BOM Code {1}.")] UpdatedItemBOMCode,
    [DefaultString("{0} deleted Item BOM Code {1}.")] DeletedItemBOMCode,
    [DefaultString("{0} opened Item BOM Listing window.")] OpenedItemBOMListing,
    [DefaultString("{0} inquired on Item BOM Listing.")] InquiredItemBOMListing,
    [DefaultString("Item BOM Listing Report")] ItemBOMListingReport,
    [DefaultString("Item BOM Code")] BOMCode,
    [DefaultString("Print Active Item BOM")] PrintActiveItemBOM,
    [DefaultString("Print Inactive Item BOM")] PrintInactiveItemBOM,
  }
}
