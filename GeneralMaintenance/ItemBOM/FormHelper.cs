﻿// Type: BCE.AutoCount.Manufacturing.RPA_ItemBOM.FormHelper
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount;
using BCE.AutoCount.Report;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Production.GeneralMaintenance.ItemBOM
{
    public class FormHelper
    {
        public const string ReportListingType = "RPA Item BOM Listing Report";
        protected DBSetting myDBSetting;
        protected PrintCriteriaItemBOM myCriteria;
        private int mySortIDWhenInquiry;
        private int myGroupingIDWhenInquiry;
        private DataSet myDs;
        private DataTable myDtblResult;
        private DataTable myDtblPrintReport;
        private ItemBOMCommand myItemBOMCmd;
        private string myReportType;

        public string ReportType
        {
            set
            {
                this.myReportType = value;
            }
        }

        public PrintCriteriaItemBOM Criteria
        {
            get
            {
                return this.myCriteria;
            }
            set
            {
                this.myCriteria = value;
            }
        }

        public DataTable DtblResult
        {
            get
            {
                return this.myDtblResult;
            }
            set
            {
                this.myDtblResult = value;
            }
        }

        public DataSet ReportDataSet
        {
            get
            {
                return this.myDs;
            }
            set
            {
                this.myDs = value;
            }
        }

        public DataTable DtblPrintReport
        {
            get
            {
                return this.myDtblPrintReport;
            }
            set
            {
                this.myDtblPrintReport = value;
            }
        }

        public int SortIDWhenInquiry
        {
            get
            {
                return this.mySortIDWhenInquiry;
            }
            set
            {
                this.mySortIDWhenInquiry = value;
            }
        }

        public int GroupingIDWhenInquiry
        {
            get
            {
                return this.myGroupingIDWhenInquiry;
            }
            set
            {
                this.myGroupingIDWhenInquiry = value;
            }
        }

        public ItemBOMCommand ItemBOMCmd
        {
            get
            {
                return this.myItemBOMCmd;
            }
            set
            {
                this.myItemBOMCmd = value;
            }
        }

        internal FormHelper(DBSetting dbSetting)
        {
            this.myDBSetting = dbSetting;
            this.myDs = new DataSet();
            this.myCriteria = (PrintCriteriaItemBOM)PersistenceUtil.LoadCriteriaData(this.myDBSetting, "PrintCriteriaRPA_ItemBOM2.setting");
            if (this.myCriteria == null)
                this.myCriteria = new PrintCriteriaItemBOM();
        }

        public void SaveSetting()
        {
            PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting)this.myCriteria, "PrintCriteriaRPA_ItemBOM2.setting");
        }

        public void Inquire(ItemBOMCommand bomOptionalCmd, string strSQL)
        {
            this.myItemBOMCmd = bomOptionalCmd;
            SqlCommand cmd = new SqlCommand();
            SearchCriteria criteria = new SearchCriteria();
            criteria.AddFilter(this.myCriteria.ItemBOMCodeFilter);
            string otherWhereSql = this.GetOtherWhereSQL(cmd);
            this.myDs = this.myItemBOMCmd.BrowseSearchData(cmd, criteria, strSQL, otherWhereSql);
        }

        public string GetOtherWhereSQL(SqlCommand cmd)
        {
            string str = string.Empty;
            if (!this.myCriteria.ActiveItem || !this.myCriteria.InactiveItem)
            {
                if (this.myCriteria.ActiveItem)
                    str = str + "IsActive = 'T'";
                else if (this.myCriteria.InactiveItem)
                    str = str + "IsActive = 'F'";
            }
            return str;
        }

        public void InquireReportBuilder(ItemBOMCommand bomOptionalCmd)
        {
            this.myItemBOMCmd = bomOptionalCmd == null ? ItemBOMCommand.Create(this.myDBSetting) : bomOptionalCmd;
            this.mySortIDWhenInquiry = 0;
            this.myGroupingIDWhenInquiry = 0;
            SearchCriteria criteria = new SearchCriteria();
            SqlCommand cmd = new SqlCommand();
            string strSQL = "SELECT * FROM RPA_ItemBOM";
            this.myDs = this.myItemBOMCmd.BrowseSearchData(cmd, criteria, strSQL, "");
        }

        public object GetReportDesignerDataSource(string reportType)
        {
            this.myReportType = reportType;
            return this.GetReportDataSource();
        }

        public DataTable ReportOptions()
        {
            DataTable dtblSelected = new DataTable();
            dtblSelected.TableName = "Report Option";
            DataColumn dataColumn = new DataColumn();
            this.CreateNewColumn(dtblSelected, "Criteria", "System.String");
            this.CreateNewColumn(dtblSelected, "ShowCriteria", "System.String");
            DataRow row = dtblSelected.NewRow();
            row.BeginEdit();
            row["Criteria"] = (object)this.Criteria.ReadableText;
            row["ShowCriteria"] = this.Criteria.ShowCriteriaInReport ? (object)"Yes" : (object)"No";
            row.EndEdit();
            dtblSelected.Rows.Add(row);
            return dtblSelected;
        }

        public void DesignReport()
        {
            ReportTool.DesignReport("RPA Item BOM Listing Report", this.GetReportDataSource(), this.myDBSetting);
        }

        public void CreateNewColumn(DataTable dtblSelected, string ColumnName, string ColumnType)
        {
            dtblSelected.Columns.Add(new DataColumn()
            {
                DataType = Type.GetType(ColumnType),
                ColumnName = ColumnName
            });
        }

        private object GetReportDataSource()
        {
            DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, "Item BOM", "Master", "Detail");
            documentReportDataSet.Tables.Add(this.myDs.Tables["Master"].Copy());
            documentReportDataSet.Tables[0].TableName = "Master";
            documentReportDataSet.Tables.Add(this.myDs.Tables["Detail"].Copy());
            documentReportDataSet.Tables[1].TableName = "Detail";
            documentReportDataSet.Tables.Add(this.myDs.Tables["BSDetail"].Copy());
            documentReportDataSet.Tables[2].TableName = "BSDetail";
            documentReportDataSet.Tables.Add(this.myDs.Tables["APDetail"].Copy());
            documentReportDataSet.Tables[3].TableName = "APDetail";
            documentReportDataSet.Tables.Add(this.myDs.Tables["OVDDetail"].Copy());
            documentReportDataSet.Tables[4].TableName = "OVDDetail";

            DataRelation relation1 = new DataRelation("MasterDetail", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["Detail"].Columns["DocKey"], false);
            documentReportDataSet.Relations.Add(relation1);
            DataRelation relation2 = new DataRelation("MasterDetailBS", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["BSDetail"].Columns["DocKey"], false);
            documentReportDataSet.Relations.Add(relation2);
            DataRelation relation3 = new DataRelation("MasterDetailAP", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["APDetail"].Columns["DocKey"], false);
            documentReportDataSet.Relations.Add(relation3);
            DataRelation relation4 = new DataRelation("MasterDetailOvd", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["OVDDetail"].Columns["DocKey"], false);
            documentReportDataSet.Relations.Add(relation4);
            documentReportDataSet.Tables.Add(this.ReportOptions());
            documentReportDataSet.AddDefaultTables();
            return (object)documentReportDataSet;
        }
    }
}
