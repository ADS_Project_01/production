﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.DetailEntity
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using System.Data;

namespace Production.GeneralMaintenance.ItemBOM
{
  public class RepDetailEntity
  {
    private DataRow myRow;

    public DataRow Row
    {
      get
      {
        return this.myRow;
      }
    }

    internal RepDetailEntity(DataRow aRow)
    {
      this.myRow = aRow;
    }
  }
}
