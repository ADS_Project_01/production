﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.ReportTypeHandler
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Report;
using BCE.Data;
using DevExpress.XtraReports.UI;
using System.Drawing.Printing;

namespace Production.GeneralMaintenance.ItemBOM
{
  public class ReportTypeHandler : BaseReportTypeHandler
  {
    private ItemBOMCommand myItemBOMCmd;
    private string myReportType;

    public ItemBOMCommand ContractCmd
    {
      set
      {
        this.myItemBOMCmd = value;
      }
    }

    public string ReportType
    {
      set
      {
        this.myReportType = value;
      }
    }

    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      FormHelper formHelper = new FormHelper(dbSetting);
      PrintCriteriaItemBOM criteriaBomOptional = new PrintCriteriaItemBOM();
      formHelper.Criteria = criteriaBomOptional;
      string str = this.myReportType;
      formHelper.ReportType = str;
      ItemBOMCommand bomOptionalCmd = this.myItemBOMCmd;
      formHelper.InquireReportBuilder(bomOptionalCmd);
      string reportType = this.myReportType;
      return formHelper.GetReportDesignerDataSource(reportType);
    }

    public override void BeforePrint(XtraReport report, System.Drawing.Printing.PrintEventArgs e)
    {
      ReportFormatter reportFormatter = new ReportFormatter(((BaseReport) report).DBSetting);
      string columnName1 = "DocDate";
      int num1 = 16;
      reportFormatter.AddColumnFormat(columnName1, (FormatStringType) num1);
      string columnName2 = "AttendedDate";
      int num2 = 16;
      reportFormatter.AddColumnFormat(columnName2, (FormatStringType) num2);
      string columnName3 = "Cost";
      int num3 = 5;
      reportFormatter.AddColumnFormat(columnName3, (FormatStringType) num3);
      string columnName4 = "Price";
      int num4 = 6;
      reportFormatter.AddColumnFormat(columnName4, (FormatStringType) num4);
      string columnName5 = "Quantity";
      int num5 = 4;
      reportFormatter.AddColumnFormat(columnName5, (FormatStringType) num5);
      string columnName6 = "SubTotal";
      int num6 = 6;
      reportFormatter.AddColumnFormat(columnName6, (FormatStringType) num6);
      XtraReport report1 = report;
      reportFormatter.InitReport(report1);
    }
  }
}
