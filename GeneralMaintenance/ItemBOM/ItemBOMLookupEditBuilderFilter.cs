﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.ItemBOMLookupEditBuilder
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.XtraUtils.LookupEditBuilder;
using BCE.Localization;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Windows.Forms;

namespace Production.GeneralMaintenance.ItemBOM
{
  public class ItemBOMLookupEditBuilderFilter : LookupEditBuilderEx
  {
    public ItemBOMLookupEditBuilderFilter(string bomcode)
    {
     // this.myUseCaching = true;
      //this.myCacheFileName = "RPA_ItemBOM2.Data";
      this.myCmdString = "SELECT BOMCode,ItemCode,Description FROM RPA_ItemBOM where IsActive='T' and BOMCode<>'"+ bomcode + "'";
    }

    protected override void SetupLookupEdit(RepositoryItemLookUpEdit myLookupEdit)
    {
      base.SetupLookupEdit(myLookupEdit);
      myLookupEdit.DisplayMember = "BOMCode";
      myLookupEdit.ValueMember = "BOMCode";
      myLookupEdit.DataSource = (object) this.myDataTable;
      myLookupEdit.Columns.Clear();
      myLookupEdit.Columns.Add(new LookUpColumnInfo("BOMCode", BCE.Localization.Localizer.GetString((Enum) ItemBOMString.BOMCode, new object[0]), 100));
            myLookupEdit.Columns.Add(new LookUpColumnInfo("ItemCode", "ItemCode", 100));
            myLookupEdit.Columns.Add(new LookUpColumnInfo("Description", BCE.Localization.Localizer.GetString((Enum) ItemBOMString.Description, new object[0]), 150));
      myLookupEdit.PopupWidth = 350;
      myLookupEdit.AutoSearchColumnIndex = 1;
      myLookupEdit.Columns["BOMCode"].SortOrder = ColumnSortOrder.Ascending;
      myLookupEdit.KeyPress += new KeyPressEventHandler(this.LookupEdt_KeyPress);
    }

    protected override int GetChangeCount()
    {
      return this.myChangeCount.GetChangeCount("RPA_ItemBOM");
    }

    public override void UnlinkEventHandlers(RepositoryItemLookUpEdit lookupEdit)
    {
      base.UnlinkEventHandlers(lookupEdit);
      lookupEdit.KeyPress -= new KeyPressEventHandler(this.LookupEdt_KeyPress);
    }

    private void LookupEdt_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (sender is LookUpEdit)
      {
        LookUpEdit lookUpEdit = sender as LookUpEdit;
        if (!lookUpEdit.IsPopupOpen)
        {
          BaseView baseView = (BaseView) null;
          if ((int) e.KeyChar >= 32 && (int) e.KeyChar <= 126)
          {
            if (baseView != null)
            {
              baseView.ShowEditorByKey(new KeyEventArgs((Keys) 115));
              baseView.ShowEditorByKeyPress(new KeyPressEventArgs(e.KeyChar));
            }
            else
            {
              lookUpEdit.ShowPopup();
              lookUpEdit.SendKey((object) new Message(), new KeyPressEventArgs(e.KeyChar));
            }
            e.Handled = true;
          }
        }
      }
    }
  }
}
