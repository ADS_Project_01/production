﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.FormItemBOMPrint
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.Controls;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.Report;
using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Localization;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTab;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Production.GeneralMaintenance.ItemBOM
{
    [SingleInstanceThreadForm]
    public class FormItemBOMPrint : XtraForm
    {
        private bool myInSearch;
        private ItemBOMCommand myItemBOMCmd;
        private DBSetting myDBSetting;
        private FormHelper myFormHelper;
        private UserAuthentication myUserAuthentication;
        private IContainer components;
        private PanelHeader panelHeader1;
        private PreviewButton previewButton1;
        private PrintButton printButton1;
        private UCInvoiceSelector ucInvoiceSelector1;
        private UCSalesAgentSelector ucSalesAgentSelector1;
        private UCItemBOMSelector ucbomOptionalSelector1;
        private Bar bar1;
        private BarButtonItem barButtonItem1;
        private BarButtonItem barButtonItem2;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private BarDockControl barDockControlTop;
        private BarManager barManager1;
        private BarSubItem barSubItem1;
        private CheckEdit chkedtPrintActive;
        private CheckEdit chkedtPrintInactive;
        private CheckEdit chkedtShowCriteriaInRpt;
        private GroupControl groupControl1;
        private GroupControl groupControl3;
        private MemoEdit memoEditCriteria;
        private PanelControl panButton;
        private PanelControl panelCriteria;
        private PanelControl panelGrid;
        private SimpleButton sBtnClose;
        private SimpleButton sBtnInquiry;
        private SimpleButton sbtnToggleOptions;
        private XtraTabControl xtraTabControl1;
        private XtraTabPage xtraTabPage1;
        private XtraTabPage xtraTabPage2;
        private Label label10;
        private XtraTabPage xtraTabPage3;
        private UCItemBOMGrid ucbomOptionalGrid1;

        public FormItemBOMPrint(ItemBOMCommand ItemBOMCmd)
        {
            this.InitializeComponent();
            this.Icon = BCE.AutoCount.Application.Icon;
            this.myInSearch = false;
            this.myItemBOMCmd = ItemBOMCmd;
            this.myDBSetting = ItemBOMCmd.myDBSetting;
            this.myUserAuthentication = UserAuthentication.GetOrCreate(this.myDBSetting);
            this.Tag = (object)EnterKeyMessageFilter.NoFilter;
            this.myFormHelper = new FormHelper(this.myDBSetting);
            this.InitializeUserControls();
            this.InitializeSettings();
            this.ucbomOptionalGrid1.Initialize(this.myDBSetting);
            this.ucbomOptionalGrid1.DataSource = (object)this.myFormHelper.ReportDataSet.Tables["Master"];
            BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "BOM_Material_Usage_Inquiry.htm");
            DBSetting dbSetting = this.myDBSetting;
            string docType = "";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            ItemBOMString local = ItemBOMString.OpenedItemBOMListing;
            object[] objArray = new object[1];
            int index = 0;
            string loginUserId = this.myUserAuthentication.LoginUserID;
            objArray[index] = (object)loginUserId;
            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            string detail = "";
            Activity.Log(dbSetting, docType, docKey, eventKey, @string, detail);
        }

        private void InitializeSettings()
        {
            this.chkedtPrintActive.Checked = this.myFormHelper.Criteria.ActiveItem;
            this.chkedtPrintInactive.Checked = this.myFormHelper.Criteria.InactiveItem;
            this.chkedtShowCriteriaInRpt.Checked = this.myFormHelper.Criteria.ShowCriteriaInReport;
        }

        private void InitializeUserControls()
        {
            this.ucbomOptionalSelector1.Initialize(this.myDBSetting, this.myFormHelper.Criteria.ItemBOMCodeFilter);
        }

        private void SaveCriteriaSetting()
        {
            this.myFormHelper.Criteria.ActiveItem = this.chkedtPrintActive.Checked;
            this.myFormHelper.Criteria.InactiveItem = this.chkedtPrintInactive.Checked;
            this.myFormHelper.SaveSetting();
        }

        private void Search()
        {
            if (!this.myInSearch)
            {
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                this.myInSearch = true;
                try
                {
                    string str = "";
                    //foreach (UDFColumn udfColumn in new UDFUtil(this.myDBSetting).GetUDF("ItemBOM"))
                        ///str = str + ", " + udfColumn.FieldName;
                    this.myFormHelper.Inquire(this.myItemBOMCmd, "select * " + str + " from RPA_ItemBOM");
                    this.ucbomOptionalGrid1.DataSource = (object)this.myFormHelper.ReportDataSet.Tables["Master"];
                }
                catch (DataAccessException ex)
                {
                    AppMessage.ShowErrorMessage(ex.Message);
                }
                finally
                {
                    Cursor.Current = current;
                    this.myInSearch = false;
                }
            }
        }

        private void sBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnToggleOptions_Click(object sender, EventArgs e)
        {
            this.panelCriteria.Visible = !this.panelCriteria.Visible;
            this.sBtnInquiry.Enabled = this.panelCriteria.Visible;
            if (this.panelCriteria.Visible)
                this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.Code_HideOptions, new object[0]);
            else
                this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum)ItemBOMMaintStringId.Code_ShowOptions, new object[0]);
        }

        private void sBtnInquiry_Click(object sender, EventArgs e)
        {
            this.SaveCriteriaSetting();
            this.Search();
            this.sbtnToggleOptions.Enabled = true;
            this.memoEditCriteria.Lines = this.myFormHelper.Criteria.ReadableTextArray;
            this.barButtonItem1.Enabled = true;
            DBSetting dbSetting = this.myDBSetting;
            string docType = "";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            ItemBOMString local = ItemBOMString.InquiredItemBOMListing;
            object[] objArray = new object[1];
            int index = 0;
            string loginUserId = this.myUserAuthentication.LoginUserID;
            objArray[index] = (object)loginUserId;
            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            string readableText = this.myFormHelper.Criteria.ReadableText;
            Activity.Log(dbSetting, docType, docKey, eventKey, @string, readableText);
        }

        private void chkedtShowCriteriaInRpt_CheckedChanged(object sender, EventArgs e)
        {
            this.myFormHelper.Criteria.ShowCriteriaInReport = this.chkedtShowCriteriaInRpt.Checked;
        }

        private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            //if (this.myUserAuthentication.AccessRight.IsAccessible("MF_BOPT_LISTING_REPORT_PRINT", (XtraForm)this))
            {
                if (this.myFormHelper.ItemBOMCmd == null)
                    this.myFormHelper.ItemBOMCmd = this.myItemBOMCmd;
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum)ItemBOMString.ItemBOMListingReport, new object[0]), "MF_BOPT_LISTING_REPORT_PRINT", "MF_BOPT_LISTING_REPORT_EXPORT", this.myFormHelper.Criteria.ReadableText);
                ReportTool.PrintReport("Item BOM Listing Report", this.myFormHelper.GetReportDesignerDataSource("Item BOM Listing Report"), this.myDBSetting, e.DefaultReport, this.myItemBOMCmd.ReportOption, reportInfo);
                Cursor.Current = current;
            }
        }

        private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            //if (this.myUserAuthentication.AccessRight.IsAccessible("MF_BOPT_LISTING_REPORT_PREVIEW", (XtraForm)this))
            {
                if (this.myFormHelper.ItemBOMCmd == null)
                    this.myFormHelper.ItemBOMCmd = this.myItemBOMCmd;
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum)ItemBOMString.ItemBOMListingReport, new object[0]), "MF_BOPT_LISTING_REPORT_PRINT", "MF_BOPT_LISTING_REPORT_EXPORT", this.myFormHelper.Criteria.ReadableText);
                ReportTool.PreviewReport("Item BOM Listing Report", this.myFormHelper.GetReportDesignerDataSource("Item BOM Listing Report"), this.myDBSetting, e.DefaultReport, false, this.myItemBOMCmd.ReportOption, reportInfo);
                Cursor.Current = current;
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.myFormHelper.DesignReport();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormBasicReportOption.ShowReportOption(this.myItemBOMCmd.ReportOption);
            this.myItemBOMCmd.SaveReportOption();
        }

        private void ucbomOptionalGrid1_ReloadAllColumnsEvent(object sender, EventArgs e)
        {
            this.Search();
        }

        private void FormItemBOMPrint_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)120)
            {
                if (this.sBtnInquiry.Enabled)
                    this.sBtnInquiry.PerformClick();
            }
            else if (e.KeyCode == (Keys)119)
            {
                if (this.previewButton1.Enabled)
                    this.previewButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)118)
            {
                if (this.printButton1.Enabled)
                    this.printButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)117)
            {
                if (this.sbtnToggleOptions.Enabled)
                    this.sbtnToggleOptions.PerformClick();
            }
            else if (e.KeyCode == (Keys.LButton | Keys.RButton | Keys.Back | Keys.ShiftKey))
                this.Close();
        }

        private void FormItemBOMPrint_Load(object sender, EventArgs e)
        {
            this.printButton1.ReportType = "Item BOM Listing Report";
            this.printButton1.SetDBSetting(this.myDBSetting);
            this.previewButton1.ReportType = "Item BOM Listing Report";
            this.previewButton1.SetDBSetting(this.myDBSetting);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelCriteria = new DevExpress.XtraEditors.PanelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.chkedtShowCriteriaInRpt = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.ucbomOptionalSelector1 = new Production.GeneralMaintenance.ItemBOM.UCItemBOMSelector();
            this.chkedtPrintInactive = new DevExpress.XtraEditors.CheckEdit();
            this.chkedtPrintActive = new DevExpress.XtraEditors.CheckEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.ucInvoiceSelector1 = new BCE.AutoCount.FilterUI.UCInvoiceSelector();
            this.ucSalesAgentSelector1 = new BCE.AutoCount.FilterUI.UCSalesAgentSelector();
            this.panelGrid = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.ucbomOptionalGrid1 = new Production.GeneralMaintenance.ItemBOM.UCItemBOMGrid();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEditCriteria = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.panButton = new DevExpress.XtraEditors.PanelControl();
            this.printButton1 = new BCE.AutoCount.Controls.PrintButton();
            this.previewButton1 = new BCE.AutoCount.Controls.PreviewButton();
            this.sbtnToggleOptions = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).BeginInit();
            this.panelCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtShowCriteriaInRpt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtPrintInactive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtPrintActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).BeginInit();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panButton)).BeginInit();
            this.panButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCriteria
            // 
            this.panelCriteria.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCriteria.Controls.Add(this.groupControl3);
            this.panelCriteria.Controls.Add(this.groupControl1);
            this.panelCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCriteria.Location = new System.Drawing.Point(0, 44);
            this.panelCriteria.Name = "panelCriteria";
            this.panelCriteria.Size = new System.Drawing.Size(860, 113);
            this.panelCriteria.TabIndex = 1;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.chkedtShowCriteriaInRpt);
            this.groupControl3.Location = new System.Drawing.Point(574, 3);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(200, 50);
            this.groupControl3.TabIndex = 0;
            this.groupControl3.Text = "Report Options :";
            // 
            // chkedtShowCriteriaInRpt
            // 
            this.chkedtShowCriteriaInRpt.Location = new System.Drawing.Point(5, 24);
            this.chkedtShowCriteriaInRpt.Name = "chkedtShowCriteriaInRpt";
            this.chkedtShowCriteriaInRpt.Properties.Caption = "Show Criteria in Report";
            this.chkedtShowCriteriaInRpt.Size = new System.Drawing.Size(149, 19);
            this.chkedtShowCriteriaInRpt.TabIndex = 0;
            this.chkedtShowCriteriaInRpt.CheckedChanged += new System.EventHandler(this.chkedtShowCriteriaInRpt_CheckedChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.ucbomOptionalSelector1);
            this.groupControl1.Controls.Add(this.chkedtPrintInactive);
            this.groupControl1.Controls.Add(this.chkedtPrintActive);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(565, 100);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Filter Options";
            // 
            // ucbomOptionalSelector1
            // 
            this.ucbomOptionalSelector1.Location = new System.Drawing.Point(76, 24);
            this.ucbomOptionalSelector1.Name = "ucbomOptionalSelector1";
            this.ucbomOptionalSelector1.Size = new System.Drawing.Size(428, 26);
            this.ucbomOptionalSelector1.TabIndex = 0;
            // 
            // chkedtPrintInactive
            // 
            this.chkedtPrintInactive.Location = new System.Drawing.Point(170, 56);
            this.chkedtPrintInactive.Name = "chkedtPrintInactive";
            this.chkedtPrintInactive.Properties.Caption = "Print Inactive Item BOM";
            this.chkedtPrintInactive.Size = new System.Drawing.Size(191, 19);
            this.chkedtPrintInactive.TabIndex = 1;
            // 
            // chkedtPrintActive
            // 
            this.chkedtPrintActive.Location = new System.Drawing.Point(10, 56);
            this.chkedtPrintActive.Name = "chkedtPrintActive";
            this.chkedtPrintActive.Properties.Caption = "Print Active Item BOM";
            this.chkedtPrintActive.Size = new System.Drawing.Size(142, 19);
            this.chkedtPrintActive.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(9, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 23);
            this.label10.TabIndex = 3;
            this.label10.Text = "BOM Code";
            // 
            // ucInvoiceSelector1
            // 
            this.ucInvoiceSelector1.Location = new System.Drawing.Point(0, 0);
            this.ucInvoiceSelector1.Name = "ucInvoiceSelector1";
            this.ucInvoiceSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucInvoiceSelector1.TabIndex = 0;
            // 
            // ucSalesAgentSelector1
            // 
            this.ucSalesAgentSelector1.Location = new System.Drawing.Point(0, 0);
            this.ucSalesAgentSelector1.Name = "ucSalesAgentSelector1";
            this.ucSalesAgentSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucSalesAgentSelector1.TabIndex = 0;
            // 
            // panelGrid
            // 
            this.panelGrid.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelGrid.Controls.Add(this.xtraTabControl1);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 201);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(860, 246);
            this.panelGrid.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(860, 246);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.ucbomOptionalGrid1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(854, 218);
            this.xtraTabPage1.Text = "Result";
            // 
            // ucbomOptionalGrid1
            // 
            this.ucbomOptionalGrid1.ColumnAutoWidth = true;
            this.ucbomOptionalGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucbomOptionalGrid1.Location = new System.Drawing.Point(0, 0);
            this.ucbomOptionalGrid1.Name = "ucbomOptionalGrid1";
            this.ucbomOptionalGrid1.Size = new System.Drawing.Size(854, 218);
            this.ucbomOptionalGrid1.TabIndex = 0;
            this.ucbomOptionalGrid1.ReloadAllColumnsEvent2 += new System.EventHandler(this.ucbomOptionalGrid1_ReloadAllColumnsEvent);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.memoEditCriteria);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(854, 218);
            this.xtraTabPage2.Text = "Criteria";
            // 
            // memoEditCriteria
            // 
            this.memoEditCriteria.Location = new System.Drawing.Point(0, 0);
            this.memoEditCriteria.Name = "memoEditCriteria";
            this.memoEditCriteria.Properties.Appearance.Options.UseFont = true;
            this.memoEditCriteria.Properties.ReadOnly = true;
            this.memoEditCriteria.Size = new System.Drawing.Size(100, 96);
            this.memoEditCriteria.TabIndex = 0;
            this.memoEditCriteria.UseOptimizedRendering = true;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(854, 240);
            this.xtraTabPage3.Text = "xtraTabPage3";
            // 
            // panButton
            // 
            this.panButton.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panButton.Controls.Add(this.printButton1);
            this.panButton.Controls.Add(this.previewButton1);
            this.panButton.Controls.Add(this.sbtnToggleOptions);
            this.panButton.Controls.Add(this.sBtnInquiry);
            this.panButton.Controls.Add(this.sBtnClose);
            this.panButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.panButton.Location = new System.Drawing.Point(0, 157);
            this.panButton.Name = "panButton";
            this.panButton.Size = new System.Drawing.Size(860, 44);
            this.panButton.TabIndex = 1;
            // 
            // printButton1
            // 
            this.printButton1.Location = new System.Drawing.Point(170, 6);
            this.printButton1.Name = "printButton1";
            this.printButton1.ReportType = "";
            this.printButton1.Size = new System.Drawing.Size(75, 23);
            this.printButton1.TabIndex = 0;
            this.printButton1.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // previewButton1
            // 
            this.previewButton1.Location = new System.Drawing.Point(92, 6);
            this.previewButton1.Name = "previewButton1";
            this.previewButton1.ReportType = "";
            this.previewButton1.Size = new System.Drawing.Size(75, 23);
            this.previewButton1.TabIndex = 1;
            this.previewButton1.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // sbtnToggleOptions
            // 
            this.sbtnToggleOptions.Location = new System.Drawing.Point(248, 6);
            this.sbtnToggleOptions.Name = "sbtnToggleOptions";
            this.sbtnToggleOptions.Size = new System.Drawing.Size(75, 23);
            this.sbtnToggleOptions.TabIndex = 3;
            this.sbtnToggleOptions.Text = "Options";
            this.sbtnToggleOptions.Click += new System.EventHandler(this.sbtnToggleOptions_Click);
            // 
            // sBtnInquiry
            // 
            this.sBtnInquiry.Location = new System.Drawing.Point(11, 6);
            this.sBtnInquiry.Name = "sBtnInquiry";
            this.sBtnInquiry.Size = new System.Drawing.Size(75, 23);
            this.sBtnInquiry.TabIndex = 2;
            this.sBtnInquiry.Text = "Inquiry";
            this.sBtnInquiry.Click += new System.EventHandler(this.sBtnInquiry_Click);
            // 
            // sBtnClose
            // 
            this.sBtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sBtnClose.Location = new System.Drawing.Point(332, 6);
            this.sBtnClose.Name = "sBtnClose";
            this.sBtnClose.Size = new System.Drawing.Size(75, 23);
            this.sBtnClose.TabIndex = 4;
            this.sBtnClose.Text = "Close";
            this.sBtnClose.Click += new System.EventHandler(this.sBtnClose_Click);
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 3;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(860, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 447);
            this.barDockControlBottom.Size = new System.Drawing.Size(860, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 447);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(860, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 447);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Enabled = false;
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Item BOM Listing";
            this.panelHeader1.HelpTopicId = "BOM_Material_Usage_Inquiry.htm";
            this.panelHeader1.Location = new System.Drawing.Point(0, 0);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(860, 44);
            this.panelHeader1.TabIndex = 2;
            // 
            // FormItemBOMPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(860, 447);
            this.Controls.Add(this.panelGrid);
            this.Controls.Add(this.panButton);
            this.Controls.Add(this.panelCriteria);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormItemBOMPrint";
            this.Text = "Item BOM Listing";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormItemBOMPrint_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormItemBOMPrint_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).EndInit();
            this.panelCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkedtShowCriteriaInRpt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkedtPrintInactive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkedtPrintActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).EndInit();
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panButton)).EndInit();
            this.panButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
