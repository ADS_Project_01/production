﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.DuplicateItemCodeException
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.ContextException;
using BCE.AutoCount.Manufacturing;
using BCE.Localization;
using System;

namespace Production.GeneralMaintenance.ItemBOM
{
  [Serializable]
  public class DuplicateItemCodeException : StandardApplicationException
  {
    public DuplicateItemCodeException()
      : base(Localizer.GetString((Enum) ItemBOMMaintStringId.ErrorMessage_DuplicateItemBOMCode, new object[0]))
    {
    }
  }
}
