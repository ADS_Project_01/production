﻿// Type: BCE.AutoCount.Manufacturing.ItemBOM.ItemBOMCommandSQL
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Settings;
using BCE.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Production.GeneralMaintenance.ItemBOM
{
    public class ItemBOMCommandSQL : ItemBOMCommand
    {
        public override DataSet LoadData(long docKey)
        {
            DataSet dataSet = new DataSet();
            DataTable myItemBOMOvdTable = new DataTable();
            DataTable myItemBOMBSTable = new DataTable();
            DataTable myItemBOMAPTable = new DataTable();
            DataTable myItemBOMRepTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            string str1 = string.Empty;
            dataSet = BCE.AutoCount.Data.SqlDataAccess.LoadMasterDetailData(this.myDBSetting, docKey, "SELECT * FROM RPA_ItemBOM WHERE DocKey=@DocKey", "SELECT * FROM RPA_ItemBOMDtl WHERE DocKey=@DocKey");
            SqlConnection myconn = new SqlConnection(myDBSetting.ConnectionString);
            string sSQLItemBOMOvd = "SELECT * FROM RPA_ItemBOMOvd WHERE DocKey=@DocKey ORDER BY DtlKey";
            string sSQLItemBOMBS = "SELECT * FROM RPA_ItemBOMBS WHERE DocKey=@DocKey ORDER BY DtlKey";

            string sSQLItemBOMAP = "SELECT * FROM RPA_ItemBOMAP WHERE DocKey=@DocKey ORDER BY DtlKey";
            string sSQLItemBOMRep = "SELECT * FROM RPA_ItemBOMRep WHERE DocKey=@DocKey ORDER BY DtlKey";
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
            try
            {
                using (SqlCommand cmdItemBOMOvd = new SqlCommand(sSQLItemBOMOvd, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMOvd);
                    cmdItemBOMOvd.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMOvd.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMOvdTable);
                }
                DataColumn[] keyItemBOMOvd = new DataColumn[1];
                keyItemBOMOvd[0] = myItemBOMOvdTable.Columns["DtlKey"];
                myItemBOMOvdTable.PrimaryKey = keyItemBOMOvd;
                myItemBOMOvdTable.TableName = "OVDDetail";

                using (SqlCommand cmdItemBOMBS = new SqlCommand(sSQLItemBOMBS, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMBS);
                    cmdItemBOMBS.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMBS.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMBSTable);
                }
                DataColumn[] keycmdItemBOMBS = new DataColumn[1];
                keycmdItemBOMBS[0] = myItemBOMBSTable.Columns["DtlKey"];
                myItemBOMBSTable.PrimaryKey = keycmdItemBOMBS;
                myItemBOMBSTable.TableName = "BSDetail";

                using (SqlCommand cmdItemBOMAP = new SqlCommand(sSQLItemBOMAP, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMAP);
                    cmdItemBOMAP.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMAP.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMAPTable);
                }
                DataColumn[] keycmdItemBOMAP = new DataColumn[1];
                keycmdItemBOMAP[0] = myItemBOMAPTable.Columns["DtlKey"];
                myItemBOMAPTable.PrimaryKey = keycmdItemBOMAP;
                myItemBOMAPTable.TableName = "APDetail";

                using (SqlCommand cmdItemBOMRep = new SqlCommand(sSQLItemBOMRep, myconn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdItemBOMRep);
                    cmdItemBOMRep.Parameters.Add("@DocKey", SqlDbType.BigInt);
                    cmdItemBOMRep.Parameters["@DocKey"].Value = docKey;
                    adapter.Fill(myItemBOMRepTable);
                }
                DataColumn[] keycmdItemBOMRep = new DataColumn[1];
                keycmdItemBOMRep[0] = myItemBOMRepTable.Columns["DtlKey"];
                myItemBOMRepTable.PrimaryKey = keycmdItemBOMRep;
                myItemBOMRepTable.TableName = "RepDetail";

                dataSet.Tables.Add(myItemBOMAPTable);
                dataSet.Tables.Add(myItemBOMBSTable);
                dataSet.Tables.Add(myItemBOMOvdTable);
                dataSet.Tables.Add(myItemBOMRepTable);

            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataSet;
        }

        public override DataTable GetDetailData(DataTable masterTable)
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            DataTable dataTable1 = masterTable;
            DataTable dataTable2 = new DataTable();
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.Connection = sqlConnection;
            selectCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
            string str1 = string.Empty;
            string str2 = string.Empty;
            string str3;
            if (dataTable1.Rows.Count != 0)
            {
                for (int index = 0; index < dataTable1.Rows.Count; ++index)
                    str2 = str2 + dataTable1.Rows[index]["DocKey"].ToString() + ",";
                string str4 = str2.Substring(0, str2.Length - 1);
                str3 = "SELECT d.*, i.Description, i.Desc2 FROM RPA_ItemBOMDtl d LEFT OUTER JOIN Item i ON i.ItemCode=d.ItemCode WHERE d.DocKey IN(SELECT * FROM LIST(@DtlKeyList))";
                selectCommand.Parameters.AddWithValue("@DtlKeyList", (object)str4);
            }
            else
                str3 = "SELECT d.*, i.Description, i.Desc2 FROM RPA_ItemBOMDtl d LEFT OUTER JOIN Item i ON i.ItemCode=d.ItemCode WHERE d.DocKey=-1";
            string str5 = str3 + " ORDER BY DtlKey";
            try
            {
                sqlConnection.Open();
                selectCommand.CommandText = str5;
                new SqlDataAdapter(selectCommand).Fill(dataTable2);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataTable2;
        }

        public override DataTable GetBSDetailData(DataTable masterTable)
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            DataTable dataTable1 = masterTable;
            DataTable dataTable2 = new DataTable();
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.Connection = sqlConnection;
            selectCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
            string str1 = string.Empty;
            string str2 = string.Empty;
            string str3;
            if (dataTable1.Rows.Count != 0)
            {
                for (int index = 0; index < dataTable1.Rows.Count; ++index)
                    str2 = str2 + dataTable1.Rows[index]["DocKey"].ToString() + ",";
                string str4 = str2.Substring(0, str2.Length - 1);
                str3 = "SELECT l.*, i.Description, i.Desc2 FROM RPA_ItemBOMBS l LEFT OUTER JOIN Item i ON i.ItemCode=l.ItemCode WHERE l.DocKey IN(SELECT * FROM LIST(@DtlKeyList))";
                selectCommand.Parameters.AddWithValue("@DtlKeyList", (object)str4);
            }
            else
                str3 = "SELECT l.*, i.Description, i.Desc2 FROM RPA_ItemBOMBS l LEFT OUTER JOIN Item i ON i.ItemCode=l.ItemCode WHERE l.DocKey=-1";
            string str5 = str3 + " ORDER BY DtlKey";
            try
            {
                sqlConnection.Open();
                selectCommand.CommandText = str5;
                new SqlDataAdapter(selectCommand).Fill(dataTable2);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataTable2;
        }

        public override DataTable GetAPDetailData(DataTable masterTable)
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            DataTable dataTable1 = masterTable;
            DataTable dataTable2 = new DataTable();
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.Connection = sqlConnection;
            selectCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
            string str1 = string.Empty;
            string str2 = string.Empty;
            string str3;
            if (dataTable1.Rows.Count != 0)
            {
                for (int index = 0; index < dataTable1.Rows.Count; ++index)
                    str2 = str2 + dataTable1.Rows[index]["DocKey"].ToString() + ",";
                string str4 = str2.Substring(0, str2.Length - 1);
                str3 = "SELECT l.*, i.Description, i.Desc2 FROM RPA_ItemBOMAP l LEFT OUTER JOIN Item i ON i.ItemCode=l.ItemCode WHERE l.DocKey IN(SELECT * FROM LIST(@DtlKeyList))";
                selectCommand.Parameters.AddWithValue("@DtlKeyList", (object)str4);
            }
            else
                str3 = "SELECT l.*, i.Description, i.Desc2 FROM RPA_ItemBOMAP l LEFT OUTER JOIN Item i ON i.ItemCode=l.ItemCode WHERE l.DocKey=-1";
            string str5 = str3 + " ORDER BY DtlKey";
            try
            {
                sqlConnection.Open();
                selectCommand.CommandText = str5;
                new SqlDataAdapter(selectCommand).Fill(dataTable2);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataTable2;
        }
        public override DataTable GetOvdDetailData(DataTable masterTable)
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            DataTable dataTable1 = masterTable;
            DataTable dataTable2 = new DataTable();
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.Connection = sqlConnection;
            selectCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
            string str1 = string.Empty;
            string str2 = string.Empty;
            string str3;
            if (dataTable1.Rows.Count != 0)
            {
                for (int index = 0; index < dataTable1.Rows.Count; ++index)
                    str2 = str2 + dataTable1.Rows[index]["DocKey"].ToString() + ",";
                string str4 = str2.Substring(0, str2.Length - 1);
                str3 = "SELECT * FROM RPA_ItemBOMOvd  WHERE DocKey IN(SELECT * FROM LIST(@DtlKeyList))";
                selectCommand.Parameters.AddWithValue("@DtlKeyList", (object)str4);
            }
            else
                str3 = "SELECT * FROM RPA_ItemBOMOvd WHERE DocKey=-1";
            string str5 = str3 + " ORDER BY DtlKey";
            try
            {
                sqlConnection.Open();
                selectCommand.CommandText = str5;
                new SqlDataAdapter(selectCommand).Fill(dataTable2);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataTable2;
        }


        public override DataTable GetRepDetailData(DataTable masterTable)
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            DataTable dataTable1 = masterTable;
            DataTable dataTable2 = new DataTable();
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.Connection = sqlConnection;
            selectCommand.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
            string str1 = string.Empty;
            string str2 = string.Empty;
            string str3;
            if (dataTable1.Rows.Count != 0)
            {
                for (int index = 0; index < dataTable1.Rows.Count; ++index)
                    str2 = str2 + dataTable1.Rows[index]["DocKey"].ToString() + ",";
                string str4 = str2.Substring(0, str2.Length - 1);
                str3 = "SELECT * FROM RPA_ItemBOMRep  WHERE DocKey IN(SELECT * FROM LIST(@DtlKeyList))";
                selectCommand.Parameters.AddWithValue("@DtlKeyList", (object)str4);
            }
            else
                str3 = "SELECT * FROM RPA_ItemBOMRep WHERE DocKey=-1";
            string str5 = str3 + " ORDER BY DtlKey";
            try
            {
                sqlConnection.Open();
                selectCommand.CommandText = str5;
                new SqlDataAdapter(selectCommand).Fill(dataTable2);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataTable2;
        }

        public override void BrowseAll(string sql)
        {
            SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
            try
            {
                connection.Open();
                SqlCommand selectCommand = new SqlCommand(sql, connection);
                this.myDtblBrowse.Clear();
                new SqlDataAdapter(selectCommand).Fill(this.myDtblBrowse);
                if (this.myDtblBrowse.PrimaryKey.Length == 0)
                {
                    DataTable dataTable = this.myDtblBrowse;
                    DataColumn[] dataColumnArray = new DataColumn[1];
                    int index = 0;
                    DataColumn dataColumn = this.myDtblBrowse.Columns["DocKey"];
                    dataColumnArray[index] = dataColumn;
                    dataTable.PrimaryKey = dataColumnArray;
                }
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        public override void SaveData(ItemBOMEntity entity)
        {
            DataTable masterTable = entity.MasterTable;
            DataTable table = entity.MasterTable.Copy();
            DataTable detailTable = entity.DetailTable;
            DataTable APDetailTable = entity.APDetailTable;
            DataTable BSDetailTable = entity.BSDetailTable;
            DataTable OVDDetailTable = entity.OvdDetailTable;
            DataTable RepDetailTable = entity.RepDetailTable;
            DataRow drmaster = masterTable.Rows[0];
            if (this.myAction == ItemBOMCommand.Action.New)
            {
                DBSetting dbSetting = this.myDBSetting;
                string cmdText = "SELECT COUNT(*) FROM RPA_ItemBOM WHERE BOMCode=?";
                object[] objArray = new object[1];
                int index = 0;
                object bomOptionalCode = entity.BOMCode;
                objArray[index] = bomOptionalCode;
                if ((int)System.Convert.ToInt16(dbSetting.ExecuteScalar(cmdText, objArray)) > 0)
                    throw new DuplicateItemCodeException();
            }
            else
            {
                drmaster["LastModified"] = System.DateTime.Now;
                drmaster["LastModifiedUserID"] = BCE.AutoCount.Authentication.UserAuthentication.GetOrCreate(myDBSetting).LoginUserID;

            }
            foreach (DataRow dataRow in (InternalDataCollectionBase)detailTable.Rows)
            {
                if (dataRow.RowState != DataRowState.Deleted && dataRow["ItemCode"].ToString().Trim().Length == 0)
                    throw new EmptySubItemCodeException();
            }
            foreach (DataRow dataRow in (InternalDataCollectionBase)BSDetailTable.Rows)
            {
                if (dataRow.RowState != DataRowState.Deleted && dataRow["ItemCode"].ToString().Trim().Length == 0)
                    throw new EmptyBomItemCodeException();
            }
            foreach (DataRow dataRow in (InternalDataCollectionBase)APDetailTable.Rows)
            {
                if (dataRow.RowState != DataRowState.Deleted && dataRow["ItemCode"].ToString().Trim().Length == 0)
                    throw new EmptyBomItemCodeException();
            }
            foreach (DataRow dataRow in (InternalDataCollectionBase)OVDDetailTable.Rows)
            {
                if (dataRow.RowState != DataRowState.Deleted && dataRow["OverheadCode"].ToString().Trim().Length == 0)
                    throw new EmptyBomItemCodeException();
            }

            foreach (DataRow dataRow in (InternalDataCollectionBase)RepDetailTable.Rows)
            {
                if (dataRow.RowState != DataRowState.Deleted && dataRow["ItemCode"].ToString().Trim().Length == 0)
                    throw new EmptyCodeException();
            }
            DBSetting dbSetting1 = this.myDBSetting.StartTransaction();
            try
            {
                table.Rows[0]["LastUpdate"] = (object)(BCE.Data.Convert.ToInt32(table.Rows[0]["LastUpdate"]) + 1);
                dbSetting1.SimpleSaveDataTable(table, "SELECT * FROM RPA_ItemBOM");
                dbSetting1.SimpleSaveDataTable(detailTable, "SELECT * FROM RPA_ItemBOMDTL");
                dbSetting1.SimpleSaveDataTable(BSDetailTable, "SELECT * FROM RPA_ItemBOMBS");
                dbSetting1.SimpleSaveDataTable(APDetailTable, "SELECT * FROM RPA_ItemBOMAP");
                dbSetting1.SimpleSaveDataTable(OVDDetailTable, "SELECT * FROM RPA_ItemBOMOvd");
                dbSetting1.SimpleSaveDataTable(RepDetailTable, "SELECT * FROM RPA_ItemBOMRep");
                dbSetting1.Commit();
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                dbSetting1.EndTransaction();
            }
            if (this.myAction == ItemBOMCommand.Action.New)
                masterTable.Rows[0]["LastUpdate"] = (object)table.Rows[0]["LastUpdate"].ToString();
            masterTable.AcceptChanges();
            if (this.myFormOwner != null && this.myFormOwner.InvokeRequired)
            {
                Form form = this.myFormOwner;
                ItemBOMCommand.OnUpdateBrowseDelegate updateBrowseDelegate = new ItemBOMCommand.OnUpdateBrowseDelegate(this.UpdateBrowseTable);
                object[] objArray = new object[1];
                int index = 0;
                ItemBOMEntity bomOptionalEntity = entity;
                objArray[index] = (object)bomOptionalEntity;
                form.Invoke((Delegate)updateBrowseDelegate, objArray);
            }
            else if (this.myDtblBrowse.PrimaryKey.Length != 0)
                this.UpdateBrowseTable(entity);
        }

        private void UpdateBrowseTable(ItemBOMEntity entity)
        {
            DataTable dataTable = entity.DataSetItemBOM.Tables["Master"];
            if (entity.Action == ItemBOMCommand.Action.New)
            {
                SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                DataRow row = this.myDtblBrowse.NewRow();
                foreach (DataColumn dataColumn in (InternalDataCollectionBase)dataTable.Columns)
                {
                    if (this.myDtblBrowse.Columns.Contains(dataColumn.ColumnName))
                        row[dataColumn.ColumnName] = dataTable.Rows[0][dataColumn.ColumnName];
                }
                this.myDtblBrowse.Rows.Add(row);
            }
            else
            {
                DataRow dataRow = this.myDtblBrowse.Rows.Find(entity.DocKey);
                if (dataRow != null)
                {
                    foreach (DataColumn dataColumn in (InternalDataCollectionBase)dataTable.Columns)
                    {
                        if (this.myDtblBrowse.Columns.Contains(dataColumn.ColumnName))
                            dataRow[dataColumn.ColumnName] = dataTable.Rows[0][dataColumn.ColumnName];
                    }
                }
            }
            this.myDtblBrowse.AcceptChanges();
        }

        private void DeleteBrowseTable(long[] DocKey)
        {
            foreach (long num in DocKey)
            {
                DataRow dataRow = this.myDtblBrowse.Rows.Find((object)num);
                if (dataRow != null)
                    dataRow.Delete();
            }
            this.myDtblBrowse.AcceptChanges();
        }

        public override void DeleteData(long[] docKey)
        {
            DBSetting dbSetting = this.myDBSetting.StartTransaction();
            try
            {
                SqlCommand command = dbSetting.CreateCommand();
                string format1 = "DELETE FROM RPA_ItemBOM WHERE DocKey IN({0})";
                string format2 = "DELETE FROM RPA_ItemBOMDtl WHERE DocKey IN({0})";
                string format3 = "DELETE FROM RPA_ItemBOMOvd WHERE DocKey IN({0})";
                string format4 = "DELETE FROM RPA_ItemBOMAP WHERE DocKey IN({0})";
                string format5 = "DELETE FROM RPA_ItemBOMBS WHERE DocKey IN({0})";
                string str1 = "";
                for (int index1 = 0; index1 < docKey.Length; ++index1)
                {
                    command.Parameters.AddWithValue("@DocKey" + (object)index1, (object)docKey[index1]);
                    object[] objArray = new object[4];
                    int index2 = 0;
                    string str2 = str1;
                    objArray[index2] = (object)str2;
                    int index3 = 1;
                    string str3 = "@DocKey";
                    objArray[index3] = (object)str3;
                    int index4 = 2;
                    // ISSUE: variable of a boxed type
                    int local = index1;
                    objArray[index4] = (object)local;
                    int index5 = 3;
                    string str4 = ",";
                    objArray[index5] = (object)str4;
                    str1 = string.Concat(objArray);
                }
                string str5 = str1.Substring(0, str1.Length - 1);
                string str6 = string.Format(format1, (object)str5);
                string str7 = string.Format(format2, (object)str5);
                string str8 = string.Format(format3, (object)str5);
                string str9 = string.Format(format4, (object)str5);
                string str10 = string.Format(format5, (object)str5);
                command.CommandText = str10;
                command.ExecuteNonQuery();
                command.CommandText = str9;
                command.ExecuteNonQuery();
                command.CommandText = str7;
                command.ExecuteNonQuery();
                command.CommandText = str8;
                command.ExecuteNonQuery();
                command.CommandText = str6;
                command.ExecuteNonQuery();
                dbSetting.Commit();
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                dbSetting.EndTransaction();
            }
            if (this.myFormOwner != null & this.myFormOwner.InvokeRequired)
            {
                Form form = this.myFormOwner;
                ItemBOMCommand.OnDeleteBrowseDelegate deleteBrowseDelegate = new ItemBOMCommand.OnDeleteBrowseDelegate(this.DeleteBrowseTable);
                object[] objArray = new object[1];
                int index = 0;
                long[] numArray = docKey;
                objArray[index] = (object)numArray;
                form.Invoke((Delegate)deleteBrowseDelegate, objArray);
            }
            else
                this.DeleteBrowseTable(docKey);
        }

        public override DataSet BrowseSearchData(SqlCommand cmd, SearchCriteria criteria, string strSQL, string strOtherWhereSQL)
        {
            SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
            DataSet dataSet = new DataSet();
            string str = criteria.BuildSQL((IDbCommand)cmd);
            try
            {
                sqlConnection.Open();
                cmd.Connection = sqlConnection;
                if (str != string.Empty)
                    strSQL = strSQL + " WHERE " + str;
                if (((object)strOtherWhereSQL).ToString().Trim() != string.Empty)
                    strSQL = !(str != string.Empty) ? strSQL + " WHERE " + strOtherWhereSQL : strSQL + " AND " + strOtherWhereSQL;
                cmd.CommandText = strSQL;
                cmd.CommandTimeout = this.myGeneralSetting.SearchCommandTimeout;
                new SqlDataAdapter(cmd).Fill(dataSet, "Master");
                DataTable detailData = this.GetDetailData(dataSet.Tables["Master"]);
                detailData.TableName = "Detail";
                dataSet.Tables.Add(detailData);
                DataTable BSDetailData = this.GetBSDetailData(dataSet.Tables["Master"]);
                BSDetailData.TableName = "BSDetail";

                DataTable APDetailData = this.GetAPDetailData(dataSet.Tables["Master"]);
                APDetailData.TableName = "APDetail";

                DataTable OvdDetailData = this.GetOvdDetailData(dataSet.Tables["Master"]);
                OvdDetailData.TableName = "OVDDetail";

                dataSet.Tables.Add(BSDetailData);
                dataSet.Tables.Add(APDetailData);
                dataSet.Tables.Add(OvdDetailData);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataSet;
        }

        public override DataRow GetItemDtl(object itemCode, object uom)
        {
            DataTable dataTable = new DataTable();
            SqlConnection connection = new SqlConnection(this.myDBSetting.ConnectionString);
            SqlCommand selectCommand = new SqlCommand("SELECT i.Description, u.Price, u.Cost FROM Item i LEFT OUTER JOIN ItemUOM u ON u.ItemCode=i.ItemCode WHERE i.ItemCode=@ItemCode AND u.UOM = @UOM", connection);
            try
            {
                connection.Open();
                selectCommand.Parameters.AddWithValue("@ItemCode", itemCode);
                selectCommand.Parameters.AddWithValue("@UOM", uom);
                new SqlDataAdapter(selectCommand).Fill(dataTable);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
            if (dataTable.Rows.Count != 0)
                return dataTable.Rows[0];
            else
                return (DataRow)null;
        }

        public static DataTable GetItemBOM(string BOMItemCode, DBSetting dbSetting)
        {
            DataTable dataTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(dbSetting.ConnectionString);
            string str = "SELECT 'F' as Tick, null as Qty, b.DocKey, b.BOMCode, b.Description, b.Desc2 FROM RPA_ItemBOMLink l LEFT OUTER JOIN ItemBOM b ON b.DocKey=l.DocKey WHERE l.BOMItemCode=@BOMItemCode AND b.IsActive='T'";
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.CommandTimeout = GeneralSetting.GetOrCreate(dbSetting).SearchCommandTimeout;
            try
            {
                sqlConnection.Open();
                selectCommand.Connection = sqlConnection;
                selectCommand.Parameters.AddWithValue("@BOMItemCode", (object)BOMItemCode);
                selectCommand.CommandText = str;
                new SqlDataAdapter(selectCommand).Fill(dataTable);
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataTable;
        }

        public static DataTable GetItemBOMItem(DataTable dtblItemBOM, DataRow[] oldItemBOM, DBSetting dbSetting)
        {
            DataTable dataTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(dbSetting.ConnectionString);
            string format = "SELECT DocKey, SubItemCode, Qty AS Rate, Qty*{0} AS Qty, OverheadCost*{0} AS OverheadCost FROM RPA_ItemBOMDtl WHERE DocKey=@DocKey";
            SqlCommand selectCommand = new SqlCommand();
            selectCommand.CommandTimeout = GeneralSetting.GetOrCreate(dbSetting).SearchCommandTimeout;
            try
            {
                sqlConnection.Open();
                selectCommand.Connection = sqlConnection;
                selectCommand.Parameters.AddWithValue("@DocKey", (object)"");
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
                try
                {
                    foreach (DataRow dataRow1 in (InternalDataCollectionBase)dtblItemBOM.Rows)
                    {
                        Decimal num = BCE.Data.Convert.ToDecimal(dataRow1["Qty"]);
                        foreach (DataRow dataRow2 in oldItemBOM)
                        {
                            if (BCE.Data.Convert.ToInt64(dataRow2["DocKey"]) == BCE.Data.Convert.ToInt64(dataRow1["DocKey"]))
                            {
                                num -= (Decimal)BCE.Data.Convert.ToInt32(dataRow2["Qty"]);
                                break;
                            }
                        }
                        if (num != Decimal.Zero)
                        {
                            string str = string.Format(format, (object)num);
                            selectCommand.Parameters["@DocKey"].Value = dataRow1["DocKey"];
                            selectCommand.CommandText = str;
                            sqlDataAdapter.SelectCommand = selectCommand;
                            sqlDataAdapter.Fill(dataTable);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    BCE.Data.DataError.HandleSqlException(ex);
                    throw;
                }
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
                throw;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return dataTable;
        }
    }
}
