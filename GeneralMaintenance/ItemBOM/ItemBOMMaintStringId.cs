﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BCE.Localization;
namespace Production.GeneralMaintenance.ItemBOM
{
    [LocalizableString]
    public enum ItemBOMMaintStringId
    {
        [DefaultString("{0} selected")] _Selected,
        [DefaultString("No record was selected.")] ErrorMessage_RecordNotSelected,
        [DefaultString("Cannot edit read-only Item BOM.")] ErrorMessage_CannotEdit,
        [DefaultString("Empty Sub Item is not allowed.")] ErrorMessage_NotAllowEmptySubItem,
        [DefaultString("Empty BOM Item Code is not allowed.")] ErrorMessage_NotAllowEmptyBOM,
        [DefaultString("Duplicate Item BOM Code, it already exist.")] ErrorMessage_DuplicateItemBOMCode,
        [DefaultString("Cannot find record, the record might be deleted.")] ErrorMessage_RecordsNotExist,
        [DefaultString("One of the records was deleted, operation aborted.")] ErrorMessage_OperationAborted,
        [DefaultString("Please enter Item BOM Code.")] ErrorMessage_EnterBomOptionalCode,
        [DefaultString("The record was modified by other, it is not possible be saved again.")] ErrorMessage_RecordWasModified,
        [DefaultString("The Item BOM Code already exist in the system.")] ErrorMessage_ItemBOMCodeExist,
        [DefaultString("Initialize method has not been called.")] ErrorMessage_InitializeMethodNotCalled,
        [DefaultString("Do you really want to delete Item BOM [{0}]?")] ConfirmMessage_DeleteItemBOM,
        [DefaultString("Record were successfully deleted.")] ShowMessage_DeleteSuccessfully,
        [DefaultString("{0} record(s) are successfully deleted")] ShowMessage_TotalDeletedSuccessfully,
        [DefaultString("Detail BOM {0} Record, Save aborted.")] ErrorMessage_DetailBOM,
        [DefaultString("Percent must equal 100 %, Save aborted.")] ErrorMessage_PercentBOM,
        [DefaultString("New Item BOM")] Code_NewItemBOM,
        [DefaultString("View Item BOM")] Code_ViewItemBOM,
        [DefaultString("Edit Item BOM")] Code_EditItemBOM,
        [DefaultString("Hide &Options")] Code_HideOptions,
        [DefaultString("Show &Options")] Code_ShowOptions,
    }
}
