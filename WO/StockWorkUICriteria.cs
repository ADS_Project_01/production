﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyUICriteria
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using System;

namespace RPASystem.WorkOrder
{
  [Serializable]
  public class StockWorkUICriteria
  {
    public bool Description;
    public bool DocNo;
    public bool DocDate;
    public bool RefDocNo;
  }
}
