﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderRecord
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Common;
using BCE.AutoCount.Data;
using BCE.Data;
using BCE.Misc;
using System;
using System.Data;

namespace RPASystem.WorkOrder
{
  public class StockWorkOrderRecord : BaseRecord
  {
    private StockWorkOrder myStock;
    protected DataRow[] myDetailRows;

    public int DetailCount
    {
      get
      {
        return this.myDetailRows.Length;
      }
    }

    public long DocKey
    {
      get
      {
        return BCE.Data.Convert.ToInt64(this.myRow["DocKey"]);
      }
    }

    public DBString DocNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["DocNo"]);
      }
      set
      {
        this.myRow["DocNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBDateTime DocDate
    {
      get
      {
        return BCE.Data.Convert.ToDBDateTime(this.myRow["DocDate"]);
      }
      set
      {
        if (value.HasValue)
          this.myRow["DocDate"] = BCE.Data.Convert.ToDBObject((DBDateTime)value);
        else
          this.myRow["DocDate"] = (object) DBNull.Value;
      }
    }

    public DBString Description
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Description"]);
      }
      set
      {
        this.myRow["Description"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString ItemCode
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["ItemCode"]);
      }
      set
      {
        this.myRow["ItemCode"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Location
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Location"]);
      }
      set
      {
        this.myRow["Location"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString BatchNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["BatchNo"]);
      }
      set
      {
        this.myRow["BatchNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString ProjNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["ProjNo"]);
      }
      set
      {
        this.myRow["ProjNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString DeptNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["DeptNo"]);
      }
      set
      {
        this.myRow["DeptNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBDecimal Qty
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["Qty"]);
      }
      set
      {
        this.myRow["Qty"] = this.myStock.Command.DecimalSetting.RoundToQuantityDBObject(value);
      }
    }

    public DBDecimal Total
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["Total"]);
      }
    }

    public DBDecimal AssemblyCost
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["AssemblyCost"]);
      }
      set
      {
        this.myRow["AssemblyCost"] = this.myStock.Command.DecimalSetting.RoundToCostDBObject(value);
      }
    }

    public DBDecimal NetTotal
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["NetTotal"]);
      }
    }

    public DBString Note
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Note"]);
      }
      set
      {
        if (value.HasValue)
          this.myRow["Note"] = (object) Rtf.ToArialRichText((string) value);
        else
          this.myRow["Note"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Remark1
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Remark1"]);
      }
      set
      {
        this.myRow["Remark1"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Remark2
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Remark2"]);
      }
      set
      {
        this.myRow["Remark2"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Remark3
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Remark3"]);
      }
      set
      {
        this.myRow["Remark3"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Remark4
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Remark4"]);
      }
      set
      {
        this.myRow["Remark4"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public short PrintCount
    {
      get
      {
        return BCE.Data.Convert.ToInt16(this.myRow["PrintCount"]);
      }
    }

    public bool Cancelled
    {
      get
      {
        return BCE.Data.Convert.TextToBoolean(this.myRow["Cancelled"]);
      }
    }

    public DBDateTime LastModified
    {
      get
      {
        return BCE.Data.Convert.ToDBDateTime(this.myRow["LastModified"]);
      }
    }

    public DBString LastModifiedUserID
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["LastModifiedUserID"]);
      }
    }

    public DBDateTime CreatedTimeStamp
    {
      get
      {
        return BCE.Data.Convert.ToDBDateTime(this.myRow["CreatedTimeStamp"]);
      }
    }

    public DBString CreatedUserID
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["CreatedUserID"]);
      }
    }

    public DBString RefDocNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["RefDocNo"]);
      }
      set
      {
        this.myRow["RefDocNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public bool CanSync
    {
      get
      {
        return BCE.Data.Convert.TextToBoolean(this.myRow["CanSync"]);
      }
      set
      {
        this.myRow["CanSync"] = (object) BCE.Data.Convert.BooleanToText(value);
      }
    }

    public DBString ExternalLinkText
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["ExternalLink"]);
      }
      set
      {
        this.myRow["ExternalLink"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public ExternalLink ExternalLink
    {
      get
      {
        return new ExternalLink(this.myRow, "ExternalLink");
      }
    }

    internal StockWorkOrderRecord(StockWorkOrder doc)
      : base(doc.MasterRow)
    {
      this.myStock = doc;
      this.myDetailRows = doc.GetValidDetailRows();
    }

    public StockWorkOrderDetailRecord GetDetailRecord(int index)
    {
      return new StockWorkOrderDetailRecord(this.myStock.Command.DBSetting, this.myDetailRows[index]);
    }

    public StockWorkOrderDetailRecord NewDetail()
    {
      StockWorkOrderDetail assemblyOrderDetail = this.myStock.AddDetail();
      this.myDetailRows = this.myStock.GetValidDetailRows();
      return new StockWorkOrderDetailRecord(this.myStock.Command.DBSetting, assemblyOrderDetail.Row);
    }

    public bool DeleteDetailRecord(int index)
    {
      if (index < 0 || index >= this.myDetailRows.Length)
        throw new ArgumentException("index not in valid range.");
      else if (this.myStock.DeleteDetail(BCE.Data.Convert.ToInt64(this.myDetailRows[index]["DtlKey"])))
      {
        this.myDetailRows = this.myStock.GetValidDetailRows();
        return true;
      }
      else
        return false;
    }

    public void SetDocNoFormatName(string formatName)
    {
      this.myStock.DocNoFormatName = formatName;
    }
  }
}
