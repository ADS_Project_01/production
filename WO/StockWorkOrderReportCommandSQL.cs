﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderReportCommandSQL
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Settings;
using BCE.AutoCount.UDF;
using BCE.Data;
using System;
using System.Data;
using System.Data.SqlClient;

namespace RPASystem.WorkOrder
{
  public class StockWorkOrderReportCommandSQL : StockWorkOrderReportCommand
  {
    protected override DataSet LoadDetailListingReportData(string dtlKeys, StockWorkOrderDetailReportingCriteria criteria)
    {
      string format = "Select A.*, B.* {0} {1} From vStockAssemblyOrder A Inner Join vStockAssemblyOrderDetail B On (A.DocKey = B.DocKey) Where B.DtlKey In (SELECT * FROM LIST(@DtlKeyList)) AND B.UOM = B.ItemBaseUOM";
      string str1 = "";
      string str2 = "";
      if (criteria.SortBy == "Document No")
        str1 = ",A.DocNo AS SortID";
      else if (criteria.SortBy == "Date")
        str1 = ", (cast(year(A.DocDate) as varchar(4)) + case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end + case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end )AS SortID ";
      else if (criteria.SortBy == "Item Code")
        str1 = ",B.ItemCode AS SortID";
      else if (criteria.SortBy == "Item Group")
        str1 = ",B.ItemGroup AS SortID";
      else if (criteria.SortBy == "Item Type")
        str1 = ",B.ItemType AS SortID";
      else if (criteria.SortBy == "Location")
        str1 = ",B.Location AS SortID";
      else if (criteria.SortBy == "Project No")
        str1 = ",B.ProjNo AS SortID";
      else if (criteria.SortBy == "Department No")
        str1 = ",B.DeptNo AS SortID";
      if (criteria.GroupBy == "Date")
        str2 = ",(cast(year(A.DocDate) as varchar(4)) +'/'+ case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end +'/'+ case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end ) AS GroupID, 'Date' AS GroupIDName, (case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end  + case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end + cast(year(A.DocDate) as varchar(4))) AS GroupIDDisplay, 'Group By ' + (case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end +'/'+ case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end +'/'+ cast(year(A.DocDate) as varchar(4))) As GroupIDDescription ";
      else if (criteria.GroupBy == "Month")
        str2 = ",cast(year(A.DocDate) as varchar(4)) + (case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2))end ) AS GroupID, 'Month' AS GroupIDName, DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4)) AS GroupIDDisplay, 'Group By ' + DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4)) AS GroupIDDescription ";
      else if (criteria.GroupBy == "Year")
        str2 = ",cast(year(A.DocDate) as varchar(4)) AS GroupID, 'Year' AS GroupIDName, cast(year(A.DocDate) as varchar(4)) AS GroupIDDisplay, 'Group By ' + cast(year(A.DocDate) AS varchar(4)) AS GroupIDDescription ";
      else if (criteria.GroupBy == "Item Code")
        str2 = ",B.ItemCode AS GroupID, 'Item Code' AS GroupIDName, B.ItemCode AS GroupIDDisplay, B.ItemDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == "Item Group")
        str2 = ",B.ItemGroup AS GroupID, 'Item Group' AS GroupIDName, B.ItemGroup AS GroupIDDisplay, B.ItemGroupDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == "Item Type")
        str2 = ",B.ItemType AS GroupID, 'Item Type' AS GroupIDName, B.ItemType AS GroupIDDisplay, B.ItemTypeDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == "Location")
        str2 = ",B.Location AS GroupID, 'Location' AS GroupIDName, B.Location AS GroupIDDisplay, B.LocationDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == "Project No")
        str2 = ",B.ProjNo AS GroupID, 'Project No' AS GroupIDName, B.ProjNo AS GroupIDDisplay, B.ProjectDescription AS GroupIDDescription ";
      else if (criteria.GroupBy == "Department No")
        str2 = ",B.DeptNo AS GroupID, 'Department No' AS GroupIDName, B.DeptNo AS GroupIDDisplay, B.DeptDescription AS GroupIDDescription ";
      string str3 = string.Format(format, (object) str1, (object) str2);
      DataSet dataSet = new DataSet();
      DBSetting dbSetting = this.myDBSetting;
      DataSet ds = dataSet;
      string tableName = "Master";
      string cmdText = str3;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DtlKeyList", (object) dtlKeys);
      objArray[index] = (object) sqlParameter;
      dbSetting.LoadDataSet(ds, tableName, cmdText, num != 0, objArray);
      return dataSet;
    }

    protected override DataSet LoadDetailListingReportDesignerData()
    {
      string cmdText = string.Format("Select TOP 100 A.*, B.* {0} {1} from vStockAssembly A inner join vStockAssemblyDetail B on (A.DocKey = B.DocKey)", (object) ", (cast(year(A.DocDate) as varchar(4)) + case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end + case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end )AS SortID ", (object) ",B.ItemCode AS GroupID, 'Item Code' AS GroupIDName, B.ItemCode AS GroupIDDisplay, B.ItemDescription AS GroupIDDescription ");
      DataSet ds = new DataSet();
      this.myDBSetting.LoadDataSet(ds, "Master", cmdText, false, new object[0]);
      return ds;
    }

    public override void DetailListingAdvanceSearch(AdvancedStockWorkOrderCriteria criteria, DataSet newDS, string checkEditColumnName)
    {
      if (newDS.Relations.Contains("MasterDetailRelation") && newDS.Relations.CanRemove(newDS.Relations["MasterDetailRelation"]))
        newDS.Relations.Clear();
      DataTable dataTable1 = newDS.Tables["Master"];
      DataTable dataTable2 = newDS.Tables["Detail"];
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      string str1 = "";
      UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
      foreach (UDFColumn udfColumn in udfUtil.GetUDF("ASMORDER"))
      {
        string[] strArray = new string[6];
        int index1 = 0;
        string str2 = str1;
        strArray[index1] = str2;
        int index2 = 1;
        string str3 = " , A.";
        strArray[index2] = str3;
        int index3 = 2;
        string fieldName1 = udfColumn.FieldName;
        strArray[index3] = fieldName1;
        int index4 = 3;
        string str4 = " AS ASMORDER";
        strArray[index4] = str4;
        int index5 = 4;
        string fieldName2 = udfColumn.FieldName;
        strArray[index5] = fieldName2;
        int index6 = 5;
        string str5 = " ";
        strArray[index6] = str5;
        str1 = string.Concat(strArray);
      }
      foreach (UDFColumn udfColumn in udfUtil.GetUDF("ASMORDERDTL"))
      {
        string[] strArray = new string[6];
        int index1 = 0;
        string str2 = str1;
        strArray[index1] = str2;
        int index2 = 1;
        string str3 = " , B.";
        strArray[index2] = str3;
        int index3 = 2;
        string fieldName1 = udfColumn.FieldName;
        strArray[index3] = fieldName1;
        int index4 = 3;
        string str4 = " AS ASMORDERDTL";
        strArray[index4] = str4;
        int index5 = 4;
        string fieldName2 = udfColumn.FieldName;
        strArray[index5] = fieldName2;
        int index6 = 5;
        string str5 = " ";
        strArray[index6] = str5;
        str1 = string.Concat(strArray);
      }
      string format = " Select A.*, B.Description AS DtlDescription, B.*, D.ItemType, D.ItemGroup, Project.Description AS ProjDesc, Dept.Description AS DeptDesc " + str1 + " From ASMORDER A  Inner Join ASMORDERDTL B on A.DocKey = B.DocKey  Left Outer Join Item D On (B.ItemCode = D.ItemCode)  LEFT OUTER JOIN Project On (B.ProjNo = Project.ProjNo)  LEFT OUTER JOIN Dept On (B.DeptNo = Dept.DeptNo)  Where {0} ";
      try
      {
        SqlCommand selectCommand = new SqlCommand();
        selectCommand.Connection = sqlConnection;
        string str2 = criteria.BuildSQL((IDbCommand) selectCommand);
        if (str2 == string.Empty)
          str2 = "(1=1)";
        string str3 = string.Format(format, (object) str2);
        selectCommand.CommandText = str3;
        selectCommand.CommandTimeout = GeneralSetting.GetOrCreate(this.myDBSetting).SearchCommandTimeout;
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
        {
          newDS.Tables["Detail"].Clear();
          newDS.Tables["Master"].Clear();
        }
        else if (checkEditColumnName.Length > 0 && dataTable1.Rows.Count > 0)
        {
          DataRow[] dataRowArray = dataTable1.Select(string.Format("{0} = true", (object) checkEditColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index = 0; index < dataRowArray.Length; ++index)
            numArray[index] = BCE.Data.Convert.ToInt64(dataRowArray[index]["DtlKey"]);
        }
        sqlDataAdapter.Fill(dataTable1);
        if (dataTable1.PrimaryKey.Length == 0)
        {
          DataTable dataTable3 = dataTable1;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index = 0;
          DataColumn dataColumn = dataTable1.Columns["DtlKey"];
          dataColumnArray[index] = dataColumn;
          dataTable3.PrimaryKey = dataColumnArray;
        }
        if (numArray != null)
        {
          foreach (long num in numArray)
          {
            DataRow dataRow = dataTable1.Rows.Find((object) num);
            if (dataRow != null)
              dataRow[checkEditColumnName] = (object) true;
          }
        }
      }
      catch (Exception ex)
      {
        if (ex is SqlException)
          BCE.Data.DataError.HandleSqlException((SqlException) ex);
        if (!(ex is ConstraintException))
          throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }

    public override void DetailListingBasicSearch(StockWorkOrderDetailReportingCriteria criteria, string columnName, DataSet newDS, string chkEditColumnName)
    {
      if (newDS.Relations.Contains("MasterDetailRelation") && newDS.Relations.CanRemove(newDS.Relations["MasterDetailRelation"]))
        newDS.Relations.Clear();
      DataTable dataTable1 = newDS.Tables["Master"];
      DataTable dataTable2 = newDS.Tables["Detail"];
      string format = " SELECT {0}, ASMORDERDTL.DtlKey, D.ItemGroup, D.ItemType FROM ASMORDER  INNER JOIN ASMORDERDTL ON ASMORDER.DocKey = ASMORDERDTL.DocKey  LEFT OUTER JOIN Item D ON (ASMORDERDTL.ItemCode = D.ItemCode)  LEFT OUTER JOIN Project On (ASMORDERDTL.ProjNo = Project.ProjNo)  LEFT OUTER JOIN Dept On (ASMORDERDTL.DeptNo = Dept.DeptNo)  WHERE {1}";
      string str1 = columnName;
      char[] chArray = new char[1];
      int index1 = 0;
      int num1 = 44;
      chArray[index1] = (char) num1;
      string[] strArray1 = str1.Split(chArray);
      columnName = string.Empty;
      string[] strArray2 = new string[14];
      int index2 = 0;
      string str2 = "ItemCode";
      strArray2[index2] = str2;
      int index3 = 1;
      string str3 = "Location";
      strArray2[index3] = str3;
      int index4 = 2;
      string str4 = "BatchNo";
      strArray2[index4] = str4;
      int index5 = 3;
      string str5 = "DtlDescription";
      strArray2[index5] = str5;
      int index6 = 4;
      string str6 = "FurtherDescription";
      strArray2[index6] = str6;
      int index7 = 5;
      string str7 = "ProjNo";
      strArray2[index7] = str7;
      int index8 = 6;
      string str8 = "DeptNo";
      strArray2[index8] = str8;
      int index9 = 7;
      string str9 = "Rate";
      strArray2[index9] = str9;
      int index10 = 8;
      string str10 = "Qty";
      strArray2[index10] = str10;
      int index11 = 9;
      string str11 = "ItemCost";
      strArray2[index11] = str11;
      int index12 = 10;
      string str12 = "OverHeadCost";
      strArray2[index12] = str12;
      int index13 = 11;
      string str13 = "SubTotalCost";
      strArray2[index13] = str13;
      int index14 = 12;
      string str14 = "ProjDesc";
      strArray2[index14] = str14;
      int index15 = 13;
      string str15 = "DeptDesc";
      strArray2[index15] = str15;
      string[] strArray3 = strArray2;
      foreach (string str16 in strArray1)
      {
        bool flag = false;
        string oldValue = str16.Trim();
        if (!(oldValue == "ItemGroup") && !(oldValue == "ItemType"))
        {
          foreach (string str17 in strArray3)
          {
            if (str17 == oldValue)
            {
              flag = true;
              columnName = !(str17 == "DtlDescription") ? (!(str17 == "ProjDesc") ? (!(str17 == "DeptDesc") ? columnName + oldValue.Replace(oldValue, "ASMORDERDTL." + oldValue + ",") : columnName + oldValue.Replace(oldValue, "Dept.Description as " + oldValue + ",")) : columnName + oldValue.Replace(oldValue, "Project.Description as " + oldValue + ",")) : columnName + oldValue.Replace(oldValue, "ASMORDERDTL.Description as " + oldValue + ",");
              break;
            }
          }
          UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
          foreach (UDFColumn udfColumn in udfUtil.GetUDF("ASMORDER"))
          {
            if ("ASMORDER" + udfColumn.FieldName == str16)
            {
              string[] strArray4 = new string[6];
              int index16 = 0;
              string str17 = columnName;
              strArray4[index16] = str17;
              int index17 = 1;
              string str18 = "ASMORDER.";
              strArray4[index17] = str18;
              int index18 = 2;
              string fieldName1 = udfColumn.FieldName;
              strArray4[index18] = fieldName1;
              int index19 = 3;
              string str19 = " AS ASMORDER";
              strArray4[index19] = str19;
              int index20 = 4;
              string fieldName2 = udfColumn.FieldName;
              strArray4[index20] = fieldName2;
              int index21 = 5;
              string str20 = ",";
              strArray4[index21] = str20;
              columnName = string.Concat(strArray4);
              flag = true;
              break;
            }
          }
          foreach (UDFColumn udfColumn in udfUtil.GetUDF("ASMORDERDTL"))
          {
            if ("ASMORDERDTL" + udfColumn.FieldName == str16)
            {
              string[] strArray4 = new string[6];
              int index16 = 0;
              string str17 = columnName;
              strArray4[index16] = str17;
              int index17 = 1;
              string str18 = "ASMORDERDTL.";
              strArray4[index17] = str18;
              int index18 = 2;
              string fieldName1 = udfColumn.FieldName;
              strArray4[index18] = fieldName1;
              int index19 = 3;
              string str19 = " AS ASMORDERDTL";
              strArray4[index19] = str19;
              int index20 = 4;
              string fieldName2 = udfColumn.FieldName;
              strArray4[index20] = fieldName2;
              int index21 = 5;
              string str20 = ",";
              strArray4[index21] = str20;
              columnName = string.Concat(strArray4);
              flag = true;
              break;
            }
          }
          if (!flag)
            columnName = columnName + oldValue.Replace(oldValue, "ASMORDER." + oldValue + ",");
        }
      }
      if (columnName.EndsWith(","))
        columnName = columnName.Remove(columnName.Length - 1, 1);
      columnName = columnName.Trim();
      string str21 = string.Empty;
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = sqlConnection;
        string whereSql = this.GetWhereSQL(criteria, sqlCommand);
        string str16 = !(whereSql == string.Empty) ? string.Format(format, (object) columnName, (object) whereSql) : string.Format(format, (object) columnName, (object) "(1=1)");
        sqlCommand.CommandText = str16;
        sqlCommand.CommandTimeout = GeneralSetting.GetOrCreate(this.myDBSetting).SearchCommandTimeout;
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
        long[] numArray = (long[]) null;
        if (!criteria.KeepSearchResult)
        {
          dataTable2.Clear();
          dataTable1.Clear();
        }
        else if (chkEditColumnName.Length > 0 && dataTable1.Rows.Count > 0)
        {
          DataRow[] dataRowArray = dataTable1.Select(string.Format("{0} = true", (object) chkEditColumnName));
          numArray = new long[dataRowArray.Length];
          for (int index16 = 0; index16 < dataRowArray.Length; ++index16)
            numArray[index16] = BCE.Data.Convert.ToInt64(dataRowArray[index16]["DtlKey"]);
        }
        sqlConnection.Open();
        sqlDataAdapter.Fill(dataTable1);
        if (dataTable1.PrimaryKey.Length == 0)
        {
          DataTable dataTable3 = dataTable1;
          DataColumn[] dataColumnArray = new DataColumn[1];
          int index16 = 0;
          DataColumn dataColumn = dataTable1.Columns["DtlKey"];
          dataColumnArray[index16] = dataColumn;
          dataTable3.PrimaryKey = dataColumnArray;
        }
        if (criteria.SortBy != "")
        {
          if (criteria.SortBy == "Document No")
            dataTable1.DefaultView.Sort = "DocNo";
          else if (criteria.SortBy == "Date")
            dataTable1.DefaultView.Sort = "DocDate";
          else if (criteria.SortBy == "Item Code")
            dataTable1.DefaultView.Sort = "ItemCode";
          else if (criteria.SortBy == "Item Group")
            dataTable1.DefaultView.Sort = "ItemGroup";
          else if (criteria.SortBy == "Item Type")
            dataTable1.DefaultView.Sort = "ItemType";
          else if (criteria.SortBy == "Location")
            dataTable1.DefaultView.Sort = "Location";
          else if (criteria.SortBy == "Project No")
            dataTable1.DefaultView.Sort = "ProjNo";
          else if (criteria.SortBy == "Department No")
            dataTable1.DefaultView.Sort = "DeptNo";
        }
        if (numArray != null)
        {
          foreach (long num2 in numArray)
          {
            DataRow dataRow = dataTable1.Rows.Find((object) num2);
            if (dataRow != null)
              dataRow[chkEditColumnName] = (object) true;
          }
        }
      }
      catch (Exception ex)
      {
        if (ex is SqlException)
          BCE.Data.DataError.HandleSqlException((SqlException) ex);
        if (!(ex is ConstraintException))
          throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }
  }
}
