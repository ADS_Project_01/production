﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormStockAssemblyOrderOustandingReportMoreOptions
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.FilterUI;
using BCE.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTab;
using System.ComponentModel;
using System.Windows.Forms;

namespace RPASystem.WorkOrder
{
  public class FormStockWorkOrderOustandingReportMoreOptions : XtraForm
  {
    private StockWorkOrderOutstandingFormCriteria myCriteria;
    private DBSetting myDBSetting;
    private IContainer components;
    private UCAllDepartmentsSelector ucDepartmentSelector1;
    private UCAllDepartmentsSelector ucDepartmentSelector2;
    private UCItemGroupSelector ucItemGroupSelector1;
    private UCItemGroupSelector ucItemGroupSelector2;
    private UCItemSelector ucItemSelector1;
    private UCItemTypeSelector ucItemTypeSelector1;
    private UCItemTypeSelector ucItemTypeSelector2;
    private UCLocationSelector ucLocationSelector1;
    private UCLocationSelector ucLocationSelector2;
    private UCAllProjectsSelector ucProjectSelector1;
    private UCAllProjectsSelector ucProjectSelector2;
    private GroupControl gbFilter;
    private PanelControl panelControl1;
    private SimpleButton sbtnClose;
    private SimpleButton sbtnOK;
    private XtraTabControl xtraTabControl1;
    private XtraTabPage xtraTabPage1;
    private XtraTabPage xtraTabPage2;
    private Label label10;
    private Label label11;
    private Label label12;
    private Label label2;
    private Label label3;
    private Label label4;
    private Label label5;
    private Label label6;
    private Label label7;
    private Label label8;
    private Label label9;

    public FormStockWorkOrderOustandingReportMoreOptions(StockWorkOrderOutstandingFormCriteria criteria, DBSetting dbSetting)
    {
      this.InitializeComponent();
      this.myCriteria = criteria;
      this.myDBSetting = dbSetting;
      this.Init();
    }

    private void Init()
    {
      this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myCriteria.ItemGroupFilter);
      this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myCriteria.ItemTypeFilter);
      this.ucLocationSelector1.Initialize(this.myDBSetting, this.myCriteria.LocationFilter);
      this.ucProjectSelector1.Initialize(this.myDBSetting, this.myCriteria.ProjectNoFilter);
      this.ucDepartmentSelector1.Initialize(this.myDBSetting, this.myCriteria.DepartmentNoFilter);
      this.ucItemSelector1.Initialize(this.myDBSetting, this.myCriteria.ItemCodeDetailFilter);
      this.ucItemGroupSelector2.Initialize(this.myDBSetting, this.myCriteria.ItemGroupDetailFilter);
      this.ucItemTypeSelector2.Initialize(this.myDBSetting, this.myCriteria.ItemTypeDetailFilter);
      this.ucLocationSelector2.Initialize(this.myDBSetting, this.myCriteria.LocationDetailFilter);
      this.ucProjectSelector2.Initialize(this.myDBSetting, this.myCriteria.ProjectNoDetailFilter);
      this.ucDepartmentSelector2.Initialize(this.myDBSetting, this.myCriteria.DepartmentNoDetailFilter);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormStockWorkOrderOustandingReportMoreOptions));
      this.panelControl1 = new PanelControl();
      this.sbtnClose = new SimpleButton();
      this.sbtnOK = new SimpleButton();
      this.gbFilter = new GroupControl();
      this.xtraTabControl1 = new XtraTabControl();
      this.xtraTabPage1 = new XtraTabPage();
      this.ucProjectSelector1 = new UCAllProjectsSelector();
      this.ucLocationSelector1 = new UCLocationSelector();
      this.ucItemTypeSelector1 = new UCItemTypeSelector();
      this.ucItemGroupSelector1 = new UCItemGroupSelector();
      this.label6 = new Label();
      this.label5 = new Label();
      this.label4 = new Label();
      this.label3 = new Label();
      this.label2 = new Label();
      this.ucDepartmentSelector1 = new UCAllDepartmentsSelector();
      this.xtraTabPage2 = new XtraTabPage();
      this.ucItemSelector1 = new UCItemSelector();
      this.ucProjectSelector2 = new UCAllProjectsSelector();
      this.ucLocationSelector2 = new UCLocationSelector();
      this.ucItemTypeSelector2 = new UCItemTypeSelector();
      this.ucItemGroupSelector2 = new UCItemGroupSelector();
      this.label7 = new Label();
      this.label8 = new Label();
      this.label9 = new Label();
      this.label10 = new Label();
      this.label11 = new Label();
      this.label12 = new Label();
      this.ucDepartmentSelector2 = new UCAllDepartmentsSelector();
      this.panelControl1.BeginInit();
      this.panelControl1.SuspendLayout();
      this.gbFilter.BeginInit();
      this.gbFilter.SuspendLayout();
      this.xtraTabControl1.BeginInit();
      this.xtraTabControl1.SuspendLayout();
      this.xtraTabPage1.SuspendLayout();
      this.xtraTabPage2.SuspendLayout();
      this.SuspendLayout();
      componentResourceManager.ApplyResources((object) this.panelControl1, "panelControl1");
      this.panelControl1.BorderStyle = BorderStyles.NoBorder;
      this.panelControl1.Controls.Add((Control) this.sbtnClose);
      this.panelControl1.Controls.Add((Control) this.sbtnOK);
      this.panelControl1.Controls.Add((Control) this.gbFilter);
      this.panelControl1.Name = "panelControl1";
      componentResourceManager.ApplyResources((object) this.sbtnClose, "sbtnClose");
      this.sbtnClose.DialogResult = DialogResult.Cancel;
      this.sbtnClose.Name = "sbtnClose";
      componentResourceManager.ApplyResources((object) this.sbtnOK, "sbtnOK");
      this.sbtnOK.DialogResult = DialogResult.OK;
      this.sbtnOK.Name = "sbtnOK";
      componentResourceManager.ApplyResources((object) this.gbFilter, "gbFilter");
      this.gbFilter.Controls.Add((Control) this.xtraTabControl1);
      this.gbFilter.Name = "gbFilter";
      componentResourceManager.ApplyResources((object) this.xtraTabControl1, "xtraTabControl1");
      this.xtraTabControl1.Name = "xtraTabControl1";
      this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
      XtraTabPageCollection tabPages = this.xtraTabControl1.TabPages;
      XtraTabPage[] pages = new XtraTabPage[2];
      int index1 = 0;
      XtraTabPage xtraTabPage1 = this.xtraTabPage1;
      pages[index1] = xtraTabPage1;
      int index2 = 1;
      XtraTabPage xtraTabPage2 = this.xtraTabPage2;
      pages[index2] = xtraTabPage2;
      tabPages.AddRange(pages);
      componentResourceManager.ApplyResources((object) this.xtraTabPage1, "xtraTabPage1");
      this.xtraTabPage1.Controls.Add((Control) this.ucProjectSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.ucLocationSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.ucItemTypeSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.ucItemGroupSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.label6);
      this.xtraTabPage1.Controls.Add((Control) this.label5);
      this.xtraTabPage1.Controls.Add((Control) this.label4);
      this.xtraTabPage1.Controls.Add((Control) this.label3);
      this.xtraTabPage1.Controls.Add((Control) this.label2);
      this.xtraTabPage1.Controls.Add((Control) this.ucDepartmentSelector1);
      this.xtraTabPage1.Name = "xtraTabPage1";
      componentResourceManager.ApplyResources((object) this.ucProjectSelector1, "ucProjectSelector1");
      this.ucProjectSelector1.Name = "ucProjectSelector1";
      componentResourceManager.ApplyResources((object) this.ucLocationSelector1, "ucLocationSelector1");
      this.ucLocationSelector1.LocationType = UCLocationType.ItemLocation;
      this.ucLocationSelector1.Name = "ucLocationSelector1";
      componentResourceManager.ApplyResources((object) this.ucItemTypeSelector1, "ucItemTypeSelector1");
      this.ucItemTypeSelector1.Name = "ucItemTypeSelector1";
      componentResourceManager.ApplyResources((object) this.ucItemGroupSelector1, "ucItemGroupSelector1");
      this.ucItemGroupSelector1.Name = "ucItemGroupSelector1";
      componentResourceManager.ApplyResources((object) this.label6, "label6");
      this.label6.Name = "label6";
      componentResourceManager.ApplyResources((object) this.label5, "label5");
      this.label5.Name = "label5";
      componentResourceManager.ApplyResources((object) this.label4, "label4");
      this.label4.Name = "label4";
      componentResourceManager.ApplyResources((object) this.label3, "label3");
      this.label3.Name = "label3";
      componentResourceManager.ApplyResources((object) this.label2, "label2");
      this.label2.Name = "label2";
      componentResourceManager.ApplyResources((object) this.ucDepartmentSelector1, "ucDepartmentSelector1");
      this.ucDepartmentSelector1.Name = "ucDepartmentSelector1";
      componentResourceManager.ApplyResources((object) this.xtraTabPage2, "xtraTabPage2");
      this.xtraTabPage2.Controls.Add((Control) this.ucItemSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucProjectSelector2);
      this.xtraTabPage2.Controls.Add((Control) this.ucLocationSelector2);
      this.xtraTabPage2.Controls.Add((Control) this.ucItemTypeSelector2);
      this.xtraTabPage2.Controls.Add((Control) this.ucItemGroupSelector2);
      this.xtraTabPage2.Controls.Add((Control) this.label7);
      this.xtraTabPage2.Controls.Add((Control) this.label8);
      this.xtraTabPage2.Controls.Add((Control) this.label9);
      this.xtraTabPage2.Controls.Add((Control) this.label10);
      this.xtraTabPage2.Controls.Add((Control) this.label11);
      this.xtraTabPage2.Controls.Add((Control) this.label12);
      this.xtraTabPage2.Controls.Add((Control) this.ucDepartmentSelector2);
      this.xtraTabPage2.Name = "xtraTabPage2";
      componentResourceManager.ApplyResources((object) this.ucItemSelector1, "ucItemSelector1");
      this.ucItemSelector1.Name = "ucItemSelector1";
      componentResourceManager.ApplyResources((object) this.ucProjectSelector2, "ucProjectSelector2");
      this.ucProjectSelector2.Name = "ucProjectSelector2";
      componentResourceManager.ApplyResources((object) this.ucLocationSelector2, "ucLocationSelector2");
      this.ucLocationSelector2.LocationType = UCLocationType.ItemLocation;
      this.ucLocationSelector2.Name = "ucLocationSelector2";
      componentResourceManager.ApplyResources((object) this.ucItemTypeSelector2, "ucItemTypeSelector2");
      this.ucItemTypeSelector2.Name = "ucItemTypeSelector2";
      componentResourceManager.ApplyResources((object) this.ucItemGroupSelector2, "ucItemGroupSelector2");
      this.ucItemGroupSelector2.Name = "ucItemGroupSelector2";
      componentResourceManager.ApplyResources((object) this.label7, "label7");
      this.label7.Name = "label7";
      componentResourceManager.ApplyResources((object) this.label8, "label8");
      this.label8.Name = "label8";
      componentResourceManager.ApplyResources((object) this.label9, "label9");
      this.label9.Name = "label9";
      componentResourceManager.ApplyResources((object) this.label10, "label10");
      this.label10.Name = "label10";
      componentResourceManager.ApplyResources((object) this.label11, "label11");
      this.label11.Name = "label11";
      componentResourceManager.ApplyResources((object) this.label12, "label12");
      this.label12.Name = "label12";
      componentResourceManager.ApplyResources((object) this.ucDepartmentSelector2, "ucDepartmentSelector2");
      this.ucDepartmentSelector2.Name = "ucDepartmentSelector2";
      this.AcceptButton = (IButtonControl) this.sbtnOK;
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.CancelButton = (IButtonControl) this.sbtnClose;
      this.Controls.Add((Control) this.panelControl1);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FormStockAssemblyOrderOustandingReportMoreOptions";
      this.ShowInTaskbar = false;
      this.panelControl1.EndInit();
      this.panelControl1.ResumeLayout(false);
      this.gbFilter.EndInit();
      this.gbFilter.ResumeLayout(false);
      this.xtraTabControl1.EndInit();
      this.xtraTabControl1.ResumeLayout(false);
      this.xtraTabPage1.ResumeLayout(false);
      this.xtraTabPage1.PerformLayout();
      this.xtraTabPage2.ResumeLayout(false);
      this.xtraTabPage2.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
