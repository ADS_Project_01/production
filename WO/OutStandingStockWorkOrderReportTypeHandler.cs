﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.OutStandingStockAssemblyOrderReportTypeHandler
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Data;

namespace RPASystem.WorkOrder
{
  public class OutStandingStockWorkOrderReportTypeHandler : StockWorkOrderReportTypeHandler
  {
    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      return StockWorkOrderOutstandingReport.Create(dbSetting).GetOutstandingListingReportDesignerDataSource();
    }
  }
}
