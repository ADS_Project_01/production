﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormStockAssemblyOrderPrintDetailListing
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Settings;
using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace RPASystem.WorkOrder
{
  [SingleInstanceThreadForm]
  public class FormStockWorkOrderPrintDetailListing : XtraForm
  {
    public static string myColumnName;
    private DBSetting myDBSetting;
    private DataTable myDataTable;
    private DataTable myDetailDataTable;
    private DataSet myDataSet;
    private StockWorkOrderReportCommand myCommand;
    private AdvancedStockWorkOrderCriteria myCriteria;
    private bool myInSearch;
    private StockWorkOrderDetailReportingCriteria myReportingCriteria;
    private const string LISTING = "Print Stock Assembly Order Detail Listing";
    private bool myLoadAllColumns;
    private bool myFilterByLocation;
    private ScriptObject myScriptObject;
    private UserAuthentication myUserAuthentication;
    private MouseDownHelper myMouseDownHelper;
    private IContainer components;
    private PanelHeader panelHeader1;
    private PreviewButton previewButton1;
    private PrintButton printButton1;
    private UCDateSelector ucDateSelector1;
    private UCDateSelector ucDateSelector2;
    private UCAllDepartmentsSelector ucDepartmentSelector1;
    private UCItemGroupSelector ucItemGroupSelector1;
    private UCItemSelector ucItemSelector1;
    private UCItemSelector ucItemSelector2;
    private UCItemTypeSelector ucItemTypeSelector1;
    private UCLocationSelector ucLocationSelector1;
    private UCAllProjectsSelector ucProjectSelector1;
    private UCSearchResult ucSearchResult1;
    private UCStockAssemblyOrderSelector ucStockAssemblyOrderSelector1;
    private UCStockAssemblyOrderSelector ucStockAssemblyOrderSelector2;
    private Bar bar1;
    private BarButtonItem barbtnAdvancedFilter;
    private BarButtonItem barBtnDesignDetailListingReport;
    private BarDockControl barDockControlBottom;
    private BarDockControl barDockControlLeft;
    private BarDockControl barDockControlRight;
    private BarDockControl barDockControlTop;
    private BarManager barManager1;
    private BarSubItem barSubItem1;
    private CheckEdit chkEditShowCriteria;
    private CheckEdit chkEditShowCriteria2;
    private ComboBoxEdit cbCancelledOption;
    private ComboBoxEdit cbEditGroupBy;
    private ComboBoxEdit cbEditGroupBy2;
    private ComboBoxEdit cbEditSortBy;
    private ComboBoxEdit cbEditSortBy2;
    private GroupControl gbBasic;
    private GroupControl gbFilter;
    private GroupControl gbReport;
    private GroupControl groupControl1;
    private MemoEdit memoEdit_Criteria;
    private PanelControl panelCenter;
    private PanelControl panelControl2;
    private PanelControl panelCriteria;
    private PanelControl pnCriteriaBasic;
    private RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private RepositoryItemTextEdit repositoryItemTextEdit_Cancelled;
    private SimpleButton sbtnAdvanceSearch;
    private SimpleButton sbtnAdvOptions;
    private SimpleButton sbtnClose;
    private SimpleButton sbtnInquiry;
    private SimpleButton sbtnToggleOptions;
    private SimpleButton simpleButton1;
    private GridColumn colAssemblyCost;
    private GridColumn colBatchNo;
    private GridColumn colCancelled;
    private GridColumn colCheck;
    private GridColumn colCreatedTimeStamp;
    private GridColumn colCreatedUserID;
    private GridColumn colDeptNo;
    private GridColumn colDescription;
    private GridColumn colDocDate;
    private GridColumn colDocNo;
    private GridColumn colDtlDescription;
    private GridColumn colFurtherDescription;
    private GridColumn colItemCode;
    private GridColumn colItemCost;
    private GridColumn colItemGroup;
    private GridColumn colItemType;
    private GridColumn colLastModified;
    private GridColumn colLastModifiedUserID;
    private GridColumn colLocation;
    private GridColumn colNetTotal;
    private GridColumn colOverHeadCost;
    private GridColumn colPrintCount;
    private GridColumn colProjNo;
    private GridColumn colQty;
    private GridColumn colRate;
    private GridColumn colRefDocNo;
    private GridColumn colRemark1;
    private GridColumn colRemark2;
    private GridColumn colRemark3;
    private GridColumn colRemark4;
    private GridColumn colSubTotalCost;
    private GridColumn colTotal;
    private GridControl gridControl1;
    private GridView gvMaster;
    private XtraTabControl xtraTabControl1;
    private XtraTabControl xtraTabControl2;
    private XtraTabPage xtraTabPage1;
    private XtraTabPage xtraTabPage2;
    private XtraTabPage xtraTabPage3;
    private XtraTabPage xtraTabPage4;
    private Label label1;
    private Label label10;
    private Label label11;
    private Label label12;
    private Label label13;
    private Label label14;
    private Label label15;
    private Label label16;
    private Label label2;
    private Label label3;
    private Label label4;
    private Label label5;
    private Label label6;
    private Label label7;
    private Label label8;
    private Label label9;
    private GridColumn colExpCompletedDate;
    private GridColumn colProjDesc;
    private GridColumn colDeptDesc;
    private BarButtonItem barBtnConvertToPlainText;

    public DataTable MasterDataTable
    {
      get
      {
        return this.myDataTable;
      }
    }

    public DataTable DetailDataTable
    {
      get
      {
        return this.myDetailDataTable;
      }
    }

    public string SelectedDocNosInString
    {
      get
      {
        return this.GenerateDocNosToString();
      }
    }

    public string SelectedDocKeysInString
    {
      get
      {
        return StringHelper.ArrayListToCommaString(new ArrayList((ICollection) CommonFunction.GetDocKeyList("ToBeUpdate", this.myDataTable)));
      }
    }

    public string SelectedDtlKeysInString
    {
      get
      {
        return StringHelper.ArrayListToCommaString(new ArrayList((ICollection) CommonFunction.GetDtlKeyList("ToBeUpdate", this.myDataTable)));
      }
    }

    public string ColumnName
    {
      get
      {
        return FormStockWorkOrderPrintDetailListing.myColumnName;
      }
    }

    public StockWorkOrderDetailReportingCriteria StockAssemblyReportingCriteria
    {
      get
      {
        return this.myReportingCriteria;
      }
    }

    public FormStockWorkOrderPrintDetailListing(DBSetting dbSetting)
    {
      this.InitializeComponent();
      this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockAssemblyOrderDetailListing");
      this.myCommand = StockWorkOrderReportCommand.Create(dbSetting, new BasicReportOption());
      this.myDBSetting = this.myCommand.DBSetting;
      this.myUserAuthentication = UserAuthentication.GetOrCreate(this.myDBSetting);
      this.previewButton1.ReportType = "Stock Assembly Order Detail Listing";
      this.previewButton1.SetDBSetting(this.myDBSetting);
      this.printButton1.ReportType = "Stock Assembly Order Detail Listing";
      this.printButton1.SetDBSetting(this.myDBSetting);
      this.myDataTable = new DataTable("Master");
      this.myDetailDataTable = new DataTable("Detail");
      this.myDataSet = new DataSet();
      this.myDataSet.Tables.Add(this.myDataTable);
      this.myDataSet.Tables.Add(this.myDetailDataTable);
      this.gridControl1.DataSource = (object) this.myDataTable;
      this.LoadCriteria();
      this.ucSearchResult1.Initialize(this.gvMaster, "ToBeUpdate");
      CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gvMaster, new EventHandler(this.ReloadAllColumns));
      this.Tag = (object) EnterKeyMessageFilter.NoFilter;
      this.InitUserControls();
      this.InitFormControls();
      this.InitializeSettings();
      new UDFUtil(this.myDBSetting).SetupDetailListingReportGrid(this.gvMaster, "ASMORDER", "ASMORDERDTL");
      this.RefreshDesignReport();
      this.myMouseDownHelper = new MouseDownHelper();
      this.myMouseDownHelper.Init(this.gvMaster);
      this.myUserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.RefreshDesignReport), (Component) this);
      BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Assembly_Order.htm");
      DBSetting dbSetting1 = dbSetting;
      string docType = "";
      long docKey = 0L;
      long eventKey = 0L;
      // ISSUE: variable of a boxed type
      StockWorkOrderString local =  StockWorkOrderString.OpenedPrintStockWorkOrderDetailListing;
      object[] objArray = new object[1];
      int index = 0;
      string loginUserId = this.myUserAuthentication.LoginUserID;
      objArray[index] = (object) loginUserId;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      string detail = "";
      Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
      string fieldname1 = "Total";
      string fieldtype1 = "Currency";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "AssemblyCost";
      string fieldtype2 = "Currency";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "NetTotal";
      string fieldtype3 = "Currency";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "Qty";
      string fieldtype4 = "Quantity";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "Rate";
      string fieldtype5 = "Quantity";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "ItemCost";
      string fieldtype6 = "Currency";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "OverheadCost";
      string fieldtype7 = "Currency";
      formControlUtil.AddField(fieldname7, fieldtype7);
      string fieldname8 = "SubTotalCost";
      string fieldtype8 = "Currency";
      formControlUtil.AddField(fieldname8, fieldtype8);
      string fieldname9 = "DocDate";
      string fieldtype9 = "Date";
      formControlUtil.AddField(fieldname9, fieldtype9);
      string fieldname10 = "LastModified";
      string fieldtype10 = "DateTime";
      formControlUtil.AddField(fieldname10, fieldtype10);
      string fieldname11 = "CreatedTimeStamp";
      string fieldtype11 = "DateTime";
      formControlUtil.AddField(fieldname11, fieldtype11);
      FormStockWorkOrderPrintDetailListing printDetailListing = this;
      formControlUtil.InitControls((Control) printDetailListing);
      DecimalSetting orCreate = DecimalSetting.GetOrCreate(this.myDBSetting);
      GridGroupSummaryItemCollection groupSummary1 = this.gvMaster.GroupSummary;
      int num1 = 3;
      string fieldName1 = "ItemCode";
      // ISSUE: variable of the null type
      GridColumn local1 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local2 = GridGroupSummaryItemStringId.ItemCodeCount;
      object[] objArray1 = new object[1];
      int index1 = 0;
      string str = "{0}";
      objArray1[index1] = (object) str;
      string string1 = BCE.Localization.Localizer.GetString((Enum) local2, objArray1);
      groupSummary1.Add((SummaryItemType) num1, fieldName1, (GridColumn) local1, string1);
      GridGroupSummaryItemCollection groupSummary2 = this.gvMaster.GroupSummary;
      int num2 = 0;
      string fieldName2 = "Total";
      // ISSUE: variable of the null type
      GridColumn local3 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.Total;
      object[] objArray2 = new object[1];
      int index2 = 0;
      string currencyFormatString1 = orCreate.GetCurrencyFormatString(0);
      objArray2[index2] = (object) currencyFormatString1;
      string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
      groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      GridGroupSummaryItemCollection groupSummary3 = this.gvMaster.GroupSummary;
      int num3 = 0;
      string fieldName3 = "NetTotal";
      // ISSUE: variable of the null type
      GridColumn local5 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local6 =  GridGroupSummaryItemStringId.NetTotal;
      object[] objArray3 = new object[1];
      int index3 = 0;
      string currencyFormatString2 = orCreate.GetCurrencyFormatString(0);
      objArray3[index3] = (object) currencyFormatString2;
      string string3 = BCE.Localization.Localizer.GetString((Enum) local6, objArray3);
      groupSummary3.Add((SummaryItemType) num3, fieldName3, (GridColumn) local5, string3);
      GridGroupSummaryItemCollection groupSummary4 = this.gvMaster.GroupSummary;
      int num4 = 0;
      string fieldName4 = "AssemblyCost";
      // ISSUE: variable of the null type
      GridColumn local7 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local8 =  GridGroupSummaryItemStringId.AssemblyCost;
      object[] objArray4 = new object[1];
      int index4 = 0;
      string costFormatString1 = orCreate.GetCostFormatString(0);
      objArray4[index4] = (object) costFormatString1;
      string string4 = BCE.Localization.Localizer.GetString((Enum) local8, objArray4);
      groupSummary4.Add((SummaryItemType) num4, fieldName4, (GridColumn) local7, string4);
      GridGroupSummaryItemCollection groupSummary5 = this.gvMaster.GroupSummary;
      int num5 = 0;
      string fieldName5 = "Qty";
      // ISSUE: variable of the null type
      GridColumn local9 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local10 =GridGroupSummaryItemStringId.Quantity;
      object[] objArray5 = new object[1];
      int index5 = 0;
      string quantityFormatString = orCreate.GetQuantityFormatString(0);
      objArray5[index5] = (object) quantityFormatString;
      string string5 = BCE.Localization.Localizer.GetString((Enum) local10, objArray5);
      groupSummary5.Add((SummaryItemType) num5, fieldName5, (GridColumn) local9, string5);
      GridGroupSummaryItemCollection groupSummary6 = this.gvMaster.GroupSummary;
      int num6 = 0;
      string fieldName6 = "ItemCost";
      // ISSUE: variable of the null type
      GridColumn local11 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local12 =  GridGroupSummaryItemStringId.ItemCost;
      object[] objArray6 = new object[1];
      int index6 = 0;
      string costFormatString2 = orCreate.GetCostFormatString(0);
      objArray6[index6] = (object) costFormatString2;
      string string6 = BCE.Localization.Localizer.GetString((Enum) local12, objArray6);
      groupSummary6.Add((SummaryItemType) num6, fieldName6, (GridColumn) local11, string6);
      GridGroupSummaryItemCollection groupSummary7 = this.gvMaster.GroupSummary;
      int num7 = 0;
      string fieldName7 = "OverHeadCost";
      // ISSUE: variable of the null type
      GridColumn local13 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local14 = GridGroupSummaryItemStringId.OverHeadCost;
      object[] objArray7 = new object[1];
      int index7 = 0;
      string costFormatString3 = orCreate.GetCostFormatString(0);
      objArray7[index7] = (object) costFormatString3;
      string string7 = BCE.Localization.Localizer.GetString((Enum) local14, objArray7);
      groupSummary7.Add((SummaryItemType) num7, fieldName7, (GridColumn) local13, string7);
      GridGroupSummaryItemCollection groupSummary8 = this.gvMaster.GroupSummary;
      int num8 = 0;
      string fieldName8 = "SubTotalCost";
      // ISSUE: variable of the null type
      GridColumn local15 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local16 =  GridGroupSummaryItemStringId.SubTotalCost;
      object[] objArray8 = new object[1];
      int index8 = 0;
      string costFormatString4 = orCreate.GetCostFormatString(0);
      objArray8[index8] = (object) costFormatString4;
      string string8 = BCE.Localization.Localizer.GetString((Enum) local16, objArray8);
      groupSummary8.Add((SummaryItemType) num8, fieldName8, (GridColumn) local15, string8);
    }

    private void SaveCriteria()
    {
      PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting) this.myReportingCriteria, "StockAssemblyOrderDocumentDetailListingReport.setting");
    }

    private void LoadCriteria()
    {
      try
      {
        this.myReportingCriteria = (StockWorkOrderDetailReportingCriteria) PersistenceUtil.LoadCriteriaData(this.myDBSetting, "StockAssemblyOrderDocumentDetailListingReport.setting");
      }
      catch
      {
      }
      if (this.myReportingCriteria == null)
        this.myReportingCriteria = new StockWorkOrderDetailReportingCriteria();
      this.cbEditGroupBy.EditValue = (object) this.myReportingCriteria.GroupBy;
      this.cbEditSortBy.EditValue = (object) this.myReportingCriteria.SortBy;
      this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
      this.chkEditShowCriteria.Checked = this.myReportingCriteria.IsShowCriteria;
      if (this.myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.All)
        this.cbCancelledOption.SelectedIndex = 0;
      else if (this.myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
        this.cbCancelledOption.SelectedIndex = 1;
      else
        this.cbCancelledOption.SelectedIndex = 2;
    }

    private void InitUserControls()
    {
      if (this.myReportingCriteria.AdvancedOptions)
      {
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToBasicOptions, new object[0]);
        this.panelCriteria.Visible = true;
        this.pnCriteriaBasic.Visible = false;
        this.cbEditGroupBy.Text = this.myReportingCriteria.GroupBy;
        this.cbEditSortBy.Text = this.myReportingCriteria.SortBy;
      }
      else
      {
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToAdvancedOptions, new object[0]);
        this.panelCriteria.Visible = false;
        this.pnCriteriaBasic.Visible = true;
        this.cbEditGroupBy2.Text = this.myReportingCriteria.GroupBy;
        this.cbEditSortBy2.Text = this.myReportingCriteria.SortBy;
      }
      this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
      this.ucStockAssemblyOrderSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
      this.ucItemSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
      this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupFilter);
      this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeFilter);
      this.ucLocationSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.LocationFilter);
      this.ucProjectSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ProjecNoFilter);
      this.ucDepartmentSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DeptNoFilter);
      this.ucDateSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
      this.ucStockAssemblyOrderSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
      this.ucItemSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
    }

    private void InitializeSettings()
    {
      this.GetGroupSortCriteria();
    }

    private void GetGroupSortCriteria()
    {
      if (this.myReportingCriteria.AdvancedOptions)
      {
        this.myReportingCriteria.GroupBy = this.cbEditGroupBy.Text;
        this.myReportingCriteria.SortBy = this.cbEditSortBy.Text;
        this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
      }
      else
      {
        this.myReportingCriteria.GroupBy = this.cbEditGroupBy2.Text;
        this.myReportingCriteria.SortBy = this.cbEditSortBy2.Text;
        this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria2.Checked;
      }
    }

    private void AddNewColumn()
    {
      this.myDataTable.Columns.Add(new DataColumn()
      {
        DataType = typeof (bool),
        AllowDBNull = true,
        Caption = "Check",
        ColumnName = "ToBeUpdate",
        DefaultValue = (object) false
      });
    }

    private void BasicSearch(bool isSearchAll)
    {
      if (!this.myInSearch)
      {
        this.myInSearch = true;
        BCE.XtraUtils.GridViewUtils.UpdateData(this.gvMaster);
        this.gridControl1.DataSource = (object) null;
        try
        {
          this.gridControl1.MainView.UpdateCurrentRow();
          this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
          string columnName = CommonFunction.BuildSQLColumns(isSearchAll, this.gvMaster.Columns.View);
          if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
            this.AddNewColumn();
          this.myCommand.DetailListingBasicSearch(this.myReportingCriteria, columnName, this.myDataSet, "ToBeUpdate");
          this.gridControl1.DataSource = (object) this.myDataTable;
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage(ex.Message);
        }
        finally
        {
          this.myInSearch = false;
        }
        FormStockWorkOrderPrintDetailListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockWorkOrderPrintDetailListing.FormInquiryEventArgs(this, this.myDataTable);
        ScriptObject scriptObject = this.myScriptObject;
        string name = "OnFormInquiry";
        System.Type[] types = new System.Type[1];
        int index1 = 0;
        System.Type type = inquiryEventArgs1.GetType();
        types[index1] = type;
        object[] objArray = new object[1];
        int index2 = 0;
        FormStockWorkOrderPrintDetailListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
        objArray[index2] = (object) inquiryEventArgs2;
        scriptObject.RunMethod(name, types, objArray);
      }
    }

    private void AdvancedSearch()
    {
      if (this.myCriteria == null)
        this.myCriteria = new AdvancedStockWorkOrderCriteria(this.myDBSetting);
      this.myCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
      using (FormAdvancedSearch formAdvancedSearch = new FormAdvancedSearch((SearchCriteria) this.myCriteria, this.myDBSetting))
      {
        if (formAdvancedSearch.ShowDialog((IWin32Window) this) == DialogResult.OK)
        {
          if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
            this.AddNewColumn();
          this.memoEdit_Criteria.Lines = this.myCriteria.BuildReadableTextArray();
          this.ucSearchResult1.KeepSearchResult = this.myCriteria.KeepSearchResult;
          Cursor current = Cursor.Current;
          Cursor.Current = Cursors.WaitCursor;
          this.myInSearch = true;
          try
          {
            this.myCommand.DetailListingAdvanceSearch(this.myCriteria, this.myDataSet, "ToBeUpdate");
          }
          catch (AppException ex)
          {
            AppMessage.ShowErrorMessage(ex.Message);
          }
          finally
          {
            this.ucSearchResult1.CheckAll();
            this.myInSearch = false;
            Cursor.Current = current;
          }
          FormStockWorkOrderPrintDetailListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockWorkOrderPrintDetailListing.FormInquiryEventArgs(this, this.myDataTable);
          ScriptObject scriptObject = this.myScriptObject;
          string name = "OnFormInquiry";
          System.Type[] types = new System.Type[1];
          int index1 = 0;
          System.Type type = inquiryEventArgs1.GetType();
          types[index1] = type;
          object[] objArray = new object[1];
          int index2 = 0;
          FormStockWorkOrderPrintDetailListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
          objArray[index2] = (object) inquiryEventArgs2;
          scriptObject.RunMethod(name, types, objArray);
        }
      }
    }

    private string GenerateDocNosToString()
    {
      BCE.XtraUtils.GridViewUtils.UpdateData(this.gvMaster);
      DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = True");
      if (dataRowArray.Length == 0)
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderDetailSelected, new object[0]));
        this.DialogResult = DialogResult.None;
        return (string) null;
      }
      else
      {
        string str = "";
        foreach (DataRow dataRow in dataRowArray)
        {
          if (str.Length != 0)
            str = str + ", ";
          str = str + "'" + dataRow["DocNo"].ToString() + "'";
        }
        return str;
      }
    }

    private void sbtnInquiry_Click(object sender, EventArgs e)
    {
      this.sbtnToggleOptions.Enabled = true;
      this.BasicSearch(this.myLoadAllColumns);
      if (this.myReportingCriteria.AdvancedOptions)
      {
        if (this.cbEditSortBy.SelectedIndex == 0)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 1)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 2)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 3)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 4)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 5)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 6)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 7)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.Ascending;
        }
      }
      else if (this.cbEditSortBy2.SelectedIndex == 0)
      {
        this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
        this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 1)
      {
        this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 2)
      {
        this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.Ascending;
        this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 3)
      {
        this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.Ascending;
        this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 4)
      {
        this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.Ascending;
        this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 5)
      {
        this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.Ascending;
        this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 6)
      {
        this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.Ascending;
        this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
      }
      else if (this.cbEditSortBy2.SelectedIndex == 7)
      {
        this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
        this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.Ascending;
      }
      this.ucSearchResult1.CheckAll();
      this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
      DBSetting dbSetting = this.myDBSetting;
      string docType = "";
      long docKey = 0L;
      long eventKey = 0L;
      // ISSUE: variable of a boxed type
      StockWorkOrderString local =  StockWorkOrderString.InquiredPrintStockWorkOrderDetailListing;
      object[] objArray = new object[1];
      int index = 0;
      string loginUserId = this.myUserAuthentication.LoginUserID;
      objArray[index] = (object) loginUserId;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      string text = this.memoEdit_Criteria.Text;
      Activity.Log(dbSetting, docType, docKey, eventKey, @string, text);
    }

    private void ReloadAllColumns(object sender, EventArgs e)
    {
      this.myLoadAllColumns = true;
      this.BasicSearch(true);
    }

    private void barbtnAdvancedFilter_ItemClick(object sender, ItemClickEventArgs e)
    {
      if (this.myReportingCriteria.AdvancedOptions)
      {
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToAdvancedOptions, new object[0]);
        this.pnCriteriaBasic.Visible = true;
        this.panelCriteria.Visible = false;
        this.myReportingCriteria.AdvancedOptions = false;
        this.ucDateSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
        this.ucStockAssemblyOrderSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
        this.ucItemSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
        this.cbEditGroupBy2.SelectedIndex = this.cbEditGroupBy.SelectedIndex;
        this.cbEditSortBy2.SelectedIndex = this.cbEditSortBy.SelectedIndex;
        this.chkEditShowCriteria2.Checked = this.chkEditShowCriteria.Checked;
      }
      else
      {
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToBasicOptions, new object[0]);
        this.pnCriteriaBasic.Visible = false;
        this.panelCriteria.Visible = true;
        this.myReportingCriteria.AdvancedOptions = true;
        this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
        this.ucStockAssemblyOrderSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
        this.ucItemSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeFilter);
        this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupFilter);
        this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeFilter);
        this.ucLocationSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.LocationFilter);
        this.ucProjectSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ProjecNoFilter);
        this.ucDepartmentSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DeptNoFilter);
        this.cbEditGroupBy.SelectedIndex = this.cbEditGroupBy2.SelectedIndex;
        this.cbEditSortBy.SelectedIndex = this.cbEditSortBy2.SelectedIndex;
        this.chkEditShowCriteria.Checked = this.chkEditShowCriteria2.Checked;
      }
    }

    private void sbtnToggleOptions_Click(object sender, EventArgs e)
    {
      if (this.myReportingCriteria.AdvancedOptions)
      {
        this.panelCriteria.Visible = !this.panelCriteria.Visible;
        if (this.panelCriteria.Visible)
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_HideOptions, new object[0]);
        else
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowOptions, new object[0]);
      }
      else
      {
        this.pnCriteriaBasic.Visible = !this.pnCriteriaBasic.Visible;
        if (this.pnCriteriaBasic.Visible)
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_HideOptions, new object[0]);
        else
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowOptions, new object[0]);
      }
    }

    private void sbtnClose_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void cbCancelledOption_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
      {
        if (this.cbCancelledOption.SelectedIndex == 0)
          this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.All;
        else if (this.cbCancelledOption.SelectedIndex == 1)
          this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.Cancelled;
        else
          this.myReportingCriteria.IsPrintCancelled = CancelledDocumentOption.UnCancelled;
      }
    }

    private void FormStockAssemblyPrintDetailListing_Closing(object sender, CancelEventArgs e)
    {
      this.SaveCriteria();
    }

    private void FormStockAssemblyPrintDetailListing_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == (Keys) 120)
      {
        if (this.sbtnInquiry.Enabled)
          this.sbtnInquiry.PerformClick();
      }
      else if (e.KeyCode == (Keys) 119)
      {
        if (this.previewButton1.Enabled)
          this.previewButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 118)
      {
        if (this.printButton1.Enabled)
          this.printButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 117 && this.sbtnToggleOptions.Enabled)
        this.sbtnToggleOptions.PerformClick();
    }

    private void cbEditSortBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
      {
        if (this.myReportingCriteria.SortBy != this.cbEditSortBy.Text)
          this.myReportingCriteria.SortBy = this.cbEditSortBy.Text;
        if (this.cbEditSortBy.SelectedIndex == 0)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 1)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 2)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 3)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 4)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 5)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 6)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy.SelectedIndex == 7)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.Ascending;
        }
      }
    }

    private void cbEditSortBy2_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
      {
        if (this.myReportingCriteria.SortBy != this.cbEditSortBy2.Text)
          this.myReportingCriteria.SortBy = this.cbEditSortBy2.Text;
        if (this.cbEditSortBy2.SelectedIndex == 0)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 1)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 2)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 3)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 4)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 5)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 6)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.Ascending;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.None;
        }
        else if (this.cbEditSortBy2.SelectedIndex == 7)
        {
          this.gvMaster.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemCode"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemGroup"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ItemType"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["Location"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["ProjNo"].SortOrder = ColumnSortOrder.None;
          this.gvMaster.Columns["DeptNo"].SortOrder = ColumnSortOrder.Ascending;
        }
      }
    }

    private void cbEditGroupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.GroupBy = this.cbEditGroupBy.Text;
    }

    private void cbEditGroupBy2_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.GroupBy = this.cbEditGroupBy2.Text;
    }

    private void sbtnAdvOptions_Click(object sender, EventArgs e)
    {
      using (FormStockWorkOrderDetailListingAdvOptions listingAdvOptions = new FormStockWorkOrderDetailListingAdvOptions(this.myDBSetting, this.myReportingCriteria))
      {
        listingAdvOptions.SetFilterByLocation(this.myFilterByLocation);
        if (listingAdvOptions.ShowDialog() == DialogResult.OK)
          this.cbCancelledOption.SelectedItem = listingAdvOptions.IsPrintCancelled;
      }
    }

    private void sbtnAdvanceSearch_Click(object sender, EventArgs e)
    {
      this.AdvancedSearch();
    }

    private void gvMaster_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      GridView gridView = (GridView) sender;
      int[] selectedRows = gridView.GetSelectedRows();
      if (selectedRows != null && selectedRows.Length != 1)
      {
        for (int index = 0; index < selectedRows.Length; ++index)
          gridView.GetDataRow(selectedRows[index])["ToBeUpdate"] = (object) true;
      }
    }

    private void chkEditShowCriteria_CheckedChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
    }

    private void repositoryItemTextEdit_Cancelled_FormatEditValue(object sender, ConvertEditValueEventArgs e)
    {
      if (e.Value != null)
      {
        if (e.Value.ToString() == "T")
          e.Value = (object) BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Cancelled, new object[0]);
        else
          e.Value = (object) "";
      }
    }

    private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      string selectedDtlKeysInString = this.SelectedDtlKeysInString;
      if (selectedDtlKeysInString == "")
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderDetailSelected, new object[0]));
        this.DialogResult = DialogResult.None;
      }
      else if (this.myUserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DTLLIST_REPORT_PREVIEW"))
      {
        Cursor current = Cursor.Current;
        Cursor.Current = Cursors.WaitCursor;
        this.GetGroupSortCriteria();
        this.myCommand.PreviewDetailListingReport(selectedDtlKeysInString, this.myReportingCriteria, e.DefaultReport);
        Cursor.Current = current;
      }
    }

    private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      string selectedDtlKeysInString = this.SelectedDtlKeysInString;
      if (selectedDtlKeysInString == "")
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderDetailSelected, new object[0]));
        this.DialogResult = DialogResult.None;
      }
      else if (this.myUserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DTLLIST_REPORT_PRINT"))
      {
        Cursor current = Cursor.Current;
        Cursor.Current = Cursors.WaitCursor;
        this.GetGroupSortCriteria();
        this.myCommand.PrintDetailListingReport(selectedDtlKeysInString, this.myReportingCriteria, e.DefaultReport);
        Cursor.Current = current;
      }
    }

    private void barBtnDesignDetailListingReport_ItemClick(object sender, ItemClickEventArgs e)
    {
      ReportTool.DesignReport("Stock Assembly Order Detail Listing", this.myDBSetting);
    }

    public virtual void RefreshDesignReport()
    {
      this.barBtnDesignDetailListingReport.Visibility = XtraBarsUtils.ToBarItemVisibility(this.myUserAuthentication.AccessRight.IsAccessible("TOOLS_RPT_SHOW"));
      if (this.ucLocationSelector1 != null)
      {
        this.myFilterByLocation = SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnableFilterByCurrentUserLocationAccessRight;
        if (this.myFilterByLocation)
        {
          this.ucLocationSelector1.Filter.Type = FilterType.ByRange;
          this.ucLocationSelector1.Filter.From = (object) this.myUserAuthentication.MainLocation;
          this.ucLocationSelector1.Filter.To = (object) this.myUserAuthentication.MainLocation;
          this.ucLocationSelector1.ApplyFilter();
          this.ucLocationSelector1.Enabled = false;
        }
        else
          this.ucLocationSelector1.Enabled = true;
      }
    }

    private void FormSAOPrintDetailListing_Load(object sender, EventArgs e)
    {
      this.FormInitialize();
    }

    private void FormInitialize()
    {
      FormStockWorkOrderPrintDetailListing.FormInitializeEventArgs initializeEventArgs1 = new FormStockWorkOrderPrintDetailListing.FormInitializeEventArgs(this);
      ScriptObject scriptObject = this.myScriptObject;
      string name = "OnFormInitialize";
      System.Type[] types = new System.Type[1];
      int index1 = 0;
      System.Type type = initializeEventArgs1.GetType();
      types[index1] = type;
      object[] objArray = new object[1];
      int index2 = 0;
      FormStockWorkOrderPrintDetailListing.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
      objArray[index2] = (object) initializeEventArgs2;
      scriptObject.RunMethod(name, types, objArray);
    }

    private void barBtnConvertToPlainText_ItemClick(object sender, ItemClickEventArgs e)
    {
      RichTextHelper.ConvertToPlainText(this.gvMaster);
    }

    private void gvMaster_DoubleClick(object sender, EventArgs e)
    {
      if (this.myMouseDownHelper.IsLeftMouseDown && BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView) sender).InRow && UserAuthentication.GetOrCreate(this.myDBSetting).AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
        this.GoToDocument();
    }

    private void GoToDocument()
    {
      ColumnView columnView = (ColumnView) this.gridControl1.FocusedView;
      int focusedRowHandle = columnView.FocusedRowHandle;
      DataRow dataRow = columnView.GetDataRow(focusedRowHandle);
      if (dataRow != null)
        DocumentDispatcher.Open(this.myDBSetting, "AO", BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormStockWorkOrderPrintDetailListing));
      this.panelCriteria = new PanelControl();
      this.gbFilter = new GroupControl();
      this.sbtnAdvanceSearch = new SimpleButton();
      this.xtraTabControl1 = new XtraTabControl();
      this.xtraTabPage1 = new XtraTabPage();
      this.ucStockAssemblyOrderSelector1 = new UCStockAssemblyOrderSelector();
      this.cbCancelledOption = new ComboBoxEdit();
      this.ucDateSelector1 = new UCDateSelector();
      this.label2 = new Label();
      this.label1 = new Label();
      this.label3 = new Label();
      this.xtraTabPage2 = new XtraTabPage();
      this.ucDepartmentSelector1 = new UCAllDepartmentsSelector();
      this.ucProjectSelector1 = new UCAllProjectsSelector();
      this.ucLocationSelector1 = new UCLocationSelector();
      this.ucItemTypeSelector1 = new UCItemTypeSelector();
      this.ucItemGroupSelector1 = new UCItemGroupSelector();
      this.ucItemSelector1 = new UCItemSelector();
      this.label7 = new Label();
      this.label5 = new Label();
      this.label6 = new Label();
      this.label4 = new Label();
      this.label8 = new Label();
      this.label9 = new Label();
      this.gbReport = new GroupControl();
      this.chkEditShowCriteria = new CheckEdit();
      this.cbEditSortBy = new ComboBoxEdit();
      this.cbEditGroupBy = new ComboBoxEdit();
      this.label11 = new Label();
      this.label10 = new Label();
      this.pnCriteriaBasic = new PanelControl();
      this.simpleButton1 = new SimpleButton();
      this.sbtnAdvOptions = new SimpleButton();
      this.groupControl1 = new GroupControl();
      this.chkEditShowCriteria2 = new CheckEdit();
      this.cbEditSortBy2 = new ComboBoxEdit();
      this.cbEditGroupBy2 = new ComboBoxEdit();
      this.label14 = new Label();
      this.label16 = new Label();
      this.gbBasic = new GroupControl();
      this.ucStockAssemblyOrderSelector2 = new UCStockAssemblyOrderSelector();
      this.ucItemSelector2 = new UCItemSelector();
      this.ucDateSelector2 = new UCDateSelector();
      this.label15 = new Label();
      this.label12 = new Label();
      this.label13 = new Label();
      this.panelCenter = new PanelControl();
      this.printButton1 = new PrintButton();
      this.previewButton1 = new PreviewButton();
      this.sbtnClose = new SimpleButton();
      this.sbtnToggleOptions = new SimpleButton();
      this.sbtnInquiry = new SimpleButton();
      this.panelControl2 = new PanelControl();
      this.xtraTabControl2 = new XtraTabControl();
      this.xtraTabPage3 = new XtraTabPage();
      this.gridControl1 = new GridControl();
      this.gvMaster = new GridView();
      this.colCheck = new GridColumn();
      this.repositoryItemCheckEdit1 = new RepositoryItemCheckEdit();
      this.colDocNo = new GridColumn();
      this.colDocDate = new GridColumn();
      this.colDescription = new GridColumn();
      this.colTotal = new GridColumn();
      this.colAssemblyCost = new GridColumn();
      this.colNetTotal = new GridColumn();
      this.colCancelled = new GridColumn();
      this.repositoryItemTextEdit_Cancelled = new RepositoryItemTextEdit();
      this.colItemCode = new GridColumn();
      this.colLocation = new GridColumn();
      this.colBatchNo = new GridColumn();
      this.colDtlDescription = new GridColumn();
      this.colFurtherDescription = new GridColumn();
      this.colProjNo = new GridColumn();
      this.colProjDesc = new GridColumn();
      this.colDeptNo = new GridColumn();
      this.colDeptDesc = new GridColumn();
      this.colRate = new GridColumn();
      this.colQty = new GridColumn();
      this.colItemCost = new GridColumn();
      this.colOverHeadCost = new GridColumn();
      this.colSubTotalCost = new GridColumn();
      this.colItemGroup = new GridColumn();
      this.colItemType = new GridColumn();
      this.colRemark1 = new GridColumn();
      this.colRemark2 = new GridColumn();
      this.colRemark3 = new GridColumn();
      this.colRemark4 = new GridColumn();
      this.colRefDocNo = new GridColumn();
      this.colPrintCount = new GridColumn();
      this.colLastModified = new GridColumn();
      this.colLastModifiedUserID = new GridColumn();
      this.colCreatedTimeStamp = new GridColumn();
      this.colCreatedUserID = new GridColumn();
      this.colExpCompletedDate = new GridColumn();
      this.ucSearchResult1 = new UCSearchResult();
      this.xtraTabPage4 = new XtraTabPage();
      this.memoEdit_Criteria = new MemoEdit();
      this.barManager1 = new BarManager(this.components);
      this.bar1 = new Bar();
      this.barSubItem1 = new BarSubItem();
      this.barBtnDesignDetailListingReport = new BarButtonItem();
      this.barbtnAdvancedFilter = new BarButtonItem();
      this.barBtnConvertToPlainText = new BarButtonItem();
      this.barDockControlTop = new BarDockControl();
      this.barDockControlBottom = new BarDockControl();
      this.barDockControlLeft = new BarDockControl();
      this.barDockControlRight = new BarDockControl();
      this.panelHeader1 = new PanelHeader();
      this.panelCriteria.BeginInit();
      this.panelCriteria.SuspendLayout();
      this.gbFilter.BeginInit();
      this.gbFilter.SuspendLayout();
      this.xtraTabControl1.BeginInit();
      this.xtraTabControl1.SuspendLayout();
      this.xtraTabPage1.SuspendLayout();
      this.cbCancelledOption.Properties.BeginInit();
      this.xtraTabPage2.SuspendLayout();
      this.gbReport.BeginInit();
      this.gbReport.SuspendLayout();
      this.chkEditShowCriteria.Properties.BeginInit();
      this.cbEditSortBy.Properties.BeginInit();
      this.cbEditGroupBy.Properties.BeginInit();
      this.pnCriteriaBasic.BeginInit();
      this.pnCriteriaBasic.SuspendLayout();
      this.groupControl1.BeginInit();
      this.groupControl1.SuspendLayout();
      this.chkEditShowCriteria2.Properties.BeginInit();
      this.cbEditSortBy2.Properties.BeginInit();
      this.cbEditGroupBy2.Properties.BeginInit();
      this.gbBasic.BeginInit();
      this.gbBasic.SuspendLayout();
      this.panelCenter.BeginInit();
      this.panelCenter.SuspendLayout();
      this.panelControl2.BeginInit();
      this.panelControl2.SuspendLayout();
      this.xtraTabControl2.BeginInit();
      this.xtraTabControl2.SuspendLayout();
      this.xtraTabPage3.SuspendLayout();
      this.gridControl1.BeginInit();
      this.gvMaster.BeginInit();
      this.repositoryItemCheckEdit1.BeginInit();
      this.repositoryItemTextEdit_Cancelled.BeginInit();
      this.xtraTabPage4.SuspendLayout();
      this.memoEdit_Criteria.Properties.BeginInit();
      this.barManager1.BeginInit();
      this.SuspendLayout();
      componentResourceManager.ApplyResources((object) this.panelCriteria, "panelCriteria");
      this.panelCriteria.BorderStyle = BorderStyles.NoBorder;
      this.panelCriteria.Controls.Add((Control) this.gbFilter);
      this.panelCriteria.Controls.Add((Control) this.gbReport);
      this.panelCriteria.Name = "panelCriteria";
      componentResourceManager.ApplyResources((object) this.gbFilter, "gbFilter");
      this.gbFilter.Controls.Add((Control) this.sbtnAdvanceSearch);
      this.gbFilter.Controls.Add((Control) this.xtraTabControl1);
      this.gbFilter.Name = "gbFilter";
      componentResourceManager.ApplyResources((object) this.sbtnAdvanceSearch, "sbtnAdvanceSearch");
      this.sbtnAdvanceSearch.Name = "sbtnAdvanceSearch";
      this.sbtnAdvanceSearch.Click += new EventHandler(this.sbtnAdvanceSearch_Click);
      componentResourceManager.ApplyResources((object) this.xtraTabControl1, "xtraTabControl1");
      this.xtraTabControl1.Name = "xtraTabControl1";
      this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
      XtraTabPageCollection tabPages1 = this.xtraTabControl1.TabPages;
      XtraTabPage[] pages1 = new XtraTabPage[2];
      int index1 = 0;
      XtraTabPage xtraTabPage1 = this.xtraTabPage1;
      pages1[index1] = xtraTabPage1;
      int index2 = 1;
      XtraTabPage xtraTabPage2 = this.xtraTabPage2;
      pages1[index2] = xtraTabPage2;
      tabPages1.AddRange(pages1);
      componentResourceManager.ApplyResources((object) this.xtraTabPage1, "xtraTabPage1");
      this.xtraTabPage1.Controls.Add((Control) this.ucStockAssemblyOrderSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.cbCancelledOption);
      this.xtraTabPage1.Controls.Add((Control) this.ucDateSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.label2);
      this.xtraTabPage1.Controls.Add((Control) this.label1);
      this.xtraTabPage1.Controls.Add((Control) this.label3);
      this.xtraTabPage1.Name = "xtraTabPage1";
      componentResourceManager.ApplyResources((object) this.ucStockAssemblyOrderSelector1, "ucStockAssemblyOrderSelector1");
      this.ucStockAssemblyOrderSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucStockAssemblyOrderSelector1.Appearance.BackColor");
      this.ucStockAssemblyOrderSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucStockAssemblyOrderSelector1.Appearance.GradientMode");
      this.ucStockAssemblyOrderSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucStockAssemblyOrderSelector1.Appearance.Image");
      this.ucStockAssemblyOrderSelector1.Appearance.Options.UseBackColor = true;
      this.ucStockAssemblyOrderSelector1.Name = "ucStockAssemblyOrderSelector1";
      componentResourceManager.ApplyResources((object) this.cbCancelledOption, "cbCancelledOption");
      this.cbCancelledOption.Name = "cbCancelledOption";
      this.cbCancelledOption.Properties.AccessibleDescription = componentResourceManager.GetString("cbCancelledOption.Properties.AccessibleDescription");
      this.cbCancelledOption.Properties.AccessibleName = componentResourceManager.GetString("cbCancelledOption.Properties.AccessibleName");
      this.cbCancelledOption.Properties.AutoHeight = (bool) componentResourceManager.GetObject("cbCancelledOption.Properties.AutoHeight");
      EditorButtonCollection buttons1 = this.cbCancelledOption.Properties.Buttons;
      EditorButton[] buttons2 = new EditorButton[1];
      int index3 = 0;
      EditorButton editorButton1 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbCancelledOption.Properties.Buttons"));
      buttons2[index3] = editorButton1;
      buttons1.AddRange(buttons2);
      ComboBoxItemCollection items1 = this.cbCancelledOption.Properties.Items;
      object[] items2 = new object[3];
      int index4 = 0;
      string string1 = componentResourceManager.GetString("cbCancelledOption.Properties.Items");
      items2[index4] = (object) string1;
      int index5 = 1;
      string string2 = componentResourceManager.GetString("cbCancelledOption.Properties.Items1");
      items2[index5] = (object) string2;
      int index6 = 2;
      string string3 = componentResourceManager.GetString("cbCancelledOption.Properties.Items2");
      items2[index6] = (object) string3;
      items1.AddRange(items2);
      this.cbCancelledOption.Properties.NullValuePrompt = componentResourceManager.GetString("cbCancelledOption.Properties.NullValuePrompt");
      this.cbCancelledOption.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("cbCancelledOption.Properties.NullValuePromptShowForEmptyValue");
      this.cbCancelledOption.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbCancelledOption.SelectedIndexChanged += new EventHandler(this.cbCancelledOption_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.ucDateSelector1, "ucDateSelector1");
      this.ucDateSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucDateSelector1.Appearance.BackColor");
      this.ucDateSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucDateSelector1.Appearance.GradientMode");
      this.ucDateSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucDateSelector1.Appearance.Image");
      this.ucDateSelector1.Appearance.Options.UseBackColor = true;
      this.ucDateSelector1.Name = "ucDateSelector1";
      componentResourceManager.ApplyResources((object) this.label2, "label2");
      this.label2.Name = "label2";
      componentResourceManager.ApplyResources((object) this.label1, "label1");
      this.label1.Name = "label1";
      componentResourceManager.ApplyResources((object) this.label3, "label3");
      this.label3.Name = "label3";
      componentResourceManager.ApplyResources((object) this.xtraTabPage2, "xtraTabPage2");
      this.xtraTabPage2.Controls.Add((Control) this.ucDepartmentSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucProjectSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucLocationSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucItemTypeSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucItemGroupSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucItemSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.label7);
      this.xtraTabPage2.Controls.Add((Control) this.label5);
      this.xtraTabPage2.Controls.Add((Control) this.label6);
      this.xtraTabPage2.Controls.Add((Control) this.label4);
      this.xtraTabPage2.Controls.Add((Control) this.label8);
      this.xtraTabPage2.Controls.Add((Control) this.label9);
      this.xtraTabPage2.Name = "xtraTabPage2";
      componentResourceManager.ApplyResources((object) this.ucDepartmentSelector1, "ucDepartmentSelector1");
      this.ucDepartmentSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucDepartmentSelector1.Appearance.BackColor");
      this.ucDepartmentSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucDepartmentSelector1.Appearance.GradientMode");
      this.ucDepartmentSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucDepartmentSelector1.Appearance.Image");
      this.ucDepartmentSelector1.Appearance.Options.UseBackColor = true;
      this.ucDepartmentSelector1.Name = "ucDepartmentSelector1";
      componentResourceManager.ApplyResources((object) this.ucProjectSelector1, "ucProjectSelector1");
      this.ucProjectSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucProjectSelector1.Appearance.BackColor");
      this.ucProjectSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucProjectSelector1.Appearance.GradientMode");
      this.ucProjectSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucProjectSelector1.Appearance.Image");
      this.ucProjectSelector1.Appearance.Options.UseBackColor = true;
      this.ucProjectSelector1.Name = "ucProjectSelector1";
      componentResourceManager.ApplyResources((object) this.ucLocationSelector1, "ucLocationSelector1");
      this.ucLocationSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucLocationSelector1.Appearance.BackColor");
      this.ucLocationSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucLocationSelector1.Appearance.GradientMode");
      this.ucLocationSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucLocationSelector1.Appearance.Image");
      this.ucLocationSelector1.Appearance.Options.UseBackColor = true;
      this.ucLocationSelector1.LocationType = UCLocationType.ItemLocation;
      this.ucLocationSelector1.Name = "ucLocationSelector1";
      componentResourceManager.ApplyResources((object) this.ucItemTypeSelector1, "ucItemTypeSelector1");
      this.ucItemTypeSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemTypeSelector1.Appearance.BackColor");
      this.ucItemTypeSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucItemTypeSelector1.Appearance.GradientMode");
      this.ucItemTypeSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucItemTypeSelector1.Appearance.Image");
      this.ucItemTypeSelector1.Appearance.Options.UseBackColor = true;
      this.ucItemTypeSelector1.Name = "ucItemTypeSelector1";
      componentResourceManager.ApplyResources((object) this.ucItemGroupSelector1, "ucItemGroupSelector1");
      this.ucItemGroupSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemGroupSelector1.Appearance.BackColor");
      this.ucItemGroupSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucItemGroupSelector1.Appearance.GradientMode");
      this.ucItemGroupSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucItemGroupSelector1.Appearance.Image");
      this.ucItemGroupSelector1.Appearance.Options.UseBackColor = true;
      this.ucItemGroupSelector1.Name = "ucItemGroupSelector1";
      componentResourceManager.ApplyResources((object) this.ucItemSelector1, "ucItemSelector1");
      this.ucItemSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemSelector1.Appearance.BackColor");
      this.ucItemSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucItemSelector1.Appearance.GradientMode");
      this.ucItemSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucItemSelector1.Appearance.Image");
      this.ucItemSelector1.Appearance.Options.UseBackColor = true;
      this.ucItemSelector1.Name = "ucItemSelector1";
      componentResourceManager.ApplyResources((object) this.label7, "label7");
      this.label7.Name = "label7";
      componentResourceManager.ApplyResources((object) this.label5, "label5");
      this.label5.Name = "label5";
      componentResourceManager.ApplyResources((object) this.label6, "label6");
      this.label6.Name = "label6";
      componentResourceManager.ApplyResources((object) this.label4, "label4");
      this.label4.Name = "label4";
      componentResourceManager.ApplyResources((object) this.label8, "label8");
      this.label8.Name = "label8";
      componentResourceManager.ApplyResources((object) this.label9, "label9");
      this.label9.Name = "label9";
      componentResourceManager.ApplyResources((object) this.gbReport, "gbReport");
      this.gbReport.Controls.Add((Control) this.chkEditShowCriteria);
      this.gbReport.Controls.Add((Control) this.cbEditSortBy);
      this.gbReport.Controls.Add((Control) this.cbEditGroupBy);
      this.gbReport.Controls.Add((Control) this.label11);
      this.gbReport.Controls.Add((Control) this.label10);
      this.gbReport.Name = "gbReport";
      componentResourceManager.ApplyResources((object) this.chkEditShowCriteria, "chkEditShowCriteria");
      this.chkEditShowCriteria.Name = "chkEditShowCriteria";
      this.chkEditShowCriteria.Properties.AccessibleDescription = componentResourceManager.GetString("chkEditShowCriteria.Properties.AccessibleDescription");
      this.chkEditShowCriteria.Properties.AccessibleName = componentResourceManager.GetString("chkEditShowCriteria.Properties.AccessibleName");
      this.chkEditShowCriteria.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEditShowCriteria.Properties.AutoHeight");
      this.chkEditShowCriteria.Properties.Caption = componentResourceManager.GetString("chkEditShowCriteria.Properties.Caption");
      this.chkEditShowCriteria.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEditShowCriteria.Properties.DisplayValueChecked");
      this.chkEditShowCriteria.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEditShowCriteria.Properties.DisplayValueGrayed");
      this.chkEditShowCriteria.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEditShowCriteria.Properties.DisplayValueUnchecked");
      this.chkEditShowCriteria.CheckedChanged += new EventHandler(this.chkEditShowCriteria_CheckedChanged);
      componentResourceManager.ApplyResources((object) this.cbEditSortBy, "cbEditSortBy");
      this.cbEditSortBy.Name = "cbEditSortBy";
      this.cbEditSortBy.Properties.AccessibleDescription = componentResourceManager.GetString("cbEditSortBy.Properties.AccessibleDescription");
      this.cbEditSortBy.Properties.AccessibleName = componentResourceManager.GetString("cbEditSortBy.Properties.AccessibleName");
      this.cbEditSortBy.Properties.AutoHeight = (bool) componentResourceManager.GetObject("cbEditSortBy.Properties.AutoHeight");
      EditorButtonCollection buttons3 = this.cbEditSortBy.Properties.Buttons;
      EditorButton[] buttons4 = new EditorButton[1];
      int index7 = 0;
      EditorButton editorButton2 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbEditSortBy.Properties.Buttons"));
      buttons4[index7] = editorButton2;
      buttons3.AddRange(buttons4);
      ComboBoxItemCollection items3 = this.cbEditSortBy.Properties.Items;
      object[] items4 = new object[8];
      int index8 = 0;
      string string4 = componentResourceManager.GetString("cbEditSortBy.Properties.Items");
      items4[index8] = (object) string4;
      int index9 = 1;
      string string5 = componentResourceManager.GetString("cbEditSortBy.Properties.Items1");
      items4[index9] = (object) string5;
      int index10 = 2;
      string string6 = componentResourceManager.GetString("cbEditSortBy.Properties.Items2");
      items4[index10] = (object) string6;
      int index11 = 3;
      string string7 = componentResourceManager.GetString("cbEditSortBy.Properties.Items3");
      items4[index11] = (object) string7;
      int index12 = 4;
      string string8 = componentResourceManager.GetString("cbEditSortBy.Properties.Items4");
      items4[index12] = (object) string8;
      int index13 = 5;
      string string9 = componentResourceManager.GetString("cbEditSortBy.Properties.Items5");
      items4[index13] = (object) string9;
      int index14 = 6;
      string string10 = componentResourceManager.GetString("cbEditSortBy.Properties.Items6");
      items4[index14] = (object) string10;
      int index15 = 7;
      string string11 = componentResourceManager.GetString("cbEditSortBy.Properties.Items7");
      items4[index15] = (object) string11;
      items3.AddRange(items4);
      this.cbEditSortBy.Properties.NullValuePrompt = componentResourceManager.GetString("cbEditSortBy.Properties.NullValuePrompt");
      this.cbEditSortBy.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("cbEditSortBy.Properties.NullValuePromptShowForEmptyValue");
      this.cbEditSortBy.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbEditSortBy.SelectedIndexChanged += new EventHandler(this.cbEditSortBy_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.cbEditGroupBy, "cbEditGroupBy");
      this.cbEditGroupBy.Name = "cbEditGroupBy";
      this.cbEditGroupBy.Properties.AccessibleDescription = componentResourceManager.GetString("cbEditGroupBy.Properties.AccessibleDescription");
      this.cbEditGroupBy.Properties.AccessibleName = componentResourceManager.GetString("cbEditGroupBy.Properties.AccessibleName");
      this.cbEditGroupBy.Properties.AutoHeight = (bool) componentResourceManager.GetObject("cbEditGroupBy.Properties.AutoHeight");
      EditorButtonCollection buttons5 = this.cbEditGroupBy.Properties.Buttons;
      EditorButton[] buttons6 = new EditorButton[1];
      int index16 = 0;
      EditorButton editorButton3 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbEditGroupBy.Properties.Buttons"));
      buttons6[index16] = editorButton3;
      buttons5.AddRange(buttons6);
      this.cbEditGroupBy.Properties.DropDownRows = 10;
      ComboBoxItemCollection items5 = this.cbEditGroupBy.Properties.Items;
      object[] items6 = new object[10];
      int index17 = 0;
      string string12 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items");
      items6[index17] = (object) string12;
      int index18 = 1;
      string string13 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items1");
      items6[index18] = (object) string13;
      int index19 = 2;
      string string14 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items2");
      items6[index19] = (object) string14;
      int index20 = 3;
      string string15 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items3");
      items6[index20] = (object) string15;
      int index21 = 4;
      string string16 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items4");
      items6[index21] = (object) string16;
      int index22 = 5;
      string string17 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items5");
      items6[index22] = (object) string17;
      int index23 = 6;
      string string18 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items6");
      items6[index23] = (object) string18;
      int index24 = 7;
      string string19 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items7");
      items6[index24] = (object) string19;
      int index25 = 8;
      string string20 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items8");
      items6[index25] = (object) string20;
      int index26 = 9;
      string string21 = componentResourceManager.GetString("cbEditGroupBy.Properties.Items9");
      items6[index26] = (object) string21;
      items5.AddRange(items6);
      this.cbEditGroupBy.Properties.NullValuePrompt = componentResourceManager.GetString("cbEditGroupBy.Properties.NullValuePrompt");
      this.cbEditGroupBy.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("cbEditGroupBy.Properties.NullValuePromptShowForEmptyValue");
      this.cbEditGroupBy.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbEditGroupBy.SelectedIndexChanged += new EventHandler(this.cbEditGroupBy_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.label11, "label11");
      this.label11.Name = "label11";
      componentResourceManager.ApplyResources((object) this.label10, "label10");
      this.label10.Name = "label10";
      componentResourceManager.ApplyResources((object) this.pnCriteriaBasic, "pnCriteriaBasic");
      this.pnCriteriaBasic.BorderStyle = BorderStyles.NoBorder;
      this.pnCriteriaBasic.Controls.Add((Control) this.simpleButton1);
      this.pnCriteriaBasic.Controls.Add((Control) this.sbtnAdvOptions);
      this.pnCriteriaBasic.Controls.Add((Control) this.groupControl1);
      this.pnCriteriaBasic.Controls.Add((Control) this.gbBasic);
      this.pnCriteriaBasic.Name = "pnCriteriaBasic";
      componentResourceManager.ApplyResources((object) this.simpleButton1, "simpleButton1");
      this.simpleButton1.Name = "simpleButton1";
      this.simpleButton1.Click += new EventHandler(this.sbtnAdvanceSearch_Click);
      componentResourceManager.ApplyResources((object) this.sbtnAdvOptions, "sbtnAdvOptions");
      this.sbtnAdvOptions.Name = "sbtnAdvOptions";
      this.sbtnAdvOptions.Click += new EventHandler(this.sbtnAdvOptions_Click);
      componentResourceManager.ApplyResources((object) this.groupControl1, "groupControl1");
      this.groupControl1.Controls.Add((Control) this.chkEditShowCriteria2);
      this.groupControl1.Controls.Add((Control) this.cbEditSortBy2);
      this.groupControl1.Controls.Add((Control) this.cbEditGroupBy2);
      this.groupControl1.Controls.Add((Control) this.label14);
      this.groupControl1.Controls.Add((Control) this.label16);
      this.groupControl1.Name = "groupControl1";
      componentResourceManager.ApplyResources((object) this.chkEditShowCriteria2, "chkEditShowCriteria2");
      this.chkEditShowCriteria2.Name = "chkEditShowCriteria2";
      this.chkEditShowCriteria2.Properties.AccessibleDescription = componentResourceManager.GetString("chkEditShowCriteria2.Properties.AccessibleDescription");
      this.chkEditShowCriteria2.Properties.AccessibleName = componentResourceManager.GetString("chkEditShowCriteria2.Properties.AccessibleName");
      this.chkEditShowCriteria2.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEditShowCriteria2.Properties.AutoHeight");
      this.chkEditShowCriteria2.Properties.Caption = componentResourceManager.GetString("chkEditShowCriteria2.Properties.Caption");
      this.chkEditShowCriteria2.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEditShowCriteria2.Properties.DisplayValueChecked");
      this.chkEditShowCriteria2.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEditShowCriteria2.Properties.DisplayValueGrayed");
      this.chkEditShowCriteria2.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEditShowCriteria2.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.cbEditSortBy2, "cbEditSortBy2");
      this.cbEditSortBy2.Name = "cbEditSortBy2";
      this.cbEditSortBy2.Properties.AccessibleDescription = componentResourceManager.GetString("cbEditSortBy2.Properties.AccessibleDescription");
      this.cbEditSortBy2.Properties.AccessibleName = componentResourceManager.GetString("cbEditSortBy2.Properties.AccessibleName");
      this.cbEditSortBy2.Properties.AutoHeight = (bool) componentResourceManager.GetObject("cbEditSortBy2.Properties.AutoHeight");
      EditorButtonCollection buttons7 = this.cbEditSortBy2.Properties.Buttons;
      EditorButton[] buttons8 = new EditorButton[1];
      int index27 = 0;
      EditorButton editorButton4 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbEditSortBy2.Properties.Buttons"));
      buttons8[index27] = editorButton4;
      buttons7.AddRange(buttons8);
      ComboBoxItemCollection items7 = this.cbEditSortBy2.Properties.Items;
      object[] items8 = new object[8];
      int index28 = 0;
      string string22 = componentResourceManager.GetString("cbEditSortBy2.Properties.Items");
      items8[index28] = (object) string22;
      int index29 = 1;
      string string23 = componentResourceManager.GetString("cbEditSortBy2.Properties.Items1");
      items8[index29] = (object) string23;
      int index30 = 2;
      string string24 = componentResourceManager.GetString("cbEditSortBy2.Properties.Items2");
      items8[index30] = (object) string24;
      int index31 = 3;
      string string25 = componentResourceManager.GetString("cbEditSortBy2.Properties.Items3");
      items8[index31] = (object) string25;
      int index32 = 4;
      string string26 = componentResourceManager.GetString("cbEditSortBy2.Properties.Items4");
      items8[index32] = (object) string26;
      int index33 = 5;
      string string27 = componentResourceManager.GetString("cbEditSortBy2.Properties.Items5");
      items8[index33] = (object) string27;
      int index34 = 6;
      string string28 = componentResourceManager.GetString("cbEditSortBy2.Properties.Items6");
      items8[index34] = (object) string28;
      int index35 = 7;
      string string29 = componentResourceManager.GetString("cbEditSortBy2.Properties.Items7");
      items8[index35] = (object) string29;
      items7.AddRange(items8);
      this.cbEditSortBy2.Properties.NullValuePrompt = componentResourceManager.GetString("cbEditSortBy2.Properties.NullValuePrompt");
      this.cbEditSortBy2.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("cbEditSortBy2.Properties.NullValuePromptShowForEmptyValue");
      this.cbEditSortBy2.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbEditSortBy2.SelectedIndexChanged += new EventHandler(this.cbEditSortBy2_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.cbEditGroupBy2, "cbEditGroupBy2");
      this.cbEditGroupBy2.Name = "cbEditGroupBy2";
      this.cbEditGroupBy2.Properties.AccessibleDescription = componentResourceManager.GetString("cbEditGroupBy2.Properties.AccessibleDescription");
      this.cbEditGroupBy2.Properties.AccessibleName = componentResourceManager.GetString("cbEditGroupBy2.Properties.AccessibleName");
      this.cbEditGroupBy2.Properties.AutoHeight = (bool) componentResourceManager.GetObject("cbEditGroupBy2.Properties.AutoHeight");
      EditorButtonCollection buttons9 = this.cbEditGroupBy2.Properties.Buttons;
      EditorButton[] buttons10 = new EditorButton[1];
      int index36 = 0;
      EditorButton editorButton5 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbEditGroupBy2.Properties.Buttons"));
      buttons10[index36] = editorButton5;
      buttons9.AddRange(buttons10);
      this.cbEditGroupBy2.Properties.DropDownRows = 10;
      ComboBoxItemCollection items9 = this.cbEditGroupBy2.Properties.Items;
      object[] items10 = new object[10];
      int index37 = 0;
      string string30 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items");
      items10[index37] = (object) string30;
      int index38 = 1;
      string string31 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items1");
      items10[index38] = (object) string31;
      int index39 = 2;
      string string32 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items2");
      items10[index39] = (object) string32;
      int index40 = 3;
      string string33 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items3");
      items10[index40] = (object) string33;
      int index41 = 4;
      string string34 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items4");
      items10[index41] = (object) string34;
      int index42 = 5;
      string string35 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items5");
      items10[index42] = (object) string35;
      int index43 = 6;
      string string36 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items6");
      items10[index43] = (object) string36;
      int index44 = 7;
      string string37 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items7");
      items10[index44] = (object) string37;
      int index45 = 8;
      string string38 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items8");
      items10[index45] = (object) string38;
      int index46 = 9;
      string string39 = componentResourceManager.GetString("cbEditGroupBy2.Properties.Items9");
      items10[index46] = (object) string39;
      items9.AddRange(items10);
      this.cbEditGroupBy2.Properties.NullValuePrompt = componentResourceManager.GetString("cbEditGroupBy2.Properties.NullValuePrompt");
      this.cbEditGroupBy2.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("cbEditGroupBy2.Properties.NullValuePromptShowForEmptyValue");
      this.cbEditGroupBy2.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbEditGroupBy2.SelectedIndexChanged += new EventHandler(this.cbEditGroupBy2_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.label14, "label14");
      this.label14.Name = "label14";
      componentResourceManager.ApplyResources((object) this.label16, "label16");
      this.label16.Name = "label16";
      componentResourceManager.ApplyResources((object) this.gbBasic, "gbBasic");
      this.gbBasic.Controls.Add((Control) this.ucStockAssemblyOrderSelector2);
      this.gbBasic.Controls.Add((Control) this.ucItemSelector2);
      this.gbBasic.Controls.Add((Control) this.ucDateSelector2);
      this.gbBasic.Controls.Add((Control) this.label15);
      this.gbBasic.Controls.Add((Control) this.label12);
      this.gbBasic.Controls.Add((Control) this.label13);
      this.gbBasic.Name = "gbBasic";
      componentResourceManager.ApplyResources((object) this.ucStockAssemblyOrderSelector2, "ucStockAssemblyOrderSelector2");
      this.ucStockAssemblyOrderSelector2.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucStockAssemblyOrderSelector2.Appearance.BackColor");
      this.ucStockAssemblyOrderSelector2.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucStockAssemblyOrderSelector2.Appearance.GradientMode");
      this.ucStockAssemblyOrderSelector2.Appearance.Image = (Image) componentResourceManager.GetObject("ucStockAssemblyOrderSelector2.Appearance.Image");
      this.ucStockAssemblyOrderSelector2.Appearance.Options.UseBackColor = true;
      this.ucStockAssemblyOrderSelector2.Name = "ucStockAssemblyOrderSelector2";
      componentResourceManager.ApplyResources((object) this.ucItemSelector2, "ucItemSelector2");
      this.ucItemSelector2.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemSelector2.Appearance.BackColor");
      this.ucItemSelector2.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucItemSelector2.Appearance.GradientMode");
      this.ucItemSelector2.Appearance.Image = (Image) componentResourceManager.GetObject("ucItemSelector2.Appearance.Image");
      this.ucItemSelector2.Appearance.Options.UseBackColor = true;
      this.ucItemSelector2.Name = "ucItemSelector2";
      componentResourceManager.ApplyResources((object) this.ucDateSelector2, "ucDateSelector2");
      this.ucDateSelector2.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucDateSelector2.Appearance.BackColor");
      this.ucDateSelector2.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucDateSelector2.Appearance.GradientMode");
      this.ucDateSelector2.Appearance.Image = (Image) componentResourceManager.GetObject("ucDateSelector2.Appearance.Image");
      this.ucDateSelector2.Appearance.Options.UseBackColor = true;
      this.ucDateSelector2.Name = "ucDateSelector2";
      componentResourceManager.ApplyResources((object) this.label15, "label15");
      this.label15.Name = "label15";
      componentResourceManager.ApplyResources((object) this.label12, "label12");
      this.label12.Name = "label12";
      componentResourceManager.ApplyResources((object) this.label13, "label13");
      this.label13.Name = "label13";
      componentResourceManager.ApplyResources((object) this.panelCenter, "panelCenter");
      this.panelCenter.BorderStyle = BorderStyles.NoBorder;
      this.panelCenter.Controls.Add((Control) this.printButton1);
      this.panelCenter.Controls.Add((Control) this.previewButton1);
      this.panelCenter.Controls.Add((Control) this.sbtnClose);
      this.panelCenter.Controls.Add((Control) this.sbtnToggleOptions);
      this.panelCenter.Controls.Add((Control) this.sbtnInquiry);
      this.panelCenter.Name = "panelCenter";
      componentResourceManager.ApplyResources((object) this.printButton1, "printButton1");
      this.printButton1.Name = "printButton1";
      this.printButton1.ReportType = "";
      this.printButton1.Print += new PrintEventHandler(this.printButton1_Print);
      componentResourceManager.ApplyResources((object) this.previewButton1, "previewButton1");
      this.previewButton1.Name = "previewButton1";
      this.previewButton1.ReportType = "";
      this.previewButton1.Preview += new PrintEventHandler(this.previewButton1_Preview);
      componentResourceManager.ApplyResources((object) this.sbtnClose, "sbtnClose");
      this.sbtnClose.DialogResult = DialogResult.Cancel;
      this.sbtnClose.Name = "sbtnClose";
      this.sbtnClose.Click += new EventHandler(this.sbtnClose_Click);
      componentResourceManager.ApplyResources((object) this.sbtnToggleOptions, "sbtnToggleOptions");
      this.sbtnToggleOptions.Name = "sbtnToggleOptions";
      this.sbtnToggleOptions.Click += new EventHandler(this.sbtnToggleOptions_Click);
      componentResourceManager.ApplyResources((object) this.sbtnInquiry, "sbtnInquiry");
      this.sbtnInquiry.Name = "sbtnInquiry";
      this.sbtnInquiry.Click += new EventHandler(this.sbtnInquiry_Click);
      componentResourceManager.ApplyResources((object) this.panelControl2, "panelControl2");
      this.panelControl2.BorderStyle = BorderStyles.NoBorder;
      this.panelControl2.Controls.Add((Control) this.xtraTabControl2);
      this.panelControl2.Name = "panelControl2";
      componentResourceManager.ApplyResources((object) this.xtraTabControl2, "xtraTabControl2");
      this.xtraTabControl2.Name = "xtraTabControl2";
      this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
      XtraTabPageCollection tabPages2 = this.xtraTabControl2.TabPages;
      XtraTabPage[] pages2 = new XtraTabPage[2];
      int index47 = 0;
      XtraTabPage xtraTabPage3 = this.xtraTabPage3;
      pages2[index47] = xtraTabPage3;
      int index48 = 1;
      XtraTabPage xtraTabPage4 = this.xtraTabPage4;
      pages2[index48] = xtraTabPage4;
      tabPages2.AddRange(pages2);
      componentResourceManager.ApplyResources((object) this.xtraTabPage3, "xtraTabPage3");
      this.xtraTabPage3.Controls.Add((Control) this.gridControl1);
      this.xtraTabPage3.Controls.Add((Control) this.ucSearchResult1);
      this.xtraTabPage3.Name = "xtraTabPage3";
      componentResourceManager.ApplyResources((object) this.gridControl1, "gridControl1");
      this.gridControl1.EmbeddedNavigator.AccessibleDescription = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.AccessibleDescription");
      this.gridControl1.EmbeddedNavigator.AccessibleName = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.AccessibleName");
      this.gridControl1.EmbeddedNavigator.AllowHtmlTextInToolTip = (DefaultBoolean) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.AllowHtmlTextInToolTip");
      this.gridControl1.EmbeddedNavigator.Anchor = (AnchorStyles) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.Anchor");
      this.gridControl1.EmbeddedNavigator.BackgroundImage = (Image) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.BackgroundImage");
      this.gridControl1.EmbeddedNavigator.BackgroundImageLayout = (ImageLayout) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.BackgroundImageLayout");
      this.gridControl1.EmbeddedNavigator.Buttons.Append.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.Append.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.Edit.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.Edit.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.EndEdit.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.Remove.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.Remove.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
      this.gridControl1.EmbeddedNavigator.ImeMode = (ImeMode) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.ImeMode");
      this.gridControl1.EmbeddedNavigator.MaximumSize = (Size) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.MaximumSize");
      this.gridControl1.EmbeddedNavigator.TextLocation = (NavigatorButtonsTextLocation) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.TextLocation");
      this.gridControl1.EmbeddedNavigator.ToolTip = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.ToolTip");
      this.gridControl1.EmbeddedNavigator.ToolTipIconType = (ToolTipIconType) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.ToolTipIconType");
      this.gridControl1.EmbeddedNavigator.ToolTipTitle = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.ToolTipTitle");
      this.gridControl1.MainView = (BaseView) this.gvMaster;
      this.gridControl1.Name = "gridControl1";
      RepositoryItemCollection repositoryItems = this.gridControl1.RepositoryItems;
      RepositoryItem[] items11 = new RepositoryItem[2];
      int index49 = 0;
      RepositoryItemCheckEdit repositoryItemCheckEdit = this.repositoryItemCheckEdit1;
      items11[index49] = (RepositoryItem) repositoryItemCheckEdit;
      int index50 = 1;
      RepositoryItemTextEdit repositoryItemTextEdit = this.repositoryItemTextEdit_Cancelled;
      items11[index50] = (RepositoryItem) repositoryItemTextEdit;
      repositoryItems.AddRange(items11);
      this.gridControl1.UseEmbeddedNavigator = true;
      ViewRepositoryCollection viewCollection = this.gridControl1.ViewCollection;
      BaseView[] views = new BaseView[1];
      int index51 = 0;
      GridView gridView = this.gvMaster;
      views[index51] = (BaseView) gridView;
      viewCollection.AddRange(views);
      componentResourceManager.ApplyResources((object) this.gvMaster, "gvMaster");
      GridColumnCollection columns1 = this.gvMaster.Columns;
      GridColumn[] columns2 = new GridColumn[35];
      int index52 = 0;
      GridColumn gridColumn1 = this.colCheck;
      columns2[index52] = gridColumn1;
      int index53 = 1;
      GridColumn gridColumn2 = this.colDocNo;
      columns2[index53] = gridColumn2;
      int index54 = 2;
      GridColumn gridColumn3 = this.colDocDate;
      columns2[index54] = gridColumn3;
      int index55 = 3;
      GridColumn gridColumn4 = this.colDescription;
      columns2[index55] = gridColumn4;
      int index56 = 4;
      GridColumn gridColumn5 = this.colTotal;
      columns2[index56] = gridColumn5;
      int index57 = 5;
      GridColumn gridColumn6 = this.colAssemblyCost;
      columns2[index57] = gridColumn6;
      int index58 = 6;
      GridColumn gridColumn7 = this.colNetTotal;
      columns2[index58] = gridColumn7;
      int index59 = 7;
      GridColumn gridColumn8 = this.colCancelled;
      columns2[index59] = gridColumn8;
      int index60 = 8;
      GridColumn gridColumn9 = this.colItemCode;
      columns2[index60] = gridColumn9;
      int index61 = 9;
      GridColumn gridColumn10 = this.colLocation;
      columns2[index61] = gridColumn10;
      int index62 = 10;
      GridColumn gridColumn11 = this.colBatchNo;
      columns2[index62] = gridColumn11;
      int index63 = 11;
      GridColumn gridColumn12 = this.colDtlDescription;
      columns2[index63] = gridColumn12;
      int index64 = 12;
      GridColumn gridColumn13 = this.colFurtherDescription;
      columns2[index64] = gridColumn13;
      int index65 = 13;
      GridColumn gridColumn14 = this.colProjNo;
      columns2[index65] = gridColumn14;
      int index66 = 14;
      GridColumn gridColumn15 = this.colProjDesc;
      columns2[index66] = gridColumn15;
      int index67 = 15;
      GridColumn gridColumn16 = this.colDeptNo;
      columns2[index67] = gridColumn16;
      int index68 = 16;
      GridColumn gridColumn17 = this.colDeptDesc;
      columns2[index68] = gridColumn17;
      int index69 = 17;
      GridColumn gridColumn18 = this.colRate;
      columns2[index69] = gridColumn18;
      int index70 = 18;
      GridColumn gridColumn19 = this.colQty;
      columns2[index70] = gridColumn19;
      int index71 = 19;
      GridColumn gridColumn20 = this.colItemCost;
      columns2[index71] = gridColumn20;
      int index72 = 20;
      GridColumn gridColumn21 = this.colOverHeadCost;
      columns2[index72] = gridColumn21;
      int index73 = 21;
      GridColumn gridColumn22 = this.colSubTotalCost;
      columns2[index73] = gridColumn22;
      int index74 = 22;
      GridColumn gridColumn23 = this.colItemGroup;
      columns2[index74] = gridColumn23;
      int index75 = 23;
      GridColumn gridColumn24 = this.colItemType;
      columns2[index75] = gridColumn24;
      int index76 = 24;
      GridColumn gridColumn25 = this.colRemark1;
      columns2[index76] = gridColumn25;
      int index77 = 25;
      GridColumn gridColumn26 = this.colRemark2;
      columns2[index77] = gridColumn26;
      int index78 = 26;
      GridColumn gridColumn27 = this.colRemark3;
      columns2[index78] = gridColumn27;
      int index79 = 27;
      GridColumn gridColumn28 = this.colRemark4;
      columns2[index79] = gridColumn28;
      int index80 = 28;
      GridColumn gridColumn29 = this.colRefDocNo;
      columns2[index80] = gridColumn29;
      int index81 = 29;
      GridColumn gridColumn30 = this.colPrintCount;
      columns2[index81] = gridColumn30;
      int index82 = 30;
      GridColumn gridColumn31 = this.colLastModified;
      columns2[index82] = gridColumn31;
      int index83 = 31;
      GridColumn gridColumn32 = this.colLastModifiedUserID;
      columns2[index83] = gridColumn32;
      int index84 = 32;
      GridColumn gridColumn33 = this.colCreatedTimeStamp;
      columns2[index84] = gridColumn33;
      int index85 = 33;
      GridColumn gridColumn34 = this.colCreatedUserID;
      columns2[index85] = gridColumn34;
      int index86 = 34;
      GridColumn gridColumn35 = this.colExpCompletedDate;
      columns2[index86] = gridColumn35;
      columns1.AddRange(columns2);
      this.gvMaster.GridControl = this.gridControl1;
      this.gvMaster.Name = "gvMaster";
      this.gvMaster.OptionsSelection.MultiSelect = true;
      this.gvMaster.OptionsView.ColumnAutoWidth = false;
      this.gvMaster.OptionsView.ShowFooter = true;
      this.gvMaster.SelectionChanged += new SelectionChangedEventHandler(this.gvMaster_SelectionChanged);
      this.gvMaster.DoubleClick += new EventHandler(this.gvMaster_DoubleClick);
      componentResourceManager.ApplyResources((object) this.colCheck, "colCheck");
      this.colCheck.ColumnEdit = (RepositoryItem) this.repositoryItemCheckEdit1;
      this.colCheck.FieldName = "ToBeUpdate";
      this.colCheck.Name = "colCheck";
      componentResourceManager.ApplyResources((object) this.repositoryItemCheckEdit1, "repositoryItemCheckEdit1");
      this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
      this.repositoryItemCheckEdit1.NullStyle = StyleIndeterminate.Unchecked;
      componentResourceManager.ApplyResources((object) this.colDocNo, "colDocNo");
      this.colDocNo.FieldName = "DocNo";
      this.colDocNo.Name = "colDocNo";
      this.colDocNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDocDate, "colDocDate");
      this.colDocDate.FieldName = "DocDate";
      this.colDocDate.Name = "colDocDate";
      this.colDocDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDescription, "colDescription");
      this.colDescription.FieldName = "Description";
      this.colDescription.Name = "colDescription";
      this.colDescription.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colTotal, "colTotal");
      this.colTotal.FieldName = "Total";
      this.colTotal.Name = "colTotal";
      this.colTotal.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colAssemblyCost, "colAssemblyCost");
      this.colAssemblyCost.FieldName = "AssemblyCost";
      this.colAssemblyCost.Name = "colAssemblyCost";
      this.colAssemblyCost.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colNetTotal, "colNetTotal");
      this.colNetTotal.FieldName = "NetTotal";
      this.colNetTotal.Name = "colNetTotal";
      this.colNetTotal.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCancelled, "colCancelled");
      this.colCancelled.ColumnEdit = (RepositoryItem) this.repositoryItemTextEdit_Cancelled;
      this.colCancelled.FieldName = "Cancelled";
      this.colCancelled.Name = "colCancelled";
      this.colCancelled.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.repositoryItemTextEdit_Cancelled, "repositoryItemTextEdit_Cancelled");
      this.repositoryItemTextEdit_Cancelled.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.AutoComplete");
      this.repositoryItemTextEdit_Cancelled.Mask.BeepOnError = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.BeepOnError");
      this.repositoryItemTextEdit_Cancelled.Mask.EditMask = componentResourceManager.GetString("repositoryItemTextEdit_Cancelled.Mask.EditMask");
      this.repositoryItemTextEdit_Cancelled.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.IgnoreMaskBlank");
      this.repositoryItemTextEdit_Cancelled.Mask.MaskType = (DevExpress.XtraEditors.Mask.MaskType) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.MaskType");
      this.repositoryItemTextEdit_Cancelled.Mask.PlaceHolder = (char) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.PlaceHolder");
      this.repositoryItemTextEdit_Cancelled.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.SaveLiteral");
      this.repositoryItemTextEdit_Cancelled.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.ShowPlaceHolders");
      this.repositoryItemTextEdit_Cancelled.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.UseMaskAsDisplayFormat");
      this.repositoryItemTextEdit_Cancelled.Name = "repositoryItemTextEdit_Cancelled";
      this.repositoryItemTextEdit_Cancelled.FormatEditValue += new ConvertEditValueEventHandler(this.repositoryItemTextEdit_Cancelled_FormatEditValue);
      componentResourceManager.ApplyResources((object) this.colItemCode, "colItemCode");
      this.colItemCode.FieldName = "ItemCode";
      this.colItemCode.Name = "colItemCode";
      this.colItemCode.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLocation, "colLocation");
      this.colLocation.FieldName = "Location";
      this.colLocation.Name = "colLocation";
      this.colLocation.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colBatchNo, "colBatchNo");
      this.colBatchNo.FieldName = "BatchNo";
      this.colBatchNo.Name = "colBatchNo";
      this.colBatchNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDtlDescription, "colDtlDescription");
      this.colDtlDescription.FieldName = "DtlDescription";
      this.colDtlDescription.Name = "colDtlDescription";
      this.colDtlDescription.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colFurtherDescription, "colFurtherDescription");
      this.colFurtherDescription.FieldName = "FurtherDescription";
      this.colFurtherDescription.Name = "colFurtherDescription";
      this.colFurtherDescription.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colProjNo, "colProjNo");
      this.colProjNo.FieldName = "ProjNo";
      this.colProjNo.Name = "colProjNo";
      this.colProjNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colProjDesc, "colProjDesc");
      this.colProjDesc.FieldName = "ProjDesc";
      this.colProjDesc.Name = "colProjDesc";
      this.colProjDesc.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDeptNo, "colDeptNo");
      this.colDeptNo.FieldName = "DeptNo";
      this.colDeptNo.Name = "colDeptNo";
      this.colDeptNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDeptDesc, "colDeptDesc");
      this.colDeptDesc.FieldName = "DeptDesc";
      this.colDeptDesc.Name = "colDeptDesc";
      this.colDeptDesc.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRate, "colRate");
      this.colRate.FieldName = "Rate";
      this.colRate.Name = "colRate";
      this.colRate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colQty, "colQty");
      this.colQty.FieldName = "Qty";
      this.colQty.Name = "colQty";
      this.colQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colItemCost, "colItemCost");
      this.colItemCost.FieldName = "ItemCost";
      this.colItemCost.Name = "colItemCost";
      this.colItemCost.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colOverHeadCost, "colOverHeadCost");
      this.colOverHeadCost.FieldName = "OverHeadCost";
      this.colOverHeadCost.Name = "colOverHeadCost";
      this.colOverHeadCost.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colSubTotalCost, "colSubTotalCost");
      this.colSubTotalCost.FieldName = "SubTotalCost";
      this.colSubTotalCost.Name = "colSubTotalCost";
      this.colSubTotalCost.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colItemGroup, "colItemGroup");
      this.colItemGroup.FieldName = "ItemGroup";
      this.colItemGroup.Name = "colItemGroup";
      this.colItemGroup.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colItemType, "colItemType");
      this.colItemType.FieldName = "ItemType";
      this.colItemType.Name = "colItemType";
      this.colItemType.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark1, "colRemark1");
      this.colRemark1.FieldName = "Remark1";
      this.colRemark1.Name = "colRemark1";
      this.colRemark1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark2, "colRemark2");
      this.colRemark2.FieldName = "Remark2";
      this.colRemark2.Name = "colRemark2";
      this.colRemark2.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark3, "colRemark3");
      this.colRemark3.FieldName = "Remark3";
      this.colRemark3.Name = "colRemark3";
      this.colRemark3.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark4, "colRemark4");
      this.colRemark4.FieldName = "Remark4";
      this.colRemark4.Name = "colRemark4";
      this.colRemark4.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRefDocNo, "colRefDocNo");
      this.colRefDocNo.FieldName = "RefDocNo";
      this.colRefDocNo.Name = "colRefDocNo";
      this.colRefDocNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colPrintCount, "colPrintCount");
      this.colPrintCount.FieldName = "PrintCount";
      this.colPrintCount.Name = "colPrintCount";
      this.colPrintCount.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLastModified, "colLastModified");
      this.colLastModified.FieldName = "LastModified";
      this.colLastModified.Name = "colLastModified";
      this.colLastModified.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLastModifiedUserID, "colLastModifiedUserID");
      this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
      this.colLastModifiedUserID.Name = "colLastModifiedUserID";
      this.colLastModifiedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedTimeStamp, "colCreatedTimeStamp");
      this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
      this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
      this.colCreatedTimeStamp.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedUserID, "colCreatedUserID");
      this.colCreatedUserID.FieldName = "CreatedUserID";
      this.colCreatedUserID.Name = "colCreatedUserID";
      this.colCreatedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colExpCompletedDate, "colExpCompletedDate");
      this.colExpCompletedDate.FieldName = "ExpectedCompletedDate";
      this.colExpCompletedDate.Name = "colExpCompletedDate";
      this.colExpCompletedDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.ucSearchResult1, "ucSearchResult1");
      this.ucSearchResult1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor");
      this.ucSearchResult1.Appearance.BackColor2 = (Color) componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor2");
      this.ucSearchResult1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucSearchResult1.Appearance.GradientMode");
      this.ucSearchResult1.Appearance.Image = (Image) componentResourceManager.GetObject("ucSearchResult1.Appearance.Image");
      this.ucSearchResult1.Appearance.Options.UseBackColor = true;
      this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
      this.ucSearchResult1.Name = "ucSearchResult1";
      componentResourceManager.ApplyResources((object) this.xtraTabPage4, "xtraTabPage4");
      this.xtraTabPage4.Controls.Add((Control) this.memoEdit_Criteria);
      this.xtraTabPage4.Name = "xtraTabPage4";
      componentResourceManager.ApplyResources((object) this.memoEdit_Criteria, "memoEdit_Criteria");
      this.memoEdit_Criteria.Name = "memoEdit_Criteria";
      this.memoEdit_Criteria.Properties.AccessibleDescription = componentResourceManager.GetString("memoEdit_Criteria.Properties.AccessibleDescription");
      this.memoEdit_Criteria.Properties.AccessibleName = componentResourceManager.GetString("memoEdit_Criteria.Properties.AccessibleName");
      this.memoEdit_Criteria.Properties.Appearance.Font = (Font) componentResourceManager.GetObject("memoEdit_Criteria.Properties.Appearance.Font");
      this.memoEdit_Criteria.Properties.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("memoEdit_Criteria.Properties.Appearance.GradientMode");
      this.memoEdit_Criteria.Properties.Appearance.Image = (Image) componentResourceManager.GetObject("memoEdit_Criteria.Properties.Appearance.Image");
      this.memoEdit_Criteria.Properties.Appearance.Options.UseFont = true;
      this.memoEdit_Criteria.Properties.NullValuePrompt = componentResourceManager.GetString("memoEdit_Criteria.Properties.NullValuePrompt");
      this.memoEdit_Criteria.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("memoEdit_Criteria.Properties.NullValuePromptShowForEmptyValue");
      this.memoEdit_Criteria.Properties.ReadOnly = true;
      this.memoEdit_Criteria.UseOptimizedRendering = true;
      Bars bars1 = this.barManager1.Bars;
      Bar[] bars2 = new Bar[1];
      int index87 = 0;
      Bar bar = this.bar1;
      bars2[index87] = bar;
      bars1.AddRange(bars2);
      this.barManager1.DockControls.Add(this.barDockControlTop);
      this.barManager1.DockControls.Add(this.barDockControlBottom);
      this.barManager1.DockControls.Add(this.barDockControlLeft);
      this.barManager1.DockControls.Add(this.barDockControlRight);
      this.barManager1.Form = (Control) this;
      BarItems items12 = this.barManager1.Items;
      BarItem[] items13 = new BarItem[4];
      int index88 = 0;
      BarSubItem barSubItem = this.barSubItem1;
      items13[index88] = (BarItem) barSubItem;
      int index89 = 1;
      BarButtonItem barButtonItem1 = this.barbtnAdvancedFilter;
      items13[index89] = (BarItem) barButtonItem1;
      int index90 = 2;
      BarButtonItem barButtonItem2 = this.barBtnDesignDetailListingReport;
      items13[index90] = (BarItem) barButtonItem2;
      int index91 = 3;
      BarButtonItem barButtonItem3 = this.barBtnConvertToPlainText;
      items13[index91] = (BarItem) barButtonItem3;
      items12.AddRange(items13);
      this.barManager1.MainMenu = this.bar1;
      this.barManager1.MaxItemId = 4;
      this.bar1.BarName = "Custom 1";
      this.bar1.DockCol = 0;
      this.bar1.DockRow = 0;
      this.bar1.DockStyle = BarDockStyle.Top;
      LinksInfo linksPersistInfo1 = this.bar1.LinksPersistInfo;
      LinkPersistInfo[] links1 = new LinkPersistInfo[1];
      int index92 = 0;
      LinkPersistInfo linkPersistInfo1 = new LinkPersistInfo((BarItem) this.barSubItem1, true);
      links1[index92] = linkPersistInfo1;
      linksPersistInfo1.AddRange(links1);
      this.bar1.OptionsBar.AllowQuickCustomization = false;
      this.bar1.OptionsBar.DrawDragBorder = false;
      this.bar1.OptionsBar.MultiLine = true;
      this.bar1.OptionsBar.UseWholeRow = true;
      componentResourceManager.ApplyResources((object) this.bar1, "bar1");
      componentResourceManager.ApplyResources((object) this.barSubItem1, "barSubItem1");
      this.barSubItem1.Id = 0;
      LinksInfo linksPersistInfo2 = this.barSubItem1.LinksPersistInfo;
      LinkPersistInfo[] links2 = new LinkPersistInfo[3];
      int index93 = 0;
      LinkPersistInfo linkPersistInfo2 = new LinkPersistInfo((BarItem) this.barBtnDesignDetailListingReport);
      links2[index93] = linkPersistInfo2;
      int index94 = 1;
      LinkPersistInfo linkPersistInfo3 = new LinkPersistInfo((BarItem) this.barbtnAdvancedFilter, true);
      links2[index94] = linkPersistInfo3;
      int index95 = 2;
      LinkPersistInfo linkPersistInfo4 = new LinkPersistInfo((BarItem) this.barBtnConvertToPlainText, true);
      links2[index95] = linkPersistInfo4;
      linksPersistInfo2.AddRange(links2);
      this.barSubItem1.MergeOrder = 1;
      this.barSubItem1.Name = "barSubItem1";
      componentResourceManager.ApplyResources((object) this.barBtnDesignDetailListingReport, "barBtnDesignDetailListingReport");
      this.barBtnDesignDetailListingReport.Id = 2;
      this.barBtnDesignDetailListingReport.Name = "barBtnDesignDetailListingReport";
      this.barBtnDesignDetailListingReport.ItemClick += new ItemClickEventHandler(this.barBtnDesignDetailListingReport_ItemClick);
      componentResourceManager.ApplyResources((object) this.barbtnAdvancedFilter, "barbtnAdvancedFilter");
      this.barbtnAdvancedFilter.Id = 1;
      this.barbtnAdvancedFilter.Name = "barbtnAdvancedFilter";
      this.barbtnAdvancedFilter.ItemClick += new ItemClickEventHandler(this.barbtnAdvancedFilter_ItemClick);
      componentResourceManager.ApplyResources((object) this.barBtnConvertToPlainText, "barBtnConvertToPlainText");
      this.barBtnConvertToPlainText.Id = 3;
      this.barBtnConvertToPlainText.Name = "barBtnConvertToPlainText";
      this.barBtnConvertToPlainText.ItemClick += new ItemClickEventHandler(this.barBtnConvertToPlainText_ItemClick);
      componentResourceManager.ApplyResources((object) this.barDockControlTop, "barDockControlTop");
      this.barDockControlTop.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlTop.Appearance.GradientMode");
      this.barDockControlTop.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlTop.Appearance.Image");
      this.barDockControlTop.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlBottom, "barDockControlBottom");
      this.barDockControlBottom.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlBottom.Appearance.GradientMode");
      this.barDockControlBottom.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlBottom.Appearance.Image");
      this.barDockControlBottom.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlLeft, "barDockControlLeft");
      this.barDockControlLeft.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlLeft.Appearance.GradientMode");
      this.barDockControlLeft.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlLeft.Appearance.Image");
      this.barDockControlLeft.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlRight, "barDockControlRight");
      this.barDockControlRight.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlRight.Appearance.GradientMode");
      this.barDockControlRight.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlRight.Appearance.Image");
      this.barDockControlRight.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.panelHeader1, "panelHeader1");
      this.panelHeader1.HelpTopicId = "Stock_Assembly_Order.htm";
      this.panelHeader1.Name = "panelHeader1";
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.CancelButton = (IButtonControl) this.sbtnClose;
      this.Controls.Add((Control) this.panelControl2);
      this.Controls.Add((Control) this.panelCenter);
      this.Controls.Add((Control) this.pnCriteriaBasic);
      this.Controls.Add((Control) this.panelCriteria);
      this.Controls.Add((Control) this.panelHeader1);
      this.Controls.Add((Control) this.barDockControlLeft);
      this.Controls.Add((Control) this.barDockControlRight);
      this.Controls.Add((Control) this.barDockControlBottom);
      this.Controls.Add((Control) this.barDockControlTop);
      this.KeyPreview = true;
      this.Name = "FormStockAssemblyOrderPrintDetailListing";
      this.Closing += new CancelEventHandler(this.FormStockAssemblyPrintDetailListing_Closing);
      this.Load += new EventHandler(this.FormSAOPrintDetailListing_Load);
      this.KeyDown += new KeyEventHandler(this.FormStockAssemblyPrintDetailListing_KeyDown);
      this.panelCriteria.EndInit();
      this.panelCriteria.ResumeLayout(false);
      this.gbFilter.EndInit();
      this.gbFilter.ResumeLayout(false);
      this.xtraTabControl1.EndInit();
      this.xtraTabControl1.ResumeLayout(false);
      this.xtraTabPage1.ResumeLayout(false);
      this.xtraTabPage1.PerformLayout();
      this.cbCancelledOption.Properties.EndInit();
      this.xtraTabPage2.ResumeLayout(false);
      this.xtraTabPage2.PerformLayout();
      this.gbReport.EndInit();
      this.gbReport.ResumeLayout(false);
      this.gbReport.PerformLayout();
      this.chkEditShowCriteria.Properties.EndInit();
      this.cbEditSortBy.Properties.EndInit();
      this.cbEditGroupBy.Properties.EndInit();
      this.pnCriteriaBasic.EndInit();
      this.pnCriteriaBasic.ResumeLayout(false);
      this.groupControl1.EndInit();
      this.groupControl1.ResumeLayout(false);
      this.groupControl1.PerformLayout();
      this.chkEditShowCriteria2.Properties.EndInit();
      this.cbEditSortBy2.Properties.EndInit();
      this.cbEditGroupBy2.Properties.EndInit();
      this.gbBasic.EndInit();
      this.gbBasic.ResumeLayout(false);
      this.gbBasic.PerformLayout();
      this.panelCenter.EndInit();
      this.panelCenter.ResumeLayout(false);
      this.panelControl2.EndInit();
      this.panelControl2.ResumeLayout(false);
      this.xtraTabControl2.EndInit();
      this.xtraTabControl2.ResumeLayout(false);
      this.xtraTabPage3.ResumeLayout(false);
      this.gridControl1.EndInit();
      this.gvMaster.EndInit();
      this.repositoryItemCheckEdit1.EndInit();
      this.repositoryItemTextEdit_Cancelled.EndInit();
      this.xtraTabPage4.ResumeLayout(false);
      this.memoEdit_Criteria.Properties.EndInit();
      this.barManager1.EndInit();
      this.ResumeLayout(false);
    }

    public class FormEventArgs
    {
      private FormStockWorkOrderPrintDetailListing myForm;

      public StockWorkOrderReportCommand Command
      {
        get
        {
          return this.myForm.myCommand;
        }
      }

      public PanelControl PanelCriteria
      {
        get
        {
          return this.myForm.panelCriteria;
        }
      }

      public PanelControl PanelButtons
      {
        get
        {
          return this.myForm.panelCenter;
        }
      }

      public XtraTabControl TabControl
      {
        get
        {
          return this.myForm.xtraTabControl2;
        }
      }

      public GridControl GridControl
      {
        get
        {
          return this.myForm.gridControl1;
        }
      }

      public FormStockWorkOrderPrintDetailListing Form
      {
        get
        {
          return this.myForm;
        }
      }

      public DBSetting DBSetting
      {
        get
        {
          return this.myForm.myDBSetting;
        }
      }

      public FormEventArgs(FormStockWorkOrderPrintDetailListing form)
      {
        this.myForm = form;
      }
    }

    public class FormInitializeEventArgs : FormStockWorkOrderPrintDetailListing.FormEventArgs
    {
      public FormInitializeEventArgs(FormStockWorkOrderPrintDetailListing form)
        : base(form)
      {
      }
    }

    public class FormInquiryEventArgs : FormStockWorkOrderPrintDetailListing.FormEventArgs
    {
      private DataTable myResultTable;

      public DataTable ResultTable
      {
        get
        {
          return this.myResultTable;
        }
      }

      public FormInquiryEventArgs(FormStockWorkOrderPrintDetailListing form, DataTable resultTable)
        : base(form)
      {
        this.myResultTable = resultTable;
      }
    }
  }
}
