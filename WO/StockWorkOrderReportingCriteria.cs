﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderReportingCriteria
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Localization;
using System;

namespace RPASystem.WorkOrder
{
  [Serializable]
  public class StockWorkOrderReportingCriteria : StockReportingCriteria
  {
    public StockWorkOrderReportingCriteria()
    {
      this.myDateFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDER", "DocDate", Localizer.GetString((Enum) StockAssemblyOrderStringId.DocDate, new object[0]), FilterControlType.Date);
      this.myDocumentNoFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDER", "DocNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.DocNo, new object[0]), FilterControlType.StockAssemblyOrder);
    }
  }
}
