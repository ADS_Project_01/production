﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderReportTypeHandler
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Manufacturing;
using BCE.Data;

namespace RPASystem.WorkOrder
{
  public class StockWorkOrderReportTypeHandler : ManufacturingDocumentReportTypeHandler
  {
    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      return StockWorkOrderCommand.Create(dbSetting).GetReportDesignerDataSource();
    }
  }
}
