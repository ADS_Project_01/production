﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderDetail
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Misc;
using System.Data;

namespace RPASystem.WorkOrder
{
  public class StockWorkOrderDetail
  {
    private DataRow myRow;
    private DBSetting myDBSetting;
    private DecimalSetting myDecimalSetting;

    public DataRow Row
    {
      get
      {
        return this.myRow;
      }
    }

    public int Seq
    {
      get
      {
        return Convert.ToInt32(this.myRow["Seq"]);
      }
    }

    public DBString Numbering
    {
      get
      {
        return Convert.ToDBString(this.myRow["Numbering"]);
      }
      set
      {
        this.myRow["Numbering"] = Convert.ToDBObject(value);
      }
    }

    public DBString ItemCode
    {
      get
      {
        return Convert.ToDBString(this.myRow["ItemCode"]);
      }
      set
      {
        this.myRow["ItemCode"] = Convert.ToDBObject(value);
      }
    }

    public DBString Location
    {
      get
      {
        return Convert.ToDBString(this.myRow["Location"]);
      }
      set
      {
        this.myRow["Location"] = Convert.ToDBObject(value);
      }
    }

    public DBString BatchNo
    {
      get
      {
        return Convert.ToDBString(this.myRow["BatchNo"]);
      }
      set
      {
        this.myRow["BatchNo"] = Convert.ToDBObject(value);
      }
    }

    public DBString Description
    {
      get
      {
        return Convert.ToDBString(this.myRow["Description"]);
      }
      set
      {
        this.myRow["Description"] = Convert.ToDBObject(value);
      }
    }

    public DBString FurtherDescription
    {
      get
      {
        return Convert.ToDBString(this.myRow["FurtherDescription"]);
      }
      set
      {
        if (value.HasValue)
          this.myRow["FurtherDescription"] = (object) Rtf.ToArialRichText((string) value);
        else
          this.myRow["FurtherDescription"] = Convert.ToDBObject(value);
      }
    }

    public DBString ProjNo
    {
      get
      {
        return Convert.ToDBString(this.myRow["ProjNo"]);
      }
      set
      {
        this.myRow["ProjNo"] = Convert.ToDBObject(value);
      }
    }

    public DBString DeptNo
    {
      get
      {
        return Convert.ToDBString(this.myRow["DeptNo"]);
      }
      set
      {
        this.myRow["DeptNo"] = Convert.ToDBObject(value);
      }
    }

    public DBDecimal Qty
    {
      get
      {
        return Convert.ToDBDecimal(this.myRow["Qty"]);
      }
      set
      {
        this.myRow["Qty"] = this.myDecimalSetting.RoundToQuantityDBObject(value);
      }
    }

    public DBDecimal Rate
    {
      get
      {
        return Convert.ToDBDecimal(this.myRow["Rate"]);
      }
      set
      {
        this.myRow["Rate"] = this.myDecimalSetting.RoundToQuantityDBObject(value);
      }
    }

    public DBDecimal OverHeadCost
    {
      get
      {
        return Convert.ToDBDecimal(this.myRow["OverHeadCost"]);
      }
      set
      {
        this.myRow["OverHeadCost"] = this.myDecimalSetting.RoundToCostDBObject(value);
      }
    }

    public long DtlKey
    {
      get
      {
        return (long) Convert.ToDBInt64(this.myRow["DtlKey"]);
      }
    }

    public long ParentDtlKey
    {
      get
      {
        return (long) Convert.ToDBInt64(this.myRow["ParentDtlKey"]);
      }
      set
      {
        this.myRow["ParentDtlKey"] = (object) value;
      }
    }

    public DBDecimal ItemCost
    {
      get
      {
        return Convert.ToDBDecimal(this.myRow["ItemCost"]);
      }
      set
      {
        this.myRow["ItemCost"] = this.myDecimalSetting.RoundToCostDBObject(value);
      }
    }

    public DBDecimal SubTotalCost
    {
      get
      {
        return Convert.ToDBDecimal(this.myRow["SubTotalCost"]);
      }
      set
      {
        this.myRow["SubTotalCost"] = this.myDecimalSetting.RoundToCostDBObject(value);
      }
    }

    public bool PrintOut
    {
      get
      {
        return Convert.TextToBoolean(this.myRow["PrintOut"]);
      }
      set
      {
        this.myRow["PrintOut"] = (object) Convert.BooleanToText(value);
      }
    }

    internal StockWorkOrderDetail(DBSetting dbSetting, DataRow row)
    {
      this.myRow = row;
      this.myDBSetting = dbSetting;
      this.myDecimalSetting = DecimalSetting.GetOrCreate(this.myDBSetting);
    }
  }
}
