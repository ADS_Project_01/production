﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderReportCommand
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount;
using BCE.AutoCount.Report;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using BCE.Localization;
using System;
using System.Data;
using System.Data.SqlClient;

namespace RPASystem.WorkOrder
{
  public class StockWorkOrderReportCommand
  {
    private BasicReportOption myReportOption;
    protected DBSetting myDBSetting;
    protected const string MasterTableName = "Master";
    protected const string DetailTableName = "Detail";
    public const string DetailListingReportStyle = "Stock Assembly Order Detail Listing";

    public BasicReportOption ReportOption
    {
      get
      {
        return this.myReportOption;
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    public static StockWorkOrderReportCommand Create(DBSetting dbSetting, BasicReportOption reportOption)
    {
      StockWorkOrderReportCommand orderReportCommand = (StockWorkOrderReportCommand) null;
      if (dbSetting.ServerType == DBServerType.SQL2000)
        orderReportCommand = (StockWorkOrderReportCommand) new StockWorkOrderReportCommandSQL();
      else
        dbSetting.ThrowServerTypeNotSupportedException();
      orderReportCommand.myDBSetting = dbSetting;
      orderReportCommand.myReportOption = reportOption;
      return orderReportCommand;
    }

    protected string GetWhereSQL(StockWorkOrderDetailReportingCriteria reportingCriteria, SqlCommand cmd)
    {
      string str1 = "";
      SearchCriteria searchCriteria = new SearchCriteria();
      BCE.AutoCount.SearchFilter.Filter dateFilter = reportingCriteria.DateFilter;
      searchCriteria.AddFilter(dateFilter);
      BCE.AutoCount.SearchFilter.Filter documentFilter = reportingCriteria.DocumentFilter;
      searchCriteria.AddFilter(documentFilter);
      BCE.AutoCount.SearchFilter.Filter itemCodeFilter = reportingCriteria.ItemCodeFilter;
      searchCriteria.AddFilter(itemCodeFilter);
      BCE.AutoCount.SearchFilter.Filter itemGroupFilter = reportingCriteria.ItemGroupFilter;
      searchCriteria.AddFilter(itemGroupFilter);
      BCE.AutoCount.SearchFilter.Filter itemTypeFilter = reportingCriteria.ItemTypeFilter;
      searchCriteria.AddFilter(itemTypeFilter);
      BCE.AutoCount.SearchFilter.Filter locationFilter = reportingCriteria.LocationFilter;
      searchCriteria.AddFilter(locationFilter);
      BCE.AutoCount.SearchFilter.Filter projecNoFilter = reportingCriteria.ProjecNoFilter;
      searchCriteria.AddFilter(projecNoFilter);
      BCE.AutoCount.SearchFilter.Filter deptNoFilter = reportingCriteria.DeptNoFilter;
      searchCriteria.AddFilter(deptNoFilter);
      int num = 1;
      searchCriteria.MatchAll = num != 0;
      SqlCommand sqlCommand = cmd;
      string str2 = searchCriteria.BuildSQL((IDbCommand) sqlCommand);
      if (str2.Length > 0)
        str1 = str2;
      if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.UnCancelled)
        str1 = str1.Length <= 0 ? " ASMORDER.Cancelled = 'F' " : str1 + " And ASMORDER.Cancelled = 'F' ";
      else if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
        str1 = str1.Length <= 0 ? " ASMORDER.Cancelled = 'T' " : str1 + " And ASMORDER.Cancelled = 'T' ";
      return str1;
    }

    public void PrintDetailListingReport(string dtlKeys, StockWorkOrderDetailReportingCriteria criteria)
    {
      string criteriaText = "";
      foreach (string str in criteria.ReadableTextArray)
        criteriaText = criteriaText + str + "\n";
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockWorkOrderString.StockWorkOrderDetailListing, new object[0]), "MF_ASMORDER_DTLLIST_REPORT_PRINT", "MF_ASMORDER_DTLLIST_REPORT_EXPORT", criteriaText);
      ReportTool.PrintReport("Stock Assembly Order Detail Listing", this.GetDetailListingReportDataSource(dtlKeys, criteria), this.myDBSetting, false, this.myReportOption, reportInfo);
    }

    public void PrintDetailListingReport(string dtlKeys, StockWorkOrderDetailReportingCriteria criteria, bool defaultValue)
    {
      string criteriaText = "";
      foreach (string str in criteria.ReadableTextArray)
        criteriaText = criteriaText + str + "\n";
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockWorkOrderString.StockWorkOrderDetailListing, new object[0]), "MF_ASMORDER_DTLLIST_REPORT_PRINT", "MF_ASMORDER_DTLLIST_REPORT_EXPORT", criteriaText);
      ReportTool.PrintReport("Stock Assembly Order Detail Listing", this.GetDetailListingReportDataSource(dtlKeys, criteria), this.myDBSetting, defaultValue, this.myReportOption, reportInfo);
    }

    public void PreviewDetailListingReport(string dtlKeys, StockWorkOrderDetailReportingCriteria criteria)
    {
      string criteriaText = "";
      foreach (string str in criteria.ReadableTextArray)
        criteriaText = criteriaText + str + "\n";
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockWorkOrderString.StockWorkOrderDetailListing, new object[0]), "MF_ASMORDER_DTLLIST_REPORT_PRINT", "MF_ASMORDER_DTLLIST_REPORT_EXPORT", criteriaText);
      ReportTool.PreviewReport("Stock Assembly Order Detail Listing", this.GetDetailListingReportDataSource(dtlKeys, criteria), this.myDBSetting, true, false, this.myReportOption, reportInfo);
    }

    public void PreviewDetailListingReport(string dtlKeys, StockWorkOrderDetailReportingCriteria criteria, bool defaultValue)
    {
      string criteriaText = "";
      foreach (string str in criteria.ReadableTextArray)
        criteriaText = criteriaText + str + "\n";
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockWorkOrderString.StockWorkOrderDetailListing, new object[0]), "MF_ASMORDER_DTLLIST_REPORT_PRINT", "MF_ASMORDER_DTLLIST_REPORT_EXPORT", criteriaText);
      ReportTool.PreviewReport("Stock Assembly Order Detail Listing", this.GetDetailListingReportDataSource(dtlKeys, criteria), this.myDBSetting, defaultValue, false, this.myReportOption, reportInfo);
    }

    private DocumentReportDataSet PreparingDetailListingReportDataSet(DataSet dsReportData, StockWorkOrderDetailReportingCriteria criteria, string dataName)
    {
      DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, dataName, "Stock Assembly Master");
      documentReportDataSet.Tables.Add(dsReportData.Tables[0].Copy());
      DataTable table = new DataTable("Report Option");
      DataColumn column1 = new DataColumn("Criteria", typeof (string));
      table.Columns.Add(column1);
      DataColumn column2 = new DataColumn("ShowCriteria", typeof (string));
      table.Columns.Add(column2);
      DataColumn column3 = new DataColumn("GroupBy", typeof (string));
      table.Columns.Add(column3);
      DataColumn column4 = new DataColumn("SortBy", typeof (string));
      table.Columns.Add(column4);
      if (criteria != null)
      {
        DataRow row = table.NewRow();
        string str1 = "";
        foreach (string str2 in criteria.ReadableTextArray)
          str1 = str1 + str2 + "\n";
        row["Criteria"] = (object) str1;
        row["ShowCriteria"] = criteria.IsShowCriteria ? (object) "Yes" : (object) "No";
        row["GroupBy"] = (object) criteria.GroupBy;
        row["SortBy"] = (object) criteria.SortBy;
        table.Rows.Add(row);
      }
      else
      {
        DataRow row = table.NewRow();
        row["Criteria"] = (object) "Default criteria";
        row["ShowCriteria"] = (object) "Yes";
        row["GroupBy"] = (object) "Item Code";
        row["SortBy"] = (object) "Date";
        table.Rows.Add(row);
      }
      documentReportDataSet.Tables.Add(table);
      documentReportDataSet.AddDefaultTables();
      return documentReportDataSet;
    }

    public object GetDetailListingReportDataSource(string dtlKeys, StockWorkOrderDetailReportingCriteria criteria)
    {
      return (object) this.PreparingDetailListingReportDataSet(this.LoadDetailListingReportData(dtlKeys, criteria), criteria, "Stock Assembly Order Detail Listing");
    }

    public object GetDetailListingReportDesignerDataSource()
    {
      return (object) this.PreparingDetailListingReportDataSet(this.LoadDetailListingReportDesignerData(), (StockWorkOrderDetailReportingCriteria) null, "Stock Assembly Order Detail Listing");
    }

    protected virtual DataSet LoadDetailListingReportData(string dtlKeys, StockWorkOrderDetailReportingCriteria criteria)
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadDetailListingReportDesignerData()
    {
      return (DataSet) null;
    }

    public virtual void DetailListingBasicSearch(StockWorkOrderDetailReportingCriteria criteria, string columnName, DataSet newDS, string chkEditColumnName)
    {
    }

    public virtual void DetailListingAdvanceSearch(AdvancedStockWorkOrderCriteria criteria, DataSet newDS, string checkEditColumnName)
    {
    }
  }
}
