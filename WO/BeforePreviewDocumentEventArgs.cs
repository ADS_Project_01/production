﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.BeforePreviewDocumentEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Report;
using BCE.Data;

namespace RPASystem.WorkOrder
{
  public class BeforePreviewDocumentEventArgs
  {
    private bool myAllowPrint = true;
    private bool myAllowPreview = true;
    private bool myAllowExport = true;
    private EmailAndFaxInfo myEmailAndFaxInfo;
    private long myDocKey;
    private DBSetting myDBSetting;

    public EmailAndFaxInfo EmailAndFaxInfo
    {
      get
      {
        return this.myEmailAndFaxInfo;
      }
    }

    public long DocKey
    {
      get
      {
        return this.myDocKey;
      }
    }

    public bool AllowPrint
    {
      get
      {
        return this.myAllowPrint;
      }
      set
      {
        this.myAllowPrint = value;
      }
    }

    public bool AllowPreview
    {
      get
      {
        return this.myAllowPreview;
      }
      set
      {
        this.myAllowPreview = value;
      }
    }

    public bool AllowExport
    {
      get
      {
        return this.myAllowExport;
      }
      set
      {
        this.myAllowExport = value;
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    public BeforePreviewDocumentEventArgs(EmailAndFaxInfo emailAndFaxInfo, long docKey, DBSetting dbSetting)
    {
      this.myEmailAndFaxInfo = emailAndFaxInfo;
      this.myDocKey = docKey;
      this.myDBSetting = dbSetting;
    }
  }
}
