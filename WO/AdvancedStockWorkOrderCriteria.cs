﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.AdvancedStockAssemblyOrderCriteria
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Localization;
using System;

namespace RPASystem.WorkOrder
{
  [Serializable]
  public class AdvancedStockWorkOrderCriteria : SearchCriteria
  {
    public AdvancedStockWorkOrderCriteria(DBSetting dbSetting)
    {
      this.Add((SearchElement) new SearchDecorator("", "", Localizer.GetString((Enum) StockAssemblyOrderStringId.MasterSearchFields, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "DocNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.AssemblyOrderNo, new object[0]), FilterControlType.StockAssemblyOrder));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "DocDate", Localizer.GetString((Enum) StockAssemblyOrderStringId.AssemblyOrderDate, new object[0]), FilterControlType.Date));
      this.Add((SearchElement) new TextSearch("A", "Description", Localizer.GetString((Enum) StockAssemblyOrderStringId.Description, new object[0])));
      this.Add((SearchElement) new NumberSearch("A", "Total", Localizer.GetString((Enum) StockAssemblyOrderStringId.Total, new object[0])));
      this.Add((SearchElement) new MemoSearch("A", "Note", Localizer.GetString((Enum) StockAssemblyOrderStringId.Note, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark1", Localizer.GetString((Enum) StockAssemblyOrderStringId.Remark1, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark2", Localizer.GetString((Enum) StockAssemblyOrderStringId.Remark2, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark3", Localizer.GetString((Enum) StockAssemblyOrderStringId.Remark3, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark4", Localizer.GetString((Enum) StockAssemblyOrderStringId.Remark4, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "RefDocNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.RefDocNo, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "ExpectedCompletedDate", Localizer.GetString((Enum) StockAssemblyOrderStringId.ExpectedCompletionDate, new object[0])));
      this.Add((SearchElement) new TextSearch("B", "UOM", Localizer.GetString((Enum) StockAssemblyOrderStringId.UOM, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "CreatedUserID", Localizer.GetString((Enum) StockAssemblyOrderStringId.CreatedUserID, new object[0]), FilterControlType.User));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "LastModifiedUserID", Localizer.GetString((Enum) StockAssemblyOrderStringId.LastModifiedUserID, new object[0]), FilterControlType.User));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "CreatedTimeStamp", Localizer.GetString((Enum) StockAssemblyOrderStringId.CreatedTimestamp, new object[0]), FilterControlType.DateTime));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "LastModified", Localizer.GetString((Enum) StockAssemblyOrderStringId.LastModified, new object[0]), FilterControlType.DateTime));
      this.Add((SearchElement) new BooleanSearch("A", "Cancelled", Localizer.GetString((Enum) StockAssemblyOrderStringId.CancelledDocument, new object[0])));
      new UDFUtil(dbSetting).AddUDFIntoAdvancedSearch((SearchCriteria) this, "ASMORDER", "A");
      this.Add((SearchElement) new SearchDecorator("", "", Localizer.GetString((Enum) StockAssemblyOrderStringId.DetailSearchField, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "ItemCode", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemCode, new object[0]), FilterControlType.Item));
      this.Add((SearchElement) new TextSearch("B", "Description", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemDescription, new object[0])));
      this.Add((SearchElement) new MemoSearch("B", "FurtherDescription", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemFurtherDescription, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "Location", Localizer.GetString((Enum) StockAssemblyOrderStringId.Location, new object[0]), FilterControlType.Location));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "ProjNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.Project, new object[0]), FilterControlType.Project));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "DeptNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.Department, new object[0]), FilterControlType.Department));
      this.Add((SearchElement) new TextSearch("B", "BatchNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.BatchNo, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "Qty", Localizer.GetString((Enum) StockAssemblyOrderStringId.Quantity, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "ItemCost", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemCost, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "SubTotal", Localizer.GetString((Enum) StockAssemblyOrderStringId.SubTotal, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "OverHeadCost", Localizer.GetString((Enum) StockAssemblyOrderStringId.OverHeadCost, new object[0])));
      this.Add((SearchElement) new TextSearch("B", "Numbering", Localizer.GetString((Enum) StockAssemblyOrderStringId.Numbering, new object[0])));
      new UDFUtil(dbSetting).AddUDFIntoAdvancedSearch((SearchCriteria) this, "ASMORDERDTL", "B");
    }
  }
}
