﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMString
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Localization;

namespace Production.StockReceiveRM
{
  [LocalizableString]
  public enum StockReceiveRMString
  {
    [DefaultString("Post from Stock Return Raw Material: {0}")] PostFromStockReceiveRM,
    [DefaultString("{0} opened Stock Return Raw Material window.")] OpenedStockReceiveRMWindow,
    [DefaultString("Stock Return Raw Material {0}")] StockReceiveRM,
    [DefaultString("{0} viewed Stock Return Raw Material {1} in edit mode.")] ViewStockReceiveRMInEditMode,
    [DefaultString("{0} viewed Stock Return Raw Material {1}.")] ViewStockReceiveRM,
    [DefaultString("{0} cancels Stock Return Raw Material {1}.")] CancelStockReceiveRM,
    [DefaultString("{0} un-cancels Stock Return Raw Material {1}.")] UncancelStockReceiveRM,
    [DefaultString("{0} edited Stock Return Raw Material {1}.")] EditedStockReceiveRM,
    [DefaultString("{0} opened Print Stock Return Raw Material Detail Listing window.")] OpenedPrintStockReceiveRMDetailListingWindow,
    [DefaultString("{0} inquired on Print Stock Return Raw Material Detail Listing.")] InquiredPrintStockReceiveRMDetailListing,
    [DefaultString("RPA Stock Return Raw Material Detail Listing")] StockReceiveRMDetailListing,
    [DefaultString("{0} opened Print Stock Return Raw Material Listing window.")] OpenedPrintStockReceiveRMListingWindow,
    [DefaultString("{0} inquired on Print Stock Return Raw Material Listing.")] InquiredPrintStockReceiveRMListing,
    [DefaultString("Batch Stock Return Raw Material")] BatchPrintStockReceiveRM,
    [DefaultString("RPA Stock Return Raw Material FG Listing")] PrintStockReceiveRMListing,
    [DefaultString("{0} created new Stock Return Raw Material {1}.")] CreateNewStockReceiveRM,
    [DefaultString("{0} updated Stock Return Raw Material {1}.")] UpdatedStockReceiveRM,
    [DefaultString("{0} deleted Stock Return Raw Material {1}.")] DeletedStockReceiveRM,
    [DefaultString("Batch Print Stock Return Raw Material / Print Stock Return Raw Material Listing")] BatchPrintStockReceiveRMOrStockReceiveRMListing,
    [DefaultString("Print Stock Return Raw Material Detail Listing")] PrintStockReceiveRMDetailListing,
  }
}
