﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMRecalculateDetailEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;
using System.Data;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMRecalculateDetailEventArgs
  {
    private DataRow myDetailRow;
    private DBSetting myDBSetting;

    public StockReceiveRMDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockReceiveRMDetailRecord(this.myDBSetting, this.myDetailRow);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    internal StockReceiveRMRecalculateDetailEventArgs(DBSetting dbSetting, DataRow detailRow)
    {
      this.myDetailRow = detailRow;
      this.myDBSetting = dbSetting;
    }
  }
}
