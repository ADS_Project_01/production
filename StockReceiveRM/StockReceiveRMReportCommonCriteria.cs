﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMReportCommonCriteria
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Localization;
using System;
using System.Collections;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMReportCommonCriteria : ReportCriteria
  {
    protected BCE.AutoCount.SearchFilter.Filter myDateFilter = new BCE.AutoCount.SearchFilter.Filter("RPA_RCVRM", "DocDate", Localizer.GetString((Enum) StockStringId.DocDate, new object[0]), FilterControlType.Date);
    protected BCE.AutoCount.SearchFilter.Filter myDocumentNoFilter = new BCE.AutoCount.SearchFilter.Filter("RPA_RCVRM", "DocNo", Localizer.GetString((Enum) StockStringId.DocNo, new object[0]), FilterControlType.StockReceive);
    protected CancelledDocumentOption myIsPrintCancelled = CancelledDocumentOption.UnCancelled;
    protected int myReportTypeOption;

    public BCE.AutoCount.SearchFilter.Filter DateFilter
    {
      get
      {
        return this.myDateFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter DocumentFilter
    {
      get
      {
        return this.myDocumentNoFilter;
      }
    }

    public CancelledDocumentOption IsPrintCancelled
    {
      get
      {
        return this.myIsPrintCancelled;
      }
      set
      {
        this.myIsPrintCancelled = value;
      }
    }

    public int ReportTypeOption
    {
      get
      {
        return this.myReportTypeOption;
      }
      set
      {
        this.myReportTypeOption = value;
      }
    }

    protected override ArrayList GetFilterOptionsText()
    {
      ArrayList filterOptionsText = base.GetFilterOptionsText();
      string str1 = this.myDateFilter.BuildReadableText(false);
      if (str1.Length > 0)
      {
        ArrayList arrayList = filterOptionsText;
        // ISSUE: variable of a boxed type
        StockStringId local =StockStringId.ReportDate;
        object[] objArray = new object[1];
        int index = 0;
        string str2 = str1;
        objArray[index] = (object) str2;
        string @string = Localizer.GetString((Enum) local, objArray);
        arrayList.Add((object) @string);
      }
      string str3 = this.myDocumentNoFilter.BuildReadableText(false);
      if (str3.Length > 0)
        filterOptionsText.Add((object) str3);
      return filterOptionsText;
    }
  }
}
