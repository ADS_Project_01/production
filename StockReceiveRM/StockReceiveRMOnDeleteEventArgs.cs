﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMOnDeleteEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMOnDeleteEventArgs : StockReceiveRMEventArgs
  {
    private DBSetting myDBSetting;

    public new DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    internal StockReceiveRMOnDeleteEventArgs(StockReceiveRM doc, DBSetting dbSetting)
      : base(doc)
    {
      this.myDBSetting = dbSetting;
    }
  }
}
