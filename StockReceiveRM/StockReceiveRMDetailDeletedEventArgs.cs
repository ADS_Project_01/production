﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMDetailDeletedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMDetailDeletedEventArgs : StockReceiveRMEventArgs
  {
    internal StockReceiveRMDetailDeletedEventArgs(StockReceiveRM doc)
      : base(doc)
    {
    }
  }
}
