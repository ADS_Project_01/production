﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMMasterColumnChangedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMMasterColumnChangedEventArgs
  {
    private string myColumnName;
    private StockReceiveRM myStock;

    public string ChangedColumnName
    {
      get
      {
        return this.myColumnName;
      }
    }

    public StockReceiveRMRecord MasterRecord
    {
      get
      {
        return new StockReceiveRMRecord(this.myStock);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.myDBSetting;
      }
    }

    internal StockReceiveRMMasterColumnChangedEventArgs(string columnName, StockReceiveRM doc)
    {
      this.myColumnName = columnName;
      this.myStock = doc;
    }
  }
}
