﻿
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;

namespace Production.StockReceiveRM
{
    [Obsolete("Use FormStockReceiveRMEntry.FormInitializeEventArgs instead")]
    public class FormInitializeEventArgs : FormStockReceiveRMEventArgs
    {
        public FormInitializeEventArgs(StockReceiveRM doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
          : base(doc, headerPanel, gridControl, tabControl)
        {
        }
    }
}
