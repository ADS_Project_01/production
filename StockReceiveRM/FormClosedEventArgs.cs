﻿
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;

namespace Production.StockReceiveRM
{
    [Obsolete("Use FormStockReceiveRMEntry.FormClosedEventArgs instead")]
    public class FormClosedEventArgs : FormStockReceiveRMEventArgs
    {
        public FormClosedEventArgs(StockReceiveRM doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
          : base(doc, headerPanel, gridControl, tabControl)
        {
        }
    }
}
