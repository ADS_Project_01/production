﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMColumnStringId
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Localization;

namespace Production.StockReceiveRM
{
  [LocalizableString]
  public enum StockReceiveRMColumnStringId
  {
    [DefaultString("Stock Return Raw Material")] StockReceiveRM,
    [DefaultString("Master Search Fields")] MasterSearchFields,
    [DefaultString("Stock Return Raw Material No.")] StockReceiveRMNo,
    [DefaultString("Stock Return Raw Material Date")] StockReceiveRMDate,
    [DefaultString("Total")] Total,
    [DefaultString("Note")] Note,
    [DefaultString("Document No.")] DocumentNo,
    [DefaultString("Date")] Date,
    [DefaultString("Description")] Description,
    [DefaultString("Ref. Doc. No.")] RefDocNo,
    [DefaultString("Remark 1")] Remark1,
    [DefaultString("Remark 2")] Remark2,
    [DefaultString("Remark 3")] Remark3,
    [DefaultString("Remark 4")] Remark4,
    [DefaultString("Created User ID")] CreatedUserID,
    [DefaultString("Last Modified User ID")] LastModifiedUserID,
    [DefaultString("Created Timestamp")] CreatedTimestamp,
    [DefaultString("Last Modified")] LastModified,
    [DefaultString("Cancelled Document")] CancelledDocument,
  }
}
