﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMBeforeSaveEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMBeforeSaveEventArgs : StockReceiveRMEventArgs
  {
    private string myErrorMessage = "";
    private DBSetting myDBSetting;
    internal bool myAbort;

    public new DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return this.myErrorMessage;
      }
      set
      {
        this.myErrorMessage = value;
      }
    }

    internal StockReceiveRMBeforeSaveEventArgs(StockReceiveRM doc, DBSetting dbSetting)
      : base(doc)
    {
      this.myDBSetting = dbSetting;
    }

    public void AbortSave()
    {
      this.myAbort = true;
    }
  }
}
