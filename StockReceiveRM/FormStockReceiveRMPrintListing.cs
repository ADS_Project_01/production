﻿
using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.CommonAccounting;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Production.StockReceiveRM
{
    [SingleInstanceThreadForm]
    public class FormStockReceiveRMPrintListing : XtraForm
    {
        public static string myColumnName;
        private DBSetting myDBSetting;
        private DataTable myDataTable;
        private StockReceiveRMCommand myCommand;
        private AdvancedStockReceiveRMCriteria myCriteria;
        private bool myInSearch;
        private long[] mySelectedDocKeylist;
        private StockReceiveRMReportingCriteria myReportingCriteria;
        private string[] myDocumentStyleOptions;
        private ScriptObject myScriptObject;
        private MouseDownHelper myMouseDownHelper;
        protected UserAuthentication myUserAuthentication;
        private IContainer components;
        private BarManager barManager1;
        private Bar bar2;
        private BarSubItem barSubItem1;
        private BarButtonItem barBtnDesignDocumentStyleReport;
        private BarButtonItem barBtnDesignListingStyleReport;
        private SimpleButton simpleButtonAdvanceSearch;
        private UCDateSelector ucDateSelector1;
        private Label label3;
        private Label label6;
        private GroupControl groupBox_SearchCriteria;
        private Label label4;
        private Label label7;
        private Label label8;
        private Label label9;
        private ComboBoxEdit cbEditGroupBy;
        private ComboBoxEdit cbEditSortBy;
        private ComboBoxEdit cbEditReportType;
        private XtraTabControl tabControl1;
        private XtraTabPage tabPage1;
        private XtraTabPage tabPage2;
        private PanelControl panel_Center;
        private MemoEdit memoEdit_Criteria;
        private UCSearchResult ucSearchResult1;
        private UCStockReceiveRMSelector ucStockReceiveRMSelector1;
        private ComboBoxEdit cbCancelledOption;
        private CheckEdit chkEditShowCriteria;
        private GroupControl gbReportOption;
        private PanelControl panelCriteria;
        private PanelControl panelControl1;
        private SimpleButton sbtnToggleOptions;
        private SimpleButton sbtnClose;
        private SimpleButton sbtnInquiry;
        private PreviewButton previewButton1;
        private PrintButton printButton1;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private PanelHeader panelHeader1;
        private XtraTabPage xtraTabPage1;
        private Bar bar1;
        private BarSubItem barSubItem2;
        private BarButtonItem barButtonItem1;
        private StockReceiveRMGrid stockReceiveRMGrid1;
        private BarButtonItem barButtonItem2;
        //private StockReceiveRMGrid stockReceiveRMGrid1;

        public DataTable MasterDataTable
        {
            get
            {
                return this.myDataTable;
            }
        }

        public long[] SelectedDocKeys
        {
            get
            {
                return this.mySelectedDocKeylist;
            }
        }

        public string SelectedDocNosInString
        {
            get
            {
                return this.GenerateDocNosToString();
            }
        }

        public string SelectedDocKeysInString
        {
            get
            {
                return StringHelper.ArrayListToCommaString(new ArrayList((ICollection)this.mySelectedDocKeylist));
            }
        }

        public string ColumnName
        {
            get
            {
                return FormStockReceiveRMPrintListing.myColumnName;
            }
        }

        public StockReceiveRMReportingCriteria StockReceiveRMReportingCriteria
        {
            get
            {
                return this.myReportingCriteria;
            }
        }

        public FormStockReceiveRMPrintListing(DBSetting dbSetting)
        {
            FormStockReceiveRMPrintListing issuePrintListing = this;
            string[] strArray = new string[2];
            int index1 = 0;
            string string1 = BCE.Localization.Localizer.GetString(StockStringId.BatchPrintStockReceive, new object[0]);
            strArray[index1] = string1;
            int index2 = 1;
            string string2 = BCE.Localization.Localizer.GetString(StockStringId.PrintStockReceiveListing, new object[0]);
            strArray[index2] = string2;
            issuePrintListing.myDocumentStyleOptions = strArray;
            // ISSUE: explicit constructor call
            //base.\u002Ector();
            this.InitializeComponent();
            this.myDBSetting = dbSetting;
            this.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
            this.myCommand = StockReceiveRMCommand.Create(dbSetting);
            this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockReceiveRMListing");
            this.previewButton1.ReportType = "Stock Return Raw Material Document";
            this.previewButton1.SetDBSetting(this.myDBSetting);
            this.printButton1.ReportType = "Stock Return Raw Material Document";
            this.printButton1.SetDBSetting(this.myDBSetting);
            this.myDataTable = new DataTable();
            this.stockReceiveRMGrid1.Initialize(this.myDBSetting);
            this.stockReceiveRMGrid1.DataSource = (object)this.myDataTable;
            this.LoadCriteria();
            this.ucSearchResult1.Initialize(this.stockReceiveRMGrid1.GridView, "ToBeUpdate");
            this.InitUserControls();
            this.cbEditGroupBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(ListingGroupByOption)));
            this.cbEditSortBy.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(ListingSortByOption)));
            this.cbCancelledOption.Properties.Items.AddRange((object[])BCE.Localization.Localizer.GetEnumStrings(typeof(CancelledDocumentOption)));
            this.cbEditReportType.Properties.Items.AddRange((object[])this.myDocumentStyleOptions);
            this.cbCancelledOption.SelectedIndex = 2;
            this.cbEditReportType.SelectedIndex = 1;
            this.RefreshDesignReport();
            this.myMouseDownHelper = new MouseDownHelper();
            this.myMouseDownHelper.Init(this.stockReceiveRMGrid1.GridView);
            this.stockReceiveRMGrid1.GridView.DoubleClick += new EventHandler(this.GridView_DoubleClick);
            BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Receive.htm#stk028");
            DBSetting dbSetting1 = dbSetting;
            string docType = "";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            StockReceiveRMString local = StockReceiveRMString.OpenedPrintStockReceiveRMListingWindow;
            object[] objArray = new object[1];
            int index3 = 0;
            string loginUserId = this.myUserAuthentication.LoginUserID;
            objArray[index3] = (object)loginUserId;
            string string3 = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            string detail = "";
            Activity.Log(dbSetting1, docType, docKey, eventKey, string3, detail);
        }

        private void SaveCriteria()
        {
            PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting)this.myReportingCriteria, "StockReceiveRMDocumentListingReport2.setting");
        }

        private void LoadCriteria()
        {
            this.myReportingCriteria = (StockReceiveRMReportingCriteria)PersistenceUtil.LoadCriteriaData(this.myDBSetting, "StockReceiveRMDocumentListingReport2.setting");
            if (this.myReportingCriteria == null)
                this.myReportingCriteria = new StockReceiveRMReportingCriteria();
            this.cbEditGroupBy.EditValue = (object)this.myReportingCriteria.GroupBy;
            this.cbEditSortBy.EditValue = (object)this.myReportingCriteria.SortBy;
            this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
            this.cbCancelledOption.SelectedIndex = (int)this.myReportingCriteria.IsPrintCancelled;
            this.chkEditShowCriteria.Checked = this.myReportingCriteria.IsShowCriteria;
        }

        private void InitUserControls()
        {
            this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
            this.ucStockReceiveRMSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
        }

        private void AddNewColumn()
        {
            this.myDataTable.Columns.Add(new DataColumn()
            {
                DataType = typeof(bool),
                AllowDBNull = true,
                Caption = "Check",
                ColumnName = "ToBeUpdate",
                DefaultValue = (object)false
            });
        }

        private void BasicSearch(bool isSearchAll)
        {
            if (!this.myInSearch)
            {
                this.myInSearch = true;
                GridViewUtils.UpdateData(this.stockReceiveRMGrid1.GridView);
                this.stockReceiveRMGrid1.DataSource = (object)null;
                try
                {
                    this.stockReceiveRMGrid1.GridControl.MainView.UpdateCurrentRow();
                    this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
                    this.myCommand.DocumentListingBasicSearch(this.myReportingCriteria, CommonFunction.BuildSQLColumns(isSearchAll, this.stockReceiveRMGrid1.GridView.Columns.View), this.myDataTable, "ToBeUpdate");
                    if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                        this.AddNewColumn();
                    this.stockReceiveRMGrid1.DataSource = (object)this.myDataTable;
                }
                finally
                {
                    this.myInSearch = false;
                }
                FormStockReceiveRMPrintListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockReceiveRMPrintListing.FormInquiryEventArgs(this, this.myDataTable);
                ScriptObject scriptObject = this.myScriptObject;
                string name = "OnFormInquiry";
                System.Type[] types = new System.Type[1];
                int index1 = 0;
                System.Type type = inquiryEventArgs1.GetType();
                types[index1] = type;
                object[] objArray = new object[1];
                int index2 = 0;
                FormStockReceiveRMPrintListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
                objArray[index2] = (object)inquiryEventArgs2;
                scriptObject.RunMethod(name, types, objArray);
            }
        }

        private void AdvancedSearch()
        {
            if (this.myCriteria == null)
                this.myCriteria = new AdvancedStockReceiveRMCriteria(this.myDBSetting);
            this.myCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
            using (FormAdvancedSearch formAdvancedSearch = new FormAdvancedSearch((SearchCriteria)this.myCriteria, this.myDBSetting))
            {
                if (formAdvancedSearch.ShowDialog((IWin32Window)this) == DialogResult.OK)
                {
                    if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                        this.AddNewColumn();
                    this.memoEdit_Criteria.Lines = this.myCriteria.BuildReadableTextArray();
                    this.ucSearchResult1.KeepSearchResult = this.myCriteria.KeepSearchResult;
                    Cursor current = Cursor.Current;
                    Cursor.Current = Cursors.WaitCursor;
                    this.myInSearch = true;
                    try
                    {
                        this.myCommand.AdvanceSearch(this.myCriteria, CommonFunction.BuildSQLColumns(false, this.stockReceiveRMGrid1.GridView.Columns.View), this.myDataTable, "ToBeUpdate");
                    }
                    catch (AppException ex)
                    {
                        AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                    }
                    finally
                    {
                        this.ucSearchResult1.CheckAll();
                        this.myInSearch = false;
                        Cursor.Current = current;
                    }
                    FormStockReceiveRMPrintListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockReceiveRMPrintListing.FormInquiryEventArgs(this, this.myDataTable);
                    ScriptObject scriptObject = this.myScriptObject;
                    string name = "OnFormInquiry";
                    System.Type[] types = new System.Type[1];
                    int index1 = 0;
                    System.Type type = inquiryEventArgs1.GetType();
                    types[index1] = type;
                    object[] objArray = new object[1];
                    int index2 = 0;
                    FormStockReceiveRMPrintListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
                    objArray[index2] = (object)inquiryEventArgs2;
                    scriptObject.RunMethod(name, types, objArray);
                }
            }
        }

        private string GenerateDocNosToString()
        {
            GridViewUtils.UpdateData(this.stockReceiveRMGrid1.GridView);
            DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = True");
            if (dataRowArray.Length == 0)
            {
                AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString(StockReceiveStringId.ShowMessage_StockReceiveNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
                return (string)null;
            }
            else
            {
                string str = "";
                foreach (DataRow dataRow in dataRowArray)
                {
                    if (str.Length != 0)
                        str = str + ", ";
                    str = str + "'" + dataRow["DocNo"].ToString() + "'";
                }
                return str;
            }
        }

        private void simpleButtonAdvanceSearch_Click(object sender, EventArgs e)
        {
            this.AdvancedSearch();
        }

        private void FormStockReceiveRMList_Closing(object sender, CancelEventArgs e)
        {
            this.SaveCriteria();
        }

        private void ReloadAllColumns(object sender, EventArgs e)
        {
            this.BasicSearch(true);
        }

        private void sbtnInquiry_Click(object sender, EventArgs e)
        {
            this.BasicSearch(false);
            if (this.cbEditSortBy.Text != "")
                this.SortByOption();
            this.ucSearchResult1.CheckAll();
            this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
            this.sbtnToggleOptions.Enabled = this.stockReceiveRMGrid1.DataSource != null;
            this.stockReceiveRMGrid1.GridControl.Focus();
            DBSetting dbSetting = this.myDBSetting;
            string docType = "";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            StockReceiveRMString local = StockReceiveRMString.InquiredPrintStockReceiveRMListing;
            object[] objArray = new object[1];
            int index = 0;
            string loginUserId = this.myUserAuthentication.LoginUserID;
            objArray[index] = (object)loginUserId;
            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            string readableText = this.myReportingCriteria.ReadableText;
            Activity.Log(dbSetting, docType, docKey, eventKey, @string, readableText);
        }

        private void cbEditGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.GroupBy = (ListingGroupByOption)this.cbEditGroupBy.SelectedIndex;
        }

        private void cbEditSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
            {
                if (this.myReportingCriteria.SortBy != (ListingSortByOption)this.cbEditSortBy.SelectedIndex)
                    this.myReportingCriteria.SortBy = (ListingSortByOption)this.cbEditSortBy.SelectedIndex;
                if (this.cbEditSortBy.Text != "")
                    this.SortByOption();
            }
        }

        private void cbEditReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.myReportingCriteria.ReportTypeOption = this.cbEditReportType.SelectedIndex;
            FormStockReceiveRMPrintListing issuePrintListing = this;
            // ISSUE: variable of a boxed type
            StockReceiveStringId local = StockReceiveStringId.Code_PrintStockReceive;
            object[] objArray = new object[1];
            int index = 0;
            string text = this.cbEditReportType.Text;
            objArray[index] = (object)text;
            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            issuePrintListing.Text = @string;
            if (this.cbEditReportType.SelectedIndex == 0)
            {
                this.previewButton1.ReportType = "Stock Return Raw Material Document";
                this.previewButton1.SetDBSetting(this.myDBSetting);
                this.printButton1.ReportType = "Stock Return Raw Material Document";
                this.printButton1.SetDBSetting(this.myDBSetting);
            }
            else
            {
                this.previewButton1.ReportType = "RPA Stock Return Raw Material Listing";
                this.previewButton1.SetDBSetting(this.myDBSetting);
                this.printButton1.ReportType = "RPA Stock Return Raw Material Listing";
                this.printButton1.SetDBSetting(this.myDBSetting);
            }
        }

        private void cbCancelledOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.IsPrintCancelled = (CancelledDocumentOption)this.cbCancelledOption.SelectedIndex;
        }

        private void chkEditShowCriteria_CheckedChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria != null)
                this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
        }

        private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GridView gridView = (GridView)sender;
            int[] selectedRows = gridView.GetSelectedRows();
            if (selectedRows != null && selectedRows.Length != 0 && selectedRows.Length != 1)
            {
                for (int index = 0; index < selectedRows.Length; ++index)
                    gridView.GetDataRow(selectedRows[index])["ToBeUpdate"] = (object)true;
            }
        }

        private string GetSelectedDocKeys()
        {
            GridViewUtils.UpdateData(this.stockReceiveRMGrid1.GridView);
            if (this.stockReceiveRMGrid1.GridView.FocusedRowHandle >= 0 && this.myDataTable.Select("ToBeUpdate = True").Length != 0)
            {
                this.mySelectedDocKeylist = DataTableHelper.GetSelectedKeyArray("ToBeUpdate", "DocKey", this.myDataTable);
                return this.SelectedDocKeysInString;
            }
            else
                return "";
        }

        private void sbtnToggleOptions_Click(object sender, EventArgs e)
        {
            this.panelCriteria.Visible = !this.panelCriteria.Visible;
            if (this.panelCriteria.Visible)
                this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_HideOptions, new object[0]);
            else
                this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString(StockReceiveStringId.Code_ShowOptions, new object[0]);
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormStockReceiveRMPrintSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)120)
            {
                if (this.sbtnInquiry.Enabled)
                    this.sbtnInquiry.PerformClick();
            }
            else if (e.KeyCode == (Keys)119)
            {
                if (this.previewButton1.Enabled)
                    this.previewButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)118)
            {
                if (this.printButton1.Enabled)
                    this.printButton1.PerformClick();
            }
            else if (e.KeyCode == (Keys)117 && this.sbtnToggleOptions.Enabled)
                this.sbtnToggleOptions.PerformClick();
        }

        private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            string selectedDocKeys = this.GetSelectedDocKeys();
            if (selectedDocKeys.Length == 0)
            {
                AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString(StockReceiveStringId.ShowMessage_StockReceiveNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            else if (this.cbEditReportType.SelectedIndex == 0)
            {
                if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_DOC_REPORT_PREVIEW", (XtraForm)this))
                {
                    if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
                    {
                        foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
                        {
                            if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RM", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
                            {
                                AccessRight accessRight = this.myUserAuthentication.AccessRight;
                                string cmdID = "RPA_RCVRM_PRINTED_PREVIEW";
                                FormStockReceiveRMPrintListing issuePrintListing = this;
                                // ISSUE: variable of a boxed type
                                StockStringId local = StockStringId.NoPermissionToPreviewPrintedDocument;
                                object[] objArray = new object[1];
                                int index = 0;
                                string str = dataRow["DocNo"].ToString();
                                objArray[index] = (object)str;
                                string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                                if (!accessRight.IsAccessible(cmdID, (XtraForm)issuePrintListing, @string))
                                    return;
                            }
                        }
                    }
                    ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString(StockReceiveRMString.BatchPrintStockReceiveRM, new object[0]), "RPA_RCVRM_DOC_REPORT_PRINT", "RPA_RCVRM_DOC_REPORT_EXPORT", this.myReportingCriteria.ReadableText);
                    reportInfo.UpdatePrintCountTableName = "RPA_RM";
                    reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
                    reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
                    ReportTool.PreviewReport("Stock Return Raw Material Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, false, this.myCommand.ReportOption, reportInfo);
                }
            }
            else if (this.cbEditReportType.SelectedIndex == 1 && this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_LISTING_REPORT_PREVIEW", (XtraForm)this))
            {
                ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString(StockReceiveRMString.PrintStockReceiveRMListing, new object[0]), "RPA_RCVRM_LISTING_REPORT_PRINT", "RPA_RCVRM_LISTING_REPORT_EXPORT", this.myReportingCriteria.ReadableText);
                ReportTool.PreviewReport("RPA Stock Return Raw Material Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.StockReceiveRMReportingCriteria), this.myDBSetting, e.DefaultReport, false, this.myCommand.ReportOption, reportInfo);
            }
        }

        private bool CheckBeforePrint(ReportInfo reportInfo)
        {
            if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
            {
                foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
                {
                    if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RM", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
                    {
                        AccessRight accessRight = this.myUserAuthentication.AccessRight;
                        string cmdID = "RPA_RCVRM_PRINTED_PRINT";
                        FormStockReceiveRMPrintListing issuePrintListing = this;
                        // ISSUE: variable of a boxed type
                        StockStringId local = StockStringId.NoPermissionToPrintPrintedDocument;
                        object[] objArray = new object[1];
                        int index = 0;
                        string str = dataRow["DocNo"].ToString();
                        objArray[index] = (object)str;
                        string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                        if (!accessRight.IsAccessible(cmdID, (XtraForm)issuePrintListing, @string))
                            return false;
                    }
                }
            }
            return true;
        }

        private bool CheckBeforeExport(ReportInfo reportInfo)
        {
            if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
            {
                foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
                {
                    if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RM", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
                    {
                        AccessRight accessRight = this.myUserAuthentication.AccessRight;
                        string cmdID = "RPA_RCVRM_PRINTED_EXPORT";
                        FormStockReceiveRMPrintListing issuePrintListing = this;
                        // ISSUE: variable of a boxed type
                        StockStringId local = StockStringId.NoPermissionToExportPrintedDocument;
                        object[] objArray = new object[1];
                        int index = 0;
                        string str = dataRow["DocNo"].ToString();
                        objArray[index] = (object)str;
                        string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                        if (!accessRight.IsAccessible(cmdID, (XtraForm)issuePrintListing, @string))
                            return false;
                    }
                }
            }
            return true;
        }

        private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
        {
            string selectedDocKeys = this.GetSelectedDocKeys();
            if (selectedDocKeys.Length == 0)
            {
                AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString(StockReceiveStringId.ShowMessage_StockReceiveNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            else if (this.cbEditReportType.SelectedIndex == 0)
            {
                if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_DOC_REPORT_PRINT", (XtraForm)this))
                {
                    if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
                    {
                        foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
                        {
                            if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RM", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
                            {
                                AccessRight accessRight = this.myUserAuthentication.AccessRight;
                                string cmdID = "RPA_RCVRM_PRINTED_PRINT";
                                FormStockReceiveRMPrintListing issuePrintListing = this;
                                // ISSUE: variable of a boxed type
                                StockStringId local = StockStringId.NoPermissionToPrintPrintedDocument;
                                object[] objArray = new object[1];
                                int index = 0;
                                string str = dataRow["DocNo"].ToString();
                                objArray[index] = (object)str;
                                string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                                if (!accessRight.IsAccessible(cmdID, (XtraForm)issuePrintListing, @string))
                                    return;
                            }
                        }
                    }
                    ReportTool.PrintReport("Stock Return Raw Material Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString(StockReceiveRMString.BatchPrintStockReceiveRM, new object[0]), "RPA_RCVRM_DOC_REPORT_PRINT", "RPA_RCVRM_DOC_REPORT_EXPORT", this.myReportingCriteria.ReadableText)
                    {
                        UpdatePrintCountTableName = "RPA_RM"
                    });
                }
            }
            else if (this.cbEditReportType.SelectedIndex == 1 && this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_LISTING_REPORT_PRINT", (XtraForm)this))
            {
                ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString(StockReceiveRMString.PrintStockReceiveRMListing, new object[0]), "RPA_RCVRM_LISTING_REPORT_PRINT", "RPA_RCVRM_LISTING_REPORT_EXPORT", this.myReportingCriteria.ReadableText);
                ReportTool.PrintReport("RPA Stock Return Raw Material Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.StockReceiveRMReportingCriteria), this.myDBSetting, e.DefaultReport, this.myCommand.ReportOption, reportInfo);
            }
        }

        private void barBtnDesignDocumentStyleReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            //string docNo = "";
            long selectedDocKey = 0;
            ReportTool.DesignReport("Stock Return Raw Material Document", this.myCommand.GetReportDataSource(selectedDocKey), this.myDBSetting);
        }

        private void barBtnDesignListingStyleReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            string selectedDocKeys = this.GetSelectedDocKeys();
            if (selectedDocKeys.Length == 0)
            {
                AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString(StockReceiveStringId.ShowMessage_StockReceiveNotSelected, new object[0]));
                this.DialogResult = DialogResult.None;
            }
            ReportTool.DesignReport("RPA Stock Return Raw Material Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.StockReceiveRMReportingCriteria), this.myDBSetting);
        }

        public virtual void RefreshDesignReport()
        {
            bool show = this.myUserAuthentication.AccessRight.IsAccessible("TOOLS_RPT_SHOW");
            this.barBtnDesignDocumentStyleReport.Visibility = XtraBarsUtils.ToBarItemVisibility(show);
            this.barBtnDesignListingStyleReport.Visibility = XtraBarsUtils.ToBarItemVisibility(show);
        }

        private void SortByOption()
        {
            ListingSortByOption listingSortByOption = (ListingSortByOption)this.cbEditSortBy.SelectedIndex;
            string[] strArray1 = new string[2];
            int index1 = 0;
            string str1 = "DocNo";
            strArray1[index1] = str1;
            int index2 = 1;
            string str2 = "DocDate";
            strArray1[index2] = str2;
            string[] strArray2 = strArray1;
            for (int index3 = 0; index3 < strArray2.Length; ++index3)
            this.stockReceiveRMGrid1.GridView.Columns[strArray2[index3]].SortOrder = (ListingSortByOption)index3 != listingSortByOption ? ColumnSortOrder.None : ColumnSortOrder.Ascending;
        }

        private void FormSIPrintSearch_Load(object sender, EventArgs e)
        {
            this.FormInitialize();
        }

        private void FormInitialize()
        {
            FormStockReceiveRMPrintListing.FormInitializeEventArgs initializeEventArgs1 = new FormStockReceiveRMPrintListing.FormInitializeEventArgs(this);
            ScriptObject scriptObject = this.myScriptObject;
            string name = "OnFormInitialize";
            System.Type[] types = new System.Type[1];
            int index1 = 0;
            System.Type type = initializeEventArgs1.GetType();
            types[index1] = type;
            object[] objArray = new object[1];
            int index2 = 0;
            FormStockReceiveRMPrintListing.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
            objArray[index2] = (object)initializeEventArgs2;
            scriptObject.RunMethod(name, types, objArray);
        }

        private void GridView_DoubleClick(object sender, EventArgs e)
        {
            if (this.myMouseDownHelper.IsLeftMouseDown && GridViewUtils.GetGridHitInfo((GridView)sender).InRow && this.myUserAuthentication.AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
                this.GoToDocument();
        }

        private void GoToDocument()
        {
            ColumnView columnView = (ColumnView)this.stockReceiveRMGrid1.GridControl.FocusedView;
            int focusedRowHandle = columnView.FocusedRowHandle;
            DataRow dataRow = columnView.GetDataRow(focusedRowHandle);
            if (dataRow != null)
                DocumentDispatcher.Open(this.myDBSetting, "RV", BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
        }

        private void stockIssueGrid_DrawGroupPanelEvent(object sender, CustomDrawEventArgs e)
        {
            CommonGridViewHelper.DrawGroupPanelString(sender, e, this.myDBSetting);
        }

        protected override void Dispose(bool disposing)
        {
            this.SaveCriteria();
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelCriteria = new DevExpress.XtraEditors.PanelControl();
            this.cbEditReportType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox_SearchCriteria = new DevExpress.XtraEditors.GroupControl();
            this.cbCancelledOption = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucStockReceiveRMSelector1 = new UCStockReceiveRMSelector();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ucDateSelector1 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.simpleButtonAdvanceSearch = new DevExpress.XtraEditors.SimpleButton();
            this.gbReportOption = new DevExpress.XtraEditors.GroupControl();
            this.label7 = new System.Windows.Forms.Label();
            this.cbEditGroupBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbEditSortBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chkEditShowCriteria = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.tabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.stockReceiveRMGrid1 = new Production.StockReceiveRM.StockReceiveRMGrid();
            this.ucSearchResult1 = new BCE.AutoCount.FilterUI.UCSearchResult();
            this.tabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit_Criteria = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel_Center = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.printButton1 = new BCE.AutoCount.Controls.PrintButton();
            this.previewButton1 = new BCE.AutoCount.Controls.PreviewButton();
            this.sbtnToggleOptions = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInquiry = new DevExpress.XtraEditors.SimpleButton();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barBtnDesignDocumentStyleReport = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnDesignListingStyleReport = new DevExpress.XtraBars.BarButtonItem();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            this.bar1 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).BeginInit();
            this.panelCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditReportType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox_SearchCriteria)).BeginInit();
            this.groupBox_SearchCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbReportOption)).BeginInit();
            this.gbReportOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Center)).BeginInit();
            this.panel_Center.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCriteria
            // 
            this.panelCriteria.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCriteria.Controls.Add(this.cbEditReportType);
            this.panelCriteria.Controls.Add(this.label9);
            this.panelCriteria.Controls.Add(this.groupBox_SearchCriteria);
            this.panelCriteria.Controls.Add(this.gbReportOption);
            this.panelCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCriteria.Location = new System.Drawing.Point(0, 66);
            this.panelCriteria.Name = "panelCriteria";
            this.panelCriteria.Size = new System.Drawing.Size(896, 147);
            this.panelCriteria.TabIndex = 2;
            // 
            // cbEditReportType
            // 
            this.cbEditReportType.EditValue = "Print Stock Return Raw Material Listing";
            this.cbEditReportType.Location = new System.Drawing.Point(119, 6);
            this.cbEditReportType.Name = "cbEditReportType";
            this.cbEditReportType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditReportType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditReportType.Size = new System.Drawing.Size(157, 20);
            this.cbEditReportType.TabIndex = 0;
            this.cbEditReportType.SelectedIndexChanged += new System.EventHandler(this.cbEditReportType_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(13, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "Report Type";
            // 
            // groupBox_SearchCriteria
            // 
            this.groupBox_SearchCriteria.Controls.Add(this.cbCancelledOption);
            this.groupBox_SearchCriteria.Controls.Add(this.ucStockReceiveRMSelector1);
            this.groupBox_SearchCriteria.Controls.Add(this.label4);
            this.groupBox_SearchCriteria.Controls.Add(this.label6);
            this.groupBox_SearchCriteria.Controls.Add(this.label3);
            this.groupBox_SearchCriteria.Controls.Add(this.ucDateSelector1);
            this.groupBox_SearchCriteria.Controls.Add(this.simpleButtonAdvanceSearch);
            this.groupBox_SearchCriteria.Location = new System.Drawing.Point(16, 32);
            this.groupBox_SearchCriteria.Name = "groupBox_SearchCriteria";
            this.groupBox_SearchCriteria.Size = new System.Drawing.Size(575, 109);
            this.groupBox_SearchCriteria.TabIndex = 2;
            this.groupBox_SearchCriteria.Text = "Filter Options";
            // 
            // cbCancelledOption
            // 
            this.cbCancelledOption.EditValue = "Show Uncancelled";
            this.cbCancelledOption.Location = new System.Drawing.Point(120, 75);
            this.cbCancelledOption.Name = "cbCancelledOption";
            this.cbCancelledOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCancelledOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbCancelledOption.Size = new System.Drawing.Size(128, 20);
            this.cbCancelledOption.TabIndex = 0;
            this.cbCancelledOption.SelectedIndexChanged += new System.EventHandler(this.cbCancelledOption_SelectedIndexChanged);
            // 
            // ucStockReceiveRMSelector1
            // 
            this.ucStockReceiveRMSelector1.Location = new System.Drawing.Point(117, 47);
            this.ucStockReceiveRMSelector1.Name = "ucStockReceiveRMSelector1";
            this.ucStockReceiveRMSelector1.Size =new System.Drawing.Size(362, 24);
            this.ucStockReceiveRMSelector1.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Document No. : ";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(5, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 18);
            this.label6.TabIndex = 3;
            this.label6.Text = " Document Date :";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cancelled Status :";
            // 
            // ucDateSelector1
            // 
            this.ucDateSelector1.Location = new System.Drawing.Point(120, 24);
            this.ucDateSelector1.Name = "ucDateSelector1";
            this.ucDateSelector1.Size = new System.Drawing.Size(362, 20);
            this.ucDateSelector1.TabIndex = 5;
            // 
            // simpleButtonAdvanceSearch
            // 
            this.simpleButtonAdvanceSearch.Location = new System.Drawing.Point(458, 76);
            this.simpleButtonAdvanceSearch.Name = "simpleButtonAdvanceSearch";
            this.simpleButtonAdvanceSearch.Size = new System.Drawing.Size(112, 27);
            this.simpleButtonAdvanceSearch.TabIndex = 6;
            this.simpleButtonAdvanceSearch.Text = "Advanced Filter...";
            this.simpleButtonAdvanceSearch.Click += new System.EventHandler(this.simpleButtonAdvanceSearch_Click);
            // 
            // gbReportOption
            // 
            this.gbReportOption.Controls.Add(this.label7);
            this.gbReportOption.Controls.Add(this.cbEditGroupBy);
            this.gbReportOption.Controls.Add(this.cbEditSortBy);
            this.gbReportOption.Controls.Add(this.chkEditShowCriteria);
            this.gbReportOption.Controls.Add(this.label8);
            this.gbReportOption.Location = new System.Drawing.Point(597, 32);
            this.gbReportOption.Name = "gbReportOption";
            this.gbReportOption.Size = new System.Drawing.Size(274, 109);
            this.gbReportOption.TabIndex = 3;
            this.gbReportOption.Text = "Report Options";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 23);
            this.label7.TabIndex = 0;
            this.label7.Text = "Group By :";
            // 
            // cbEditGroupBy
            // 
            this.cbEditGroupBy.EditValue = "None";
            this.cbEditGroupBy.Location = new System.Drawing.Point(88, 24);
            this.cbEditGroupBy.Name = "cbEditGroupBy";
            this.cbEditGroupBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditGroupBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditGroupBy.Size = new System.Drawing.Size(153, 20);
            this.cbEditGroupBy.TabIndex = 1;
            this.cbEditGroupBy.SelectedIndexChanged += new System.EventHandler(this.cbEditGroupBy_SelectedIndexChanged);
            // 
            // cbEditSortBy
            // 
            this.cbEditSortBy.EditValue = "Date";
            this.cbEditSortBy.Location = new System.Drawing.Point(88, 50);
            this.cbEditSortBy.Name = "cbEditSortBy";
            this.cbEditSortBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEditSortBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEditSortBy.Size = new System.Drawing.Size(153, 20);
            this.cbEditSortBy.TabIndex = 2;
            this.cbEditSortBy.SelectedIndexChanged += new System.EventHandler(this.cbEditSortBy_SelectedIndexChanged);
            // 
            // chkEditShowCriteria
            // 
            this.chkEditShowCriteria.Location = new System.Drawing.Point(86, 76);
            this.chkEditShowCriteria.Name = "chkEditShowCriteria";
            this.chkEditShowCriteria.Properties.Caption = "Show Criteria in Report";
            this.chkEditShowCriteria.Size = new System.Drawing.Size(164, 19);
            this.chkEditShowCriteria.TabIndex = 3;
            this.chkEditShowCriteria.CheckedChanged += new System.EventHandler(this.chkEditShowCriteria_CheckedChanged);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(12, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 20);
            this.label8.TabIndex = 4;
            this.label8.Text = "Sort By :";
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabPage = this.tabPage1;
            this.tabControl1.Size = new System.Drawing.Size(896, 260);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPage1,
            this.tabPage2});
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.stockReceiveRMGrid1);
            this.tabPage1.Controls.Add(this.ucSearchResult1);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(890, 232);
            this.tabPage1.Text = "Result";
            // 
            // stockReceiveRMGrid1
            // 
            this.stockReceiveRMGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stockReceiveRMGrid1.Location = new System.Drawing.Point(0, 46);
            this.stockReceiveRMGrid1.Name = "stockReceiveRMGrid1";
            this.stockReceiveRMGrid1.Size = new System.Drawing.Size(890, 186);
            this.stockReceiveRMGrid1.TabIndex = 2;
            // 
            // ucSearchResult1
            // 
            this.ucSearchResult1.Appearance.Options.UseBackColor = true;
            this.ucSearchResult1.ClearAllUncheckRecords = false;
            this.ucSearchResult1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucSearchResult1.KeepSearchResult = true;
            this.ucSearchResult1.Location = new System.Drawing.Point(0, 0);
            this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ucSearchResult1.Name = "ucSearchResult1";
            this.ucSearchResult1.Size = new System.Drawing.Size(890, 46);
            this.ucSearchResult1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.memoEdit_Criteria);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(890, 232);
            this.tabPage2.Text = "Criteria";
            // 
            // memoEdit_Criteria
            // 
            this.memoEdit_Criteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit_Criteria.Location = new System.Drawing.Point(0, 0);
            this.memoEdit_Criteria.Name = "memoEdit_Criteria";
            this.memoEdit_Criteria.Properties.ReadOnly = true;
            this.memoEdit_Criteria.Size = new System.Drawing.Size(890, 232);
            this.memoEdit_Criteria.TabIndex = 0;
            this.memoEdit_Criteria.UseOptimizedRendering = true;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(890, 271);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // panel_Center
            // 
            this.panel_Center.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel_Center.Controls.Add(this.tabControl1);
            this.panel_Center.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Center.Location = new System.Drawing.Point(0, 252);
            this.panel_Center.Name = "panel_Center";
            this.panel_Center.Size = new System.Drawing.Size(896, 260);
            this.panel_Center.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.printButton1);
            this.panelControl1.Controls.Add(this.previewButton1);
            this.panelControl1.Controls.Add(this.sbtnToggleOptions);
            this.panelControl1.Controls.Add(this.sbtnClose);
            this.panelControl1.Controls.Add(this.sbtnInquiry);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 213);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(896, 39);
            this.panelControl1.TabIndex = 1;
            // 
            // printButton1
            // 
            this.printButton1.Location = new System.Drawing.Point(173, 6);
            this.printButton1.Name = "printButton1";
            this.printButton1.ReportType = "";
            this.printButton1.Size = new System.Drawing.Size(72, 23);
            this.printButton1.TabIndex = 0;
            this.printButton1.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton1_Print);
            // 
            // previewButton1
            // 
            this.previewButton1.Location = new System.Drawing.Point(96, 6);
            this.previewButton1.Name = "previewButton1";
            this.previewButton1.ReportType = "";
            this.previewButton1.Size = new System.Drawing.Size(72, 23);
            this.previewButton1.TabIndex = 1;
            this.previewButton1.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton1_Preview);
            // 
            // sbtnToggleOptions
            // 
            this.sbtnToggleOptions.Location = new System.Drawing.Point(250, 6);
            this.sbtnToggleOptions.Name = "sbtnToggleOptions";
            this.sbtnToggleOptions.Size = new System.Drawing.Size(75, 23);
            this.sbtnToggleOptions.TabIndex = 2;
            this.sbtnToggleOptions.Text = "Hide Options";
            this.sbtnToggleOptions.Click += new System.EventHandler(this.sbtnToggleOptions_Click);
            // 
            // sbtnClose
            // 
            this.sbtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnClose.Location = new System.Drawing.Point(330, 6);
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Size = new System.Drawing.Size(75, 23);
            this.sbtnClose.TabIndex = 3;
            this.sbtnClose.Text = "Close";
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // sbtnInquiry
            // 
            this.sbtnInquiry.Location = new System.Drawing.Point(16, 6);
            this.sbtnInquiry.Name = "sbtnInquiry";
            this.sbtnInquiry.Size = new System.Drawing.Size(75, 23);
            this.sbtnInquiry.TabIndex = 4;
            this.sbtnInquiry.Text = "Inquiry";
            this.sbtnInquiry.Click += new System.EventHandler(this.sbtnInquiry_Click);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem2,
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 6;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Report";
            this.barSubItem2.Id = 3;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Design Document Style Report";
            this.barButtonItem1.Id = 4;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDocumentStyleReport_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Design Listing Style Report";
            this.barButtonItem2.Id = 5;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignListingStyleReport_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(896, 22);
            this.barDockControlTop.Click += new System.EventHandler(this.barDockControlTop_Click);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 512);
            this.barDockControlBottom.Size = new System.Drawing.Size(896, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 490);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(896, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 490);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 0;
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barBtnDesignDocumentStyleReport
            // 
            this.barBtnDesignDocumentStyleReport.Id = 1;
            this.barBtnDesignDocumentStyleReport.Name = "barBtnDesignDocumentStyleReport";
            this.barBtnDesignDocumentStyleReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignDocumentStyleReport_ItemClick);
            // 
            // barBtnDesignListingStyleReport
            // 
            this.barBtnDesignListingStyleReport.Id = 2;
            this.barBtnDesignListingStyleReport.Name = "barBtnDesignListingStyleReport";
            this.barBtnDesignListingStyleReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDesignListingStyleReport_ItemClick);
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Print Stock Return Raw Material Listing";
            this.panelHeader1.HelpNavigator = System.Windows.Forms.HelpNavigator.AssociateIndex;
            this.panelHeader1.HelpTopicId = "Stock_Receive.htm#stk028";
            this.panelHeader1.Location = new System.Drawing.Point(0, 22);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(896, 44);
            this.panelHeader1.TabIndex = 3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.Text = "Custom 1";
            // 
            // FormStockReceiveRMPrintListing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.sbtnClose;
            this.ClientSize = new System.Drawing.Size(896, 512);
            this.Controls.Add(this.panel_Center);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelCriteria);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormStockReceiveRMPrintListing";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormStockReceiveRMList_Closing);
            this.Load += new System.EventHandler(this.FormSIPrintSearch_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormStockReceiveRMPrintSearch_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelCriteria)).EndInit();
            this.panelCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbEditReportType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox_SearchCriteria)).EndInit();
            this.groupBox_SearchCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelledOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbReportOption)).EndInit();
            this.gbReportOption.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbEditGroupBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEditSortBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditShowCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Criteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Center)).EndInit();
            this.panel_Center.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        public class FormEventArgs
        {
            private FormStockReceiveRMPrintListing myForm;

            public StockReceiveRMCommand Command
            {
                get
                {
                    return this.myForm.myCommand;
                }
            }

            public PanelControl PanelCriteria
            {
                get
                {
                    return this.myForm.panelCriteria;
                }
            }

            public PanelControl PanelButtons
            {
                get
                {
                    return this.myForm.panelControl1;
                }
            }

            public XtraTabControl TabControl
            {
                get
                {
                    return this.myForm.tabControl1;
                }
            }

            public GridControl GridControl
            {
                get
                {
                    return this.myForm.stockReceiveRMGrid1.GridControl;
                }
            }

            public FormStockReceiveRMPrintListing Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public DBSetting DBSetting
            {
                get
                {
                    return this.myForm.myDBSetting;
                }
            }

            public FormEventArgs(FormStockReceiveRMPrintListing form)
            {
                this.myForm = form;
            }
        }

        public class FormInitializeEventArgs : FormStockReceiveRMPrintListing.FormEventArgs
        {
            public FormInitializeEventArgs(FormStockReceiveRMPrintListing form)
              : base(form)
            {
            }
        }

        public class FormInquiryEventArgs : FormStockReceiveRMPrintListing.FormEventArgs
        {
            private DataTable myResultTable;

            public DataTable ResultTable
            {
                get
                {
                    return this.myResultTable;
                }
            }

            public FormInquiryEventArgs(FormStockReceiveRMPrintListing form, DataTable resultTable)
              : base(form)
            {
                this.myResultTable = resultTable;
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void barDockControlTop_Click(object sender, EventArgs e)
        {

        }
    }

}
