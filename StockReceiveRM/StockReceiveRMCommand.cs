﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMCommand
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.Data;
using BCE.AutoCount.Document;
using BCE.AutoCount.GL.JournalEntry;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.RegistryID.LastSavedDescriptionID;
using BCE.AutoCount.RegistryID.Misc;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.SerialNumber2;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Production.StockReceiveRM
{
  public class StockReceiveRMCommand
  {
    public const string DocumentStyleReportType = "Stock Return Raw Material Document";
    public const string ListingStyleReportType = "RPA Stock Return Raw Material Listing";
    protected const string ReportOptionSettingFileName = "StockReceiveRMReportOption.setting";
    internal const string MasterTableName = "Master";
    internal const string ShadowMasterTableName = "ShadowMaster";
    internal const string DetailTableName = "Detail";
    internal const string SerialNoTableName = "SerialNoTrans";
    protected DataTable myDataTableAllMaster;
    protected internal StockHelper myHelper;
    protected internal DBSetting myDBSetting;
    internal FiscalYear myFiscalYear;
    internal GeneralSetting myGeneralSetting;
    private BasicReportOption myReportOption;
    protected const string LoadMasterDataSQL = "SELECT * FROM RPA_RCVRM WHERE DocKey=@DocKey";
    protected const string LoadDetailDataSQL = "SELECT * FROM RPA_RCVRMDtl WHERE DocKey=@DocKey ORDER BY Seq";
    protected const string LoadMasterDataByDocNoSQL = "SELECT * FROM RPA_RCVRM WHERE DocNo=@DocNo";
    protected const string LoadDetailDataByDocNoSQL = "SELECT A.* FROM RPA_RCVRMDtl A, RPA_RCVRM B WHERE A.DocKey=B.DocKey AND B.DocNo=@DocNo ORDER BY A.Seq";
    protected const string SaveMasterDataSQL = "SELECT * FROM RPA_RCVRM";
    protected const string SaveDetailDataSQL = "SELECT * FROM RPA_RCVRMDtl";
    protected const string DeleteMasterDataSQL = "DELETE FROM RPA_RCVRM WHERE DocKey=@DocKey";
    protected const string DeleteDetailDataSQL = "DELETE FROM RPA_RCVRMDtl WHERE DocKey=@DocKey";
    protected const string LoadAllMasterDataSQL = "SELECT {0} FROM RPA_RCVRM {1}";
    protected const string LoadSearchMasterDataSQL = "SELECT DISTINCT {0} FROM RPA_RCVRM A, RPA_RCVRMDtl B WHERE (A.DocKey=B.DocKey)";
    protected const string LoadSearchMasterDataOnlySQL = "SELECT {0} FROM RPA_RCVRM A";
    protected const string LoadDesignReportMasterDataSQL = "SELECT TOP 5 * FROM vRPA_StockReceiveRM ORDER BY DocKey";
    protected const string LoadDesignReportDetailDataSQL = "SELECT * FROM vRPA_StockReceiveRMDtl WHERE DocKey IN (SELECT TOP 5 DocKey FROM vRPA_StockReceiveRM ORDER BY DocKey) AND PrintOut='T' ORDER BY DocKey, Seq";
    protected const string LoadReportMasterDataSQL = "SELECT * FROM vRPA_StockReceiveRM WHERE DocKey=@DocKey";
    protected const string LoadReportDetailDataSQL = "SELECT * FROM vRPA_StockReceiveRMDtl WHERE DocKey=@DocKey AND PrintOut='T' ORDER BY Seq";
    protected const string BasicSearchSQL = " SELECT {0} FROM RPA_RCVRM WHERE DocKey In (SELECT DISTINCT RPA_RCVRM.DocKey FROM RPA_RCVRM INNER JOIN RPA_RCVRMDtl ON RPA_RCVRM.DocKey = RPA_RCVRMDtl.DocKey WHERE {1})";
    protected const string LoadDocumentListingReportMasterDataSQL = "SELECT * FROM vRPA_StockReceiveRM WHERE DocKey IN ({0}) ";
    protected const string LoadDocumentListingReportDetailDataSQL = "SELECT * FROM vRPA_StockReceiveRMDtl WHERE DocKey IN ({0}) ORDER BY Seq";
    protected const string LoadDesignDocumentListingReportMasterDataSQL = "SELECT TOP 100 * FROM vRPA_StockReceiveRM ORDER BY DocKey";
    protected const string LoadDesignDocumentListingReportDetailDataSQL = "SELECT * FROM vRPA_StockReceiveRMDtl WHERE DocKey IN (SELECT TOP 100 DocKey FROM vRPA_StockReceiveRM ORDER BY DocKey) ORDER BY DocKey, Seq";
    protected const string DocumentStyleReportingBasicSearchSQL = " SELECT DISTINCT {0} FROM RPA_RCVRM LEFT OUTER JOIN RPA_RCVRMDtl ON (RPA_RCVRM.DocKey=RPA_RCVRMDtl.DocKey) WHERE {1}";
    private static AsyncDataSetUpdate myDataSetUpdate;
    private DBRegistry myDBReg;
    protected DecimalSetting myDecimalSetting;
    protected UserAuthentication myUserAuthentication;

    public static AsyncDataSetUpdate DataSetUpdate
    {
      get
      {
        if (StockReceiveRMCommand.myDataSetUpdate == null)
          StockReceiveRMCommand.myDataSetUpdate = new AsyncDataSetUpdate();
        return StockReceiveRMCommand.myDataSetUpdate;
      }
    }

    public BasicReportOption ReportOption
    {
      get
      {
        return this.myReportOption;
      }
    }

    public DataTable DataTableAllMaster
    {
      get
      {
        return this.myDataTableAllMaster;
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    public DBRegistry DBReg
    {
      get
      {
        return this.myDBReg;
      }
    }

    public GeneralSetting GeneralSetting
    {
      get
      {
        return this.myGeneralSetting;
      }
    }

    static StockReceiveRMCommand()
    {
    }

    internal StockReceiveRMCommand()
    {
      this.myDataTableAllMaster = new DataTable();
      try
      {
        this.myReportOption = (BasicReportOption) PersistenceUtil.LoadUserSetting("StockReceiveRMReportOption.setting");
      }
      catch
      {
      }
      if (this.myReportOption == null)
        this.myReportOption = new BasicReportOption();
    }

    public static StockReceiveRMCommand Create(DBSetting dbSetting)
    {
      UserAuthentication.GetOrCreate(dbSetting).CheckHasLogined();
      StockReceiveRMCommand stockReceiveCommand = (StockReceiveRMCommand) null;
      if (dbSetting.ServerType == DBServerType.SQL2000)
        stockReceiveCommand = (StockReceiveRMCommand) new StockReceiveRMCommandSQL();
      else
        dbSetting.ThrowServerTypeNotSupportedException();
      stockReceiveCommand.myDBSetting = dbSetting;
      stockReceiveCommand.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
      stockReceiveCommand.myDecimalSetting = DecimalSetting.GetOrCreate(dbSetting);
      stockReceiveCommand.myHelper = StockHelper.Create(dbSetting);
      stockReceiveCommand.myDBReg = DBRegistry.Create(dbSetting);
      stockReceiveCommand.myFiscalYear = FiscalYear.GetOrCreate(dbSetting);
      stockReceiveCommand.myGeneralSetting = GeneralSetting.GetOrCreate(dbSetting);
      return stockReceiveCommand;
    }

    protected virtual DataSet LoadData(long docKey)
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadData(string docNo)
    {
      return (DataSet) null;
    }

    protected virtual long LoadFirst()
    {
      return -1L;
    }

    protected virtual long LoadLast()
    {
      return -1L;
    }

    protected virtual long LoadNext(string docNo)
    {
      return -1L;
    }

    protected virtual long LoadPrev(string docNo)
    {
      return -1L;
    }

    protected internal virtual void SaveData(StockReceiveRM stockReceive, bool canceldoc)
    {
    }

    protected virtual void DeleteData(long docKey)
    {
    }

    public virtual void DocumentListingBasicSearch(StockReceiveRMReportingCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
    {
    }

    public virtual void AdvanceSearch(AdvancedStockReceiveRMCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
    {
    }

    public virtual void BasicSearch(StockReceiveRMCriteria criteria, string columnName, DataTable resultDataTable, string checkEditColumnName)
    {
    }

    public virtual void SaveDocumentInfoTable(DataTable table)
    {
    }

    public virtual DataTable GetDocumentInfoTable(long[] docKeys)
    {
      return (DataTable) null;
    }

    private void CreateShadowMasterTable(DataSet ds)
    {
      if (ds.Tables.IndexOf("ShadowMaster") < 0)
      {
        DataTable table = ds.Tables["Master"].Copy();
        table.TableName = "ShadowMaster";
        ds.Tables.Add(table);
      }
    }

    public StockReceiveRM LoadFromTempDocument(long docKey)
    {
      DataSet dataSet = TempDocument.Create(this.myDBSetting).Load("RR", docKey);
      if (dataSet == null)
      {
        return (StockReceiveRM) null;
      }
      else
      {
        this.CreateShadowMasterTable(dataSet);
        StockReceiveRMAction action = dataSet.Tables["Master"].Rows[0].RowState != DataRowState.Added ? StockReceiveRMAction.Edit : StockReceiveRMAction.New;
        return new StockReceiveRM(this, dataSet, action);
      }
    }

    public void SaveToTempDocument(StockReceiveRM document, string saveReason)
    {
      TempDocument.Create(this.myDBSetting).Save(document.DocKey, "RR", (string) document.DocNo, saveReason, (string) document.Description, document.myDataSet);
    }

    public StockReceiveRM AddNew()
    {
      DataSet dataSet = this.LoadData(-1L);
      DataRow row = dataSet.Tables["Master"].NewRow();
      this.InitNewMasterRow(row);
      dataSet.Tables["Master"].Rows.Add(row);
      this.CreateShadowMasterTable(dataSet);
      StockReceiveRM doc = new StockReceiveRM(this, dataSet, StockReceiveRMAction.New);
      StockReceiveRMEventArgs stockReceiveEventArgs1 = new StockReceiveRMEventArgs(doc);
      ScriptObject scriptObject = doc.ScriptObject;
      string name = "OnNewDocument";
      Type[] types = new Type[1];
      int index1 = 0;
      Type type = stockReceiveEventArgs1.GetType();
      types[index1] = type;
      object[] objArray = new object[1];
      int index2 = 0;
      StockReceiveRMEventArgs stockReceiveEventArgs2 = stockReceiveEventArgs1;
      objArray[index2] = (object) stockReceiveEventArgs2;
      scriptObject.RunMethod(name, types, objArray);
      return doc;
    }

    private void InitNewMasterRow(DataRow row)
    {
      row.BeginEdit();
      if (row.Table.Columns.Contains("Guid"))
        row["Guid"] = (object) Guid.NewGuid();
      row["DocKey"] = (object) this.myDBReg.IncOne((IRegistryID) new GlobalUniqueKey());
      row["PrintCount"] = (object) 0;
      row["Cancelled"] = (object) BCE.Data.Convert.BooleanToText(false);
      row["CanSync"] = (object) BCE.Data.Convert.BooleanToText(true);
      row["DocNo"] = (object) "<<New>>";
      row["DocDate"] = (object) Application.SystemDate;
      row["LastUpdate"] = (object) -1;
      row["LastModifiedUserID"] = (object) "";
      row["LastModified"] = (object) DateTime.MinValue;
      row["CreatedUserID"] = (object) "";
      row["CreatedTimeStamp"] = (object) DateTime.MinValue;
     // string str = this.myDBReg.GetString((IRegistryID) new StockReceiveDescriptionID());
     // int maxLength = row.Table.Columns["Description"].MaxLength;
    //  if (str.Length > maxLength)
       // str = str.Substring(0, maxLength);
      row["Description"] = (object)"STOCK RETURN RAW MATERIAL";
      //row["ReallocatePurchaseByProject"] = (object) BCE.Data.Convert.BooleanToText(this.myDBReg.GetBoolean((IRegistryID) new ReallocatePurchaseByProject()));
      row.EndEdit();
    }

    public StockReceiveRM View(long docKey)
    {
      return this.InternalView(this.LoadData(docKey));
    }

    public StockReceiveRM View(string docNo)
    {
      return this.InternalView(this.LoadData(docNo));
    }

    public StockReceiveRM ViewFirst()
    {
      long docKey = this.LoadFirst();
      if (docKey < 0L)
        return (StockReceiveRM) null;
      else
        return this.View(docKey);
    }

    public StockReceiveRM ViewLast()
    {
      long docKey = this.LoadLast();
      if (docKey < 0L)
        return (StockReceiveRM) null;
      else
        return this.View(docKey);
    }

    public StockReceiveRM ViewNext(string docNo)
    {
      long docKey = this.LoadNext(docNo);
      if (docKey < 0L)
        return (StockReceiveRM) null;
      else
        return this.View(docKey);
    }

    public StockReceiveRM ViewPrev(string docNo)
    {
      long docKey = this.LoadPrev(docNo);
      if (docKey < 0L)
        return (StockReceiveRM) null;
      else
        return this.View(docKey);
    }

    private StockReceiveRM InternalView(DataSet newDataSet)
    {
      if (newDataSet.Tables["Master"].Rows.Count == 0)
      {
        return (StockReceiveRM) null;
      }
      else
      {
        this.CreateShadowMasterTable(newDataSet);
        return new StockReceiveRM(this, newDataSet, StockReceiveRMAction.View);
      }
    }

    public StockReceiveRM Edit(long docKey)
    {
      return this.InternalEdit(this.LoadData(docKey));
    }

    public StockReceiveRM Edit(string docNo)
    {
      return this.InternalEdit(this.LoadData(docNo));
    }

    private StockReceiveRM InternalEdit(DataSet newDataSet)
    {
      if (newDataSet.Tables["Master"].Rows.Count == 0)
      {
        return (StockReceiveRM) null;
      }
      else
      {
        this.myFiscalYear.CheckTransactionDate(BCE.Data.Convert.ToDateTime(newDataSet.Tables["Master"].Rows[0]["DocDate"]), "StockReceiveRM", this.myDBSetting);
        this.CreateShadowMasterTable(newDataSet);
        return new StockReceiveRM(this, newDataSet, StockReceiveRMAction.Edit);
      }
    }

    private long GetDocKeyByDocNo(string docNo)
    {
      DBSetting dbSetting = this.myDBSetting;
      string cmdText = "SELECT DocKey FROM RPA_RCVRM WHERE DocNo=?";
      object[] objArray = new object[1];
      int index = 0;
      string str = docNo;
      objArray[index] = (object) str;
      object obj = dbSetting.ExecuteScalar(cmdText, objArray);
      if (obj == null)
        return -1L;
      else
        return BCE.Data.Convert.ToInt64(obj);
    }

    public void Delete(long docKey)
    {
      this.DeleteData(docKey);
    }

    public void Delete(string docNo)
    {
      this.DeleteData(this.GetDocKeyByDocNo(docNo));
    }

    public bool CancelDocument(long docKey, string userID)
    {
      StockReceiveRM stockReceive = this.View(docKey);
      if (stockReceive != null)
      {
        stockReceive.CancelDocument(userID);
        return true;
      }
      else
        return false;
    }

    public bool CancelDocument(string docNo, string userID)
    {
      StockReceiveRM stockReceive = this.View(docNo);
      if (stockReceive != null)
      {
        stockReceive.CancelDocument(userID);
        return true;
      }
      else
        return false;
    }

    public bool UncancelDocument(long docKey, string userID)
    {
      StockReceiveRM stockReceive = this.View(docKey);
      if (stockReceive != null)
      {
        stockReceive.UncancelDocument(userID);
        return true;
      }
      else
        return false;
    }

    public bool UncancelDocument(string docNo, string userID)
    {
      StockReceiveRM stockReceive = this.View(docNo);
      if (stockReceive != null)
      {
        stockReceive.UncancelDocument(userID);
        return true;
      }
      else
        return false;
    }

    protected void SaveSerialNo(DataSet ds, DBSetting newDBSetting)
    {
      DataTable dataTable = ds.Tables["Detail"];
      DataTable dtblSerialNo = ds.Tables["SerialNoTrans"];
      if (dataTable.Rows.Count != 0 && dtblSerialNo.Rows.Count != 0 && !BCE.Data.Convert.TextToBoolean(ds.Tables["Master"].Rows[0]["Cancelled"]))
      {
        SerialNumberHelper serialNumberHelper = new SerialNumberHelper(newDBSetting, "RR");
        foreach (DataRow detailRw in (InternalDataCollectionBase) dataTable.Rows)
          serialNumberHelper.SaveSerialNo(ds.Tables["Master"].Rows[0], detailRw, dtblSerialNo);
      }
    }

    protected void CancelUncancelSerialNo(DataSet ds, DBSetting newDBSetting, bool isCancelled)
    {
      DataTable dataTable = ds.Tables["Detail"];
      DataTable dtblSerialNo = ds.Tables["SerialNoTrans"];
      if (dataTable.Rows.Count != 0 && dtblSerialNo.Rows.Count != 0)
      {
        SerialNumberHelper serialNumberHelper = new SerialNumberHelper(newDBSetting, "RR");
        foreach (DataRow detailRow in (InternalDataCollectionBase) dataTable.Rows)
          serialNumberHelper.CancellationSN(detailRow, dtblSerialNo, isCancelled);
      }
    }

    protected void DeleteSerialNo(DataSet ds, DBSetting newDBSetting)
    {
      DataTable dataTable = ds.Tables["Detail"];
      DataTable dtblSerialNo = ds.Tables["SerialNoTrans"];
      if (dataTable.Rows.Count != 0 && dtblSerialNo.Rows.Count != 0)
      {
        DataRow dataRow = ds.Tables["Master"].Rows[0];
        string index1 = "Cancelled";
        bool isCancelled = BCE.Data.Convert.TextToBoolean(dataRow[index1]);
        SerialNumberHelper serialNumberHelper = new SerialNumberHelper(newDBSetting, "RR");
        string index2 = "DocKey";
        long docKey = BCE.Data.Convert.ToInt64(dataRow[index2]);
        foreach (DataRow detailRow in (InternalDataCollectionBase) dataTable.Rows)
          serialNumberHelper.DeleteSN(docKey, detailRow, dtblSerialNo, isCancelled, true);
      }
    }

    public virtual int InquireAllMaster(string columnSQL, bool hasYearMonth)
    {
      return 0;
    }

    public virtual int SearchMaster(SearchCriteria criteria, string columnSQL, DataTable resultTable, string MultiSelectColumnName)
    {
      return 0;
    }
        protected void PostToStockCostingWIP(DataSet ds, DBSetting newDBSetting)
        {

            DataRow dataRow1 = ds.Tables["Master"].Rows[0];
            DataTable dataTable = ds.Tables["Detail"];
            DataTable subdataTable = ds.Tables["SubDetail"];
            DateTime docDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            long docKey = BCE.Data.Convert.ToInt64(dataRow1["DocKey"]);
            if (docDate >= this.myFiscalYear.ActualDataStartDate && !BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
            {
                bool enable = ModuleControl.GetOrCreate(newDBSetting).ModuleController.AdvancedMultiUOM.Enable;
                Production.StockCard.StockTrans trans = new Production.StockCard.StockTrans(newDBSetting, "SR", docKey);
                foreach (DataRow dataRow2 in dataTable.Select("ItemCode IS NOT NULL", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent))
                {

                    
                    {
                        Decimal num = BCE.Data.Convert.ToDecimal(dataRow2["Qty"]);
                        Decimal dunitcost = BCE.Data.Convert.ToDecimal(dataRow2["UnitCost"]);

                        string itemCode = dataRow2["ItemCode"].ToString();
                        string str = dataRow2["UOM"].ToString();
                        string strWONo = "";
                        string strBOMCode = "";
                        string strProductCode = "";
                        object obj = myDBSetting.ExecuteScalar("select FromDocNo from RPA_RMDTL with(nolock) where DtlKey=?", (object)dataRow2["FromDocDtlKey"]);

                        if (obj != null && obj != DBNull.Value)
                            strWONo = obj.ToString();
                        if (dataRow2["BOMCode"] != null && dataRow2["BOMCode"] != DBNull.Value)
                            strBOMCode = dataRow2["BOMCode"].ToString();
                         obj = myDBSetting.ExecuteScalar("select c.ItemCode from RPA_WODtl a with(nolock) inner join RPA_WO b with(nolock) on a.DocKey=b.DocKey inner join RPA_WOProduct c with(nolock) on c.DocKey=b.DocKey and a.FromDocDtlKey=c.DtlKey  where DocNo=? and a.DtlKey=?", (object)strWONo, (object)dataRow2["FromDocDtlKey"]);
                        if (obj != null && obj != DBNull.Value)
                        {
                            strProductCode = obj.ToString();
                        }
                        if (!enable)
                        {
                            StockHelper stockHelper = StockHelper.Create(myDBSetting);
                            try
                            {
                                Decimal itemUomRate = stockHelper.GetItemUOMRate(itemCode, str);
                                if (itemUomRate != Decimal.Zero)
                                {
                                   
                                    dunitcost = 0;
                                    str = stockHelper.GetBaseUOM(dataRow2["ItemCode"].ToString());
                                    num = this.myDecimalSetting.RoundQuantity(num * itemUomRate);
                                    StockDocumentItem stockDocumentItem = myHelper.LoadStockDocumentItem(dataRow2["ItemCode"].ToString());
                                    dunitcost = BCE.Data.Convert.ToDecimal(stockDocumentItem.Cost);

                                    if (dunitcost == 0)
                                    {
                                        obj = myDBSetting.ExecuteScalar("select RealCost from itemuom where ItemCode=? and UOM=?", (object)dataRow2["ItemCode"], (object)str);
                                        if (obj != null && obj != DBNull.Value)
                                            dunitcost = BCE.Data.Convert.ToDecimal(obj);
                                    }
                                }
                            }
                            catch { }
                        }
                        object oBatchNo = null;
                        if (dataRow2["BatchNo"] != null)
                            oBatchNo = dataRow2["BatchNo"].ToString() == "" ? null : dataRow2["BatchNo"];

                        trans.Add(BCE.Data.Convert.ToInt64(dataRow2["DtlKey"]), dataRow1["DocNo"].ToString(), strWONo, strProductCode, strBOMCode, itemCode, str, dataRow2["Location"].ToString(), dataRow2["BatchNo"],"", dataRow2["ProjNo"], dataRow2["DeptNo"], docDate, -num, dunitcost, 0L);

                    }

                }
                Production.StockCard.StockCosting.Create(newDBSetting).Add(trans);
            }
            else
                this.DeleteFromStockCostingWIP(docKey, newDBSetting);
        }

        protected void DeleteFromStockCostingWIP(long docKey, DBSetting newDBSetting)
        {
            Production.StockCard.StockCosting.Create(newDBSetting).Remove("SR", docKey);
        }


        protected void PostToStockCosting(DataSet ds, DBSetting newDBSetting)
        {
            DataRow dataRow1 = ds.Tables["Master"].Rows[0];
            DataTable dataTable = ds.Tables["Detail"];
            DateTime docDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            long docKey = BCE.Data.Convert.ToInt64(dataRow1["DocKey"]);
            if (docDate >= this.myFiscalYear.ActualDataStartDate && !BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
            {
                bool enable = ModuleControl.GetOrCreate(newDBSetting).ModuleController.AdvancedMultiUOM.Enable;
                StockTrans trans = new StockTrans(newDBSetting, "SR", docKey);
                foreach (DataRow dataRow2 in dataTable.Select("ItemCode IS NOT NULL", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent))
                {
                    Decimal cost = BCE.Data.Convert.ToDecimal(dataRow2["UnitCost"]);
                    Decimal qty = BCE.Data.Convert.ToDecimal(dataRow2["Qty"]);
                    string itemCode = dataRow2["ItemCode"].ToString();
                    string str = dataRow2["UOM"].ToString();
                    if (!enable)
                    {
                        StockHelper stockHelper = StockHelper.Create(newDBSetting);
                        Decimal itemUomRate = stockHelper.GetItemUOMRate(itemCode, str);
                        if (itemUomRate != Decimal.Zero)
                        {
                            str = stockHelper.GetBaseUOM(dataRow2["ItemCode"].ToString());
                            qty = this.myDecimalSetting.RoundQuantity(qty * itemUomRate);
                            cost = this.myDecimalSetting.RoundCost(cost / itemUomRate);
                        }
                    }
                    trans.Add(BCE.Data.Convert.ToInt64(dataRow2["DtlKey"]), itemCode, str, dataRow2["Location"].ToString(), dataRow2["BatchNo"], dataRow2["ProjNo"], dataRow2["DeptNo"], docDate, qty, cost, 0L);
                }
                StockCosting.Create(newDBSetting).Add(trans);
            }
            else
                this.DeleteFromStockCosting(docKey, newDBSetting);
        }

        protected void DeleteFromStockCosting(long docKey, DBSetting newDBSetting)
    {
      StockCosting.Create(newDBSetting).Remove("RR", docKey);
    }

    protected void PostAuditLog(DataSet ds, DBSetting dbSetting, AuditTrail.EventType eventType)
    {
      DataRow dataRow = ds.Tables["Master"].Rows[0];
      AuditColumnMap auditColumnMap1 = new AuditColumnMap(Localizer.GetString((Enum) StockReceiveRMColumnStringId.StockReceiveRM, new object[0]), ds.Tables["Master"]);
      auditColumnMap1.AddColumn("DocNo", Localizer.GetString((Enum) StockReceiveRMColumnStringId.DocumentNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap1.AddColumn("DocDate", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Date, new object[0]), DocumentAuditTrail.ColumnType.Date);
      auditColumnMap1.AddColumn("Description", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Description, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap1.AddColumn("RefDocNo", Localizer.GetString((Enum) StockReceiveRMColumnStringId.RefDocNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      RemarkNameEntity remarkName = RemarkName.Create(dbSetting).GetRemarkName("RR");
      if (remarkName == null)
      {
        auditColumnMap1.AddColumn("Remark1", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Remark1, new object[0]), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark2", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Remark2, new object[0]), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark3", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Remark3, new object[0]), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark4", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Remark4, new object[0]), DocumentAuditTrail.ColumnType.String);
      }
      else
      {
        auditColumnMap1.AddColumn("Remark1", remarkName.GetRemarkName(1), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark2", remarkName.GetRemarkName(2), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark3", remarkName.GetRemarkName(3), DocumentAuditTrail.ColumnType.String);
        auditColumnMap1.AddColumn("Remark4", remarkName.GetRemarkName(4), DocumentAuditTrail.ColumnType.String);
      }
      AuditColumnMap auditColumnMap2 = new AuditColumnMap(Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Detail, new object[0]), ds.Tables["Detail"]);
      auditColumnMap2.AddColumn("ItemCode", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.ItemCode, new object[0]), DocumentAuditTrail.ColumnType.String, true);
      auditColumnMap2.AddColumn("UOM", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.UOM, new object[0]), DocumentAuditTrail.ColumnType.String, true);
      auditColumnMap2.AddColumn("Description", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Description, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("Location", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Location, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("BatchNo", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.BatchNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("ProjNo", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.ProjectNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("DeptNo", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.DepartmentNo, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("Qty", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Quantity, new object[0]), DocumentAuditTrail.ColumnType.Quantity);
      auditColumnMap2.AddColumn("UnitCost", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.UnitCost, new object[0]), DocumentAuditTrail.ColumnType.Cost);
      auditColumnMap2.AddColumn("SerialNoList", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.SerialNoList, new object[0]), DocumentAuditTrail.ColumnType.String);
      auditColumnMap2.AddColumn("Numbering", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Numbering, new object[0]), DocumentAuditTrail.ColumnType.String);
      DocumentAuditTrail documentAuditTrail = new DocumentAuditTrail(dbSetting, "RR");
      AuditColumnMap masterColMap1 = auditColumnMap1;
      AuditColumnMap detailColMap1 = auditColumnMap2;
      DBSetting newDBSetting = dbSetting;
      int num1 = (int) eventType;
      documentAuditTrail.PostAuditLog(masterColMap1, detailColMap1, newDBSetting, (AuditTrail.EventType) num1);
      string detail = "";
      AuditColumnMap masterColMap2 = auditColumnMap1;
      AuditColumnMap detailColMap2 = auditColumnMap2;
      int num2 = (int) eventType;
      // ISSUE: explicit reference operation
      // ISSUE: variable of a reference type
      string message = @detail;
      documentAuditTrail.BuildAuditLog(masterColMap2, detailColMap2, (AuditTrail.EventType) num2,ref message);
      long docKey = dataRow.RowState != DataRowState.Deleted ? BCE.Data.Convert.ToInt64(dataRow["DocKey"]) : BCE.Data.Convert.ToInt64(dataRow["DocKey", DataRowVersion.Original]);
      if (eventType == AuditTrail.EventType.New)
      {
        // ISSUE: variable of a boxed type
        StockReceiveRMString local =  StockReceiveRMString.CreateNewStockReceiveRM;
        object[] objArray = new object[2];
        int index1 = 0;
        string loginUserId = this.myUserAuthentication.LoginUserID;
        objArray[index1] = (object) loginUserId;
        int index2 = 1;
        string str = dataRow["DocNo"].ToString();
        objArray[index2] = (object) str;
        string @string = Localizer.GetString((Enum) local, objArray);
        Activity.Log(this.myDBSetting, "RR", docKey, 0L, @string, detail);
      }
      else if (eventType == AuditTrail.EventType.Edit)
      {
        if (detail.Length > 0)
        {
          // ISSUE: variable of a boxed type
          StockReceiveRMString local =  StockReceiveRMString.UpdatedStockReceiveRM;
          object[] objArray = new object[2];
          int index1 = 0;
          string loginUserId = this.myUserAuthentication.LoginUserID;
          objArray[index1] = (object) loginUserId;
          int index2 = 1;
          string str = dataRow["DocNo", DataRowVersion.Original].ToString();
          objArray[index2] = (object) str;
          string @string = Localizer.GetString((Enum) local, objArray);
          Activity.Log(this.myDBSetting, "RR", docKey, 0L, @string, detail);
        }
      }
      else if (eventType == AuditTrail.EventType.Delete)
      {
        // ISSUE: variable of a boxed type
        StockReceiveRMString local = StockReceiveRMString.DeletedStockReceiveRM;
        object[] objArray = new object[2];
        int index1 = 0;
        string loginUserId = this.myUserAuthentication.LoginUserID;
        objArray[index1] = (object) loginUserId;
        int index2 = 1;
        string str = dataRow["DocNo", DataRowVersion.Original].ToString();
        objArray[index2] = (object) str;
        string @string = Localizer.GetString((Enum) local, objArray);
        Activity.Log(this.myDBSetting, "RR", docKey, 0L, @string, detail);
      }
    }

    protected void UpdateStockUOMConv(StockReceiveRM doc)
    {
      DBSetting dbSetting = this.myDBSetting;
      string cmdText = "SELECT FromDocNo FROM UOMConv WHERE FromDocKey = ?";
      object[] objArray = new object[1];
      int index = 0;
      // ISSUE: variable of a boxed type
      long local =  doc.DocKey;
      objArray[index] = (object) local;
      object obj = dbSetting.ExecuteScalar(cmdText, objArray);
      if (obj != null && obj.ToString() != (string) doc.DocNo)
        this.myDBSetting.ExecuteNonQuery(string.Format("UPDATE UOMConv SET FromDocNo = '{0}' WHERE FromDocKey = {1}", (object) doc.DocNo, (object) doc.DocKey), new object[0]);
    }

  

    public void SaveReportOption()
    {
      PersistenceUtil.SaveUserSetting((object) this.myReportOption, "StockReceiveRMReportOption.setting");
    }

    protected virtual DataSet LoadDesignReportData()
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadReportData(long docKey)
    {
      return (DataSet) null;
    }

    private DocumentReportDataSet PreparingReportDataSet(DataSet dsReportData)
    {
      DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, "Stock Return Raw Material", "Stock Return Raw Material Master", "Stock Return Raw Material Detail");
      documentReportDataSet.Tables.Add(dsReportData.Tables[0].Copy());
      documentReportDataSet.Tables.Add(dsReportData.Tables[1].Copy());
      DataRelation relation = new DataRelation("MasterDetailRelation", documentReportDataSet.Tables["Master"].Columns["DocKey"], documentReportDataSet.Tables["Detail"].Columns["DocKey"], false);
      documentReportDataSet.Relations.Add(relation);
      documentReportDataSet.AddDefaultTables();
      return documentReportDataSet;
    }

    public object GetReportDesignerDataSource()
    {
      return (object) this.PreparingReportDataSet(this.LoadDesignReportData());
    }

    public object GetReportDataSource(long docKey)
    {
      return (object) this.PreparingReportDataSet(this.LoadReportData(docKey));
    }

    public object GetReportDataSource(string dockeys)
    {
      return (object) this.PreparingReportDataSet(this.LoadDocumentListingReportData(dockeys));
    }

    public void PrintReport(SearchCriteria criteria)
    {
    }

    private DocumentReportDataSet PreparingDocumentListingReportDataSet(DataSet dsReportData, string docKeys, StockReceiveRMReportingCriteria criteria, string dataName)
    {
      DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, dataName, "Stock Return Raw Material Master", "Stock Return Raw Material Detail");
      documentReportDataSet.Tables.Add(dsReportData.Tables[0].Copy());
      documentReportDataSet.Tables.Add(dsReportData.Tables[1].Copy());
      documentReportDataSet.Relations.Add("MasterDetailRelation", documentReportDataSet.Tables[0].Columns["DocKey"], documentReportDataSet.Tables[1].Columns["DocKey"], false);
      StockReceiveRMCommand.AddFinalizeTables(this.myDBSetting, criteria, (DataSet) documentReportDataSet, docKeys, "RPA_RCVRM");
      documentReportDataSet.AddDefaultTables();
      return documentReportDataSet;
    }

    public object GetDocumentListingReportDataSource(string docKeys, StockReceiveRMReportingCriteria criteria)
    {
      return (object) this.PreparingDocumentListingReportDataSet(this.LoadDocumentListingReportData(docKeys), docKeys, criteria, "RPA Stock Return Raw Material Listing");
    }

    public object GetDocumentListingReportDesignerDataSource(StockReceiveRMReportingCriteria criteria)
    {
      DataSet dsReportData = this.LoadDocumentListingReportDesignerData();
      string docKeys = "";
      foreach (DataRow dataRow in (InternalDataCollectionBase) dsReportData.Tables["Master"].Rows)
        docKeys = docKeys + dataRow["Dockey"] + ", ";
      return (object) this.PreparingDocumentListingReportDataSet(dsReportData, docKeys, criteria, "RPA Stock Return Raw Material Listing");
    }

    protected virtual DataSet LoadDocumentListingReportData(string docKeys)
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadDocumentListingReportDesignerData()
    {
      return (DataSet) null;
    }

    public static EmailAndFaxInfo GetEmailAndFaxInfo(long docKey, DBSetting dbSetting)
    {
      string str1 = "Select A.DocNo, A.DocDate From RPA_RCVRM A WHERE A.DocKey = ?";
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      object[] objArray = new object[1];
      int index = 0;
      // ISSUE: variable of a boxed type
     long local =  docKey;
      objArray[index] = (object) local;
      DataRow firstDataRow = dbSetting1.GetFirstDataRow(cmdText, objArray);
      if (firstDataRow == null)
      {
        return new EmailAndFaxInfo();
      }
      else
      {
        EmailAndFaxInfo emailAndFaxInfo = new EmailAndFaxInfo();
        string str2 = string.Format("{0} {1}, dated {2}", (object) DocumentType.ToLocalizedString("RR"), (object) firstDataRow["DocNo"].ToString(), (object) GeneralSetting.GetOrCreate(dbSetting).FormatDate(BCE.Data.Convert.ToDateTime(firstDataRow["DocDate"])));
        emailAndFaxInfo.Subject = str2;
        string str3 = StringHelper.ConvertToValidFilename(string.Format("{0} {1}", (object) DocumentType.ToLocalizedString("RR"), (object) firstDataRow["DocNo"].ToString()));
        emailAndFaxInfo.AttachmentPdfFilename = str3;
        return emailAndFaxInfo;
      }
    }

    public static void AddFinalizeTables(DBSetting dbSetting, StockReceiveRMReportingCriteria criteria, DataSet dsReport, string docKeys, string masterTableName)
    {
      StockReceiveRMCommand.CreateReportOptionTable(criteria, dsReport);
      StockReceiveRMCommand.CreateGroupIDAndSortIDColumn(criteria, dsReport);
      StockReceiveRMCommand.AddSummaryTable(dbSetting, dsReport, docKeys, masterTableName);
    }

    public static void AddSummaryTable(DBSetting dbSetting, DataSet ds, string docKeys, string masterTableName)
    {
      ds.Tables.Add(StockReceiveRMCommand.GetItemAnalysisSummaryTable(dbSetting, docKeys, masterTableName + "DTL"));
      ds.Tables.Add(StockReceiveRMCommand.GetDateAnalysisSummaryTable(dbSetting, docKeys, masterTableName, masterTableName + "DTL"));
      ds.Tables.Add(StockReceiveRMCommand.GetMonthAnalysisSummaryTable(dbSetting, docKeys, masterTableName, masterTableName + "DTL"));
      ds.Tables.Add(StockReceiveRMCommand.GetYearAnalysisSummaryTable(dbSetting, docKeys, masterTableName, masterTableName + "DTL"));
    }

    public static void CreateReportOptionTable(StockReceiveRMReportingCriteria criteria, DataSet dsReport)
    {
      DataTable table = new DataTable("Report Option");
      table.Columns.Add("Criteria", typeof (string));
      table.Columns.Add("ShowCriteria", typeof (string));
      table.Columns.Add("GroupBy", typeof (string));
      table.Columns.Add("SortBy", typeof (string));
      table.Columns.Add("FromDate", typeof (DateTime));
      table.Columns.Add("ToDate", typeof (DateTime));
      table.Columns.Add("DocumentFilterTypeString", typeof (string));
      table.Columns.Add("CancelledStatus", typeof (string));
      if (criteria == null)
        criteria = new StockReceiveRMReportingCriteria();
      DataRow row = table.NewRow();
      row["Criteria"] = (object) criteria.ReadableText;
      row["ShowCriteria"] = criteria.IsShowCriteria ? (object) "Yes" : (object) "No";
      row["GroupBy"] = (object) criteria.GroupBy;
      row["SortBy"] = (object) criteria.SortBy;
      if (criteria.DateFilter.Type == FilterType.ByRange)
      {
        row["FromDate"] = criteria.DateFilter.From == null || criteria.DateFilter.From.ToString().Length == 0 ? (object) DateTime.MinValue : (object) (DateTime) criteria.DateFilter.From;
        row["ToDate"] = criteria.DateFilter.To == null || criteria.DateFilter.To.ToString().Length == 0 ? (object) DateTime.MaxValue : (object) (DateTime) criteria.DateFilter.To;
      }
      else
      {
        row["FromDate"] = (object) DateTime.MinValue;
        row["ToDate"] = (object) DateTime.MaxValue;
      }
      row["DocumentFilterTypeString"] = (object) criteria.DocumentFilter.ToFilterTypeString();
      row["CancelledStatus"] = criteria.IsPrintCancelled != CancelledDocumentOption.All ? (criteria.IsPrintCancelled != CancelledDocumentOption.Cancelled ? (object) "Show Uncancelled" : (object) "Show Cancelled") : (object) "Show All";
      table.Rows.Add(row);
      dsReport.Tables.Add(table);
    }

    public static void CreateGroupIDAndSortIDColumn(StockReceiveRMReportingCriteria criteria, DataSet dsReport)
    {
      dsReport.Tables[0].Columns.Add("GroupID", typeof (string));
      dsReport.Tables[0].Columns.Add("GroupIDDisplay", typeof (string));
      dsReport.Tables[0].Columns.Add("GroupIDName", typeof (string));
      dsReport.Tables[0].Columns.Add("GroupIDDescription", typeof (string));
      dsReport.Tables[0].Columns.Add("SortID", typeof (string));
      foreach (DataRow dataRow1 in (InternalDataCollectionBase) dsReport.Tables[0].Rows)
      {
        if (criteria != null)
        {
          if (criteria.SortBy == ListingSortByOption.Date)
          {
            string str = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]).ToString("yyyyMMdd");
            dataRow1["SortID"] = (object) str;
          }
          else
            dataRow1["SortID"] = (object) dataRow1["DocNo"].ToString();
          int year;
          if (criteria.GroupBy == ListingGroupByOption.Date)
          {
            DateTime dateTime = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            dataRow1["GroupID"] = (object) BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]).ToString("yyyy/MM/dd");
            dataRow1["GroupIDDisplay"] = (object) BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]).ToString("dd/MM/yyyy");
            dataRow1["GroupIDName"] = (object) (Localizer.GetString((Enum) ListingGroupByOption.Date, new object[0]) + ":");
            dataRow1["GroupIDDescription"] = (object) (Localizer.GetString((Enum) StockStringId.GroupBy, new object[0]) + " " + dateTime.ToString("dd/MM/yyyy"));
            dataRow1.EndEdit();
          }
          else if (criteria.GroupBy == ListingGroupByOption.Month)
          {
            DateTime dateTime = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            string str1 = dateTime.ToString("yyyyMM");
            dataRow1["GroupID"] = (object) str1;
            DataRow dataRow2 = dataRow1;
            string index1 = "GroupIDDisplay";
            string str2 = Utils.NumberToMonth(dateTime.Month);
            string str3 = " ";
            year = dateTime.Year;
            string str4 = year.ToString();
            string str5 = str2 + str3 + str4;
            dataRow2[index1] = (object) str5;
            dataRow1["GroupIDName"] = (object) (Localizer.GetString((Enum) ListingGroupByOption.Month, new object[0]) + ":");
            DataRow dataRow3 = dataRow1;
            string index2 = "GroupIDDescription";
            string[] strArray = new string[5];
            int index3 = 0;
            string @string = Localizer.GetString((Enum) StockStringId.GroupBy, new object[0]);
            strArray[index3] = @string;
            int index4 = 1;
            string str6 = " ";
            strArray[index4] = str6;
            int index5 = 2;
            string str7 = Utils.NumberToMonth(dateTime.Month);
            strArray[index5] = str7;
            int index6 = 3;
            string str8 = " ";
            strArray[index6] = str8;
            int index7 = 4;
            year = dateTime.Year;
            string str9 = year.ToString();
            strArray[index7] = str9;
            string str10 = string.Concat(strArray);
            dataRow3[index2] = (object) str10;
            dataRow1.EndEdit();
          }
          else if (criteria.GroupBy == ListingGroupByOption.Year)
          {
            DateTime dateTime = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
            DataRow dataRow2 = dataRow1;
            string index1 = "GroupID";
            year = dateTime.Year;
            string str1 = year.ToString();
            dataRow2[index1] = (object) str1;
            dataRow1["GroupIDDisplay"] = dataRow1["GroupID"];
            dataRow1["GroupIDName"] = (object) (Localizer.GetString((Enum) ListingGroupByOption.Year, new object[0]) + ":");
            DataRow dataRow3 = dataRow1;
            string index2 = "GroupIDDescription";
            string @string = Localizer.GetString((Enum) StockStringId.GroupBy, new object[0]);
            string str2 = " ";
            year = dateTime.Year;
            string str3 = year.ToString();
            string str4 = @string + str2 + str3;
            dataRow3[index2] = (object) str4;
            dataRow1.EndEdit();
          }
        }
      }
    }
        protected bool PostToSR(StockReceiveRM stockissue, DataSet ds, DBSetting newDBSetting, StockReceiveRMAction myAction, bool canceldoc)
        {
            DataRow dataRow1 = ds.Tables["Master"].Rows[0];
            DataTable detailTable = ds.Tables["Detail"];
            // DataTable crewTable = ds.Tables["Crew"];
            // DataTable crewcatchTable = ds.Tables["CrewCatch"];
            //Int64 lJEKey = 0;
            //if (dataRow1["JEKey"] != null && dataRow1["JEKey"] != DBNull.Value)
            //{
            //    lJEKey = BCE.Data.Convert.ToInt64(dataRow1["JEKey"]);
            //}
            bool bexists = false;
            object obj = myDBSetting.ExecuteScalar("select count(*) from RCV with(NOLOCK) where DocNo=?", (object)stockissue.DocNo.ToString());
            if (obj != null && obj != DBNull.Value)
            {
                if (BCE.Data.Convert.ToDecimal(obj) > 0)
                    bexists = true;
            }
            DBSetting TransdbSetting = newDBSetting.StartTransaction();
            try
            {
                BCE.AutoCount.Stock.StockReceive.StockReceiveCommand srCommand = BCE.AutoCount.Stock.StockReceive.StockReceiveCommand.Create(TransdbSetting);


                if (myAction == StockReceiveRMAction.New || myAction == StockReceiveRMAction.Edit)
                {

                    BCE.AutoCount.Stock.StockReceive.StockReceive srentity;

                    if (bexists)
                    {
                        srentity = srCommand.Edit(stockissue.DocNo);
                        //srentity.DocNo = dataRow1["DocNo"].ToString();
                        srentity.DocDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
                        srentity.DataTableMaster.Rows[0]["Cancelled"] = dataRow1["Cancelled"];
                        srentity.DataTableMaster.Rows[0]["RefDocNo"] = dataRow1["RefDocNo"];
                        srentity.DataTableMaster.Rows[0]["Description"] = dataRow1["Description"];

                        srentity.UDF["JNSTRANS"] = "RM";

                        // jeentity.CurrencyCode = "IDR";
                        // jeentity.CurrencyRate = (decimal)1;
                        srentity.ClearDetails();
                        for (int i = 0; i < stockissue.GetValidDetailRows().Length; i++)
                        {
                            DataRow drDetail = stockissue.GetValidDetailRows()[i];
                            BCE.AutoCount.Stock.StockReceive.StockReceiveDetail dtlrecord = srentity.AddDetail();

                            dtlrecord.Row["ItemCode"] = drDetail["ItemCode"];
                            dtlrecord.Row["Description"] = drDetail["Description"];
                            dtlrecord.Row["BatchNo"] = drDetail["BatchNo"];
                            dtlrecord.Row["FurtherDescription"] = drDetail["FurtherDescription"];
                            dtlrecord.Row["ProjNo"] = drDetail["ProjNo"];
                            dtlrecord.Row["Location"] = drDetail["Location"];

                            // dTotalDR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                            dtlrecord.Row["UOM"] = drDetail["UOM"];
                            dtlrecord.Row["Qty"] = drDetail["Qty"];
                            dtlrecord.Row["UnitCost"] = drDetail["UnitCost"];
                            //dtlrecord.Row["SubTotal"] = drDetail["SubTotal"];
                            dtlrecord.UDF["Debet"] = drDetail["Debit"];
                            dtlrecord.UDF["Kredit"] = drDetail["Credit"];
                            dtlrecord.EndEdit();
                            // TransdbSetting.ExecuteNonQuery("insert into BG_TransOutstandingBP(TransKey,TransNo,TransDate,TransType,ProjNo,BucketTotal,PointTotal,Amount,TotalAmount,TransferAmt,Percentage,ReceiveCode,ReceiveName) values(?,?,?,?,?,?,?,?,?,?,?,?,?)", (object)dataRow1["Dockey"], (object)dataRow1["DocNo"], (object)dataRow1["DocDate"], (object)drgroup["TransType"], (object)drgroup["ProjNo"], (object)drgroup["BucketTotal"], (object)drgroup["PointTotal"], (object)drgroup["Amount"], (object)drgroup["TotalAmount"], (object)0, (object)drgroup["Percentage"], (object)drgroup["ReceiveCode"], (object)drgroup["ReceiveName"]);

                        }
                        srentity.Save(this.myUserAuthentication.LoginUserID);
                        //if(stockissue.RefDocNo!=null && stockissue.RefDocNo!=DBNull.Value)
                        
                    }
                    else
                    {
                        srentity = srCommand.AddNew();
                        srentity.DocNo = dataRow1["DocNo"].ToString();
                        srentity.DocDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
                        srentity.DataTableMaster.Rows[0]["Cancelled"] = dataRow1["Cancelled"];
                        srentity.DataTableMaster.Rows[0]["RefDocNo"] = dataRow1["RefDocNo"];
                        srentity.DataTableMaster.Rows[0]["Description"] = dataRow1["Description"];
                        srentity.UDF["JNSTRANS"] = "RM";

                        // jeentity.CurrencyCode = "IDR";
                        // jeentity.CurrencyRate = (decimal)1;
                        srentity.ClearDetails();
                        for (int i = 0; i < stockissue.GetValidDetailRows().Length; i++)
                        {
                            DataRow drDetail = stockissue.GetValidDetailRows()[i];
                            BCE.AutoCount.Stock.StockReceive.StockReceiveDetail dtlrecord = srentity.AddDetail();
                            dtlrecord.Row["ItemCode"] = drDetail["ItemCode"];
                            dtlrecord.Row["Description"] = drDetail["Description"];
                            dtlrecord.Row["BatchNo"] = drDetail["BatchNo"];
                            dtlrecord.Row["Location"] = drDetail["Location"];
                            // dtlrecord.SE
                            dtlrecord.Row["FurtherDescription"] = drDetail["FurtherDescription"];
                            dtlrecord.Row["ProjNo"] = drDetail["ProjNo"];
                            // dTotalDR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                            dtlrecord.Row["UOM"] = drDetail["UOM"];
                            dtlrecord.Row["Qty"] = drDetail["Qty"];
                            dtlrecord.Row["UnitCost"] = drDetail["UnitCost"];
                            //dtlrecord.Row["SubTotal"] = drDetail["SubTotal"];
                            dtlrecord.UDF["Debet"] = drDetail["Debit"];
                            dtlrecord.UDF["Kredit"] = drDetail["Credit"];
                            dtlrecord.EndEdit();
                            // TransdbSetting.ExecuteNonQuery("insert into BG_TransOutstandingBP(TransKey,TransNo,TransDate,TransType,ProjNo,BucketTotal,PointTotal,Amount,TotalAmount,TransferAmt,Percentage,ReceiveCode,ReceiveName) values(?,?,?,?,?,?,?,?,?,?,?,?,?)", (object)dataRow1["Dockey"], (object)dataRow1["DocNo"], (object)dataRow1["DocDate"], (object)drgroup["TransType"], (object)drgroup["ProjNo"], (object)drgroup["BucketTotal"], (object)drgroup["PointTotal"], (object)drgroup["Amount"], (object)drgroup["TotalAmount"], (object)0, (object)drgroup["Percentage"], (object)drgroup["ReceiveCode"], (object)drgroup["ReceiveName"]);

                        }
                        srentity.Save(this.myUserAuthentication.LoginUserID);
                    }
                }
                else
                {
                    if (BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
                        srCommand.CancelDocument(stockissue.DocKey, this.myUserAuthentication.LoginUserID);
                    else if (!BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
                        srCommand.UncancelDocument(stockissue.DocKey, this.myUserAuthentication.LoginUserID);

                }
                TransdbSetting.Commit();

            }
            catch (SqlException ex)
            {
                TransdbSetting.Rollback();
                BCE.Data.DataError.HandleSqlException(ex);
                return false;
            }
            finally
            {
                TransdbSetting.EndTransaction();
                // AccountBookControl.EnableTransactionCounter();
            }
            return true;
        }

        protected bool PostToJE(StockReceiveRM stockissue, DataSet ds, DBSetting newDBSetting, StockReceiveRMAction myAction,bool canceldoc)
        {
            DataRow dataRow1 = ds.Tables["Master"].Rows[0];
            DataTable detailTable = ds.Tables["Detail"];
            // DataTable crewTable = ds.Tables["Crew"];
            // DataTable crewcatchTable = ds.Tables["CrewCatch"];
            Int64 lJEKey = 0;
            if (dataRow1["JEKey"] != null && dataRow1["JEKey"] != DBNull.Value)
            {
                lJEKey = BCE.Data.Convert.ToInt64(dataRow1["JEKey"]);
            }

            DBSetting TransdbSetting = newDBSetting.StartTransaction();
            try
            {
               
                    BCE.AutoCount.GL.JournalEntry.JournalEntryCommand jeCmd = BCE.AutoCount.GL.JournalEntry.JournalEntryCommand.Create(TransdbSetting);
                    if (myAction == StockReceiveRMAction.New || myAction == StockReceiveRMAction.Edit)
                    {
                        BCE.AutoCount.GL.JournalEntry.JournalEntry jeentity = jeCmd.Edit(lJEKey);
                        if (jeentity != null)
                        {
                        jeentity.Description = dataRow1["Description"] != null ? dataRow1["Description"].ToString() : "";
                        jeentity.DocDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);
                            jeentity.MasterRow["Cancelled"] = dataRow1["Cancelled"];
                            // jeentity.CurrencyCode = "IDR";
                            // jeentity.CurrencyRate = (decimal)1;
                            jeentity.ClearDetails();
                            for (int i = 0; i < stockissue.GetValidDetailRows().Length; i++)
                            {
                                DataRow drDetail = stockissue.GetValidDetailRows()[i];
                             
                                        //Add DR
                                        BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordDR = jeentity.AddDetail();
                                        dtlrecordDR.AccNo =BCE.Data.Convert.ToDBString(drDetail["Debit"]);
                            if (drDetail["ProjNo"].ToString() != "")
                                dtlrecordDR.ProjNo = drDetail["ProjNo"].ToString();
                            dtlrecordDR.DR = BCE.Data.Convert.ToDecimal(drDetail["SubTotal"]);
                                        // dTotalDR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                                        dtlrecordDR.RefNo2 = dataRow1["DocNo"].ToString();
                                        dtlrecordDR.EndEdit();
                                        //Add CR
                                        BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordCR = jeentity.AddDetail();
                                        dtlrecordCR.AccNo = BCE.Data.Convert.ToDBString(drDetail["Credit"]);
                            if (drDetail["ProjNo"].ToString() != "")
                                dtlrecordCR.ProjNo = drDetail["ProjNo"].ToString();
                            dtlrecordCR.CR = BCE.Data.Convert.ToDecimal(drDetail["SubTotal"]);
                            //dTotalCR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                            dtlrecordCR.RefNo2 = dataRow1["DocNo"].ToString();
                                        dtlrecordCR.EndEdit();
                                       // TransdbSetting.ExecuteNonQuery("insert into BG_TransOutstandingBP(TransKey,TransNo,TransDate,TransType,ProjNo,BucketTotal,PointTotal,Amount,TotalAmount,TransferAmt,Percentage,ReceiveCode,ReceiveName) values(?,?,?,?,?,?,?,?,?,?,?,?,?)", (object)dataRow1["Dockey"], (object)dataRow1["DocNo"], (object)dataRow1["DocDate"], (object)drgroup["TransType"], (object)drgroup["ProjNo"], (object)drgroup["BucketTotal"], (object)drgroup["PointTotal"], (object)drgroup["Amount"], (object)drgroup["TotalAmount"], (object)0, (object)drgroup["Percentage"], (object)drgroup["ReceiveCode"], (object)drgroup["ReceiveName"]);

                            }

                            jeentity.Save(this.myUserAuthentication.LoginUserID);
                            dataRow1["JEKey"] = jeentity.DocKey;
                            dataRow1["JENo"] = jeentity.DocNo;
                        }
                        else
                        {
                            jeentity = jeCmd.AddNew();
                        jeentity.DocNo = dataRow1["DocNo"].ToString();
                        //  jeentity.CurrencyCode = "IDR";
                        // jeentity.CurrencyRate = (decimal)1;
                        jeentity.Description = dataRow1["Description"] != null ? dataRow1["Description"].ToString() : "";
                        jeentity.DocDate = BCE.Data.Convert.ToDateTime(dataRow1["DocDate"]);

                            jeentity.MasterRow["Cancelled"] = dataRow1["Cancelled"];
                            decimal dTotalDR = 0;
                            decimal dTotalCR = 0;
                          //  BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordDR;
                        for (int i = 0; i < stockissue.GetValidDetailRows().Length; i++)
                        {
                            DataRow drDetail = stockissue.GetValidDetailRows()[i];

                            //Add DR
                            BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordDR = jeentity.AddDetail();
                            dtlrecordDR.AccNo = BCE.Data.Convert.ToDBString(drDetail["Debit"]);
                            if(drDetail["ProjNo"].ToString()!="")
                                 dtlrecordDR.ProjNo = drDetail["ProjNo"].ToString();
                            dtlrecordDR.DR = BCE.Data.Convert.ToDecimal(drDetail["SubTotal"]);
                            // dTotalDR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                            dtlrecordDR.RefNo2 = dataRow1["DocNo"].ToString();
                            dtlrecordDR.EndEdit();
                            //Add CR
                            BCE.AutoCount.GL.JournalEntry.JournalEntryDetail dtlrecordCR = jeentity.AddDetail();
                            dtlrecordCR.AccNo = BCE.Data.Convert.ToDBString(drDetail["Credit"]);
                            if (drDetail["ProjNo"].ToString() != "")
                                dtlrecordCR.ProjNo = drDetail["ProjNo"].ToString();
                            dtlrecordCR.CR = BCE.Data.Convert.ToDecimal(drDetail["SubTotal"]);
                            //dTotalCR += BCE.Data.Convert.ToDecimal(drgroup["Amount"]);
                            dtlrecordCR.RefNo2 = dataRow1["DocNo"].ToString();
                            dtlrecordCR.EndEdit();
                            // TransdbSetting.ExecuteNonQuery("insert into BG_TransOutstandingBP(TransKey,TransNo,TransDate,TransType,ProjNo,BucketTotal,PointTotal,Amount,TotalAmount,TransferAmt,Percentage,ReceiveCode,ReceiveName) values(?,?,?,?,?,?,?,?,?,?,?,?,?)", (object)dataRow1["Dockey"], (object)dataRow1["DocNo"], (object)dataRow1["DocDate"], (object)drgroup["TransType"], (object)drgroup["ProjNo"], (object)drgroup["BucketTotal"], (object)drgroup["PointTotal"], (object)drgroup["Amount"], (object)drgroup["TotalAmount"], (object)0, (object)drgroup["Percentage"], (object)drgroup["ReceiveCode"], (object)drgroup["ReceiveName"]);



                        }

                        // dTotalDR = dTotalDR*1;
                        //  dTotalCR = dTotalCR*1;
                        jeentity.Save(this.myUserAuthentication.LoginUserID);


                            dataRow1["JEKey"] = jeentity.DocKey;
                            dataRow1["JENo"] = jeentity.DocNo;
                        }
                    }
                    else
                    {
                    if (BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
                        jeCmd.CancelDocument(lJEKey, this.myUserAuthentication.LoginUserID);
                    else if (!BCE.Data.Convert.TextToBoolean(dataRow1["Cancelled"]))
                        jeCmd.UncancelDocument(lJEKey, this.myUserAuthentication.LoginUserID);

                }
                    TransdbSetting.Commit();
                
            }
            catch (SqlException ex)
            {
                TransdbSetting.Rollback();
                BCE.Data.DataError.HandleSqlException(ex);
                return false;
            }
            finally
            {
                TransdbSetting.EndTransaction();
                // AccountBookControl.EnableTransactionCounter();
            }
            return true;
        }

        public static DataTable GetItemAnalysisSummaryTable(DBSetting dbSetting, string docKeys, string tableName)
    {
      string str1 = string.Format("SELECT A.ItemCode, A.UOM, B.Description AS ItemDescription, C.Rate, SUM(A.Qty) AS Qty, SUM(A.Subtotal) AS Amount FROM {0} A, Item B, ItemUOM C WHERE A.DocKey IN (SELECT * FROM LIST(@DocKeyList)) AND A.ItemCode=B.ItemCode AND A.ItemCode=C.ItemCode AND A.UOM=C.UOM Group By A.ItemCode, A.UOM, B.Description, C.Rate ORDER BY A.ItemCode, A.UOM", (object) tableName);
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray[index] = (object) sqlParameter;
      DataTable dataTable = dbSetting1.GetDataTable(cmdText, num != 0, objArray);
      string str2 = "Item Code Analysis";
      dataTable.TableName = str2;
      return dataTable;
    }

    public static DataTable GetDateAnalysisSummaryTable(DBSetting dbSetting, string docKeys, string masterTableName, string detailTableName)
    {
      string str1 = string.Format("SELECT convert(nvarchar(10),A.DocDate,112) AS Date, SUM(D.Qty) AS Qty, SUM(A.Total) AS Amount FROM {0} A LEFT OUTER JOIN (SELECT B.DocKey, SUM(B.Qty) AS Qty FROM {1} B WHERE B.ItemCode IS NOT NULL Group By B.DocKey) AS D ON A.DocKey = D.DocKey WHERE A.DocKey IN (SELECT * FROM LIST(@DocKeyList)) Group By A.DocDate ", (object) masterTableName, (object) detailTableName);
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray[index] = (object) sqlParameter;
      DataTable dataTable = dbSetting1.GetDataTable(cmdText, num != 0, objArray);
      string str2 = "Analysis by Date";
      dataTable.TableName = str2;
      return dataTable;
    }

    public static DataTable GetMonthAnalysisSummaryTable(DBSetting dbSetting, string docKeys, string masterTableName, string detailTableName)
    {
      string str1 = string.Format("SELECT DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4)) AS Month, SUM(D.Qty) AS Qty, SUM(A.Total) AS Amount FROM {0} A LEFT OUTER JOIN (SELECT B.DocKey, SUM(B.Qty) AS Qty FROM {1} B WHERE B.ItemCode IS NOT NULL Group By B.DocKey) AS D ON A.DocKey = D.DocKey WHERE A.DocKey IN (SELECT * FROM LIST(@DocKeyList)) Group By DateName(mm,A.DocDate) + ' '+ cast(year(A.DocDate) as varchar(4))", (object) masterTableName, (object) detailTableName);
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray[index] = (object) sqlParameter;
      DataTable dataTable = dbSetting1.GetDataTable(cmdText, num != 0, objArray);
      string str2 = "Analysis by Month";
      dataTable.TableName = str2;
      return dataTable;
    }

    public static DataTable GetYearAnalysisSummaryTable(DBSetting dbSetting, string docKeys, string masterTableName, string detailTableName)
    {
      string str1 = string.Format("SELECT Year(A.DocDate) AS Year, SUM(D.Qty) AS Qty, SUM(A.Total) AS Amount FROM {0} A LEFT OUTER JOIN (SELECT B.DocKey, SUM(B.Qty) AS Qty FROM {1} B WHERE B.ItemCode IS NOT NULL Group By B.DocKey) AS D ON A.DocKey = D.DocKey WHERE A.DocKey IN (SELECT * FROM LIST(@DocKeyList)) Group By Year(A.DocDate) ", (object) masterTableName, (object) detailTableName);
      DBSetting dbSetting1 = dbSetting;
      string cmdText = str1;
      int num = 0;
      object[] objArray = new object[1];
      int index = 0;
      SqlParameter sqlParameter = new SqlParameter("@DocKeyList", (object) docKeys);
      objArray[index] = (object) sqlParameter;
      DataTable dataTable = dbSetting1.GetDataTable(cmdText, num != 0, objArray);
      string str2 = "Analysis by Year";
      dataTable.TableName = str2;
      return dataTable;
    }
  }
}
