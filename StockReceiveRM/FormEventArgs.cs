﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.FormEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Document;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using System;
using System.Data;

namespace Production.StockReceiveRM
{
    [Obsolete("Use FormStockReceiveRMEntry.FormEventArgs instead")]
    public class FormStockReceiveRMEventArgs
    {
        private StockReceiveRM myStockReceiveRM;
        private PanelControl myHeaderPanel;
        private GridControl myGridControl;
        private XtraTabControl myTabControl;

        public PanelControl HeaderPanel
        {
            get
            {
                return this.myHeaderPanel;
            }
        }

        public GridControl GridControl
        {
            get
            {
                return this.myGridControl;
            }
        }

        public XtraTabControl TabControl
        {
            get
            {
                return this.myTabControl;
            }
        }

        public StockReceiveRM StockReceiveRM
        {
            get
            {
                return this.myStockReceiveRM;
            }
        }

        public DataTable MasterTable
        {
            get
            {
                return this.myStockReceiveRM.DataTableMaster;
            }
        }

        public DataTable DetailTable
        {
            get
            {
                return this.myStockReceiveRM.DataTableDetail;
            }
        }

        public EditWindowMode EditWindowMode
        {
            get
            {
                if (this.myStockReceiveRM.Action == StockReceiveRMAction.New)
                    return EditWindowMode.New;
                else if (this.myStockReceiveRM.Action == StockReceiveRMAction.Edit)
                    return EditWindowMode.Edit;
                else
                    return EditWindowMode.View;
            }
        }

        public FormStockReceiveRMEventArgs(StockReceiveRM doc, PanelControl headerPanel, GridControl gridControl, XtraTabControl tabControl)
        {
            this.myStockReceiveRM = doc;
            this.myHeaderPanel = headerPanel;
            this.myGridControl = gridControl;
            this.myTabControl = tabControl;
        }
    }
}
