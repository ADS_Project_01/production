﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMEventArgs
  {
    private StockReceiveRM myStock;

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.DBSetting;
      }
    }

    public StockReceiveRMRecord MasterRecord
    {
      get
      {
        return new StockReceiveRMRecord(this.myStock);
      }
    }

    internal StockReceiveRMEventArgs(StockReceiveRM doc)
    {
      this.myStock = doc;
    }
  }
}
