﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMReportTypeHandler
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.Stock;
using BCE.Data;

namespace Production.StockReceiveRM
{
  public class StockReceiveRMReportTypeHandler : StockDocumentReportTypeHandler
  {
    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      return StockReceiveRMCommand.Create(dbSetting).GetReportDesignerDataSource();
    }
  }
}
