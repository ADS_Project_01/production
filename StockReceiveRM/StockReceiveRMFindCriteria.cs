﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMFindCriteria
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMFindCriteria
  {
    public bool Description;
    public bool DocNo;
    public bool DocDate;
    public bool RefDocNo;
    public bool UDF;
    public bool DetailUDF;
  }
}
