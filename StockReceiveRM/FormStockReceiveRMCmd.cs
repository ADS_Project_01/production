﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.FormStockReceiveRMCmd
// Assembly: BCE.AutoCount.Stock, Version=1.5.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.RegistryID.CommandFormStartupID;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.Stock;
using BCE.AutoCount.XtraUtils;
using BCE.Controls;
using BCE.Data;
using BCE.Localization;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BCE.AutoCount.Authentication;
namespace Production.StockReceiveRM
{
    //[BCE.AutoCount.PlugIns.PlugInsEntryPoint("Stock Return Raw Material", 8, true, "PRPLUGINS_MR_OPEN", "PRPLUGINS_MR_SHOW")]
    [BCE.AutoCount.PlugIn.MenuItem("Stock Return Raw Material",5, false, "RPA_RCVRM_SHOW", "RPA_RCVRM_OPEN")]

    [SingleInstanceThreadForm]
    public class FormStockReceiveRMCmd : XtraForm
    {
        protected UserAuthentication myUserAuthentication;
        private AsyncDataSetUpdateDelegate myUpdateDataSetDelegate;
       // private ScriptObject myScriptObject;
        //private ScriptObject myScriptObject = ScriptManager.CreateObject("StockReceiveRMCommandRMForm");
        private StockReceiveRMCommand myStockReceiveRMCmd;
        private PanelControl panelGrid;
        private PanelControl panelCmd;
        private Label label1;
        private DBSetting myDBSetting;
        private SimpleButton sbtnView;
        private SimpleButton sbtnDel;
        private SimpleButton sbtnEdit;
        private SimpleButton sbtnRefresh;
        private CheckEdit chkEdtShowAtStartup;
        private Label lblShowAll;
        private DocumentCommandFormController myCommandFormController;
        private BarManager barManager1;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private Bar bar1;
        private HyperLinkEdit lblNew;
        private HyperLinkEdit lblPrint;
        private PanelControl panelControl1;
        private HyperLinkEdit lblFind;
        private PrintButton printButton;
        private PreviewButton previewButton;
        private IContainer components;
        private PanelHeader panelHeader1;
        private BarButtonItem barButtonItem1;
        private BarButtonItem barButtonItem2;
        private StockReceiveRMGrid stockReceiveRMGrid1;
        private BarButtonItem barButtonItem3;
        //private StockReceiveRMGrid stockReceiveRMGrid1;

        public FormStockReceiveRMCmd(DBSetting dbSetting)
        {
            this.InitializeComponent();
           // this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockReceiveRMCommandRMForm");
            this.myDBSetting = dbSetting;
            this.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
            this.myStockReceiveRMCmd = StockReceiveRMCommand.Create(this.myDBSetting);
            StockReceiveRMCommand.DataSetUpdate.Add(this.myDBSetting, this.myUpdateDataSetDelegate = new AsyncDataSetUpdateDelegate(this.UpdateDataSet));
           // this.stockReceiveRMGrid1 = new Production.StockReceiveRM.StockReceiveRMGrid();
            this.stockReceiveRMGrid1.Initialize(this.myDBSetting);
            this.previewButton.ReportType = "Stock Return Raw Material Document";
            this.previewButton.SetDBSetting(this.myDBSetting);
            this.printButton.ReportType = "Stock Return Raw Material Document";
            this.printButton.SetDBSetting(this.myDBSetting);
            this.InitCommandFormController(this.barManager1);
            BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Receive.htm");
            DBSetting dbSetting1 = this.myDBSetting;
            string docType = "RV";
            long docKey = 0L;
            long eventKey = 0L;
            // ISSUE: variable of a boxed type
            StockReceiveRMString local = StockReceiveRMString.OpenedStockReceiveRMWindow;
            object[] objArray = new object[1];
            int index = 0;
            string loginUserId = this.myUserAuthentication.LoginUserID;
            objArray[index] = (object)loginUserId;
            string @string = BCE.Localization.Localizer.GetString(local, objArray);
            string detail = "";
            Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
        }
        private void UpdateDataSet(DataSet ds, AsyncDataSetUpdateAction action)
        {
            DataTable dataTable1 = (DataTable)this.stockReceiveRMGrid1.DataSource;
            if (dataTable1.PrimaryKey.Length != 0)
            {
                DataTable dataTable2 = ds.Tables["Master"];
                long num = BCE.Data.Convert.ToInt64(dataTable2.Rows[0]["DocKey"]);
                DataRow row = dataTable1.Rows.Find((object)num);
                if (action == AsyncDataSetUpdateAction.Update)
                {
                    if (row == null)
                        row = dataTable1.NewRow();
                    foreach (DataColumn index1 in (InternalDataCollectionBase)row.Table.Columns)
                    {
                        int index2 = dataTable2.Columns.IndexOf(index1.ColumnName);
                        if (index2 >= 0)
                            row[index1] = dataTable2.Rows[0][index2];
                    }
                    row.EndEdit();
                    if (row.RowState == DataRowState.Detached)
                        dataTable1.Rows.Add(row);
                    for (int rowHandle = 0; rowHandle < this.stockReceiveRMGrid1.GridView.RowCount; ++rowHandle)
                    {
                        DataRow dataRow = this.stockReceiveRMGrid1.GridView.GetDataRow(rowHandle);
                        if (dataRow != null && BCE.Data.Convert.ToInt64(dataRow["DocKey"]) == num)
                        {
                            this.stockReceiveRMGrid1.GridView.FocusedRowHandle = rowHandle;
                            break;
                        }
                    }
                }
                else
                {
                    if (row != null)
                        row.Delete();
                    dataTable1.AcceptChanges();
                }
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitCommandFormController(BarManager barManager)
        {
            this.myCommandFormController = new DocumentCommandFormController(this.myDBSetting, (Form)this, barManager, true);
            this.myCommandFormController.NewDocumentEvent = new MethodInvoker(this.NewDocument);
            this.myCommandFormController.EditDocumentEvent = new MethodInvoker(this.EditDocument);
            this.myCommandFormController.ViewDocumentEvent = new MethodInvoker(this.ViewDocument);
            this.myCommandFormController.DeleteDocumentEvent = new MethodInvoker(this.DeleteDocument);
            this.myCommandFormController.PreviewWithDefaultReportEvent = new PrintWithDefaultReportMethodInvoker(this.PreviewDocument);
            this.myCommandFormController.PrintWithDefaultReportEvent = new PrintWithDefaultReportMethodInvoker(this.PrintDocument);
            this.myCommandFormController.FindEvent = new MethodInvoker(this.Find);
            this.myCommandFormController.GeneralPrintEvent = new MethodInvoker(this.GeneralPrint);
            this.myCommandFormController.RefreshGridEvent = new MethodInvoker(this.RefreshGrid);
            this.myCommandFormController.ShowAllRecordsEvent = new ShowAllRecordsDelegate(this.ShowAllRecords);
            this.myCommandFormController.DesignDocumentStyleReportEvent = new MethodInvoker(this.DesignDocumentStyleReport);
            this.myCommandFormController.DesignListingStyleReportEvent = new MethodInvoker(this.DesignDocumentListingStyleReport);
            this.myCommandFormController.DesignDetailListingReportEvent = new MethodInvoker(this.DesignDetailListingReport);
            this.myCommandFormController.ReportOptionEvent = new MethodInvoker(this.ReportOption);
            this.myCommandFormController.SetNewControl((Control)this.lblNew);
            this.myCommandFormController.SetFindControl((Control)this.lblFind);
            this.myCommandFormController.SetPrintControl((Control)this.lblPrint);
            this.myCommandFormController.SetShowAllControl((Control)this.lblShowAll);
            this.myCommandFormController.SetMainGridView(this.stockReceiveRMGrid1.GridView);
            this.myCommandFormController.SetEditDocumentControl((Control)this.sbtnEdit);
            this.myCommandFormController.SetViewDocumentControl((Control)this.sbtnView);
            this.myCommandFormController.SetDeleteDocumentControl((Control)this.sbtnDel);
            this.myCommandFormController.SetPreviewWithDefaultReportControl(this.previewButton);
            this.myCommandFormController.SetPrintWithDefaultReportControl(this.printButton);
            this.myCommandFormController.SetRefreshGridControl((Control)this.sbtnRefresh);
            Utils.SetHottrackControls(new Control[4]
      {
        (Control) this.lblNew,
        (Control) this.lblFind,
        (Control) this.lblPrint,
        (Control) this.lblShowAll
      });
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelGrid = new DevExpress.XtraEditors.PanelControl();
            this.stockReceiveRMGrid1 = new Production.StockReceiveRM.StockReceiveRMGrid();
            this.printButton = new BCE.AutoCount.Controls.PrintButton();
            this.previewButton = new BCE.AutoCount.Controls.PreviewButton();
            this.chkEdtShowAtStartup = new DevExpress.XtraEditors.CheckEdit();
            this.sbtnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnView = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.panelCmd = new DevExpress.XtraEditors.PanelControl();
            this.lblFind = new DevExpress.XtraEditors.HyperLinkEdit();
            this.lblPrint = new DevExpress.XtraEditors.HyperLinkEdit();
            this.lblNew = new DevExpress.XtraEditors.HyperLinkEdit();
            this.lblShowAll = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelHeader1 = new BCE.AutoCount.Controls.PanelHeader();
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).BeginInit();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEdtShowAtStartup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCmd)).BeginInit();
            this.panelCmd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblFind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelGrid
            // 
            this.panelGrid.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelGrid.Controls.Add(this.stockReceiveRMGrid1);
            this.panelGrid.Controls.Add(this.printButton);
            this.panelGrid.Controls.Add(this.previewButton);
            this.panelGrid.Controls.Add(this.chkEdtShowAtStartup);
            this.panelGrid.Controls.Add(this.sbtnRefresh);
            this.panelGrid.Controls.Add(this.sbtnView);
            this.panelGrid.Controls.Add(this.sbtnDel);
            this.panelGrid.Controls.Add(this.sbtnEdit);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 0);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(1032, 409);
            this.panelGrid.TabIndex = 0;
            // 
            // stockReceiveRMGrid1
            // 
            this.stockReceiveRMGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stockReceiveRMGrid1.Location = new System.Drawing.Point(2, 36);
            this.stockReceiveRMGrid1.Name = "stockReceiveRMGrid1";
            this.stockReceiveRMGrid1.Size = new System.Drawing.Size(1029, 373);
            this.stockReceiveRMGrid1.TabIndex = 8;
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(225, 6);
            this.printButton.Name = "printButton";
            this.printButton.ReportType = "";
            this.printButton.Size = new System.Drawing.Size(72, 24);
            this.printButton.TabIndex = 1;
            this.printButton.Print += new BCE.AutoCount.Controls.PrintEventHandler(this.printButton_Print);
            // 
            // previewButton
            // 
            this.previewButton.Location = new System.Drawing.Point(153, 6);
            this.previewButton.Name = "previewButton";
            this.previewButton.ReportType = "";
            this.previewButton.Size = new System.Drawing.Size(72, 24);
            this.previewButton.TabIndex = 2;
            this.previewButton.Preview += new BCE.AutoCount.Controls.PrintEventHandler(this.previewButton_Preview);
            // 
            // chkEdtShowAtStartup
            // 
            this.chkEdtShowAtStartup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkEdtShowAtStartup.Location = new System.Drawing.Point(870, 11);
            this.chkEdtShowAtStartup.Name = "chkEdtShowAtStartup";
            this.chkEdtShowAtStartup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.chkEdtShowAtStartup.Properties.Appearance.Options.UseBackColor = true;
            this.chkEdtShowAtStartup.Properties.Caption = "Show this grid At Startup";
            this.chkEdtShowAtStartup.Size = new System.Drawing.Size(150, 19);
            this.chkEdtShowAtStartup.TabIndex = 3;
            this.chkEdtShowAtStartup.CheckedChanged += new System.EventHandler(this.chkEdtShowAtStartup_CheckedChanged);
            // 
            // sbtnRefresh
            // 
            this.sbtnRefresh.Location = new System.Drawing.Point(372, 6);
            this.sbtnRefresh.Name = "sbtnRefresh";
            this.sbtnRefresh.Size = new System.Drawing.Size(75, 24);
            this.sbtnRefresh.TabIndex = 4;
            this.sbtnRefresh.Text = "Refresh";
            this.sbtnRefresh.Click += new System.EventHandler(this.sbtnRefresh_Click);
            // 
            // sbtnView
            // 
            this.sbtnView.Location = new System.Drawing.Point(78, 6);
            this.sbtnView.Name = "sbtnView";
            this.sbtnView.Size = new System.Drawing.Size(75, 24);
            this.sbtnView.TabIndex = 5;
            this.sbtnView.Text = "View";
            this.sbtnView.Click += new System.EventHandler(this.sbtnView_Click);
            // 
            // sbtnDel
            // 
            this.sbtnDel.Appearance.ForeColor = System.Drawing.Color.Red;
            this.sbtnDel.Appearance.Options.UseForeColor = true;
            this.sbtnDel.Location = new System.Drawing.Point(297, 6);
            this.sbtnDel.Name = "sbtnDel";
            this.sbtnDel.Size = new System.Drawing.Size(75, 24);
            this.sbtnDel.TabIndex = 6;
            this.sbtnDel.Text = "Delete";
            this.sbtnDel.Click += new System.EventHandler(this.sbtnDel_Click);
            // 
            // sbtnEdit
            // 
            this.sbtnEdit.Location = new System.Drawing.Point(3, 6);
            this.sbtnEdit.Name = "sbtnEdit";
            this.sbtnEdit.Size = new System.Drawing.Size(75, 24);
            this.sbtnEdit.TabIndex = 7;
            this.sbtnEdit.Text = "Edit";
            this.sbtnEdit.Click += new System.EventHandler(this.sbtnEdit_Click);
            // 
            // panelCmd
            // 
            this.panelCmd.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCmd.Controls.Add(this.lblFind);
            this.panelCmd.Controls.Add(this.lblPrint);
            this.panelCmd.Controls.Add(this.lblNew);
            this.panelCmd.Controls.Add(this.lblShowAll);
            this.panelCmd.Controls.Add(this.label1);
            this.panelCmd.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCmd.Location = new System.Drawing.Point(0, 66);
            this.panelCmd.Name = "panelCmd";
            this.panelCmd.Size = new System.Drawing.Size(1032, 100);
            this.panelCmd.TabIndex = 5;
            this.panelCmd.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCmd_Paint);
            // 
            // lblFind
            // 
            this.lblFind.EditValue = "Find Stock Return Raw Material";
            this.lblFind.Location = new System.Drawing.Point(368, 34);
            this.lblFind.Name = "lblFind";
            this.lblFind.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblFind.Properties.Appearance.Options.UseBackColor = true;
            this.lblFind.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblFind.Properties.LinkColor = System.Drawing.Color.Black;
            this.lblFind.Size = new System.Drawing.Size(160, 18);
            this.lblFind.TabIndex = 0;
            this.lblFind.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.lblFind_OpenLink);
            // 
            // lblPrint
            // 
            this.lblPrint.EditValue = "Print Stock Return Raw Material Listing";
            this.lblPrint.Location = new System.Drawing.Point(622, 34);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblPrint.Properties.Appearance.Options.UseBackColor = true;
            this.lblPrint.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblPrint.Properties.LinkColor = System.Drawing.Color.Black;
            this.lblPrint.Size = new System.Drawing.Size(195, 18);
            this.lblPrint.TabIndex = 1;
            this.lblPrint.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.lblPrint_OpenLink);
            // 
            // lblNew
            // 
            this.lblNew.EditValue = "Create New Stock Return Raw Material";
            this.lblNew.Location = new System.Drawing.Point(78, 34);
            this.lblNew.Name = "lblNew";
            this.lblNew.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblNew.Properties.Appearance.Options.UseBackColor = true;
            this.lblNew.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblNew.Properties.LinkColor = System.Drawing.Color.DarkGoldenrod;
            this.lblNew.Size = new System.Drawing.Size(219, 18);
            this.lblNew.TabIndex = 2;
            this.lblNew.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.lblNew_OpenLink);
            // 
            // lblShowAll
            // 
            this.lblShowAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblShowAll.ForeColor = System.Drawing.Color.Black;
            this.lblShowAll.Location = new System.Drawing.Point(15, 76);
            this.lblShowAll.Name = "lblShowAll";
            this.lblShowAll.Size = new System.Drawing.Size(809, 16);
            this.lblShowAll.TabIndex = 3;
            this.lblShowAll.Text = "atau anda dapat memunculkan daftar Stock Return Raw Material, kemudian cari doku" +
    "men yang anda inginkan untuk anda lakukan sesuatu ";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(115)))), ((int)(((byte)(250)))));
            this.label1.Location = new System.Drawing.Point(15, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "You Can :";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 5;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1032, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 575);
            this.barDockControlBottom.Size = new System.Drawing.Size(1032, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 553);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1032, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 553);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Design Document Style Report";
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick_1);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Design Listing Style Report";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Desing Detail Listing Report";
            this.barButtonItem3.Id = 4;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panelGrid);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 166);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1032, 409);
            this.panelControl1.TabIndex = 4;
            // 
            // panelHeader1
            // 
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader1.Header = "Stock Return Raw Material";
            this.panelHeader1.HelpTopicId = "Stock_Receive.htm";
            this.panelHeader1.Location = new System.Drawing.Point(0, 22);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1032, 44);
            this.panelHeader1.TabIndex = 6;
            this.panelHeader1.Load += new System.EventHandler(this.panelHeader1_Load);
            // 
            // FormStockReceiveRMCmd
            // 
            this.ClientSize = new System.Drawing.Size(1032, 575);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelCmd);
            this.Controls.Add(this.panelHeader1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = "FormStockReceiveRMCmd";
            this.ShowInTaskbar = false;
            this.Text = "Stock Return Raw Material";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormStockReceiveRMCmd_FormClosed);
            this.Load += new System.EventHandler(this.FormItemCmd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).EndInit();
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEdtShowAtStartup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCmd)).EndInit();
            this.panelCmd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblFind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        public static void StartEntryForm(StockReceiveRM stockReceive, DBSetting dbSetting, DataTable searchTable)
        {
            new ThreadForm(typeof(FormStockReceiveRMEntry).AssemblyQualifiedName, new System.Type[3]
      {
        stockReceive.GetType(),
        dbSetting.GetType(),
        new DataTable().GetType()
      }, new object[3]
      {
        (object) stockReceive,
        (object) dbSetting,
        (object) searchTable
      }).Show();
        }
        public static void StartEntryListingForm(StockReceiveRM stockReceive, DBSetting dbSetting, DataTable searchTable)
        {
            new ThreadForm(typeof(FormStockReceiveRMPrintListing).AssemblyQualifiedName, new System.Type[3]
      {
        stockReceive.GetType(),
        dbSetting.GetType(),
        new DataTable().GetType()
      }, new object[3]
      {
        (object) stockReceive,
        (object) dbSetting,
        (object) searchTable
      }).Show();
        }
        private void FormItemCmd_Load(object sender, EventArgs e)
        {
            new CustomizeGridLayout(this.myDBSetting, this.Name, this.stockReceiveRMGrid1.GridView, new EventHandler(this.ReloadAllColumns)).PageHeader = BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.Code_StockReceiveListing, new object[0]);
            foreach (GridColumn gridColumn in (CollectionBase)this.stockReceiveRMGrid1.GridView.Columns)
                gridColumn.OptionsColumn.AllowEdit = false;
            this.LoadCommandFormStartupState();
        }

        private void LoadCommandFormStartupState()
        {
            bool boolean = true;//DBRegistry.Create(this.myDBSetting).GetBoolean((IRegistryID)new StockReceiveRMCommandRMFormStartupID());
            this.chkEdtShowAtStartup.Checked = boolean;
            this.FormInitialize();
            this.myCommandFormController.ShowAll(boolean);
        }

        private void SaveCommandFormStartupState()
        {
            //DBRegistry dbRegistry = DBRegistry.Create(this.myDBSetting);
            //StockReceiveRMCommandRMFormStartupID commandFormStartupId = new StockReceiveRMCommandRMFormStartupID();
            //commandFormStartupId.NewValue = (object)System.Convert.ToBoolean(this.chkEdtShowAtStartup.Checked ? 1 : 0);
            //dbRegistry.SetValue((IRegistryID)commandFormStartupId);
        }

        private long GetSelectedDocKey(ref string docNo)
        {
            if (this.stockReceiveRMGrid1.GridView.FocusedRowHandle >= 0)
            {
                DataRow dataRow = this.stockReceiveRMGrid1.GridView.GetDataRow(this.stockReceiveRMGrid1.GridView.FocusedRowHandle);
                if (dataRow != null)
                {
                    long num = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
                    docNo = dataRow.Table.Columns.IndexOf("DocNo") < 0 ? "(Internal DocKey=" + num.ToString() + ")" : dataRow["DocNo"].ToString();
                    return num;
                }
                else
                    return -1L;
            }
            else
            {
                AppMessage.ShowMessage((Form)this, BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.ShowMessage_RecordNotSelected, new object[0]));
            return -1L;
            }
        }

        private void View(long docKey, DataTable searchTable)
        {
            try
            {
                StockReceiveRM stockReceive = this.myStockReceiveRMCmd.View(docKey);
                if (stockReceive == null)
                    AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.ErrorMessage_RecordDoesNotExist, new object[0]));
                else
                    FormStockReceiveRMCmd.StartEntryForm(stockReceive);
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
        }

        private void Edit(long docKey, DataTable searchTable)
        {
            try
            {
                StockReceiveRM stockReceive = this.myStockReceiveRMCmd.Edit(docKey);
                if (stockReceive == null)
                    AppMessage.ShowMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.ErrorMessage_RecordDoesNotExist, new object[0]));
                else
                    FormStockReceiveRMCmd.StartEntryForm(stockReceive);
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
            }
        }

        private void SearchAll(bool allColumns)
        {
            bool hasYearMonth = false;
            foreach (GridColumn gridColumn in (CollectionBase)this.stockReceiveRMGrid1.GridView.Columns)
            {
                if ((gridColumn.FieldName == "Year" || gridColumn.FieldName == "YearMonth") && gridColumn.VisibleIndex > -1)
                {
                    hasYearMonth = true;
                    break;
                }
            }
            string columnSQL = ColumnViewUtils.BuildSQLColumnListFromColumnView(this.stockReceiveRMGrid1.GridView.Columns.View, "", (allColumns ? 1 : 0) != 0, new string[1]
      {
        "DocKey"
      }, new string[3]
      {
        "Delete",
        "Year",
        "YearMonth"
      });
            try
            {
                this.myStockReceiveRMCmd.InquireAllMaster(columnSQL, hasYearMonth);
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
            }
      //      FormStockReceiveRMCmd.FormLoadDataEventArgs loadDataEventArgs = new FormStockReceiveRMCmd.FormLoadDataEventArgs(this.panelCmd, this.panelGrid, this.stockReceiveRMGrid1.GridControl, this, this.myStockReceiveRMCmd.DataTableAllMaster, this.myStockReceiveRMCmd);
      //      this.myScriptObject.RunMethod("OnFormLoadData", new System.Type[1]
      //{
      //  loadDataEventArgs.GetType()
      //}, new object[1]
      //{
      //  (object) loadDataEventArgs
      //});
        }

        private void ReloadAllColumns(object sender, EventArgs e)
        {
            this.SearchAll(true);
        }

        private void NewDocument()
        {
            if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_NEW", (XtraForm)this))
            {
                try
                {
                    StockReceiveRM stockReceive = this.myStockReceiveRMCmd.AddNew();
                    if (stockReceive != null)
                    {
                         FormStockReceiveRMCmd.StartEntryForm(stockReceive);
                        //FormStockReceiveRMEntry formStockReceiveRMEntry = new FormStockReceiveRMEntry(stockReceive, this.myDBSetting, (DataTable)null);
                        //formStockReceiveRMEntry.Show();
                    }
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage((Form)this, ex.Message);
                }
            }
        }

        private void Find()
        {
           // int num = (int)new FormStockReceiveRMFind(this.myStockReceiveRMCmd).ShowDialog((IWin32Window)this);
        }

        private void EditDocument()
        {
            if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_EDIT", (XtraForm)this))
            {
                string docNo = "";
                long selectedDocKey = this.GetSelectedDocKey(ref docNo);
                if (selectedDocKey > -1L && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RCVRM", selectedDocKey) <= 0 || this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_PRINTED_EDIT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedEditPrintedDocument, new object[0]))))
                    this.Edit(selectedDocKey, (DataTable)null);
            }
        }

        private void ViewDocument()
        {
            if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_VIEW", (XtraForm)this))
            {
                string docNo = "";
                long selectedDocKey = this.GetSelectedDocKey(ref docNo);
                if (selectedDocKey > -1L)
                    this.View(selectedDocKey, (DataTable)null);
            }
        }

        private void DeleteDocument()
        {
            if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_DELETE", (XtraForm)this))
            {
                string docNo = "";
                long selectedDocKey = this.GetSelectedDocKey(ref docNo);
                if (selectedDocKey > -1L && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RCVRM", selectedDocKey) <= 0 || this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_PRINTED_DELETE", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedDeletePrintedDocument, new object[0]))))
                {
                    // ISSUE: variable of a boxed type
                    StockReceiveStringId local = StockReceiveStringId.ConfirmMessage_DeleteStockReceive;
                    object[] objArray = new object[1];
                    int index = 0;
                    string str = docNo;
                    objArray[index] = (object)str;
                    if (AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum)local, objArray)))
                    {
                        try
                        {
                            this.myStockReceiveRMCmd.Delete(selectedDocKey);
                            AppMessage.ShowMessage(BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.ShowMessage_DeleteSuccessfully, new object[0]));
                        }
                        catch (DBConcurrencyException ex)
                        {
                            AppMessage.ShowErrorMessage((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.ErrorMessage_DocumentDoesNotExist, new object[0]));
                        }
                        catch (AppException ex)
                        {
                            AppMessage.ShowErrorMessage((IWin32Window)this, ex.Message);
                        }
                    }
                }
            }
        }
        private bool CheckBeforePrint(ReportInfo reportInfo)
        {
            if (!((BeforePreviewDocumentEventArgs)reportInfo.Tag).AllowPrint || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RCVRM", reportInfo.DocKey) > 0 && !this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_PRINTED_PRINT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0])))
                return false;
            else
                return true;
        }

        private bool CheckBeforeExport(ReportInfo reportInfo)
        {
            if (!((BeforePreviewDocumentEventArgs)reportInfo.Tag).AllowExport || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RCVRM", reportInfo.DocKey) > 0 && !this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_PRINTED_EXPORT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedExportPrintedDocument, new object[0])))
                return false;
            else
                return true;
        }
        private void PreviewDocument(bool useDefaultReport)
        {
            string docNo = "";
            long selectedDocKey = this.GetSelectedDocKey(ref docNo);
            if (selectedDocKey > -1L && this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_DOC_REPORT_PREVIEW", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RCVRM", selectedDocKey) <= 0 || this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_PRINTED_PREVIEW", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPreviewPrintedDocument, new object[0]))))
            {
                // ISSUE: variable of a boxed type
               StockReceiveRMString local = StockReceiveRMString.StockReceiveRM;
                object[] objArray1 = new object[1];
                int index1 = 0;
                string str = docNo;
                objArray1[index1] = (object)str;
                ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum)local, objArray1), "RPA_RCVRM_DOC_REPORT_PRINT", "RPA_RCVRM_DOC_REPORT_EXPORT", "");
                reportInfo.DocType = "RV";
                reportInfo.DocKey = selectedDocKey;
                reportInfo.UpdatePrintCountTableName = "RPA_RCVRM";
                reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
                reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
                reportInfo.EmailAndFaxInfo = StockReceiveRMCommand.GetEmailAndFaxInfo(selectedDocKey, this.myDBSetting);
                BeforePreviewDocumentEventArgs documentEventArgs1 = new BeforePreviewDocumentEventArgs(reportInfo.EmailAndFaxInfo, reportInfo.DocKey, this.myDBSetting);
                //ScriptObject scriptObject = this.myScriptObject;
                //string name = "BeforePreviewDocument";
                //System.Type[] types = new System.Type[1];
                //int index2 = 0;
                //System.Type type = documentEventArgs1.GetType();
                //types[index2] = type;
                //object[] objArray2 = new object[1];
                //int index3 = 0;
                //BeforePreviewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
                //objArray2[index3] = (object)documentEventArgs2;
                //scriptObject.RunMethod(name, types, objArray2);
                //reportInfo.Tag = (object)documentEventArgs1;
                //if (documentEventArgs1.AllowPreview)
                    ReportTool.PreviewReport("Stock Return Raw Material Document", this.myStockReceiveRMCmd.GetReportDataSource(selectedDocKey), this.myDBSetting, useDefaultReport, false, this.myStockReceiveRMCmd.ReportOption, reportInfo);
            }
        }

        private void PrintDocument(bool useDefaultReport)
        {
            string docNo = "";
            long selectedDocKey = this.GetSelectedDocKey(ref docNo);
            if (selectedDocKey > -1L && this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_DOC_REPORT_PRINT", (XtraForm)this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "RPA_RCVRM", selectedDocKey) <= 0 || this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_PRINTED_PRINT", (XtraForm)this, BCE.Localization.Localizer.GetString((Enum)BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0]))))
            {
                // ISSUE: variable of a boxed type
                StockReceiveRMString local = StockReceiveRMString.StockReceiveRM;
                object[] objArray = new object[1];
                int index = 0;
                string str = docNo;
                objArray[index] = (object)str;
                ReportTool.PrintReport("Stock Return Raw Material Document", this.myStockReceiveRMCmd.GetReportDataSource(selectedDocKey), this.myDBSetting, useDefaultReport, this.myStockReceiveRMCmd.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum)local, objArray), "RPA_RCVRM_DOC_REPORT_PRINT", "RPA_RCVRM_DOC_REPORT_EXPORT", "")
                {
                    DocType = "RV",
                    DocKey = selectedDocKey,
                    UpdatePrintCountTableName = "RPA_RCVRM",
                    EmailAndFaxInfo = StockReceiveRMCommand.GetEmailAndFaxInfo(selectedDocKey, this.myDBSetting)
                });
            }
        }

        private void GeneralPrint()
        {
            string[] rptTypes = new string[2];
            int index1 = 0;
            string string1 = BCE.Localization.Localizer.GetString((Enum)StockReceiveRMString.BatchPrintStockReceiveRMOrStockReceiveRMListing, new object[0]);
            rptTypes[index1] = string1;
            int index2 = 1;
            string string2 = BCE.Localization.Localizer.GetString((Enum)StockReceiveRMString.PrintStockReceiveRMDetailListing, new object[0]);
            rptTypes[index2] = string2;
            switch (FormSelectListingReportType.SelectListingReportType(rptTypes))
            {
                case 0:
                    if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_LISTING_REPORT_SHOW", true))
                    {
                        FormStockReceiveRMPrintListing form = new FormStockReceiveRMPrintListing(myDBSetting);
                        form.WindowState = FormWindowState.Maximized;
                        form.Show();
                    }
                    break;
                case 1:
                    if (this.myUserAuthentication.AccessRight.IsAccessible("RPA_RCVRM_DTLLIST_REPORT_SHOW", true))
                    {
                        FormStockReceiveRMPrintDetailListing formDtl = new FormStockReceiveRMPrintDetailListing(myDBSetting);
                        formDtl.WindowState = FormWindowState.Maximized;
                        formDtl.Show();
                    }
                    break;
            }
        }

        private void ShowAllRecords(bool show)
        {
            this.panelGrid.Visible = show;
            if (this.panelGrid.Visible && this.stockReceiveRMGrid1.DataSource == null)
            {
                this.SearchAll(false);
                this.stockReceiveRMGrid1.DataSource = (object)this.myStockReceiveRMCmd.DataTableAllMaster;
            }
        }

        private void RefreshGrid()
        {
            this.SearchAll(false);
        }

        private void DesignDocumentStyleReport()
        {
            string docNo = "";
            long selectedDocKey = this.GetSelectedDocKey(ref docNo);
            ReportTool.DesignReport("Stock Return Raw Material Document", this.myStockReceiveRMCmd.GetReportDataSource(selectedDocKey), this.myDBSetting);
        }

        private void DesignDocumentListingStyleReport()
        {
            ReportTool.DesignReport("RPA Stock Return Raw Material Listing", this.myDBSetting);
        }

        private void DesignDetailListingReport()
        {
            ReportTool.DesignReport("RPA Stock Return Raw Material Detail Listing", this.myDBSetting);
        }

        private void ReportOption()
        {
            FormBasicReportOption.ShowReportOption(this.myStockReceiveRMCmd.ReportOption);
            this.myStockReceiveRMCmd.SaveReportOption();
        }

        private void chkEdtShowAtStartup_CheckedChanged(object sender, EventArgs e)
        {
            this.SaveCommandFormStartupState();
        }

        private void FormInitialize()
        {
            //FormStockReceiveRMCmd.FormInitializeEventArgs initializeEventArgs1 = new FormStockReceiveRMCmd.FormInitializeEventArgs(this);
            //ScriptObject scriptObject = this.myScriptObject;
            //string name = "OnFormInitialize";
            //System.Type[] types = new System.Type[1];
            //int index1 = 0;
            //System.Type type = initializeEventArgs1.GetType();
            //types[index1] = type;
            //object[] objArray = new object[1];
            //int index2 = 0;
            //FormStockReceiveRMCmd.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
            //objArray[index2] = (object)initializeEventArgs2;
            //scriptObject.RunMethod(name, types, objArray);
        }

        public static void ViewDocument(DBSetting dbSetting, long docKey)
        {
            if (UserAuthentication.GetOrCreate(dbSetting).AccessRight.IsAccessible("RPA_RCVRM_VIEW", true))
            {
                try
                {
                    StockReceiveRM stockReceive = StockReceiveRMCommand.Create(dbSetting).View(docKey);
                    if (stockReceive == null)
                        AppMessage.ShowMessage((IWin32Window)null, BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.ErrorMessage_RecordDoesNotExist, new object[0]));
                    else
                        FormStockReceiveRMCmd.StartEntryForm(stockReceive);
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage((IWin32Window)null, ex.Message);
                }
            }
        }

        public static void ViewDocument(DBSetting dbSetting, string docNo)
        {
            DBSetting dbSetting1 = dbSetting;
            string cmdText = "SELECT DocKey FROM RPA_RCVRM WHERE DocNo=?";
            object[] objArray1 = new object[1];
            int index1 = 0;
            string str1 = docNo;
            objArray1[index1] = (object)str1;
            object obj = dbSetting1.ExecuteScalar(cmdText, objArray1);
            if (obj == null)
            {
                // ISSUE: variable of a boxed type
                StockReceiveStringId local = StockReceiveStringId.ErrorMessage_UnableToViewStockReceive;
                object[] objArray2 = new object[1];
                int index2 = 0;
                string str2 = docNo;
                objArray2[index2] = (object)str2;
                AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum)local, objArray2));
            }
            long docKey = (long)obj;
            FormStockReceiveRMCmd.ViewDocument(dbSetting, docKey);
        }
        public static void StartEntryForm(StockReceiveRM stockReceive)
        {
            BCE.AutoCount.Stock.StockReceive.FormStockReceiveEditOption editOption=new BCE.AutoCount.Stock.StockReceive.FormStockReceiveEditOption();
           // FormStockReceiveRMEntry formentry = new FormStockReceiveRMEntry(stockReceive, editOption);
            string assemblyQualifiedName = typeof(FormStockReceiveRMEntry).AssemblyQualifiedName;
            System.Type[] types = new System.Type[1];
            int index1 = 0;
            System.Type type = stockReceive.GetType();
            types[index1] = type;
            object[] parameters = new object[1];
            int index2 = 0;
            StockReceiveRM stockReceive1 = stockReceive;
            parameters[index2] = (object)stockReceive1;
            new ThreadForm(assemblyQualifiedName, types, parameters).Show();
            //formentry.Show();
        }
        public static void EditDocument(DBSetting dbSetting, long docKey)
        {
            if (UserAuthentication.GetOrCreate(dbSetting).AccessRight.IsAccessible("RPA_RCVRM_EDIT", true))
            {
                try
                {
                    StockReceiveRM stockReceive = StockReceiveRMCommand.Create(dbSetting).Edit(docKey);
                    if (stockReceive == null)
                        AppMessage.ShowMessage((IWin32Window)null, BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.ErrorMessage_RecordDoesNotExist, new object[0]));
                    else
                        FormStockReceiveRMCmd.StartEntryForm(stockReceive);
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage((IWin32Window)null, ex.Message);
                }
            }
        }

        public static void EditDocument(DBSetting dbSetting, string docNo)
        {
            DBSetting dbSetting1 = dbSetting;
            string cmdText = "SELECT DocKey FROM RPA_RCVRM WHERE DocNo=?";
            object[] objArray1 = new object[1];
            int index1 = 0;
            string str1 = docNo;
            objArray1[index1] = (object)str1;
            object obj = dbSetting1.ExecuteScalar(cmdText, objArray1);
            if (obj == null)
            {
                // ISSUE: variable of a boxed type
                StockReceiveStringId local = StockReceiveStringId.ErrorMessage_UnableToEditStockReceive;
                object[] objArray2 = new object[1];
                int index2 = 0;
                string str2 = docNo;
                objArray2[index2] = (object)str2;
                AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum)local, objArray2));
            }
            long docKey = (long)obj;
            FormStockReceiveRMCmd.EditDocument(dbSetting, docKey);
        }

        public static void NewDocument(DBSetting dbSetting)
        {
            if (UserAuthentication.GetOrCreate(dbSetting).AccessRight.IsAccessible("RPA_RCVRM_NEW", true))
            {
                try
                {
                    StockReceiveRM stockReceive = StockReceiveRMCommand.Create(dbSetting).AddNew();
                    if (stockReceive != null)
                        FormStockReceiveRMCmd.StartEntryForm(stockReceive, dbSetting, (DataTable)null);
                }
                catch (AppException ex)
                {
                    AppMessage.ShowErrorMessage((Form)null, ex.Message);
                }
            }
        }

        public static void EditTempDocument(DBSetting dbSetting, long docKey)
        {
            try
            {
                StockReceiveRM stockReceive = StockReceiveRMCommand.Create(dbSetting).LoadFromTempDocument(docKey);
                if (stockReceive == null)
                    AppMessage.ShowMessage((Form)null, BCE.Localization.Localizer.GetString((Enum)StockReceiveStringId.ErrorMessage_RecordDoesNotExist, new object[0]));
                else
                    FormStockReceiveRMCmd.StartEntryForm(stockReceive, dbSetting, (DataTable)null);
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage((Form)null, ex.Message);
            }
        }

        private void FormStockReceiveRMCmd_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            this.stockReceiveRMGrid1.DataSource = (object)null;
            this.myStockReceiveRMCmd = (StockReceiveRMCommand)null;
            if (this.myUpdateDataSetDelegate != null)
            {
                StockReceiveRMCommand.DataSetUpdate.Remove(this.myDBSetting, this.myUpdateDataSetDelegate);
                this.myUpdateDataSetDelegate = (AsyncDataSetUpdateDelegate)null;
            }
            if (this.myCommandFormController != null)
            {
                this.myCommandFormController.Cleanup();
                this.myCommandFormController = (DocumentCommandFormController)null;
            }
            //FormStockReceiveRMCmd.FormClosedEventArgs formClosedEventArgs1 = new FormStockReceiveRMCmd.FormClosedEventArgs(this);
            //ScriptObject scriptObject = this.myScriptObject;
            //string name = "OnFormClosed";
            //System.Type[] types = new System.Type[1];
            //int index1 = 0;
            //System.Type type = formClosedEventArgs1.GetType();
            //types[index1] = type;
            //object[] objArray = new object[1];
            //int index2 = 0;
            //FormStockReceiveRMCmd.FormClosedEventArgs formClosedEventArgs2 = formClosedEventArgs1;
            //objArray[index2] = (object)formClosedEventArgs2;
            //scriptObject.RunMethod(name, types, objArray);
        }

        public class FormEventArgs
        {
            private PanelControl myPanelAction;
            private PanelControl myPanelGrid;
            private GridControl myGridControl;
            private FormStockReceiveRMCmd myForm;
            private StockReceiveRMCommand myCommand;

            public StockReceiveRMCommand Command
            {
                get
                {
                    return this.myCommand;
                }
            }

            public PanelControl PanelAction
            {
                get
                {
                    return this.myPanelAction;
                }
            }

            public PanelControl PanelGrid
            {
                get
                {
                    return this.myPanelGrid;
                }
            }

            public GridControl GridControl
            {
                get
                {
                    return this.myGridControl;
                }
            }

            public FormStockReceiveRMCmd Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public FormEventArgs(FormStockReceiveRMCmd form)
            {
                this.myForm = form;
            }
        }

        public class FormInitializeEventArgs : FormStockReceiveRMCmd.FormEventArgs
        {
            public FormInitializeEventArgs(FormStockReceiveRMCmd form)
        : base(form)
            {
            }
        }

        public class FormClosedEventArgs : FormStockReceiveRMCmd.FormEventArgs
        {
            public FormClosedEventArgs(FormStockReceiveRMCmd form)
              : base(form)
            {
            }
        }

        public class FormLoadDataEventArgs : FormStockReceiveRMCmd.FormEventArgs
        {
            private DataTable myGridDataTable;

            public DataTable GridDataTable
            {
                get
                {
                    return this.myGridDataTable;
                }
            }

            public FormLoadDataEventArgs(FormStockReceiveRMCmd form, DataTable gridDataTable)
       : base(form)
            {
                this.myGridDataTable = gridDataTable;
            }
        }

        private void panelHeader1_Load(object sender, EventArgs e)
        {

        }

        private void panelCmd_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblNew_OpenLink(object sender, OpenLinkEventArgs e)
        {
           // NewDocument();
        }

        private void lblFind_OpenLink(object sender, OpenLinkEventArgs e)
        {
           // Find();
        }

        private void sbtnEdit_Click(object sender, EventArgs e)
        {
           // EditDocument();
        }

        private void sbtnView_Click(object sender, EventArgs e)
        {
           // ViewDocument();
        }

        private void previewButton_Preview(object sender, PrintEventArgs e)
        {
           // PreviewDocument(true);
        }

        private void printButton_Print(object sender, PrintEventArgs e)
        {
            //PrintDocument(true);
        }

        private void sbtnDel_Click(object sender, EventArgs e)
        {
            //DeleteDocument();
        }

        private void sbtnRefresh_Click(object sender, EventArgs e)
        {
           // Refresh();
        }

        private void lblPrint_OpenLink(object sender, OpenLinkEventArgs e)
        {

        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItem1_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            DesignDocumentStyleReport();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            DesignDocumentListingStyleReport();
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            DesignDetailListingReport();
        }
    }
}
