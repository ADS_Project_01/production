﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMValidateItemQtyEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMValidateItemQtyEventArgs : StockReceiveRMEventArgs
  {
    private bool myHandled;

    public bool Handled
    {
      get
      {
        return this.myHandled;
      }
      set
      {
        this.myHandled = value;
      }
    }

    internal StockReceiveRMValidateItemQtyEventArgs(StockReceiveRM doc)
      : base(doc)
    {
    }
  }
}
