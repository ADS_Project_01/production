﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMDetail
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Application;
using BCE.AutoCount.Data;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.SerialNumber2;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Data;

namespace Production.StockReceiveRM
{
  public class StockReceiveRMDetail : BaseRecord
  {
    private StockReceiveRM myStockReceiveRM;
    protected DecimalSetting myDecimalSetting;

    public DataRow Row
    {
      get
      {
        return this.myRow;
      }
    }

    public int Seq
    {
      get
      {
        return BCE.Data.Convert.ToInt32(this.myRow["Seq"]);
      }
    }

    public DBString Numbering
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Numbering"]);
      }
      set
      {
        this.myRow["Numbering"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString ItemCode
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["ItemCode"]);
      }
      set
      {
        this.myRow["ItemCode"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString UOM
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["UOM"]);
      }
      set
      {
        this.myRow["UOM"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Location
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Location"]);
      }
      set
      {
        this.myRow["Location"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString BatchNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["BatchNo"]);
      }
      set
      {
        this.myRow["BatchNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Description
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Description"]);
      }
      set
      {
        this.myRow["Description"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString FurtherDescription
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["FurtherDescription"]);
      }
      set
      {
        if (value.HasValue)
          this.myRow["FurtherDescription"] = (object) Rtf.ToArialRichText((string) value);
        else
          this.myRow["FurtherDescription"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString ProjNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["ProjNo"]);
      }
      set
      {
        this.myRow["ProjNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString DeptNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["DeptNo"]);
      }
      set
      {
        this.myRow["DeptNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBDecimal Qty
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["Qty"]);
      }
      set
      {
        this.myRow["Qty"] = this.myDecimalSetting.RoundToQuantityDBObject(value);
      }
    }
     
    
        public DBDecimal UnitCost
        {
            get
            {
                return BCE.Data.Convert.ToDBDecimal(this.myRow["UnitCost"]);
            }
            set
            {
                this.myRow["UnitCost"] = this.myDecimalSetting.RoundToCostDBObject(value);
            }
        }
        public DBDecimal SubTotal
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["SubTotal"]);
      }
      set
      {
        this.myRow["SubTotal"] = this.myDecimalSetting.RoundToCurrencyDBObject(value);
      }
    }

    public bool PrintOut
    {
      get
      {
        return BCE.Data.Convert.TextToBoolean(this.myRow["PrintOut"]);
      }
      set
      {
        this.myRow["PrintOut"] = (object) BCE.Data.Convert.BooleanToText(value);
      }
    }

    public DBString SerialNoList
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["SerialNoList"]);
      }
    }
        public DBString FromDocType
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["FromDocType"]);
            }
        }

        public DBString FromDocNo
        {
            get
            {
                return BCE.Data.Convert.ToDBString(this.myRow["FromDocNo"]);
            }
        }
        internal StockReceiveRMDetail(DataRow row, StockReceiveRM stockReceive)
      : base(row)
    {
      this.myStockReceiveRM = stockReceive;
      this.myDecimalSetting = DecimalSetting.GetOrCreate(stockReceive.Command.DBSetting);
    }

    public int GetSerialNumberTransactionRecordCount()
    {
      return this.myStockReceiveRM.DataTableSerialNo.Select("DtlKey = " + this.myRow["DtlKey"].ToString()).Length;
    }

    public SerialNumberTransactionRecord GetSerialNumberTransactionRecord(int index)
    {
      DataRow[] dataRowArray = this.myStockReceiveRM.DataTableSerialNo.Select("DtlKey = " + this.myRow["DtlKey"].ToString());
      if (index >= dataRowArray.Length)
        return (SerialNumberTransactionRecord) null;
      else
        return new SerialNumberTransactionRecord(dataRowArray[index]);
    }

    public SerialNumberTransactionRecord GetSerialNumberTransactionRecord(string serialNo)
    {
      DataRow[] dataRowArray = this.myStockReceiveRM.DataTableSerialNo.Select("DtlKey = " + this.myRow["DtlKey"].ToString());
      for (int index = 0; index < dataRowArray.Length; ++index)
      {
        if (string.Compare(dataRowArray[index]["FromSerialNo"].ToString(), serialNo, true) == 0 && dataRowArray[index]["ToSerialNo"].ToString().Length == 0)
          return new SerialNumberTransactionRecord(dataRowArray[index]);
      }
      return (SerialNumberTransactionRecord) null;
    }

    public SerialNumberTransactionRecord GetSerialNumberTransactionRecord(string fromSerialNo, string toSerialNo)
    {
      DataRow[] dataRowArray = this.myStockReceiveRM.DataTableSerialNo.Select("DtlKey = " + this.myRow["DtlKey"].ToString());
      for (int index = 0; index < dataRowArray.Length; ++index)
      {
        if (string.Compare(dataRowArray[index]["FromSerialNo"].ToString(), fromSerialNo, true) == 0 && string.Compare(dataRowArray[index]["ToSerialNo"].ToString(), toSerialNo, true) == 0)
          return new SerialNumberTransactionRecord(dataRowArray[index]);
      }
      return (SerialNumberTransactionRecord) null;
    }

    public bool DeleteSerialNumberTransactionRecord(string serialNo)
    {
      return this.DeleteSerialNumberTransactionRecord(serialNo, "");
    }

    public bool DeleteSerialNumberTransactionRecord(string fromSerialNo, string toSerialNo)
    {
      DataRow[] dataRowArray = this.myStockReceiveRM.DataTableSerialNo.Select("DtlKey = " + this.myRow["DtlKey"].ToString());
      for (int index = 0; index < dataRowArray.Length; ++index)
      {
        if (string.Compare(dataRowArray[index]["FromSerialNo"].ToString(), fromSerialNo, true) == 0 && string.Compare(dataRowArray[index]["ToSerialNo"].ToString(), toSerialNo, true) == 0)
        {
          dataRowArray[index].Delete();
          return true;
        }
      }
      return false;
    }

    public SerialNumberTransactionRecord AddSerialNumberTransactionRecord(string serialNo)
    {
      return this.AddSerialNumberTransactionRecord(serialNo, "");
    }

    public SerialNumberTransactionRecord AddSerialNumberTransactionRecord(string fromSerialNo, string toSerialNo)
    {
      if (fromSerialNo == null)
        throw new ArgumentNullException("fromSerialNo");
      else if (fromSerialNo.Length == 0)
        throw new ArgumentException("fromSerialNo cannot empty string.", "fromSerialNo");
      else if (!this.ItemCode.HasValue)
        throw new AppException(Localizer.GetString((Enum) StockReceiveStringId.ErrorMessage_UnableAddSerialNum, new object[0]));
      else if (!StockHelper.Create(this.myStockReceiveRM.Command.DBSetting).HasSerialNumberControl((string) this.ItemCode))
      {
        throw new AppException(Localizer.GetString((Enum) StockReceiveStringId.ErrorMessage_NoSerialNumControl, new object[0]));
      }
      else
      {
        DataRow row = this.myStockReceiveRM.DataTableSerialNo.NewRow();
        DBRegistry dbRegistry = DBRegistry.Create(this.myStockReceiveRM.Command.DBSetting);
        row["TransKey"] = (object) dbRegistry.IncOne((IRegistryID) new GlobalUniqueKey());
        row["DocType"] = (object) "SR";
        row["DtlKey"] = this.myRow["DtlKey"];
        row["DocKey"] = this.myRow["DocKey"];
        row["ItemCode"] = this.myRow["ItemCode"];
        row["FromSerialNo"] = (object) fromSerialNo;
        if (toSerialNo != null && toSerialNo.Length > 0)
          row["ToSerialNo"] = (object) toSerialNo;
        this.myStockReceiveRM.DataTableSerialNo.Rows.Add(row);
        return new SerialNumberTransactionRecord(row);
      }
    }
  }
}
