﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMDetailDeletingEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using System;
using System.Data;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMDetailDeletingEventArgs : StockReceiveRMNewDetailEventArgs
  {
    internal StockReceiveRMDetailDeletingEventArgs(StockReceiveRM doc, DataRow detailRow)
      : base(doc, detailRow)
    {
    }
  }
}
