﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMReportCommand
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount;
using BCE.AutoCount.Report;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using BCE.Localization;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Production.StockReceiveRM
{
  public class StockReceiveRMReportCommand
  {
    private BasicReportOption myReportOption;
    protected DBSetting myDBSetting;
    protected const string MasterTableName = "Master";
    protected const string DetailTableName = "Detail";
    public const string DetailListingReportStyle = "RPA Stock Return Raw Material Detail Listing";

    public BasicReportOption ReportOption
    {
      get
      {
        return this.myReportOption;
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myDBSetting;
      }
    }

    public static StockReceiveRMReportCommand Create(DBSetting dbSetting, BasicReportOption reportOption)
    {
      if (dbSetting.ServerType == DBServerType.SQL2000)
      {
        StockReceiveRMReportCommand issueReportCommand = (StockReceiveRMReportCommand) new StockReceiveRMReportCommandSQL();
        issueReportCommand.myDBSetting = dbSetting;
        issueReportCommand.myReportOption = reportOption;
        return issueReportCommand;
      }
      else
        throw new ArgumentException("Server type: " + (object) dbSetting.ServerType + " not supported.");
    }

    protected string GetWhereSQL(StockReceiveRMDetailReportingCriteria reportingCriteria, SqlCommand cmd)
    {
      string str1 = "";
      SearchCriteria searchCriteria = new SearchCriteria();
      BCE.AutoCount.SearchFilter.Filter dateFilter = reportingCriteria.DateFilter;
      searchCriteria.AddFilter(dateFilter);
      BCE.AutoCount.SearchFilter.Filter documentFilter = reportingCriteria.DocumentFilter;
      searchCriteria.AddFilter(documentFilter);
      BCE.AutoCount.SearchFilter.Filter itemCodeFilter = reportingCriteria.ItemCodeFilter;
      searchCriteria.AddFilter(itemCodeFilter);
      BCE.AutoCount.SearchFilter.Filter itemGroupFilter = reportingCriteria.ItemGroupFilter;
      searchCriteria.AddFilter(itemGroupFilter);
      BCE.AutoCount.SearchFilter.Filter itemTypeFilter = reportingCriteria.ItemTypeFilter;
      searchCriteria.AddFilter(itemTypeFilter);
      BCE.AutoCount.SearchFilter.Filter locationFilter = reportingCriteria.LocationFilter;
      searchCriteria.AddFilter(locationFilter);
      BCE.AutoCount.SearchFilter.Filter projecNoFilter = reportingCriteria.ProjecNoFilter;
      searchCriteria.AddFilter(projecNoFilter);
      BCE.AutoCount.SearchFilter.Filter deptNoFilter = reportingCriteria.DeptNoFilter;
      searchCriteria.AddFilter(deptNoFilter);
      int num = 1;
      searchCriteria.MatchAll = num != 0;
      SqlCommand sqlCommand = cmd;
      string str2 = searchCriteria.BuildSQL((IDbCommand) sqlCommand);
      if (str2.Length > 0)
        str1 = str2;
      if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.UnCancelled)
        str1 = str1.Length <= 0 ? " RPA_RCVRM.Cancelled = 'F' " : str1 + " And RPA_RCVRM.Cancelled = 'F' ";
      else if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
        str1 = str1.Length <= 0 ? " RPA_RCVRM.Cancelled = 'T' " : str1 + " And RPA_RCVRM.Cancelled = 'T' ";
      return str1;
    }

    public void PrintDetailListingReport(string dtlKeys, StockReceiveRMDetailReportingCriteria criteria)
    {
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockReceiveRMString.StockReceiveRMDetailListing, new object[0]), "STK_ISS_DTLLIST_REPORT_PRINT", "STK_ISS_DTLLIST_REPORT_EXPORT", criteria.ReadableText);
      ReportTool.PrintReport("RPA Stock Return Raw Material Detail Listing", this.GetDetailListingReportDataSource(dtlKeys, criteria), this.myDBSetting, false, this.myReportOption, reportInfo);
    }

    public void PrintDetailListingReport(string dtlKeys, StockReceiveRMDetailReportingCriteria criteria, bool defaultValue)
    {
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockReceiveRMString.StockReceiveRMDetailListing, new object[0]), "STK_ISS_DTLLIST_REPORT_PRINT", "STK_ISS_DTLLIST_REPORT_EXPORT", criteria.ReadableText);
      ReportTool.PrintReport("RPA Stock Return Raw Material Detail Listing", this.GetDetailListingReportDataSource(dtlKeys, criteria), this.myDBSetting, defaultValue, this.myReportOption, reportInfo);
    }

    public void PreviewDetailListingReport(string dtlKeys, StockReceiveRMDetailReportingCriteria criteria)
    {
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockReceiveRMString.StockReceiveRMDetailListing, new object[0]), "STK_ISS_DTLLIST_REPORT_PRINT", "STK_ISS_DTLLIST_REPORT_EXPORT", criteria.ReadableText);
      ReportTool.PreviewReport("RPA Stock Return Raw Material Detail Listing", this.GetDetailListingReportDataSource(dtlKeys, criteria), this.myDBSetting, true, false, this.myReportOption, reportInfo);
    }

    public void PreviewDetailListingReport(string dtlKeys, StockReceiveRMDetailReportingCriteria criteria, bool defaultValue)
    {
      ReportInfo reportInfo = new ReportInfo(Localizer.GetString((Enum) StockReceiveRMString.StockReceiveRMDetailListing, new object[0]), "STK_ISS_DTLLIST_REPORT_PRINT", "STK_ISS_DTLLIST_REPORT_EXPORT", criteria.ReadableText);
      ReportTool.PreviewReport("RPA Stock Return Raw Material Detail Listing", this.GetDetailListingReportDataSource(dtlKeys, criteria), this.myDBSetting, defaultValue, false, this.myReportOption, reportInfo);
    }

    private DocumentReportDataSet PreparingDetailListingReportDataSet(DataSet dsReportData, StockReceiveRMDetailReportingCriteria criteria, string dataName)
    {
      DocumentReportDataSet documentReportDataSet = new DocumentReportDataSet(this.myDBSetting, dataName, "Stock Return Raw Material Master");
      documentReportDataSet.Tables.Add(dsReportData.Tables[0].Copy());
      DataTable table = new DataTable("Report Option");
      table.Columns.Add(new DataColumn("Criteria", typeof (string)));
      table.Columns.Add(new DataColumn("ShowCriteria", typeof (string)));
      table.Columns.Add(new DataColumn("GroupBy", typeof (string)));
      table.Columns.Add(new DataColumn("SortBy", typeof (string)));
      table.Columns.Add(new DataColumn("FromDate", typeof (DateTime)));
      table.Columns.Add(new DataColumn("ToDate", typeof (DateTime)));
      table.Columns.Add(new DataColumn("DocumentFilterTypeString", typeof (string)));
      table.Columns.Add(new DataColumn("CancelledStatus", typeof (string)));
      if (criteria == null)
        criteria = new StockReceiveRMDetailReportingCriteria();
      DataRow row = table.NewRow();
      row["Criteria"] = (object) criteria.ReadableText;
      row["ShowCriteria"] = criteria.IsShowCriteria ? (object) "Yes" : (object) "No";
      row["GroupBy"] = (object) criteria.GroupBy;
      row["SortBy"] = (object) criteria.SortBy;
      if (criteria.DateFilter.Type == FilterType.ByRange)
      {
        row["FromDate"] = criteria.DateFilter.From == null || criteria.DateFilter.From.ToString().Length == 0 ? (object) DateTime.MinValue : (object) (DateTime) criteria.DateFilter.From;
        row["ToDate"] = criteria.DateFilter.To == null || criteria.DateFilter.To.ToString().Length == 0 ? (object) DateTime.MaxValue : (object) (DateTime) criteria.DateFilter.To;
      }
      else
      {
        row["FromDate"] = (object) DateTime.MinValue;
        row["ToDate"] = (object) DateTime.MaxValue;
      }
      row["DocumentFilterTypeString"] = (object) criteria.DocumentFilter.ToFilterTypeString();
      row["CancelledStatus"] = criteria.IsPrintCancelled != CancelledDocumentOption.All ? (criteria.IsPrintCancelled != CancelledDocumentOption.Cancelled ? (object) "Show Uncancelled" : (object) "Show Cancelled") : (object) "Show All";
      table.Rows.Add(row);
      documentReportDataSet.Tables.Add(table);
      documentReportDataSet.AddDefaultTables();
      return documentReportDataSet;
    }

    public object GetDetailListingReportDataSource(string dtlKeys, StockReceiveRMDetailReportingCriteria criteria)
    {
      return (object) this.PreparingDetailListingReportDataSet(this.LoadDetailListingReportData(dtlKeys, criteria), criteria, "RPA Stock Return Raw Material Detail Listing");
    }

    public object GetDetailListingReportDesignerDataSource()
    {
      return (object) this.PreparingDetailListingReportDataSet(this.LoadDetailListingReportDesignerData(), (StockReceiveRMDetailReportingCriteria) null, "RPA Stock Return Raw Material Detail Listing");
    }

    protected virtual DataSet LoadDetailListingReportData(string dtlKeys, StockReceiveRMDetailReportingCriteria criteria)
    {
      return (DataSet) null;
    }

    protected virtual DataSet LoadDetailListingReportDesignerData()
    {
      return (DataSet) null;
    }

    public virtual void DetailListingBasicSearch(StockReceiveRMDetailReportingCriteria criteria, string columnName, DataSet newDS, string chkEditColumnName)
    {
    }

    public virtual void DetailListingAdvanceSearch(AdvancedStockReceiveRMCriteria criteria, DataSet newDS, string checkEditColumnName)
    {
    }
  }
}
