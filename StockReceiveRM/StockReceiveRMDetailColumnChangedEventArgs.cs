﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.StockReceiveRMDetailColumnChangedEventArgs
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.Data;
using System;
using System.Data;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class StockReceiveRMDetailColumnChangedEventArgs
  {
    private string myColumnName;
    private DataRow myDetailRow;
    private StockReceiveRM myStock;

    public string ChangedColumnName
    {
      get
      {
        return this.myColumnName;
      }
    }

    public StockReceiveRMRecord MasterRecord
    {
      get
      {
        return new StockReceiveRMRecord(this.myStock);
      }
    }

    public StockReceiveRM StockReceiveRM
    {
      get
      {
        return this.myStock;
      }
    }

    public StockReceiveRMDetailRecord CurrentDetailRecord
    {
      get
      {
        return new StockReceiveRMDetailRecord(this.myStock.Command.DBSetting, this.myDetailRow);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.myDBSetting;
      }
    }

    internal StockReceiveRMDetailColumnChangedEventArgs(string columnName, StockReceiveRM doc, DataRow detailRow)
    {
      this.myColumnName = columnName;
      this.myStock = doc;
      this.myDetailRow = detailRow;
    }
  }
}
