﻿// Type: BCE.AutoCount.Stock.StockReceiveRM.AdvancedStockReceiveRMCriteria
// Assembly: BCE.AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Stock.dll

using BCE.AutoCount.SearchFilter;
//using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Localization;
using System;

namespace Production.StockReceiveRM
{
  [Serializable]
  public class AdvancedStockReceiveRMCriteria : SearchCriteria
  {
    public AdvancedStockReceiveRMCriteria(DBSetting dbSetting)
    {
      this.Add((SearchElement) new SearchDecorator("", "", Localizer.GetString((Enum) StockReceiveRMColumnStringId.MasterSearchFields, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "DocNo", Localizer.GetString((Enum) StockReceiveRMColumnStringId.StockReceiveRMNo, new object[0]), FilterControlType.StockReceive));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "DocDate", Localizer.GetString((Enum) StockReceiveRMColumnStringId.StockReceiveRMDate, new object[0]), FilterControlType.Date));
      this.Add((SearchElement) new TextSearch("A", "Description", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Description, new object[0])));
      this.Add((SearchElement) new NumberSearch("A", "Total", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Total, new object[0])));
      this.Add((SearchElement) new MemoSearch("A", "Note", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Note, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark1", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Remark1, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark2", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Remark2, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark3", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Remark3, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "Remark4", Localizer.GetString((Enum) StockReceiveRMColumnStringId.Remark4, new object[0])));
      this.Add((SearchElement) new TextSearch("A", "RefDocNo", Localizer.GetString((Enum) StockReceiveRMColumnStringId.RefDocNo, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "CreatedUserID", Localizer.GetString((Enum) StockReceiveRMColumnStringId.CreatedUserID, new object[0]), FilterControlType.User));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "LastModifiedUserID", Localizer.GetString((Enum) StockReceiveRMColumnStringId.LastModifiedUserID, new object[0]), FilterControlType.User));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "CreatedTimeStamp", Localizer.GetString((Enum) StockReceiveRMColumnStringId.CreatedTimestamp, new object[0]), FilterControlType.DateTime));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("A", "LastModified", Localizer.GetString((Enum) StockReceiveRMColumnStringId.LastModified, new object[0]), FilterControlType.DateTime));
      this.Add((SearchElement) new BooleanSearch("A", "Cancelled", Localizer.GetString((Enum) StockReceiveRMColumnStringId.CancelledDocument, new object[0])));
      //new UDFUtil(dbSetting).AddUDFIntoAdvancedSearch((SearchCriteria) this, "RPA_RCV", "A");
      this.Add((SearchElement) new SearchDecorator("", "", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.DetailSearchFields, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "ItemCode", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.ItemCode, new object[0]), FilterControlType.Item));
      this.Add((SearchElement) new TextSearch("B", "Description", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.ItemDescription, new object[0])));
      this.Add((SearchElement) new MemoSearch("B", "FurtherDescription", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.ItemFurtherDescription, new object[0])));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "Location", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Location, new object[0]), FilterControlType.Location));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "ProjNo", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Project, new object[0]), FilterControlType.Project));
      this.Add((SearchElement) new BCE.AutoCount.SearchFilter.Filter("B", "DeptNo", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Department, new object[0]), FilterControlType.Department));
      this.Add((SearchElement) new TextSearch("B", "BatchNo", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.BatchNo, new object[0])));
      this.Add((SearchElement) new TextSearch("B", "UOM", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.UOM, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "Qty", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Quantity, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "UnitCost", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.UnitCost, new object[0])));
      this.Add((SearchElement) new NumberSearch("B", "SubTotal", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.SubTotal, new object[0])));
      this.Add((SearchElement) new TextSearch("B", "Numbering", Localizer.GetString((Enum) StockReceiveRMDetailColumnStringId.Numbering, new object[0])));
     // new UDFUtil(dbSetting).AddUDFIntoAdvancedSearch((SearchCriteria) this, "RPA_RCVDtl", "B");
    }
  }
}
