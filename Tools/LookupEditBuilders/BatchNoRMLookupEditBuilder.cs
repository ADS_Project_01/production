﻿using BCE.AutoCount;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.AutoCount.SearchFilter;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.Tools.LookupEditBuilders
{
    public class BatchNoRMLookupEditBuilder : BaseAccountLookupEditBuilder
    {
        private string myLastDocNo = "!@#!@#!!@#";
        public BatchNoRMLookupEditBuilder()
        {
            this.myUseCaching = true;
            this.myHasActiveSet = true;
            this.myCacheFileName = "BatchNoRMLookupEditBuilder.Data";
            this.myUseSameDataTableObject = false;
            this.myCmdString = "SELECT DISTINCT  wod.ItemCode,wod.FromDocDtlKey,RM.BatchNo,RM.Description,RM.ManufacturedDate,RM.ExpiryDate FROM RPA_WODtl WOD WITH(NOLOCK) inner join (select b.FromDocType, C.BatchNo, C.Description, B.FromDocDtlKey, C.ManufacturedDate, C.ExpiryDate, C.Qty from RPA_RM a  WITH(NOLOCK) inner join RPA_RMDTL b  WITH(NOLOCK) on a.DocKey = b.DocKey inner join RPA_RMSubDtl c WITH(NOLOCK) on a.DocKey = b.DocKey and b.DtlKey = c.DtlKey where FromDocType = 'WO') RM ON WOD.DtlKey = RM.FromDocDtlKey where wod.FromDocDtlKey=-1";
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            DecimalSetting orCreate = DecimalSetting.GetOrCreate(this.myDBSetting);
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "BatchNo";
            lookupEdit.ValueMember = "BatchNo";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("BatchNo", "BatchNo", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Description", "Description", 150));
            lookupEdit.Columns.Add(new LookUpColumnInfo("FromDocDtlKey", "FromDocDtlKey", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("ItemCode", "ItemCode", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("ManufacturedDate", "Manufactured Date", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("ExpiryDate", "ExpiryDate", 100));
            lookupEdit.PopupWidth = 650;
        }

        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.GetItemUOMChangeCount();
        //}

        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastDocNo = "!@#!@#!!@#";
        }
        private string GetWOWhereSql(SqlCommand cmd, BCE.AutoCount.SearchFilter.Filter wofilter)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.AddFilter(wofilter);
            return searchCriteria.BuildSQL((IDbCommand)cmd);
        }
        public void FilterWONo(string sfromdocdtlkey)
        {
            

            if (this.NeedUpdate())
            {
             //   this.myLastDocNo = wono;
                //if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command = sqlConnection.CreateCommand();
                       // string whereSql = //this.GetWOWhereSql(command, wofilter);
                       // if (whereSql.Length != 0)
                         //   whereSql = "AND " + whereSql;
                        //whereSql = whereSql.Replace("a.", "c.");
                        //whereSql = whereSql.Replace("b.", "RPA_WODtl.");
                        string str = "SELECT DISTINCT  RM.BatchNo,RM.Description,RM.ManufacturedDate,RM.ExpiryDate FROM RPA_WODtl WOD WITH(NOLOCK) inner join (select b.FromDocType, C.BatchNo, C.Description, B.FromDocDtlKey, C.ManufacturedDate, C.ExpiryDate, C.Qty from RPA_RM a  WITH(NOLOCK) inner join RPA_RMDTL b  WITH(NOLOCK) on a.DocKey = b.DocKey inner join RPA_RMSubDtl c WITH(NOLOCK) on a.DocKey = b.DocKey and b.DtlKey = c.DtlKey where FromDocType = 'WO') RM ON WOD.DtlKey = RM.FromDocDtlKey WHERE wod.FromDocDtlKey="+ sfromdocdtlkey ;
                        command.CommandText = str;
                        //command.Parameters.AddWithValue("@DocNo", (object)wono);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        BatchNoRMLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }

       
        //public void FilterItemCodeUOMList(List<string> itemCodeList)
        //{
        //    if (this.myDBSetting.ServerType == DBServerType.SQL2000)
        //    {
        //        SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
        //        try
        //        {
        //            sqlConnection.Open();
        //            SqlCommand command = sqlConnection.CreateCommand();
        //            string str = "SELECT DISTINCT UOM FROM ItemUOM WHERE ItemCode IN (SELECT * FROM List2(@ItemCodeList)) ORDER BY UOM";
        //            command.CommandText = str;
        //            command.Parameters.AddWithValue("@ItemCodeList", (object)StringHelper.ToList2String(itemCodeList.ToArray()));
        //            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
        //            ItemUOMLookupEditBuilder lookupEditBuilder = this;
        //            bool lockTaken = false;
        //            try
        //            {
        //                Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
        //                this.myDataTable.Clear();
        //                sqlDataAdapter.Fill(this.myDataTable);
        //                this.AfterFillDataTable();
        //            }
        //            finally
        //            {
        //                if (lockTaken)
        //                    Monitor.Exit((object)lookupEditBuilder);
        //            }
        //        }
        //        catch (SqlException ex)
        //        {
        //            DataError.HandleSqlException(ex);
        //        }
        //        finally
        //        {
        //            sqlConnection.Close();
        //            sqlConnection.Dispose();
        //        }
        //    }
        //}

    }
}



