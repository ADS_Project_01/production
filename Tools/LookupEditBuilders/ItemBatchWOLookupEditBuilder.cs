﻿using BCE.AutoCount;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.Tools.LookupEditBuilders
{
    public class ItemBatchWOLookupEditBuilder : BaseAccountLookupEditBuilder
    {
        private string myLastItemCode = "!@#!@#!!@#";

        public ItemBatchWOLookupEditBuilder()
        {
            this.myUseCaching = true;
            this.myHasActiveSet = true;
            this.myCacheFileName = "ItemBatchWOLookupEditBuilder.Data";
            //this.myUseCaching = true;
            // this.myCacheFileName = "ItemUOM.Data";
            this.myUseSameDataTableObject = false;
            this.myCmdString = "select distinct b.BatchNo from RPA_RCV a inner join RPA_RCVDTL b on a.DocKey=b.DocKey where Cancelled='F' AND FromDocNo='10L'";
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            DecimalSetting orCreate = DecimalSetting.GetOrCreate(this.myDBSetting);
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "BatchNo";
            lookupEdit.ValueMember = "BatchNo";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("BatchNo", "BatchNo", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("ManufacturedDate", "Manufactured Date", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("ExpiryDate", "ExpiryDate", 100));
            //lookupEdit.Columns.Add(new LookUpColumnInfo("CostType", "Cost Type", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("BalQty", "BalQty", 50));

            lookupEdit.PopupWidth =350;
        }
        public void FilterItemCode(string wono)
        {
            if (!(wono == this.myLastItemCode) || this.NeedUpdate())
            {
                if (this.myDBSetting == null)
                    return;
                this.myLastItemCode = wono;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command = sqlConnection.CreateCommand();
                        string str = "select b.BatchNo,ManufacturedDate,ExpiryDate,sum(BalQty) as BalQty from RPA_RCV a inner join RPA_RCVDTL b on a.DocKey=b.DocKey  inner join ItemBatch c on c.BatchNo=b.BatchNo where Cancelled='F' AND FromDocNo=@FromDocNo group by b.BatchNo,ManufacturedDate,ExpiryDate";
                        command.CommandText = str;
                        command.Parameters.AddWithValue("@FromDocNo", (object)wono);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        ItemBatchWOLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }

        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.GetItemUOMChangeCount();
        //}

        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastItemCode = "!@#!@#!!@#";
        }

       
    }
}



