﻿using BCE.AutoCount;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace RPASystem.Tools.LookupEditBuilders
{
    public class ItemFGLookupEditBuilder : LookupEditBuilderEx
    {

        private string myLastDocNo = "!@#!@#!!@#";
        public ItemFGLookupEditBuilder()
        {
            //this.myUseCaching = true;
            // this.myCacheFileName = "ItemUOM.Data";
            this.myUseSameDataTableObject = false;
            this.myCmdString = "SELECT C.ItemCode,C.DocNo AS WONo,C.DocType,C.Code, A.Description, A.Desc2, SUM(B.Rate*B.BalQty) AS BalQty, SUM(ISNULL(B.Rate*B.CSGNQty,0)) AS CSGNQty, SUM(B.Rate*(B.BalQty-ISNULL(B.CSGNQty,0))) AS AfterCSGNQty FROM Item A INNER JOIN vRPA_TransWOtoRCV C ON A.ItemCode = C.ItemCode LEFT OUTER JOIN ItemUOM B ON(A.ItemCode = B.ItemCode) WHERE A.IsActive = 'T' GROUP BY C.ItemCode,C.DocNo,C.DocType,C.Code, A.Description, A.Desc2, A.IsActive ORDER BY C.ItemCode";
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            DecimalSetting orCreate = DecimalSetting.GetOrCreate(this.myDBSetting);
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "ItemCode";
            lookupEdit.ValueMember = "ItemCode";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("ItemCode", "ItemCode", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Description", "Description", 200));
            lookupEdit.Columns.Add(new LookUpColumnInfo("WONo", "WONo", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("DocType", "DocType", 50));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Code", "Code", 50));
            lookupEdit.Columns.Add(new LookUpColumnInfo("BalQty", "BalQty", 50));

            lookupEdit.PopupWidth = 550;
        }

        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.GetItemUOMChangeCount();
        //}

        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastDocNo = "!@#!@#!!@#";
        }

        public void FilterWONo(string wono)
        {
            if (!(wono == this.myLastDocNo) || this.NeedUpdate())
            {
                this.myLastDocNo = wono;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command = sqlConnection.CreateCommand();
                        string str = "SELECT C.ItemCode,C.DocNo AS WONo,C.DocType,C.Code, A.Description, A.Desc2, SUM(B.Rate*B.BalQty) AS BalQty, SUM(ISNULL(B.Rate*B.CSGNQty,0)) AS CSGNQty, SUM(B.Rate*(B.BalQty-ISNULL(B.CSGNQty,0))) AS AfterCSGNQty FROM Item A INNER JOIN vRPA_TransWOtoRCV C ON A.ItemCode = C.ItemCode LEFT OUTER JOIN ItemUOM B ON(A.ItemCode = B.ItemCode) WHERE A.IsActive = 'T' and C.DocNo = @DocNo GROUP BY C.ItemCode,C.DocNo,C.DocType,C.Code, A.Description, A.Desc2, A.IsActive ORDER BY C.ItemCode";
                        command.CommandText = str;
                        command.Parameters.AddWithValue("@DocNo", (object)wono);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        ItemFGLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }


 
        //public void FilterItemCodeUOMList(List<string> itemCodeList)
        //{
        //    if (this.myDBSetting.ServerType == DBServerType.SQL2000)
        //    {
        //        SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
        //        try
        //        {
        //            sqlConnection.Open();
        //            SqlCommand command = sqlConnection.CreateCommand();
        //            string str = "SELECT DISTINCT UOM FROM ItemUOM WHERE ItemCode IN (SELECT * FROM List2(@ItemCodeList)) ORDER BY UOM";
        //            command.CommandText = str;
        //            command.Parameters.AddWithValue("@ItemCodeList", (object)StringHelper.ToList2String(itemCodeList.ToArray()));
        //            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
        //            ItemUOMLookupEditBuilder lookupEditBuilder = this;
        //            bool lockTaken = false;
        //            try
        //            {
        //                Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
        //                this.myDataTable.Clear();
        //                sqlDataAdapter.Fill(this.myDataTable);
        //                this.AfterFillDataTable();
        //            }
        //            finally
        //            {
        //                if (lockTaken)
        //                    Monitor.Exit((object)lookupEditBuilder);
        //            }
        //        }
        //        catch (SqlException ex)
        //        {
        //            DataError.HandleSqlException(ex);
        //        }
        //        finally
        //        {
        //            sqlConnection.Close();
        //            sqlConnection.Dispose();
        //        }
        //    }
        //}

    }
}



