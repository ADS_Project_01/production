﻿using BCE.AutoCount;
using BCE.Localization;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Reflection;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.Tools.LookupEditBuilders
{
    public class OverheadCodeLookupEditBuilder : BaseAccountLookupEditBuilder
    {
        private static Type myOverheadCodeLookupEditBuilderType = typeof(OverheadCodeLookupEditBuilder);

        public static Type ItemGroupLookupEditBuilderType
        {
            get
            {
                return OverheadCodeLookupEditBuilder.myOverheadCodeLookupEditBuilderType;
            }
            set
            {
                OverheadCodeLookupEditBuilder.myOverheadCodeLookupEditBuilderType = value;
            }
        }

        static OverheadCodeLookupEditBuilder()
        {
        }

        protected OverheadCodeLookupEditBuilder()
        {
            this.myUseCaching = true;
            this.myHasActiveSet = true;
            this.myCacheFileName = "OverheadCodeLookupEditBuilder.Data";
            //this.myUseCaching = true;
            //this.myCacheFileName = "RPA_Overhead.Data";
            this.myCmdString = "SELECT OverheadCode, Description FROM RPA_Overhead ORDER BY OverheadCode";
        }

        public static OverheadCodeLookupEditBuilder Create()
        {
            return OverheadCodeLookupEditBuilder.myOverheadCodeLookupEditBuilderType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, (Binder)null, new Type[0], (ParameterModifier[])null).Invoke(new object[0]) as OverheadCodeLookupEditBuilder;
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
           
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "OverheadCode";
            lookupEdit.ValueMember = "OverheadCode";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("OverheadCode",100, "Overhead Code"));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Description", BCE.Localization.Localizer.GetString((Enum)GridColumnStringId.Description, new object[0]), 200));
            lookupEdit.PopupWidth = 300;
        }

        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.GetItemGroupChangeCount();
        //}
    }

}
