﻿using BCE.AutoCount.XtraUtils;
using BCE.Localization;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Data;
using System.Windows.Forms;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.Tools.LookupEditBuilders
{
    public class ItemBatchForReceiveFGLookupEditBuilder : ItemBatchWO2LookupEditBuilder
    {
        private object myOldValue;
        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.EditValueChanging += new ChangingEventHandler(this.myLookupEdit_EditValueChanging);
            lookupEdit.EditValueChanged += new EventHandler(this.myLookupEdit_EditValueChanged);
        }

        public override void UnlinkEventHandlers(RepositoryItemLookUpEdit lookupEdit)
        {
            if (lookupEdit != null)
            {
                base.UnlinkEventHandlers(lookupEdit);
                lookupEdit.EditValueChanging -= new ChangingEventHandler(this.myLookupEdit_EditValueChanging);
                lookupEdit.EditValueChanged -= new EventHandler(this.myLookupEdit_EditValueChanged);
            }
        }

        protected override void AfterFillDataTable()
        {
            if (this.myHasBatchNo)
            {
                DataRow row = this.myDataTable.NewRow();
                row["BatchNo"] = (object)" ";
                row["Description"] = (object)BCE.Localization.Localizer.GetString((Enum)LookupEditBuilderStringId.AddBatchNo, new object[0]);
                this.myDataTable.Rows.Add(row);
            }
        }

        private void myLookupEdit_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (e.NewValue != null && e.NewValue.ToString() == " ")
                this.myOldValue = e.OldValue;
        }

        private void myLookupEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (sender is LookUpEdit && ((Control)sender).Text == " ")
            {
                ((BaseEdit)sender).EditValue = this.myOldValue;
                FormAddBatchNo formAddBatchNo = new FormAddBatchNo(this.myLastItemCode, this.myDBSetting);
                try
                {
                    if (formAddBatchNo.ShowDialog() == DialogResult.OK)
                    {
                        this.FilterItemCode(this.myLastItemCode, this.myLastDocDate, true);
                        ((BaseEdit)sender).EditValue = (object)formAddBatchNo.AddedBatchNo;
                    }
                }
                finally
                {
                    formAddBatchNo.Dispose();
                }
            }
        }
    }

}
