﻿using BCE.AutoCount;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.Tools.LookupEditBuilders
{
    public class ItemLookupEditBuilder : BaseAccountLookupEditBuilder
    {

        private string myLastDocNo = "!@#!@#!!@#";
        public ItemLookupEditBuilder()
        {
            this.myUseCaching = true;
            this.myHasActiveSet = true;
            this.myCacheFileName = "ItemLookupEditBuilder.Data";
            //this.myUseCaching = true;
            // this.myCacheFileName = "ItemUOM.Data";
            this.myUseSameDataTableObject = false;
            this.myCmdString = "SELECT A.ItemCode, A.Description, A.Desc2, SUM(B.Rate*B.BalQty) AS BalQty, SUM(ISNULL(B.Rate*B.CSGNQty,0)) AS CSGNQty, SUM(B.Rate*(B.BalQty-ISNULL(B.CSGNQty,0))) AS AfterCSGNQty FROM Item A LEFT OUTER JOIN ItemUOM B ON (A.ItemCode=B.ItemCode) WHERE A.IsActive='T' GROUP BY A.ItemCode, A.Description, A.Desc2, A.IsActive ORDER BY A.ItemCode";
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            DecimalSetting orCreate = DecimalSetting.GetOrCreate(this.myDBSetting);
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "ItemCode";
            lookupEdit.ValueMember = "ItemCode";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("ItemCode", "ItemCode", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Description", "Description", 200));
            lookupEdit.Columns.Add(new LookUpColumnInfo("BalQty", "BalQty", 50));

            lookupEdit.PopupWidth = 350;
        }

        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.GetItemUOMChangeCount();
        //}

        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastDocNo = "!@#!@#!!@#";
        }

        public void FilterWONo(string wono)
        {
            if (!(wono == this.myLastDocNo) || this.NeedUpdate())
            {
                this.myLastDocNo = wono;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command = sqlConnection.CreateCommand();
                        string str = "SELECT A.ItemCode, A.Description, A.Desc2, SUM(B.Rate*B.BalQty) AS BalQty, SUM(ISNULL(B.Rate*B.CSGNQty,0)) AS CSGNQty, SUM(B.Rate*(B.BalQty-ISNULL(B.CSGNQty,0))) AS AfterCSGNQty FROM Item A LEFT OUTER JOIN ItemUOM B ON (A.ItemCode=B.ItemCode) WHERE A.IsActive='T' and exists(SELECT 1 FROM vRPA_TransWOtoRCV where vRPA_TransWOtoRCV.ItemCode=A.ItemCode and DocNo=@DocNo) GROUP BY A.ItemCode, A.Description, A.Desc2, A.IsActive ORDER BY A.ItemCode";
                        command.CommandText = str;
                        command.Parameters.AddWithValue("@DocNo", (object)wono);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        ItemLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }


        public void FilterStockIssueRMNo(string sino)
        {
            if (!(sino == this.myLastDocNo) || this.NeedUpdate())
            {
                this.myLastDocNo = sino;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command = sqlConnection.CreateCommand();
                        string str = "SELECT A.ItemCode, A.Description, A.Desc2, SUM(COALESCE(C.Qty,0)-COALESCE(C.TransferedQty,0)) AS BalQty, SUM(ISNULL(B.Rate*B.CSGNQty,0)) AS CSGNQty, SUM(COALESCE(C.Qty,0)-COALESCE(C.TransferedQty,0)) AS AfterCSGNQty FROM Item A LEFT OUTER JOIN ItemUOM B ON(A.ItemCode= B.ItemCode) INNER JOIN RPA_RMDTL C with(NOLOCK) ON(C.ItemCode = A.ItemCode) INNER JOIN RPA_RM D with(NOLOCK) ON(C.DocKey = D.DocKey) WHERE A.IsActive = 'T' AND COALESCE(C.Qty,0)-COALESCE(C.TransferedQty, 0) > 0 AND D.DocNo = @DocNo GROUP BY A.ItemCode, A.Description, A.Desc2, A.IsActive ORDER BY A.ItemCode";
                        command.CommandText = str;
                        command.Parameters.AddWithValue("@DocNo", (object)sino);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        ItemLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }

        //public void FilterItemCodeUOMList(List<string> itemCodeList)
        //{
        //    if (this.myDBSetting.ServerType == DBServerType.SQL2000)
        //    {
        //        SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
        //        try
        //        {
        //            sqlConnection.Open();
        //            SqlCommand command = sqlConnection.CreateCommand();
        //            string str = "SELECT DISTINCT UOM FROM ItemUOM WHERE ItemCode IN (SELECT * FROM List2(@ItemCodeList)) ORDER BY UOM";
        //            command.CommandText = str;
        //            command.Parameters.AddWithValue("@ItemCodeList", (object)StringHelper.ToList2String(itemCodeList.ToArray()));
        //            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
        //            ItemUOMLookupEditBuilder lookupEditBuilder = this;
        //            bool lockTaken = false;
        //            try
        //            {
        //                Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
        //                this.myDataTable.Clear();
        //                sqlDataAdapter.Fill(this.myDataTable);
        //                this.AfterFillDataTable();
        //            }
        //            finally
        //            {
        //                if (lockTaken)
        //                    Monitor.Exit((object)lookupEditBuilder);
        //            }
        //        }
        //        catch (SqlException ex)
        //        {
        //            DataError.HandleSqlException(ex);
        //        }
        //        finally
        //        {
        //            sqlConnection.Close();
        //            sqlConnection.Dispose();
        //        }
        //    }
        //}

    }
}



