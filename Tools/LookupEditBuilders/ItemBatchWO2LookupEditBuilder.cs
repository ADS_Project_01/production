﻿using BCE.AutoCount;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.RegistryID.Misc;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.Tools.LookupEditBuilders
{
    public class ItemBatchWO2LookupEditBuilder : BaseAccountLookupEditBuilder
    {
        protected string myLastItemCode = "!@#!@#!!@#";
        protected DateTime myLastDocDate;
        protected bool myHasBatchNo;

        public ItemBatchWO2LookupEditBuilder()
        {
            //this.myCmdString = "SELECT A.BatchNo, A.Description, A.ManufacturedDate, A.ExpiryDate, A.BalQty, SUM(ISNULL(B.BalQty * C.Rate, 0)) AS CSGNQty, (A.BalQty - SUM(ISNULL(B.BalQty * C.Rate, 0))) AS AfterCSGNQty FROM ItemBatch A LEFT OUTER JOIN CSGNItemBalQty B ON A.ItemCode=B.ItemCode AND A.BatchNo=B.BatchNo LEFT OUTER JOIN ItemUOM C ON B.ItemCode=C.ItemCode AND B.UOM=C.UOM WHERE (1=0) GROUP BY A.ItemCode, A.BatchNo, A.Description, A.ManufacturedDate, A.ExpiryDate, A.BalQty ORDER BY A.BatchNo";
            this.myUseCaching = true;
            this.myHasActiveSet = true;
            this.myCacheFileName = "ItemBatchWO2LookupEditBuilder.Data";
            this.myCmdString = "select c.BatchNo,C.Description,ManufacturedDate,ExpiryDate,sum(BalQty) as BalQty from ItemBatch c where ManufacturedDate>=DateAdd(DAY,-15,GETDATE())  group by c.BatchNo,C.Description,ManufacturedDate,ExpiryDate"; //"select c.BatchNo,C.Description,ManufacturedDate,ExpiryDate,sum(BalQty) as BalQty from vRPA_TransWOtoRCV a inner join Item b on a.ItemCode=b.ItemCode  inner join ItemBatch c on c.ItemCode=b.ItemCode WHERE DocNo='10L'   group by c.BatchNo,C.Description,ManufacturedDate,ExpiryDate";
            this.myHasBatchNo = false;
        }

        protected override void AfterFillDataTable()
        {
            DataRow row = this.myDataTable.NewRow();
            row["Description"] = (object)"(null)";
            this.myDataTable.Rows.Add(row);
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            GeneralSetting orCreate1 = GeneralSetting.GetOrCreate(this.myDBSetting);
            DecimalSetting orCreate2 = DecimalSetting.GetOrCreate(this.myDBSetting);
            int num = 560;
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "BatchNo";
            lookupEdit.ValueMember = "BatchNo";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("BatchNo", BCE.Localization.Localizer.GetString((Enum)GridColumnStringId.BatchNo, new object[0]), 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Description", BCE.Localization.Localizer.GetString((Enum)GridColumnStringId.Description, new object[0]), 200));
            lookupEdit.Columns.Add(new LookUpColumnInfo("ManufacturedDate", BCE.Localization.Localizer.GetString((Enum)GridColumnStringId.ManufacturedDate, new object[0]), 80, FormatType.DateTime, orCreate1.ShortDateFormat, true, HorzAlignment.Default));
            lookupEdit.Columns.Add(new LookUpColumnInfo("ExpiryDate", BCE.Localization.Localizer.GetString((Enum)GridColumnStringId.ExpiryDate, new object[0]), 80, FormatType.DateTime, orCreate1.ShortDateFormat, true, HorzAlignment.Default));
            lookupEdit.Columns.Add(new LookUpColumnInfo("BalQty", BCE.Localization.Localizer.GetString((Enum)GridColumnStringId.BalQty, new object[0]), 100, FormatType.Numeric, "n" + orCreate2.QuantityDecimal.ToString(), true, HorzAlignment.Far));
            if (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.Consignment.Enable)
            {
                lookupEdit.Columns.Add(new LookUpColumnInfo("CSGNQty", BCE.Localization.Localizer.GetString((Enum)GridColumnStringId.CSGNQty, new object[0]), 100, FormatType.Numeric, "n" + orCreate2.QuantityDecimal.ToString(), true, HorzAlignment.Far));
                lookupEdit.Columns.Add(new LookUpColumnInfo("AfterCSGNQty", BCE.Localization.Localizer.GetString((Enum)GridColumnStringId.AfterCSGNQty, new object[0]), 100, FormatType.Numeric, "n" + orCreate2.QuantityDecimal.ToString(), true, HorzAlignment.Far));
                num += 160;
            }
            lookupEdit.PopupWidth = num;
        }

        protected override int GetChangeCount()
        {
            return this.myChangeCount.GetItemBatchChangeCount();
        }

        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastItemCode = "!@#!@#!!@#";
        }

        protected override void Refresh()
        {
            if (this.NeedUpdate())
                this.FilterItemCode(this.myLastItemCode, this.myLastDocDate);
        }

        public void FilterItemCode(string itemCode)
        {
            this.FilterItemCode(itemCode, DateTime.MinValue, true);
        }

        public void FilterItemCode(string itemCode, DateTime docDate)
        {
            this.FilterItemCode(itemCode, docDate, true);
        }

        public void FilterItemCode(string itemCode, bool alwaysFill)
        {
            this.FilterItemCode(itemCode, DateTime.MinValue, alwaysFill);
        }

        public void FilterItemCode(string itemCode, DateTime docDate, bool alwaysFill)
        {
            if (alwaysFill || !(itemCode == this.myLastItemCode) || (!(docDate == this.myLastDocDate) || this.NeedUpdate()))
            {
                bool boolean = DBRegistry.Create(this.myDBSetting).GetBoolean((IRegistryID)new ShowZeroBalanceQtyBatchNo());
                this.myLastItemCode = itemCode;
                this.myLastDocDate = docDate;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command1 = sqlConnection.CreateCommand();
                        string str = "SELECT HasBatchNo FROM Item WHERE ItemCode=@ItemCode";
                        command1.CommandText = str;
                        command1.Parameters.AddWithValue("@ItemCode", (object)itemCode);
                        SqlDataReader sqlDataReader = command1.ExecuteReader();
                        if (sqlDataReader.HasRows)
                        {
                            sqlDataReader.Read();
                            this.myHasBatchNo = sqlDataReader.GetString(0) == "T";
                        }
                        else
                            this.myHasBatchNo = false;
                        sqlDataReader.Close();
                        SqlCommand command2 = sqlConnection.CreateCommand();
                        //if (docDate != DateTime.MinValue)
                        //{
                        //    if (boolean)
                        //        command2.CommandText = "select distinct c.BatchNo,C.Description,ManufacturedDate,ExpiryDate,sum(BalQty) as BalQty from vRPA_TransWOtoRCV a inner join Item b on a.ItemCode=b.ItemCode  inner join ItemBatch c on c.ItemCode=b.ItemCode WHERE a.DocNo=@DocNo group by c.BatchNo,C.Description,ManufacturedDate,ExpiryDate ORDER BY C.BatchNo";
                        //    else
                        //        command2.CommandText = "select distinct  c.BatchNo,C.Description,ManufacturedDate,ExpiryDate,sum(BalQty) as BalQty from vRPA_TransWOtoRCV a inner join Item b on a.ItemCode=b.ItemCode  inner join ItemBatch c on c.ItemCode=b.ItemCode WHERE a.DocNo=@DocNo  AND (BalQty<>0) group by c.BatchNo,C.Description,ManufacturedDate,ExpiryDate ORDER BY C.BatchNo";
                        //    command2.Parameters.AddWithValue("@DocDate", (object)docDate);
                        //}
                        //else if (boolean)
                        //    command2.CommandText = "select distinct  c.BatchNo,C.Description,ManufacturedDate,ExpiryDate,sum(BalQty) as BalQty from vRPA_TransWOtoRCV a inner join Item b on a.ItemCode=b.ItemCode  inner join ItemBatch c on c.ItemCode=b.ItemCode WHERE a.DocNo=@DocNo group by c.BatchNo,C.Description,ManufacturedDate,ExpiryDate ORDER BY c.BatchNo";
                        //else
                        //    command2.CommandText = "select distinct  c.BatchNo,C.Description,ManufacturedDate,ExpiryDate,sum(BalQty) as BalQty from vRPA_TransWOtoRCV a inner join Item b on a.ItemCode=b.ItemCode  inner join ItemBatch c on c.ItemCode=b.ItemCode WHERE a.DocNo=@DocNo AND BalQty<>0 group by c.BatchNo,C.Description,ManufacturedDate,ExpiryDate ORDER BY c.BatchNo";


                        command2.CommandText = "select c.BatchNo,C.Description,ManufacturedDate,ExpiryDate,sum(BalQty) as BalQty from ItemBatch c where ManufacturedDate >= DateAdd(DAY, -15, GETDATE()) group by c.BatchNo,C.Description,ManufacturedDate,ExpiryDate"; 
                       // command2.Parameters.AddWithValue("@ItemCode", (object)itemCode);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command2);
                        ItemBatchWO2LookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        BCE.Data.DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }
    }

}
