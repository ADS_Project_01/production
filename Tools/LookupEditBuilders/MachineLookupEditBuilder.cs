﻿using BCE.AutoCount;
using BCE.Localization;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Reflection;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Popup;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.Layout;
using BCE.AutoCount.Controller;
namespace Production.Tools.LookupEditBuilders
{
    public class MachineLookupEditBuilder : BaseAccountLookupEditBuilder
    {
        private static Type myMachineLookupEditBuilderType = typeof(MachineLookupEditBuilder);

        public static Type ItemGroupLookupEditBuilderType
        {
            get
            {
                return MachineLookupEditBuilder.myMachineLookupEditBuilderType;
            }
            set
            {
                MachineLookupEditBuilder.myMachineLookupEditBuilderType = value;
            }
        }

        static MachineLookupEditBuilder()
        {
        }
        private void lookupEdit_Popup(object sender, EventArgs e)
        {
            LookUpEdit edit = sender as LookUpEdit;
            PopupLookUpEditForm popupForm = this.GetPopupForm(edit);
            SimpleButton simpleButton1 = (SimpleButton)null;
            foreach (Control control in (ArrangedElementCollection)popupForm.Controls)
            {
                if (control is SimpleButton && control.Text ==BCE.Localization.Localizer.GetString((Enum)GridButtonStringId.New, new object[0]))
                {
                    simpleButton1 = (SimpleButton)control;
                    break;
                }
            }
            if (simpleButton1 != null)
                popupForm.Controls.Remove((Control)simpleButton1);
            SimpleButton simpleButton2 = new SimpleButton();
            simpleButton2.Left = popupForm.CloseButton.Left + popupForm.CloseButton.Width + 2;
            simpleButton2.Width = 36;
            simpleButton2.Top = popupForm.CloseButton.Top;
            simpleButton2.Height = popupForm.CloseButton.Height;
            simpleButton2.Text =BCE.Localization.Localizer.GetString((Enum)GridButtonStringId.New, new object[0]);
            simpleButton2.ToolTip = "Alt-Insert";
            simpleButton2.Tag = (object)edit;
            simpleButton2.TabStop = false;
           // simpleButton2.Click += new EventHandler(this.newButton_Click);
            popupForm.Controls.Add((Control)simpleButton2);
            popupForm.CloseButton.LocationChanged -= new EventHandler(this.CloseButton_LocationChanged);
            popupForm.CloseButton.LocationChanged += new EventHandler(this.CloseButton_LocationChanged);
        }

        private void CloseButton_LocationChanged(object sender, EventArgs e)
        {
            SimpleButton simpleButton1 = (SimpleButton)sender;
            PopupLookUpEditForm popupLookUpEditForm = (PopupLookUpEditForm)simpleButton1.Parent;
            SimpleButton simpleButton2 = (SimpleButton)null;
            foreach (Control control in (ArrangedElementCollection)popupLookUpEditForm.Controls)
            {
                if (control is SimpleButton && control.Text ==BCE.Localization.Localizer.GetString((Enum)GridButtonStringId.New, new object[0]))
                {
                    simpleButton2 = (SimpleButton)control;
                    break;
                }
            }
            if (simpleButton2 != null)
            {
                simpleButton2.Left = simpleButton1.Left + simpleButton1.Width + 2;
                simpleButton2.Top = simpleButton1.Top;
            }
        }
        protected MachineLookupEditBuilder()
        {
            this.myUseCaching = true;
            this.myHasActiveSet = true;
            this.myCacheFileName = "RPA_Machine.Data";
            this.myCmdString = "SELECT MachineCode, Description FROM RPA_Machine ORDER BY MachineCode";
        }

        public static MachineLookupEditBuilder Create()
        {
            return MachineLookupEditBuilder.myMachineLookupEditBuilderType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, (Binder)null, new Type[0], (ParameterModifier[])null).Invoke(new object[0]) as MachineLookupEditBuilder;
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.Popup += new EventHandler(this.lookupEdit_Popup);
            lookupEdit.KeyDown += new KeyEventHandler(this.lookupEdit_KeyDown);
            lookupEdit.DisplayMember = "MachineCode";
            lookupEdit.ValueMember = "MachineCode";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("MachineCode", 100, "Machine Code"));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Description", BCE.Localization.Localizer.GetString((Enum)GridColumnStringId.Description, new object[0]), 200));
            lookupEdit.PopupWidth = 300;            

        }
        private void lookupEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (sender is LookUpEdit)
            {
                LookUpEdit lookUpEdit = sender as LookUpEdit;
                if (lookUpEdit.IsPopupOpen && e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space) && e.Alt)
                {
                   // string str = CommandCenter..NewDebtor();
                    //if (str.Length > 0)
                    {
                        this.Refresh(lookUpEdit.Properties);
                      //  lookUpEdit.EditValue = (object)str;
                        lookUpEdit.ClosePopup();
                    }
                }
            }
        }
        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.get();
        //}
    }

}
