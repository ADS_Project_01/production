﻿using BCE.AutoCount;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.Tools.LookupEditBuilders
{
    public class ItemBOMLookupEditBuilder : BaseAccountLookupEditBuilder
    {
        private string myLastItemCode = "!@#!@#!!@#";

        public ItemBOMLookupEditBuilder()
        {
            this.myUseCaching = true;
            this.myHasActiveSet = true;
            this.myCacheFileName = "ItemBOMLookupEditBuilder.Data";
            //this.myUseCaching = true;
            // this.myCacheFileName = "ItemUOM.Data";
            this.myUseSameDataTableObject = false;            
            this.myCmdString = "SELECT BOMCode, Description,Qty,CostType,Cost FROM RPA_ItemBOM WHERE IsActive='T'";
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            DecimalSetting orCreate = DecimalSetting.GetOrCreate(this.myDBSetting);
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "BOMCode";
            lookupEdit.ValueMember = "BOMCode";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("BOMCode", "BOM Code", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Description", "Description", 200));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Qty", "Qty", 50));
            lookupEdit.Columns.Add(new LookUpColumnInfo("CostType", "Cost Type", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Cost", "Cost", 100));
            
            lookupEdit.PopupWidth = 550;
        }

        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.GetItemUOMChangeCount();
        //}

        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastItemCode = "!@#!@#!!@#";
        }

        public void FilterItemCode(string itemCode)
        {
            if (!(itemCode == this.myLastItemCode) || this.NeedUpdate())
            {
                this.myLastItemCode = itemCode;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command = sqlConnection.CreateCommand();
                        string str = "SELECT BOMCode, Description,Qty,CostType,Cost FROM RPA_ItemBOM WHERE IsActive='T' and ItemCode=@ItemCode ORDER BY BOMCode";
                        command.CommandText = str;
                        command.Parameters.AddWithValue("@ItemCode", (object)itemCode);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        ItemBOMLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }

        //public void FilterItemCodeUOMList(List<string> itemCodeList)
        //{
        //    if (this.myDBSetting.ServerType == DBServerType.SQL2000)
        //    {
        //        SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
        //        try
        //        {
        //            sqlConnection.Open();
        //            SqlCommand command = sqlConnection.CreateCommand();
        //            string str = "SELECT DISTINCT UOM FROM ItemUOM WHERE ItemCode IN (SELECT * FROM List2(@ItemCodeList)) ORDER BY UOM";
        //            command.CommandText = str;
        //            command.Parameters.AddWithValue("@ItemCodeList", (object)StringHelper.ToList2String(itemCodeList.ToArray()));
        //            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
        //            ItemUOMLookupEditBuilder lookupEditBuilder = this;
        //            bool lockTaken = false;
        //            try
        //            {
        //                Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
        //                this.myDataTable.Clear();
        //                sqlDataAdapter.Fill(this.myDataTable);
        //                this.AfterFillDataTable();
        //            }
        //            finally
        //            {
        //                if (lockTaken)
        //                    Monitor.Exit((object)lookupEditBuilder);
        //            }
        //        }
        //        catch (SqlException ex)
        //        {
        //            DataError.HandleSqlException(ex);
        //        }
        //        finally
        //        {
        //            sqlConnection.Close();
        //            sqlConnection.Dispose();
        //        }
        //    }
        //}

    }
}



