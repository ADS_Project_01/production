﻿using BCE.AutoCount;
using BCE.AutoCount;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.AutoCount.SearchFilter;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.Tools.LookupEditBuilders
{
    public class ItemRMLookupEditBuilder : BaseAccountLookupEditBuilder
    {

        private string myLastDocNo = "!@#!@#!!@#";
        public ItemRMLookupEditBuilder()
        {
            this.myUseCaching = true;
            this.myHasActiveSet = true;
            this.myCacheFileName = "ItemRMLookupEditBuilder.Data";
            //this.myUseCaching = true;
            // this.myCacheFileName = "ItemUOM.Data";
            this.myUseSameDataTableObject = false;
            this.myCmdString = "SELECT C.ItemCode,C.DocNo AS WONo,C.Code, A.Description, A.Desc2, SUM(B.Rate*B.BalQty) AS BalQty, SUM(ISNULL(B.Rate*B.CSGNQty,0)) AS CSGNQty, SUM(B.Rate*(B.BalQty-ISNULL(B.CSGNQty,0))) AS AfterCSGNQty FROM Item A INNER JOIN (SELECT RPA_WO.DocNo,DtlKey as Code, RPA_WODtl.* from RPA_WO inner join RPA_WODtl on RPA_WO.DocKey=RPA_WODtl.DocKey where Status='InProcess') C ON A.ItemCode = C.ItemCode LEFT OUTER JOIN ItemUOM B ON(A.ItemCode = B.ItemCode) WHERE A.IsActive = 'T' GROUP BY C.ItemCode,C.DocNo,C.Code, A.Description, A.Desc2, A.IsActive ORDER BY C.ItemCode";
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            DecimalSetting orCreate = DecimalSetting.GetOrCreate(this.myDBSetting);
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "ItemCode";
            lookupEdit.ValueMember = "ItemCode";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("ItemCode", "ItemCode", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Description", "Description", 200));
            lookupEdit.Columns.Add(new LookUpColumnInfo("WONo", "WONo", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("DocType", "DocType", 50));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Code", "Code", 50));
            lookupEdit.Columns.Add(new LookUpColumnInfo("BalQty", "BalQty", 50));

            lookupEdit.PopupWidth = 550;
        }

        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.GetItemUOMChangeCount();
        //}

        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastDocNo = "!@#!@#!!@#";
        }
        private string GetWOWhereSql(SqlCommand cmd, BCE.AutoCount.SearchFilter.Filter wofilter)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.AddFilter(wofilter);
            return searchCriteria.BuildSQL((IDbCommand)cmd);
        }
        public void FilterWONo(BCE.AutoCount.SearchFilter.Filter wofilter)
        {
            

            if (this.NeedUpdate())
            {
             //   this.myLastDocNo = wono;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command = sqlConnection.CreateCommand();
                        string whereSql = this.GetWOWhereSql(command, wofilter);
                        if (whereSql.Length != 0)
                            whereSql = "AND " + whereSql;
                        //whereSql = whereSql.Replace("a.", "c.");
                        whereSql = whereSql.Replace("b.", "vRPA_TransWOtoSIRM.");
                        string str = "SELECT C.ItemCode,C.DocNo AS WONo,C.Code,C.ID,C.ItemType, A.Description, A.Desc2, SUM(B.Rate*B.BalQty) AS BalQty, SUM(ISNULL(B.Rate*B.CSGNQty,0)) AS CSGNQty, SUM(B.Rate*(B.BalQty-ISNULL(B.CSGNQty,0))) AS AfterCSGNQty FROM Item A INNER JOIN (SELECT DtlKey as Code, * from vRPA_TransWOtoSIRM where 1=1 " + whereSql + ") C ON A.ItemCode = C.ItemCode LEFT OUTER JOIN ItemUOM B ON(A.ItemCode = B.ItemCode) WHERE A.IsActive = 'T'  GROUP BY C.ItemCode,C.DocNo,C.ItemType,C.ID,C.Code, A.Description, A.Desc2, A.IsActive ORDER BY C.ItemCode";
                        command.CommandText = str;
                        //command.Parameters.AddWithValue("@DocNo", (object)wono);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        ItemRMLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }


        public void FilterFrozen(string wono)
        {
            if (!(wono == this.myLastDocNo) || this.NeedUpdate())
            {
                this.myLastDocNo = wono;
                if (this.myDBSetting.ServerType == DBServerType.SQL2000)
                {
                    SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
                    try
                    {
                        sqlConnection.Open();
                        SqlCommand command = sqlConnection.CreateCommand();
                        string str = "SELECT C.ItemCode,C.DocNo AS WONo,C.DocType,C.Code, A.Description, A.Desc2, SUM(C.BalanceQty) AS BalQty, SUM(ISNULL(B.Rate*B.CSGNQty,0)) AS CSGNQty, SUM(B.Rate*(B.BalQty-ISNULL(B.CSGNQty,0))) AS AfterCSGNQty FROM Item A INNER JOIN vRPA_TransWOtoFrozen C ON A.ItemCode = C.ItemCode LEFT OUTER JOIN ItemUOM B ON(A.ItemCode = B.ItemCode) WHERE A.IsActive = 'T' and C.DocNo =@DocNo GROUP BY C.ItemCode,C.DocNo,C.DocType,C.Code, A.Description, A.Desc2, A.IsActive ORDER BY C.ItemCode";
                        command.CommandText = str;
                        command.Parameters.AddWithValue("@DocNo", (object)wono);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        ItemRMLookupEditBuilder lookupEditBuilder = this;
                        bool lockTaken = false;
                        try
                        {
                            Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
                            this.myDataTable.Clear();
                            sqlDataAdapter.Fill(this.myDataTable);
                            this.AfterFillDataTable();
                        }
                        finally
                        {
                            if (lockTaken)
                                Monitor.Exit((object)lookupEditBuilder);
                        }
                    }
                    catch (SqlException ex)
                    {
                        DataError.HandleSqlException(ex);
                    }
                    finally
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
            }
        }

        //public void FilterItemCodeUOMList(List<string> itemCodeList)
        //{
        //    if (this.myDBSetting.ServerType == DBServerType.SQL2000)
        //    {
        //        SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
        //        try
        //        {
        //            sqlConnection.Open();
        //            SqlCommand command = sqlConnection.CreateCommand();
        //            string str = "SELECT DISTINCT UOM FROM ItemUOM WHERE ItemCode IN (SELECT * FROM List2(@ItemCodeList)) ORDER BY UOM";
        //            command.CommandText = str;
        //            command.Parameters.AddWithValue("@ItemCodeList", (object)StringHelper.ToList2String(itemCodeList.ToArray()));
        //            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
        //            ItemUOMLookupEditBuilder lookupEditBuilder = this;
        //            bool lockTaken = false;
        //            try
        //            {
        //                Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
        //                this.myDataTable.Clear();
        //                sqlDataAdapter.Fill(this.myDataTable);
        //                this.AfterFillDataTable();
        //            }
        //            finally
        //            {
        //                if (lockTaken)
        //                    Monitor.Exit((object)lookupEditBuilder);
        //            }
        //        }
        //        catch (SqlException ex)
        //        {
        //            DataError.HandleSqlException(ex);
        //        }
        //        finally
        //        {
        //            sqlConnection.Close();
        //            sqlConnection.Dispose();
        //        }
        //    }
        //}

    }
}



