﻿using BCE.AutoCount;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
namespace Production.Tools.LookupEditBuilders
{
    public class ItemWithBOMLookupEditBuilder : BaseAccountLookupEditBuilder
    {

        private string myLastDocNo = "!@#!@#!!@#";
        public ItemWithBOMLookupEditBuilder()
        {
            this.myUseCaching = true;
            this.myHasActiveSet = true;
            this.myCacheFileName = "ItemWithBOMLookupEditBuilder2.Data";
            this.myUseCaching = true;
            //this.myCacheFileName = "ItemUOM.Data";
            this.myUseSameDataTableObject = false;
            this.myCmdString = "SELECT * from vRPA_ItemWithBOM ORDER BY ItemBOMCode";
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit lookupEdit)
        {
            DecimalSetting orCreate = DecimalSetting.GetOrCreate(this.myDBSetting);
            base.SetupLookupEdit(lookupEdit);
            lookupEdit.DisplayMember = "ItemBOMCode";
            lookupEdit.ValueMember = "ItemBOMCode";
            lookupEdit.Columns.Clear();
            lookupEdit.Columns.Add(new LookUpColumnInfo("ItemBOMCode", "ItemBOMCode", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("ItemCode", "ItemCode", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("Description", "Description", 200));
            lookupEdit.Columns.Add(new LookUpColumnInfo("BOMCode", "BOM Code", 100));
            lookupEdit.Columns.Add(new LookUpColumnInfo("BOMDesc", "BOM Desc", 200));

            lookupEdit.PopupWidth = 700;
        }

        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.GetItemUOMChangeCount();
        //}

        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastDocNo = "!@#!@#!!@#";
        }

              //public void FilterItemCodeUOMList(List<string> itemCodeList)
        //{
        //    if (this.myDBSetting.ServerType == DBServerType.SQL2000)
        //    {
        //        SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
        //        try
        //        {
        //            sqlConnection.Open();
        //            SqlCommand command = sqlConnection.CreateCommand();
        //            string str = "SELECT DISTINCT UOM FROM ItemUOM WHERE ItemCode IN (SELECT * FROM List2(@ItemCodeList)) ORDER BY UOM";
        //            command.CommandText = str;
        //            command.Parameters.AddWithValue("@ItemCodeList", (object)StringHelper.ToList2String(itemCodeList.ToArray()));
        //            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
        //            ItemUOMLookupEditBuilder lookupEditBuilder = this;
        //            bool lockTaken = false;
        //            try
        //            {
        //                Monitor.Enter((object)lookupEditBuilder, ref lockTaken);
        //                this.myDataTable.Clear();
        //                sqlDataAdapter.Fill(this.myDataTable);
        //                this.AfterFillDataTable();
        //            }
        //            finally
        //            {
        //                if (lockTaken)
        //                    Monitor.Exit((object)lookupEditBuilder);
        //            }
        //        }
        //        catch (SqlException ex)
        //        {
        //            DataError.HandleSqlException(ex);
        //        }
        //        finally
        //        {
        //            sqlConnection.Close();
        //            sqlConnection.Dispose();
        //        }
        //    }
        //}

    }
}



