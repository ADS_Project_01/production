﻿using BCE.AutoCount.Common;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using BCE.AutoCount.Tools;
namespace Production.Tools.StockCosting
{
    public class RecalculateStockCostingSQL : RecalculateStockCosting
    {
        private string GetWhereTransactionSQL(SqlCommand cmd, RecalculateStockCostingCriteria aCriteria)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BCE.AutoCount.SearchFilter.Filter dateFilter = aCriteria.DateFilter;
            searchCriteria.AddFilter(dateFilter);
          
            SqlCommand sqlCommand = cmd;
            string str1 = searchCriteria.BuildSQL((IDbCommand)sqlCommand);           
            if (str1.Length > 0)
                str1 = str1 ;

            if (str1.Length > 0)
                return "AND " + str1;
            else
                return "";
        }
        private string GetWhereSQL(SqlCommand cmd, RecalculateStockCostingCriteria aCriteria)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BCE.AutoCount.SearchFilter.Filter itemFilter = aCriteria.ItemFilter;
            searchCriteria.AddFilter(itemFilter);
            BCE.AutoCount.SearchFilter.Filter itemGroupFilter = aCriteria.ItemGroupFilter;
            searchCriteria.AddFilter(itemGroupFilter);
            BCE.AutoCount.SearchFilter.Filter itemTypeFilter = aCriteria.ItemTypeFilter;
            searchCriteria.AddFilter(itemTypeFilter);
            SqlCommand sqlCommand = cmd;
            string str1 = searchCriteria.BuildSQL((IDbCommand)sqlCommand);
            if (!aCriteria.FixedCost || !aCriteria.WeightedAverage || (!aCriteria.FIFO || !aCriteria.LIFO) || !aCriteria.MostRecently)
            {
                string str2 = "";
                if (aCriteria.FixedCost)
                    str2 = str2 + "0,";
                if (aCriteria.WeightedAverage)
                    str2 = str2 + "1,";
                if (aCriteria.FIFO)
                    str2 = str2 + "2,";
                if (aCriteria.LIFO)
                    str2 = str2 + "3,";
                if (aCriteria.MostRecently)
                    str2 = str2 + "4,";
                string str3 = str2.Remove(str2.Length - 1, 1);
                if (str1.Length > 0)
                    str1 = str1 + " AND";
                str1 = str1 + " CostingMethod IN (" + str3 + ")";
            }
            if (str1.Length > 0)
                str1 = str1 + " AND";
            string str4 = str1 + " StockControl = 'T'";
            if (str4.Length > 0)
                return "WHERE " + str4;
            else
                return "";
        }

        public override void Recalculate(RecalculateStockCostingCriteria criteria, bool changeToNewCostingMethod, CostingMethod newCostingMethod)
        {
            string successItems = string.Empty;
            string failItems = string.Empty;
            DataTable dataTable = new DataTable();
            DBSetting dbSetting = this.myDBSetting.StartTransaction();
            this.myCancel = false;
            try
            {
                SqlCommand command = dbSetting.CreateCommand();
                command.CommandText = "SELECT ItemCode, CostingMethod FROM Item " + this.GetWhereSQL(command, criteria) + " ORDER BY ItemCode";
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
                if (changeToNewCostingMethod)
                {
                    foreach (DataRow dataRow in (InternalDataCollectionBase)dataTable.Rows)
                        dataRow["CostingMethod"] = (object)(sbyte)newCostingMethod;
                    SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(adapter);
                    adapter.Update(dataTable);
                }
                dbSetting.Commit();
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                dbSetting.EndTransaction();
            }
            int count = dataTable.Rows.Count;
            for (int index = 0; index < count; ++index)
            {
                string itemCode = dataTable.Rows[index][0].ToString();
                if (this.myItemProgress != null)
                    this.myItemProgress(count, itemCode, index + 1);
                if (this.Recalculate(criteria,itemCode, criteria.AutoReorder, criteria.EndDate, false))
                    successItems = successItems + string.Format("{0}, ", (object)itemCode);
                else
                    failItems = failItems + string.Format("{0}, ", (object)itemCode);
                if (this.myCancel)
                    break;
            }
            if (successItems.Length > 1)
                successItems = successItems.Substring(0, successItems.Length - 2);
            if (failItems.Length > 1)
                failItems = failItems.Substring(0, failItems.Length - 2);
            this.PostAuditLog(criteria, changeToNewCostingMethod, newCostingMethod, successItems, failItems, this.myDBSetting);
        }

        public override bool Recalculate(RecalculateStockCostingCriteria criteria, string itemCode, bool autoReorder, DateTime endDate, bool postAuditLog)
        {
            bool flag = false;
            StringBuilder stringBuilder1 = new StringBuilder();
            DBSetting dbSetting1 = this.myDBSetting.StartTransaction();
            RecalculateStockCosting.BeginRecalculateStockCosting();
            try
            {
                UTDCosting utdCosting = UTDCosting.Create(dbSetting1);
                SqlCommand command = dbSetting1.CreateCommand("DELETE FROM UTDStockCostDTL WHERE UTDStockCostKey IN (SELECT UTDStockCostKey FROM UTDStockCost WHERE ItemCode=@ItemCode)", new object[0]);
                command.Parameters.AddWithValue("@ItemCode", (object)itemCode);
                command.ExecuteNonQuery();
                command.CommandText = "DELETE FROM UTDStockCost WHERE ItemCode=@ItemCode";
                command.ExecuteNonQuery();
                command.CommandText = "DELETE FROM FIFOCost WHERE StockDtlKey IN (SELECT StockDTLKey FROM StockDTL WHERE ItemCode=@ItemCode)";
                command.ExecuteNonQuery();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                DataTable dataTable = new DataTable();
                DataRow[] dataRowArray1;
                if (autoReorder)
                {
                    string str = !this.myIgnoreLocationInCostingCalculation || !this.myIgnoreBatchNumberInCostingCalculation ? (!this.myIgnoreLocationInCostingCalculation ? (!this.myIgnoreBatchNumberInCostingCalculation ? "A.UOM, A.Location, A.BatchNo, A.DocDate, A.Seq" : "A.UOM, A.Location, A.DocDate, A.Seq") : "A.UOM, A.BatchNo, A.DocDate, A.Seq") : "A.UOM, A.DocDate, A.Seq";
                    if (endDate == DateTime.MinValue)
                    {
                        command.CommandText = "SELECT A.*, B.Rate FROM StockDTL A LEFT OUTER JOIN ItemUOM B ON (A.ItemCode=B.ItemCode AND A.UOM=B.UOM) WHERE A.ItemCode=@ItemCode " + this.GetWhereTransactionSQL(command, criteria) + " ORDER By " + str;
                    }
                    else
                    {
                        command.CommandText = "SELECT A.*, B.Rate FROM StockDTL A LEFT OUTER JOIN ItemUOM B ON (A.ItemCode=B.ItemCode AND A.UOM=B.UOM) WHERE A.ItemCode=@ItemCode AND A.DocDate <= @EndDate " + this.GetWhereTransactionSQL(command, criteria) + " ORDER By " + str;
                        command.Parameters.AddWithValue("@EndDate", (object)endDate);
                    }
                    sqlDataAdapter.Fill(dataTable);
                    dataRowArray1 = this.Reorder(dataTable);
                }
                else
                {
                    if (endDate == DateTime.MinValue)
                    {
                        command.CommandText = "SELECT A.*, B.Rate FROM StockDTL A LEFT OUTER JOIN ItemUOM B ON (A.ItemCode=B.ItemCode AND A.UOM=B.UOM) WHERE A.ItemCode=@ItemCode " + this.GetWhereTransactionSQL(command, criteria) + " ORDER By A.DocDate, A.Seq";
                    }
                    else
                    {
                        command.CommandText = "SELECT A.*, B.Rate FROM StockDTL A LEFT OUTER JOIN ItemUOM B ON (A.ItemCode=B.ItemCode AND A.UOM=B.UOM) WHERE A.ItemCode=@ItemCode AND A.DocDate <= @EndDate " + this.GetWhereTransactionSQL(command, criteria) + " ORDER By A.DocDate, A.Seq";
                        command.Parameters.AddWithValue("@EndDate", (object)endDate);
                    }
                    sqlDataAdapter.Fill(dataTable);
                    dataRowArray1 = dataTable.Select();
                }
                StockHelper helper = StockHelper.Create(dbSetting1);
                CostingMethod costingMethod = helper.GetCostingMethod(itemCode);
                int length = dataRowArray1.Length;
                for (int index1 = 0; index1 < length && !this.myCancel; ++index1)
                {
                    DataRow dataRow = dataRowArray1[index1];
                    if (this.myTransactionProgress != null)
                        this.myTransactionProgress(length, index1 + 1);
                    bool isWriteOff = dataRow["DocType"].ToString() == "WO";
                    long stockDTLKey = BCE.Data.Convert.ToInt64(dataRow["StockDTLKey"]);
                    FIFOCost fifoCost = FIFOCost.Create(dbSetting1, stockDTLKey);
                    Decimal qty = BCE.Data.Convert.ToDecimal(dataRow["Qty"]);
                    long num1 = BCE.Data.Convert.ToInt64(dataRow["ReferTo"]);
                    if (num1 > 0L && qty > Decimal.Zero)
                    {
                        DataRow[] dataRowArray2 = dataTable.Select(string.Format("StockDTLKey = {0}", (object)num1));
                        if (dataRowArray2.Length != 0)
                        {
                            if (string.Compare(dataRow["UOM"].ToString(), dataRowArray2[0]["UOM"].ToString(), true) == 0)
                            {
                                dataRow["Cost"] = dataRowArray2[0]["Cost"];
                            }
                            else
                            {
                                Decimal num2 = BCE.Data.Convert.ToDecimal(dataRowArray2[0]["Cost"]) * BCE.Data.Convert.ToDecimal(dataRow["Rate"]) / BCE.Data.Convert.ToDecimal(dataRowArray2[0]["Rate"]);
                                dataRow["Cost"] = (object)num2;
                            }
                        }
                    }
                    ComputedCost computedCost = new ComputedCost();
                    UTDCostHelper utdCostHelper = UTDCostHelper.Create(dbSetting1, dataRow["ItemCode"].ToString(), dataRow["UOM"].ToString(), this.myIgnoreLocationInCostingCalculation ? this.myDefaultLocationInCostingCalculation : dataRow["Location"].ToString(), this.myIgnoreBatchNumberInCostingCalculation ? (object)DBNull.Value : dataRow["BatchNo"], BCE.Data.Convert.ToDateTime(dataRow["DocDate"]), helper, costingMethod);
                    if (qty >= Decimal.Zero || dataRow["DocType"].ToString() == "OB")
                    {
                        Decimal cost = num1 <= 0L ? BCE.Data.Convert.ToDecimal(dataRow["InputCost"]) : BCE.Data.Convert.ToDecimal(dataRow["Cost"]);
                        computedCost = utdCosting.AddInQty(utdCostHelper, qty, cost);
                        if (utdCostHelper.CostingMethod == CostingMethod.FixedCost)
                        {
                            Decimal fixedCost = utdCosting.Helper.GetFixedCost(utdCostHelper.ItemCode, utdCostHelper.UOM);
                            dataRow["Cost"] = (object)fixedCost;
                            computedCost.TotalCost = qty * fixedCost;
                            dataRow["CostType"] = (object)0;
                        }
                        else if (utdCostHelper.CostingMethod == CostingMethod.MostRecently)
                        {
                            MostRecentlyCost mostRecentlyCost = utdCostHelper.GetMostRecentlyCost();
                            Decimal num2 = mostRecentlyCost.Cost;
                            dataRow["Cost"] = (object)num2;
                            computedCost.TotalCost = qty * num2;
                            dataRow["CostType"] = (object)(sbyte)mostRecentlyCost.CostType;
                        }
                        else
                        {
                            dataRow["CostType"] = BCE.Data.Convert.ToInt64(dataRow["ReferTo"]) <= 0L ? (object)4 : (object)5;
                            if (qty != Decimal.Zero)
                                dataRow["Cost"] = (object)(computedCost.TotalCost / qty);
                        }
                    }
                    else if (qty < Decimal.Zero)
                    {
                        computedCost = utdCosting.AddOutQty(utdCostHelper, -qty, isWriteOff);
                        dataRow["Cost"] = (object)(computedCost.TotalCost / qty);
                        dataRow["CostType"] = (object)(sbyte)computedCost.CostType;
                    }
                    dataRow["TotalCost"] = (object)computedCost.TotalCost;
                    DataTable fifoTable = computedCost.FIFOTable;
                    fifoCost.CopyFIFOTable(fifoTable);
                    dataRow["AdjustedCost"] = (object)computedCost.AdjustedCost;
                    fifoCost.Save();
                    if (this.NeedToUpdateDocumentCost && this.UpdateDocumentCost != null)
                    {
                        string docType = dataRow["DocType"].ToString();
                        if (docType == "DO" || docType == "IV" || (docType == "CS" || docType == "CN") || (docType == "DN" || docType == "DR" || docType == "OS"))
                        {
                            long dtlKey = BCE.Data.Convert.ToInt64(dataRow["DtlKey"]);
                            try
                            {
                                this.UpdateDocumentCost(docType, dtlKey, -computedCost.TotalCost);
                            }
                            catch
                            {
                            }
                        }
                    }
                    if (this.NeedToUpdateStockDocumentCost && this.UpdateStockDocumentCost != null)
                    {
                        string docType = dataRow["DocType"].ToString();
                        if (docType == "SA" || docType == "SI" || (docType == "ST" || docType == "AS"))
                        {
                            long docKey = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
                            long dtlKey = BCE.Data.Convert.ToInt64(dataRow["DtlKey"]);
                            try
                            {
                                this.UpdateStockDocumentCost(docType, docKey, dtlKey, -computedCost.TotalCost);
                            }
                            catch
                            {
                            }
                        }
                    }
                    Decimal num3 = Decimal.Round(BCE.Data.Convert.ToDecimal(dataRow["Cost"]), 8);
                    Decimal num4 = Decimal.Round(BCE.Data.Convert.ToDecimal(dataRow["TotalCost"]), 8);
                    Decimal num5 = Decimal.Round(BCE.Data.Convert.ToDecimal(dataRow["AdjustedCost"]), 8);
                    DBSetting dbSetting2 = dbSetting1;
                    string cmdText = "UPDATE StockDTL SET [Cost]=?, [TotalCost]=?, [AdjustedCost]=?, [CostType]=? WHERE StockDTLKey=?";
                    object[] objArray = new object[5];
                    int index2 = 0;
                    // ISSUE: variable of a boxed type
                    Decimal local1 = num3;
                    objArray[index2] = (object)local1;
                    int index3 = 1;
                    // ISSUE: variable of a boxed type
                    Decimal local2 = num4;
                    objArray[index3] = (object)local2;
                    int index4 = 2;
                    // ISSUE: variable of a boxed type
                    Decimal local3 = num5;
                    objArray[index4] = (object)local3;
                    int index5 = 3;
                    // ISSUE: variable of a boxed type
                    short local4 = BCE.Data.Convert.ToInt16(dataRow["CostType"]);
                    objArray[index5] = (object)local4;
                    int index6 = 4;
                    // ISSUE: variable of a boxed type
                    long local5 = BCE.Data.Convert.ToInt64(dataRow["StockDTLKey"]);
                    objArray[index6] = (object)local5;
                    dbSetting2.ExecuteNonQuery(cmdText, objArray);
                }
                if (postAuditLog)
                {
                    stringBuilder1.AppendLine(Utils.BooleanToReadableText(Localizer.GetString((Enum)RecalculateStockCostingStringId.AutomaticReorderTransactionSequence, new object[0]), autoReorder));
                    if (endDate != DateTime.MinValue)
                    {
                        StringBuilder stringBuilder2 = stringBuilder1;
                        // ISSUE: variable of a boxed type
                        RecalculateStockCostingStringId local = RecalculateStockCostingStringId.EndDate;
                        object[] objArray = new object[1];
                        int index = 0;
                        string str = endDate.ToLongDateString();
                        objArray[index] = (object)str;
                        string @string = Localizer.GetString((Enum)local, objArray);
                        stringBuilder2.AppendLine(@string);
                    }
                    AuditTrail.Log(dbSetting1, "", 0L, AuditTrail.EventType.Others, string.Format("{0}: {1}", (object)Localizer.GetString((Enum)RecalculateStockCostingStringId.RecalculateStockCosting, new object[0]), (object)itemCode), ((object)stringBuilder1).ToString());
                }
                dbSetting1.Commit();
                flag = true;
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                RecalculateStockCosting.EndRecalculateStockCosting();
                dbSetting1.EndTransaction();
            }
            return flag;
        }

        private DataRow[] Reorder(DataTable table)
        {
            if (table.Rows.Count == 0)
            {
                return new DataRow[0];
            }
            else
            {
                string strB1 = "xczxczzxc";
                string strB2 = "";
                string strB3 = "";
                ArrayList arrayList1 = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                foreach (DataRow dataRow in (InternalDataCollectionBase)table.Rows)
                {
                    if (string.Compare(dataRow["UOM"].ToString(), strB1, true) != 0 || !this.myIgnoreLocationInCostingCalculation && string.Compare(dataRow["Location"].ToString(), strB2, true) != 0 || !this.myIgnoreBatchNumberInCostingCalculation && string.Compare(dataRow["BatchNo"].ToString(), strB3, true) != 0)
                    {
                        if (arrayList2.Count > 0)
                            arrayList1.Add((object)arrayList2);
                        arrayList2 = new ArrayList();
                        strB1 = dataRow["UOM"].ToString();
                        strB2 = !this.myIgnoreLocationInCostingCalculation ? dataRow["Location"].ToString() : this.myDefaultLocationInCostingCalculation;
                        strB3 = !this.myIgnoreBatchNumberInCostingCalculation ? dataRow["BatchNo"].ToString() : "";
                    }
                    arrayList2.Add((object)dataRow);
                }
                if (arrayList2.Count > 0)
                    arrayList1.Add((object)arrayList2);
                ArrayList arrayList3 = new ArrayList();
                for (int index = 0; index < arrayList1.Count; ++index)
                {
                    DataRow[] rowArray = (DataRow[])((ArrayList)arrayList1[index]).ToArray(typeof(DataRow));
                    this.ReorderRows(ref rowArray);
                    arrayList3.Add((object)rowArray);
                }
                int[] numArray = new int[arrayList3.Count];
                for (int index = 0; index < arrayList3.Count; ++index)
                    numArray[index] = 0;
                DataRow[] dataRowArray1 = new DataRow[table.Rows.Count];
                int num1 = 0;
                bool flag1 = false;
                bool flag2 = false;
                while (!flag2)
                {
                    flag2 = true;
                    int num2 = num1;
                    for (int index1 = 0; index1 < arrayList3.Count; ++index1)
                    {
                        DataRow[] dataRowArray2 = (DataRow[])arrayList3[index1];
                        if (numArray[index1] <= dataRowArray2.Length)
                        {
                            int index2;
                            for (index2 = numArray[index1]; index2 < dataRowArray2.Length; ++index2)
                            {
                                DataRow dataRow = dataRowArray2[index2];
                                long num3 = BCE.Data.Convert.ToInt64(dataRow["ReferTo"]);
                                if (num3 == 0L || num3 == BCE.Data.Convert.ToInt64(dataRow["StockDTLKey"]))
                                    dataRowArray1[num1++] = dataRow;
                                else if (flag1)
                                {
                                    dataRowArray1[num1++] = dataRow;
                                    flag1 = false;
                                }
                                else
                                {
                                    bool flag3 = false;
                                    for (int index3 = 0; index3 < num1; ++index3)
                                    {
                                        if (num3 == BCE.Data.Convert.ToInt64(dataRowArray1[index3]["StockDTLKey"]))
                                        {
                                            dataRowArray1[num1++] = dataRow;
                                            flag3 = true;
                                            break;
                                        }
                                    }
                                    if (!flag3)
                                        break;
                                }
                            }
                            numArray[index1] = index2;
                            if (index2 < dataRowArray2.Length)
                                flag2 = false;
                        }
                    }
                    flag1 = num1 == num2;
                }
                return dataRowArray1;
            }
        }

        private void ReorderRows(ref DataRow[] rowArray)
        {
            if (rowArray.Length >= 2)
            {
                Decimal num1 = new Decimal();
                int index1;
                for (index1 = 0; index1 < rowArray.Length; ++index1)
                {
                    num1 += BCE.Data.Convert.ToDecimal(rowArray[index1]["Qty"]);
                    if (rowArray[index1]["DocType"].ToString() != "OB")
                        break;
                }
                for (int index2 = index1 + 1; index2 < rowArray.Length; ++index2)
                {
                    DataRow dataRow = rowArray[index2];
                    Decimal num2 = BCE.Data.Convert.ToDecimal(dataRow["Qty"]);
                    if (num1 < Decimal.Zero && num2 > Decimal.Zero)
                    {
                        Decimal num3 = num1;
                        int index3 = index2 - 1;
                        while (index3 >= 0 && rowArray[index3]["DocType"].ToString() != "OB" && (num3 < Decimal.Zero && BCE.Data.Convert.ToDecimal(rowArray[index3]["Qty"]) < Decimal.Zero))
                            num3 -= BCE.Data.Convert.ToDecimal(rowArray[index3--]["Qty"]);
                        for (int index4 = index2; index4 > index3 + 1; --index4)
                            rowArray[index4] = rowArray[index4 - 1];
                        rowArray[index3 + 1] = dataRow;
                    }
                    num1 += num2;
                }
            }
        }

        private void PostAuditLog(RecalculateStockCostingCriteria criteria, bool changeToNewCostingMethod, CostingMethod newCostingMethod, string successItems, string failItems, DBSetting dbSetting)
        {
            bool option1 = this.NeedToUpdateDocumentCost && this.UpdateDocumentCost != null;
            bool option2 = this.NeedToUpdateStockDocumentCost && this.UpdateStockDocumentCost != null;
            string string1 = Localizer.GetString((Enum)RecalculateStockCostingStringId.FilterOptions, new object[0]);
            string string2 = Localizer.GetString((Enum)RecalculateStockCostingStringId.CostingMethod, new object[0]);
            string string3 = Localizer.GetString((Enum)RecalculateStockCostingStringId.ChangeToNewCostingMethod, new object[0]);
            string string4 = Localizer.GetString((Enum)RecalculateStockCostingStringId.UpdateDocumentCostOptions, new object[0]);
            string string5 = Localizer.GetString((Enum)RecalculateStockCostingStringId.RecalculateOptions, new object[0]);
            StringBuilder stringBuilder1 = new StringBuilder();
            string str1 = "".PadLeft(string1.Length, ' ');
            stringBuilder1.AppendLine(string1 + criteria.ItemFilter.BuildReadableText());
            stringBuilder1.AppendLine(str1 + criteria.ItemGroupFilter.BuildReadableText());
            stringBuilder1.AppendLine(str1 + criteria.ItemTypeFilter.BuildReadableText());
            stringBuilder1.AppendLine(str1 + string2 + Utils.BooleanToReadableText(Localizer.GetString((Enum)RecalculateStockCostingStringId.FixedCost, new object[0]), criteria.FixedCost));
            string str2 = str1 + "".PadLeft(string2.Length, ' ');
            stringBuilder1.AppendLine(str2 + Utils.BooleanToReadableText(Localizer.GetString((Enum)RecalculateStockCostingStringId.WeightedAverage, new object[0]), criteria.WeightedAverage));
            stringBuilder1.AppendLine(str2 + Utils.BooleanToReadableText(Localizer.GetString((Enum)RecalculateStockCostingStringId.FIFO, new object[0]), criteria.FIFO));
            stringBuilder1.AppendLine(str2 + Utils.BooleanToReadableText(Localizer.GetString((Enum)RecalculateStockCostingStringId.LIFO, new object[0]), criteria.LIFO));
            stringBuilder1.AppendLine(str2 + Utils.BooleanToReadableText(Localizer.GetString((Enum)RecalculateStockCostingStringId.MostRecently, new object[0]), criteria.MostRecently));
            if (changeToNewCostingMethod)
                stringBuilder1.AppendLine(string3 + Localizer.GetString((Enum)newCostingMethod, new object[0]));
            string str3 = "".PadLeft(string4.Length, ' ');
            stringBuilder1.AppendLine(string4 + Utils.BooleanToReadableText(Localizer.GetString((Enum)RecalculateStockCostingStringId.UpdateDocumentCost, new object[0]), option1));
            stringBuilder1.AppendLine(str3 + Utils.BooleanToReadableText(Localizer.GetString((Enum)RecalculateStockCostingStringId.UpdateStockDocumentCost, new object[0]), option2));
            string str4 = "".PadLeft(string5.Length, ' ');
            stringBuilder1.AppendLine(string5 + Utils.BooleanToReadableText(Localizer.GetString((Enum)RecalculateStockCostingStringId.AutomaticReorderTransactionSequence, new object[0]), criteria.AutoReorder));
            if (criteria.EndDate != DateTime.MinValue)
            {
                StringBuilder stringBuilder2 = stringBuilder1;
                string str5 = str4;
                // ISSUE: variable of a boxed type
                RecalculateStockCostingStringId local = RecalculateStockCostingStringId.EndDate;
                object[] objArray = new object[1];
                int index = 0;
                string str6 = criteria.EndDate.ToLongDateString();
                objArray[index] = (object)str6;
                string string6 = Localizer.GetString((Enum)local, objArray);
                string str7 = str5 + string6;
                stringBuilder2.AppendLine(str7);
            }
            if (successItems.Length > 0)
            {
                stringBuilder1.AppendLine();
                StringBuilder stringBuilder2 = stringBuilder1;
                // ISSUE: variable of a boxed type
                RecalculateStockCostingStringId local = RecalculateStockCostingStringId.SuccessItems;
                object[] objArray = new object[1];
                int index = 0;
                string str5 = successItems;
                objArray[index] = (object)str5;
                string string6 = Localizer.GetString((Enum)local, objArray);
                stringBuilder2.AppendLine(string6);
            }
            if (failItems.Length > 0)
            {
                stringBuilder1.AppendLine();
                StringBuilder stringBuilder2 = stringBuilder1;
                // ISSUE: variable of a boxed type
                RecalculateStockCostingStringId local = RecalculateStockCostingStringId.FailItems;
                object[] objArray = new object[1];
                int index = 0;
                string str5 = failItems;
                objArray[index] = (object)str5;
                string string6 = Localizer.GetString((Enum)local, objArray);
                stringBuilder2.AppendLine(string6);
            }
            AuditTrail.Log(dbSetting, "", 0L, AuditTrail.EventType.Others, Localizer.GetString((Enum)RecalculateStockCostingStringId.RecalculateStockCosting, new object[0]), ((object)stringBuilder1).ToString());
        }
    }

}
