﻿using BCE.AutoCount.SearchFilter;
using BCE.Localization;
using System;
namespace Production.Tools.StockCosting
{
    [Serializable]
    public class RecalculateStockCostingCriteria:SearchCriteria
    {
        private BCE.AutoCount.SearchFilter.Filter myItemFilter;
        private BCE.AutoCount.SearchFilter.Filter myItemGroupFilter;
        private BCE.AutoCount.SearchFilter.Filter myItemTypeFilter;
        protected BCE.AutoCount.SearchFilter.Filter myDateFilter;

        private bool myFixedCost;
        private bool myWeightedAverage;
        private bool myFIFO;
        private bool myLIFO;
        private bool myMostRecently;
        private bool myAutoReorder;
        private DateTime myEndDate;

        public BCE.AutoCount.SearchFilter.Filter DateFilter
        {
            get
            {
                return this.myDateFilter;
            }
        }
        public BCE.AutoCount.SearchFilter.Filter ItemFilter
        {
            get
            {
                return this.myItemFilter;
            }
        }

        public BCE.AutoCount.SearchFilter.Filter ItemGroupFilter
        {
            get
            {
                return this.myItemGroupFilter;
            }
        }

        public BCE.AutoCount.SearchFilter.Filter ItemTypeFilter
        {
            get
            {
                return this.myItemTypeFilter;
            }
        }

        public bool FixedCost
        {
            get
            {
                return this.myFixedCost;
            }
            set
            {
                this.myFixedCost = value;
            }
        }

        public bool WeightedAverage
        {
            get
            {
                return this.myWeightedAverage;
            }
            set
            {
                this.myWeightedAverage = value;
            }
        }

        public bool FIFO
        {
            get
            {
                return this.myFIFO;
            }
            set
            {
                this.myFIFO = value;
            }
        }

        public bool LIFO
        {
            get
            {
                return this.myLIFO;
            }
            set
            {
                this.myLIFO = value;
            }
        }

        public bool MostRecently
        {
            get
            {
                return this.myMostRecently;
            }
            set
            {
                this.myMostRecently = value;
            }
        }

        public bool AutoReorder
        {
            get
            {
                return this.myAutoReorder;
            }
            set
            {
                this.myAutoReorder = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return this.myEndDate;
            }
            set
            {
                this.myEndDate = value.Date;
            }
        }

        public RecalculateStockCostingCriteria()
        {
            this.myItemFilter = new BCE.AutoCount.SearchFilter.Filter("", "ItemCode", Localizer.GetString((Enum)RecalculateStockCostingStringId.ItemCode, new object[0]), FilterControlType.Item);
            this.myItemGroupFilter = new BCE.AutoCount.SearchFilter.Filter("", "ItemGroup", Localizer.GetString((Enum)RecalculateStockCostingStringId.ItemGroup, new object[0]), FilterControlType.ItemGroup);
            this.myItemTypeFilter = new BCE.AutoCount.SearchFilter.Filter("", "ItemType", Localizer.GetString((Enum)RecalculateStockCostingStringId.ItemType, new object[0]), FilterControlType.ItemType);
            this.myDateFilter = new BCE.AutoCount.SearchFilter.Filter("", "DocDate","DocDate", FilterControlType.DateTime);
            this.myFixedCost = true;
            this.myWeightedAverage = true;
            this.myFIFO = true;
            this.myLIFO = true;
            this.myMostRecently = true;
            this.myAutoReorder = true;
            this.myEndDate = DateTime.MinValue;
        }
    }

}
