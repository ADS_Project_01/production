﻿using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.Stock;
using BCE.Data;
using BCE.Localization;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using BCE.AutoCount.Tools;
namespace Production.Tools.StockCosting
{
    [BCE.AutoCount.PlugIn.MenuItem("Recalculate Stock Costing", BeginNewGroup = false, MenuOrder = 1, OpenAccessRight = "RPA_SETTINGS_OPEN", ParentBeginNewGroup = false, ParentMenuCaption = "Tools", ParentMenuOrder = 6, ShowAsDialog = false, VisibleAccessRight = "RPA_SETTINGS_SHOW")]
    [SingleInstanceThreadForm]

    public class FormRecalculateStockCosting : XtraForm
    {
        private DBSetting myDBSetting;
        private RecalculateStockCostingCriteria myCriteria;
        private RecalculateStockCosting myRecalculateStockCosting;
        private RecalculateSalesDocumentCosting myRecalculateSalesDocumentCosting;
        private UpdateStockDocumentsFromRecalculateStockCosting myUpdateStockDocuments;
        private bool myRecalculateInProgress;
        private IContainer components;
        private UCItemGroupSelector ucItemGroupSelector1;
        private UCItemSelector ucItemSelector1;
        private UCItemTypeSelector ucItemTypeSelector1;
        private CheckEdit ckEdtAutoReorder;
        private CheckEdit ckEdtChangeToNewCostingMethod;
        private CheckEdit ckEdtFIFO;
        private CheckEdit ckEdtFixedCost;
        private CheckEdit ckEdtLIFO;
        private CheckEdit ckEdtMostRecently;
        private CheckEdit ckEdtSmartUpdateDocumentCost;
        private CheckEdit ckEdtUpdateDocumentCost;
        private CheckEdit ckEdtUpdateStockDocumentCost;
        private CheckEdit ckEdtWeightedAverage;
        private ComboBoxEdit cbEdtNewCostingMethod;
        private PanelControl panelControl1;
        private ProgressBarControl progressBarItem;
        private ProgressBarControl progressBarTransaction;
        private SimpleButton sbtnCalculate;
        private SimpleButton sbtnClose;
        private Label label13;
        private Label label14;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label labelItem;
        private Label lblCurrentItem;
        private Label lblTotalItem;
        private Label label15;
        private UCDateSelector ucDateSelector1;
        private Label lblTotalTransaction;

        public FormRecalculateStockCosting(DBSetting dbSetting)
        {
            this.CenterToScreen();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Location = new Point((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                          (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
            this.InitializeComponent();
            this.myDBSetting = dbSetting;
            this.cbEdtNewCostingMethod.SelectedIndex = 1;
            this.myCriteria = new RecalculateStockCostingCriteria();
            this.myRecalculateStockCosting = RecalculateStockCosting.Create(this.myDBSetting);
            this.myRecalculateStockCosting.ItemProgressEvent += new ItemProgressDelegate(this.ShowItemProgress);
            this.myRecalculateStockCosting.TransactionProgressEvent += new TransactionProgressDelegate(this.ShowTransactionProgress);
            this.myRecalculateSalesDocumentCosting = new RecalculateSalesDocumentCosting(this.myDBSetting);
            this.myUpdateStockDocuments = new UpdateStockDocumentsFromRecalculateStockCosting(this.myDBSetting);
            // BCE.AutoCount.Help.HelpProvider.SetHelpTopic(new System.Windows.Forms.HelpProvider(), (Control)this, "Recalculate_Stock_Costing.htm");
            this.ucDateSelector1.Initialize(this.myDBSetting, this.myCriteria.DateFilter);
        }

        private void UpdateSalesLocalTotalCost(string docType, long dtlKey, Decimal localTotalCost)
        {
            this.myRecalculateSalesDocumentCosting.CalculateDetailLocalTotalCost(docType, dtlKey, localTotalCost);
        }

        private void UpdateStockTotalCost(string docType, long docKey, long dtlKey, Decimal localTotalCost)
        {
            this.myUpdateStockDocuments.UpdateInternalCostRecords(docType, docKey, dtlKey, localTotalCost);
        }

        private void ShowItemProgress(int maxItem, string itemCode, int currentItem)
        {
            this.lblTotalItem.Text = maxItem.ToString();
            this.progressBarItem.Properties.Maximum = maxItem;
            this.progressBarItem.Position = currentItem;
            Label label = this.lblCurrentItem;
            // ISSUE: variable of a boxed type
            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.Code_CurrentItem;
            object[] objArray = new object[3];
            int index1 = 0;
            string str1 = currentItem.ToString();
            objArray[index1] = (object)str1;
            int index2 = 1;
            string str2 = maxItem.ToString();
            objArray[index2] = (object)str2;
            int index3 = 2;
            string str3 = itemCode;
            objArray[index3] = (object)str3;
            string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
            label.Text = @string;
            System.Windows.Forms.Application.DoEvents();
        }

        private void ShowTransactionProgress(int maxTransaction, int currentTransaction)
        {
            this.lblTotalTransaction.Text = maxTransaction.ToString();
            this.progressBarTransaction.Properties.Maximum = maxTransaction;
            this.progressBarTransaction.Position = currentTransaction;
            this.Update();
            System.Windows.Forms.Application.DoEvents();
        }

        private void sbtnCalculate_Click(object sender, EventArgs e)
        {
            if (!this.myRecalculateInProgress)
            {
                if (!this.ckEdtChangeToNewCostingMethod.Checked || AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum)RecalculateStockCostingStringId.ConfirmMessage_ChangeSelectedStockItems, new object[0])))
                {
                    DateTime now = DateTime.Now;
                    this.sbtnCalculate.Text = BCE.Localization.Localizer.GetString((Enum)RecalculateStockCostingStringId.Code_Cancel, new object[0]);
                    this.sbtnClose.Enabled = false;
                    this.ucItemSelector1.Enabled = false;
                    this.ucItemGroupSelector1.Enabled = false;
                    this.ucItemTypeSelector1.Enabled = false;
                    this.ckEdtFixedCost.Enabled = false;
                    this.ckEdtWeightedAverage.Enabled = false;
                    this.ckEdtFIFO.Enabled = false;
                    this.ckEdtLIFO.Enabled = false;
                    this.ckEdtMostRecently.Enabled = false;
                    this.ckEdtUpdateDocumentCost.Enabled = false;
                    this.ckEdtSmartUpdateDocumentCost.Enabled = false;
                    this.ckEdtUpdateStockDocumentCost.Enabled = false;
                    this.ckEdtAutoReorder.Enabled = false;
                    this.ckEdtChangeToNewCostingMethod.Enabled = false;
                    this.cbEdtNewCostingMethod.Enabled = false;
                    this.myRecalculateInProgress = true;
                    RecalculateStockCosting.BeginRecalculateStockCosting();
                    try
                    {
                        this.myCriteria.FixedCost = this.ckEdtFixedCost.Checked;
                        this.myCriteria.WeightedAverage = this.ckEdtWeightedAverage.Checked;
                        this.myCriteria.FIFO = this.ckEdtFIFO.Checked;
                        this.myCriteria.LIFO = this.ckEdtLIFO.Checked;
                        this.myCriteria.MostRecently = this.ckEdtMostRecently.Checked;
                        this.myCriteria.AutoReorder = this.ckEdtAutoReorder.Checked;
                        if (this.ckEdtUpdateDocumentCost.Checked)
                            this.myRecalculateStockCosting.UpdateDocumentCost += new UpdateDocumentCostDelegate(this.UpdateSalesLocalTotalCost);
                        else
                            this.myRecalculateStockCosting.UpdateDocumentCost -= new UpdateDocumentCostDelegate(this.UpdateSalesLocalTotalCost);
                        if (this.ckEdtUpdateStockDocumentCost.Checked)
                            this.myRecalculateStockCosting.UpdateStockDocumentCost += new UpdateStockDocumentCostDelegate(this.UpdateStockTotalCost);
                        else
                            this.myRecalculateStockCosting.UpdateStockDocumentCost -= new UpdateStockDocumentCostDelegate(this.UpdateStockTotalCost);
                        this.myRecalculateSalesDocumentCosting.SmartUpdate = this.ckEdtSmartUpdateDocumentCost.Checked;
                        this.myRecalculateStockCosting.Recalculate(this.myCriteria, this.ckEdtChangeToNewCostingMethod.Checked, (CostingMethod)this.cbEdtNewCostingMethod.SelectedIndex);
                        if (this.ckEdtUpdateDocumentCost.Checked)
                        {
                            this.myRecalculateSalesDocumentCosting.UpdateDetailLocalTotalCost();
                            this.myRecalculateSalesDocumentCosting.UpdateMasterLocalTotalCost();
                            this.myRecalculateSalesDocumentCosting.ShowError();
                        }
                        if (this.ckEdtUpdateStockDocumentCost.Checked)
                        {
                            this.myUpdateStockDocuments.UpdateStockDocuments();
                            this.myUpdateStockDocuments.ShowError();
                        }
                        if (this.myRecalculateStockCosting.Cancel)
                        {
                            BCE.Localization.Localizer.GetString((Enum)RecalculateStockCostingStringId.Code_RecalculateWasCancelled, new object[0]);
                            int num = (int)XtraMessageBox.Show((IWin32Window)this, BCE.Localization.Localizer.GetString((Enum)RecalculateStockCostingStringId.Code_RecalculateWasCancelled, new object[0]), BCE.Localization.Localizer.GetString((Enum)RecalculateStockCostingStringId.Code_RecalculateStockCosting, new object[0]), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        else
                        {
                            TimeSpan timeSpan = DateTime.Now.Subtract(now);
                            FormRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local1 = RecalculateStockCostingStringId.Code_RecalculateCompleted;
                            object[] objArray = new object[4];
                            int index1 = 0;
                            // ISSUE: variable of a boxed type
                            int local2 = timeSpan.Hours;
                            objArray[index1] = (object)local2;
                            int index2 = 1;
                            // ISSUE: variable of a boxed type
                            int local3 = timeSpan.Minutes;
                            objArray[index2] = (object)local3;
                            int index3 = 2;
                            // ISSUE: variable of a boxed type
                            int local4 = timeSpan.Seconds;
                            objArray[index3] = (object)local4;
                            int index4 = 3;
                            // ISSUE: variable of a boxed type
                            int local5 = timeSpan.Milliseconds;
                            objArray[index4] = (object)local5;
                            string string1 = BCE.Localization.Localizer.GetString((Enum)local1, objArray);
                            string string2 = BCE.Localization.Localizer.GetString((Enum)RecalculateStockCostingStringId.Code_RecalculateStockCosting, new object[0]);
                            int num1 = 0;
                            int num2 = 64;
                            int num3 = (int)XtraMessageBox.Show((IWin32Window)recalculateStockCosting, string1, string2, (MessageBoxButtons)num1, (MessageBoxIcon)num2);
                        }
                    }
                    catch (AppException ex)
                    {
                        AppMessage.ShowErrorMessage(ex.Message);
                    }
                    finally
                    {
                        RecalculateStockCosting.EndRecalculateStockCosting();
                        this.myRecalculateInProgress = false;
                        this.ckEdtChangeToNewCostingMethod.Enabled = true;
                        this.cbEdtNewCostingMethod.Enabled = true;
                        this.ckEdtFixedCost.Enabled = true;
                        this.ckEdtWeightedAverage.Enabled = true;
                        this.ckEdtFIFO.Enabled = true;
                        this.ckEdtLIFO.Enabled = true;
                        this.ckEdtMostRecently.Enabled = true;
                        this.ckEdtUpdateDocumentCost.Enabled = true;
                        this.ckEdtSmartUpdateDocumentCost.Enabled = true;
                        this.ckEdtUpdateStockDocumentCost.Enabled = true;
                        this.ckEdtAutoReorder.Enabled = true;
                        this.ucItemSelector1.Enabled = true;
                        this.ucItemGroupSelector1.Enabled = true;
                        this.ucItemTypeSelector1.Enabled = true;
                        this.sbtnClose.Enabled = true;
                        this.sbtnCalculate.Text = BCE.Localization.Localizer.GetString((Enum)RecalculateStockCostingStringId.Code_Calculate, new object[0]);
                        DBSetting dbSetting = this.myDBSetting;
                        string docType = "";
                        long docKey = 0L;
                        long eventKey = 0L;
                        // ISSUE: variable of a boxed type
                        RecalculateStockCostingStringId local = RecalculateStockCostingStringId.RunningRecalculateStockCosting;
                        object[] objArray = new object[1];
                        int index = 0;
                        string loginUserId = UserAuthentication.GetOrCreate(this.myDBSetting).LoginUserID;
                        objArray[index] = (object)loginUserId;
                        string @string = BCE.Localization.Localizer.GetString((Enum)local, objArray);
                        string detail = "";
                        Activity.Log(dbSetting, docType, docKey, eventKey, @string, detail);
                    }
                }
            }
            else if (AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum)RecalculateStockCostingStringId.ConfirmMessage_CancelRecalculateStockCosting, new object[0])))
                this.myRecalculateStockCosting.Cancel = true;
        }

        private void FormRecalculateStockCosting_Load(object sender, EventArgs e)
        {
            this.Icon = BCE.AutoCount.Application.Icon;
            this.ucItemSelector1.Initialize(this.myDBSetting, this.myCriteria.ItemFilter);
            this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myCriteria.ItemGroupFilter);
            this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myCriteria.ItemTypeFilter);
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRecalculateStockCosting));
            this.ucItemSelector1 = new BCE.AutoCount.FilterUI.UCItemSelector();
            this.ucItemGroupSelector1 = new BCE.AutoCount.FilterUI.UCItemGroupSelector();
            this.ucItemTypeSelector1 = new BCE.AutoCount.FilterUI.UCItemTypeSelector();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelItem = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ckEdtFixedCost = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ckEdtWeightedAverage = new DevExpress.XtraEditors.CheckEdit();
            this.ckEdtFIFO = new DevExpress.XtraEditors.CheckEdit();
            this.ckEdtLIFO = new DevExpress.XtraEditors.CheckEdit();
            this.ckEdtMostRecently = new DevExpress.XtraEditors.CheckEdit();
            this.sbtnCalculate = new DevExpress.XtraEditors.SimpleButton();
            this.progressBarItem = new DevExpress.XtraEditors.ProgressBarControl();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTotalItem = new System.Windows.Forms.Label();
            this.lblCurrentItem = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTotalTransaction = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.progressBarTransaction = new DevExpress.XtraEditors.ProgressBarControl();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ucDateSelector1 = new BCE.AutoCount.FilterUI.UCDateSelector();
            this.label15 = new System.Windows.Forms.Label();
            this.ckEdtSmartUpdateDocumentCost = new DevExpress.XtraEditors.CheckEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.ckEdtUpdateStockDocumentCost = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ckEdtAutoReorder = new DevExpress.XtraEditors.CheckEdit();
            this.cbEdtNewCostingMethod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ckEdtChangeToNewCostingMethod = new DevExpress.XtraEditors.CheckEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ckEdtUpdateDocumentCost = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtFixedCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtWeightedAverage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtFIFO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtLIFO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtMostRecently.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarTransaction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtSmartUpdateDocumentCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtUpdateStockDocumentCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtAutoReorder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEdtNewCostingMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtChangeToNewCostingMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtUpdateDocumentCost.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ucItemSelector1
            // 
            resources.ApplyResources(this.ucItemSelector1, "ucItemSelector1");
            this.ucItemSelector1.Name = "ucItemSelector1";
            // 
            // ucItemGroupSelector1
            // 
            resources.ApplyResources(this.ucItemGroupSelector1, "ucItemGroupSelector1");
            this.ucItemGroupSelector1.Name = "ucItemGroupSelector1";
            // 
            // ucItemTypeSelector1
            // 
            resources.ApplyResources(this.ucItemTypeSelector1, "ucItemTypeSelector1");
            this.ucItemTypeSelector1.Name = "ucItemTypeSelector1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // labelItem
            // 
            resources.ApplyResources(this.labelItem, "labelItem");
            this.labelItem.Name = "labelItem";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // ckEdtFixedCost
            // 
            resources.ApplyResources(this.ckEdtFixedCost, "ckEdtFixedCost");
            this.ckEdtFixedCost.Name = "ckEdtFixedCost";
            this.ckEdtFixedCost.Properties.Caption = resources.GetString("ckEdtFixedCost.Properties.Caption");
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // ckEdtWeightedAverage
            // 
            resources.ApplyResources(this.ckEdtWeightedAverage, "ckEdtWeightedAverage");
            this.ckEdtWeightedAverage.Name = "ckEdtWeightedAverage";
            this.ckEdtWeightedAverage.Properties.Caption = resources.GetString("ckEdtWeightedAverage.Properties.Caption");
            // 
            // ckEdtFIFO
            // 
            resources.ApplyResources(this.ckEdtFIFO, "ckEdtFIFO");
            this.ckEdtFIFO.Name = "ckEdtFIFO";
            this.ckEdtFIFO.Properties.Caption = resources.GetString("ckEdtFIFO.Properties.Caption");
            // 
            // ckEdtLIFO
            // 
            resources.ApplyResources(this.ckEdtLIFO, "ckEdtLIFO");
            this.ckEdtLIFO.Name = "ckEdtLIFO";
            this.ckEdtLIFO.Properties.Caption = resources.GetString("ckEdtLIFO.Properties.Caption");
            // 
            // ckEdtMostRecently
            // 
            resources.ApplyResources(this.ckEdtMostRecently, "ckEdtMostRecently");
            this.ckEdtMostRecently.Name = "ckEdtMostRecently";
            this.ckEdtMostRecently.Properties.Caption = resources.GetString("ckEdtMostRecently.Properties.Caption");
            // 
            // sbtnCalculate
            // 
            resources.ApplyResources(this.sbtnCalculate, "sbtnCalculate");
            this.sbtnCalculate.Name = "sbtnCalculate";
            this.sbtnCalculate.Click += new System.EventHandler(this.sbtnCalculate_Click);
            // 
            // progressBarItem
            // 
            resources.ApplyResources(this.progressBarItem, "progressBarItem");
            this.progressBarItem.Name = "progressBarItem";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // lblTotalItem
            // 
            resources.ApplyResources(this.lblTotalItem, "lblTotalItem");
            this.lblTotalItem.Name = "lblTotalItem";
            // 
            // lblCurrentItem
            // 
            resources.ApplyResources(this.lblCurrentItem, "lblCurrentItem");
            this.lblCurrentItem.Name = "lblCurrentItem";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // lblTotalTransaction
            // 
            resources.ApplyResources(this.lblTotalTransaction, "lblTotalTransaction");
            this.lblTotalTransaction.Name = "lblTotalTransaction";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // progressBarTransaction
            // 
            resources.ApplyResources(this.progressBarTransaction, "progressBarTransaction");
            this.progressBarTransaction.Name = "progressBarTransaction";
            // 
            // sbtnClose
            // 
            resources.ApplyResources(this.sbtnClose, "sbtnClose");
            this.sbtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.ucDateSelector1);
            this.panelControl1.Controls.Add(this.label15);
            this.panelControl1.Controls.Add(this.ckEdtSmartUpdateDocumentCost);
            this.panelControl1.Controls.Add(this.label14);
            this.panelControl1.Controls.Add(this.ckEdtUpdateStockDocumentCost);
            this.panelControl1.Controls.Add(this.label13);
            this.panelControl1.Controls.Add(this.label12);
            this.panelControl1.Controls.Add(this.ckEdtAutoReorder);
            this.panelControl1.Controls.Add(this.cbEdtNewCostingMethod);
            this.panelControl1.Controls.Add(this.ckEdtChangeToNewCostingMethod);
            this.panelControl1.Controls.Add(this.label11);
            this.panelControl1.Controls.Add(this.label9);
            this.panelControl1.Controls.Add(this.label7);
            this.panelControl1.Controls.Add(this.ckEdtUpdateDocumentCost);
            this.panelControl1.Controls.Add(this.ucItemSelector1);
            this.panelControl1.Controls.Add(this.ucItemGroupSelector1);
            this.panelControl1.Controls.Add(this.ucItemTypeSelector1);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.labelItem);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.ckEdtFixedCost);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.ckEdtWeightedAverage);
            this.panelControl1.Controls.Add(this.ckEdtFIFO);
            this.panelControl1.Controls.Add(this.ckEdtLIFO);
            this.panelControl1.Controls.Add(this.ckEdtMostRecently);
            this.panelControl1.Controls.Add(this.lblCurrentItem);
            this.panelControl1.Controls.Add(this.label8);
            this.panelControl1.Controls.Add(this.lblTotalTransaction);
            this.panelControl1.Controls.Add(this.progressBarTransaction);
            this.panelControl1.Controls.Add(this.sbtnClose);
            this.panelControl1.Controls.Add(this.sbtnCalculate);
            this.panelControl1.Controls.Add(this.progressBarItem);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.lblTotalItem);
            this.panelControl1.Controls.Add(this.label10);
            this.panelControl1.Controls.Add(this.label6);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.Name = "panelControl1";
            // 
            // ucDateSelector1
            // 
            this.ucDateSelector1.Appearance.Options.UseBackColor = true;
            resources.ApplyResources(this.ucDateSelector1, "ucDateSelector1");
            this.ucDateSelector1.Name = "ucDateSelector1";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // ckEdtSmartUpdateDocumentCost
            // 
            resources.ApplyResources(this.ckEdtSmartUpdateDocumentCost, "ckEdtSmartUpdateDocumentCost");
            this.ckEdtSmartUpdateDocumentCost.Name = "ckEdtSmartUpdateDocumentCost";
            this.ckEdtSmartUpdateDocumentCost.Properties.Caption = resources.GetString("ckEdtSmartUpdateDocumentCost.Properties.Caption");
            // 
            // label14
            // 
            this.label14.ForeColor = System.Drawing.Color.DarkGreen;
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // ckEdtUpdateStockDocumentCost
            // 
            resources.ApplyResources(this.ckEdtUpdateStockDocumentCost, "ckEdtUpdateStockDocumentCost");
            this.ckEdtUpdateStockDocumentCost.Name = "ckEdtUpdateStockDocumentCost";
            this.ckEdtUpdateStockDocumentCost.Properties.Caption = resources.GetString("ckEdtUpdateStockDocumentCost.Properties.Caption");
            // 
            // label13
            // 
            this.label13.ForeColor = System.Drawing.Color.DarkGreen;
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // ckEdtAutoReorder
            // 
            resources.ApplyResources(this.ckEdtAutoReorder, "ckEdtAutoReorder");
            this.ckEdtAutoReorder.Name = "ckEdtAutoReorder";
            this.ckEdtAutoReorder.Properties.Caption = resources.GetString("ckEdtAutoReorder.Properties.Caption");
            // 
            // cbEdtNewCostingMethod
            // 
            resources.ApplyResources(this.cbEdtNewCostingMethod, "cbEdtNewCostingMethod");
            this.cbEdtNewCostingMethod.Name = "cbEdtNewCostingMethod";
            this.cbEdtNewCostingMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cbEdtNewCostingMethod.Properties.Buttons"))))});
            this.cbEdtNewCostingMethod.Properties.Items.AddRange(new object[] {
            resources.GetString("cbEdtNewCostingMethod.Properties.Items"),
            resources.GetString("cbEdtNewCostingMethod.Properties.Items1"),
            resources.GetString("cbEdtNewCostingMethod.Properties.Items2"),
            resources.GetString("cbEdtNewCostingMethod.Properties.Items3"),
            resources.GetString("cbEdtNewCostingMethod.Properties.Items4")});
            this.cbEdtNewCostingMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // ckEdtChangeToNewCostingMethod
            // 
            resources.ApplyResources(this.ckEdtChangeToNewCostingMethod, "ckEdtChangeToNewCostingMethod");
            this.ckEdtChangeToNewCostingMethod.Name = "ckEdtChangeToNewCostingMethod";
            this.ckEdtChangeToNewCostingMethod.Properties.Caption = resources.GetString("ckEdtChangeToNewCostingMethod.Properties.Caption");
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label9
            // 
            this.label9.ForeColor = System.Drawing.Color.DarkGreen;
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // ckEdtUpdateDocumentCost
            // 
            resources.ApplyResources(this.ckEdtUpdateDocumentCost, "ckEdtUpdateDocumentCost");
            this.ckEdtUpdateDocumentCost.Name = "ckEdtUpdateDocumentCost";
            this.ckEdtUpdateDocumentCost.Properties.Caption = resources.GetString("ckEdtUpdateDocumentCost.Properties.Caption");
            // 
            // FormRecalculateStockCosting
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.sbtnClose;
            this.Controls.Add(this.panelControl1);
            this.Name = "FormRecalculateStockCosting";
            this.Load += new System.EventHandler(this.FormRecalculateStockCosting_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtFixedCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtWeightedAverage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtFIFO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtLIFO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtMostRecently.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarTransaction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtSmartUpdateDocumentCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtUpdateStockDocumentCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtAutoReorder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEdtNewCostingMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtChangeToNewCostingMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEdtUpdateDocumentCost.Properties)).EndInit();
            this.ResumeLayout(false);

        }
    }

}
