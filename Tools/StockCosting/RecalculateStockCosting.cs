﻿
using BCE.AutoCount.RegistryID.CostingOption;
using BCE.AutoCount.Stock;
using BCE.Data;
using System;
using BCE.AutoCount.Tools;
namespace Production.Tools.StockCosting
{
    public class RecalculateStockCosting
    {
        private bool myNeedToUpdateDocumentCost = true;
        private bool myNeedToUpdateStockDocumentCost = true;
        protected DBSetting myDBSetting;
        protected ItemProgressDelegate myItemProgress;
        protected bool myCancel;
        protected TransactionProgressDelegate myTransactionProgress;
        private UpdateDocumentCostDelegate myUpdateDocumentCost;
        private UpdateStockDocumentCostDelegate myUpdateStockDocumentCost;
        protected bool myIgnoreLocationInCostingCalculation;
        protected bool myIgnoreBatchNumberInCostingCalculation;
        protected string myDefaultLocationInCostingCalculation;
        [ThreadStatic]
        private static int myRecalculateStockCostingRunCounter;

        public static bool IsRecalculateStockCostingRunning
        {
            get
            {
                return RecalculateStockCosting.myRecalculateStockCostingRunCounter > 0;
            }
        }

        public UpdateDocumentCostDelegate UpdateDocumentCost
        {
            get
            {
                return this.myUpdateDocumentCost;
            }
            set
            {
                this.myUpdateDocumentCost = value;
            }
        }

        public bool NeedToUpdateDocumentCost
        {
            get
            {
                return this.myNeedToUpdateDocumentCost;
            }
            set
            {
                this.myNeedToUpdateDocumentCost = value;
            }
        }

        public UpdateStockDocumentCostDelegate UpdateStockDocumentCost
        {
            get
            {
                return this.myUpdateStockDocumentCost;
            }
            set
            {
                this.myUpdateStockDocumentCost = value;
            }
        }

        public bool NeedToUpdateStockDocumentCost
        {
            get
            {
                return this.myNeedToUpdateStockDocumentCost;
            }
            set
            {
                this.myNeedToUpdateStockDocumentCost = value;
            }
        }

        public ItemProgressDelegate ItemProgressEvent
        {
            get
            {
                return this.myItemProgress;
            }
            set
            {
                this.myItemProgress = value;
            }
        }

        public TransactionProgressDelegate TransactionProgressEvent
        {
            get
            {
                return this.myTransactionProgress;
            }
            set
            {
                this.myTransactionProgress = value;
            }
        }

        public bool Cancel
        {
            get
            {
                return this.myCancel;
            }
            set
            {
                this.myCancel = value;
            }
        }

        static RecalculateStockCosting()
        {
        }

        internal RecalculateStockCosting()
        {
        }

        public static RecalculateStockCosting Create(DBSetting dbSetting)
        {
            RecalculateStockCosting recalculateStockCosting = (RecalculateStockCosting)null;
            if (dbSetting.ServerType == DBServerType.SQL2000)
                recalculateStockCosting = (RecalculateStockCosting)new RecalculateStockCostingSQL();
            else
                dbSetting.ThrowServerTypeNotSupportedException();
            recalculateStockCosting.myDBSetting = dbSetting;
            DBRegistry dbRegistry = DBRegistry.Create(dbSetting);
            recalculateStockCosting.myIgnoreLocationInCostingCalculation = dbRegistry.GetBoolean((IRegistryID)new IgnoreLocationInCostingCalculation());
            recalculateStockCosting.myIgnoreBatchNumberInCostingCalculation = dbRegistry.GetBoolean((IRegistryID)new IgnoreBatchNumberInCostingCalculation());
            recalculateStockCosting.myDefaultLocationInCostingCalculation = dbRegistry.GetString((IRegistryID)new DefaultLocationInCostingCalculation());
            return recalculateStockCosting;
        }

        internal static void BeginRecalculateStockCosting()
        {
            ++RecalculateStockCosting.myRecalculateStockCostingRunCounter;
        }

        internal static void EndRecalculateStockCosting()
        {
            if (RecalculateStockCosting.myRecalculateStockCostingRunCounter > 0)
                --RecalculateStockCosting.myRecalculateStockCostingRunCounter;
        }

        public void Recalculate(RecalculateStockCostingCriteria criteria,string itemCode)
        {
            this.Recalculate(criteria,itemCode, true, DateTime.MinValue, true);
        }

        public virtual bool Recalculate(RecalculateStockCostingCriteria criteria, string itemCode, bool autoReorder, DateTime endDate, bool postAuditLog)
        {
            return false;
        }

        public void Recalculate(RecalculateStockCostingCriteria criteria)
        {
            this.Recalculate(criteria, false, CostingMethod.FIFO);
        }

        public virtual void Recalculate(RecalculateStockCostingCriteria criteria, bool changeToNewCostingMethod, CostingMethod newCostingMethod)
        {
        }
    }

}
