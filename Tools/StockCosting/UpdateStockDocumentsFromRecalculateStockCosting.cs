﻿
using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Common.Exceptions;
using BCE.AutoCount.Manufacturing.StockAssembly;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock.StockAdjustment;
using BCE.AutoCount.Stock.StockIssue;
using BCE.AutoCount.Stock.StockTransfer;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using BCE.AutoCount.Tools;
namespace Production.Tools.StockCosting
{
    public class UpdateStockDocumentsFromRecalculateStockCosting
    {
        protected string myErrorLogFilename = "";
        protected DBSetting myDBSetting;
        protected DataTable myTable;
        protected int myErrorCount;
        protected GeneralSetting myGeneralSetting;

        public UpdateStockDocumentsFromRecalculateStockCosting(DBSetting dbSetting)
        {
            this.myDBSetting = dbSetting;
            this.myGeneralSetting = GeneralSetting.GetOrCreate(this.myDBSetting);
            this.Init();
        }

        private void Init()
        {
            this.myTable = new DataTable();
            this.myTable.Columns.Add("DocType", typeof(string));
            this.myTable.Columns.Add("DocKey", typeof(long));
            this.myTable.Columns.Add("DtlKey", typeof(long));
            this.myTable.Columns.Add("TotalCost", typeof(Decimal));
        }

        public void UpdateInternalCostRecords(string docType, long docKey, long dtlKey, Decimal totalCost)
        {
            DataRowCollection rows = this.myTable.Rows;
            object[] objArray = new object[4];
            int index1 = 0;
            string str = docType;
            objArray[index1] = (object)str;
            int index2 = 1;
            // ISSUE: variable of a boxed type
            long local1 = docKey;
            objArray[index2] = (object)local1;
            int index3 = 2;
            // ISSUE: variable of a boxed type
            long local2 = dtlKey;
            objArray[index3] = (object)local2;
            int index4 = 3;
            // ISSUE: variable of a boxed type
            Decimal local3 = totalCost;
            objArray[index4] = (object)local3;
            rows.Add(objArray);
        }

        public void UpdateStockDocuments()
        {
            this.UpdateStockIssue();
            this.UpdateStockAdjustment();
            this.UpdateStockTransfer();
            this.UpdateStockAssembly();
        }

        private void UpdateStockIssue()
        {
            DataRow[] dataRowArray = this.myTable.Select(string.Format("DocType='{0}'", (object)"SI"), "DocType, DocKey, DtlKey");
            if (dataRowArray.Length != 0)
            {
                StockIssueCommand stockIssueCommand = StockIssueCommand.Create(this.myDBSetting);
                StockIssue stockIssue = (StockIssue)null;
                long num1 = 0L;
                foreach (DataRow dataRow in dataRowArray)
                {
                    long docKey = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
                    if (docKey != num1 || stockIssue == null)
                    {
                        if (stockIssue != null)
                        {
                            if (stockIssue.IsModified())
                            {
                                try
                                {
                                    stockIssue.Save((string)stockIssue.LastModifiedUserID);
                                }
                                catch (TransactionDateIsLockException ex)
                                {
                                    UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                                    // ISSUE: variable of a boxed type
                                    RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockIssueCauseTransactionDate;
                                    object[] objArray = new object[2];
                                    int index1 = 0;
                                    string str1 = stockIssue.DocNo.ToString();
                                    objArray[index1] = (object)str1;
                                    int index2 = 1;
                                    string str2 = this.myGeneralSetting.FormatDate((DateTime)stockIssue.DocDate);
                                    objArray[index2] = (object)str2;
                                    string @string = Localizer.GetString((Enum)local, objArray);
                                    recalculateStockCosting.WriteLog(@string);
                                }
                                catch (Exception ex)
                                {
                                    UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                                    // ISSUE: variable of a boxed type
                                    RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockIssueCauseUnknownError;
                                    object[] objArray = new object[2];
                                    int index1 = 0;
                                    string str = stockIssue.DocNo.ToString();
                                    objArray[index1] = (object)str;
                                    int index2 = 1;
                                    string message = ex.Message;
                                    objArray[index2] = (object)message;
                                    string @string = Localizer.GetString((Enum)local, objArray);
                                    recalculateStockCosting.WriteLog(@string);
                                }
                            }
                        }
                        DataRow firstDataRow;
                        try
                        {
                            DBSetting dbSetting = this.myDBSetting;
                            string cmdText = "SELECT DocNo, DocDate FROM ISS WHERE DocKey=?";
                            object[] objArray = new object[1];
                            int index = 0;
                            // ISSUE: variable of a boxed type
                            long local = docKey;
                            objArray[index] = (object)local;
                            firstDataRow = dbSetting.GetFirstDataRow(cmdText, objArray);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local1 = RecalculateStockCostingStringId.ErrorMessage_UnableGetStockIssueKeyCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            // ISSUE: variable of a boxed type
                            long local2 = docKey;
                            objArray[index1] = (object)local2;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local1, objArray);
                            recalculateStockCosting.WriteLog(@string);
                            continue;
                        }
                        stockIssue = (StockIssue)null;
                        try
                        {
                            stockIssue = stockIssueCommand.Edit(docKey);
                        }
                        catch (TransactionDateIsLockException ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableEditStockIssueCauseTransactionDate;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str1 = firstDataRow["DocNo"].ToString();
                            objArray[index1] = (object)str1;
                            int index2 = 1;
                            string str2 = this.myGeneralSetting.FormatDate(BCE.Data.Convert.ToDateTime(firstDataRow["DocDate"]));
                            objArray[index2] = (object)str2;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableEditStockIssueCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = firstDataRow["DocNo"].ToString();
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        if (stockIssue != null)
                            num1 = docKey;
                        else
                            continue;
                    }
                    long dtlKey = BCE.Data.Convert.ToInt64(dataRow["DtlKey"]);
                    Decimal num2 = BCE.Data.Convert.ToDecimal(dataRow["TotalCost"]);
                    StockIssueDetail detailByDtlKey = stockIssue.GetDetailByDtlKey(dtlKey);
                    if (detailByDtlKey != null)
                    {
                        Decimal num3 = Decimal.One;
                        if (detailByDtlKey.Qty.HasValue && (Decimal)detailByDtlKey.Qty != Decimal.Zero)
                            num3 = (Decimal)detailByDtlKey.Qty;
                        Decimal num4 = num2 / num3;
                        if ((Decimal)detailByDtlKey.UnitCost != num4)
                            detailByDtlKey.UnitCost = (DBDecimal)num4;
                    }
                }
                if (stockIssue != null)
                {
                    if (stockIssue.IsModified())
                    {
                        try
                        {
                            stockIssue.Save((string)stockIssue.LastModifiedUserID);
                        }
                        catch (TransactionDateIsLockException ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockIssueCauseTransactionDate;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str1 = stockIssue.DocNo.ToString();
                            objArray[index1] = (object)str1;
                            int index2 = 1;
                            string str2 = this.myGeneralSetting.FormatDate((DateTime)stockIssue.DocDate);
                            objArray[index2] = (object)str2;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockIssueCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = stockIssue.DocNo.ToString();
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                    }
                }
            }
        }

        private void UpdateStockAdjustment()
        {
            DataRow[] dataRowArray = this.myTable.Select(string.Format("DocType='{0}'", (object)"SA"), "DocType, DocKey, DtlKey");
            if (dataRowArray.Length != 0)
            {
                StockAdjustmentCommand adjustmentCommand = StockAdjustmentCommand.Create(this.myDBSetting);
                StockAdjustment stockAdjustment = (StockAdjustment)null;
                long num1 = 0L;
                foreach (DataRow dataRow in dataRowArray)
                {
                    long docKey = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
                    if (docKey != num1 || stockAdjustment == null)
                    {
                        if (stockAdjustment != null)
                        {
                            if (stockAdjustment.IsModified())
                            {
                                try
                                {
                                    stockAdjustment.Save((string)stockAdjustment.LastModifiedUserID);
                                }
                                catch (TransactionDateIsLockException ex)
                                {
                                    UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                                    // ISSUE: variable of a boxed type
                                    RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockAdjustmentCauseTransactionDate;
                                    object[] objArray = new object[2];
                                    int index1 = 0;
                                    string str1 = stockAdjustment.DocNo.ToString();
                                    objArray[index1] = (object)str1;
                                    int index2 = 1;
                                    string str2 = this.myGeneralSetting.FormatDate((DateTime)stockAdjustment.DocDate);
                                    objArray[index2] = (object)str2;
                                    string @string = Localizer.GetString((Enum)local, objArray);
                                    recalculateStockCosting.WriteLog(@string);
                                }
                                catch (Exception ex)
                                {
                                    UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                                    // ISSUE: variable of a boxed type
                                    RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockAdjustmentCauseUnknownError;
                                    object[] objArray = new object[2];
                                    int index1 = 0;
                                    string str = stockAdjustment.DocNo.ToString();
                                    objArray[index1] = (object)str;
                                    int index2 = 1;
                                    string message = ex.Message;
                                    objArray[index2] = (object)message;
                                    string @string = Localizer.GetString((Enum)local, objArray);
                                    recalculateStockCosting.WriteLog(@string);
                                }
                            }
                        }
                        DataRow firstDataRow;
                        try
                        {
                            DBSetting dbSetting = this.myDBSetting;
                            string cmdText = "SELECT DocNo, DocDate FROM ADJ WHERE DocKey=?";
                            object[] objArray = new object[1];
                            int index = 0;
                            // ISSUE: variable of a boxed type
                            long local = docKey;
                            objArray[index] = (object)local;
                            firstDataRow = dbSetting.GetFirstDataRow(cmdText, objArray);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local1 = RecalculateStockCostingStringId.ErrorMessage_UnableGetStockAdjustmentKeyCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            // ISSUE: variable of a boxed type
                            long local2 = docKey;
                            objArray[index1] = (object)local2;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local1, objArray);
                            recalculateStockCosting.WriteLog(@string);
                            continue;
                        }
                        stockAdjustment = (StockAdjustment)null;
                        try
                        {
                            stockAdjustment = adjustmentCommand.Edit(docKey);
                        }
                        catch (TransactionDateIsLockException ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableEditStockAdjustmentCauseTransactionDate;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str1 = firstDataRow["DocNo"].ToString();
                            objArray[index1] = (object)str1;
                            int index2 = 1;
                            string str2 = this.myGeneralSetting.FormatDate(BCE.Data.Convert.ToDateTime(firstDataRow["DocDate"]));
                            objArray[index2] = (object)str2;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableEditStockAdjustmentCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = firstDataRow["DocNo"].ToString();
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        if (stockAdjustment != null)
                            num1 = docKey;
                        else
                            continue;
                    }
                    long dtlKey = BCE.Data.Convert.ToInt64(dataRow["DtlKey"]);
                    Decimal num2 = BCE.Data.Convert.ToDecimal(dataRow["TotalCost"]);
                    StockAdjustmentDetail detailByDtlKey = stockAdjustment.GetDetailByDtlKey(dtlKey);
                    if (detailByDtlKey != null)
                    {
                        Decimal num3 = Decimal.One;
                        if (detailByDtlKey.Qty.HasValue && (Decimal)detailByDtlKey.Qty != Decimal.Zero)
                            num3 = (Decimal)detailByDtlKey.Qty;
                        if (num3 < Decimal.Zero)
                        {
                            Decimal num4 = num2 / -num3;
                            if ((Decimal)detailByDtlKey.UnitCost != num4)
                                detailByDtlKey.UnitCost = (DBDecimal)num4;
                        }
                    }
                }
                if (stockAdjustment != null)
                {
                    if (stockAdjustment.IsModified())
                    {
                        try
                        {
                            stockAdjustment.Save((string)stockAdjustment.LastModifiedUserID);
                        }
                        catch (TransactionDateIsLockException ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockAdjustmentCauseTransactionDate;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str1 = stockAdjustment.DocNo.ToString();
                            objArray[index1] = (object)str1;
                            int index2 = 1;
                            string str2 = this.myGeneralSetting.FormatDate((DateTime)stockAdjustment.DocDate);
                            objArray[index2] = (object)str2;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockAdjustmentCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = stockAdjustment.DocNo.ToString();
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                    }
                }
            }
        }

        private void UpdateStockTransfer()
        {
            DataRow[] dataRowArray = this.myTable.Select(string.Format("DocType='{0}'", (object)"ST"), "DocType, DocKey, DtlKey");
            if (dataRowArray.Length != 0)
            {
                StockTransferCommand stockTransferCommand = StockTransferCommand.Create(this.myDBSetting);
                StockTransfer stockTransfer = (StockTransfer)null;
                long num1 = 0L;
                foreach (DataRow dataRow in dataRowArray)
                {
                    long docKey = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
                    if (docKey != num1 || stockTransfer == null)
                    {
                        if (stockTransfer != null)
                        {
                            if (stockTransfer.IsModified())
                            {
                                try
                                {
                                    stockTransfer.Save((string)stockTransfer.LastModifiedUserID);
                                }
                                catch (TransactionDateIsLockException ex)
                                {
                                    UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                                    // ISSUE: variable of a boxed type
                                    RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockTransferCauseTransactionDate;
                                    object[] objArray = new object[2];
                                    int index1 = 0;
                                    string str1 = stockTransfer.DocNo.ToString();
                                    objArray[index1] = (object)str1;
                                    int index2 = 1;
                                    string str2 = this.myGeneralSetting.FormatDate((DateTime)stockTransfer.DocDate);
                                    objArray[index2] = (object)str2;
                                    string @string = Localizer.GetString((Enum)local, objArray);
                                    recalculateStockCosting.WriteLog(@string);
                                }
                                catch (Exception ex)
                                {
                                    UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                                    // ISSUE: variable of a boxed type
                                    RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockTransferCauseUnknownError;
                                    object[] objArray = new object[2];
                                    int index1 = 0;
                                    string str = stockTransfer.DocNo.ToString();
                                    objArray[index1] = (object)str;
                                    int index2 = 1;
                                    string message = ex.Message;
                                    objArray[index2] = (object)message;
                                    string @string = Localizer.GetString((Enum)local, objArray);
                                    recalculateStockCosting.WriteLog(@string);
                                }
                            }
                        }
                        DataRow firstDataRow;
                        try
                        {
                            DBSetting dbSetting = this.myDBSetting;
                            string cmdText = "SELECT DocNo, DocDate FROM XFER WHERE DocKey=?";
                            object[] objArray = new object[1];
                            int index = 0;
                            // ISSUE: variable of a boxed type
                            long local = docKey;
                            objArray[index] = (object)local;
                            firstDataRow = dbSetting.GetFirstDataRow(cmdText, objArray);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local1 = RecalculateStockCostingStringId.ErrorMessage_UnableGetStockTransferKeyCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            // ISSUE: variable of a boxed type
                            long local2 = docKey;
                            objArray[index1] = (object)local2;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local1, objArray);
                            recalculateStockCosting.WriteLog(@string);
                            continue;
                        }
                        stockTransfer = (StockTransfer)null;
                        try
                        {
                            stockTransfer = stockTransferCommand.Edit(docKey);
                        }
                        catch (TransactionDateIsLockException ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableEditStockTransferCauseTransactionDate;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str1 = firstDataRow["DocNo"].ToString();
                            objArray[index1] = (object)str1;
                            int index2 = 1;
                            string str2 = this.myGeneralSetting.FormatDate(BCE.Data.Convert.ToDateTime(firstDataRow["DocDate"]));
                            objArray[index2] = (object)str2;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableEditStockTransferCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = firstDataRow["DocNo"].ToString();
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        if (stockTransfer != null)
                            num1 = docKey;
                        else
                            continue;
                    }
                    long dtlKey = BCE.Data.Convert.ToInt64(dataRow["DtlKey"]);
                    Decimal num2 = BCE.Data.Convert.ToDecimal(dataRow["TotalCost"]);
                    StockTransferDetail detailByDtlKey = stockTransfer.GetDetailByDtlKey(dtlKey);
                    if (detailByDtlKey != null)
                    {
                        Decimal num3 = Decimal.One;
                        if (detailByDtlKey.Qty.HasValue && (Decimal)detailByDtlKey.Qty != Decimal.Zero)
                            num3 = (Decimal)detailByDtlKey.Qty;
                        Decimal num4 = num2 / num3;
                        if ((Decimal)detailByDtlKey.UnitCost != num4)
                            detailByDtlKey.UnitCost = (DBDecimal)num4;
                    }
                }
                if (stockTransfer != null)
                {
                    if (stockTransfer.IsModified())
                    {
                        try
                        {
                            stockTransfer.Save((string)stockTransfer.LastModifiedUserID);
                        }
                        catch (TransactionDateIsLockException ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockTransferCauseTransactionDate;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str1 = stockTransfer.DocNo.ToString();
                            objArray[index1] = (object)str1;
                            int index2 = 1;
                            string str2 = this.myGeneralSetting.FormatDate((DateTime)stockTransfer.DocDate);
                            objArray[index2] = (object)str2;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockTransferCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = stockTransfer.DocNo.ToString();
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                    }
                }
            }
        }

        private void UpdateStockAssembly()
        {
            DataRow[] dataRowArray = this.myTable.Select(string.Format("DocType='{0}'", (object)"AS"), "DocType, DocKey, DtlKey");
            if (dataRowArray.Length != 0)
            {
                StockAssemblyCommand stockAssemblyCommand = StockAssemblyCommand.Create(this.myDBSetting);
                StockAssembly stockAssembly = (StockAssembly)null;
                long num1 = 0L;
                bool flag = false;
                foreach (DataRow dataRow in dataRowArray)
                {
                    long docKey = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
                    if (docKey != num1 || stockAssembly == null)
                    {
                        if (stockAssembly != null & flag)
                        {
                            try
                            {
                                stockAssembly.Save((string)stockAssembly.LastModifiedUserID);
                            }
                            catch (TransactionDateIsLockException ex)
                            {
                                UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                                // ISSUE: variable of a boxed type
                                RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockAssemblyCauseTransactionDate;
                                object[] objArray = new object[2];
                                int index1 = 0;
                                string str1 = stockAssembly.DocNo.ToString();
                                objArray[index1] = (object)str1;
                                int index2 = 1;
                                string str2 = this.myGeneralSetting.FormatDate((DateTime)stockAssembly.DocDate);
                                objArray[index2] = (object)str2;
                                string @string = Localizer.GetString((Enum)local, objArray);
                                recalculateStockCosting.WriteLog(@string);
                            }
                            catch (Exception ex)
                            {
                                UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                                // ISSUE: variable of a boxed type
                                RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockAssemblyCauseUnknownError;
                                object[] objArray = new object[2];
                                int index1 = 0;
                                string str = stockAssembly.DocNo.ToString();
                                objArray[index1] = (object)str;
                                int index2 = 1;
                                string message = ex.Message;
                                objArray[index2] = (object)message;
                                string @string = Localizer.GetString((Enum)local, objArray);
                                recalculateStockCosting.WriteLog(@string);
                            }
                        }
                        DataRow firstDataRow;
                        try
                        {
                            DBSetting dbSetting = this.myDBSetting;
                            string cmdText = "SELECT DocNo, DocDate FROM ASM WHERE DocKey=?";
                            object[] objArray = new object[1];
                            int index = 0;
                            // ISSUE: variable of a boxed type
                            long local = docKey;
                            objArray[index] = (object)local;
                            firstDataRow = dbSetting.GetFirstDataRow(cmdText, objArray);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local1 = RecalculateStockCostingStringId.ErrorMessage_UnableGetStockAssemblyKeyCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            // ISSUE: variable of a boxed type
                            long local2 = docKey;
                            objArray[index1] = (object)local2;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local1, objArray);
                            recalculateStockCosting.WriteLog(@string);
                            continue;
                        }
                        stockAssembly = (StockAssembly)null;
                        try
                        {
                            stockAssembly = stockAssemblyCommand.Edit(docKey);
                        }
                        catch (TransactionDateIsLockException ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableEditStockAssemblyCauseTransactionDate;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str1 = firstDataRow["DocNo"].ToString();
                            objArray[index1] = (object)str1;
                            int index2 = 1;
                            string str2 = this.myGeneralSetting.FormatDate(BCE.Data.Convert.ToDateTime(firstDataRow["DocDate"]));
                            objArray[index2] = (object)str2;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        catch (Exception ex)
                        {
                            UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                            // ISSUE: variable of a boxed type
                            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableEditStockAssemblyCauseUnknownError;
                            object[] objArray = new object[2];
                            int index1 = 0;
                            string str = firstDataRow["DocNo"].ToString();
                            objArray[index1] = (object)str;
                            int index2 = 1;
                            string message = ex.Message;
                            objArray[index2] = (object)message;
                            string @string = Localizer.GetString((Enum)local, objArray);
                            recalculateStockCosting.WriteLog(@string);
                        }
                        if (stockAssembly != null)
                        {
                            num1 = docKey;
                            flag = false;
                        }
                        else
                            continue;
                    }
                    long dtlKey = BCE.Data.Convert.ToInt64(dataRow["DtlKey"]);
                    Decimal num2 = BCE.Data.Convert.ToDecimal(dataRow["TotalCost"]);
                    StockAssemblyDetail detailByDtlKey = stockAssembly.GetDetailByDtlKey(dtlKey);
                    if (detailByDtlKey != null)
                    {
                        Decimal num3 = Decimal.One;
                        if (detailByDtlKey.Qty.HasValue && (Decimal)detailByDtlKey.Qty != Decimal.Zero)
                            num3 = (Decimal)detailByDtlKey.Qty;
                        Decimal num4 = num2 / num3;
                        if ((Decimal)detailByDtlKey.ItemCost != num4)
                        {
                            detailByDtlKey.ItemCost = (DBDecimal)num4;
                            flag = true;
                        }
                    }
                }
                if (stockAssembly != null & flag)
                {
                    try
                    {
                        stockAssembly.Save((string)stockAssembly.LastModifiedUserID);
                    }
                    catch (TransactionDateIsLockException ex)
                    {
                        UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                        // ISSUE: variable of a boxed type
                        RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockAssemblyCauseTransactionDate;
                        object[] objArray = new object[2];
                        int index1 = 0;
                        string str1 = stockAssembly.DocNo.ToString();
                        objArray[index1] = (object)str1;
                        int index2 = 1;
                        string str2 = this.myGeneralSetting.FormatDate((DateTime)stockAssembly.DocDate);
                        objArray[index2] = (object)str2;
                        string @string = Localizer.GetString((Enum)local, objArray);
                        recalculateStockCosting.WriteLog(@string);
                    }
                    catch (Exception ex)
                    {
                        UpdateStockDocumentsFromRecalculateStockCosting recalculateStockCosting = this;
                        // ISSUE: variable of a boxed type
                        RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UnableUpdateStockAssemblyCauseUnknownError;
                        object[] objArray = new object[2];
                        int index1 = 0;
                        string str = stockAssembly.DocNo.ToString();
                        objArray[index1] = (object)str;
                        int index2 = 1;
                        string message = ex.Message;
                        objArray[index2] = (object)message;
                        string @string = Localizer.GetString((Enum)local, objArray);
                        recalculateStockCosting.WriteLog(@string);
                    }
                }
            }
        }

        private string GetErrorLogFilename()
        {
            string str1 = Utils.ConvertCompanyNameRemarkToFilename((string)this.myGeneralSetting.CompanyProfile.CompanyName, (string)this.myGeneralSetting.CompanyProfile.Remark);
            string myAutoCountFolder = Application.MyAutoCountFolder;
            // ISSUE: variable of a boxed type
            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_UpdateStockDocumentCostError;
            object[] objArray = new object[2];
            int index1 = 0;
            string str2 = str1;
            objArray[index1] = (object)str2;
            int index2 = 1;
            string str3 = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
            objArray[index2] = (object)str3;
            string @string = Localizer.GetString((Enum)local, objArray);
            return Path.Combine(myAutoCountFolder, @string);
        }

        private void WriteLog(string message)
        {
            this.myErrorCount = this.myErrorCount + 1;
            if (this.myErrorLogFilename.Length == 0)
                this.myErrorLogFilename = this.GetErrorLogFilename();
            try
            {
                StreamWriter streamWriter = File.AppendText(this.myErrorLogFilename);
                foreach (string str in StringHelper.ToMultiLineString(message))
                    streamWriter.WriteLine(str);
                ((TextWriter)streamWriter).Flush();
                streamWriter.Close();
            }
            catch
            {
            }
        }

        public void ShowError()
        {
            if (this.myErrorCount > 0 && this.myErrorLogFilename.Length > 0)
            {
                AppMessage.ShowMessage(Localizer.GetString(RecalculateStockCostingStringId.ShowMessage_UpdateStockDocumentCostError, new object[0]));
                Process.Start("Notepad.exe", this.myErrorLogFilename);
            }
        }
    }

}
