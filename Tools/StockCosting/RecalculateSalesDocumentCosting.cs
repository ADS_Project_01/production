﻿using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Data;
using BCE.AutoCount.Invoicing;
using BCE.AutoCount.Invoicing.Sales;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.RegistryID.Misc;
using BCE.AutoCount.SerialNumber2;
using BCE.AutoCount.Settings;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;

namespace Production.Tools.StockCosting
{
    public class RecalculateSalesDocumentCosting
    {
        protected bool mySmartUpdate = true;
        private string myErrorLogFilename = "";
        protected DBSetting myDBSetting;
        protected DataTable myMasterTable;
        protected DataTable myDetailTable;
        protected DecimalSetting myDecimalSetting;
        private int myErrorCount;

        public bool SmartUpdate
        {
            get
            {
                return this.mySmartUpdate;
            }
            set
            {
                this.mySmartUpdate = value;
            }
        }

        public RecalculateSalesDocumentCosting(DBSetting dbSetting)
        {
            this.myDBSetting = dbSetting;
            this.myDecimalSetting = DecimalSetting.GetOrCreate(this.myDBSetting);
            this.Init();
        }

        private void Init()
        {
            this.myMasterTable = new DataTable();
            this.myMasterTable.Columns.Add("DocType", typeof(string));
            this.myMasterTable.Columns.Add("DocKey", typeof(long));
            this.myMasterTable.Columns.Add("IsAssigned", typeof(bool));
            this.myDetailTable = new DataTable();
            this.myDetailTable.Columns.Add("DocType", typeof(string));
            this.myDetailTable.Columns.Add("DtlKey", typeof(long));
            this.myDetailTable.Columns.Add("LocalTotalCost", typeof(Decimal));
            this.myDetailTable.Columns.Add("LocalFOCTotalCost", typeof(Decimal));
        }

        public void CalculateDetailLocalTotalCost(string docType, long dtlKey, Decimal localtotalCost)
        {
            try
            {
                bool isFOCDtlKey = false;
                DataRow documentDetailRow = this.GetDocumentDetailRow(docType, dtlKey, ref isFOCDtlKey);
                DBRegistry dbRegistry = DBRegistry.Create(this.myDBSetting);
                SerialNumberHelper serialNumberHelper = new SerialNumberHelper(this.myDBSetting, docType);
                if (documentDetailRow != null)
                {
                    dtlKey = BCE.Data.Convert.ToInt64(documentDetailRow["DtlKey"]);
                    localtotalCost = this.myDecimalSetting.RoundCost(localtotalCost);
                    if (docType != "CN")
                    {
                        DataRow row;
                        if (documentDetailRow.Table.Columns.IndexOf("FOCQty") >= 0)
                        {
                            Decimal num1 = BCE.Data.Convert.ToDecimal(documentDetailRow["FocQty"]);
                            Decimal num2 = BCE.Data.Convert.ToDecimal(documentDetailRow["SmallestQty"]);
                            Decimal num3 = new Decimal();
                            Decimal localTotalCost = localtotalCost;
                            if (!isFOCDtlKey)
                            {
                                Decimal num4 = num2 + num1;
                                if (num4 != Decimal.Zero)
                                {
                                    num3 = localtotalCost * num1 / num4;
                                    localTotalCost = localtotalCost - num3;
                                }
                                if ((docType == "DO" || docType == "IV" || (docType == "CS" || docType == "DN") || docType == "OS") && (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.SerialNo.Enable && dbRegistry.GetBoolean((IRegistryID)new UseSerialNumberCostForStockOut()) && InvoicingHelper.HasSerialNo(this.myDBSetting, documentDetailRow["ItemCode"].ToString())))
                                {
                                    DBSetting dbSetting = this.myDBSetting;
                                    string cmdText = "Select * from SerialNoTrans where DocType = ? AND DtlKey = ?";
                                    int num5 = 0;
                                    object[] objArray = new object[2];
                                    int index1 = 0;
                                    string str = docType;
                                    objArray[index1] = (object)str;
                                    int index2 = 1;
                                    // ISSUE: variable of a boxed type
                                    long local = dtlKey;
                                    objArray[index2] = (object)local;
                                    DataTable dataTable = dbSetting.GetDataTable(cmdText, num5 != 0, objArray);
                                    if (dataTable.Rows.Count != 0)
                                        serialNumberHelper.GetTotalCostBySN(dataTable, documentDetailRow["ItemCode"].ToString(), documentDetailRow["Location"].ToString(), BCE.Data.Convert.ToDecimal(documentDetailRow["SmallestQty"]), ref localTotalCost);
                                }
                                if (this.mySmartUpdate && localTotalCost == this.myDecimalSetting.RoundCost(documentDetailRow["LocalTotalCost"]) && (num3 == this.myDecimalSetting.RoundCost(documentDetailRow["LocalFOCTotalCost"]) && docType != "DO"))
                                {
                                    return;
                                }
                                else
                                {
                                    documentDetailRow["LocalTotalCost"] = (object)localTotalCost;
                                    documentDetailRow["LocalFOCTotalCost"] = (object)num3;
                                }
                            }
                            else if (this.mySmartUpdate && localtotalCost == this.myDecimalSetting.RoundCost(documentDetailRow["LocalFOCTotalCost"]) && docType != "DO")
                                return;
                            else
                                documentDetailRow["LocalFOCTotalCost"] = (object)localtotalCost;
                            row = this.myDetailTable.NewRow();
                            row["DocType"] = documentDetailRow["DocType"];
                            row["DtlKey"] = documentDetailRow["DtlKey"];
                            if (!isFOCDtlKey)
                            {
                                row["LocalTotalCost"] = (object)localTotalCost;
                                row["LocalFOCTotalCost"] = (object)num3;
                            }
                            else
                                row["LocalFOCTotalCost"] = (object)localtotalCost;
                        }
                        else
                        {
                            if ((docType == "DO" || docType == "IV" || (docType == "CS" || docType == "DN") || docType == "OS") && (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.SerialNo.Enable && dbRegistry.GetBoolean((IRegistryID)new UseSerialNumberCostForStockOut()) && InvoicingHelper.HasSerialNo(this.myDBSetting, documentDetailRow["ItemCode"].ToString())))
                            {
                                DBSetting dbSetting = this.myDBSetting;
                                string cmdText = "Select * from SerialNoTrans where DocType = ? AND DtlKey = ?";
                                int num = 0;
                                object[] objArray = new object[2];
                                int index1 = 0;
                                string str = docType;
                                objArray[index1] = (object)str;
                                int index2 = 1;
                                // ISSUE: variable of a boxed type
                                long local = dtlKey;
                                objArray[index2] = (object)local;
                                DataTable dataTable = dbSetting.GetDataTable(cmdText, num != 0, objArray);
                                if (dataTable.Rows.Count != 0)
                                    serialNumberHelper.GetTotalCostBySN(dataTable, documentDetailRow["ItemCode"].ToString(), documentDetailRow["Location"].ToString(), BCE.Data.Convert.ToDecimal(documentDetailRow["SmallestQty"]), ref localtotalCost);
                            }
                            if (this.mySmartUpdate && localtotalCost == this.myDecimalSetting.RoundCost(documentDetailRow["LocalTotalCost"]))
                            {
                                return;
                            }
                            else
                            {
                                row = this.myDetailTable.NewRow();
                                row["DocType"] = documentDetailRow["DocType"];
                                row["DtlKey"] = documentDetailRow["DtlKey"];
                                row["LocalTotalCost"] = (object)localtotalCost;
                            }
                        }
                        this.myDetailTable.Rows.Add(row);
                    }
                    else
                    {
                        Decimal num1 = BCE.Data.Convert.ToDecimal(documentDetailRow["CurrencyRate"]);
                        Decimal num2 = BCE.Data.Convert.ToDecimal(documentDetailRow["Qty"]);
                        Decimal num3 = BCE.Data.Convert.ToDecimal(documentDetailRow["FOCQty"]);
                        if ((!isFOCDtlKey && num2 != Decimal.Zero || isFOCDtlKey && num3 != Decimal.Zero) && num1 != Decimal.Zero)
                        {
                            if (docType == "CN" && ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.SerialNo.Enable && (dbRegistry.GetBoolean((IRegistryID)new UseSerialNumberCostForStockOut()) && InvoicingHelper.HasSerialNo(this.myDBSetting, documentDetailRow["ItemCode"].ToString())))
                            {
                                DBSetting dbSetting = this.myDBSetting;
                                string cmdText = "Select * from SerialNoTrans where DocType = ? AND DtlKey = ?";
                                int num4 = 0;
                                object[] objArray = new object[2];
                                int index1 = 0;
                                string str = docType;
                                objArray[index1] = (object)str;
                                int index2 = 1;
                                // ISSUE: variable of a boxed type
                                long local = dtlKey;
                                objArray[index2] = (object)local;
                                DataTable dataTable = dbSetting.GetDataTable(cmdText, num4 != 0, objArray);
                                if (dataTable.Rows.Count != 0)
                                    serialNumberHelper.GetTotalCostBySN(dataTable, documentDetailRow["ItemCode"].ToString(), documentDetailRow["Location"].ToString(), BCE.Data.Convert.ToDecimal(documentDetailRow["SmallestQty"]), ref localtotalCost);
                            }
                            if (this.mySmartUpdate && (!isFOCDtlKey && this.myDecimalSetting.RoundCost(-localtotalCost / num2 / num1) == BCE.Data.Convert.ToDecimal(documentDetailRow["UnitCost"]) || isFOCDtlKey && this.myDecimalSetting.RoundCost(-localtotalCost / num3 / num1) == BCE.Data.Convert.ToDecimal(documentDetailRow["FOCUnitCost"])))
                            {
                                return;
                            }
                            else
                            {
                                DataRow row = this.myDetailTable.NewRow();
                                row["DocType"] = documentDetailRow["DocType"];
                                row["DtlKey"] = documentDetailRow["DtlKey"];
                                row["LocalFOCTotalCost"] = isFOCDtlKey ? (object)this.myDecimalSetting.RoundCost(-localtotalCost / num3 / num1) : (object)this.myDecimalSetting.RoundCost(-localtotalCost / num2 / num1);
                                this.myDetailTable.Rows.Add(row);
                            }
                        }
                    }
                    if (this.myMasterTable.Select(string.Format("DocType='{0}' AND DocKey = {1}", (object)documentDetailRow["DocType"].ToString(), (object)documentDetailRow["DocKey"].ToString())).Length == 0)
                    {
                        DataRow row = this.myMasterTable.NewRow();
                        row["DocKey"] = documentDetailRow["DocKey"];
                        row["DocType"] = documentDetailRow["DocType"];
                        row["IsAssigned"] = (object)false;
                        this.myMasterTable.Rows.Add(row);
                    }
                    long docKey = BCE.Data.Convert.ToInt64(documentDetailRow["DocKey"]);
                    if (docType == "DO" || docType == "IV")
                    {
                        SalesTransferToStatus transferToStatus = (SalesTransferToStatus)new SalesTransferToStatusSQL(docType, docKey, this.myDBSetting);
                        DataTable partialTransferStatus = transferToStatus.GetPartialTransferStatus(dtlKey);
                        if (partialTransferStatus != null && partialTransferStatus.Rows.Count > 0)
                        {
                            foreach (DataRow transferredToRow in (InternalDataCollectionBase)partialTransferStatus.Rows)
                            {
                                if (transferredToRow["DocType"].ToString() != "CN")
                                {
                                    this.CalculatePartialTransferedTotalCost(documentDetailRow, transferredToRow);
                                    DataRow row1 = this.myDetailTable.NewRow();
                                    row1["DocType"] = transferredToRow["DocType"];
                                    row1["DtlKey"] = transferredToRow["DtlKey"];
                                    row1["LocalTotalCost"] = transferredToRow["LocalTotalCost"];
                                    if (transferredToRow.Table.Columns.IndexOf("FOCQty") >= 0)
                                        row1["LocalFOCTotalCost"] = transferredToRow["LocalFOCTotalCost"];
                                    this.myDetailTable.Rows.Add(row1);
                                    if (this.myMasterTable.Select("DocKey = " + transferredToRow["DocKey"].ToString(), "DocKey", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent).Length == 0)
                                    {
                                        DataRow row2 = this.myMasterTable.NewRow();
                                        row2["DocKey"] = transferredToRow["DocKey"];
                                        row2["DocType"] = transferredToRow["DocType"];
                                        row2["IsAssigned"] = (object)false;
                                        this.myMasterTable.Rows.Add(row2);
                                    }
                                }
                            }
                        }
                        DataTable withDetailRecords = transferToStatus.GetFullTransferStatusWithDetailRecords();
                        if (withDetailRecords != null && withDetailRecords.Rows.Count > 0)
                        {
                            DataRow[] dataRowArray = withDetailRecords.Select("FullTransferOption = 1 AND FromDocDtlKey = " + dtlKey.ToString(), "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
                            if (dataRowArray.Length != 0 && dataRowArray[0]["DocType"].ToString() != "CN")
                            {
                                this.CalculatePartialTransferedTotalCost(documentDetailRow, dataRowArray[0]);
                                DataRow row = this.myDetailTable.NewRow();
                                row["DocType"] = dataRowArray[0]["DocType"];
                                row["DtlKey"] = dataRowArray[0]["DtlKey"];
                                row["LocalTotalCost"] = dataRowArray[0]["LocalTotalCost"];
                                if (withDetailRecords.Columns.IndexOf("FOCQty") >= 0)
                                    row["LocalFOCTotalCost"] = dataRowArray[0]["LocalFOCTotalCost"];
                                this.myDetailTable.Rows.Add(row);
                            }
                            if (docType == "DO" && withDetailRecords.Columns.Contains("ToDocKey") && (withDetailRecords.Rows[0]["ToDocKey"] != DBNull.Value && withDetailRecords.Rows[0]["ToDocType"].ToString() == "CN") && this.myMasterTable.Select("DocKey = " + withDetailRecords.Rows[0]["ToDocKey"].ToString(), "DocKey", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent).Length == 0)
                            {
                                DataRow row = this.myMasterTable.NewRow();
                                row["DocKey"] = withDetailRecords.Rows[0]["ToDocKey"];
                                row["DocType"] = withDetailRecords.Rows[0]["ToDocType"];
                                row["IsAssigned"] = (object)false;
                                this.myMasterTable.Rows.Add(row);
                            }
                            if (this.myMasterTable.Select("DocKey = " + withDetailRecords.Rows[0]["DocKey"].ToString(), "DocKey", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent).Length == 0)
                            {
                                DataRow row = this.myMasterTable.NewRow();
                                row["DocKey"] = withDetailRecords.Rows[0]["DocKey"];
                                row["DocType"] = withDetailRecords.Rows[0]["DocType"];
                                row["IsAssigned"] = (object)false;
                                this.myMasterTable.Rows.Add(row);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // ISSUE: variable of a boxed type
                RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_LogMessage;
                object[] objArray = new object[3];
                int index1 = 0;
                string str1 = docType;
                objArray[index1] = (object)str1;
                int index2 = 1;
                string str2 = dtlKey.ToString();
                objArray[index2] = (object)str2;
                int index3 = 2;
                string message = ex.Message;
                objArray[index3] = (object)message;
                string @string = Localizer.GetString((Enum)local, objArray);
                this.myErrorCount = this.myErrorCount + 1;
                this.WriteLog(this.myDBSetting, @string);
            }
        }

        public void UpdateDetailLocalTotalCost()
        {
            DBSetting dbSetting = this.myDBSetting.StartTransaction();
            try
            {
                SqlCommand command = dbSetting.CreateCommand();
                foreach (DataRow dataRow in (InternalDataCollectionBase)this.myDetailTable.Rows)
                {
                    string str1 = dataRow["DocType"].ToString();
                    long num1 = BCE.Data.Convert.ToInt64(dataRow["DtlKey"]);
                    Decimal num2 = this.myDecimalSetting.RoundCost(dataRow["LocalTotalCost"]);
                    Decimal num3 = this.myDecimalSetting.RoundCost(dataRow["LocalFOCTotalCost"]);
                    string str2;
                    if (str1 == "IV")
                        str2 = " Update IVDTL SET LocalTotalCost = @LocalTotalCost, LocalFOCTotalCost = @LocalFOCTotalCost  Where DtlKey = @DtlKey ";
                    else if (str1 == "CS")
                        str2 = " Update CSDTL SET LocalTotalCost = @LocalTotalCost, LocalFOCTotalCost = @LocalFOCTotalCost  Where DtlKey = @DtlKey ";
                    else if (str1 == "DR")
                        str2 = " Update DRDTL SET LocalTotalCost = @LocalTotalCost, LocalFOCTotalCost = @LocalFOCTotalCost  Where DtlKey = @DtlKey ";
                    else if (str1 == "CN")
                        str2 = " Update CNDTL SET UnitCost = @LocalTotalCost, FOCUnitCost = @LocalFOCTotalCost  Where DtlKey = @DtlKey ";
                    else if (str1 == "DN")
                        str2 = " Update DNDTL SET LocalTotalCost = @LocalTotalCost, LocalFOCTotalCost = @LocalFOCTotalCost  Where DtlKey = @DtlKey ";
                    else if (str1 == "DO")
                        str2 = " Update DODTL SET LocalTotalCost = @LocalTotalCost, LocalFOCTotalCost = @LocalFOCTotalCost  Where DtlKey = @DtlKey ";
                    else if (str1 == "OS")
                        str2 = " Update POSDTL SET LocalTotalCost = @LocalTotalCost  Where DtlKey = @DtlKey ";
                    else
                        continue;
                    command.CommandText = str2;
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@LocalTotalCost", (object)num2);
                    if (str1 != "OS")
                        command.Parameters.AddWithValue("@LocalFOCTotalCost", (object)num3);
                    command.Parameters.AddWithValue("@DtlKey", (object)num1);
                    command.ExecuteNonQuery();
                }
                dbSetting.Commit();
            }
            catch (SqlException ex)
            {
                BCE.Data.DataError.HandleSqlException(ex);
            }
            finally
            {
                dbSetting.EndTransaction();
            }
        }

        public void UpdateMasterLocalTotalCost()
        {
            if (this.myMasterTable.Rows.Count != 0)
            {
                DBSetting dbSetting1 = this.myDBSetting.StartTransaction();
                try
                {
                    foreach (DataRow dataRow1 in (InternalDataCollectionBase)this.myMasterTable.Rows)
                    {
                        if (!BCE.Data.Convert.ToBoolean(dataRow1["IsAssigned"]))
                        {
                            string str1 = dataRow1["DocType"].ToString();
                            long num1 = BCE.Data.Convert.ToInt64(dataRow1["DocKey"]);
                            string str2;
                            string str3;
                            string cmdText1;
                            string cmdText2;
                            if (str1 == "IV")
                            {
                                str2 = "Select * From IV Where DocKey = @DocKey";
                                str3 = "Select * From IVDTL Where DocKey = @DocKey ORDER BY Seq";
                                cmdText1 = "Select * From IV";
                                cmdText2 = "Select * From IVDTL";
                            }
                            else if (str1 == "CS")
                            {
                                str2 = "Select * From CS Where DocKey = @DocKey";
                                str3 = "Select * From CSDTL Where DocKey = @DocKey ORDER BY Seq";
                                cmdText1 = "Select * From CS";
                                cmdText2 = "Select * From CSDTL";
                            }
                            else if (str1 == "DR")
                            {
                                str2 = "Select * From DR Where DocKey = @DocKey";
                                str3 = "Select * From DRDTL Where DocKey = @DocKey ORDER BY Seq";
                                cmdText1 = "Select * From DR";
                                cmdText2 = "Select * From DRDTL";
                            }
                            else if (str1 == "CN")
                            {
                                str2 = "Select * From CN Where DocKey = @DocKey";
                                str3 = "Select * From CNDTL Where DocKey = @DocKey ORDER BY Seq";
                                cmdText1 = "Select * From CN";
                                cmdText2 = "Select * From CNDTL";
                            }
                            else if (str1 == "DO")
                            {
                                str2 = "Select * From DO Where DocKey = @DocKey";
                                str3 = "Select * From DODTL Where DocKey = @DocKey ORDER BY Seq";
                                cmdText1 = "Select * From DO";
                                cmdText2 = "Select * From DODTL";
                            }
                            else if (str1 == "DN")
                            {
                                str2 = "Select * From DN Where DocKey = @DocKey";
                                str3 = "Select * From DNDTL Where DocKey = @DocKey ORDER BY Seq";
                                cmdText1 = "Select * From DN";
                                cmdText2 = "Select * From DNDTL";
                            }
                            else if (str1 == "OS")
                            {
                                DBSetting dbSetting2 = dbSetting1;
                                string cmdText3 = "UPDATE POS SET LocalTotalCost = (SELECT SUM(LocalTotalCost) FROM POSDTL WHERE DocKey=POS.DocKey) WHERE DocKey = ?";
                                object[] objArray = new object[1];
                                int index = 0;
                                // ISSUE: variable of a boxed type
                                long local = num1;
                                objArray[index] = (object)local;
                                dbSetting2.ExecuteNonQuery(cmdText3, objArray);
                                continue;
                            }
                            else
                                continue;
                            DBSetting dbSetting3 = dbSetting1;
                            string cmdText4 = str2;
                            int num2 = 0;
                            object[] objArray1 = new object[1];
                            int index1 = 0;
                            SqlParameter sqlParameter1 = new SqlParameter("@DocKey", (object)num1);
                            objArray1[index1] = (object)sqlParameter1;
                            DataTable dataTable1 = dbSetting3.GetDataTable(cmdText4, num2 != 0, objArray1);
                            DBSetting dbSetting4 = dbSetting1;
                            string cmdText5 = str3;
                            int num3 = 0;
                            object[] objArray2 = new object[1];
                            int index2 = 0;
                            SqlParameter sqlParameter2 = new SqlParameter("@DocKey", (object)num1);
                            objArray2[index2] = (object)sqlParameter2;
                            DataTable dataTable2 = dbSetting4.GetDataTable(cmdText5, num3 != 0, objArray2);
                            bool flag = false;
                            string str4 = dataTable1.Rows[0]["DocNo"].ToString();
                            Decimal num4 = BCE.Data.Convert.ToDecimal(dataTable1.Rows[0]["CurrencyRate"]);
                            Decimal num5 = new Decimal();
                            Decimal num6 = new Decimal();
                            foreach (DataRow dataRow2 in (InternalDataCollectionBase)dataTable2.Rows)
                            {
                                if (dataRow2["PackageDocKey"] != DBNull.Value)
                                {
                                    if (str1 == "CN")
                                    {
                                        dataRow2["UnitCost"] = (object)0;
                                        dataRow2["FOCUnitCost"] = (object)0;
                                    }
                                    else
                                    {
                                        dataRow2["LocalTotalCost"] = (object)0;
                                        dataRow2["LocalFOCTotalCost"] = (object)0;
                                    }
                                }
                            }
                            foreach (DataRow dataRow2 in (InternalDataCollectionBase)dataTable2.Rows)
                            {
                                if (dataRow2["PackageDocKey"] == DBNull.Value)
                                {
                                    if (str1 != "DO" && str1 != "DN")
                                    {
                                        if (dataRow2["FullTransferOption"].ToString() == "2" || dataRow2["FullTransferOption"].ToString() == "3")
                                        {
                                            string str5 = dataRow2["FullTransferFromDocList"].ToString();
                                            char[] chArray = new char[1];
                                            int index3 = 0;
                                            int num7 = 44;
                                            chArray[index3] = (char)num7;
                                            string[] strArray = str5.Split(chArray);
                                            if (str1 != "CN")
                                            {
                                                if (strArray.Length != 0)
                                                {
                                                    Decimal number = new Decimal();
                                                    foreach (string str6 in strArray)
                                                    {
                                                        if (str6.Length != 0)
                                                        {
                                                            string docType = str6.Substring(0, 2);
                                                            if (!(docType == "QT") && !(docType == "SO"))
                                                            {
                                                                string docKey = str6.Substring(2);
                                                                DataTable dataTable3 = this.LoadMaster(docType, docKey, dbSetting1);
                                                                if (dataTable3 != null && dataTable3.Rows.Count > 0)
                                                                {
                                                                    if (this.myMasterTable.Select("IsAssigned = False and DocKey = " + dataTable3.Rows[0]["DocKey"].ToString(), "", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent).Length != 0)
                                                                        number += this.ExternallyUpdateMasterLocalTotalCost(BCE.Data.Convert.ToInt64(dataTable3.Rows[0]["DocKey"]), dbSetting1);
                                                                    else
                                                                        number += BCE.Data.Convert.ToDecimal(dataTable3.Rows[0]["LocalTotalCost"]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    dataRow2["LocalTotalCost"] = (object)this.myDecimalSetting.RoundCost(number);
                                                    dataRow2["LocalFOCTotalCost"] = (object)0;
                                                }
                                            }
                                            else if (strArray.Length != 0)
                                            {
                                                Decimal num8 = new Decimal();
                                                foreach (string str6 in strArray)
                                                {
                                                    if (str6.Length != 0)
                                                    {
                                                        string docType = str6.Substring(0, 2);
                                                        if (!(docType == "QT") && !(docType == "SO"))
                                                        {
                                                            string docKey = str6.Substring(2);
                                                            DataTable dataTable3 = this.LoadMaster(docType, docKey, dbSetting1);
                                                            if (dataTable3 != null && dataTable3.Rows.Count > 0)
                                                            {
                                                                if (this.myMasterTable.Select("IsAssigned = False and DocKey = " + dataTable3.Rows[0]["DocKey"].ToString(), "", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent).Length != 0)
                                                                    num8 += this.ExternallyUpdateMasterLocalTotalCost(BCE.Data.Convert.ToInt64(dataTable3.Rows[0]["DocKey"]), dbSetting1);
                                                                else
                                                                    num8 += BCE.Data.Convert.ToDecimal(dataTable3.Rows[0]["LocalTotalCost"]);
                                                            }
                                                        }
                                                    }
                                                }
                                                dataRow2["UnitCost"] = (object)this.myDecimalSetting.RoundCost(num8 / num4);
                                                dataRow2["FOCUnitCost"] = (object)0;
                                            }
                                        }
                                        else if (dataRow2["FullTransferOption"].ToString() == "1" && dataRow2["ItemCode"] == DBNull.Value)
                                        {
                                            if (dataRow2["FullTransferFromDocList"] == DBNull.Value)
                                            {
                                                flag = true;
                                                continue;
                                            }
                                            else
                                            {
                                                string str5 = dataRow2["FullTransferFromDocList"].ToString();
                                                string docType = str5.Substring(0, 2);
                                                if (!(docType == "QT") && !(docType == "SO"))
                                                {
                                                    string str6 = str5.Substring(2);
                                                    if (this.myMasterTable.Select("IsAssigned = False and DocKey = " + str6).Length != 0)
                                                        this.ExternallyUpdateMasterLocalTotalCost(BCE.Data.Convert.ToInt64((object)str6), dbSetting1);
                                                    long dtlKey = BCE.Data.Convert.ToInt64(dataRow2["FromDocDtlKey"]);
                                                    DataTable dataTable3 = this.LoadDetail(docType, dtlKey, dbSetting1);
                                                    if (dataTable3 != null && dataTable3.Rows.Count > 0)
                                                    {
                                                        Decimal num7 = BCE.Data.Convert.ToDecimal(dataTable3.Rows[0]["LocalTotalCost"]);
                                                        Decimal num8 = BCE.Data.Convert.ToDecimal(dataTable3.Rows[0]["Qty"]);
                                                        Decimal num9 = BCE.Data.Convert.ToDecimal(dataTable3.Rows[0]["LocalFOCTotalCost"]);
                                                        Decimal num10 = BCE.Data.Convert.ToDecimal(dataTable3.Rows[0]["FOCQty"]);
                                                        if (str1 != "CN")
                                                        {
                                                            dataRow2["LocalTotalCost"] = dataTable3.Rows[0]["LocalTotalCost"];
                                                            dataRow2["LocalFOCTotalCost"] = dataTable3.Rows[0]["LocalFOCTotalCost"];
                                                        }
                                                        else
                                                        {
                                                            if (num8 != Decimal.Zero)
                                                                dataRow2["UnitCost"] = (object)this.myDecimalSetting.RoundCost(num7 / num8 * num4);
                                                            if (num10 != Decimal.Zero)
                                                                dataRow2["FOCUnitCost"] = (object)this.myDecimalSetting.RoundCost(num9 / num10 * num4);
                                                        }
                                                    }
                                                }
                                                else
                                                    continue;
                                            }
                                        }
                                    }
                                    Decimal num11 = new Decimal();
                                    Decimal num12 = new Decimal();
                                    if (str1 != "CN")
                                    {
                                        num11 = BCE.Data.Convert.ToDecimal(dataRow2["LocalTotalCost"]);
                                        num12 = BCE.Data.Convert.ToDecimal(dataRow2["LocalFOCTotalCost"]);
                                    }
                                    else
                                    {
                                        string str5 = dataRow2["DtlType"].ToString();
                                        if (str5 == "N" || str5 == "P")
                                        {
                                            num11 += this.myDecimalSetting.RoundCost(BCE.Data.Convert.ToDecimal(dataRow2["UnitCost"]) * BCE.Data.Convert.ToDecimal(dataRow2["Qty"]) * num4);
                                            num12 += this.myDecimalSetting.RoundCost(BCE.Data.Convert.ToDecimal(dataRow2["FOCUnitCost"]) * BCE.Data.Convert.ToDecimal(dataRow2["FOCQty"]) * num4);
                                        }
                                    }
                                    num5 += num11;
                                    num6 += num12;
                                    if (dataRow2["ParentDtlKey"] != DBNull.Value)
                                    {
                                        DataRow[] dataRowArray = dataTable2.Select("DtlKey = " + (object)BCE.Data.Convert.ToInt64(dataRow2["ParentDtlKey"]));
                                        if (dataRowArray.Length != 0)
                                        {
                                            DataRow dataRow3 = dataRowArray[0];
                                            if (str1 == "CN")
                                            {
                                                Decimal num7 = BCE.Data.Convert.ToDecimal(dataRow3["Qty"]);
                                                if (num7 != Decimal.Zero)
                                                    dataRow3["UnitCost"] = (object)(BCE.Data.Convert.ToDecimal(dataRow3["UnitCost"]) + num11 / num7 / num4);
                                                Decimal num8 = BCE.Data.Convert.ToDecimal(dataRow3["FOCQty"]);
                                                if (num8 != Decimal.Zero)
                                                    dataRow3["FOCUnitCost"] = (object)(BCE.Data.Convert.ToDecimal(dataRow3["FOCUnitCost"]) + num12 / num8 / num4);
                                            }
                                            else
                                            {
                                                dataRow3["LocalTotalCost"] = (object)(BCE.Data.Convert.ToDecimal(dataRow3["LocalTotalCost"]) + num11);
                                                dataRow3["LocalFOCTotalCost"] = (object)(BCE.Data.Convert.ToDecimal(dataRow3["LocalFOCTotalCost"]) + num12);
                                            }
                                        }
                                    }
                                }
                            }
                            if (flag)
                            {
                                // ISSUE: variable of a boxed type
                                RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_LogMessage2;
                                object[] objArray3 = new object[2];
                                int index3 = 0;
                                string str5 = str1;
                                objArray3[index3] = (object)str5;
                                int index4 = 1;
                                string str6 = str4;
                                objArray3[index4] = (object)str6;
                                string @string = Localizer.GetString((Enum)local, objArray3);
                                this.myErrorCount = this.myErrorCount + 1;
                                this.WriteLog(dbSetting1, @string);
                            }
                            dataTable1.Rows[0]["LocalTotalCost"] = (object)(num5 + num6);
                            SqlDataAdapter adapter1 = new SqlDataAdapter(dbSetting1.CreateCommand(cmdText1, new object[0]));
                            SqlCommandBuilder sqlCommandBuilder1 = new SqlCommandBuilder(adapter1);
                            DataTable dataTable4 = dataTable1;
                            adapter1.Update(dataTable4);
                            SqlDataAdapter adapter2 = new SqlDataAdapter(dbSetting1.CreateCommand(cmdText2, new object[0]));
                            SqlCommandBuilder sqlCommandBuilder2 = new SqlCommandBuilder(adapter2);
                            DataTable dataTable5 = dataTable2;
                            adapter2.Update(dataTable5);
                            dataRow1["IsAssigned"] = (object)true;
                        }
                    }
                    dbSetting1.Commit();
                }
                catch (SqlException ex)
                {
                    BCE.Data.DataError.HandleSqlException(ex);
                }
                finally
                {
                    dbSetting1.EndTransaction();
                }
            }
        }

        public void ShowError()
        {
            if (this.myErrorCount > 0 && this.myErrorLogFilename.Length > 0)
            {
                AppMessage.ShowMessage(Localizer.GetString((Enum)RecalculateStockCostingStringId.ShowMessage_RecalculatedError, new object[0]));
                Process.Start("Notepad.exe", this.myErrorLogFilename);
            }
        }

        private Decimal ExternallyUpdateMasterLocalTotalCost(long docKey, DBSetting newDBSetting)
        {
            DataRow[] dataRowArray = this.myMasterTable.Select("DocKey = " + docKey.ToString());
            string str1 = dataRowArray[0]["DocType"].ToString();
            string masterSQL;
            string detailSQL;
            string selectCmdText1;
            string selectCmdText2;
            if (str1 == "IV")
            {
                masterSQL = "Select * From IV Where DocKey = @DocKey";
                detailSQL = "Select * From IVDTL Where DocKey = @DocKey ORDER BY Seq";
                selectCmdText1 = "Select * From IV";
                selectCmdText2 = "Select * From IVDTL";
            }
            else if (str1 == "DO")
            {
                masterSQL = "Select * From DO Where DocKey = @DocKey";
                detailSQL = "Select * From DODTL Where DocKey = @DocKey ORDER BY Seq";
                selectCmdText1 = "Select * From DO";
                selectCmdText2 = "Select * From DODTL";
            }
            else
                return Decimal.Zero;
            DataSet dataSet = DocumentHelper.LoadMasterDetailData(this.myDBSetting, docKey, masterSQL, detailSQL);
            DataTable table1 = dataSet.Tables["Master"];
            DataTable table2 = dataSet.Tables["Detail"];
            BCE.Data.Convert.ToDecimal(table1.Rows[0]["CurrencyRate"]);
            Decimal num1 = new Decimal();
            Decimal num2 = new Decimal();
            foreach (DataRow dataRow in (InternalDataCollectionBase)table2.Rows)
            {
                if (str1 != "DO" && (dataRow["FullTransferOption"].ToString() == "2" || dataRow["FullTransferOption"].ToString() == "3"))
                {
                    string str2 = dataRow["FullTransferFromDocList"].ToString();
                    char[] chArray = new char[1];
                    int index = 0;
                    int num3 = 44;
                    chArray[index] = (char)num3;
                    string[] strArray = str2.Split(chArray);
                    if (strArray.Length != 0)
                    {
                        Decimal number = new Decimal();
                        foreach (string str3 in strArray)
                        {
                            if (str3.Length != 0)
                            {
                                string docType = str3.Substring(0, 2);
                                if (!(docType == "QT") && !(docType == "SO"))
                                {
                                    string docKey1 = str3.Substring(2);
                                    DataTable dataTable = this.LoadMaster(docType, docKey1, newDBSetting);
                                    if (dataTable != null && dataTable.Rows.Count > 0)
                                    {
                                        if (this.myMasterTable.Select("IsAssigned = False and DocKey = " + dataTable.Rows[0]["DocKey"].ToString(), "", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent).Length != 0)
                                            number += this.ExternallyUpdateMasterLocalTotalCost(BCE.Data.Convert.ToInt64(dataTable.Rows[0]["DocKey"]), newDBSetting);
                                        else
                                            number += BCE.Data.Convert.ToDecimal(dataTable.Rows[0]["LocalTotalCost"]);
                                    }
                                }
                            }
                        }
                        dataRow["LocalTotalCost"] = (object)this.myDecimalSetting.RoundCost(number);
                        dataRow["LocalFOCTotalCost"] = (object)0;
                    }
                }
                num1 += BCE.Data.Convert.ToDecimal(dataRow["LocalTotalCost"]);
                num2 += BCE.Data.Convert.ToDecimal(dataRow["LocalFOCTotalCost"]);
            }
            table1.Rows[0]["LocalTotalCost"] = (object)this.myDecimalSetting.RoundCost(num1 + num2);
            newDBSetting.SimpleSaveDataTable(table1, selectCmdText1);
            newDBSetting.SimpleSaveDataTable(table2, selectCmdText2);
            dataRowArray[0]["IsAssigned"] = (object)true;
            return BCE.Data.Convert.ToDecimal(table1.Rows[0]["LocalTotalCost"]);
        }

        private void CalculatePartialTransferedTotalCost(DataRow detailRow, DataRow transferredToRow)
        {
            if (transferredToRow["DocType"].ToString() != "CN")
            {
                Decimal num1 = this.myDecimalSetting.RoundCost(detailRow["LocalTotalCost"]);
                Decimal num2 = this.myDecimalSetting.RoundQuantity(detailRow["SmallestQty"]);
                Decimal num3 = this.myDecimalSetting.RoundQuantity(transferredToRow["SmallestQty"]);
                transferredToRow["LocalTotalCost"] = !(num2 != Decimal.Zero) ? (object)Decimal.Zero : (object)this.myDecimalSetting.RoundCost(num3 / num2 * num1);
                if (transferredToRow.Table.Columns.IndexOf("FOCQty") >= 0 && transferredToRow["FOCQty"] != DBNull.Value)
                {
                    Decimal num4 = this.myDecimalSetting.RoundCost(detailRow["LocalFOCTotalCost"]);
                    Decimal num5 = this.myDecimalSetting.RoundQuantity(detailRow["FOCQty"]);
                    Decimal num6 = this.myDecimalSetting.RoundQuantity(transferredToRow["FOCQty"]);
                    if (num5 != Decimal.Zero)
                        transferredToRow["LocalFOCTotalCost"] = (object)this.myDecimalSetting.RoundCost(num6 / num5 * num4);
                    else
                        transferredToRow["LocalFOCTotalCost"] = (object)Decimal.Zero;
                }
            }
        }

        private DataTable LoadMaster(string docType, string docKey, DBSetting newDBSetting)
        {
            string cmdText;
            if (docType == "DO")
                cmdText = "Select LocalTotalCost, DocKey From DO Where DocKey = " + docKey;
            else if (docType == "IV")
                cmdText = "Select LocalTotalCost, DocKey From IV Where DocKey = " + docKey;
            else
                return new DataTable();
            return newDBSetting.GetDataTable(cmdText, false, new object[0]);
        }

        private DataRow GetDocumentDetailRow(string docType, long dtlKey, ref bool isFOCDtlKey)
        {
            string str;
            if (docType == "DO")
                str = "Select DocKey, DtlKey, LocalTotalCost, LocalFOCTotalCost, FOCQty, SmallestQty, 'DO' As DocType, ItemCode, Location From DODTL Where DtlKey = @DtlKey OR FOCDtlKey = @DtlKey";
            else if (docType == "IV")
                str = "Select DocKey, DtlKey, LocalTotalCost, LocalFOCTotalCost, FOCQty, SmallestQty, 'IV' As DocType, ItemCode, Location From IVDTL Where DtlKey = @DtlKey OR FOCDtlKey = @DtlKey";
            else if (docType == "CS")
                str = "Select DocKey, DtlKey, LocalTotalCost, LocalFOCTotalCost, FOCQty, SmallestQty, 'CS' As DocType, ItemCode, Location From CSDTL Where DtlKey = @DtlKey OR FOCDtlKey = @DtlKey";
            else if (docType == "DN")
                str = "Select DocKey, DtlKey, LocalTotalCost, LocalFOCTotalCost, FOCQty, SmallestQty, 'DN' As DocType, ItemCode, Location From DNDTL Where DtlKey = @DtlKey OR FOCDtlKey = @DtlKey";
            else if (docType == "CN")
                str = "Select A.CurrencyRate, 'CN' As DocType, B.DocKey, B.DtlKey, B.Qty, B.FOCQty, B.UnitCost, B.FOCUnitCost, B.SmallestQty, B.ItemCode, B.Location From CNDTL B Inner Join CN A On A.DocKey = B.DocKey Where B.DtlKey = @DtlKey OR B.FOCDtlKey = @DtlKey";
            else if (docType == "DR")
                str = "Select DocKey, DtlKey, LocalTotalCost, LocalFOCTotalCost, FOCQty, SmallestQty, 'DR' As DocType From DRDTL Where DtlKey = @DtlKey OR FOCDtlKey = @DtlKey";
            else if (docType == "OS")
                str = "Select B.DocKey, B.DtlKey, B.LocalTotalCost, 'OS' As DocType, (COALESCE(B.Qty, 0) * COALESCE(U.Rate, 1)) As SmallestQty, B.ItemCode, T.Outlet As Location From POSDTL B Inner Join POS A On A.DocKey = B.DocKey Left Outer Join PosTerminal T On A.TerminalID=T.TerminalID Left Outer Join ItemUOM U On B.ItemCode=U.ItemCode AND B.UOM=U.UOM Where B.DtlKey = @DtlKey";
            else
                throw new ArgumentException("Incorrect Document Type.", "docType");
            DBSetting dbSetting = this.myDBSetting;
            string cmdText = str;
            object[] objArray = new object[1];
            int index = 0;
            SqlParameter sqlParameter = new SqlParameter("@DtlKey", (object)dtlKey);
            objArray[index] = (object)sqlParameter;
            DataRow firstDataRow = dbSetting.GetFirstDataRow(cmdText, objArray);
            if (firstDataRow != null)
                isFOCDtlKey = BCE.Data.Convert.ToInt64(firstDataRow["DtlKey"]) != dtlKey;
            return firstDataRow;
        }

        private DataTable LoadDetail(string docType, long dtlKey, DBSetting newDBSetting)
        {
            string cmdText;
            if (docType == "DO")
                cmdText = "Select *, 'DO' As DocType From DODTL Where DtlKey = " + dtlKey.ToString();
            else if (docType == "IV")
                cmdText = "Select *, 'IV' As DocType From IVDTL Where DtlKey = " + dtlKey.ToString();
            else if (docType == "CS")
                cmdText = "Select *, 'CS' As DocType From CSDTL Where DtlKey = " + dtlKey.ToString();
            else if (docType == "DN")
                cmdText = "Select *, 'DN' As DocType From DNDTL Where DtlKey = " + dtlKey.ToString();
            else if (docType == "CN")
                cmdText = "Select A.CurrencyRate, 'CN' As DocType, B.* From CNDTL B Inner Join CN A On A.DocKey = B.DocKey Where B.DtlKey = " + dtlKey.ToString();
            else if (docType == "DR")
                cmdText = "Select *, 'DR' As DocType From DRDTL Where DtlKey = " + dtlKey.ToString();
            else
                throw new ArgumentException("Incorrect Document Type.", "docType");
            return newDBSetting.GetDataTable(cmdText, false, new object[0]);
        }

        private static string GetErrorLogFilename(DBSetting dbSetting)
        {
            string str1 = BCE.Misc.Utils.ConvertCompanyNameRemarkToFilename((string)GeneralSetting.GetOrCreate(dbSetting).CompanyProfile.CompanyName, (string)GeneralSetting.GetOrCreate(dbSetting).CompanyProfile.Remark);
            string myAutoCountFolder = Application.MyAutoCountFolder;
            // ISSUE: variable of a boxed type
            RecalculateStockCostingStringId local = RecalculateStockCostingStringId.ErrorMessage_RecalculateStockCostingError;
            object[] objArray = new object[2];
            int index1 = 0;
            string str2 = str1;
            objArray[index1] = (object)str2;
            int index2 = 1;
            string str3 = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
            objArray[index2] = (object)str3;
            string @string = Localizer.GetString((Enum)local, objArray);
            return Path.Combine(myAutoCountFolder, @string);
        }

        private void WriteLog(DBSetting dbSetting, string message)
        {
            if (this.myErrorLogFilename.Length == 0)
                this.myErrorLogFilename = RecalculateSalesDocumentCosting.GetErrorLogFilename(dbSetting);
            StreamWriter streamWriter = File.AppendText(this.myErrorLogFilename);
            foreach (string str in StringHelper.ToMultiLineString(message))
                streamWriter.WriteLine(str);
            ((TextWriter)streamWriter).Flush();
            streamWriter.Close();
        }
    }

}
